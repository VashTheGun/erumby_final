% --------------------
%% Plot telemetry data
% --------------------

% ------------
% Plot the main quantities
% ------------
label_fig_mainQuantities = strcat({'Main quantities'},{' '},{testID});
figure('Name',label_fig_mainQuantities{1},'NumberTitle','off'), clf 
% --- u --- %
ax(1) = subplot(231);
hold on
plot(time_telem_orig,u_telem_orig,'.')
plot(time_telem_NN,u_telem_NN,'.')
grid on
title('$u$ [m/s]')
legend('full data','NN data','location','best')
xlim([time_telem(1) time_telem(end)])
ylim([-0.1 3.5])
% --- delta --- %
ax(2) = subplot(232);
hold on
plot(time_telem_orig,rad2deg(delta_telem_orig),'.')
plot(time_telem_NN,rad2deg(delta_telem_NN),'.')
grid on
title('$\delta$ [deg]')
legend('full data','NN data','location','best')
xlim([time_telem(1) time_telem(end)])
ylim([-25 25])
% --- Omega --- %
ax(3) = subplot(233);
hold on
plot(time_telem_orig,Omega_telem_orig,'.')
plot(time_telem_NN,Omega_telem_NN,'.')
grid on
title('$\Omega$ [rad/s] IMU')
legend('full data','NN data','location','best')
xlim([time_telem(1) time_telem(end)])
% --- Ay --- %
ax(4) = subplot(234);
hold on
plot(time_telem_orig,Ay_telem_orig,'.')
plot(time_telem_NN,Ay_telem_NN,'.')
% plot(time_telem,Ay_ss_telem,'.')
grid on
title('$A_{y}$ [m/s$^2$] IMU')
legend('full data','NN data','location','best')
% legend('IMU','$A_{y,ss} = \Omega u$','location','best')
xlim([time_telem(1) time_telem(end)])
% --- Ax --- %
ax(5) = subplot(235);
hold on
plot(time_telem_orig,Ax_telem_orig,'.')
plot(time_telem_NN,Ax_telem_NN,'.')
grid on
title('$A_{x}$ [m/s$^2$] IMU')
legend('full data','NN data','location','best')
xlim([time_telem(1) time_telem(end)])
% --- others --- %
ax(6) = subplot(236);
hold on
scale_delta = 7;
plot(time_telem_orig,delta_telem_orig*scale_delta,'.')
plot(time_telem_orig,Omega_telem_orig,'.')
grid on
legend('$\delta$ scaled','$\Omega$','location','best')
ylim([-2.5 2.5])

% ------------
% Plot vehicle path and pose
% ------------
label_fig_vehPath = strcat({'Vehicle path'},{' '},{testID});
figure('Name',label_fig_vehPath{1},'NumberTitle','off'), clf
% --- path --- %
ax(1) = subplot(221);
hold on
plot(x_telem_orig,y_telem_orig,'.')
plot(x_telem_NN,y_telem_NN,'.')
grid on
axis equal
xlabel('$x$ [m]')
ylabel('$y$ [m]')
title('Vehicle path')
legend('full data','NN data','location','best')
% --- x coord --- %
ax(2) = subplot(222);
hold on
plot(time_telem_orig,x_telem_orig,'.')
plot(time_telem_NN,x_telem_NN,'.')
grid on
title('$x$ [m]')
legend('full data','NN data','location','best')
xlim([time_telem(1) time_telem(end)])
% --- y coord --- %
ax(3) = subplot(223);
hold on
plot(time_telem_orig,y_telem_orig,'.')
plot(time_telem_NN,y_telem_NN,'.')
grid on
title('$y$ [m]')
legend('full data','NN data','location','best')
xlim([time_telem(1) time_telem(end)])
% --- psi --- %
ax(4) = subplot(224);
hold on
psi_plot = rad2deg(psi_telem);
plot(time_telem_orig,psi_telem_orig,'.')
plot(time_telem_NN,psi_telem_NN,'.')
grid on
title('$\psi$ [deg]')
legend('full data','NN data','location','best')
xlim([time_telem(1) time_telem(end)])
