% ---------------------------------------
%% Extract and re-sample telemetry data
% ---------------------------------------

time_telem_orig  = telemData.data(:,1);
Ts_orig = time_telem_orig(2)-time_telem_orig(1);
u_telem_orig     = telemData.data(:,2);
delta_telem_orig = telemData.data(:,3);
Omega_telem_orig = telemData.data(:,4);
Ay_telem_orig    = telemData.data(:,5);
Ay_ss_telem_orig = telemData.data(:,6);
Ax_telem_orig    = telemData.data(:,7);
x_telem_orig     = telemData.data(:,8);
y_telem_orig     = telemData.data(:,9);
psi_telem_orig   = telemData.data(:,10);

% Down-sampling of the original signals
Ts_new = 0.01;  % [s]
time_telem  = (time_telem_orig(1):Ts_new:time_telem_orig(end))';
u_telem     = interp1(time_telem_orig,u_telem_orig,time_telem);
delta_telem = interp1(time_telem_orig,delta_telem_orig,time_telem);
Omega_telem = interp1(time_telem_orig,Omega_telem_orig,time_telem);
Ay_telem    = interp1(time_telem_orig,Ay_telem_orig,time_telem);
Ay_ss_telem = interp1(time_telem_orig,Ay_ss_telem_orig,time_telem);
Ax_telem    = interp1(time_telem_orig,Ax_telem_orig,time_telem);
x_telem     = interp1(time_telem_orig,x_telem_orig,time_telem);
y_telem     = interp1(time_telem_orig,y_telem_orig,time_telem);
psi_telem   = interp1(time_telem_orig,psi_telem_orig,time_telem);

time_telem_NN_orig  = [];
time_telem_NN = [];  % down-sampled version
u_telem_NN_orig     = [];
delta_telem_NN_orig = [];
Omega_telem_NN_orig = [];
Ay_telem_NN_orig    = [];
Ay_ss_telem_NN_orig = [];
Ax_telem_NN_orig    = [];
x_telem_NN_orig     = [];
y_telem_NN_orig     = [];
psi_telem_NN_orig   = [];
for jj=1:size(indices_sel,1)
    time_telem_NN_orig  = [time_telem_NN_orig; time_telem_orig(indices_sel(jj,1):indices_sel(jj,2))];
    time_telem_NN = [time_telem_NN; (time_telem_orig(indices_sel(jj,1)):Ts_new:time_telem_orig(indices_sel(jj,2)))'];
    u_telem_NN_orig     = [u_telem_NN_orig; u_telem_orig(indices_sel(jj,1):indices_sel(jj,2))];
    delta_telem_NN_orig = [delta_telem_NN_orig; delta_telem_orig(indices_sel(jj,1):indices_sel(jj,2))];
    Omega_telem_NN_orig = [Omega_telem_NN_orig; Omega_telem_orig(indices_sel(jj,1):indices_sel(jj,2))];
    Ay_telem_NN_orig    = [Ay_telem_NN_orig; Ay_telem_orig(indices_sel(jj,1):indices_sel(jj,2))];
    Ay_ss_telem_NN_orig = [Ay_ss_telem_NN_orig; Ay_ss_telem_orig(indices_sel(jj,1):indices_sel(jj,2))];
    Ax_telem_NN_orig    = [Ax_telem_NN_orig; Ax_telem_orig(indices_sel(jj,1):indices_sel(jj,2))];
    x_telem_NN_orig     = [x_telem_NN_orig; x_telem_orig(indices_sel(jj,1):indices_sel(jj,2))];
    y_telem_NN_orig     = [y_telem_NN_orig; y_telem_orig(indices_sel(jj,1):indices_sel(jj,2))];
    psi_telem_NN_orig   = [psi_telem_NN_orig; psi_telem_orig(indices_sel(jj,1):indices_sel(jj,2))];
end

% Down-sampling of the NN signals
u_telem_NN     = interp1(time_telem_NN_orig,u_telem_NN_orig,time_telem_NN);
delta_telem_NN = interp1(time_telem_NN_orig,delta_telem_NN_orig,time_telem_NN);
Omega_telem_NN = interp1(time_telem_NN_orig,Omega_telem_NN_orig,time_telem_NN);
Ay_telem_NN    = interp1(time_telem_NN_orig,Ay_telem_NN_orig,time_telem_NN);
Ay_ss_telem_NN = interp1(time_telem_NN_orig,Ay_ss_telem_NN_orig,time_telem_NN);
Ax_telem_NN    = interp1(time_telem_NN_orig,Ax_telem_NN_orig,time_telem_NN);
x_telem_NN     = interp1(time_telem_NN_orig,x_telem_NN_orig,time_telem_NN);
y_telem_NN     = interp1(time_telem_NN_orig,y_telem_NN_orig,time_telem_NN);
psi_telem_NN   = interp1(time_telem_NN_orig,psi_telem_NN_orig,time_telem_NN);

if (isempty(time_telem_NN_full))
    t_fin = 0;
else
    t_fin = time_telem_NN_full(end);
end
time_telem_NN_full  = [time_telem_NN_full; t_fin+time_telem_NN];
u_telem_NN_full     = [u_telem_NN_full; u_telem_NN];
delta_telem_NN_full = [delta_telem_NN_full; delta_telem_NN];
Omega_telem_NN_full = [Omega_telem_NN_full; Omega_telem_NN];
Ay_telem_NN_full    = [Ay_telem_NN_full; Ay_telem_NN];
Ay_ss_telem_NN_full = [Ay_ss_telem_NN_full; Ay_ss_telem_NN];
Ax_telem_NN_full    = [Ax_telem_NN_full; Ax_telem_NN];
x_telem_NN_full     = [x_telem_NN_full; x_telem_NN];
y_telem_NN_full     = [y_telem_NN_full; y_telem_NN]; 
psi_telem_NN_full   = [psi_telem_NN_full; psi_telem_NN];

