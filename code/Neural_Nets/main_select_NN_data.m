% -----------------------------------
%% Load and plot telemetry data
% -----------------------------------

clc;
clearvars;

set(0,'DefaultFigureWindowStyle','docked');
set(0,'defaultAxesFontSize',20)
set(0,'DefaultLegendFontSize',20)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

addpath('utilities')

% --------------------
%% Load telemetry data
% --------------------

indices_data = load('./NN_dataset/ivalid.mat');
indices_use = {indices_data.Expression1, indices_data.Expression2, indices_data.Expression3, ...
               indices_data.Expression4, indices_data.Expression5};

% testID can be:
%   --> 'testMPCY', with Y = {10,12,14,15,16}, to load the telemetry of a full lap (with MPC)
test_names = {'10','12','14','15','16'};

% Initialize the arrays that will contain the full NN dataset
time_telem_NN_full  = [];
u_telem_NN_full     = [];
delta_telem_NN_full = [];
Omega_telem_NN_full = [];
Ay_telem_NN_full    = [];
Ay_ss_telem_NN_full = [];
Ax_telem_NN_full    = [];
x_telem_NN_full     = [];
y_telem_NN_full     = [];
psi_telem_NN_full   = [];

for ii=1:length(test_names)
    testID = strcat('testMPC',test_names{ii});
    telemData = importdata(strcat('./telemetry_data/MPC_lap_telemetries/Dataset_saved/',testID,'.txt'));
    indices_sel = indices_use{ii};
    % Extract and re-sample telemetry data
    resample_telem;
    plot_NN_dataset;
end

save_NN_dataset;
