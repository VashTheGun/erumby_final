% --------------------
%% Save the NN dataset
% --------------------

% Find the indices in which new sequences start or end
time_thresh = 2*Ts_new;
indices_sequences = find(abs(diff(time_telem_NN_full))>time_thresh);

figure('Name','NN data','NumberTitle','off'), clf 
% --- u --- %
hold on
plot(time_telem_NN_full,u_telem_NN_full,'.','HandleVisibility','off')
plot(time_telem_NN_full(indices_sequences),u_telem_NN_full(indices_sequences),'ro','MarkerFaceColor','r','MarkerSize',6)
grid on
title('$u$ [m/s]')
legend('sequence final point')

% Save the NN dataset
save('./NN_dataset/time_data','time_telem_NN_full')
save('./NN_dataset/u_data','u_telem_NN_full')
save('./NN_dataset/delta_data','delta_telem_NN_full')
save('./NN_dataset/Omega_data','Omega_telem_NN_full')
save('./NN_dataset/Ay_data','Ay_telem_NN_full')
save('./NN_dataset/Ay_ss_data','Ay_ss_telem_NN_full')
save('./NN_dataset/Ax_data','Ax_telem_NN_full')
save('./NN_dataset/indices_sequences','indices_sequences')

