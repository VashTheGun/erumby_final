function [dx, y] = single_corner(t, x, u, m, Lf, Lr, Cf, Cr, varargin)
%VEHICLE_M  A biycle vehicle model structure commonly employed in the
%   vehicle dynamics Euler equations
%   d
%   -- b(t) = -(Cf+Cr/mu)*b + ( -1 + (-CfLf + CrLr)/mu^2 )*omega + Cf/mu * delta
%   dt  
%
%   d
%   -- omega(t) = ((-CfLf + CrLr)/Iz)*b - ((CfLf^2 - CrLr^2)/uIz)*omega +
%   dt             CfLf/Iz * omega
%
% 
%       m  = p[1];    Vehicle mass.                    
%       Lf = p[2];    Distance from front axle to COG.
%       Lr = p[3];    Distance from rear axle to COG.  
%       Cf = p[4];    Lateral front tire stiffness.     
%       Cr = p[5];    Lateral rear tire stiffness.             
%       x[1];         beta angle. 
%       x[2];         yaw rate. 
%       y[1];         beta angle. 
%       y[2];         yaw rate. 

    Iz = ((0.5*(Lf+Lr))^2*m);             % vehicle inertia
    % Output equations.
    y = [x(1);                                                ... % beta angle.
         x(2)];                                               ... % Yaw rate.

    % State equations.
  
 
    dx = [ - x(1)*((Cf+Cr)/(m*u(1))) + x(2)*( -1 + (-Cf*Lf + Cr*Lr)/(m*u(1)^2))...
           + u(2)*Cf/(m*u(1));                                                 ...
           x(1)*(-Cf*Lf+Cr*Lr)/Iz - x(2)*(Cf*Lf^2 + Cr*Lr^2)/(Iz*u(1))         ...
           + u(2)*(Cf*Lf)/Iz ];
end