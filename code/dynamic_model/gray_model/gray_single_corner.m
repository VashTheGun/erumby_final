clc;clear;

% Create the gray model
Order         = [2 2 2];                                % Model orders [ny nx nu].
Parameters    = [4.4; 0.17; 0.15; 99; 166];            % Initial parameters.
InitialStates = [0.1; 0];                              % Initial value of initial states.
Ts            = 0;                                   % sampling time.

nlgr = idnlgrey('single_corner', Order, Parameters, InitialStates, Ts, ...
                'Name', 'Bicycle vehicle model', 'TimeUnit', 's');
nlgr.InputName =  {'longitudinal velocity';             ...   % u(1).
                         'steering angle'};                   % u(2).
nlgr.InputUnit =  {'m/s'; 'rad';};

nlgr.OutputName = {'beta';  ...                         % y(1); 
                         'Yaw rate'};                         % y(2).
nlgr.OutputUnit = {'rad'; 'rad/s'};
nlgr = setinit(nlgr, 'Name', {'beta'        ... % x(1).
                       'Yaw rate'});        ... % x(2).

% set delle condizioni iniziali

nlgr = setinit(nlgr, 'Unit', {'rad'; 'rad/s'});
nlgr.InitialStates(1).Minimum = eps(0);   % Longitudinal velocity > 0 for the model to be valid.
nlgr = setpar(nlgr, 'Name', {'Vehicle mass';                         ... % m.
                      'Distance from front axle to COG';      ... % Lf
                      'Distance from rear axle to COG';       ... % Lr.
                      'Lateral front tire stiffness';          ... % Cf.
                      'Lateral rear tire stiffness'});             ... % Cr
nlgr = setpar(nlgr, 'Unit', {'kg'; 'm'; 'm'; 'N/rad'; 'N/rad'});
nlgr = setpar(nlgr, 'Minimum', num2cell(eps(0)*ones(5, 1)));   % All parameters > 0!

% set the estimation of parameters

nlgr.Parameters(1).Fixed = true;
nlgr.Parameters(2).Fixed = true;
nlgr.Parameters(3).Fixed = true;
% nlgr.Parameters(4).Minimum = -Inf;
% nlgr.Parameters(5).Minimum = -Inf;


present(nlgr);

%%

addpath('C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\Raspberry_master');

j = 3;
% filename1 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\telemetry_06_12\';
% filename1 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\telemetry_06_25\';
filename1 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\telemetry_05_30\';
i_s = num2str(j);
test = strcat('test',i_s,'.txt');


filename1 = strcat(filename1,test);

% filename2 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\optitrack_06_12\';
% filename2 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\optitrack_06_25\';
 filename2 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\optitrack_05_30\';
test = strcat('test',i_s,'.csv');
filename2 = strcat(filename2,test);

[telemetry] = telemetry_import(filename1);
[optitrack] = optitrack_import(filename2);

[remap_telemetry,remap_optitrack] = test_data_remap(telemetry,optitrack);

%% stima dal optitrack
for i = 1:length(remap_optitrack.roll)
    R_x(:,:,i) = [ 1 0 0 0;
                   0 cos(remap_optitrack.roll(i)) -sin(remap_optitrack.roll(i)) 0;
                   0 sin(remap_optitrack.roll(i)) cos(remap_optitrack.roll(i)) 0;
                   0 0 0 1];
    R_y(:,:,i) = [ cos(remap_optitrack.pitch(i)) 0 sin(remap_optitrack.pitch(i)) 0;
                   0 1 0 0;
                   -sin(remap_optitrack.pitch(i)) 0 cos(remap_optitrack.pitch(i)) 0;
                   0 0 0 1];
    R_z(:,:,i) = [ cos(remap_optitrack.yaw(i)) -sin(remap_optitrack.yaw(i)) 0 0;
                   sin(remap_optitrack.yaw(i)) cos(remap_optitrack.yaw(i)) 0 0;
                   0 0 1 0;
                   0 0 0 1;];
    T(:,:,i) =   [ 1 0 0 remap_optitrack.x_gps(i);
                   0 1 0 remap_optitrack.y_gps(i);
                   0 0 1 remap_optitrack.z_gps(i);
                   0 0 0 1];
               

    T_a(:,:,i) = T(:,:,i)*R_z(:,:,i)*R_y(:,:,i)*R_x(:,:,i);
    V_relative(:,i) = [remap_optitrack.Vx_gps(i), remap_optitrack.Vy_gps(i), 0, 1]*T(:,:,i)*R_z(:,:,i)*R_y(:,:,i)*R_x(:,:,i);;
    XYZ_relative(:,i) = [remap_optitrack.x_gps(i), remap_optitrack.y_gps(i), remap_optitrack.z_gps(i), 1]*T(:,:,i)*R_z(:,:,i);
    new_pitch(i) = -asin(T_a(3,1,i));
    new_roll(i) = atan(T_a(3,2,i)/(T_a(3,3,i)))*180/pi;
    new_yaw(i) = atan(T_a(2,1,i)/T_a(1,1,i))*180/pi;
end

lpf_beta = designfilt('lowpassiir', ...         % Response type
             'FilterOrder',8, ...      % Filter order
             'HalfPowerFrequency',0.005);


beta_gps = atan2(V_relative(2,:),V_relative(1,:));
beta_gps = filtfilt(lpf_beta,beta_gps);

idx = find( remap_telemetry.gyro_z > 0 & remap_telemetry.traction>0 & remap_telemetry.steering ~= 0 );

y = [beta_gps(idx)', remap_telemetry.gyro_z(idx)'];                      % output creation

delta =   abs((0.17+0.15)*(0.0404 + 0.0170*remap_telemetry.steering));            % steering angle calculation with map conversion [%] ->> [rad]

u = [remap_optitrack.V_gps(idx)', delta(idx)'];

% starting the identification

nlgr1 = nlgr;
nlgr1.Name = 'Bicycle vehicle model ';
z1 = iddata(y, u, 0.001, 'Name', 'Simulated tire stiffness vehicle data');
z1.InputName = nlgr1.InputName;
z1.InputUnit = nlgr1.InputUnit;
z1.OutputName = nlgr1.OutputName;
z1.OutputUnit = nlgr1.OutputUnit;
z1.Tstart = 0;
z1.TimeUnit = 's';

figure()
h_gcf = gcf;
set(h_gcf,'DefaultLegendLocation','southeast');
h_gcf.Position = [100 100 795 634];
for i = 1:z1.Nu
   subplot(z1.Nu, 1, i);
   plot(z1.SamplingInstants, z1.InputData(:,i));
   title(['Input #' num2str(i) ': ' z1.InputName{i}]);
   xlabel('');
   axis tight;
end
xlabel([z1.Domain ' (' z1.TimeUnit ')']);

figure()
h_gcf = gcf;
set(h_gcf,'DefaultLegendLocation','southeast');
h_gcf.Position = [100 100 795 634];
for i = 1:z1.Ny
   subplot(z1.Ny, 1, i);
   plot(z1.SamplingInstants, z1.OutputData(:,i));
   title(['Output #' num2str(i) ': ' z1.OutputName{i}]);
   xlabel('');
   axis tight;
end
xlabel([z1.Domain ' (' z1.TimeUnit ')']);
clf
compare(z1, nlgr1, [], compareOptions('InitialCondition', 'model'));

%%
figure()
for i = 1:z1.Ny
   subplot(z1.Ny, 1, i);
   plot(z1.SamplingInstants, z1.OutputData(:,i));
   title(['Output #' num2str(i) ': ' z1.OutputName{i}]);
   xlabel('');
   axis tight;
end
nlgr1 = nlgreyest(z1, nlgr1);
clf
compare(z1, nlgr1, [], compareOptions('InitialCondition', 'model'));
disp('Param           Estimated');
fprintf('Cf     :    %6.0f\n', nlgr1.Parameters(4).Value);
fprintf('Cr     :    %6.0f\n', nlgr1.Parameters(5).Value);
