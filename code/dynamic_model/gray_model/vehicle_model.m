function [dx, y] = vehicle_model(t, x, u, m, Cd0, Cd1, k, varargin)
    %VEHICLE_M  A biycle vehicle model structure commonly employed in the
    %        Retrieve model parameters. 
    %       double *m, Cd0, Cd1
    %       m               Vehicle mass                    
    %       Cd0 Cd1         drag coefficient
    %       k               proportional value
    %       x[0]            Longitudinal vehicle velocity.
    %       u[0]            Longitudinal force/traction input
    


    % Output equations.
    y = x(1);               % Longitudinal vehicle velocity.                                             

    % State equations.
    dx = (k.*u(1) - Cd0 -Cd1*x(1).^2)/m;
end