function [dx, y] = single_corner_non_linear(t, x, u, m, Lf, Lr, Df, Dr, varargin)
%VEHICLE_M  A biycle vehicle model structure commonly employed in the
%   vehicle dynamics Euler equations
%   d
%   -- b(t) = -(Cf+Cr/mu)*b + ( -1 + (-CfLf + CrLr)/mu^2 )*omega + Cf/mu * delta
%   dt  
%
%   d
%   -- omega(t) = ((-CfLf + CrLr)/Iz)*b - ((CfLf^2 - CrLr^2)/uIz)*omega +
%   dt             CfLf/Iz * omega
%
% 
%       m  = p[1];    Vehicle mass.                    
%       Lf = p[2];    Distance from front axle to COG.
%       Lr = p[3];    Distance from rear axle to COG.  
%       D = p[4];    Lateral front tire stiffness.     
%       K = p[5];    Lateral rear tire stiffness.             
%       x[1];         beta angle. 
%       x[2];         yaw rate. 
%       y[1];         beta angle. 
%       y[2];         yaw rate. 

    Iz = ((0.5*(Lf+Lr))^2*m);             % vehicle inertia
    Fz_f = m*9.81*Lr/(Lf+Lr);
    Fz_r = m*9.81*Lf/(Lf+Lr);
    C = 1.2;
    Kf = 166;
    Kr = 99;
    % Output equations.
    y = [x(1);                                                ... % beta angle.
         x(2)];                                               ... % Yaw rate.

    % State equations.
  
    dx = [ (Df*Fz_f*sin( C*atan(Kf/(C*Df)*(-x(1)-x(2)/u(1)*Lf  + u(2)))) + Dr*Fz_r*sin( C*atan(Kr/(C*Dr)*(-x(1)+x(2)/u(1)*Lr ))))/(m*u(1)) - x(2);...
        (Lf*Df*Fz_f*sin( C*atan(Kf/(C*Df)*(-x(1)-x(2)/u(1)*Lf + u(2)))) - Lr*Dr*Fz_r*sin( C*atan(Kr/(C*Dr)*(-x(1)+x(2)/u(1)*Lr ))))/Iz];

%     dx = [ - x(1)*((Kf+Kr)/(m*u(1))) + x(2)*( -1 + (-Kf*Lf + Kr*Lr)/(m*u(1)^2))...
%        + u(2)*Kf/(m*u(1));                                                 ...
%        (Lf*D*Fz_f*sin( C*atan(Kf/(C*D)*(-x(1)-x(2)/u(1)*Lf + u(2)))) - Lr*D*Fz_r*sin( C*atan(Kr/(C*D)*(-x(1)+x(2)/u(1)*Lr ))))/Iz ];
end