clc;clear;

% Create the gray model
Order         = [3 5 3];                                % Model orders [ny nx nu].
Parameters    = [4.4; 0.17; 0.15; 1e1; 1e1; 0.3];     % Initial parameters.
InitialStates = [1; 0; 0];                            % Initial value of initial states.
Ts            = 0;                                   % sampling time.

nlgr = idnlgrey('double_track', Order, Parameters, InitialStates, Ts, ...
                'Name', 'Bicycle vehicle model', 'TimeUnit', 's');
nlgr.InputName =  {'Slip on front left tire';             ...   % u(1).
                         'Slip on front right tire';              ...   % u(2).
                         'Slip on rear left tire';                ...   % u(3).
                         'Slip on rear right tire';               ...   % u(4).
                         'Steering angle'};                       ...   % u(5).
nlgr.InputUnit =  {'ratio'; 'ratio'; 'ratio'; 'ratio'; 'rad'};

nlgr.OutputName = {'Long. velocity';  ...   % y(1); Longitudinal vehicle velocity
                         'Lat. accel.';   ...     % y(2); Lateral vehicle acceleration
                         'Yaw rate'};                             ...   % y(3).
nlgr.OutputUnit = {'m/s'; 'm/s^2'; 'rad/s'};
nlgr = setinit(nlgr, 'Name', {'Longitudinal vehicle velocity'        ... % x(1).
                       'Lateral vehicle velocity'             ... % x(2).
                       'Yaw rate'});                          ... % x(3).

% set delle condizioni iniziali

nlgr = setinit(nlgr, 'Unit', {'m/s'; 'm/s'; 'rad/s'});
nlgr.InitialStates(1).Minimum = eps(0);   % Longitudinal velocity > 0 for the model to be valid.
nlgr = setpar(nlgr, 'Name', {'Vehicle mass';                         ... % m.
                      'Distance from front axle to COG';      ... % a
                      'Distance from rear axle to COG';       ... % b.
                      'Longitudinal tire stiffness';          ... % Cx.
                      'Lateral tire stiffness';               ... % Cy.
                      'Air resistance coefficient'});         ... % CA.
nlgr = setpar(nlgr, 'Unit', {'kg'; 'm'; 'm'; 'N'; 'N/rad'; '1/m'});
nlgr = setpar(nlgr, 'Minimum', num2cell(eps(0)*ones(6, 1)));   % All parameters > 0!

% set the estimation of parameters

nlgr.Parameters(1).Fixed = true;
nlgr.Parameters(2).Fixed = true;
nlgr.Parameters(3).Fixed = true;
nlgr.Parameters(6).Fixed = true;

present(nlgr);

%% start the model identification

addpath('C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\Raspberry_master');

i = 4;          % number of thest selection
r = 0.05;       % wheel radius
filename1 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\telemetry_steering_id\steering_map\';
i_s = num2str(i);
test = strcat('test',i_s,'.txt');
filename1 = strcat(filename1,test);
telemetry = telemetry_import(filename1);

lpf_tel = designfilt('lowpassiir', ...         % Response type
             'FilterOrder',8, ...              % Filter order
             'HalfPowerFrequency',0.15);       % Cut off freq. 
         

idx = find( telemetry.gyro_z_mpu > 0 & telemetry.traction>0 & telemetry.steering ~= 0 );        % S.State selection 
V = r*((telemetry.enc_fl+telemetry.enc_fr)/2);              % long velocity
V = filtfilt(lpf_tel, V);                                   % filtering
a_y = filtfilt(lpf_tel,telemetry.a_y_mpu)*9.8; 
yaw_rate = filtfilt(lpf_tel, telemetry.gyro_z_mpu); 

y = [V(idx), a_y(idx), yaw_rate(idx)];                      % output creation

slip_fl = zeros(length(V),1);                               % slip calculation
slip_fr = zeros(length(V),1);
slip_rr = medfilt1(abs((r*telemetry.enc_rr - V)./V),12);
slip_rl = medfilt1(abs((r*telemetry.enc_rl - V)./V),12);
delta =   abs((0.17+0.15)*(0.0404 + 0.0170*telemetry.steering));            % steering angle calculation with map conversion [%] ->> [rad]

u = [slip_fl(idx), slip_fr(idx), slip_rl(idx), slip_rr(idx), delta(idx)];   % input creation   

% starting the identification

nlgr1 = nlgr;
nlgr1.Name = 'Bicycle vehicle model with high tire stiffness';
z1 = iddata(y, u, 0.01, 'Name', 'Simulated high tire stiffness vehicle data');
z1.InputName = nlgr1.InputName;
z1.InputUnit = nlgr1.InputUnit;
z1.OutputName = nlgr1.OutputName;
z1.OutputUnit = nlgr1.OutputUnit;
z1.Tstart = 0;
z1.TimeUnit = 's';

figure()
h_gcf = gcf;
set(h_gcf,'DefaultLegendLocation','southeast');
h_gcf.Position = [100 100 795 634];
for i = 1:z1.Nu
   subplot(z1.Nu, 1, i);
   plot(z1.SamplingInstants, z1.InputData(:,i));
   title(['Input #' num2str(i) ': ' z1.InputName{i}]);
   xlabel('');
   axis tight;
end
xlabel([z1.Domain ' (' z1.TimeUnit ')']);

figure()
h_gcf = gcf;
set(h_gcf,'DefaultLegendLocation','southeast');
h_gcf.Position = [100 100 795 634];
for i = 1:z1.Ny
   subplot(z1.Ny, 1, i);
   plot(z1.SamplingInstants, z1.OutputData(:,i));
   title(['Output #' num2str(i) ': ' z1.OutputName{i}]);
   xlabel('');
   axis tight;
end
xlabel([z1.Domain ' (' z1.TimeUnit ')']);
clf
compare(z1, nlgr1, [], compareOptions('InitialCondition', 'model'));

%%
figure()
h_gcf = gcf;
set(h_gcf,'DefaultLegendLocation','southeast');
h_gcf.Position = [100 100 795 634];
for i = 1:z1.Ny
   subplot(z1.Ny, 1, i);
   plot(z1.SamplingInstants, z1.OutputData(:,i));
   title(['Output #' num2str(i) ': ' z1.OutputName{i}]);
   xlabel('');
   axis tight;
end
nlgr1 = nlgreyest(z1, nlgr1);
clf
compare(z1, nlgr1, [], compareOptions('InitialCondition', 'model'));
disp('Param           Estimated');
fprintf('Cx     :    %6.0f\n', nlgr1.Parameters(4).Value);
fprintf('Cy     :    %6.0f\n', nlgr1.Parameters(5).Value);

