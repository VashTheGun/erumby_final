function [dx, y] = double_track(t, x, u, m, Lf, Lr, Cx, Cy, CA, varargin)
%VEHICLE_M  A biycle vehicle model structure commonly employed in the
%   vehicle dynamics Euler equations
%   d
%   -- v_x(t) = v_y(t)*Omega(t) + 1/m*((F_x,FL(t)+F_x,FR(t))*cos(delta(t))
%   dt                             - (F_y,FL(t)+F_y,FR(t))*sin(delta(t))
%                                  + F_x,RL(t)+F_x,RR(t)
%                                  - C_A*v_x(t)^2)
%   d
%   -- v_y(t) = -v_x(t)*Omega(t) + 1/m*((F_x,FL(t)+F_x,FR(t))*sin(delta(t))
%   dt                              + (F_y,FL(t)+F_y,FR(t))*cos(delta(t))
%                                   + F_y,RL(t)+F_y,RR(t))
%   d
%   -- r(t)   = 1/J*(  a*(  (F_x,FL(t)+F_x,FR(t))*sin(delta(t))
%   dt                    + (F_y,FL(t)+F_y,FR(t))*cos(delta(t)))
%                    - b*(F_y,RL(t)+F_y,RR(t)))
% 
%       m  = p[1];    Vehicle mass.                    
%       Lf = p[2];    Distance from front axle to COG.
%       Lr = p[3];    Distance from rear axle to COG.  
%       Cx = p[4];    Longitudinal tire stiffness.     
%       Cy = p[5];    Lateral tire stiffness.          
%       CA = p[6];    Air resistance coefficient.   
%       x[1];         Longitudinal vehicle velocity. 
%       x[2];         Lateral vehicle velocity. 
%       x[3];         Yaw rate.
%       y[1];         Longitudinal vehicle velocity. 
%       y[2];         Lateral vehicle acceleration. 
%       y[3];         Yaw rate.

    % Output equations.
    y = [x(1);                                                ... % Longitudinal vehicle velocity.
         1/m*(  Cx*(u(1)+u(2))*sin(u(5))                      ... % Lateral vehicle acceleration.
                 + 2*Cy*(u(5)-(x(2)+Lf*x(3))/x(1))*cos(u(5))  ...
                 + 2*Cy*(Lr*x(3)-x(2))/x(1));                 ...
         x(3)];                                               ... % Yaw rate.

%     % State equations.
    dx = [x(2)*x(3)+1/m*(  Cx*(u(1)+u(2))*cos(u(5))                        ... % Longitudinal vehicle velocity.
                            - 2*Cy*(u(5)-(x(2)+Lf*x(3))/x(1))*sin(u(5))    ...
                            + Cx*(u(3)+u(4))-CA*x(1)^2);                   ...
          -x(1)*x(3)+1/m*(  Cx*(u(1)+u(2))*sin(u(5))                       ... % Lateral vehicle velocity.
                             + 2*Cy*(u(5)-(x(2)+Lf*x(3))/x(1))*cos(u(5))   ...
                             + 2*Cy*(Lr*x(3)-x(2))/x(1));                  ...
          1/((0.5*(Lf+Lr))^2*m)*(  Lf*(  Cx*(u(1)+u(2))*sin(u(5))          ... % Yaw rate.
                                    + 2*Cy*(u(5)-(x(2)+Lf*x(3))/x(1))*cos(u(5))) ...
                               - 2*Lr*Cy*(Lr*x(3)-x(2))/x(1))];
                           

                           
end