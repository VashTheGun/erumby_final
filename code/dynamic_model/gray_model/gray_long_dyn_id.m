clc;clear;

% Create the gray model

Order         = [1 1 1];          % Model orders [ny nu nx].
Parameters    = [4.4; 1; 1; 1];   % Initial parameter vector (m, Cd0, Cd1).
InitialStates = [0.1];            % Initial values of initial states (longitudinal velocity).
Ts = 0.01;
nlgr    = idnlgrey('vehicle_model', Order, Parameters, InitialStates, Ts,...
                    'Name', 'single track vehicle model', 'TimeUnit', 's');
nlgr.InputName =  {'traction'};          % u(1).

nlgr.InputUnit =  {'percent'};

nlgr.OutputName = {'Long. velocity'};    % y(1); Longitudinal vehicle velocity

nlgr.OutputUnit = {'m/s'};

% set delle condizioni iniziali

nlgr = setinit(nlgr, 'Name', {'Longitudinal vehicle velocity'});  % x(1)
nlgr = setinit(nlgr, 'Unit', {'m/s'});
nlgr.InitialStates(1).Minimum = eps(0);   % Longitudinal velocity > 0 for the model to be valid.
nlgr = setpar(nlgr, 'Name', {'Vehicle mass'; ...            % m.
                      'static drag coefficient';  ...       % Cd0
                      'dynamic drag coefficient'; ...       % Cd1.
                      'proportional constant'});            % k
nlgr = setpar(nlgr, 'Unit', {'kg'; 'adimensional'; 'adimensional';'percent'});
nlgr = setpar(nlgr, 'Minimum', num2cell(eps(0)*ones(4, 1)));   % All parameters > 0!  
nlgr.Parameters(1).Fixed = true;                               % fix the constant parameters

present(nlgr);

%% start the model identification

addpath('C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\Raspberry_master');

i = 1;
r = 0.05;
filename1 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\telemetry_control_identification\';
i_s = num2str(i);
test = strcat('test',i_s,'.txt');
filename1 = strcat(filename1,test);
telemetry = telemetry_import(filename1);

idx = find(telemetry.traction>0 & telemetry.gyro_z_mpu > 0 & telemetry.a_x_mpu <0.01 & telemetry.a_x_mpu > -0.01 );
y = r*((telemetry.enc_fl+telemetry.enc_fr+telemetry.enc_rr+telemetry.enc_rl)/4);
u = telemetry.traction;%./((telemetry.enc_fl+telemetry.enc_fr+telemetry.enc_rr+telemetry.enc_rl)/4);
u = fillmissing(u,'constant',0);
u(~isfinite(u))=0;

nlgr1 = nlgr;
nlgr1.Name = 'Bicycle vehicle model';
z1 = iddata(y(idx), u(idx), 0.01, 'Name', 'test data');
z1.InputName = nlgr1.InputName;
z1.InputUnit = nlgr1.InputUnit;
z1.OutputName = nlgr1.OutputName;
z1.OutputUnit = nlgr1.OutputUnit;
z1.Tstart = 0;
z1.TimeUnit = 's';



figure()
plot(z1.SamplingInstants, z1.InputData(:,1));
title(['Input #' num2str(1) ': ' z1.InputName{1}]);
xlabel('');
axis tight;
xlabel([z1.Domain ' (' z1.TimeUnit ')']);

figure()
plot(z1.SamplingInstants, z1.OutputData(:,1));
title(['Output #' num2str(1) ': ' z1.OutputName{1}]);
xlabel('');
axis tight;
xlabel([z1.Domain ' (' z1.TimeUnit ')']);

clf
compare(z1, nlgr1, [], compareOptions('InitialCondition', 'model'));
%%

figure()
plot(z1.SamplingInstants, z1.OutputData(:,1));
title(['Output #' num2str(1) ': ' z1.OutputName{1}]);
xlabel('');
axis tight;
xlabel([z1.Domain ' (' z1.TimeUnit ')']);
nlgr1 = nlgreyest(z1, nlgr1);
compare(z1, nlgr1, [], compareOptions('InitialCondition', 'model'));

disp('Param           Estimated');
fprintf('Cd0     :    %6.0f\n', nlgr1.Parameters(2).Value);
fprintf('Cd1     :    %6.0f\n', nlgr1.Parameters(3).Value);