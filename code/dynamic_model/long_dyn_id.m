omega=zeros(1,5);
U=zeros(1,5);

clear;clc;

% Identification of the trasfer function U/omega
% U is the accelerator command 

for i = 1:5
    
    filename1 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\telemetry_control_identification\';
    i_s = num2str(i);
    test = strcat('test',i_s,'.txt');
    filename1 = strcat(filename1,test);
    telemetry = telemetry_import(filename1);
    
    lpf_tel = designfilt('lowpassiir', ...         % Response type
                 'FilterOrder',8, ...      % Filter order
                 'HalfPowerFrequency',0.015);

    telemetry.a_x_mpu = filtfilt(lpf_tel,telemetry.a_x_mpu);
    idx = find(telemetry.traction>0 & telemetry.gyro_z_mpu > 0 & telemetry.a_x_mpu <0.01 & telemetry.a_x_mpu > -0.01 );
    omega(i) = mean((telemetry.enc_fl(idx)+telemetry.enc_fr(idx)+telemetry.enc_rr(idx)+telemetry.enc_rl(idx))/4);
    U(i) = mean(telemetry.target(idx));
    
end

ft = fittype(@(a_1,a_2,a_3,omega) a_1+a_2*omega+a_3*omega.^2,...
             'independent',{'omega'},...
             'coefficients',{'a_1','a_2','a_3'});
[curve2,gof2] = fit(omega',U',ft)  %,'options',fo)

figure()
plot(omega,U,'+')
hold on
plot(curve2)
title('TF U/omega')
ylabel('PWM')
xlabel('rad/s')

%%
ft = fittype(@(a_1,a_2,a_3,omega) a_1+a_2*omega+a_3*omega.^2,...
             'independent',{'omega'},...
             'coefficients',{'a_1','a_2','a_3'});
[curve2,gof2] = fit(U',omega',ft)  %,'options',fo)

plot(curve2)


diocane = curve2.a_3*targetINT_filt^2 + curve2.a_2*targetINT_filt + curve2.a_1
