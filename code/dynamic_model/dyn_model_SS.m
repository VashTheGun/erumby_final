function [dX] = dyn_model_SS(X,Vx,delta)
%#codegen

    % Single-Track model for vehicle dynamics
    % variables
    Omega = X(1);
    Vy = X(2);
    % inizialization of the vehicle parameters

    m = 4.2;                % mass
    Kr = 15.93;              % cornering stiffness
    Kf = 10.43;
    Lf = 0.172;              % front wheel base
    Lr = 0.153;              % rear wheel base
    Iz = ((0.5*(Lf+Lr))^2*m);         % moment of inertia z-ax
    Ca = 0.3;
    Fz_f = m*9.81*Lr/(Lf+Lr);
    Fz_r = m*9.81*Lf/(Lf+Lr);
    
    Cf = 1.5;              % C front pachecka
    Df = 0.6403;           % D front pachecka
    alpha_off_f = 0.04407;
    
    Cr = 1.5;
    Dr = 0.6784;
    alpha_off_r = 0.029755;
    
    
    % calculation of lateral force
% 
    Fyr = Fz_r*Dr*sin(Cr*atan(Kr/(Cr*Dr)*((Omega*Lr-Vy)/Vx + alpha_off_r) ));    % alpha_r * Cr
    Fyf = Fz_f*Df*sin(Cf*atan(Kf/(Cf*Df)*(-(Omega*Lf+Vy)/Vx + delta + alpha_off_f) ));  % alpha_f * Cf
%     Fyf = (-(Omega*Lf+Vy)/Vx + delta)*Cf;
%     Fyr = ((Omega*Lr-Vy)/Vx)*Cr; 

    % dynamic equation

    Omega_dot = Fyf*Lf/Iz*cos(delta) - Fyr*Lr/Iz;   % angular acceleration
%     u_dot = (Fxr - Fyf*sin(delta) + Omega*Vy*m - Ca*Vx^2)/m ;     % long accelleration
    v_dot = (Fyr + Fyf*cos(delta) - Omega*Vx*m)/m;     % lateral accelleration

    dX(1) = Omega_dot;
    dX(2) = v_dot;

end
