% fitting test

%%
clear;clc;
addpath(genpath('./../lib'))
j = 4;
filename1 = './../../test_session/18_06_12/telemetry_06_12/';
%filename1 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\telemetry_05_30\';
i_s = num2str(j);
test = strcat('test',i_s,'.txt');


filename1 = strcat(filename1,test);

filename2 = './../../test_session/18_06_12/optitrack_06_12/';
%filename2 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\optitrack_05_30\';
test = strcat('test',i_s,'.csv');
filename2 = strcat(filename2,test);

[telemetry] = telemetry_import_V2(filename1);
[optitrack] = optitrack_import(filename2);

[remap_telemetry,remap_optitrack] = test_data_remap(telemetry,optitrack);

%% stima dal optitrack
for i = 1:length(remap_optitrack.roll)
    R_x(:,:,i) = [ 1 0 0 0;
                   0 cos(remap_optitrack.roll(i)) -sin(remap_optitrack.roll(i)) 0;
                   0 sin(remap_optitrack.roll(i)) cos(remap_optitrack.roll(i)) 0;
                   0 0 0 1];
    R_y(:,:,i) = [ cos(remap_optitrack.pitch(i)) 0 sin(remap_optitrack.pitch(i)) 0;
                   0 1 0 0;
                   -sin(remap_optitrack.pitch(i)) 0 cos(remap_optitrack.pitch(i)) 0;
                   0 0 0 1];
    R_z(:,:,i) = [ cos(remap_optitrack.yaw(i)) -sin(remap_optitrack.yaw(i)) 0 0;
                   sin(remap_optitrack.yaw(i)) cos(remap_optitrack.yaw(i)) 0 0;
                   0 0 1 0;
                   0 0 0 1;];
    T(:,:,i) =   [ 1 0 0 remap_optitrack.x_gps(i);
                   0 1 0 remap_optitrack.y_gps(i);
                   0 0 1 remap_optitrack.z_gps(i);
                   0 0 0 1];
               
%     T_a(:,:,i) = T(:,:,i)*R_x(:,:,i)*R_y(:,:,i)*R_z(:,:,i);
    T_a(:,:,i) = T(:,:,i)*R_z(:,:,i)*R_y(:,:,i)*R_x(:,:,i);
    V_relative(:,i) = [remap_optitrack.Vx_gps(i), remap_optitrack.Vy_gps(i), 0, 1]*T(:,:,i)*R_z(:,:,i);
    XYZ_relative(:,i) = [remap_optitrack.x_gps(i), remap_optitrack.y_gps(i), remap_optitrack.z_gps(i), 1]*T(:,:,i)*R_z(:,:,i);
    new_pitch(i) = -asin(T_a(3,1,i));
    new_roll(i) = atan(T_a(3,2,i)/(T_a(3,3,i)))*180/pi;
    new_yaw(i) = atan(T_a(2,1,i)/T_a(1,1,i))*180/pi;
end

L  = 0.325;      % wheel base


lpf_beta = designfilt('lowpassiir', ...         % Response type
             'FilterOrder',8, ...      % Filter order
             'HalfPowerFrequency',0.005);


beta_gps = atan2(V_relative(2,:),V_relative(1,:));

beta_gps = filtfilt(lpf_beta,beta_gps);
Vx = filtfilt(lpf_beta,V_relative(1,:));
Vy = filtfilt(lpf_beta,V_relative(2,:));
yaw_rate = filtfilt(lpf_beta,remap_optitrack.yaw_rate_gps);

%idx = find(abs(remap_telemetry.gyro_z)>0.0 & abs(remap_telemetry.traction)>0 & abs(remap_telemetry.steering)>0);
idx = find(remap_telemetry.time > 25 & remap_telemetry.time < 180 & remap_telemetry.a_y<6 & remap_telemetry.a_y>1);
%curv = remap_telemetry.gyro_z(idx)./remap_telemetry.V_enc(idx);
curv = abs(remap_telemetry.gyro_z(idx))./remap_optitrack.V_gps(idx);

delta_const = mean(remap_telemetry.steering);

%curv                   = (KUS0_L+KUS1_L*ay)*ay+delta_const/tau_L;
%curv-delta_const/tau_L = (KUS0_L+KUS1_L*ay)*ay;

fo = fitoptions('Method','NonlinearLeastSquares',... 
  'Lower',[-1,-1,-0.01],...
  'Upper',[1,1,0.01],...
  'StartPoint',[0 0, 0.0039 ]);

ft = fittype(@(KUS0_L,KUS1_L,tau_L,ay) (KUS0_L+KUS1_L*ay).*ay+delta_const*tau_L,...
             'independent',{'ay'},...
             'coefficients',{'KUS0_L','KUS1_L','tau_L'});
[curve2,gof2] = fit(abs(remap_telemetry.a_y(idx))',curv',ft) ; %,'options',fo)

figure()
plot(abs(remap_telemetry.a_y(idx)),curv,'+')
hold on
plot(curve2,abs(remap_telemetry.a_y(idx)),curv)

figure()
plot(abs(remap_telemetry.a_y(idx)),abs(beta_gps(idx)),'+')


