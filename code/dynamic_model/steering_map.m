%% Analize sstate test for erumby project
clear;clc;

% if the tests have different total time take the smallest time

ts = 0.01;
max_time = 24/ts; 
r = 0.05;


lpf_tel = designfilt('lowpassiir', ...         % Response type
             'FilterOrder',8, ...      % Filter order
             'HalfPowerFrequency',0.15);
% j is the total number of the test

for j = 1:12

    filename1 = 'C:\Users\PC\Google Drive\Ricerca\E_rumby doc\Core_code\erumby_2018\test_session\telemetry_steering_id\steering_map\';
    i_s = num2str(j);
    test = strcat('test',i_s,'.txt');
      
    filename1 = strcat(filename1,test);

    [telemetry] = telemetry_import(filename1);
    
    telemetry.a_x_mpu = filtfilt(lpf_tel,telemetry.a_x_mpu);
    telemetry.a_y_mpu = filtfilt(lpf_tel,telemetry.a_y_mpu);
    telemetry.a_z_mpu = filtfilt(lpf_tel,telemetry.a_z_mpu);
    telemetry.gyro_z_mpu = filtfilt(lpf_tel, telemetry.gyro_z_mpu);    
    V_enc = r.*(telemetry.enc_fl+telemetry.enc_fr)./2;
    
    idx = find( telemetry.gyro_z_mpu > 0 & telemetry.traction>0 & telemetry.steering ~= 0 );

    V(j) = mean(V_enc(idx));
    yaw_rate(j) = mean(telemetry.gyro_z_mpu(idx));
    curv(j) = mean(telemetry.gyro_z_mpu(idx)./V_enc(idx));
    std_curv(j) = std(telemetry.gyro_z_mpu(idx)./V_enc(idx));
    a_y(j) = mean(telemetry.a_y_mpu(idx));
    std_a_y(j) = std(telemetry.a_y_mpu(idx));
    delta_const(j) = mean(telemetry.steering(idx));
    enc_fl(1,:,j) = telemetry.enc_fl(1:max_time);
    enc_fr(1,:,j) = telemetry.enc_fr(1:max_time);
    enc_rl(1,:,j) = telemetry.enc_rl(1:max_time);
    enc_rr(1,:,j) = telemetry.enc_rr(1:max_time);    
    
end

%%

% fmincon_target = @(mq)(lms_targ(mq, delta_const', yaw_rate'));
% mq = fmincon(fmincon_target, [0,0], [], [], [], [], [-1; 0], [1; 1]);
% k = mq(1);
% tau = mq(2);

% solution of the problem in vectorial form    a+b*delta+c*delta^2 = rho
% [a b c] * |1 a_y1 a_y1^2| = [rho1 rho2...rohi]
%           |1 a_y2 a_y2^2|
%           |.............|
%           |1 a_yi a_yi^2|

delta_const = -delta_const;
x =delta_const';
y = curv';
X = [ones(size(x)), x, x.^2];
X = pinv(X);
Y = y;
theta = X*Y;

xn = linspace(0,90,100)';
Xn = [ones(size(xn)), xn, xn.^2];
Yn = Xn * theta;

figure;
plot(delta_const, curv, '+', xn, Yn);
title('curvature vs delta')
ylabel('rho')
xlabel('% steer')

%%
delta_pwm= fixpt_interp1(  [-100, 100], [5024, 8738], delta_const, sfix(16), 1, sfix(16), 1);
y =delta_pwm';
x = curv';
X = [ones(size(x)), x, x.^2];
X = pinv(X);
Y = y;
theta = X*Y;

ft = fittype(@(a_1, a_2, a_3, delta_pwm) a_1 + a_2*delta_pwm+ a_3*delta_pwm.^2,...
             'independent',{'delta_pwm'},...
             'coefficients',{'a_1','a_2','a_3'});
[curve2,gof2] = fit(delta_pwm',curv',ft)

figure;
plot(curve2,delta_pwm, curv, '+');
title('curvature vs PWM')
ylabel('rho')
xlabel('pwm')

delta_rad = (6881-delta_pwm)/4402;

figure;
plot(delta_rad, curv, '+');
title('curvature vs delta')
ylabel('rho')
xlabel('rad')
