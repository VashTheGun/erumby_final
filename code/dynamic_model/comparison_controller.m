%% path controller sim
clc;clear;

addpath('./G1fitting');
% a double lane change gen
% parameters for simulink:
%%
simTime     = 10;
% initial configuration:
x0         = -5;
y0         = 2;

npts      = 20*5;   % assumed number of points from initial to final point

% for inner lines
px_in=zeros(1,npts); py_in=zeros(1,npts);
% for outter lines
px_out=zeros(1,npts); py_out=zeros(1,npts);
px=zeros(1,npts); py=zeros(1,npts); pd=zeros(1,npts);
for i=1:npts        % smax = rectiliniar path arclength
    if i <= 20
        px(i) = -5 + (2/20)*i;
        py(i) = 2.0;
        pd(i) = 0;
    elseif i <= 20*2
        % compute clothoid parameters
        [k,dk,L] = buildClothoid( -3, 2.0, 0, -1, 3.0, 0 ) ;
        % compute points on clothoid
        XY = pointsOnClothoid( -3, 2.0, 0, k, dk, L, 20 ) ;
        px(i) = XY(1,i-20);
        py(i) = XY(2,i-20);
        pd(i) = atan2(py(i) - py(i-1), px(i) - px(i-1));
    elseif i <= 20*3
        px(i) = -1 + (2/20)*(i-20*2);
        py(i) = 3.0;
        pd(i) = 0;
    elseif i <= 20*4
        % compute clothoid parameters
        [k,dk,L] = buildClothoid( 1.0, 3.0, 0, 3, 2.0, 0 ) ;
        % compute points on clothoid
        XY = pointsOnClothoid( 1.0, 3.0, 0, k, dk, L, 20 ) ;
        px(i) = XY(1,i-20*3);
        py(i) = XY(2,i-20*3);
        pd(i) = atan2(py(i) - py(i-1), px(i) - px(i-1));
    else
        px(i) = 3 + (2/20)*(i-20*4);
        py(i) = 2.0;
        pd(i) = 0;
    end
end

% %%
% % a racing course
% % parameters for simulink:
% simTime     = 15;
% % initial configuration:
% x0         = -1.2;
% y0         = 1;
% theta_0     = 0;
% 
% 
% points_per_road = 20;
% npts      = 2*points_per_road + points_per_road*2;   % assumed number of points from initial to final point
% px=zeros(1,npts); py=zeros(1,npts); pd=zeros(1,npts);
% 
% % for inner lines
% px_in=zeros(1,npts); py_in=zeros(1,npts);
% % for outter lines
% px_out=zeros(1,npts); py_out=zeros(1,npts);
% 
% for i=1:1:npts
%     if i <= points_per_road
%         px(i) = -1.2 + (1.9/points_per_road)*i;
%         py(i) = 1.0;
%         pd(i) = 0;
% 
%         px_in(i) = 0.1*i;
%         py_in(i) = 1;
%         px_out(i) = 0.1*i;
%         py_out(i) = -1;
%     elseif i <= points_per_road*2
%         % compute clothoid parameters
%         [k,dk,L] = buildClothoid( 1.9, 1.0, 0, 1.9, 3.8, pi ) ;
%         % compute points on clothoid
%         XY = pointsOnClothoid( 1.9, 1.0, 0, k, dk, L, points_per_road ) ;
%         px(i) = XY(1,i-points_per_road);
%         py(i) = XY(2,i-points_per_road);
%         pd(i) = atan2(py(i) - py(i-1), px(i) - px(i-1));
% 
%         px_in(i) = 10 + 1*cos(-pi/2 + (pi/60)*(i-100));
%         py_in(i) = 2 + 1*sin(-pi/2 + (pi/60)*(i-100));
%         px_out(i) = 10 + 3*cos(-pi/2 + (pi/60)*(i-100));
%         py_out(i) = 2 + 3*sin(-pi/2 + (pi/60)*(i-100));
%     elseif i <= points_per_road*3
%         px(i) = 1.9 - (3.1/points_per_road)*(i - points_per_road*2 -1);
%         py(i) = 3.8;
%         pd(i) = pi;
% 
%         px_in(i) = 10 - 0.1*(i - 160);
%         py_in(i) = 3;
%         px_out(i) = 10 - 0.1*(i - 160);
%         py_out(i) = 5;
%     else
%         % compute clothoid parameters
%         [k,dk,L] = buildClothoid( -1.2, 3.8, pi, -1.2, 1.0, 2*pi ) ;
%         % compute points on clothoid
%         XY = pointsOnClothoid( -1.2, 3.8, pi, k, dk, L, points_per_road ) ;
%         px(i) = XY(1,i-points_per_road*3);
%         py(i) = XY(2,i-points_per_road*3);
%         pd(i) = atan2(py(i) - py(i-1), px(i) - px(i-1));
% 
%         px_in(i) = -1*sin((pi/60)*(i-260));
%         py_in(i) = 2 + 1*cos((pi/60)*(i-260));
%         px_out(i) = -3*sin((pi/60)*(i-260));
%         py_out(i) = 2 + 3*cos((pi/60)*(i-260));
%     end
% 
% end

%% sim and plot

sim('single_track_pursuit');

figure();
cla, hold on, grid on
plot(px,py,'b');
plot( x_sim, y_sim, 'black') ; % plot trace of the center
legend('desired','actual')
xlabel('X [m]')
ylabel('Y [m]')

sim('single_track_preview');

figure();
cla, hold on, grid on
plot(px,py,'b');
plot( x_sim, y_sim, 'black') ; % plot trace of the center
legend('desired','actual')
xlabel('X [m]')
ylabel('Y [m]')

%%
% sistema di riferimento
axes_object = axes();

% punti del poligono che rappresenta il veicolo
x = [0 0.5 0.5 0];
y = [-0.15 -0.15 0.15 0.15];

% creazione della patch
plot(px,py,'b');
hold on
vehiclePatch = patch(x,y,'red');
transform_object = hgtransform('Parent',axes_object);
set(vehiclePatch, 'Parent', transform_object)
xlim([-5 5])
ylim([0 5])


% loop sui dati x, y, theta   [m], [m], [rad] in coordinate assolute
for i = 1:length(x_sim)

   vehiclePlace(transform_object,x_sim(i), y_sim(i), theta_sim(i))
   pause(0.001)
end