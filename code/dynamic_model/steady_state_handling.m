%% Analize sstate test for erumby project for handling diagram
clear;clc;

% if the tests have different total time take the smallest time

ts = 0.001;
max_time = 24/ts; 


lpf_beta = designfilt('lowpassiir', ...         % Response type
             'FilterOrder',8, ...      % Filter order
             'HalfPowerFrequency',0.005);
% j is the total number of the test

for j = 1:7

    filename1 = './../../test_session/18_06_12/telemetry_06_12/';
    i_s = num2str(j);
    test = strcat('test',i_s,'.txt');
    
    
    filename1 = strcat(filename1,test);

    filename2 = './../../test_session/18_06_12/optitrack_06_12/';
    test = strcat('test',i_s,'.csv');
    filename2 = strcat(filename2,test);

    [telemetry] = telemetry_import_V2(filename1);
    [optitrack] = optitrack_import(filename2);

    [remap_telemetry,remap_optitrack] = test_data_remap(telemetry,optitrack);
    
% trasformation matrix absolute2relative

    for i = 1:length(remap_optitrack.roll)
        R_x(:,:,i) = [ 1 0 0 0;
                       0 cos(remap_optitrack.roll(i)) -sin(remap_optitrack.roll(i)) 0;
                       0 sin(remap_optitrack.roll(i)) cos(remap_optitrack.roll(i)) 0;
                       0 0 0 1];
        R_y(:,:,i) = [ cos(remap_optitrack.pitch(i)) 0 sin(remap_optitrack.pitch(i)) 0;
                       0 1 0 0;
                       -sin(remap_optitrack.pitch(i)) 0 cos(remap_optitrack.pitch(i)) 0;
                       0 0 0 1];
        R_z(:,:,i) = [ cos(remap_optitrack.yaw(i)) -sin(remap_optitrack.yaw(i)) 0 0;
                       sin(remap_optitrack.yaw(i)) cos(remap_optitrack.yaw(i)) 0 0;
                       0 0 1 0;
                       0 0 0 1;];
        T(:,:,i) =   [ 1 0 0 remap_optitrack.x_gps(i);
                       0 1 0 remap_optitrack.y_gps(i);
                       0 0 1 remap_optitrack.z_gps(i);
                       0 0 0 1];

        T_a(:,:,i) = T(:,:,i)*R_z(:,:,i)*R_y(:,:,i)*R_x(:,:,i);
        V_a2r(:,i) = [remap_optitrack.Vx_gps(i), remap_optitrack.Vy_gps(i), 0, 1]*T(:,:,i)*R_z(:,:,i);
    end
   
   idx = [14000:17000];
    tmp = atan2(remap_optitrack.Vy_rel,remap_optitrack.Vx_rel);
    tmp = filtfilt(lpf_beta,tmp);
    beta(j) = mean(tmp(idx));
    std_beta(j) = std(tmp(idx));
    V(j) = mean(remap_optitrack.V_gps(idx));
    yaw_rate(j) = mean(remap_telemetry.gyro_z(idx));   
    std_curv(j) = std(remap_telemetry.gyro_z(idx)./remap_optitrack.V_gps(idx));
    a_y(j) = mean(remap_telemetry.a_y(idx));
    std_a_y(j) = std(remap_telemetry.a_y(idx));
    X_gps(1,:,j) = remap_optitrack.x_gps(1:max_time);
    Y_gps(1,:,j) = remap_optitrack.y_gps(1:max_time);
    enc_fl(1,:,j) = remap_telemetry.enc_fl(1:max_time);
    enc_fr(1,:,j) = remap_telemetry.enc_fr(1:max_time);
    enc_rl(1,:,j) = remap_telemetry.enc_rl(1:max_time);
    enc_rr(1,:,j) = remap_telemetry.enc_rr(1:max_time);    
    slip_left(j) = mean(medfilt1(remap_telemetry.enc_rl(idx)./remap_telemetry.enc_fl(idx),100));
    slip_right(j) = mean(medfilt1(remap_telemetry.enc_rr(idx)./remap_telemetry.enc_fr(idx),100));    
end

curv = yaw_rate./V;

delta_const = mean(remap_telemetry.steering);
fo = fitoptions('Method','NonlinearLeastSquares',... 
  'Lower',[-1,-1,-1],...
  'Upper',[1,1,1],...
  'StartPoint',[0 0, 0.0039 ]);

ft = fittype(@(KUS0_L,KUS1_L,tau_L,ay) (KUS0_L+KUS1_L*ay).*ay+delta_const*tau_L,...
             'independent',{'ay'},...
             'coefficients',{'KUS0_L','KUS1_L','tau_L'});
[curve2,gof2] = fit(a_y(1:4)',curv(1:4)',ft ,fo);

figure()
plot(a_y,curv,'+')
hold on
plot(curve2,a_y,curv)

% solution of the problem in vectorial form    a+b*a_y+c*a_y^2 = rho
% [a b c] * |1 a_y1 a_y1^2| = [rho1 rho2...rohi]
%           |1 a_y2 a_y2^2|
%           |.............|
%           |1 a_yi a_yi^2|
x =a_y(1:3)';
y = curv(1:3)';
X = [ones(size(x)), x, x.^2];
X = pinv(X);
Y = y;
theta = X*Y;

xn = linspace(0,6,100)';
Xn = [ones(size(xn)), xn, xn.^2];
Yn = Xn * theta;

tmp = [0:0.5:6];
rect = theta(1)+theta(2)*tmp;

figure;
plot(a_y, curv, '+', xn, Yn, tmp, rect,'--',tmp,theta(1),tmp, ones(size(tmp))*theta(1),'--');
title('curvature vs ay')
ylabel('rho')
xlabel('m/s^2')


figure()
plot(a_y, curv, '+')
title('curvature vs ay')
ylabel('\rho')
xlabel('a_y')
hold on
for i = 1:7
    a=std_a_y(i); % horizontal radius
    b=std_curv(i); % vertical radius
    x0=a_y(i); % x0,y0 ellipse centre coordinates
    y0=curv(i);
    t=-pi:0.01:pi;
    x=x0+a*cos(t);
    y=y0+b*sin(t);
    plot(x,y)  
end


figure()
plot(a_y,beta,'+')
title('beta vs ay')
ylabel('\beta')
xlabel('a_y')
hold on
for i = 1:7
    a=std_a_y(i); % horizontal radius
    b=std_beta(i); % vertical radius
    x0=a_y(i); % x0,y0 ellipse centre coordinates
    y0=beta(i);
    t=-pi:0.01:pi;
    x=x0+a*cos(t);
    y=y0+b*sin(t);
    plot(x,y)  
end


figure()
hold on
for i = 1:7
    plot(X_gps(:,:,i)-X_gps(1,1,i),Y_gps(:,:,i)-Y_gps(1,1,i))
    title('trajectory for different ay')
    xlabel('m')
    ylabel('m')
end
legend(num2str(a_y(1)),num2str(a_y(2)),num2str(a_y(3)),num2str(a_y(4)),num2str(a_y(5)),...
        num2str(a_y(6)),num2str(a_y(7)))
axis equal

figure()
hold on
for i = 1:7
    plot(enc_rl(:,:,i))
end

figure()
plot(a_y, slip_left,'+', a_y, slip_right,'+')
title('slip vs a_y')
xlabel('m/s^2')
ylabel('slip')
legend('left', 'right')
