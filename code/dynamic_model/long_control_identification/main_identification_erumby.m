clear all; 
% close all;
clc;

% load the identification data of erumby
 load('erumby_id.mat');
% extract the time, the input and the output
Ts = data_r.Ts;
time = Ts*[0:1:length(data_r.u)-1]';
y = data_r.y;
u = data_r.u;


% load('erumby_id_1.mat');
% Ts = 0.01;% data_r.Ts;
% time = Ts*[0:1:length(data.u)-1]';
% y = data.y;
% u = data.u;


% search for the rising time
ris_points = find(diff(u)>0);
% select a waiting time and a time window
T_WAIT = 2; 
T_AVE = 3;
N_WAIT = round(T_WAIT/Ts);
N_AVE = round(T_AVE/Ts);

% define a vector of steady state points
y_ss = zeros(length(ris_points)+1,1);
u_ss = zeros(length(ris_points)+1,1);

% for the nonlinearity to pass through the origin
y_ss(1) = 0; 
u_ss(1) = 0;

% select average points
for i=1:length(ris_points)
    y_ss(i+1,1) = mean(y(ris_points(i)+N_WAIT:ris_points(i)+N_WAIT+N_AVE,1));
    u_ss(i+1,1) = mean(u(ris_points(i)+N_WAIT:ris_points(i)+N_WAIT+N_AVE,1));
end 

% plot the data
figure(); 
subplot(2,1,1);
plot(time,u);
hold on; 
plot(time(ris_points)+T_WAIT+T_AVE/2,u_ss(2:end),'o');
xlabel('time')
ylabel('PWM [%] ')
title('input vs time')
subplot(2,1,2);
plot(time,y);
hold on; 
plot(time(ris_points)+T_WAIT+T_AVE/2,y_ss(2:end),'o')
xlabel('time')
ylabel('\omega [rad/s] ')
title('output vs time')

% define a personalized fitting function
myfittype = fittype('c1*x+c2*x^2',...
                    'dependent',{'y'},'independent',{'x'},...
                    'coefficients',{'c1','c2'});
myfit = fit(y_ss,u_ss,myfittype);

% plot the graph u_ss-y_ss
figure();
grid on;
plot(u_ss,y_ss,'-o');

% plot the inverse function and the fitting
figure;
grid on;
plot(myfit,y_ss,u_ss);
xlabel('\omega [rad/s]')
ylabel('PWM [%]')
title('input-output relationship')

% retrieve the internal state of the system
x = myfit(y);


% prepare the data for system identification 
data_id = iddata(x,u,Ts);

% plot the data to manually identify the delay
figure(); 
grid on; 
plot(time,u);
hold on;
plot(time,x);

% set the input delay
IO_delay = 0.080;

% identify the system
sys = tfest(data_id,1,0,'Ts',0,'InputDelay',IO_delay);
compare(data_id,sys);

