clear all; 
close all; 
clc; 

% set the plant parameter
c1 = 0.00118; 
c2 = 1.532e-05; 
a = 3.17;
tau = 0.08;
Kp = 0.02;
Ki = 0.03;

DUTY_ESC_IDLE = 7010;
DUTY_ESC_MAX = 8412;
DUTY_ESC_MIN = 5608;


% c1 = 0.001176;
% c2 = 1.558e-05; 
% a = 3.235;