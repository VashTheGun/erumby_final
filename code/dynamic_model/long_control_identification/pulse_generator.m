%% script per generare un treno di impulsi per l esc dell'erumby
clc;clear

x = [0 5 5 10 10 12 12 17 17 19 19 24 24 26 26 31 31 33 33 39 39]; 
y = [7010 7010 7060 7060 7010 7010 7110 7110 7010 7010 7160 7160 7010 7010 7210 7210 7010 7010 7260 7260 7010];

DUTY_ESC_IDLE = 7010;
DUTY_ESC_STEP = linspace(DUTY_ESC_IDLE, 7300, 15);
x_out = [];
y_out = [];
for i = 1:15
    y = [DUTY_ESC_IDLE DUTY_ESC_IDLE DUTY_ESC_STEP(i) DUTY_ESC_STEP(i) DUTY_ESC_IDLE];
    x = [0 2 2 10 10] + (i-1)*10;
    x_out = ([x_out x]);
    y_out = ([y_out y]);
end

plot(x_out, y_out)
