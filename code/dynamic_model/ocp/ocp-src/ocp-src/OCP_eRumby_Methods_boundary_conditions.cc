/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_Methods.cc                                          |
 |                                                                       |
 |  version: 1.0   date 28/11/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby.hh"
#include "OCP_eRumby_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_rightWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_R")
#define ALIAS_rightWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_R")
#define ALIAS_rightWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_R")
#define ALIAS_leftWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_L")
#define ALIAS_leftWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_L")
#define ALIAS_leftWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_L")
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_FxLimit_DD(__t1) FxLimit.DD( __t1)
#define ALIAS_FxLimit_D(__t1) FxLimit.D( __t1)
#define ALIAS_deltaLimit_DD(__t1) deltaLimit.DD( __t1)
#define ALIAS_deltaLimit_D(__t1) deltaLimit.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_speed_limit_DD(__t1) speed_limit.DD( __t1)
#define ALIAS_speed_limit_D(__t1) speed_limit.D( __t1)
#define ALIAS_Fx__r__OControl_D_3(__t1, __t2, __t3) Fx__r__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2(__t1, __t2, __t3) Fx__r__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1(__t1, __t2, __t3) Fx__r__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_3_3(__t1, __t2, __t3) Fx__r__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_3(__t1, __t2, __t3) Fx__r__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_2(__t1, __t2, __t3) Fx__r__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_3(__t1, __t2, __t3) Fx__r__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_2(__t1, __t2, __t3) Fx__r__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_1(__t1, __t2, __t3) Fx__r__OControl.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3(__t1, __t2, __t3) delta__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2(__t1, __t2, __t3) delta__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1(__t1, __t2, __t3) delta__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3_3(__t1, __t2, __t3) delta__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_3(__t1, __t2, __t3) delta__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_2(__t1, __t2, __t3) delta__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_3(__t1, __t2, __t3) delta__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_2(__t1, __t2, __t3) delta__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_1(__t1, __t2, __t3) delta__OControl.D_1_1( __t1, __t2, __t3)


namespace OCP_eRumbyDefine {

  /*\
   |   ___                   _
   |  | _ ) ___ _  _ _ _  __| |__ _ _ _ _  _
   |  | _ \/ _ \ || | ' \/ _` / _` | '_| || |
   |  |___/\___/\_,_|_||_\__,_\__,_|_|  \_, |
   |    ___             _ _ _   _       |__/
   |   / __|___ _ _  __| (_) |_(_)___ _ _  ___
   |  | (__/ _ \ ' \/ _` | |  _| / _ \ ' \(_-<
   |   \___\___/_||_\__,_|_|\__|_\___/_||_/__/
  \*/

  integer
  OCP_eRumby::boundaryConditions_numEqns() const
  { return 0; }

  void
  OCP_eRumby::boundaryConditions_eval(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);

    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"boundaryConditions_eval",0);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  OCP_eRumby::DboundaryConditionsDx_numRows() const
  { return 0; }

  integer
  OCP_eRumby::DboundaryConditionsDx_numCols() const
  { return 14; }

  integer
  OCP_eRumby::DboundaryConditionsDx_nnz() const
  { return 0; }

  void
  OCP_eRumby::DboundaryConditionsDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {

  }

  void
  OCP_eRumby::DboundaryConditionsDx_sparse(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  OCP_eRumby::DboundaryConditionsDp_numRows() const
  { return 0; }

  integer
  OCP_eRumby::DboundaryConditionsDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DboundaryConditionsDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DboundaryConditionsDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {

  }

  void
  OCP_eRumby::DboundaryConditionsDp_sparse(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY

  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::adjointBC_numEqns() const
  { return 14; }

  void
  OCP_eRumby::adjointBC_eval(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    L_const_pointer_type LL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    L_const_pointer_type LR__,
    P_const_pointer_type P__,
    OMEGA_full_const_pointer_type OMEGA__,
    real_type                     result__[]
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    real_type t1   = ModelPars[17];
    result__[ 0   ] = 2 * (XL__[0] - ModelPars[26]) * ModelPars[18] * t1 + LL__[0];
    result__[ 1   ] = 2 * (XL__[1] - ModelPars[34]) * ModelPars[44] * t1 - LL__[1];
    result__[ 2   ] = 2 * (XL__[2] - ModelPars[29]) * ModelPars[19] * t1 + LL__[3];
    result__[ 3   ] = 2 * (XL__[3] - ModelPars[31]) * ModelPars[20] * t1 + LL__[4];
    result__[ 4   ] = 2 * (XL__[4] - ModelPars[36]) * ModelPars[41] * t1 + LL__[2];
    result__[ 5   ] = 2 * (XL__[5] - ModelPars[46]) * ModelPars[42] * t1 + LL__[5];
    result__[ 6   ] = 2 * (XL__[6] - ModelPars[22]) * ModelPars[43] * t1 + LL__[6];
    real_type t58  = ModelPars[13];
    result__[ 7   ] = 2 * (XR__[0] - ModelPars[25]) * ModelPars[14] * t58 - LR__[0];
    result__[ 8   ] = 2 * (XR__[1] - ModelPars[33]) * ModelPars[40] * t58 + LR__[1];
    result__[ 9   ] = 2 * (XR__[2] - ModelPars[28]) * ModelPars[15] * t58 - LR__[3];
    result__[ 10  ] = 2 * (XR__[3] - ModelPars[30]) * ModelPars[16] * t58 - LR__[4];
    result__[ 11  ] = 2 * (XR__[4] - ModelPars[35]) * ModelPars[38] * t58 - LR__[2];
    result__[ 12  ] = 2 * (XR__[5] - ModelPars[45]) * ModelPars[39] * t58 - LR__[5];
    result__[ 13  ] = -LR__[6];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"adjointBC_eval",14);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  OCP_eRumby::DadjointBCDx_numRows() const
  { return 14; }

  integer
  OCP_eRumby::DadjointBCDx_numCols() const
  { return 14; }

  integer
  OCP_eRumby::DadjointBCDx_nnz() const
  { return 13; }

  void
  OCP_eRumby::DadjointBCDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 1   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 2   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 3   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 4   ; jIndex[ 4  ] = 4   ;
    iIndex[ 5  ] = 5   ; jIndex[ 5  ] = 5   ;
    iIndex[ 6  ] = 6   ; jIndex[ 6  ] = 6   ;
    iIndex[ 7  ] = 7   ; jIndex[ 7  ] = 7   ;
    iIndex[ 8  ] = 8   ; jIndex[ 8  ] = 8   ;
    iIndex[ 9  ] = 9   ; jIndex[ 9  ] = 9   ;
    iIndex[ 10 ] = 10  ; jIndex[ 10 ] = 10  ;
    iIndex[ 11 ] = 11  ; jIndex[ 11 ] = 11  ;
    iIndex[ 12 ] = 12  ; jIndex[ 12 ] = 12  ;
  }

  void
  OCP_eRumby::DadjointBCDx_sparse(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    L_const_pointer_type LL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    L_const_pointer_type LR__,
    P_const_pointer_type P__,
    OMEGA_full_const_pointer_type OMEGA__,
    real_type                     result__[]
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    real_type t1   = ModelPars[17];
    result__[ 0   ] = 2 * ModelPars[18] * t1;
    result__[ 1   ] = 2 * ModelPars[44] * t1;
    result__[ 2   ] = 2 * ModelPars[19] * t1;
    result__[ 3   ] = 2 * ModelPars[20] * t1;
    result__[ 4   ] = 2 * ModelPars[41] * t1;
    result__[ 5   ] = 2 * ModelPars[42] * t1;
    result__[ 6   ] = 2 * ModelPars[43] * t1;
    real_type t16  = ModelPars[13];
    result__[ 7   ] = 2 * ModelPars[14] * t16;
    result__[ 8   ] = 2 * ModelPars[40] * t16;
    result__[ 9   ] = 2 * ModelPars[15] * t16;
    result__[ 10  ] = 2 * ModelPars[16] * t16;
    result__[ 11  ] = 2 * ModelPars[38] * t16;
    result__[ 12  ] = 2 * ModelPars[39] * t16;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DadjointBCDxp_sparse",13);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  OCP_eRumby::DadjointBCDp_numRows() const
  { return 14; }

  integer
  OCP_eRumby::DadjointBCDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DadjointBCDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DadjointBCDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {

  }

  void
  OCP_eRumby::DadjointBCDp_sparse(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    L_const_pointer_type LL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    L_const_pointer_type LR__,
    P_const_pointer_type P__,
    OMEGA_full_const_pointer_type OMEGA__,
    real_type                     result__[]
  ) const {
    // EMPTY!
  }

}

// EOF: OCP_eRumby_Methods.cc
