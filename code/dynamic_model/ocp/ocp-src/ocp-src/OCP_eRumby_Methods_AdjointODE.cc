/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_Methods.cc                                          |
 |                                                                       |
 |  version: 1.0   date 28/11/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby.hh"
#include "OCP_eRumby_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_rightWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_R")
#define ALIAS_rightWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_R")
#define ALIAS_rightWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_R")
#define ALIAS_leftWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_L")
#define ALIAS_leftWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_L")
#define ALIAS_leftWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_L")
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_FxLimit_DD(__t1) FxLimit.DD( __t1)
#define ALIAS_FxLimit_D(__t1) FxLimit.D( __t1)
#define ALIAS_deltaLimit_DD(__t1) deltaLimit.DD( __t1)
#define ALIAS_deltaLimit_D(__t1) deltaLimit.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_speed_limit_DD(__t1) speed_limit.DD( __t1)
#define ALIAS_speed_limit_D(__t1) speed_limit.D( __t1)
#define ALIAS_Fx__r__OControl_D_3(__t1, __t2, __t3) Fx__r__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2(__t1, __t2, __t3) Fx__r__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1(__t1, __t2, __t3) Fx__r__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_3_3(__t1, __t2, __t3) Fx__r__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_3(__t1, __t2, __t3) Fx__r__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_2(__t1, __t2, __t3) Fx__r__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_3(__t1, __t2, __t3) Fx__r__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_2(__t1, __t2, __t3) Fx__r__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_1(__t1, __t2, __t3) Fx__r__OControl.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3(__t1, __t2, __t3) delta__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2(__t1, __t2, __t3) delta__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1(__t1, __t2, __t3) delta__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3_3(__t1, __t2, __t3) delta__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_3(__t1, __t2, __t3) delta__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_2(__t1, __t2, __t3) delta__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_3(__t1, __t2, __t3) delta__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_2(__t1, __t2, __t3) delta__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_1(__t1, __t2, __t3) delta__OControl.D_1_1( __t1, __t2, __t3)


namespace OCP_eRumbyDefine {

  /*\
   |  _   _
   | | | | |_  __
   | | |_| \ \/ /
   | |  _  |>  <
   | |_| |_/_/\_\
   |
  \*/

  integer
  OCP_eRumby::Hx_numEqns() const
  { return 7; }

  void
  OCP_eRumby::Hx_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = cos(t1);
    real_type t3   = X__[2];
    real_type t5   = sin(t1);
    real_type t6   = X__[3];
    real_type t8   = t3 * t2 - t6 * t5;
    real_type t9   = 1.0 / t8;
    real_type t10  = Q__[1];
    real_type t11  = t10 * t9;
    real_type t12  = speed_limit(t3);
    real_type t14  = X__[0];
    real_type t15  = ModelPars[49];
    real_type t16  = Q__[0];
    real_type t17  = ALIAS_rightWidth(t16);
    real_type t18  = t14 - t15 + t17;
    real_type t19  = roadRightLateralBoundaries(t18);
    real_type t22  = t14 * t10 - 1;
    real_type t23  = t22 * t9;
    real_type t24  = ALIAS_roadRightLateralBoundaries_D(t18);
    real_type t26  = ALIAS_leftWidth(t16);
    real_type t27  = t26 - t14 - t15;
    real_type t28  = roadLeftLateralBoundaries(t27);
    real_type t30  = ALIAS_roadLeftLateralBoundaries_D(t27);
    real_type t32  = X__[5];
    real_type t33  = deltaLimit(t32);
    real_type t35  = X__[6];
    real_type t36  = FxLimit(t35);
    real_type t38  = ModelPars[21];
    real_type t39  = 1 - t38;
    real_type t42  = ModelPars[27] * ModelPars[27];
    real_type t43  = 1.0 / t42;
    real_type t50  = t14 * t14;
    real_type t53  = t43 * t50 * t39 + t38 * ModelPars[32];
    real_type t56  = L__[0];
    real_type t59  = -t6 * t2 - t3 * t5;
    real_type t60  = t59 * t56;
    real_type t62  = L__[1];
    real_type t63  = t10 * t10;
    real_type t65  = 1.0 / t22;
    real_type t69  = X__[4];
    real_type t71  = (t65 * t8 * t10 + t69) * t62;
    real_type t73  = L__[2];
    real_type t74  = ModelPars[11];
    real_type t76  = t74 * t69 + t6;
    real_type t77  = 1.0 / t3;
    real_type t79  = -t77 * t76 + t32;
    real_type t80  = MF_Fy_f(t79);
    real_type t81  = t74 * t80;
    real_type t83  = 1.0 / ModelPars[7];
    real_type t84  = cos(t32);
    real_type t85  = t84 * t83;
    real_type t87  = ModelPars[12];
    real_type t89  = t87 * t69 - t6;
    real_type t90  = t77 * t89;
    real_type t91  = MF_Fy_r(t90);
    real_type t95  = (t83 * t87 * t91 - t85 * t81) * t73;
    real_type t97  = L__[3];
    real_type t98  = sin(t32);
    real_type t99  = t98 * t80;
    real_type t101 = ModelPars[24];
    real_type t103 = ModelPars[0];
    real_type t104 = t3 * t3;
    real_type t107 = (t101 * t6 * t69 - t104 * t103 + t35 - t99) * t97;
    real_type t108 = 1.0 / t101;
    real_type t109 = t9 * t108;
    real_type t110 = t10 * t109;
    real_type t112 = L__[4];
    real_type t113 = t84 * t80;
    real_type t117 = (-t101 * t3 * t69 + t113 + t91) * t112;
    real_type t121 = L__[5] * U__[1];
    real_type t125 = L__[6] * U__[0];
    result__[ 0   ] = -2 * t22 * t9 * t43 * t14 * t39 - t10 * t9 * t53 - t65 * t63 * t62 - t110 * t107 - t12 * t11 - t11 * t121 - t11 * t125 - t19 * t11 - t28 * t11 - t33 * t11 - t36 * t11 + t11 * t60 + t11 * t71 + t11 * t95 - t110 * t117 - t24 * t23 + t30 * t23;
    real_type t127 = t8 * t8;
    real_type t128 = 1.0 / t127;
    real_type t129 = t22 * t128;
    real_type t140 = t128 * t53;
    real_type t146 = t59 * t59;
    real_type t149 = t10 * t62;
    real_type t152 = t59 * t129;
    real_type t155 = t108 * t107;
    real_type t157 = t108 * t117;
    result__[ 1   ] = t59 * t12 * t129 - t129 * t146 * t56 + t59 * t19 * t129 + t59 * t28 * t129 + t59 * t33 * t129 + t59 * t36 * t129 + t59 * t22 * t140 + t9 * t59 * t149 - t23 * t8 * t56 + t152 * t121 + t152 * t125 + t152 * t155 + t152 * t157 - t152 * t71 - t152 * t95;
    real_type t163 = ALIAS_speed_limit_D(t3);
    real_type t177 = t2 * t129;
    real_type t182 = MF_Fy_f_D(t79);
    real_type t183 = t76 * t182;
    real_type t184 = 1.0 / t104;
    real_type t187 = t84 * t83 * t74;
    real_type t189 = MF_Fy_r_D(t90);
    real_type t190 = t89 * t189;
    real_type t204 = t22 * t109;
    real_type t210 = t101 * t69;
    result__[ 2   ] = t2 * t12 * t129 - t163 * t23 + t2 * t19 * t129 + t2 * t28 * t129 + t2 * t33 * t129 + t2 * t36 * t129 + t2 * t22 * t140 - t23 * t5 * t56 - t177 * t60 + t9 * t2 * t149 - t177 * t71 + t23 * (-t83 * t87 * t184 * t190 - t187 * t184 * t183) * t73 - t177 * t95 - t204 * (-t98 * t184 * t183 - 2 * t3 * t103) * t97 + t177 * t155 - t204 * (t84 * t184 * t183 - t184 * t190 - t210) * t112 + t177 * t157 + t177 * t121 + t177 * t125;
    real_type t231 = t5 * t129;
    real_type t236 = t77 * t182;
    real_type t238 = t77 * t189;
    result__[ 3   ] = -t5 * t12 * t129 - t5 * t19 * t129 - t5 * t28 * t129 - t5 * t33 * t129 - t5 * t36 * t129 - t5 * t22 * t140 - t23 * t2 * t56 + t231 * t60 - t9 * t5 * t149 + t231 * t71 + t23 * (-t83 * t87 * t238 + t187 * t236) * t73 + t231 * t95 - t204 * (t98 * t236 + t210) * t97 - t231 * t155 - t204 * (-t84 * t236 - t238) * t112 - t231 * t157 - t231 * t121 - t231 * t125;
    real_type t259 = t74 * t74;
    real_type t261 = t83 * t77;
    real_type t264 = t87 * t87;
    real_type t270 = t74 * t182;
    result__[ 4   ] = t22 * t9 * t62 + t23 * (t84 * t261 * t259 * t182 + t261 * t264 * t189) * t73 - t204 * (t98 * t77 * t270 + t101 * t6) * t97 - t204 * (t77 * t87 * t189 - t84 * t77 * t270 - t101 * t3) * t112;
    real_type t285 = ALIAS_deltaLimit_D(t32);
    result__[ 5   ] = -t285 * t23 + t23 * (t98 * t83 * t81 - t85 * t270) * t73 - t204 * (-t98 * t182 - t113) * t97 - t204 * (t84 * t182 - t99) * t112;
    real_type t301 = ALIAS_FxLimit_D(t35);
    result__[ 6   ] = -t23 * t108 * t97 - t301 * t23;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hx_eval",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DHxDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DHxDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby::DHxDx_nnz() const
  { return 45; }

  void
  OCP_eRumby::DHxDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 0   ; jIndex[ 4  ] = 4   ;
    iIndex[ 5  ] = 0   ; jIndex[ 5  ] = 5   ;
    iIndex[ 6  ] = 0   ; jIndex[ 6  ] = 6   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 0   ;
    iIndex[ 8  ] = 1   ; jIndex[ 8  ] = 1   ;
    iIndex[ 9  ] = 1   ; jIndex[ 9  ] = 2   ;
    iIndex[ 10 ] = 1   ; jIndex[ 10 ] = 3   ;
    iIndex[ 11 ] = 1   ; jIndex[ 11 ] = 4   ;
    iIndex[ 12 ] = 1   ; jIndex[ 12 ] = 5   ;
    iIndex[ 13 ] = 1   ; jIndex[ 13 ] = 6   ;
    iIndex[ 14 ] = 2   ; jIndex[ 14 ] = 0   ;
    iIndex[ 15 ] = 2   ; jIndex[ 15 ] = 1   ;
    iIndex[ 16 ] = 2   ; jIndex[ 16 ] = 2   ;
    iIndex[ 17 ] = 2   ; jIndex[ 17 ] = 3   ;
    iIndex[ 18 ] = 2   ; jIndex[ 18 ] = 4   ;
    iIndex[ 19 ] = 2   ; jIndex[ 19 ] = 5   ;
    iIndex[ 20 ] = 2   ; jIndex[ 20 ] = 6   ;
    iIndex[ 21 ] = 3   ; jIndex[ 21 ] = 0   ;
    iIndex[ 22 ] = 3   ; jIndex[ 22 ] = 1   ;
    iIndex[ 23 ] = 3   ; jIndex[ 23 ] = 2   ;
    iIndex[ 24 ] = 3   ; jIndex[ 24 ] = 3   ;
    iIndex[ 25 ] = 3   ; jIndex[ 25 ] = 4   ;
    iIndex[ 26 ] = 3   ; jIndex[ 26 ] = 5   ;
    iIndex[ 27 ] = 3   ; jIndex[ 27 ] = 6   ;
    iIndex[ 28 ] = 4   ; jIndex[ 28 ] = 0   ;
    iIndex[ 29 ] = 4   ; jIndex[ 29 ] = 1   ;
    iIndex[ 30 ] = 4   ; jIndex[ 30 ] = 2   ;
    iIndex[ 31 ] = 4   ; jIndex[ 31 ] = 3   ;
    iIndex[ 32 ] = 4   ; jIndex[ 32 ] = 4   ;
    iIndex[ 33 ] = 4   ; jIndex[ 33 ] = 5   ;
    iIndex[ 34 ] = 5   ; jIndex[ 34 ] = 0   ;
    iIndex[ 35 ] = 5   ; jIndex[ 35 ] = 1   ;
    iIndex[ 36 ] = 5   ; jIndex[ 36 ] = 2   ;
    iIndex[ 37 ] = 5   ; jIndex[ 37 ] = 3   ;
    iIndex[ 38 ] = 5   ; jIndex[ 38 ] = 4   ;
    iIndex[ 39 ] = 5   ; jIndex[ 39 ] = 5   ;
    iIndex[ 40 ] = 6   ; jIndex[ 40 ] = 0   ;
    iIndex[ 41 ] = 6   ; jIndex[ 41 ] = 1   ;
    iIndex[ 42 ] = 6   ; jIndex[ 42 ] = 2   ;
    iIndex[ 43 ] = 6   ; jIndex[ 43 ] = 3   ;
    iIndex[ 44 ] = 6   ; jIndex[ 44 ] = 6   ;
  }

  void
  OCP_eRumby::DHxDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = cos(t1);
    real_type t3   = X__[2];
    real_type t5   = sin(t1);
    real_type t6   = X__[3];
    real_type t8   = t3 * t2 - t6 * t5;
    real_type t9   = 1.0 / t8;
    real_type t10  = Q__[1];
    real_type t11  = t10 * t9;
    real_type t12  = X__[0];
    real_type t13  = ModelPars[49];
    real_type t14  = Q__[0];
    real_type t15  = ALIAS_rightWidth(t14);
    real_type t16  = t12 - t13 + t15;
    real_type t17  = ALIAS_roadRightLateralBoundaries_D(t16);
    real_type t21  = t12 * t10 - 1;
    real_type t22  = t21 * t9;
    real_type t23  = ALIAS_roadRightLateralBoundaries_DD(t16);
    real_type t25  = ALIAS_leftWidth(t14);
    real_type t26  = t25 - t12 - t13;
    real_type t27  = ALIAS_roadLeftLateralBoundaries_D(t26);
    real_type t30  = ALIAS_roadLeftLateralBoundaries_DD(t26);
    real_type t32  = ModelPars[21];
    real_type t33  = 1 - t32;
    real_type t35  = ModelPars[27] * ModelPars[27];
    real_type t36  = 1.0 / t35;
    real_type t40  = t12 * t33;
    result__[ 0   ] = -4 * t10 * t9 * t36 * t40 - 2 * t22 * t36 * t33 - 2 * t17 * t11 + 2 * t27 * t11 - t23 * t22 - t30 * t22;
    real_type t45  = t8 * t8;
    real_type t46  = 1.0 / t45;
    real_type t47  = t10 * t46;
    real_type t48  = speed_limit(t3);
    real_type t51  = -t6 * t2 - t3 * t5;
    real_type t52  = t51 * t48;
    real_type t54  = roadRightLateralBoundaries(t16);
    real_type t55  = t51 * t54;
    real_type t57  = t21 * t46;
    real_type t60  = roadLeftLateralBoundaries(t26);
    real_type t61  = t51 * t60;
    real_type t65  = X__[5];
    real_type t66  = deltaLimit(t65);
    real_type t67  = t51 * t66;
    real_type t69  = X__[6];
    real_type t70  = FxLimit(t69);
    real_type t71  = t51 * t70;
    real_type t73  = t36 * t40;
    real_type t74  = t51 * t57;
    real_type t79  = t12 * t12;
    real_type t82  = t36 * t79 * t33 + t32 * ModelPars[32];
    real_type t83  = t46 * t82;
    real_type t86  = L__[0];
    real_type t87  = -t8;
    real_type t88  = t87 * t86;
    real_type t90  = t51 * t51;
    real_type t91  = t90 * t86;
    real_type t93  = L__[1];
    real_type t94  = t10 * t10;
    real_type t95  = t94 * t93;
    real_type t96  = 1.0 / t21;
    real_type t102 = X__[4];
    real_type t104 = (t96 * t8 * t10 + t102) * t93;
    real_type t105 = t51 * t47;
    real_type t107 = L__[2];
    real_type t108 = ModelPars[11];
    real_type t110 = t108 * t102 + t6;
    real_type t111 = 1.0 / t3;
    real_type t113 = -t111 * t110 + t65;
    real_type t114 = MF_Fy_f(t113);
    real_type t115 = t108 * t114;
    real_type t117 = 1.0 / ModelPars[7];
    real_type t118 = cos(t65);
    real_type t119 = t118 * t117;
    real_type t120 = t119 * t115;
    real_type t121 = ModelPars[12];
    real_type t123 = t121 * t102 - t6;
    real_type t124 = t111 * t123;
    real_type t125 = MF_Fy_r(t124);
    real_type t129 = (t117 * t121 * t125 - t120) * t107;
    real_type t131 = L__[3];
    real_type t132 = sin(t65);
    real_type t133 = t132 * t114;
    real_type t135 = ModelPars[24];
    real_type t137 = ModelPars[0];
    real_type t138 = t3 * t3;
    real_type t142 = 1.0 / t135;
    real_type t143 = t142 * (t135 * t6 * t102 - t138 * t137 - t133 + t69) * t131;
    real_type t145 = L__[4];
    real_type t146 = t118 * t114;
    real_type t151 = t142 * (-t135 * t3 * t102 + t125 + t146) * t145;
    real_type t155 = L__[5] * U__[1];
    real_type t159 = L__[6] * U__[0];
    result__[ 1   ] = t9 * t96 * t51 * t95 + t51 * t10 * t83 + t51 * t17 * t57 - t51 * t27 * t57 - t105 * t104 - t105 * t129 + t105 * t143 + t105 * t151 + t105 * t155 + t105 * t159 + t11 * t88 + t52 * t47 + t55 * t47 + t61 * t47 + t67 * t47 + t71 * t47 - t47 * t91 + 2 * t74 * t73;
    real_type t161 = t2 * t48;
    real_type t163 = ALIAS_speed_limit_D(t3);
    real_type t165 = t2 * t54;
    real_type t169 = t2 * t60;
    real_type t173 = t2 * t66;
    real_type t175 = t2 * t70;
    real_type t177 = t2 * t57;
    real_type t182 = t5 * t86;
    real_type t184 = t2 * t10 * t83 + t2 * t17 * t57 - t2 * t27 * t57 - t163 * t11 - t11 * t182 + t161 * t47 + t165 * t47 + t169 * t47 + t173 * t47 + t175 * t47 + 2 * t177 * t73;
    real_type t185 = t51 * t86;
    real_type t186 = t2 * t47;
    real_type t192 = MF_Fy_f_D(t113);
    real_type t193 = t110 * t192;
    real_type t194 = 1.0 / t138;
    real_type t195 = t194 * t193;
    real_type t196 = t117 * t108;
    real_type t197 = t118 * t196;
    real_type t199 = MF_Fy_r_D(t124);
    real_type t200 = t123 * t199;
    real_type t205 = (-t117 * t121 * t194 * t200 - t197 * t195) * t107;
    real_type t208 = t132 * t194;
    real_type t209 = t208 * t193;
    real_type t213 = (-2 * t3 * t137 - t209) * t131;
    real_type t214 = t9 * t142;
    real_type t215 = t10 * t214;
    real_type t219 = t118 * t194;
    real_type t220 = t219 * t193;
    real_type t221 = t135 * t102;
    real_type t223 = (-t194 * t200 + t220 - t221) * t145;
    real_type t228 = t9 * t96 * t2 * t95 - t186 * t104 + t11 * t205 - t186 * t129 + t186 * t143 + t186 * t151 + t186 * t155 + t186 * t159 - t186 * t185 - t215 * t213 - t215 * t223;
    result__[ 2   ] = t184 + t228;
    real_type t229 = t5 * t48;
    real_type t231 = t5 * t54;
    real_type t235 = t5 * t60;
    real_type t239 = t5 * t66;
    real_type t241 = t5 * t70;
    real_type t243 = t5 * t57;
    real_type t248 = t2 * t86;
    real_type t251 = t5 * t47;
    real_type t257 = t111 * t192;
    real_type t259 = t111 * t199;
    real_type t260 = t117 * t121;
    real_type t263 = (t197 * t257 - t260 * t259) * t107;
    real_type t266 = t132 * t257;
    real_type t268 = (t266 + t221) * t131;
    real_type t271 = t118 * t257;
    real_type t273 = (-t259 - t271) * t145;
    real_type t278 = -t9 * t96 * t5 * t95 + t251 * t104 + t11 * t263 + t251 * t129 - t251 * t143 - t251 * t151 - t251 * t155 - t251 * t159 + t251 * t185 - t215 * t268 - t215 * t273;
    result__[ 3   ] = -t5 * t10 * t83 - t5 * t17 * t57 + t5 * t27 * t57 - t11 * t248 - t229 * t47 - t231 * t47 - t235 * t47 - t239 * t47 - t241 * t47 - 2 * t243 * t73 + t278;
    real_type t281 = t108 * t108;
    real_type t282 = t281 * t192;
    real_type t283 = t117 * t111;
    real_type t284 = t118 * t283;
    real_type t286 = t121 * t121;
    real_type t287 = t286 * t199;
    real_type t290 = (t284 * t282 + t283 * t287) * t107;
    real_type t292 = t108 * t192;
    real_type t293 = t132 * t111;
    real_type t294 = t293 * t292;
    real_type t297 = (t135 * t6 + t294) * t131;
    real_type t299 = t121 * t199;
    real_type t301 = t118 * t111;
    real_type t302 = t301 * t292;
    real_type t305 = (t111 * t299 - t135 * t3 - t302) * t145;
    result__[ 4   ] = t10 * t9 * t93 + t11 * t290 - t215 * t297 - t215 * t305;
    real_type t307 = ALIAS_deltaLimit_D(t65);
    real_type t310 = t132 * t117;
    real_type t313 = (t310 * t115 - t119 * t292) * t107;
    real_type t315 = t132 * t192;
    real_type t317 = (-t315 - t146) * t131;
    real_type t319 = t118 * t192;
    real_type t321 = (t319 - t133) * t145;
    result__[ 5   ] = -t307 * t11 + t11 * t313 - t215 * t317 - t215 * t321;
    real_type t323 = ALIAS_FxLimit_D(t69);
    real_type t325 = t142 * t131;
    result__[ 6   ] = -t323 * t11 - t11 * t325;
    result__[ 7   ] = result__[1];
    real_type t328 = 1.0 / t45 / t8;
    real_type t329 = t21 * t328;
    real_type t330 = t90 * t329;
    real_type t333 = t87 * t57;
    real_type t355 = -2 * t90 * t66 * t329 + 2 * t330 * t104 - t333 * t104 + 2 * t330 * t129 - t333 * t129 - 2 * t330 * t143 + t333 * t143 - 2 * t330 * t151 + t333 * t151 - 2 * t330 * t155 + t333 * t155 - 2 * t330 * t159 + t333 * t159 - 3 * t74 * t88;
    real_type t370 = t10 * t93;
    real_type t376 = t328 * t82;
    real_type t397 = 2 * t329 * t90 * t51 * t86 - 2 * t90 * t21 * t376 + t87 * t21 * t83 - t22 * t51 * t86 - 2 * t90 * t48 * t329 - 2 * t90 * t54 * t329 - 2 * t90 * t60 * t329 - 2 * t90 * t70 * t329 - 2 * t46 * t90 * t370 + t9 * t87 * t370 + t87 * t48 * t57 + t87 * t54 * t57 + t87 * t60 * t57 + t87 * t66 * t57 + t87 * t70 * t57;
    result__[ 8   ] = t355 + t397;
    real_type t398 = t328 * t104;
    real_type t399 = t51 * t21;
    real_type t400 = t2 * t399;
    real_type t403 = t328 * t129;
    real_type t408 = t328 * t159;
    real_type t411 = t328 * t155;
    real_type t414 = t142 * t213;
    real_type t416 = t142 * t223;
    real_type t436 = -2 * t2 * t52 * t329 - 2 * t2 * t55 * t329 - 2 * t2 * t61 * t329 - 2 * t2 * t67 * t329 + t243 * t129 - t243 * t143 - t243 * t151 - t243 * t155 - t243 * t159 - t74 * t205 - 2 * t400 * t376 + 2 * t400 * t398 + 2 * t400 * t403 - 2 * t400 * t408 - 2 * t400 * t411 + t74 * t414 + t74 * t416;
    real_type t446 = t46 * t51;
    real_type t452 = t2 * t51 * t329;
    real_type t458 = t5 * t21;
    real_type t469 = t51 * t163 * t57 - 2 * t2 * t71 * t329 + 2 * t2 * t329 * t91 - 2 * t2 * t446 * t370 - t9 * t5 * t370 + t243 * t104 - 2 * t452 * t143 - 2 * t452 * t151 - t177 * t88 + 2 * t243 * t185 - t22 * t248 - t229 * t57 - t231 * t57 - t235 * t57 - t239 * t57 - t241 * t57 - t458 * t83;
    result__[ 9   ] = t436 + t469;
    real_type t470 = t5 * t399;
    real_type t481 = t142 * t268;
    real_type t483 = t142 * t273;
    real_type t501 = 2 * t5 * t55 * t329 + 2 * t5 * t61 * t329 + 2 * t5 * t67 * t329 + 2 * t5 * t71 * t329 + t177 * t129 - t177 * t143 - t177 * t151 - t177 * t155 - t177 * t159 - t74 * t263 - 2 * t470 * t398 - 2 * t470 * t403 + 2 * t470 * t408 + 2 * t470 * t411 + t74 * t481 + t74 * t483;
    real_type t518 = t5 * t51 * t329;
    real_type t523 = t2 * t21;
    real_type t533 = -t9 * t2 * t370 + 2 * t5 * t52 * t329 - 2 * t5 * t329 * t91 + 2 * t5 * t446 * t370 + t177 * t104 + 2 * t518 * t143 + 2 * t518 * t151 - t161 * t57 - t165 * t57 - t169 * t57 - t173 * t57 - t175 * t57 + 2 * t177 * t185 + t22 * t182 + t243 * t88 + 2 * t470 * t376 - t523 * t83;
    result__[ 10  ] = t501 + t533;
    real_type t534 = t46 * t93;
    real_type t537 = t142 * t297;
    real_type t539 = t142 * t305;
    result__[ 11  ] = -t74 * t290 - t399 * t534 + t74 * t537 + t74 * t539;
    real_type t544 = t142 * t317;
    real_type t546 = t142 * t321;
    result__[ 12  ] = t51 * t307 * t57 - t74 * t313 + t74 * t544 + t74 * t546;
    result__[ 13  ] = t51 * t323 * t57 + t74 * t325;
    result__[ 14  ] = result__[2];
    result__[ 15  ] = result__[9];
    real_type t553 = t2 * t2;
    real_type t554 = t553 * t329;
    real_type t567 = ALIAS_speed_limit_DD(t3);
    real_type t571 = MF_Fy_f_DD(t113);
    real_type t572 = t110 * t110;
    real_type t573 = t572 * t571;
    real_type t574 = t138 * t138;
    real_type t575 = 1.0 / t574;
    real_type t579 = 1.0 / t138 / t3;
    real_type t586 = t21 * t214;
    real_type t588 = MF_Fy_r_DD(t124);
    real_type t589 = t123 * t123;
    real_type t590 = t589 * t588;
    real_type t602 = 2 * t177 * t416 - 2 * t554 * t155 - 2 * t554 * t159 + 2 * t554 * t185 + 2 * t554 * t104 + 2 * t554 * t129 + 2 * t177 * t414 - t567 * t22 - 2 * t177 * t205 - t586 * (2 * t132 * t579 * t193 - t132 * t575 * t573 - 2 * t137) * t131 - t586 * (-2 * t118 * t579 * t193 + t118 * t575 * t573 + 2 * t579 * t200 + t575 * t590) * t145;
    real_type t622 = 2 * t177 * t182;
    real_type t648 = -2 * t553 * t48 * t329 - 2 * t553 * t54 * t329 - 2 * t553 * t60 * t329 - 2 * t553 * t66 * t329 - 2 * t553 * t70 * t329 - 2 * t553 * t21 * t376 + t622 - 2 * t46 * t553 * t370 + 2 * t2 * t163 * t57 + t22 * (2 * t117 * t121 * t579 * t200 + t117 * t121 * t575 * t590 + 2 * t197 * t579 * t193 - t197 * t575 * t573) * t107 - 2 * t554 * t143 - 2 * t554 * t151;
    result__[ 16  ] = t602 + t648;
    real_type t650 = t5 * t2 * t329;
    real_type t656 = t5 * t523;
    real_type t677 = 2 * t5 * t165 * t329 + 2 * t5 * t169 * t329 - 2 * t656 * t328 * t185 + 2 * t650 * t143 + 2 * t650 * t151 + t177 * t481 + t177 * t483 - t243 * t414 - t243 * t416 - 2 * t656 * t398 - 2 * t656 * t403 + 2 * t656 * t408 + 2 * t656 * t411;
    real_type t680 = t579 * t588;
    real_type t682 = t194 * t199;
    real_type t683 = t579 * t571;
    real_type t686 = t194 * t192;
    real_type t712 = t5 * t5;
    real_type t729 = 2 * t656 * t376 - t586 * (-t118 * t110 * t683 + t118 * t686 + t123 * t680 + t682) * t145 - t586 * (t132 * t110 * t683 - t132 * t686) * t131 + t243 * t205 - t177 * t263 + 2 * t5 * t173 * t329 + 2 * t5 * t175 * t329 + 2 * t5 * t46 * t2 * t370 + 2 * t5 * t161 * t329 - t57 * t712 * t86 + t57 * t553 * t86 - t5 * t163 * t57 + t22 * (t117 * t121 * t123 * t680 + t197 * t110 * t683 - t197 * t686 + t260 * t682) * t107;
    result__[ 17  ] = t677 + t729;
    real_type t731 = t281 * t571;
    real_type t736 = t117 * t194;
    real_type t737 = t118 * t736;
    real_type t739 = t286 * t588;
    real_type t740 = t123 * t579;
    real_type t748 = t108 * t571;
    real_type t749 = t110 * t579;
    real_type t757 = t121 * t588;
    result__[ 18  ] = -t523 * t534 + t22 * (t118 * t117 * t110 * t579 * t731 - t117 * t740 * t739 - t737 * t282 - t736 * t287) * t107 - t177 * t290 - t586 * (t132 * t749 * t748 - t208 * t292) * t131 + t177 * t537 - t586 * (-t118 * t749 * t748 - t194 * t299 + t219 * t292 - t740 * t757 - t135) * t145 + t177 * t539;
    real_type t769 = t110 * t571;
    real_type t772 = t132 * t196;
    result__[ 19  ] = t2 * t307 * t57 + t22 * (-t197 * t194 * t769 + t772 * t195) * t107 - t177 * t313 - t586 * (-t208 * t769 - t220) * t131 + t177 * t544 - t586 * (t219 * t769 - t209) * t145 + t177 * t546;
    result__[ 20  ] = t2 * t323 * t57 + t177 * t325;
    result__[ 21  ] = result__[3];
    result__[ 22  ] = result__[10];
    result__[ 23  ] = result__[17];
    real_type t794 = t194 * t571;
    real_type t796 = t194 * t588;
    real_type t821 = t712 * t329;
    real_type t845 = t571 * t131;
    real_type t848 = t22 * t142 * t132;
    real_type t850 = -t586 * (t118 * t794 + t796) * t145 - 2 * t821 * t155 - 2 * t821 * t159 - t622 + 2 * t821 * t185 + 2 * t821 * t104 - 2 * t821 * t151 - 2 * t243 * t481 - 2 * t821 * t143 - 2 * t243 * t483 + t848 * t194 * t845;
    result__[ 24  ] = -2 * t46 * t712 * t370 + t22 * (-t197 * t794 + t260 * t796) * t107 - 2 * t712 * t48 * t329 - 2 * t712 * t54 * t329 - 2 * t712 * t60 * t329 - 2 * t712 * t66 * t329 - 2 * t712 * t70 * t329 - 2 * t712 * t21 * t376 + 2 * t243 * t263 + 2 * t821 * t129 + t850;
    result__[ 25  ] = t458 * t534 + t22 * (-t737 * t731 - t736 * t739) * t107 + t243 * t290 - t586 * (-t208 * t748 + t135) * t131 - t243 * t537 - t586 * (-t194 * t757 + t219 * t748) * t145 - t243 * t539;
    real_type t871 = t111 * t571;
    result__[ 26  ] = -t5 * t307 * t57 + t22 * (t197 * t871 - t772 * t257) * t107 + t243 * t313 - t586 * (t132 * t871 + t271) * t131 - t243 * t544 - t586 * (-t118 * t871 + t266) * t145 - t243 * t546;
    result__[ 27  ] = -t5 * t323 * t57 - t243 * t325;
    result__[ 28  ] = result__[4];
    result__[ 29  ] = result__[11];
    result__[ 30  ] = result__[18];
    result__[ 31  ] = result__[25];
    result__[ 32  ] = t22 * (-t737 * t281 * t108 * t571 + t736 * t286 * t121 * t588) * t107 + t848 * t194 * t281 * t845 - t586 * (t194 * t739 + t219 * t731) * t145;
    result__[ 33  ] = t22 * (-t132 * t283 * t282 + t284 * t731) * t107 - t586 * (t293 * t748 + t302) * t131 - t586 * (-t301 * t748 + t294) * t145;
    result__[ 34  ] = result__[5];
    result__[ 35  ] = result__[12];
    result__[ 36  ] = result__[19];
    result__[ 37  ] = result__[26];
    result__[ 38  ] = result__[33];
    real_type t922 = ALIAS_deltaLimit_DD(t65);
    result__[ 39  ] = -t922 * t22 + t22 * (-t119 * t748 + 2 * t310 * t292 + t120) * t107 - t586 * (-t132 * t571 + t133 - 2 * t319) * t131 - t586 * (t118 * t571 - t146 - 2 * t315) * t145;
    result__[ 40  ] = result__[6];
    result__[ 41  ] = result__[13];
    result__[ 42  ] = result__[20];
    result__[ 43  ] = result__[27];
    real_type t940 = ALIAS_FxLimit_DD(t69);
    result__[ 44  ] = -t940 * t22;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DHxDx_sparse",45);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DHxDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DHxDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DHxDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DHxDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DHxDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |  _   _
   | | | | |_   _
   | | |_| | | | |
   | |  _  | |_| |
   | |_| |_|\__,_|
   |
  \*/

  integer
  OCP_eRumby::Hu_numEqns() const
  { return 2; }

  void
  OCP_eRumby::Hu_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t2   = X__[1];
    real_type t3   = cos(t2);
    real_type t6   = sin(t2);
    real_type t10  = 1.0 / (X__[2] * t3 - X__[3] * t6);
    real_type t15  = Q__[1] * X__[0] - 1;
    result__[ 0   ] = -t15 * t10 * L__[6];
    result__[ 1   ] = -t15 * t10 * L__[5];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hu_eval",2);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DHuDx_numRows() const
  { return 2; }

  integer
  OCP_eRumby::DHuDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby::DHuDx_nnz() const
  { return 8; }

  void
  OCP_eRumby::DHuDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 1   ; jIndex[ 4  ] = 0   ;
    iIndex[ 5  ] = 1   ; jIndex[ 5  ] = 1   ;
    iIndex[ 6  ] = 1   ; jIndex[ 6  ] = 2   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 3   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DHuDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = L__[6];
    real_type t2   = X__[1];
    real_type t3   = cos(t2);
    real_type t4   = X__[2];
    real_type t6   = sin(t2);
    real_type t7   = X__[3];
    real_type t9   = t4 * t3 - t7 * t6;
    real_type t10  = 1.0 / t9;
    real_type t12  = Q__[1];
    result__[ 0   ] = -t12 * t10 * t1;
    real_type t14  = t9 * t9;
    real_type t15  = 1.0 / t14;
    real_type t16  = t15 * t1;
    real_type t19  = X__[0] * t12 - 1;
    real_type t23  = (-t7 * t3 - t4 * t6) * t19;
    result__[ 1   ] = t23 * t16;
    real_type t24  = t3 * t19;
    result__[ 2   ] = t24 * t16;
    real_type t25  = t6 * t19;
    result__[ 3   ] = -t25 * t16;
    real_type t27  = L__[5];
    result__[ 4   ] = -t12 * t10 * t27;
    real_type t30  = t15 * t27;
    result__[ 5   ] = t23 * t30;
    result__[ 6   ] = t24 * t30;
    result__[ 7   ] = -t25 * t30;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DHuDx_sparse",8);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DHuDp_numRows() const
  { return 2; }

  integer
  OCP_eRumby::DHuDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DHuDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DHuDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DHuDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |  _   _
   | | | | |_ __
   | | |_| | '_ \
   | |  _  | |_) |
   | |_| |_| .__/
   |       |_|
  \*/

  integer
  OCP_eRumby::Hp_numEqns() const
  { return 0; }

  void
  OCP_eRumby::Hp_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);

    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hp_eval",0);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DHpDp_numRows() const
  { return 0; }

  integer
  OCP_eRumby::DHpDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DHpDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DHpDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DHpDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |        _
   |    ___| |_ __ _
   |   / _ \ __/ _` |
   |  |  __/ || (_| |
   |   \___|\__\__,_|
  \*/

  integer
  OCP_eRumby::eta_numEqns() const
  { return 7; }

  void
  OCP_eRumby::eta_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = L__[0];
    result__[ 1   ] = -L__[1];
    result__[ 2   ] = L__[3];
    result__[ 3   ] = L__[4];
    result__[ 4   ] = L__[2];
    result__[ 5   ] = L__[5];
    result__[ 6   ] = L__[6];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"eta_eval",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DetaDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DetaDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby::DetaDx_nnz() const
  { return 0; }

  void
  OCP_eRumby::DetaDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DetaDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DetaDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DetaDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DetaDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DetaDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DetaDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |    _ __  _   _
   |   | '_ \| | | |
   |   | | | | |_| |
   |   |_| |_|\__,_|
  \*/

  integer
  OCP_eRumby::nu_numEqns() const
  { return 7; }

  void
  OCP_eRumby::nu_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = V__[0];
    result__[ 1   ] = -V__[1];
    result__[ 2   ] = V__[4];
    result__[ 3   ] = V__[2];
    result__[ 4   ] = V__[3];
    result__[ 5   ] = V__[5];
    result__[ 6   ] = V__[6];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"nu_eval",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DnuDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DnuDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby::DnuDx_nnz() const
  { return 0; }

  void
  OCP_eRumby::DnuDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DnuDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DnuDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DnuDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DnuDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DnuDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DnuDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

}

// EOF: OCP_eRumby_Methods.cc
