/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_Methods.cc                                          |
 |                                                                       |
 |  version: 1.0   date 28/11/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby.hh"
#include "OCP_eRumby_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_rightWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_R")
#define ALIAS_rightWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_R")
#define ALIAS_rightWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_R")
#define ALIAS_leftWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_L")
#define ALIAS_leftWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_L")
#define ALIAS_leftWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_L")
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_FxLimit_DD(__t1) FxLimit.DD( __t1)
#define ALIAS_FxLimit_D(__t1) FxLimit.D( __t1)
#define ALIAS_deltaLimit_DD(__t1) deltaLimit.DD( __t1)
#define ALIAS_deltaLimit_D(__t1) deltaLimit.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_speed_limit_DD(__t1) speed_limit.DD( __t1)
#define ALIAS_speed_limit_D(__t1) speed_limit.D( __t1)
#define ALIAS_Fx__r__OControl_D_3(__t1, __t2, __t3) Fx__r__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2(__t1, __t2, __t3) Fx__r__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1(__t1, __t2, __t3) Fx__r__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_3_3(__t1, __t2, __t3) Fx__r__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_3(__t1, __t2, __t3) Fx__r__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_2(__t1, __t2, __t3) Fx__r__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_3(__t1, __t2, __t3) Fx__r__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_2(__t1, __t2, __t3) Fx__r__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_1(__t1, __t2, __t3) Fx__r__OControl.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3(__t1, __t2, __t3) delta__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2(__t1, __t2, __t3) delta__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1(__t1, __t2, __t3) delta__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3_3(__t1, __t2, __t3) delta__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_3(__t1, __t2, __t3) delta__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_2(__t1, __t2, __t3) delta__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_3(__t1, __t2, __t3) delta__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_2(__t1, __t2, __t3) delta__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_1(__t1, __t2, __t3) delta__OControl.D_1_1( __t1, __t2, __t3)


namespace OCP_eRumbyDefine {

  /*\
   |    ___  ___  ___
   |   / _ \|   \| __|
   |  | (_) | |) | _|
   |   \___/|___/|___|
  \*/

  integer
  OCP_eRumby::rhs_ode_numEqns() const
  { return 7; }

  void
  OCP_eRumby::rhs_ode_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = sin(t1);
    real_type t3   = X__[2];
    real_type t5   = cos(t1);
    real_type t6   = X__[3];
    real_type t11  = -t6 * t2 + t3 * t5;
    real_type t12  = 1.0 / t11;
    real_type t14  = Q__[1];
    real_type t17  = X__[0] * t14 - 1;
    result__[ 0   ] = t17 * t12 * (-t3 * t2 - t6 * t5);
    real_type t21  = X__[4];
    result__[ 1   ] = t17 * t12 * (1.0 / t17 * t11 * t14 + t21);
    real_type t24  = ModelPars[11];
    real_type t27  = 1.0 / t3;
    real_type t29  = X__[5];
    real_type t31  = MF_Fy_f(-t27 * (t24 * t21 + t6) + t29);
    real_type t34  = 1.0 / ModelPars[7];
    real_type t35  = cos(t29);
    real_type t38  = ModelPars[12];
    real_type t42  = MF_Fy_r(t27 * (t38 * t21 - t6));
    result__[ 2   ] = t17 * t12 * (-t35 * t34 * t24 * t31 + t34 * t38 * t42);
    real_type t48  = sin(t29);
    real_type t51  = ModelPars[24];
    real_type t54  = t3 * t3;
    real_type t57  = 1.0 / t51;
    real_type t59  = t17 * t12;
    result__[ 3   ] = -t59 * t57 * (t51 * t6 * t21 - t48 * t31 - t54 * ModelPars[0] + X__[6]);
    result__[ 4   ] = -t59 * t57 * (-t51 * t3 * t21 + t35 * t31 + t42);
    result__[ 5   ] = -t17 * t12 * U__[1];
    result__[ 6   ] = -t17 * t12 * U__[0];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"rhs_ode",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::Drhs_odeDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby::Drhs_odeDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby::Drhs_odeDx_nnz() const
  { return 36; }

  void
  OCP_eRumby::Drhs_odeDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 1   ; jIndex[ 4  ] = 0   ;
    iIndex[ 5  ] = 1   ; jIndex[ 5  ] = 1   ;
    iIndex[ 6  ] = 1   ; jIndex[ 6  ] = 2   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 3   ;
    iIndex[ 8  ] = 1   ; jIndex[ 8  ] = 4   ;
    iIndex[ 9  ] = 2   ; jIndex[ 9  ] = 0   ;
    iIndex[ 10 ] = 2   ; jIndex[ 10 ] = 1   ;
    iIndex[ 11 ] = 2   ; jIndex[ 11 ] = 2   ;
    iIndex[ 12 ] = 2   ; jIndex[ 12 ] = 3   ;
    iIndex[ 13 ] = 2   ; jIndex[ 13 ] = 4   ;
    iIndex[ 14 ] = 2   ; jIndex[ 14 ] = 5   ;
    iIndex[ 15 ] = 3   ; jIndex[ 15 ] = 0   ;
    iIndex[ 16 ] = 3   ; jIndex[ 16 ] = 1   ;
    iIndex[ 17 ] = 3   ; jIndex[ 17 ] = 2   ;
    iIndex[ 18 ] = 3   ; jIndex[ 18 ] = 3   ;
    iIndex[ 19 ] = 3   ; jIndex[ 19 ] = 4   ;
    iIndex[ 20 ] = 3   ; jIndex[ 20 ] = 5   ;
    iIndex[ 21 ] = 3   ; jIndex[ 21 ] = 6   ;
    iIndex[ 22 ] = 4   ; jIndex[ 22 ] = 0   ;
    iIndex[ 23 ] = 4   ; jIndex[ 23 ] = 1   ;
    iIndex[ 24 ] = 4   ; jIndex[ 24 ] = 2   ;
    iIndex[ 25 ] = 4   ; jIndex[ 25 ] = 3   ;
    iIndex[ 26 ] = 4   ; jIndex[ 26 ] = 4   ;
    iIndex[ 27 ] = 4   ; jIndex[ 27 ] = 5   ;
    iIndex[ 28 ] = 5   ; jIndex[ 28 ] = 0   ;
    iIndex[ 29 ] = 5   ; jIndex[ 29 ] = 1   ;
    iIndex[ 30 ] = 5   ; jIndex[ 30 ] = 2   ;
    iIndex[ 31 ] = 5   ; jIndex[ 31 ] = 3   ;
    iIndex[ 32 ] = 6   ; jIndex[ 32 ] = 0   ;
    iIndex[ 33 ] = 6   ; jIndex[ 33 ] = 1   ;
    iIndex[ 34 ] = 6   ; jIndex[ 34 ] = 2   ;
    iIndex[ 35 ] = 6   ; jIndex[ 35 ] = 3   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::Drhs_odeDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = sin(t1);
    real_type t3   = X__[2];
    real_type t5   = cos(t1);
    real_type t6   = X__[3];
    real_type t8   = -t3 * t2 - t6 * t5;
    real_type t11  = -t6 * t2 + t3 * t5;
    real_type t12  = 1.0 / t11;
    real_type t14  = Q__[1];
    result__[ 0   ] = t14 * t12 * t8;
    real_type t19  = X__[0] * t14 - 1;
    real_type t21  = t8 * t8;
    real_type t22  = t11 * t11;
    real_type t23  = 1.0 / t22;
    result__[ 1   ] = -t19 * t12 * t11 - t19 * t23 * t21;
    real_type t28  = t23 * t8;
    real_type t29  = t5 * t19;
    result__[ 2   ] = -t19 * t12 * t2 - t29 * t28;
    real_type t33  = t2 * t19;
    result__[ 3   ] = -t19 * t12 * t5 + t33 * t28;
    real_type t35  = t14 * t14;
    real_type t36  = 1.0 / t19;
    real_type t40  = X__[4];
    real_type t41  = t36 * t11 * t14 + t40;
    result__[ 4   ] = t14 * t12 * t41 - t36 * t35;
    real_type t44  = t23 * t41;
    real_type t45  = t8 * t19;
    result__[ 5   ] = -t45 * t44 + result__[0];
    result__[ 6   ] = t12 * t5 * t14 - t29 * t44;
    result__[ 7   ] = -t12 * t2 * t14 + t33 * t44;
    result__[ 8   ] = t19 * t12;
    real_type t53  = ModelPars[11];
    real_type t55  = t53 * t40 + t6;
    real_type t56  = 1.0 / t3;
    real_type t58  = X__[5];
    real_type t59  = -t56 * t55 + t58;
    real_type t60  = MF_Fy_f(t59);
    real_type t61  = t53 * t60;
    real_type t63  = 1.0 / ModelPars[7];
    real_type t64  = cos(t58);
    real_type t65  = t64 * t63;
    real_type t67  = ModelPars[12];
    real_type t69  = t67 * t40 - t6;
    real_type t70  = t56 * t69;
    real_type t71  = MF_Fy_r(t70);
    real_type t74  = t63 * t67 * t71 - t65 * t61;
    result__[ 9   ] = t14 * t12 * t74;
    real_type t76  = t23 * t74;
    result__[ 10  ] = -t45 * t76;
    real_type t78  = MF_Fy_f_D(t59);
    real_type t79  = t55 * t78;
    real_type t80  = t3 * t3;
    real_type t81  = 1.0 / t80;
    real_type t84  = t64 * t63 * t53;
    real_type t86  = MF_Fy_r_D(t70);
    real_type t87  = t69 * t86;
    result__[ 11  ] = t19 * t12 * (-t63 * t67 * t81 * t87 - t84 * t81 * t79) - t29 * t76;
    real_type t95  = t56 * t78;
    real_type t97  = t56 * t86;
    result__[ 12  ] = t19 * t12 * (-t63 * t67 * t97 + t84 * t95) + t33 * t76;
    real_type t104 = t53 * t53;
    real_type t106 = t63 * t56;
    real_type t109 = t67 * t67;
    result__[ 13  ] = t19 * t12 * (t64 * t106 * t104 * t78 + t106 * t109 * t86);
    real_type t114 = t53 * t78;
    real_type t116 = sin(t58);
    result__[ 14  ] = t19 * t12 * (t116 * t63 * t61 - t65 * t114);
    real_type t122 = t116 * t60;
    real_type t124 = ModelPars[24];
    real_type t126 = ModelPars[0];
    real_type t129 = 1.0 / t124;
    real_type t130 = t129 * (t124 * t6 * t40 - t80 * t126 - t122 + X__[6]);
    real_type t131 = t14 * t12;
    result__[ 15  ] = -t131 * t130;
    real_type t133 = t19 * t23;
    real_type t134 = t8 * t133;
    result__[ 16  ] = t134 * t130;
    real_type t142 = t5 * t133;
    result__[ 17  ] = -result__[8] * t129 * (-t116 * t81 * t79 - 2 * t3 * t126) + t142 * t130;
    real_type t145 = t124 * t40;
    real_type t149 = t2 * t133;
    result__[ 18  ] = -result__[8] * t129 * (t116 * t95 + t145) - t149 * t130;
    result__[ 19  ] = -result__[8] * t129 * (t116 * t56 * t114 + t124 * t6);
    real_type t158 = t64 * t60;
    result__[ 20  ] = -result__[8] * t129 * (-t116 * t78 - t158);
    result__[ 21  ] = -t19 * t12 * t129;
    real_type t167 = t129 * (-t124 * t3 * t40 + t158 + t71);
    result__[ 22  ] = -t131 * t167;
    result__[ 23  ] = t134 * t167;
    result__[ 24  ] = -result__[8] * t129 * (t64 * t81 * t79 - t81 * t87 - t145) + t142 * t167;
    result__[ 25  ] = -result__[8] * t129 * (-t64 * t95 - t97) - t149 * t167;
    result__[ 26  ] = -result__[8] * t129 * (-t64 * t56 * t114 + t56 * t67 * t86 - t124 * t3);
    result__[ 27  ] = -result__[8] * t129 * (t64 * t78 - t122);
    real_type t193 = U__[1];
    result__[ 28  ] = -t14 * t12 * t193;
    real_type t196 = t23 * t193;
    result__[ 29  ] = t45 * t196;
    result__[ 30  ] = t29 * t196;
    result__[ 31  ] = -t33 * t196;
    real_type t198 = U__[0];
    result__[ 32  ] = -t14 * t12 * t198;
    real_type t201 = t23 * t198;
    result__[ 33  ] = t45 * t201;
    result__[ 34  ] = t29 * t201;
    result__[ 35  ] = -t33 * t201;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Drhs_odeDxp_sparse",36);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::Drhs_odeDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby::Drhs_odeDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::Drhs_odeDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::Drhs_odeDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::Drhs_odeDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::Drhs_odeDu_numRows() const
  { return 7; }

  integer
  OCP_eRumby::Drhs_odeDu_numCols() const
  { return 2; }

  integer
  OCP_eRumby::Drhs_odeDu_nnz() const
  { return 2; }

  void
  OCP_eRumby::Drhs_odeDu_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 5   ; jIndex[ 0  ] = 1   ;
    iIndex[ 1  ] = 6   ; jIndex[ 1  ] = 0   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::Drhs_odeDu_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = cos(t1);
    real_type t5   = sin(t1);
    result__[ 0   ] = -(Q__[1] * X__[0] - 1) / (X__[2] * t2 - X__[3] * t5);
    result__[ 1   ] = result__[0];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Drhs_odeDu_sparse",2);
    #endif
  }

  /*\
   |   __  __              __  __      _       _
   |  |  \/  |__ _ ______ |  \/  |__ _| |_ _ _(_)_ __
   |  | |\/| / _` (_-<_-< | |\/| / _` |  _| '_| \ \ /
   |  |_|  |_\__,_/__/__/ |_|  |_\__,_|\__|_| |_/_\_\
  \*/

  integer
  OCP_eRumby::A_numRows() const
  { return 7; }

  integer
  OCP_eRumby::A_numCols() const
  { return 7; }

  integer
  OCP_eRumby::A_nnz() const
  { return 7; }

  void
  OCP_eRumby::A_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 1   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 2   ; jIndex[ 2  ] = 4   ;
    iIndex[ 3  ] = 3   ; jIndex[ 3  ] = 2   ;
    iIndex[ 4  ] = 4   ; jIndex[ 4  ] = 3   ;
    iIndex[ 5  ] = 5   ; jIndex[ 5  ] = 5   ;
    iIndex[ 6  ] = 6   ; jIndex[ 6  ] = 6   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::A_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = 1;
    result__[ 1   ] = -1;
    result__[ 2   ] = 1;
    result__[ 3   ] = 1;
    result__[ 4   ] = 1;
    result__[ 5   ] = 1;
    result__[ 6   ] = 1;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"A_sparse",7);
    #endif
  }

}

// EOF: OCP_eRumby_Methods.cc
