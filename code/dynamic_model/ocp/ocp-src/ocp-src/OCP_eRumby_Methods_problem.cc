/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_Methods1.cc                                         |
 |                                                                       |
 |  version: 1.0   date 28/11/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby.hh"
#include "OCP_eRumby_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_rightWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_R")
#define ALIAS_rightWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_R")
#define ALIAS_rightWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_R")
#define ALIAS_leftWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_L")
#define ALIAS_leftWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_L")
#define ALIAS_leftWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_L")
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_FxLimit_DD(__t1) FxLimit.DD( __t1)
#define ALIAS_FxLimit_D(__t1) FxLimit.D( __t1)
#define ALIAS_deltaLimit_DD(__t1) deltaLimit.DD( __t1)
#define ALIAS_deltaLimit_D(__t1) deltaLimit.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_speed_limit_DD(__t1) speed_limit.DD( __t1)
#define ALIAS_speed_limit_D(__t1) speed_limit.D( __t1)
#define ALIAS_Fx__r__OControl_D_3(__t1, __t2, __t3) Fx__r__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2(__t1, __t2, __t3) Fx__r__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1(__t1, __t2, __t3) Fx__r__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_3_3(__t1, __t2, __t3) Fx__r__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_3(__t1, __t2, __t3) Fx__r__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_2(__t1, __t2, __t3) Fx__r__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_3(__t1, __t2, __t3) Fx__r__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_2(__t1, __t2, __t3) Fx__r__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_1(__t1, __t2, __t3) Fx__r__OControl.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3(__t1, __t2, __t3) delta__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2(__t1, __t2, __t3) delta__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1(__t1, __t2, __t3) delta__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3_3(__t1, __t2, __t3) delta__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_3(__t1, __t2, __t3) delta__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_2(__t1, __t2, __t3) delta__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_3(__t1, __t2, __t3) delta__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_2(__t1, __t2, __t3) delta__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_1(__t1, __t2, __t3) delta__OControl.D_1_1( __t1, __t2, __t3)


namespace OCP_eRumbyDefine {

  /*\
   |  _   _               ___             _   _
   | | | | |___ ___ _ _  | __|  _ _ _  __| |_(_)___ _ _  ___
   | | |_| (_-</ -_) '_| | _| || | ' \/ _|  _| / _ \ ' \(_-<
   |  \___//__/\___|_|   |_| \_,_|_||_\__|\__|_\___/_||_/__/
  \*/
  // user defined functions which has a body defined in MAPLE
  real_type
  OCP_eRumby::MF_Fy_r( real_type alpha__XO ) const {
    real_type t2   = ModelPars[4];
    real_type t4   = ModelPars[2];
    real_type t11  = atan(alpha__XO / t2 / t4 * ModelPars[9]);
    real_type t13  = sin(t11 * t4);
    return t13 * t2 * ModelPars[6];
  }

  real_type
  OCP_eRumby::MF_Fy_r_D( real_type alpha__XO ) const {
    real_type t2   = ModelPars[9];
    real_type t4   = ModelPars[2];
    real_type t7   = ModelPars[4];
    real_type t11  = atan(alpha__XO / t7 / t4 * t2);
    real_type t13  = cos(t11 * t4);
    real_type t15  = alpha__XO * alpha__XO;
    real_type t16  = t2 * t2;
    real_type t18  = t4 * t4;
    real_type t19  = t7 * t7;
    return t19 * t18 / (t16 * t15 + t19 * t18) * t13 * t2 * ModelPars[6];
  }

  real_type
  OCP_eRumby::MF_Fy_r_DD( real_type alpha__XO ) const {
    real_type t1   = ModelPars[2];
    real_type t2   = t1 * t1;
    real_type t3   = ModelPars[4];
    real_type t4   = t3 * t3;
    real_type t5   = t4 * t2;
    real_type t8   = ModelPars[9];
    real_type t9   = t8 * t8;
    real_type t16  = atan(alpha__XO / t3 / t1 * t8);
    real_type t17  = t16 * t1;
    real_type t18  = sin(t17);
    real_type t20  = cos(t17);
    real_type t26  = alpha__XO * alpha__XO;
    real_type t29  = pow(t9 * t26 + t5, 2);
    return -1.0 / t29 * (t18 * t3 * t2 + 2 * alpha__XO * t20 * t8) * t9 * ModelPars[6] * t5;
  }

  real_type
  OCP_eRumby::MF_Fy_f( real_type alpha__XO ) const {
    real_type t2   = ModelPars[3];
    real_type t4   = ModelPars[1];
    real_type t11  = atan(alpha__XO / t2 / t4 * ModelPars[8]);
    real_type t13  = sin(t11 * t4);
    return t13 * t2 * ModelPars[5];
  }

  real_type
  OCP_eRumby::MF_Fy_f_D( real_type alpha__XO ) const {
    real_type t2   = ModelPars[8];
    real_type t4   = ModelPars[1];
    real_type t7   = ModelPars[3];
    real_type t11  = atan(alpha__XO / t7 / t4 * t2);
    real_type t13  = cos(t11 * t4);
    real_type t15  = alpha__XO * alpha__XO;
    real_type t16  = t2 * t2;
    real_type t18  = t4 * t4;
    real_type t19  = t7 * t7;
    return t19 * t18 / (t16 * t15 + t19 * t18) * t13 * t2 * ModelPars[5];
  }

  real_type
  OCP_eRumby::MF_Fy_f_DD( real_type alpha__XO ) const {
    real_type t1   = ModelPars[1];
    real_type t2   = t1 * t1;
    real_type t3   = ModelPars[3];
    real_type t4   = t3 * t3;
    real_type t5   = t4 * t2;
    real_type t8   = ModelPars[8];
    real_type t9   = t8 * t8;
    real_type t16  = atan(alpha__XO / t3 / t1 * t8);
    real_type t17  = t16 * t1;
    real_type t18  = sin(t17);
    real_type t20  = cos(t17);
    real_type t26  = alpha__XO * alpha__XO;
    real_type t29  = pow(t9 * t26 + t5, 2);
    return -1.0 / t29 * (t18 * t3 * t2 + 2 * alpha__XO * t20 * t8) * t9 * ModelPars[5] * t5;
  }


  /*\
   |  _  _            _ _ _            _
   | | || |__ _ _ __ (_) | |_ ___ _ _ (_)__ _ _ _
   | | __ / _` | '  \| | |  _/ _ \ ' \| / _` | ' \
   | |_||_\__,_|_|_|_|_|_|\__\___/_||_|_\__,_|_||_|
   |
  \*/

  real_type
  OCP_eRumby::H_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = cos(t1);
    real_type t3   = X__[2];
    real_type t5   = sin(t1);
    real_type t6   = X__[3];
    real_type t8   = t3 * t2 - t6 * t5;
    real_type t9   = 1.0 / t8;
    real_type t10  = Q__[1];
    real_type t11  = X__[0];
    real_type t13  = t11 * t10 - 1;
    real_type t14  = t13 * t9;
    real_type t15  = speed_limit(t3);
    real_type t17  = ModelPars[49];
    real_type t18  = Q__[0];
    real_type t19  = ALIAS_rightWidth(t18);
    real_type t21  = roadRightLateralBoundaries(t11 - t17 + t19);
    real_type t23  = ALIAS_leftWidth(t18);
    real_type t25  = roadLeftLateralBoundaries(t23 - t11 - t17);
    real_type t27  = X__[5];
    real_type t28  = deltaLimit(t27);
    real_type t30  = X__[6];
    real_type t31  = FxLimit(t30);
    real_type t34  = ModelPars[21];
    real_type t37  = t11 * t11;
    real_type t40  = ModelPars[27] * ModelPars[27];
    real_type t56  = X__[4];
    real_type t61  = ModelPars[11];
    real_type t64  = 1.0 / t3;
    real_type t67  = MF_Fy_f(-t64 * (t61 * t56 + t6) + t27);
    real_type t70  = 1.0 / ModelPars[7];
    real_type t71  = cos(t27);
    real_type t74  = ModelPars[12];
    real_type t78  = MF_Fy_r(t64 * (t74 * t56 - t6));
    real_type t85  = sin(t27);
    real_type t88  = ModelPars[24];
    real_type t91  = t3 * t3;
    real_type t97  = t13 * t9 / t88;
    return -t15 * t14 - t21 * t14 - t25 * t14 - t28 * t14 - t31 * t14 - t13 * t9 * (t34 * ModelPars[32] + 1.0 / t40 * t37 * (1 - t34)) + t14 * (-t6 * t2 - t3 * t5) * L__[0] + t14 * (1.0 / t13 * t8 * t10 + t56) * L__[1] + t14 * (-t71 * t70 * t61 * t67 + t70 * t74 * t78) * L__[2] - t97 * (t88 * t6 * t56 - t85 * t67 - t91 * ModelPars[0] + t30) * L__[3] - t97 * (-t88 * t3 * t56 + t71 * t67 + t78) * L__[4] - t14 * L__[5] * U__[1] - t14 * L__[6] * U__[0];
  }

  /*\
   |   ___               _ _   _
   |  | _ \___ _ _  __ _| | |_(_)___ ___
   |  |  _/ -_) ' \/ _` | |  _| / -_|_-<
   |  |_| \___|_||_\__,_|_|\__|_\___/__/
  \*/

  real_type
  OCP_eRumby::penalties_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = cos(t1);
    real_type t3   = X__[2];
    real_type t5   = sin(t1);
    real_type t11  = X__[0];
    real_type t14  = (t11 * Q__[1] - 1) / (t3 * t2 - X__[3] * t5);
    real_type t15  = speed_limit(t3);
    real_type t17  = ModelPars[49];
    real_type t18  = Q__[0];
    real_type t19  = ALIAS_rightWidth(t18);
    real_type t21  = roadRightLateralBoundaries(t11 - t17 + t19);
    real_type t23  = ALIAS_leftWidth(t18);
    real_type t25  = roadLeftLateralBoundaries(t23 - t11 - t17);
    real_type t28  = deltaLimit(X__[5]);
    real_type t31  = FxLimit(X__[6]);
    return -t15 * t14 - t21 * t14 - t25 * t14 - t28 * t14 - t31 * t14;
  }

  real_type
  OCP_eRumby::control_penalties_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = cos(t1);
    real_type t5   = sin(t1);
    real_type t14  = (Q__[1] * X__[0] - 1) / (X__[2] * t2 - X__[3] * t5);
    real_type t16  = ModelPars[47];
    real_type t17  = delta__OControl(U__[1], -t16, t16);
    real_type t20  = ModelPars[48];
    real_type t21  = Fx__r__OControl(U__[0], -t20, t20);
    return -t17 * t14 - t21 * t14;
  }

  /*\
   |   _
   |  | |   __ _ __ _ _ _ __ _ _ _  __ _ ___
   |  | |__/ _` / _` | '_/ _` | ' \/ _` / -_)
   |  |____\__,_\__, |_| \__,_|_||_\__, \___|
   |            |___/              |___/
  \*/

  real_type
  OCP_eRumby::lagrange_target(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t2   = ModelPars[21];
    real_type t5   = X__[0];
    real_type t6   = t5 * t5;
    real_type t9   = ModelPars[27] * ModelPars[27];
    real_type t13  = X__[1];
    real_type t14  = cos(t13);
    real_type t17  = sin(t13);
    return -(t5 * Q__[1] - 1) / (X__[2] * t14 - X__[3] * t17) * (t2 * ModelPars[32] + 1.0 / t9 * t6 * (1 - t2));
  }

  /*\
   |   __  __
   |  |  \/  |__ _ _  _ ___ _ _
   |  | |\/| / _` | || / -_) '_|
   |  |_|  |_\__,_|\_, \___|_|
   |               |__/
  \*/

  real_type
  OCP_eRumby::mayer_target(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    real_type t6   = pow(XL__[2] - ModelPars[29], 2);
    real_type t12  = pow(XL__[3] - ModelPars[31], 2);
    real_type t18  = pow(XL__[0] - ModelPars[26], 2);
    real_type t24  = pow(XL__[1] - ModelPars[34], 2);
    real_type t30  = pow(XL__[5] - ModelPars[46], 2);
    real_type t36  = pow(XL__[4] - ModelPars[36], 2);
    real_type t42  = pow(XL__[6] - ModelPars[22], 2);
    real_type t51  = pow(XR__[2] - ModelPars[28], 2);
    real_type t57  = pow(XR__[3] - ModelPars[30], 2);
    real_type t63  = pow(XR__[0] - ModelPars[25], 2);
    real_type t69  = pow(XR__[1] - ModelPars[33], 2);
    real_type t75  = pow(XR__[5] - ModelPars[45], 2);
    real_type t81  = pow(XR__[4] - ModelPars[35], 2);
    return (t12 * ModelPars[20] + t18 * ModelPars[18] + t24 * ModelPars[44] + t30 * ModelPars[42] + t36 * ModelPars[41] + t42 * ModelPars[43] + t6 * ModelPars[19]) * ModelPars[17] + (t51 * ModelPars[15] + t57 * ModelPars[16] + t63 * ModelPars[14] + t69 * ModelPars[40] + t75 * ModelPars[39] + t81 * ModelPars[38]) * ModelPars[13];
  }

  /*\
   |    ___
   |   / _ \
   |  | (_) |
   |   \__\_\
  \*/

  integer
  OCP_eRumby::q_numEqns() const
  { return 5; }

  void
  OCP_eRumby::q_eval(
    integer        i_node,
    integer        i_segment,
    real_type      s,
    Q_pointer_type result__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = s;
    result__[ 1   ] = ALIAS_Curv(s);
    result__[ 2   ] = ALIAS_xLane(s);
    result__[ 3   ] = ALIAS_yLane(s);
    result__[ 4   ] = ALIAS_theta(s);
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__.pointer(),"q_eval",5);
    #endif
  }

  /*\
   |    ___
   |   / __|_  _ ___ ______
   |  | (_ | || / -_|_-<_-<
   |   \___|\_,_\___/__/__/
  \*/

  integer
  OCP_eRumby::u_guess_numEqns() const
  { return 2; }

  void
  OCP_eRumby::u_guess_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    U_pointer_type       U__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    std::fill_n( U__.pointer(), 2, 0 );
    U__[ iU_Fx__r__O ] = 0;
    U__[ iU_delta__O ] = 0;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(U__.pointer(),"u_guess_eval",2);
    #endif
  }

  /*\
   |    ___ _           _
   |   / __| |_  ___ __| |__
   |  | (__| ' \/ -_) _| / /
   |   \___|_||_\___\__|_\_\
  \*/

  void
  OCP_eRumby::u_check_if_admissible(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    try {
      real_type t2   = ModelPars[48];
      Fx__r__OControl.check_range(U__[0], -t2, t2);
      real_type t4   = ModelPars[47];
      delta__OControl.check_range(U__[1], -t4, t4);
    } catch ( exception const & exc ) {
      MECHATRONIX_DO_ERROR("OCP_eRumby::u_check_if_admissible: " << exc.what());
    }
  }

  /*\
   |   ___        _     ___                       _
   |  | _ \___ __| |_  | _ \_ _ ___  __ ___ _____(_)_ _  __ _
   |  |  _/ _ (_-<  _| |  _/ '_/ _ \/ _/ -_|_-<_-< | ' \/ _` |
   |  |_| \___/__/\__| |_| |_| \___/\__\___/__/__/_|_||_\__, |
   |                                                    |___/
  \*/

  integer
  OCP_eRumby::post_numEqns() const
  { return 30; }

  void
  OCP_eRumby::post_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t2   = ModelPars[47];
    result__[ 0   ] = delta__OControl(U__[1], -t2, t2);
    real_type t4   = ModelPars[48];
    result__[ 1   ] = Fx__r__OControl(U__[0], -t4, t4);
    real_type t5   = X__[2];
    result__[ 2   ] = speed_limit(t5);
    real_type t6   = X__[0];
    real_type t7   = ModelPars[49];
    real_type t8   = Q__[0];
    real_type t9   = ALIAS_rightWidth(t8);
    result__[ 3   ] = roadRightLateralBoundaries(t6 - t7 + t9);
    real_type t11  = ALIAS_leftWidth(t8);
    result__[ 4   ] = roadLeftLateralBoundaries(t11 - t6 - t7);
    real_type t13  = X__[5];
    result__[ 5   ] = deltaLimit(t13);
    real_type t14  = X__[6];
    result__[ 6   ] = FxLimit(t14);
    real_type t15  = X__[4];
    result__[ 7   ] = t5 * t15;
    real_type t16  = X__[1];
    real_type t17  = cos(t16);
    real_type t19  = sin(t16);
    real_type t20  = X__[3];
    result__[ 8   ] = -1.0 / (t6 * Q__[1] - 1) * (t5 * t17 - t20 * t19);
    real_type t28  = Q__[2];
    real_type t29  = Q__[4];
    real_type t30  = t29 + t16;
    real_type t31  = sin(t30);
    result__[ 9   ] = -t6 * t31 + t28;
    real_type t33  = Q__[3];
    real_type t34  = cos(t30);
    result__[ 10  ] = t6 * t34 + t33;
    real_type t36  = t6 + t7;
    result__[ 11  ] = -t36 * t31 + t28;
    result__[ 12  ] = t36 * t34 + t33;
    real_type t39  = t6 - t7;
    result__[ 13  ] = -t39 * t31 + t28;
    result__[ 14  ] = t39 * t34 + t33;
    real_type t42  = sin(t29);
    result__[ 15  ] = -t11 * t42 + t28;
    real_type t44  = cos(t29);
    result__[ 16  ] = t11 * t44 + t33;
    result__[ 17  ] = t9 * t42 + t28;
    result__[ 18  ] = -t9 * t44 + t33;
    real_type t48  = 1.0 / t5;
    result__[ 19  ] = atan(t48 * t20);
    real_type t51  = t5 * t5;
    real_type t55  = ModelPars[11] * t15;
    real_type t58  = MF_Fy_f(-t48 * (-t5 * t13 + t20 + t55));
    real_type t59  = sin(t13);
    real_type t61  = t51 * ModelPars[0] + t59 * t58 - t14;
    result__[ 20  ] = -1.0 / ModelPars[24] * t61;
    result__[ 21  ] = -1.0 / ModelPars[10] * ModelPars[23] * t61;
    real_type t72  = -t48 * (t55 + t20) + t13;
    result__[ 22  ] = MF_Fy_f(t72);
    real_type t76  = t48 * (ModelPars[12] * t15 - t20);
    result__[ 23  ] = MF_Fy_r(t76);
    result__[ 24  ] = t76;
    result__[ 25  ] = t72;
    real_type t78  = 1.0 / ModelPars[6];
    result__[ 26  ] = t78 * result__[23];
    result__[ 27  ] = 1.0 / ModelPars[5] * result__[22];
    result__[ 28  ] = t78 * t14;
    real_type t85  = pow(t5 - ModelPars[29], 2);
    real_type t90  = pow(t20 - ModelPars[31], 2);
    real_type t95  = pow(t6 - ModelPars[26], 2);
    real_type t100 = pow(t16 - ModelPars[34], 2);
    real_type t105 = pow(t13 - ModelPars[46], 2);
    real_type t110 = pow(t15 - ModelPars[36], 2);
    real_type t115 = pow(t14 - ModelPars[22], 2);
    real_type t123 = pow(t5 - ModelPars[28], 2);
    real_type t128 = pow(t20 - ModelPars[30], 2);
    real_type t133 = pow(t6 - ModelPars[25], 2);
    real_type t138 = pow(t16 - ModelPars[33], 2);
    real_type t143 = pow(t13 - ModelPars[45], 2);
    real_type t148 = pow(t15 - ModelPars[35], 2);
    result__[ 29  ] = (t100 * ModelPars[44] + t105 * ModelPars[42] + t110 * ModelPars[41] + t115 * ModelPars[43] + t85 * ModelPars[19] + t90 * ModelPars[20] + t95 * ModelPars[18]) * ModelPars[17] + (t123 * ModelPars[15] + t128 * ModelPars[16] + t133 * ModelPars[14] + t138 * ModelPars[40] + t143 * ModelPars[39] + t148 * ModelPars[38]) * ModelPars[13];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"post_eval",30);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::integrated_post_numEqns() const
  { return 1; }

  void
  OCP_eRumby::integrated_post_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = cos(t1);
    real_type t5   = sin(t1);
    result__[ 0   ] = -(Q__[1] * X__[0] - 1) / (X__[2] * t2 - X__[3] * t5);
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"integrated_post_eval",1);
    #endif
  }

}

// EOF: OCP_eRumby_Methods1.cc
