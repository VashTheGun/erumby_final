/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_Pars.hh                                             |
 |                                                                       |
 |  version: 1.0   date 28/11/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#ifndef OCP_ERUMBYPARS_HH
#define OCP_ERUMBYPARS_HH

#define numBc                    0
#define numModelPars             50
#define numConstraint1D          5
#define numConstraint2D          0
#define numConstraintU           2
#define numXvars                 7
#define numLvars                 7
#define numUvars                 2
#define numOMEGAvars             0
#define numQvars                 5
#define numPvars                 0
#define numPostProcess           30
#define numIntegratedPostProcess 1
#define numContinuationSteps     0

// Xvars
#define iX_n             0
#define iX_xi            1
#define iX_u             2
#define iX_v             3
#define iX_Omega         4
#define iX_delta         5
#define iX_Fx__r         6

// Lvars
#define iL_lambda1__xo   0
#define iL_lambda2__xo   1
#define iL_lambda3__xo   2
#define iL_lambda4__xo   3
#define iL_lambda5__xo   4
#define iL_lambda6__xo   5
#define iL_lambda7__xo   6

// Uvars
#define iU_Fx__r__O      0
#define iU_delta__O      1

// Qvars
#define iQ_zeta          0
#define iQ_Curv          1
#define iQ_xLane         2
#define iQ_yLane         3
#define iQ_theta         4

// Pvars

// ModelPars Maps
#define iM_Ca            0
#define iM_Cf            1
#define iM_Cr            2
#define iM_Df            3
#define iM_Dr            4
#define iM_Fz__f0        5
#define iM_Fz__r0        6
#define iM_Izz           7
#define iM_Kf            8
#define iM_Kr            9
#define iM_L             10
#define iM_Lf            11
#define iM_Lr            12
#define iM_WBCF          13
#define iM_WBCF__n       14
#define iM_WBCF__u       15
#define iM_WBCF__v       16
#define iM_WBCI          17
#define iM_WBCI__n       18
#define iM_WBCI__u       19
#define iM_WBCI__v       20
#define iM_alpha         21
#define iM_fx__i         22
#define iM_h             23
#define iM_m             24
#define iM_n__f          25
#define iM_n__i          26
#define iM_n__max        27
#define iM_u__f          28
#define iM_u__i          29
#define iM_v__f          30
#define iM_v__i          31
#define iM_wT            32
#define iM_xi__f         33
#define iM_xi__i         34
#define iM_Omega__f      35
#define iM_Omega__i      36
#define iM_V_target      37
#define iM_WBCF__Omega   38
#define iM_WBCF__delta   39
#define iM_WBCF__xi      40
#define iM_WBCI__Omega   41
#define iM_WBCI__delta   42
#define iM_WBCI__fx      43
#define iM_WBCI__xi      44
#define iM_delta__f      45
#define iM_delta__i      46
#define iM_delta_dot_max 47
#define iM_jerk_x_max    48
#define iM_vehHalfWidth  49

#endif

// EOF: OCP_eRumby_Pars.hh
