%% main dyn model initialize

clear all; 
close all; 
clc; 

addpath('./OCP-lib');

%% ocp problem
ocp_MLT = OCP_eRumby_MLT( 'OCP_eRumby_MLT' );
ocp_MLT.setup('./OCP-main/ocp-eRumby_MLT/data/OCP_eRumby_MLT_Data.rb');
ocp_MLT.infoLevel(0);
ocp_MLT.set_guess(); % use default guess generated in MAPLE

data_ocp = ocp_MLT.get_ocp_data(); % retrieve ocp-problem data
% -------------------------------------------------------------------------
% COMPUTE SOLUTION
% -------------------------------------------------------------------------
converged = ocp_MLT.solve();

plot(ocp_MLT.solution('xCoMCar'), ocp_MLT.solution('yCoMCar'))


%%
addpath(genpath('./../../../../../lib/matlab/Clothoids'));
% c++ convention
M_PI = pi;
 
 % track parameters
x = [0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0];
y = [0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0];
theta = [0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI, M_PI, 0];

S = ClothoidList() ;

for i = 1:length(x)-1
    S.push_back_G1(x(i), y(i), theta(i), x(i+1), y(i+1), theta(i+1) ) ; % track creation
end

clear x y theta M_PI
%% 
ds      = 0.01;
LH_max = round(S.length);
horizon = [0:ds:LH_max];       

% Compute the horizon
start_zeta = 0;

horizon_shift = horizon + start_zeta;

[x_r,y_r,theta_r,curv_r] = S.evaluate(horizon_shift); % extract data 

data_ocp  = ocp_MLT.get_ocp_data(); % retrieve ocp-problem data

data_ocp.Trajectory.abscissa                = horizon; %: 
data_ocp.Trajectory.abscissa_step           = ds;
data_ocp.Trajectory.curvature               = curv_r; %: 
data_ocp.Trajectory.mesh.segments{1}.length = 1;      
data_ocp.Trajectory.mesh.segments{1}.n      = LH_max/ds;    %length(horizon);
data_ocp.Trajectory.theta0                  = theta_r(1); %
data_ocp.Trajectory.x0                      = x_r(1);   %
data_ocp.Trajectory.y0                      = y_r(1);   %  


% Boundary condition data -----------------------------------------------
% Change boundary conditions
% initial 
bcs.initial.u           = 1;
bcs.initial.v           = 0;
bcs.initial.n           = 0;
bcs.initial.xi          = 0;
bcs.initial.delta       = 0;
bcs.initial.omega__z    = 0;
bcs.initial.fx          = 0;

% 
% % final
bcs.final.u           = 0;%30/3.6;
% bcs.final.v           = 0;
bcs.final.n           = 0;
bcs.final.xi          = 0;
% bcs.final.delta       = 0;
% bcs.final.omega__z    = 0;



data_ocp = create_boundary_conditions(data_ocp,bcs);


ocp_MLT.setup(data_ocp);
ocp_MLT.infoLevel(0);
ocp_MLT.set_guess();

% solve ocp solution

ok = ocp_MLT.solve();

road.xRoadLeft       = ocp_MLT.solution('xLeftEdge');
road.yRoadLeft       = ocp_MLT.solution('yLeftEdge');
road.xRoadRight      = ocp_MLT.solution('xRightEdge');
road.yRoadRight      = ocp_MLT.solution('yRightEdge');

fake_veh.x      = ocp_MLT.solution('xLane');
fake_veh.y      = ocp_MLT.solution('yLane');
fake_veh.theta  = ocp_MLT.solution('theta');
fake_veh.u      = ocp_MLT.solution('u') - 0.5;
fake_veh.v      = ocp_MLT.solution('v');

MLT.x_ocp       = ocp_MLT.solution('xCoMCar');
MLT.y_ocp       = ocp_MLT.solution('yCoMCar');

MLT.abscissa        = ocp_MLT.solution('zeta');
MLT.delta           = ocp_MLT.solution('delta__O');
MLT.Omega           = ocp_MLT.solution('Omega');
MLT.u               = ocp_MLT.solution('u');
MLT.xi              = ocp_MLT.solution('xi');
MLT.n               = ocp_MLT.solution('n');

        

clearvars -except ocp_MLT S MLT fake_veh road
%%
%% ocp problem
ocp_MHT = OCP_eRumby_MHT( 'OCP_eRumby_MHT' );
ocp_MHT.setup('./OCP-main/ocp-eRumby_MHT/data/OCP_eRumby_MHT_Data.rb');
ocp_MHT.infoLevel(0);
ocp_MHT.set_guess(); % use default guess generated in MAPLE

data_ocp = ocp_MHT.get_ocp_data(); % retrieve ocp-problem data
% -------------------------------------------------------------------------
% COMPUTE SOLUTION
% -------------------------------------------------------------------------
converged = ocp_MHT.solve();

%%
LH_max        = 9;  % Lunghezza horizon
guess_ocp_old = [];
ok_old        = 0;
j             = 0;


h = figure(1)
hold on

plot(road.xRoadLeft, road.yRoadLeft, 'k')
plot(road.xRoadRight, road.yRoadRight, 'k')
plot(MLT.x_ocp, MLT.y_ocp)
mht = plot(MLT.x_ocp, MLT.y_ocp)

for i = 1:length(fake_veh.x)
        % trovo la posizione attuale in c.curv. su S
    tic
    [t, n] = S.find_coord(fake_veh.x (i), fake_veh.y(i));
    [~,~,tmp,~] = S.evaluate(t);
    % init condizioni iniziali
    in_cond.u       = fake_veh.u(i);
    in_cond.v       = fake_veh.v(i);
    in_cond.theta   = tmp;
    in_cond.n       = -n;
    in_cond.delta   = interp1(MLT.abscissa,MLT.delta,t);
    in_cond.Omega   = interp1(MLT.abscissa,MLT.Omega,t);
    % init condizioni finali
    fin_cond.u      = interp1(MLT.abscissa,MLT.u,t+LH_max);
    fin_cond.n      = interp1(MLT.abscissa,MLT.n,t+LH_max);
    fin_cond.xi     = interp1(MLT.abscissa,MLT.xi,t+LH_max);
    
    % ocp replanning
    if( mod(i,10) == 0 || i == 1)
        [ok, x_rep, y_rep, delta, u, horizon_shift, guess_ocp, n_iter] = MHT_erumby(S, ocp_MHT,in_cond , fin_cond, t, LH_max, guess_ocp_old, ok_old);
        j = j +1;
    end
    % save results
    MHT.x(:,j) = x_rep;
    MHT.y(:,j) = y_rep;
    MHT.delta(:,j) = delta;
    MHT.u(:,j) = u;
    MHT.converged(j) = ok; 
    MHT.abscissa(:,j) = horizon_shift;
    MHT.iterations(j) = n_iter;
    ok_old = ok;
    guess_ocp_old = guess_ocp;
    elaps_time(j) = toc;
    

    set(mht,'XData', MHT.x(:,j), 'YData', MHT.y(:,j))
    drawnow
    a = 1+1;
    

        
end

%%

figure(1)
hold on

plot(road.xRoadLeft, road.yRoadLeft, 'k')
plot(road.xRoadRight, road.yRoadRight, 'k')
plot(MLT.x_ocp, MLT.y_ocp)

for i = 1:size(MHT.x,2)
    plot(MHT.x(:,i), MHT.y(:,i))
%     pause()
end
%%

figure(2)
hold on

plot(MLT.abscissa, MLT.delta)
delta_plot = plot(MHT.abscissa(:,1), MHT.delta(:,1))
for i = 1:size(MHT.delta,2)
    set(delta_plot,'XData',MHT.abscissa(:,i), 'YData',MHT.delta(:,i))
    pause
end
