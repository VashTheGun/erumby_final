function [ok, x_ocp, y_ocp, delta, u, horizon_shift, guess_ocp, n_iter] = MHT_erumby(S, ocp, in_cond, fin_cond,t_in, LH_max, guess_ocp_old, ok_old) 

    % create the mesh
    ds      = 0.01;
    LH      = LH_max;
    horizon = [0:ds:LH];       

    % Compute the horizon
    start_zeta = t_in;

    horizon_shift = horizon + start_zeta;

    [x_r,y_r,theta_r,curv_r] = S.evaluate(horizon_shift); % extract data 

    xi = theta_r(1) - in_cond.theta;
    data_ocp  = ocp.get_ocp_data(); % retrieve ocp-problem data

    % set the new mesh
    data_ocp.Trajectory.abscissa                = horizon; %: 
    data_ocp.Trajectory.abscissa_step           = ds;
    data_ocp.Trajectory.curvature               = curv_r; %: 
    data_ocp.Trajectory.mesh.segments{1}.length = 1;      
    data_ocp.Trajectory.mesh.segments{1}.n      = LH/ds;    %length(horizon);
    data_ocp.Trajectory.theta0                  = theta_r(1); %
    data_ocp.Trajectory.x0                      = x_r(1);   %
    data_ocp.Trajectory.y0                      = y_r(1);   %
  


    % Boundary condition data -----------------------------------------------
    % Change boundary conditions
    % initial 
    bcs.initial.u           = in_cond.u;
    bcs.initial.v           = in_cond.v;
    bcs.initial.n           = -in_cond.n;
    bcs.initial.xi          = xi;
    bcs.initial.delta       = in_cond.delta;
    bcs.initial.omega__z    = in_cond.Omega;

    % final
    bcs.final.u           = fin_cond.u;
    bcs.final.n           = fin_cond.n;
    bcs.final.xi          = fin_cond.xi;


% 
% 
    data_ocp = create_boundary_conditions(data_ocp,bcs);
    data_ocp.Solver.max_iter = 100;


% 
    ocp.setup(data_ocp);
    ocp.infoLevel(0);
    
    if(isempty(guess_ocp_old) || ok_old == 0)
        ocp.set_guess();
    else
        ocp.set_guess(guess_ocp_old);
    end
   

    
    % solve ocp solution
    ok = ocp.solve();
    guess_ocp = ocp.get_solution_as_guess();
    if (ok == 0)
        a = 1+1;
        
    end

    x_ocp           = ocp.solution('xCoMCar');
    y_ocp           = ocp.solution('yCoMCar');
    n_iter          = ocp.solution.nonlinear_system_solver.iterations;
    delta           = ocp.solution('delta');
    u               = ocp.solution('u');


%     a = 1+1;
    
    
    
    
    
    
end