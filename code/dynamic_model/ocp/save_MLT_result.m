%%
clc;clear;

addpath(genpath('./../../../../../lib/matlab'));
ocp = importdata('./OCP_eRumby_OCP_result.txt');

%% 

zeta = getData(ocp,'zeta');
n = getData(ocp,'n');
u = getData(ocp,'u');


test = "MLT_data.txt";             % first file
fileID  = fopen(test, 'w');

fprintf(fileID, '% 8s\t % 8s\t % 8s\n', 'zeta', 'n', 'u');
fprintf(fileID, '% 3.5f\t % 3.5f\t % 3.5f\n', [zeta'; n'; u']);
fclose('all');