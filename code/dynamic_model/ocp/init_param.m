%% main dyn model initialize

clear all; 
close all; 
clc; 

addpath('./OCP-lib');

% set the plant parameter
c1 = 0.00118; 
c2 = 1.532e-05; 
a = 3.17;
tau = 0.08;
Kp = 0.02;
Ki = 0.03;



%% ocp problem
%% ocp problem
ocp_MLT = OCP_eRumby_MLT( 'OCP_eRumby_MLT' );
ocp_MLT.setup('./OCP-main/ocp-eRumby_MLT/data/OCP_eRumby_MLT_Data.rb');
ocp_MLT.infoLevel(0);
ocp_MLT.set_guess(); % use default guess generated in MAPLE

data_ocp = ocp_MLT.get_ocp_data(); % retrieve ocp-problem data
% -------------------------------------------------------------------------
% COMPUTE SOLUTION
% -------------------------------------------------------------------------
converged = ocp_MLT.solve();



%%
addpath(genpath('./../../../../../lib/matlab/Clothoids'));
% c++ convention
M_PI = pi;
 
 % track parameters
x = [0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0];
y = [0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0];
theta = [0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI, M_PI, 0];

S = ClothoidList() ;

for i = 1:length(x)-1
    S.push_back_G1(x(i), y(i), theta(i), x(i+1), y(i+1), theta(i+1) ) ; % track creation
end


%% 
ds      = 0.01;
LH_max = round(S.length);
% LH      = LH_max;
LH      = 9;
horizon = [0:ds:LH_max];       

% Compute the horizon
start_zeta = 0;

horizon_shift = horizon + start_zeta;

[x_r,y_r,theta_r,curv_r] = S.evaluate(horizon_shift); % extract data 

data_ocp  = ocp_MLT.get_ocp_data(); % retrieve ocp-problem data

data_ocp.Trajectory.abscissa                = horizon; %: 
data_ocp.Trajectory.abscissa_step           = ds;
data_ocp.Trajectory.curvature               = curv_r; %: 
data_ocp.Trajectory.mesh.segments{1}.length = 1;      
data_ocp.Trajectory.mesh.segments{1}.n      = LH_max/ds;    %length(horizon);
data_ocp.Trajectory.theta0                  = theta_r(1); %
data_ocp.Trajectory.x0                      = x_r(1);   %
data_ocp.Trajectory.y0                      = y_r(1);   %  


% Boundary condition data -----------------------------------------------
% Change boundary conditions
% initial 
bcs.initial.u           = 1;
bcs.initial.v           = 0;
bcs.initial.n           = 0;
bcs.initial.xi          = 0;
bcs.initial.delta       = 0;
bcs.initial.omega__z    = 0;
bcs.initial.fx          = 0;

% 
% % final
bcs.final.u           = 0;%30/3.6;
% bcs.final.v           = 0;
bcs.final.n           = 0;
bcs.final.xi          = 0;
% bcs.final.delta       = 0;
% bcs.final.omega__z    = 0;



data_ocp = create_boundary_conditions(data_ocp,bcs);
% data_ocp.Solver.max_iter = 50;


ocp_MLT.setup(data_ocp);
ocp_MLT.set_guess();

% solve ocp solution

ok = ocp_MLT.solve();

xRoadLeft = ocp_MLT.solution('xLeftEdge');
yRoadLeft = ocp_MLT.solution('yLeftEdge');
xRoadRight = ocp_MLT.solution('xRightEdge');
yRoadRight = ocp_MLT.solution('yRightEdge');
x_ocp = ocp_MLT.solution('xCoMCar');
y_ocp = ocp_MLT.solution('yCoMCar');

time        = ocp_MLT.solution('time');
abscissa    = ocp_MLT.solution('zeta');
delta           = ocp_MLT.solution('delta__O');
Omega           = ocp_MLT.solution('Omega');
u               = ocp_MLT.solution('u');

%%

figure(1)
plot(x_sim,y_sim)
hold on
plot(x_ocp, y_ocp)

plot(xRoadLeft, yRoadLeft,'k')
plot(xRoadRight, yRoadRight,'k')

 
% for i = 1:100:size(x_ocp_sim,3)
%     plot(x_ocp_sim(:,1,i), y_ocp_sim(:,1,i))
%     pause
% end
axis equal
%%
figure(2)
plot(tout,delta_sim)
hold on
plot(time,delta)
plot(tout,delta_ctr)
%%
figure(3)
plot(time, u)
hold on
plot(tout,u_sim)
