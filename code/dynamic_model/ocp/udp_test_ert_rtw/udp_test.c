/*
 * udp_test.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "udp_test".
 *
 * Model version              : 1.173
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C source code generated on : Wed Dec 18 18:32:15 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "udp_test.h"
#include "udp_test_private.h"

/* Block states (default storage) */
DW_udp_test_T udp_test_DW;

/* Real-time model */
RT_MODEL_udp_test_T udp_test_M_;
RT_MODEL_udp_test_T *const udp_test_M = &udp_test_M_;

/* Model step function */
void udp_test_step(void)
{
  char_T *sErr;

  /* Update for S-Function (sdspToNetwork): '<Root>/UDP Send' incorporates:
   *  Constant: '<Root>/Constant'
   */
  sErr = GetErrorBuffer(&udp_test_DW.UDPSend_NetworkLib[0U]);
  LibUpdate_Network(&udp_test_DW.UDPSend_NetworkLib[0U],
                    &udp_test_P.Constant_Value, 1);
  if (*sErr != 0) {
    rtmSetErrorStatus(udp_test_M, sErr);
    rtmSetStopRequested(udp_test_M, 1);
  }

  /* End of Update for S-Function (sdspToNetwork): '<Root>/UDP Send' */

  /* Matfile logging */
  rt_UpdateTXYLogVars(udp_test_M->rtwLogInfo, (&udp_test_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.01s, 0.0s] */
    if ((rtmGetTFinal(udp_test_M)!=-1) &&
        !((rtmGetTFinal(udp_test_M)-udp_test_M->Timing.taskTime0) >
          udp_test_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(udp_test_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  udp_test_M->Timing.taskTime0 =
    (++udp_test_M->Timing.clockTick0) * udp_test_M->Timing.stepSize0;
}

/* Model initialize function */
void udp_test_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)udp_test_M, 0,
                sizeof(RT_MODEL_udp_test_T));
  rtmSetTFinal(udp_test_M, 10.0);
  udp_test_M->Timing.stepSize0 = 0.01;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    udp_test_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(udp_test_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(udp_test_M->rtwLogInfo, (NULL));
    rtliSetLogT(udp_test_M->rtwLogInfo, "");
    rtliSetLogX(udp_test_M->rtwLogInfo, "");
    rtliSetLogXFinal(udp_test_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(udp_test_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(udp_test_M->rtwLogInfo, 4);
    rtliSetLogMaxRows(udp_test_M->rtwLogInfo, 0);
    rtliSetLogDecimation(udp_test_M->rtwLogInfo, 1);
    rtliSetLogY(udp_test_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(udp_test_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(udp_test_M->rtwLogInfo, (NULL));
  }

  /* states (dwork) */
  (void) memset((void *)&udp_test_DW, 0,
                sizeof(DW_udp_test_T));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(udp_test_M->rtwLogInfo, 0.0, rtmGetTFinal
    (udp_test_M), udp_test_M->Timing.stepSize0, (&rtmGetErrorStatus(udp_test_M)));

  {
    char_T *sErr;

    /* Start for S-Function (sdspToNetwork): '<Root>/UDP Send' */
    sErr = GetErrorBuffer(&udp_test_DW.UDPSend_NetworkLib[0U]);
    CreateUDPInterface(&udp_test_DW.UDPSend_NetworkLib[0U]);
    if (*sErr == 0) {
      LibCreate_Network(&udp_test_DW.UDPSend_NetworkLib[0U], 1,
                        "255.255.255.255", -1, "10.0.0.12",
                        udp_test_P.UDPSend_Port, 8192, 8, 0);
    }

    if (*sErr == 0) {
      LibStart(&udp_test_DW.UDPSend_NetworkLib[0U]);
    }

    if (*sErr != 0) {
      DestroyUDPInterface(&udp_test_DW.UDPSend_NetworkLib[0U]);
      if (*sErr != 0) {
        rtmSetErrorStatus(udp_test_M, sErr);
        rtmSetStopRequested(udp_test_M, 1);
      }
    }

    /* End of Start for S-Function (sdspToNetwork): '<Root>/UDP Send' */
  }
}

/* Model terminate function */
void udp_test_terminate(void)
{
  char_T *sErr;

  /* Terminate for S-Function (sdspToNetwork): '<Root>/UDP Send' */
  sErr = GetErrorBuffer(&udp_test_DW.UDPSend_NetworkLib[0U]);
  LibTerminate(&udp_test_DW.UDPSend_NetworkLib[0U]);
  if (*sErr != 0) {
    rtmSetErrorStatus(udp_test_M, sErr);
    rtmSetStopRequested(udp_test_M, 1);
  }

  LibDestroy(&udp_test_DW.UDPSend_NetworkLib[0U], 1);
  DestroyUDPInterface(&udp_test_DW.UDPSend_NetworkLib[0U]);

  /* End of Terminate for S-Function (sdspToNetwork): '<Root>/UDP Send' */
}
