/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: udp_test_data.c
 *
 * Code generated for Simulink model 'udp_test'.
 *
 * Model version                  : 1.173
 * Simulink Coder version         : 9.1 (R2019a) 23-Nov-2018
 * C/C++ source code generated on : Wed Dec 18 18:32:15 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "udp_test.h"
#include "udp_test_private.h"

/* Block parameters (default storage) */
P_udp_test_T udp_test_P = {
  /* Expression: 1
   * Referenced by: '<Root>/Constant'
   */
  1.0,

  /* Computed Parameter: UDPSend_Port
   * Referenced by: '<Root>/UDP Send'
   */
  35000
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
