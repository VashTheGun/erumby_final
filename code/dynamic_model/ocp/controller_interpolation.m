classdef (StrictDefaults)controller_interpolation < matlab.System & matlab.system.mixin.Propagates
%slexVarSizeMATLABSystemFindIfSysObj Find input elements that satisfy the given condition.
%     The condition is specified in the block dialog. Both outputs 
%     are variable-sized vectors.
%

%#codegen
% Copyright 2015 The MathWorks, Inc.

properties (Nontunable)
end

properties (Access = private)

    S = ClothoidList();

end

properties (Constant)
    % constant configuration method
    
end

  
  methods(Access=protected)
    
    function validatePropertiesImpl(obj)

    end

    function setupImpl(obj) % init fnct

        % -----------------------------------------------------------------------------
        % SET UP THE ROADRACE
        % -----------------------------------------------------------------------------                      
        % initial point
        % track parameters
        M_PI = pi;
        x = [0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0];
        y = [0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0];
        theta = [0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI, M_PI, 0];

        obj.S = ClothoidList() ;

        for i = 1:length(x)-1
            obj.S.push_back_G1(x(i), y(i), theta(i), x(i+1), y(i+1), theta(i+1) ) ; % track creation
        end        




    end

    function [delta, u] = stepImpl(obj, x, y, delta_in,...
              u_in, abscissa)

        [t, n]  = obj.S.find_coord(x, y);
        if(isempty(nonzeros(abscissa)))
            abscissa = t + [0:0.01:9];
        end
        u       = interp1(abscissa, u_in, t);
        delta   = interp1(abscissa, delta_in, t);
        
        if(isnan(delta))
           a = 1+1;

        end



    end
    
   function [sz_1, sz_2] = getOutputSizeImpl(obj) 
       
      sz_1 = [1 1]; % output size 
      sz_2 = [1 1]; % output size 

   end
   

    
    function [fz1,fz2] = isOutputFixedSizeImpl(~)
      %Both outputs are always fixed-sized
      fz1 = true; 
      fz2 = true; 

    end
    
    function [dt1,dt2] = getOutputDataTypeImpl(obj)
        dt1 = 'double';
        dt2 = 'double';
        dt3 = 'double';
        dt4 = 'double';
        dt5 = 'double';
        dt6 = 'double';
    end
    
    function [cp1,cp2] = isOutputComplexImpl(obj)
        cp1 = false; % Linear indices are always real value
        cp2 = false;

    end
    
  end
  
end