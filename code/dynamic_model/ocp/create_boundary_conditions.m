function data_MHT = create_boundary_conditions(data_MHT_in,bcs)
  
 data_MHT = data_MHT_in;
  % Change boundary conditions
  % initial 
  data_MHT.Parameters.u__i         = bcs.initial.u;
  data_MHT.Parameters.v__i         = bcs.initial.v;
  data_MHT.Parameters.n__i         = bcs.initial.n;
  data_MHT.Parameters.xi__i        = bcs.initial.xi;
  data_MHT.Parameters.delta__i     = bcs.initial.delta;
  data_MHT.Parameters.Omega__i     = bcs.initial.omega__z;
%   data_MHT.Parameters.fx__i        = bcs.initial.fx;
  
  % final
  data_MHT.Parameters.u__f         = bcs.final.u;
%   data_MHT.Parameters.v__f         = bcs.final.v;
  data_MHT.Parameters.n__f         = bcs.final.n;
  data_MHT.Parameters.xi__f        = bcs.final.xi;
%   data_MHT.Parameters.delta__f     = bcs.final.delta;
%   data_MHT.Parameters.Omega__f     = bcs.final.omega__z;
  
  % weights
  % initial
  data_MHT.Parameters.WBCI          = 1 ;
  data_MHT.Parameters.WBCI__u       = 1;    % u
  data_MHT.Parameters.WBCI__v       = 1 ;   % v
  data_MHT.Parameters.WBCI__n       = 1 ;   % n
  data_MHT.Parameters.WBCI__xi      = 1 ;   % xi
  data_MHT.Parameters.WBCI__delta   = 1 ;   % delta
  data_MHT.Parameters.WBCI__Omega   = 0 ;   % omega__z
  data_MHT.Parameters.WBCI__fx      = 0 ;   % fx
  
 
  
%   % final
  data_MHT.Parameters.WBCC          = 0.1;
  data_MHT.Parameters.WBCF__u       = 1;   % u
  data_MHT.Parameters.WBCF__v       = 0 ;   % v
  data_MHT.Parameters.WBCF__xi      = 1;   % v
  data_MHT.Parameters.WBCF__n       = 1;   % n
  data_MHT.Parameters.WBCF__delta   = 0 ;        % delta
  data_MHT.Parameters.WBCF__Omega   = 0 ;   % omega__z
%   
 
end