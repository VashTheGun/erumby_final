classdef (StrictDefaults)replanning_mht < matlab.System & matlab.system.mixin.Propagates
%slexVarSizeMATLABSystemFindIfSysObj Find input elements that satisfy the given condition.
%     The condition is specified in the block dialog. Both outputs 
%     are variable-sized vectors.
%

%#codegen
% Copyright 2015 The MathWorks, Inc.

properties (Nontunable)
end

properties (Access = private)
    % call the class OCP
    ocp = OCP_eRumby( 'OCP_eRumby' );
    S = ClothoidList();
    guess_ocp;
    u_targ;
    xi_targ;
    n_targ;
    abscissa_targ;
    lagrange_absc;
    lagrange_n;
    lagrange_u;
    
end

properties (Constant)
    % constant configuration method
    
    ds      = 0.01;          % ocp grid step [m]
    LH_max  = 9;
end

  
  methods(Access=protected)
    
    function validatePropertiesImpl(obj)

    end

    function setupImpl(obj) % init fnct

        % -----------------------------------------------------------------------------
        % SET UP THE ROADRACE
        % -----------------------------------------------------------------------------                      
        % initial point
        % c++ convention
        M_PI = pi;

         % track parameters
          % track parameters
        x = [0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0];
        y = [0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0];
        theta = [0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI, M_PI, 0];

        obj.S = ClothoidList() ;

        for i = 1:length(x)-1
            obj.S.push_back_G1(x(i), y(i), theta(i), x(i+1), y(i+1), theta(i+1) ) ; % track creation
        end        



        % -----------------------------------------------------------------------------
        % SET UP OF OPTIMAL CONTROL PROBLEM
        % -----------------------------------------------------------------------------
        % READ PROBLEM DATA-------------------------------------------------------------
        % model data from 'model' structure defined in the following m.file

        obj.ocp.setup('./OCP-main/ocp-eRumby/data/OCP_eRumby_Data.rb');
        obj.ocp.infoLevel(1);

        LH = round(obj.S.length);
        horizon = [0:obj.ds:LH];       

        % Compute the horizon
        start_zeta = 0;

        horizon_shift = horizon + start_zeta;

        [x_r,y_r,theta_r,curv_r] = obj.S.evaluate(horizon_shift); % extract data 

        data_ocp  = obj.ocp.get_ocp_data(); % retrieve ocp-problem data

        data_ocp.Trajectory.abscissa                = horizon; %: 
        data_ocp.Trajectory.abscissa_step           = obj.ds;
        data_ocp.Trajectory.curvature               = curv_r; %: 
        data_ocp.Trajectory.mesh.segments{1}.length = 1;      
        data_ocp.Trajectory.mesh.segments{1}.n      = LH/obj.ds;    %length(horizon);
        data_ocp.Trajectory.theta0                  = theta_r(1); %
        data_ocp.Trajectory.x0                      = x_r(1);   %
        data_ocp.Trajectory.y0                      = y_r(1);   % 

        % Boundary condition data -----------------------------------------------
        % Change boundary conditions
        % initial 
        bcs.initial.u           = 1;
        bcs.initial.v           = 0;
        bcs.initial.n           = 0;
        bcs.initial.xi          = 0;
        bcs.initial.delta       = 0;
        bcs.initial.omega__z    = 0;
        bcs.initial.fx          = 0;

        bcs.final.u           = 0;%30/3.6;
        bcs.final.n           = 0;
        bcs.final.xi          = 0;
        
        data_ocp = create_boundary_conditions(data_ocp,bcs);

        obj.ocp.setup(data_ocp);
        obj.ocp.set_guess();

        % solve ocp solution

        ok = obj.ocp.solve();
        obj.lagrange_absc   = data_ocp.SplineSetReferenceTraj.xdata;
        obj.lagrange_n      = data_ocp.SplineSetReferenceTraj.ydata{1};
        obj.lagrange_u      = data_ocp.SplineSetReferenceTraj.ydata{2};
        
        obj.abscissa_targ    = obj.ocp.solution('zeta');
        obj.xi_targ          = obj.ocp.solution('xi');
        obj.n_targ           = obj.ocp.solution('n');
        obj.u_targ           = obj.ocp.solution('u');

    end

    function [delta, u, Omega, abscissa, x_ocp, y_ocp, ok] = stepImpl(obj, x, y, theta,...
              state_feedback, delta_old, u_old, Omega_old, abscissa_old, x_ocp_old, y_ocp_old)


        % Compute the horizon
        horizon = 0:obj.ds:obj.LH_max; 
        [start_zeta, n]  = obj.S.find_coord(x, y);
        horizon_shift = horizon + start_zeta;

        [x_r,y_r,dir_r,curv_r] = obj.S.evaluate(horizon_shift); % extract data 
        xi = theta - dir_r(1);
        data_ocp  = obj.ocp.get_ocp_data(); % retrieve ocp-problem data

        data_ocp.Trajectory.abscissa                = horizon; %: 
        data_ocp.Trajectory.abscissa_step           = obj.ds;
        data_ocp.Trajectory.curvature               = curv_r; %: 
        data_ocp.Trajectory.mesh.segments{1}.length = 1;      
        data_ocp.Trajectory.mesh.segments{1}.n      = obj.LH_max/obj.ds;    %length(horizon);
        data_ocp.Trajectory.theta0                  = dir_r(1); %
        data_ocp.Trajectory.x0                      = x_r(1);   %
        data_ocp.Trajectory.y0                      = y_r(1);   %  
        data_ocp.SplineSetReferenceTraj.xdata       = horizon_shift;
        data_ocp.SplineSetReferenceTraj.ydata{1}    = interp1(obj.lagrange_absc, obj.lagrange_n, horizon_shift);
        data_ocp.SplineSetReferenceTraj.ydata{2}    = interp1(obj.lagrange_absc, obj.lagrange_u, horizon_shift);


        % Boundary condition data -----------------------------------------------
        % Change boundary conditions
        % initial 
        bcs.initial.u           = state_feedback(1);
        bcs.initial.v           = state_feedback(2);
        bcs.initial.n           = -n;
        bcs.initial.xi          = xi;
        bcs.initial.delta       = state_feedback(4);
        bcs.initial.omega__z    = state_feedback(3);
        bcs.initial.omega__x    = 0;
        bcs.initial.omega__y    = 0;
        
        bcs.final.u             = interp1(obj.abscissa_targ, obj.u_targ, start_zeta + obj.LH_max);
        bcs.final.xi            = interp1(obj.abscissa_targ, obj.xi_targ, start_zeta + obj.LH_max);
        bcs.final.n             = interp1(obj.abscissa_targ, obj.n_targ, start_zeta + obj.LH_max);

        data_ocp = create_boundary_conditions(data_ocp,bcs);
        data_ocp.Solver.max_iter = 100;
        obj.ocp.setup(data_ocp);
       
        obj.ocp.set_guess();
        
        % ~~~ dimensions variables ~~~~~~~~~
        % -------------------------------------------------------------------------
        % COMPUTE SOLUTION
        % -------------------------------------------------------------------------
        ok = obj.ocp.solve();
        obj.guess_ocp = obj.ocp.get_solution_as_guess();
        ok = double(ok);
        

        if ok == 1
            delta           = obj.ocp.solution('delta');
            Omega           = obj.ocp.solution('Omega');
            u               = obj.ocp.solution('u');
            abscissa        = horizon_shift';
            x_ocp           = obj.ocp.solution('xCoMCar');
            y_ocp           = obj.ocp.solution('yCoMCar');
        else      
            delta           = delta_old;
            Omega           = Omega_old;
            u               = u_old;
            abscissa        = abscissa_old;
            x_ocp           = x_ocp_old;
            y_ocp           = y_ocp_old;
        end

    end
    
   function [sz_1, sz_2, sz_3, sz_4, sz_5, sz_6, sz_7] = getOutputSizeImpl(obj) 
       
      sz_1 = [obj.LH_max/obj.ds+1 1]; % output size 
      sz_2 = [obj.LH_max/obj.ds+1 1]; % output size 
      sz_3 = [obj.LH_max/obj.ds+1 1]; % output size 
      sz_4 = [obj.LH_max/obj.ds+1 1]; % output size 
      sz_5 = [obj.LH_max/obj.ds+1 1]; % output size
      sz_6 = [obj.LH_max/obj.ds+1 1]; % output size 
      sz_7 = [1 1]; % output size 
   end
   

    
    function [fz1,fz2,fz3,fz4,fz5,fz6, fz7] = isOutputFixedSizeImpl(~)
      %Both outputs are always fixed-sized
      fz1 = true; 
      fz2 = true; 
      fz3 = true; 
      fz4 = true; 
      fz5 = true; 
      fz6 = true;
      fz7 = true;      
    end
    
    function [dt1,dt2, dt3,dt4,dt5,dt6, dt7] = getOutputDataTypeImpl(obj)
        dt1 = 'double';
        dt2 = 'double';
        dt3 = 'double';
        dt4 = 'double';
        dt5 = 'double';
        dt6 = 'double';
        dt7 = 'double';       
    end
    
    function [cp1,cp2,cp3,cp4,cp5,cp6,cp7] = isOutputComplexImpl(obj)
        cp1 = false; % Linear indices are always real value
        cp2 = false;
        cp3 = false;
        cp4 = false;
        cp5 = false;
        cp6 = false;
        cp7 = false;       
    end
    
  end
  
end