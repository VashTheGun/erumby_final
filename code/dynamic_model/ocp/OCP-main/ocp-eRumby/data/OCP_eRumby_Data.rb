#-----------------------------------------------------------------------#
#  file: OCP_eRumby_Data.rb                                             #
#                                                                       #
#  version: 1.0   date 13/12/2019                                       #
#                                                                       #
#  Copyright (C) 2019                                                   #
#                                                                       #
#      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             #
#      Dipartimento di Ingegneria Industriale                           #
#      Universita` degli Studi di Trento                                #
#      Via Sommarive 9, I-38123, Trento, Italy                          #
#      email: enrico.bertolazzi@unitn.it                                #
#             francesco.biral@unitn.it                                  #
#             paolo.bosetti@unitn.it                                    #
#-----------------------------------------------------------------------#


include Mechatronix

# Auxiliary values
m            = 4.2
Fx_min       = -20
u_max        = 5
pen_epsi     = 0.01
Fx_max       = 20
u_min        = 0.1
Lr           = 0.153
g            = 9.81
Wf           = 0.2
pen_epsi_u   = 0.1
Ca           = 0.3
vehHalfWidth = 1/2.0*Wf
delta_max    = 0.5
u0           = 1
fx__i        = Ca*u0**2
xi__s_xo     = 0.1
Lf           = 0.172
Izz          = (0.5*Lr+0.5*Lf)**2*m
Fz__r0       = m*g*Lf/(Lr+Lf)
Fz__f0       = m*g*Lr/(Lr+Lf)
u__i         = u0
L            = Lr+Lf

mechatronix do |data|

  # Level of message
  data.InfoLevel = 4

  data.ControlSolver = {
    # ==============================================================
    # 'LU', 'LUPQ', 'QR', 'QRP', 'SVD', 'LSS', 'LSY', 'MINIMIZATION'
    :factorization => 'LU',
    # ==============================================================
    :Rcond         => 1e-14,  # reciprocal condition number thresold for QR, SVD, LSS, LSY
    :Tau           => 0.1,
    :MaxIter       => 50,
    :Tolerance     => 1e-9,
    :Iterative     => false,
    :InfoLevel     => 1
  }

  # Enable doctor
  data.Doctor = false

  # Enable check jacobian
  data.JacobianCheck            = false
  data.JacobianCheckFull        = false
  data.JacobianCheck_epsilon    = 1e-4
  data.FiniteDifferenceJacobian = false

  # Redirect output to GenericContainer["stream_output"]
  data.RedirectStreamToString = false

  # Dump Function and Jacobian if uncommented
  #data.DumpFile = "OCP_eRumby_dump"

  # spline output (all values as function of "s")
  data.OutputSplines = [:s]

  # setup solver
  data.Solver = {
    # Linear algebra factorization selection:
    # 'LU', 'QR', 'QRP', 'SUPERLU'
    # =================
    :factorization => 'LU',
    # =================

    # Last Block selection:
    # 'LU', 'LUPQ', 'QR', 'QRP', 'SVD', 'LSS', 'LSY'
    # ==============================================
    :last_factorization => 'LU',
    # ==============================================

    # choose solves: Hyness, NewtonDumped
    # ===================================
    :solver => "Hyness",
    # ===================================

    # solver parameters
    :max_iter             => 600,
    :max_step_iter        => 100,
    :max_accumulated_iter => 5000,
    :tolerance            => 9.999999999999999e-10,
    # continuation parameters
    :ns_continuation_begin => 0,
    :ns_continuation_end   => 1,
    :continuation => {
      :initial_step   => 0.2,   # initial step for continuation
      :min_step       => 0.001, # minimum accepted step for continuation
      :reduce_factor  => 0.5,   # p fails, reduce step by this factor
      :augment_factor => 1.5,   # if step successful in less than few_iteration augment step by this factor
      :few_iterations => 8,     #
    }
  }

  # Boundary Conditions
  data.BoundaryConditions = {
    :initial_Omega => SET,
  }

  # Guess
  data.Guess = {
    # possible value: zero, default, none, warm
    :initialize => 'zero',
    # possible value: default, none, warm, spline, table
    :guess_type => 'default',
  }

  data.Parameters = {

    # Model Parameters
    :Ca          => Ca,
    :Fx_max      => Fx_max,
    :Fx_min      => Fx_min,
    :Izz         => Izz,
    :Lf          => Lf,
    :Lr          => Lr,
    :W__time     => 0.1,
    :m           => m,
    :n_delta     => 0.1,
    :tau__Fx     => 0.5,
    :u_delta     => 0.15,
    :Fx__r__s_xo => 10,
    :Omega__s_xo => 100,
    :alpha_width => 1.5,
    :delta__s_xo => 0.1,
    :delta_max   => delta_max,
    :tau__delta  => 0.1,
    :xi__s_xo    => xi__s_xo,

    # Guess Parameters
    :u0 => u0,

    # Boundary Conditions
    :WBCC        => 0,
    :WBCF__n     => 1,
    :WBCF__u     => 1,
    :WBCF__v     => 1,
    :WBCI        => 1,
    :WBCI__n     => 1,
    :WBCI__u     => 1,
    :WBCI__v     => 1,
    :fx__i       => fx__i,
    :n__f        => 0,
    :n__i        => 0,
    :u__f        => 0,
    :u__i        => u__i,
    :v__f        => 0,
    :v__i        => 0,
    :xi__f       => 0,
    :xi__i       => 0,
    :Omega__f    => 0,
    :Omega__i    => 0,
    :WBCF__Omega => 1,
    :WBCF__delta => 1,
    :WBCF__xi    => 1,
    :WBCI__Omega => 1,
    :WBCI__delta => 1,
    :WBCI__fx    => 1,
    :WBCI__xi    => 1,
    :delta__f    => 0,
    :delta__i    => 0,

    # Post Processing Parameters
    :Fz__f0       => Fz__f0,
    :Fz__r0       => Fz__r0,
    :L            => L,
    :h            => 0.09,
    :vehHalfWidth => vehHalfWidth,

    # User Function Parameters
    :Cf => 1.218,
    :Cr => 1.5,
    :Df => 0.6403,
    :Dr => 0.6784,
    :Kf => 15.93,
    :Kr => 10.49,

    # Continuation Parameters

    # Constraints Parameters
  }

  # functions mapped on objects
  data.MappedObjects = {}

  # PositivePartRegularizedWithSinAtan
  data.MappedObjects[:posPart] = { :h => 0.1 }

  # NegativePartRegularizedWithSinAtan
  data.MappedObjects[:negPart] = { :h => 0.1 }

  # SignRegularizedWithErf
  data.MappedObjects[:SignReg] = { :epsilon => 0.01, :h => 0.1 }

  # Controls
  # Penalty type controls: "QUADRATIC", "QUADRATIC2", "PARABOLA", "CUBIC"
  # Barrier type controls: "LOGARITHMIC", "COS_LOGARITHMIC", "TAN2", "HYPERBOLIC"

  data.Controls = {}
  data.Controls[:delta__OControl] = {
    :type      => 'QUADRATIC',
    :epsilon   => pen_epsi_u,
    :tolerance => 0.001
  }

  data.Controls[:Fx__r__OControl] = {
    :type      => 'QUADRATIC',
    :epsilon   => pen_epsi_u,
    :tolerance => 0.001
  }


  data.Constraints = {}
  # Constraint1D
  # Penalty subtype: 'PENALTY_REGULAR', 'PENALTY_SMOOTH', 'PENALTY_PIECEWISE'
  # Barrier subtype: 'BARRIER_LOG', 'BARRIER_LOG_EXP', 'BARRIER_LOG0'
  # PenaltyBarrier1DInterval
  data.Constraints[:speed_limit] = {
    :subType   => 'PENALTY_REGULAR',
    :epsilon   => pen_epsi,
    :tolerance => 0.001,
    :min       => u_min,
    :max       => u_max,
    :active    => true
  }
  # PenaltyBarrier1DGreaterThan
  data.Constraints[:roadRightLateralBoundaries] = {
    :subType   => 'PENALTY_REGULAR',
    :epsilon   => pen_epsi,
    :tolerance => 0.001,
    :active    => true
  }
  # PenaltyBarrier1DGreaterThan
  data.Constraints[:roadLeftLateralBoundaries] = {
    :subType   => 'PENALTY_REGULAR',
    :epsilon   => pen_epsi,
    :tolerance => 0.001,
    :active    => true
  }
  # Constraint2D: none defined

  # User defined classes initialization
  # User defined classes: S P L I N E S E T R O A D 2 D
  require_relative('../../Custom_Tracks/spline_set_road2D_data.rb',__FILE__)
  # User defined classes: T R A J E C T O R Y
  require_relative('../../Custom_Tracks/trajectory_data.rb',__FILE__)
  # User defined classes: S P L I N E S E T R E F E R E N C E T R A J
  require_relative('../../Custom_Tracks/spline_set_reference_traj_data.rb',__FILE__)


end

# EOF
