%-----------------------------------------------------------------------%
%  file: OCP_eRumby_Data.rb                                             %
%                                                                       %
%  version: 1.0   date 13/12/2019                                       %
%                                                                       %
%  Copyright (C) 2019                                                   %
%                                                                       %
%      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             %
%      Dipartimento di Ingegneria Industriale                           %
%      Universita` degli Studi di Trento                                %
%      Via Sommarive 9, I-38123, Trento, Italy                          %
%      email: enrico.bertolazzi@unitn.it                                %
%             francesco.biral@unitn.it                                  %
%             paolo.bosetti@unitn.it                                    %
%-----------------------------------------------------------------------%


clear all;
clear functions;
clear mex;
clc;

CompileMex();
