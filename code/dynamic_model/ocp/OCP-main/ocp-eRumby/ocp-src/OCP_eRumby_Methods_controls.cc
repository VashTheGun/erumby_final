/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_Methods.cc                                          |
 |                                                                       |
 |  version: 1.0   date 13/12/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby.hh"
#include "OCP_eRumby_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_u_ref_DD(__t1) pSplineSetReferenceTraj -> eval_DD( __t1,"u")
#define ALIAS_u_ref_D(__t1) pSplineSetReferenceTraj -> eval_D( __t1,"u")
#define ALIAS_u_ref(__t1) pSplineSetReferenceTraj -> eval( __t1,"u")
#define ALIAS_n_ref_DD(__t1) pSplineSetReferenceTraj -> eval_DD( __t1,"n")
#define ALIAS_n_ref_D(__t1) pSplineSetReferenceTraj -> eval_D( __t1,"n")
#define ALIAS_n_ref(__t1) pSplineSetReferenceTraj -> eval( __t1,"n")
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_rightWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_R")
#define ALIAS_rightWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_R")
#define ALIAS_rightWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_R")
#define ALIAS_leftWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_L")
#define ALIAS_leftWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_L")
#define ALIAS_leftWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_L")
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_speed_limit_DD(__t1) speed_limit.DD( __t1)
#define ALIAS_speed_limit_D(__t1) speed_limit.D( __t1)
#define ALIAS_Fx__r__OControl_D_3(__t1, __t2, __t3) Fx__r__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2(__t1, __t2, __t3) Fx__r__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1(__t1, __t2, __t3) Fx__r__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_3_3(__t1, __t2, __t3) Fx__r__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_3(__t1, __t2, __t3) Fx__r__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_2(__t1, __t2, __t3) Fx__r__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_3(__t1, __t2, __t3) Fx__r__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_2(__t1, __t2, __t3) Fx__r__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_1(__t1, __t2, __t3) Fx__r__OControl.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3(__t1, __t2, __t3) delta__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2(__t1, __t2, __t3) delta__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1(__t1, __t2, __t3) delta__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3_3(__t1, __t2, __t3) delta__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_3(__t1, __t2, __t3) delta__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_2(__t1, __t2, __t3) delta__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_3(__t1, __t2, __t3) delta__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_2(__t1, __t2, __t3) delta__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_1(__t1, __t2, __t3) delta__OControl.D_1_1( __t1, __t2, __t3)


namespace OCP_eRumbyDefine {

  /*\
   |    __ _
   |   / _` |
   |  | (_| |
   |   \__, |
   |   |___/
  \*/

  integer
  OCP_eRumby::g_numEqns() const
  { return 2; }

  void
  OCP_eRumby::g_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t4   = X__[1] * ModelPars[57];
    real_type t5   = cos(t4);
    real_type t8   = sin(t4);
    real_type t12  = 1.0 / (X__[2] * t5 - X__[3] * t8);
    real_type t17  = Q__[1] * X__[0] - 1;
    real_type t19  = t17 * t12;
    real_type t23  = ALIAS_Fx__r__OControl_D_1(U__[0], ModelPars[6], ModelPars[5]);
    result__[ 0   ] = -t17 * t12 * L__[6] - t23 * t19;
    real_type t29  = ModelPars[54];
    real_type t30  = ALIAS_delta__OControl_D_1(U__[1], -t29, t29);
    result__[ 1   ] = -t17 * t12 * L__[5] - t30 * t19;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"g_eval",2);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DgDxlp_numRows() const
  { return 2; }

  integer
  OCP_eRumby::DgDxlp_numCols() const
  { return 14; }

  integer
  OCP_eRumby::DgDxlp_nnz() const
  { return 10; }

  void
  OCP_eRumby::DgDxlp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 0   ; jIndex[ 4  ] = 13  ;
    iIndex[ 5  ] = 1   ; jIndex[ 5  ] = 0   ;
    iIndex[ 6  ] = 1   ; jIndex[ 6  ] = 1   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 2   ;
    iIndex[ 8  ] = 1   ; jIndex[ 8  ] = 3   ;
    iIndex[ 9  ] = 1   ; jIndex[ 9  ] = 12  ;
  }

  void
  OCP_eRumby::DgDxlp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = L__[6];
    real_type t3   = ModelPars[57];
    real_type t4   = t3 * X__[1];
    real_type t5   = cos(t4);
    real_type t6   = X__[2];
    real_type t8   = sin(t4);
    real_type t9   = X__[3];
    real_type t11  = t6 * t5 - t9 * t8;
    real_type t12  = 1.0 / t11;
    real_type t14  = Q__[1];
    real_type t16  = t14 * t12;
    real_type t20  = ALIAS_Fx__r__OControl_D_1(U__[0], ModelPars[6], ModelPars[5]);
    result__[ 0   ] = -t14 * t12 * t1 - t20 * t16;
    real_type t22  = t11 * t11;
    real_type t23  = 1.0 / t22;
    real_type t24  = t23 * t1;
    real_type t27  = X__[0] * t14 - 1;
    real_type t32  = -t9 * t5 * t3 - t6 * t8 * t3;
    real_type t33  = t32 * t27;
    real_type t35  = t27 * t23;
    result__[ 1   ] = t32 * t20 * t35 + t33 * t24;
    real_type t38  = t5 * t27;
    result__[ 2   ] = t5 * t20 * t35 + t38 * t24;
    real_type t42  = t8 * t27;
    result__[ 3   ] = -t8 * t20 * t35 - t42 * t24;
    result__[ 4   ] = -t27 * t12;
    real_type t47  = L__[5];
    real_type t51  = ModelPars[54];
    real_type t52  = ALIAS_delta__OControl_D_1(U__[1], -t51, t51);
    result__[ 5   ] = -t14 * t12 * t47 - t52 * t16;
    real_type t54  = t23 * t47;
    result__[ 6   ] = t32 * t52 * t35 + t33 * t54;
    result__[ 7   ] = t5 * t52 * t35 + t38 * t54;
    result__[ 8   ] = -t8 * t52 * t35 - t42 * t54;
    result__[ 9   ] = result__[4];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DgDxlp_sparse",10);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DgDu_numRows() const
  { return 2; }

  integer
  OCP_eRumby::DgDu_numCols() const
  { return 2; }

  integer
  OCP_eRumby::DgDu_nnz() const
  { return 2; }

  void
  OCP_eRumby::DgDu_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 1   ; jIndex[ 1  ] = 1   ;
  }

  void
  OCP_eRumby::DgDu_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t3   = X__[1] * ModelPars[57];
    real_type t4   = cos(t3);
    real_type t7   = sin(t3);
    real_type t16  = (Q__[1] * X__[0] - 1) / (X__[2] * t4 - X__[3] * t7);
    real_type t20  = ALIAS_Fx__r__OControl_D_1_1(U__[0], ModelPars[6], ModelPars[5]);
    result__[ 0   ] = -t20 * t16;
    real_type t23  = ModelPars[54];
    real_type t24  = ALIAS_delta__OControl_D_1_1(U__[1], -t23, t23);
    result__[ 1   ] = -t24 * t16;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DgDu_sparse",2);
    #endif
  }

  /*\
   |   ____            _             _
   |  / ___|___  _ __ | |_ _ __ ___ | |___
   | | |   / _ \| '_ \| __| '__/ _ \| / __|
   | | |__| (_) | | | | |_| | | (_) | \__ \
   |  \____\___/|_| |_|\__|_|  \___/|_|___/
  \*/

  integer
  OCP_eRumby::u_numEqns() const
  { return 2; }

  void
  OCP_eRumby::u_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U_guess__,
    P_const_pointer_type P__,
    U_pointer_type       U__
  ) const {
    if ( u_solve_iterative ) {
      u_standard_eval( i_segment, Q__, X__, L__, U_guess__, P__, U__ );
    } else {
      Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
      U__[ iU_Fx__r__O ] = Fx__r__OControl.solve(-L__[6], ModelPars[6], ModelPars[5]);
      real_type t5   = ModelPars[54];
      U__[ iU_delta__O ] = delta__OControl.solve(-L__[5], -t5, t5);
    }
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(U__.pointer(),"u_eval",2);
    #endif
  }

  void
  OCP_eRumby::DuDxlp_full(
    integer                    i_segment,
    Q_const_pointer_type       Q__,
    X_const_pointer_type       X__,
    L_const_pointer_type       L__,
    P_const_pointer_type       P__,
    U_const_pointer_type       U__,
    MatrixWrapper<real_type> & DuDxlp
  ) const {
    if ( u_solve_iterative ) {
      DuDxlp_standard_full( i_segment, Q__, X__, L__, P__, U__, DuDxlp );
    } else {
      DuDxlp(0, 0) = 0;
      DuDxlp(1, 0) = 0;
      DuDxlp(0, 1) = 0;
      DuDxlp(1, 1) = 0;
      DuDxlp(0, 2) = 0;
      DuDxlp(1, 2) = 0;
      DuDxlp(0, 3) = 0;
      DuDxlp(1, 3) = 0;
      DuDxlp(0, 4) = 0;
      DuDxlp(1, 4) = 0;
      DuDxlp(0, 5) = 0;
      DuDxlp(1, 5) = 0;
      DuDxlp(0, 6) = 0;
      DuDxlp(1, 6) = 0;
      DuDxlp(0, 7) = 0;
      DuDxlp(1, 7) = 0;
      DuDxlp(0, 8) = 0;
      DuDxlp(1, 8) = 0;
      DuDxlp(0, 9) = 0;
      DuDxlp(1, 9) = 0;
      DuDxlp(0, 10) = 0;
      DuDxlp(1, 10) = 0;
      DuDxlp(0, 11) = 0;
      DuDxlp(1, 11) = 0;
      DuDxlp(0, 12) = 0;
      DuDxlp(1, 12) = -delta__OControl.solve_rhs(-L__[5], -ModelPars[54], ModelPars[54]);
      DuDxlp(0, 13) = -Fx__r__OControl.solve_rhs(-L__[6], ModelPars[6], ModelPars[5]);
      DuDxlp(1, 13) = 0;
    }
  }

  /*\
   |   ____                                  _   _     _       _
   |  / ___|  ___  __ _ _ __ ___   ___ _ __ | |_| |   (_)_ __ | | __
   |  \___ \ / _ \/ _` | '_ ` _ \ / _ \ '_ \| __| |   | | '_ \| |/ /
   |   ___) |  __/ (_| | | | | | |  __/ | | | |_| |___| | | | |   <
   |  |____/ \___|\__, |_| |_| |_|\___|_| |_|\__|_____|_|_| |_|_|\_\
   |              |___/
  \*/

  integer
  OCP_eRumby::segmentLink_numEqns() const
  { return 0; }

  void
  OCP_eRumby::segmentLink_eval(
    integer              i_segment_left,
    Q_const_pointer_type qL,
    X_const_pointer_type xL,

    integer              i_segment_right,
    Q_const_pointer_type qR,
    X_const_pointer_type xR,

    P_const_pointer_type p,

    real_type            segmentLink[]
  ) const {
    ASSERT(false,"NON IMPLEMENTATA");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DsegmentLinkDxp_numRows() const
  { return 0; }

  integer
  OCP_eRumby::DsegmentLinkDxp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DsegmentLinkDxp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DsegmentLinkDxp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    ASSERT(false,"NON IMPLEMENTATA");
  }

  void
  OCP_eRumby::DsegmentLinkDxp_sparse(
    integer              i_segment_left,
    Q_const_pointer_type qL,
    X_const_pointer_type xL,

    integer              i_segment_right,
    Q_const_pointer_type qR,
    X_const_pointer_type xR,

    P_const_pointer_type p,

    real_type            DsegmentLinkDxp[]
  ) const {
    ASSERT(false,"NON IMPLEMENTATA");
  }

  /*\
   |     _
   |  _ | |_  _ _ __  _ __
   | | || | || | '  \| '_ \
   |  \__/ \_,_|_|_|_| .__/
   |                 |_|
  \*/

  integer
  OCP_eRumby::jump_numEqns() const
  { return 14; }

  void
  OCP_eRumby::jump_eval(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    L_const_pointer_type LL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    L_const_pointer_type LR__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    result__[ 0   ] = XR__[0] - XL__[0];
    result__[ 1   ] = XR__[1] - XL__[1];
    result__[ 2   ] = XR__[2] - XL__[2];
    result__[ 3   ] = XR__[3] - XL__[3];
    result__[ 4   ] = XR__[4] - XL__[4];
    result__[ 5   ] = XR__[5] - XL__[5];
    result__[ 6   ] = XR__[6] - XL__[6];
    result__[ 7   ] = LR__[0] - LL__[0];
    real_type t17  = ModelPars[57];
    result__[ 8   ] = LL__[1] * t17 - LR__[1] * t17;
    result__[ 9   ] = LR__[3] - LL__[3];
    result__[ 10  ] = LR__[4] - LL__[4];
    real_type t26  = ModelPars[42];
    result__[ 11  ] = -LL__[2] * t26 + LR__[2] * t26;
    real_type t33  = ModelPars[53] * ModelPars[55];
    result__[ 12  ] = -LL__[5] * t33 + LR__[5] * t33;
    real_type t40  = ModelPars[39] * ModelPars[30];
    result__[ 13  ] = -LL__[6] * t40 + LR__[6] * t40;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"jump_eval",14);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DjumpDxlp_numRows() const
  { return 14; }

  integer
  OCP_eRumby::DjumpDxlp_numCols() const
  { return 28; }

  integer
  OCP_eRumby::DjumpDxlp_nnz() const
  { return 28; }

  void
  OCP_eRumby::DjumpDxlp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 14  ;
    iIndex[ 2  ] = 1   ; jIndex[ 2  ] = 1   ;
    iIndex[ 3  ] = 1   ; jIndex[ 3  ] = 15  ;
    iIndex[ 4  ] = 2   ; jIndex[ 4  ] = 2   ;
    iIndex[ 5  ] = 2   ; jIndex[ 5  ] = 16  ;
    iIndex[ 6  ] = 3   ; jIndex[ 6  ] = 3   ;
    iIndex[ 7  ] = 3   ; jIndex[ 7  ] = 17  ;
    iIndex[ 8  ] = 4   ; jIndex[ 8  ] = 4   ;
    iIndex[ 9  ] = 4   ; jIndex[ 9  ] = 18  ;
    iIndex[ 10 ] = 5   ; jIndex[ 10 ] = 5   ;
    iIndex[ 11 ] = 5   ; jIndex[ 11 ] = 19  ;
    iIndex[ 12 ] = 6   ; jIndex[ 12 ] = 6   ;
    iIndex[ 13 ] = 6   ; jIndex[ 13 ] = 20  ;
    iIndex[ 14 ] = 7   ; jIndex[ 14 ] = 7   ;
    iIndex[ 15 ] = 7   ; jIndex[ 15 ] = 21  ;
    iIndex[ 16 ] = 8   ; jIndex[ 16 ] = 8   ;
    iIndex[ 17 ] = 8   ; jIndex[ 17 ] = 22  ;
    iIndex[ 18 ] = 9   ; jIndex[ 18 ] = 10  ;
    iIndex[ 19 ] = 9   ; jIndex[ 19 ] = 24  ;
    iIndex[ 20 ] = 10  ; jIndex[ 20 ] = 11  ;
    iIndex[ 21 ] = 10  ; jIndex[ 21 ] = 25  ;
    iIndex[ 22 ] = 11  ; jIndex[ 22 ] = 9   ;
    iIndex[ 23 ] = 11  ; jIndex[ 23 ] = 23  ;
    iIndex[ 24 ] = 12  ; jIndex[ 24 ] = 12  ;
    iIndex[ 25 ] = 12  ; jIndex[ 25 ] = 26  ;
    iIndex[ 26 ] = 13  ; jIndex[ 26 ] = 13  ;
    iIndex[ 27 ] = 13  ; jIndex[ 27 ] = 27  ;
  }

  void
  OCP_eRumby::DjumpDxlp_sparse(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    L_const_pointer_type LL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    L_const_pointer_type LR__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    result__[ 0   ] = -1;
    result__[ 1   ] = 1;
    result__[ 2   ] = -1;
    result__[ 3   ] = 1;
    result__[ 4   ] = -1;
    result__[ 5   ] = 1;
    result__[ 6   ] = -1;
    result__[ 7   ] = 1;
    result__[ 8   ] = -1;
    result__[ 9   ] = 1;
    result__[ 10  ] = -1;
    result__[ 11  ] = 1;
    result__[ 12  ] = -1;
    result__[ 13  ] = 1;
    result__[ 14  ] = -1;
    result__[ 15  ] = 1;
    result__[ 16  ] = ModelPars[57];
    result__[ 17  ] = -result__[16];
    result__[ 18  ] = -1;
    result__[ 19  ] = 1;
    result__[ 20  ] = -1;
    result__[ 21  ] = 1;
    real_type t1   = ModelPars[42];
    result__[ 22  ] = -t1;
    result__[ 23  ] = t1;
    real_type t4   = ModelPars[55] * ModelPars[53];
    result__[ 24  ] = -t4;
    result__[ 25  ] = t4;
    real_type t7   = ModelPars[30] * ModelPars[39];
    result__[ 26  ] = -t7;
    result__[ 27  ] = t7;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DjumpDxlp_sparse",28);
    #endif
  }

}

// EOF: OCP_eRumby_Methods.cc
