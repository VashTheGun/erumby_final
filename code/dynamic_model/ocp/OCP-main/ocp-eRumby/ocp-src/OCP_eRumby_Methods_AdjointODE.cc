/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_Methods.cc                                          |
 |                                                                       |
 |  version: 1.0   date 13/12/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby.hh"
#include "OCP_eRumby_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_u_ref_DD(__t1) pSplineSetReferenceTraj -> eval_DD( __t1,"u")
#define ALIAS_u_ref_D(__t1) pSplineSetReferenceTraj -> eval_D( __t1,"u")
#define ALIAS_u_ref(__t1) pSplineSetReferenceTraj -> eval( __t1,"u")
#define ALIAS_n_ref_DD(__t1) pSplineSetReferenceTraj -> eval_DD( __t1,"n")
#define ALIAS_n_ref_D(__t1) pSplineSetReferenceTraj -> eval_D( __t1,"n")
#define ALIAS_n_ref(__t1) pSplineSetReferenceTraj -> eval( __t1,"n")
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_rightWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_R")
#define ALIAS_rightWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_R")
#define ALIAS_rightWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_R")
#define ALIAS_leftWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_L")
#define ALIAS_leftWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_L")
#define ALIAS_leftWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_L")
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_speed_limit_DD(__t1) speed_limit.DD( __t1)
#define ALIAS_speed_limit_D(__t1) speed_limit.D( __t1)
#define ALIAS_Fx__r__OControl_D_3(__t1, __t2, __t3) Fx__r__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2(__t1, __t2, __t3) Fx__r__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1(__t1, __t2, __t3) Fx__r__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_3_3(__t1, __t2, __t3) Fx__r__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_3(__t1, __t2, __t3) Fx__r__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_2(__t1, __t2, __t3) Fx__r__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_3(__t1, __t2, __t3) Fx__r__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_2(__t1, __t2, __t3) Fx__r__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_1(__t1, __t2, __t3) Fx__r__OControl.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3(__t1, __t2, __t3) delta__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2(__t1, __t2, __t3) delta__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1(__t1, __t2, __t3) delta__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3_3(__t1, __t2, __t3) delta__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_3(__t1, __t2, __t3) delta__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_2(__t1, __t2, __t3) delta__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_3(__t1, __t2, __t3) delta__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_2(__t1, __t2, __t3) delta__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_1(__t1, __t2, __t3) delta__OControl.D_1_1( __t1, __t2, __t3)


namespace OCP_eRumbyDefine {

  /*\
   |  _   _
   | | | | |_  __
   | | |_| \ \/ /
   | |  _  |>  <
   | |_| |_/_/\_\
   |
  \*/

  integer
  OCP_eRumby::Hx_numEqns() const
  { return 7; }

  void
  OCP_eRumby::Hx_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t2   = ModelPars[57];
    real_type t3   = t2 * X__[1];
    real_type t4   = cos(t3);
    real_type t5   = X__[2];
    real_type t7   = sin(t3);
    real_type t8   = X__[3];
    real_type t10  = t5 * t4 - t8 * t7;
    real_type t11  = 1.0 / t10;
    real_type t12  = Q__[1];
    real_type t13  = t12 * t11;
    real_type t14  = speed_limit(t5);
    real_type t16  = X__[0];
    real_type t17  = Q__[0];
    real_type t18  = ALIAS_rightWidth(t17);
    real_type t19  = ModelPars[50];
    real_type t21  = t19 * t18 + t16;
    real_type t22  = roadRightLateralBoundaries(t21);
    real_type t25  = t16 * t12 - 1;
    real_type t26  = t25 * t11;
    real_type t27  = ALIAS_roadRightLateralBoundaries_D(t21);
    real_type t29  = ALIAS_leftWidth(t17);
    real_type t31  = t19 * t29 - t16;
    real_type t32  = roadLeftLateralBoundaries(t31);
    real_type t34  = ALIAS_roadLeftLateralBoundaries_D(t31);
    real_type t36  = ModelPars[23];
    real_type t37  = 1 - t36;
    real_type t38  = ALIAS_n_ref(t17);
    real_type t39  = t16 - t38;
    real_type t40  = t39 * t39;
    real_type t42  = ModelPars[29] * ModelPars[29];
    real_type t43  = 1.0 / t42;
    real_type t45  = ALIAS_u_ref(t17);
    real_type t46  = t5 - t45;
    real_type t47  = t46 * t46;
    real_type t49  = ModelPars[34] * ModelPars[34];
    real_type t50  = 1.0 / t49;
    real_type t54  = t36 + (t43 * t40 + t50 * t47) * t37;
    real_type t60  = L__[0];
    real_type t64  = (-t8 * t4 - t5 * t7) * t60;
    real_type t66  = L__[1];
    real_type t67  = t12 * t12;
    real_type t69  = 1.0 / t25;
    real_type t74  = ModelPars[42];
    real_type t75  = t74 * X__[4];
    real_type t77  = (t69 * t10 * t12 + t75) * t66;
    real_type t79  = L__[2];
    real_type t80  = ModelPars[13];
    real_type t82  = t80 * t75 + t8;
    real_type t83  = 1.0 / t5;
    real_type t86  = ModelPars[53];
    real_type t87  = t86 * X__[5];
    real_type t88  = -t83 * t82 + t87;
    real_type t89  = MF_Fy_f(t88);
    real_type t90  = t80 * t89;
    real_type t92  = 1.0 / ModelPars[9];
    real_type t93  = cos(t87);
    real_type t96  = ModelPars[14];
    real_type t98  = t96 * t75 - t8;
    real_type t99  = t83 * t98;
    real_type t100 = MF_Fy_r(t99);
    real_type t104 = (t92 * t96 * t100 - t93 * t92 * t90) * t79;
    real_type t106 = L__[3];
    real_type t108 = ModelPars[39];
    real_type t109 = t108 * X__[6];
    real_type t110 = sin(t87);
    real_type t112 = ModelPars[26];
    real_type t115 = ModelPars[0];
    real_type t116 = t5 * t5;
    real_type t119 = (t112 * t8 * t75 - t110 * t89 - t116 * t115 + t109) * t106;
    real_type t120 = 1.0 / t112;
    real_type t121 = t11 * t120;
    real_type t122 = t12 * t121;
    real_type t124 = L__[4];
    real_type t129 = (-t112 * t5 * t75 + t93 * t89 + t100) * t124;
    real_type t131 = L__[5];
    real_type t134 = (t87 - U__[1]) * t131;
    real_type t136 = L__[6];
    real_type t139 = (t109 - U__[0]) * t136;
    result__[ 0   ] = -2 * t43 * t39 * t37 * t26 - t69 * t67 * t66 + t13 * t104 - t122 * t119 - t122 * t129 + t13 * t134 + t13 * t139 - t14 * t13 - t22 * t13 - t32 * t13 - t54 * t13 + t13 * t64 + t13 * t77 - t27 * t26 + t34 * t26;
    real_type t141 = t10 * t10;
    real_type t143 = t25 / t141;
    real_type t144 = t7 * t2;
    real_type t146 = t4 * t2;
    real_type t148 = -t5 * t144 - t8 * t146;
    real_type t162 = t148 * t143;
    real_type t164 = t12 * t66;
    real_type t169 = t120 * t119;
    real_type t171 = t120 * t129;
    result__[ 1   ] = t148 * t14 * t143 + t148 * t22 * t143 + t148 * t32 * t143 + t148 * t54 * t143 + t26 * (t8 * t144 - t5 * t146) * t60 - t162 * t64 + t11 * t148 * t164 - t162 * t77 - t162 * t104 + t162 * t169 + t162 * t171 - t162 * t134 - t162 * t139;
    real_type t177 = ALIAS_speed_limit_D(t5);
    real_type t191 = t4 * t143;
    real_type t196 = MF_Fy_f_D(t88);
    real_type t197 = t82 * t196;
    real_type t198 = 1.0 / t116;
    real_type t201 = t93 * t92 * t80;
    real_type t203 = MF_Fy_r_D(t99);
    real_type t204 = t98 * t203;
    real_type t218 = t25 * t121;
    real_type t224 = t112 * t75;
    result__[ 2   ] = t4 * t14 * t143 - t177 * t26 + t4 * t22 * t143 + t4 * t32 * t143 + t4 * t54 * t143 - 2 * t50 * t46 * t37 * t26 - t26 * t7 * t60 - t191 * t64 + t11 * t4 * t164 - t191 * t77 + t26 * (-t92 * t96 * t198 * t204 - t201 * t198 * t197) * t79 - t191 * t104 - t218 * (-t110 * t198 * t197 - 2 * t5 * t115) * t106 + t191 * t169 - t218 * (t93 * t198 * t197 - t198 * t204 - t224) * t124 + t191 * t171 - t191 * t134 - t191 * t139;
    real_type t241 = t7 * t143;
    real_type t246 = t83 * t196;
    real_type t248 = t83 * t203;
    result__[ 3   ] = -t7 * t14 * t143 - t7 * t22 * t143 - t7 * t32 * t143 - t7 * t54 * t143 - t26 * t4 * t60 + t241 * t64 - t11 * t7 * t164 + t241 * t77 + t26 * (-t92 * t96 * t248 + t201 * t246) * t79 + t241 * t104 - t218 * (t110 * t246 + t224) * t106 - t241 * t169 - t218 * (-t93 * t246 - t248) * t124 - t241 * t171 + t241 * t134 + t241 * t139;
    real_type t269 = t74 * t196;
    real_type t270 = t80 * t80;
    real_type t275 = t74 * t203;
    real_type t276 = t96 * t96;
    real_type t283 = t83 * t80;
    result__[ 4   ] = t26 * t74 * t66 + t26 * (t93 * t92 * t83 * t270 * t269 + t92 * t83 * t276 * t275) * t79 - t218 * (t110 * t283 * t269 + t112 * t8 * t74) * t106 - t218 * (-t112 * t5 * t74 - t93 * t283 * t269 + t83 * t96 * t275) * t124;
    real_type t300 = t86 * t196;
    real_type t309 = t86 * t89;
    result__[ 5   ] = t26 * (t110 * t86 * t92 * t90 - t201 * t300) * t79 - t218 * (-t110 * t300 - t93 * t309) * t106 - t218 * (-t110 * t309 + t93 * t300) * t124 + t26 * t86 * t131;
    result__[ 6   ] = -t218 * t108 * t106 + t26 * t108 * t136;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hx_eval",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DHxDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DHxDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby::DHxDx_nnz() const
  { return 44; }

  void
  OCP_eRumby::DHxDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 0   ; jIndex[ 4  ] = 4   ;
    iIndex[ 5  ] = 0   ; jIndex[ 5  ] = 5   ;
    iIndex[ 6  ] = 0   ; jIndex[ 6  ] = 6   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 0   ;
    iIndex[ 8  ] = 1   ; jIndex[ 8  ] = 1   ;
    iIndex[ 9  ] = 1   ; jIndex[ 9  ] = 2   ;
    iIndex[ 10 ] = 1   ; jIndex[ 10 ] = 3   ;
    iIndex[ 11 ] = 1   ; jIndex[ 11 ] = 4   ;
    iIndex[ 12 ] = 1   ; jIndex[ 12 ] = 5   ;
    iIndex[ 13 ] = 1   ; jIndex[ 13 ] = 6   ;
    iIndex[ 14 ] = 2   ; jIndex[ 14 ] = 0   ;
    iIndex[ 15 ] = 2   ; jIndex[ 15 ] = 1   ;
    iIndex[ 16 ] = 2   ; jIndex[ 16 ] = 2   ;
    iIndex[ 17 ] = 2   ; jIndex[ 17 ] = 3   ;
    iIndex[ 18 ] = 2   ; jIndex[ 18 ] = 4   ;
    iIndex[ 19 ] = 2   ; jIndex[ 19 ] = 5   ;
    iIndex[ 20 ] = 2   ; jIndex[ 20 ] = 6   ;
    iIndex[ 21 ] = 3   ; jIndex[ 21 ] = 0   ;
    iIndex[ 22 ] = 3   ; jIndex[ 22 ] = 1   ;
    iIndex[ 23 ] = 3   ; jIndex[ 23 ] = 2   ;
    iIndex[ 24 ] = 3   ; jIndex[ 24 ] = 3   ;
    iIndex[ 25 ] = 3   ; jIndex[ 25 ] = 4   ;
    iIndex[ 26 ] = 3   ; jIndex[ 26 ] = 5   ;
    iIndex[ 27 ] = 3   ; jIndex[ 27 ] = 6   ;
    iIndex[ 28 ] = 4   ; jIndex[ 28 ] = 0   ;
    iIndex[ 29 ] = 4   ; jIndex[ 29 ] = 1   ;
    iIndex[ 30 ] = 4   ; jIndex[ 30 ] = 2   ;
    iIndex[ 31 ] = 4   ; jIndex[ 31 ] = 3   ;
    iIndex[ 32 ] = 4   ; jIndex[ 32 ] = 4   ;
    iIndex[ 33 ] = 4   ; jIndex[ 33 ] = 5   ;
    iIndex[ 34 ] = 5   ; jIndex[ 34 ] = 0   ;
    iIndex[ 35 ] = 5   ; jIndex[ 35 ] = 1   ;
    iIndex[ 36 ] = 5   ; jIndex[ 36 ] = 2   ;
    iIndex[ 37 ] = 5   ; jIndex[ 37 ] = 3   ;
    iIndex[ 38 ] = 5   ; jIndex[ 38 ] = 4   ;
    iIndex[ 39 ] = 5   ; jIndex[ 39 ] = 5   ;
    iIndex[ 40 ] = 6   ; jIndex[ 40 ] = 0   ;
    iIndex[ 41 ] = 6   ; jIndex[ 41 ] = 1   ;
    iIndex[ 42 ] = 6   ; jIndex[ 42 ] = 2   ;
    iIndex[ 43 ] = 6   ; jIndex[ 43 ] = 3   ;
  }

  void
  OCP_eRumby::DHxDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t2   = ModelPars[57];
    real_type t3   = t2 * X__[1];
    real_type t4   = cos(t3);
    real_type t5   = X__[2];
    real_type t7   = sin(t3);
    real_type t8   = X__[3];
    real_type t10  = t5 * t4 - t8 * t7;
    real_type t11  = 1.0 / t10;
    real_type t12  = Q__[1];
    real_type t13  = t12 * t11;
    real_type t14  = X__[0];
    real_type t15  = Q__[0];
    real_type t16  = ALIAS_rightWidth(t15);
    real_type t17  = ModelPars[50];
    real_type t19  = t17 * t16 + t14;
    real_type t20  = ALIAS_roadRightLateralBoundaries_D(t19);
    real_type t24  = t14 * t12 - 1;
    real_type t25  = t24 * t11;
    real_type t26  = ALIAS_roadRightLateralBoundaries_DD(t19);
    real_type t28  = ALIAS_leftWidth(t15);
    real_type t30  = t17 * t28 - t14;
    real_type t31  = ALIAS_roadLeftLateralBoundaries_D(t30);
    real_type t34  = ALIAS_roadLeftLateralBoundaries_DD(t30);
    real_type t36  = ModelPars[23];
    real_type t37  = 1 - t36;
    real_type t38  = ALIAS_n_ref(t15);
    real_type t39  = t14 - t38;
    real_type t42  = ModelPars[29] * ModelPars[29];
    real_type t43  = 1.0 / t42;
    result__[ 0   ] = -4 * t43 * t39 * t37 * t13 - 2 * t43 * t37 * t25 - 2 * t20 * t13 + 2 * t31 * t13 - t26 * t25 - t34 * t25;
    real_type t50  = t10 * t10;
    real_type t51  = 1.0 / t50;
    real_type t52  = t12 * t51;
    real_type t53  = speed_limit(t5);
    real_type t54  = t7 * t2;
    real_type t56  = t4 * t2;
    real_type t58  = -t5 * t54 - t8 * t56;
    real_type t59  = t58 * t53;
    real_type t61  = roadRightLateralBoundaries(t19);
    real_type t62  = t58 * t61;
    real_type t64  = t24 * t51;
    real_type t67  = roadLeftLateralBoundaries(t30);
    real_type t68  = t58 * t67;
    real_type t72  = t39 * t39;
    real_type t74  = ALIAS_u_ref(t15);
    real_type t75  = t5 - t74;
    real_type t76  = t75 * t75;
    real_type t78  = ModelPars[34] * ModelPars[34];
    real_type t79  = 1.0 / t78;
    real_type t83  = t36 + (t43 * t72 + t79 * t76) * t37;
    real_type t84  = t58 * t83;
    real_type t86  = t37 * t64;
    real_type t87  = t43 * t39;
    real_type t91  = L__[0];
    real_type t95  = (-t5 * t56 + t8 * t54) * t91;
    real_type t100 = (-t8 * t4 - t5 * t7) * t91;
    real_type t101 = t58 * t52;
    real_type t103 = L__[1];
    real_type t104 = t12 * t12;
    real_type t105 = t104 * t103;
    real_type t106 = 1.0 / t24;
    real_type t113 = ModelPars[42];
    real_type t114 = t113 * X__[4];
    real_type t116 = (t106 * t10 * t12 + t114) * t103;
    real_type t118 = L__[2];
    real_type t119 = ModelPars[13];
    real_type t121 = t119 * t114 + t8;
    real_type t122 = 1.0 / t5;
    real_type t125 = ModelPars[53];
    real_type t126 = t125 * X__[5];
    real_type t127 = -t122 * t121 + t126;
    real_type t128 = MF_Fy_f(t127);
    real_type t129 = t119 * t128;
    real_type t131 = 1.0 / ModelPars[9];
    real_type t132 = cos(t126);
    real_type t133 = t132 * t131;
    real_type t135 = ModelPars[14];
    real_type t137 = t135 * t114 - t8;
    real_type t138 = t122 * t137;
    real_type t139 = MF_Fy_r(t138);
    real_type t143 = (t131 * t135 * t139 - t133 * t129) * t118;
    real_type t145 = L__[3];
    real_type t147 = ModelPars[39];
    real_type t148 = t147 * X__[6];
    real_type t149 = sin(t126);
    real_type t151 = ModelPars[26];
    real_type t154 = ModelPars[0];
    real_type t155 = t5 * t5;
    real_type t159 = 1.0 / t151;
    real_type t160 = t159 * (t151 * t8 * t114 - t149 * t128 - t155 * t154 + t148) * t145;
    real_type t162 = L__[4];
    real_type t168 = t159 * (-t151 * t5 * t114 + t132 * t128 + t139) * t162;
    real_type t170 = L__[5];
    real_type t173 = (t126 - U__[1]) * t170;
    real_type t175 = L__[6];
    real_type t178 = (t148 - U__[0]) * t175;
    result__[ 1   ] = t11 * t106 * t58 * t105 + t58 * t20 * t64 - t58 * t31 * t64 + 2 * t58 * t87 * t86 - t101 * t100 - t101 * t116 - t101 * t143 + t101 * t160 + t101 * t168 - t101 * t173 - t101 * t178 + t13 * t95 + t59 * t52 + t62 * t52 + t68 * t52 + t84 * t52;
    real_type t180 = t4 * t53;
    real_type t182 = ALIAS_speed_limit_D(t5);
    real_type t184 = t4 * t61;
    real_type t188 = t4 * t67;
    real_type t192 = t4 * t83;
    real_type t201 = t7 * t91;
    real_type t204 = t4 * t52;
    real_type t210 = MF_Fy_f_D(t127);
    real_type t211 = t121 * t210;
    real_type t212 = 1.0 / t155;
    real_type t213 = t212 * t211;
    real_type t214 = t131 * t119;
    real_type t215 = t132 * t214;
    real_type t217 = MF_Fy_r_D(t138);
    real_type t218 = t137 * t217;
    real_type t219 = t135 * t212;
    real_type t223 = (-t131 * t219 * t218 - t215 * t213) * t118;
    real_type t226 = t149 * t212;
    real_type t231 = (-2 * t5 * t154 - t226 * t211) * t145;
    real_type t232 = t11 * t159;
    real_type t233 = t12 * t232;
    real_type t239 = t151 * t114;
    real_type t241 = (t132 * t212 * t211 - t212 * t218 - t239) * t162;
    real_type t246 = t11 * t106 * t4 * t105 - t204 * t100 - t204 * t116 + t13 * t223 - t204 * t143 + t204 * t160 + t204 * t168 - t204 * t173 - t204 * t178 - t233 * t231 - t233 * t241;
    result__[ 2   ] = -2 * t79 * t75 * t37 * t13 + t4 * t20 * t64 - t4 * t31 * t64 + 2 * t4 * t87 * t86 - t182 * t13 - t13 * t201 + t180 * t52 + t184 * t52 + t188 * t52 + t192 * t52 + t246;
    real_type t262 = t4 * t91;
    real_type t264 = t7 * t52;
    real_type t270 = t122 * t210;
    real_type t272 = t122 * t217;
    real_type t273 = t131 * t135;
    real_type t276 = (t215 * t270 - t273 * t272) * t118;
    real_type t281 = (t149 * t270 + t239) * t145;
    real_type t286 = (-t132 * t270 - t272) * t162;
    result__[ 3   ] = -t11 * t106 * t7 * t105 - t7 * t20 * t64 + t7 * t31 * t64 - t7 * t53 * t52 - t7 * t61 * t52 - t7 * t67 * t52 - t7 * t83 * t52 - 2 * t7 * t87 * t86 + t264 * t100 + t264 * t116 - t13 * t262 + t13 * t276 + t264 * t143 - t264 * t160 - t264 * t168 + t264 * t173 + t264 * t178 - t233 * t281 - t233 * t286;
    real_type t291 = t113 * t103;
    real_type t293 = t113 * t210;
    real_type t294 = t119 * t119;
    real_type t295 = t294 * t293;
    real_type t296 = t131 * t122;
    real_type t299 = t113 * t217;
    real_type t300 = t135 * t135;
    real_type t305 = (t131 * t122 * t300 * t299 + t132 * t296 * t295) * t118;
    real_type t307 = t122 * t119;
    real_type t308 = t149 * t307;
    real_type t313 = (t151 * t8 * t113 + t308 * t293) * t145;
    real_type t317 = t132 * t307;
    real_type t322 = (-t151 * t5 * t113 + t122 * t135 * t299 - t317 * t293) * t162;
    result__[ 4   ] = t13 * t291 + t13 * t305 - t233 * t313 - t233 * t322;
    real_type t324 = t125 * t210;
    real_type t327 = t149 * t125 * t131;
    real_type t330 = (t327 * t129 - t215 * t324) * t118;
    real_type t333 = t125 * t128;
    real_type t336 = (-t132 * t333 - t149 * t324) * t145;
    real_type t341 = (t132 * t324 - t149 * t333) * t162;
    real_type t343 = t125 * t170;
    result__[ 5   ] = t13 * t330 + t13 * t343 - t233 * t336 - t233 * t341;
    real_type t345 = t147 * t145;
    real_type t347 = t147 * t175;
    result__[ 6   ] = t13 * t347 - t233 * t345;
    result__[ 7   ] = result__[1];
    real_type t349 = t58 * t64;
    real_type t352 = t2 * t2;
    real_type t353 = t4 * t352;
    real_type t355 = t7 * t352;
    real_type t357 = -t5 * t353 + t8 * t355;
    real_type t358 = t357 * t64;
    real_type t361 = 1.0 / t50 / t10;
    real_type t362 = t24 * t361;
    real_type t363 = t58 * t58;
    real_type t364 = t363 * t362;
    real_type t384 = t357 * t53 * t64 - 2 * t363 * t53 * t362 - t358 * t100 + 2 * t364 * t100 - t358 * t116 + 2 * t364 * t116 - t358 * t143 + 2 * t364 * t143 - t358 * t173 + 2 * t364 * t173 - t358 * t178 + 2 * t364 * t178 - 2 * t349 * t95;
    real_type t390 = t12 * t103;
    real_type t417 = t25 * (t8 * t353 + t5 * t355) * t91 - 2 * t51 * t363 * t390 + t11 * t357 * t390 - 2 * t363 * t83 * t362 + t357 * t83 * t64 - 2 * t363 * t61 * t362 - 2 * t363 * t67 * t362 + t357 * t61 * t64 + t357 * t67 * t64 - 2 * t364 * t160 + t358 * t160 + t358 * t168 - 2 * t364 * t168;
    result__[ 8   ] = t384 + t417;
    real_type t421 = t2 * t53;
    real_type t424 = t4 * t64;
    real_type t426 = t2 * t91;
    real_type t431 = t51 * t58;
    real_type t440 = t2 * t83;
    real_type t446 = t2 * t61;
    real_type t452 = t2 * t67;
    real_type t457 = t4 * t58 * t362;
    real_type t460 = t54 * t64;
    real_type t462 = -t24 * t11 * t4 * t426 - t11 * t54 * t390 - 2 * t4 * t59 * t362 - 2 * t4 * t62 * t362 - 2 * t4 * t68 * t362 - 2 * t4 * t84 * t362 - 2 * t4 * t431 * t390 - t7 * t421 * t64 - t7 * t440 * t64 - t7 * t446 * t64 - t7 * t452 * t64 - 2 * t457 * t160 - t460 * t160 + t349 * t201 - t349 * t223 - t424 * t95;
    real_type t468 = t361 * t100;
    real_type t469 = t58 * t24;
    real_type t470 = t4 * t469;
    real_type t473 = t51 * t100;
    real_type t474 = t2 * t24;
    real_type t475 = t7 * t474;
    real_type t477 = t361 * t116;
    real_type t480 = t51 * t116;
    real_type t482 = t361 * t143;
    real_type t485 = t51 * t143;
    real_type t487 = t361 * t178;
    real_type t490 = t51 * t178;
    real_type t492 = t361 * t173;
    real_type t495 = t51 * t173;
    real_type t497 = t79 * t75;
    real_type t501 = t159 * t231;
    real_type t503 = t159 * t241;
    real_type t505 = t58 * t182 * t64 + 2 * t58 * t497 * t86 - 2 * t457 * t168 - t460 * t168 + t349 * t501 + t349 * t503 + 2 * t470 * t468 + 2 * t470 * t477 + 2 * t470 * t482 + 2 * t470 * t487 + 2 * t470 * t492 + t475 * t473 + t475 * t480 + t475 * t485 + t475 * t490 + t475 * t495;
    result__[ 9   ] = t462 + t505;
    real_type t514 = t7 * t64;
    real_type t538 = t56 * t64;
    real_type t540 = t24 * t11 * t7 * t426 - t11 * t56 * t390 + 2 * t7 * t59 * t362 + 2 * t7 * t62 * t362 + 2 * t7 * t68 * t362 + 2 * t7 * t84 * t362 + 2 * t7 * t431 * t390 - t4 * t421 * t64 - t4 * t440 * t64 - t4 * t446 * t64 - t4 * t452 * t64 - t538 * t160 + t349 * t262 - t349 * t276 + t514 * t95;
    real_type t542 = t7 * t58 * t362;
    real_type t548 = t7 * t469;
    real_type t551 = t4 * t474;
    real_type t565 = t159 * t281;
    real_type t567 = t159 * t286;
    real_type t569 = 2 * t542 * t160 - t538 * t168 + 2 * t542 * t168 + t349 * t565 + t349 * t567 - 2 * t548 * t468 + t551 * t473 - 2 * t548 * t477 + t551 * t480 - 2 * t548 * t482 + t551 * t485 - 2 * t548 * t487 + t551 * t490 - 2 * t548 * t492 + t551 * t495;
    result__[ 10  ] = t540 + t569;
    real_type t572 = t159 * t313;
    real_type t574 = t159 * t322;
    result__[ 11  ] = -t349 * t291 - t349 * t305 + t349 * t572 + t349 * t574;
    real_type t577 = t159 * t336;
    real_type t579 = t159 * t341;
    result__[ 12  ] = -t349 * t330 - t349 * t343 + t349 * t577 + t349 * t579;
    real_type t582 = t159 * t345;
    result__[ 13  ] = -t349 * t347 + t349 * t582;
    result__[ 14  ] = result__[2];
    result__[ 15  ] = result__[9];
    real_type t585 = MF_Fy_f_DD(t127);
    real_type t586 = t121 * t121;
    real_type t587 = t586 * t585;
    real_type t588 = t155 * t155;
    real_type t589 = 1.0 / t588;
    real_type t593 = 1.0 / t155 / t5;
    real_type t600 = t24 * t232;
    real_type t602 = MF_Fy_r_DD(t138);
    real_type t603 = t137 * t137;
    real_type t604 = t603 * t602;
    real_type t616 = t4 * t4;
    real_type t633 = 2 * t424 * t201;
    real_type t642 = -t600 * (2 * t149 * t593 * t211 - t149 * t589 * t587 - 2 * t154) * t145 - t600 * (-2 * t132 * t593 * t211 + t132 * t589 * t587 + 2 * t593 * t218 + t589 * t604) * t162 - 2 * t616 * t53 * t362 - 2 * t616 * t61 * t362 - 2 * t616 * t67 * t362 - 2 * t616 * t83 * t362 - 2 * t79 * t37 * t25 + t633 - 2 * t51 * t616 * t390 - 2 * t424 * t223 + 4 * t4 * t497 * t86;
    real_type t643 = t616 * t362;
    real_type t666 = t135 * t593;
    real_type t676 = ALIAS_speed_limit_DD(t5);
    real_type t682 = 2 * t643 * t100 + 2 * t643 * t116 + 2 * t643 * t143 + 2 * t424 * t501 + 2 * t424 * t503 + 2 * t643 * t173 + 2 * t643 * t178 + t25 * (t131 * t135 * t589 * t604 + 2 * t131 * t666 * t218 + 2 * t215 * t593 * t211 - t215 * t589 * t587) * t118 + 2 * t4 * t182 * t64 - t676 * t25 - 2 * t643 * t160 - 2 * t643 * t168;
    result__[ 16  ] = t642 + t682;
    real_type t683 = t7 * t7;
    real_type t688 = t593 * t585;
    real_type t691 = t212 * t210;
    real_type t693 = t593 * t602;
    real_type t697 = t212 * t217;
    real_type t705 = t7 * t4 * t362;
    real_type t711 = t7 * t4 * t24;
    real_type t725 = -t64 * t683 * t91 + t64 * t616 * t91 + t25 * (t131 * t135 * t137 * t693 + t215 * t121 * t688 - t215 * t691 + t273 * t697) * t118 - t7 * t182 * t64 + 2 * t705 * t160 + 2 * t705 * t168 - 2 * t711 * t468 - 2 * t711 * t477 - 2 * t711 * t482 - 2 * t711 * t487 - 2 * t711 * t492 - 2 * t7 * t497 * t86;
    real_type t761 = t424 * t565 - t514 * t501 + t424 * t567 - t514 * t503 + 2 * t7 * t180 * t362 + 2 * t7 * t51 * t4 * t390 + 2 * t7 * t192 * t362 + 2 * t7 * t184 * t362 + 2 * t7 * t188 * t362 - t600 * (t149 * t121 * t688 - t149 * t691) * t145 + t514 * t223 - t424 * t276 - t600 * (-t132 * t121 * t688 + t132 * t691 + t137 * t693 + t697) * t162;
    result__[ 17  ] = t725 + t761;
    real_type t763 = t113 * t585;
    real_type t764 = t294 * t763;
    real_type t765 = t121 * t593;
    real_type t769 = t132 * t131 * t212;
    real_type t771 = t113 * t602;
    real_type t776 = t212 * t300;
    real_type t777 = t131 * t776;
    real_type t783 = t119 * t763;
    real_type t786 = t212 * t119;
    real_type t787 = t149 * t786;
    real_type t798 = t132 * t786;
    real_type t800 = t151 * t113;
    result__[ 18  ] = -t424 * t291 + t25 * (-t131 * t137 * t593 * t300 * t771 + t133 * t765 * t764 - t769 * t295 - t777 * t299) * t118 - t424 * t305 - t600 * (t149 * t765 * t783 - t787 * t293) * t145 + t424 * t572 - t600 * (-t132 * t765 * t783 - t137 * t666 * t771 - t219 * t299 + t798 * t293 - t800) * t162 + t424 * t574;
    real_type t805 = t125 * t585;
    real_type t809 = t149 * t125;
    real_type t816 = t212 * t121;
    real_type t819 = t125 * t212;
    result__[ 19  ] = t25 * (-t133 * t786 * t121 * t805 + t809 * t214 * t213) * t118 - t424 * t330 - t600 * (-t132 * t819 * t211 - t149 * t816 * t805) * t145 + t424 * t577 - t600 * (t132 * t816 * t805 - t149 * t819 * t211) * t162 + t424 * t579 - t424 * t343;
    result__[ 20  ] = -t424 * t347 + t424 * t582;
    result__[ 21  ] = result__[3];
    result__[ 22  ] = result__[10];
    result__[ 23  ] = result__[17];
    real_type t849 = t683 * t362;
    real_type t857 = t212 * t585;
    real_type t859 = t212 * t602;
    real_type t868 = t585 * t145;
    result__[ 24  ] = -2 * t683 * t53 * t362 - 2 * t683 * t61 * t362 - 2 * t683 * t67 * t362 - 2 * t683 * t83 * t362 - t633 + 2 * t849 * t100 - 2 * t51 * t683 * t390 + 2 * t849 * t116 + t25 * (-t215 * t857 + t273 * t859) * t118 + 2 * t514 * t276 + 2 * t849 * t143 + t25 * t159 * t149 * t212 * t868 - 2 * t514 * t565 - 2 * t849 * t160 - t600 * (t132 * t857 + t859) * t162 - 2 * t514 * t567 - 2 * t849 * t168 + 2 * t849 * t173 + 2 * t849 * t178;
    result__[ 25  ] = t514 * t291 + t25 * (-t769 * t764 - t777 * t771) * t118 + t514 * t305 - t600 * (-t787 * t763 + t800) * t145 - t514 * t572 - t600 * (-t219 * t771 + t798 * t763) * t162 - t514 * t574;
    result__[ 26  ] = t25 * (-t327 * t119 * t270 + t215 * t122 * t805) * t118 + t514 * t330 - t600 * (t149 * t122 * t805 + t132 * t125 * t270) * t145 - t514 * t577 - t600 * (-t132 * t122 * t805 + t809 * t270) * t162 - t514 * t579 + t514 * t343;
    result__[ 27  ] = t514 * t347 - t514 * t582;
    result__[ 28  ] = result__[4];
    result__[ 29  ] = result__[11];
    result__[ 30  ] = result__[18];
    result__[ 31  ] = result__[25];
    real_type t933 = t113 * t113;
    real_type t934 = t933 * t585;
    real_type t938 = t933 * t602;
    result__[ 32  ] = t25 * (t131 * t212 * t300 * t135 * t938 - t769 * t294 * t119 * t934) * t118 + t600 * t226 * t294 * t933 * t868 - t600 * (t132 * t212 * t294 * t934 + t776 * t938) * t162;
    real_type t957 = t113 * t805;
    real_type t967 = t119 * t293;
    real_type t968 = t125 * t122;
    result__[ 33  ] = t25 * (t133 * t122 * t294 * t957 - t809 * t296 * t295) * t118 - t600 * (t132 * t968 * t967 + t308 * t957) * t145 - t600 * (t149 * t968 * t967 - t317 * t957) * t162;
    result__[ 34  ] = result__[5];
    result__[ 35  ] = result__[12];
    result__[ 36  ] = result__[19];
    result__[ 37  ] = result__[26];
    result__[ 38  ] = result__[33];
    real_type t980 = t125 * t125;
    real_type t981 = t980 * t585;
    real_type t983 = t980 * t210;
    real_type t996 = t980 * t128;
    result__[ 39  ] = t25 * (t132 * t980 * t131 * t129 + 2 * t149 * t214 * t983 - t215 * t981) * t118 - t600 * (-2 * t132 * t983 - t149 * t981 + t149 * t996) * t145 - t600 * (t132 * t981 - t132 * t996 - 2 * t149 * t983) * t162;
    result__[ 40  ] = result__[6];
    result__[ 41  ] = result__[13];
    result__[ 42  ] = result__[20];
    result__[ 43  ] = result__[27];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DHxDx_sparse",44);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DHxDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DHxDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DHxDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DHxDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DHxDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |  _   _
   | | | | |_   _
   | | |_| | | | |
   | |  _  | |_| |
   | |_| |_|\__,_|
   |
  \*/

  integer
  OCP_eRumby::Hu_numEqns() const
  { return 2; }

  void
  OCP_eRumby::Hu_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t4   = X__[1] * ModelPars[57];
    real_type t5   = cos(t4);
    real_type t8   = sin(t4);
    real_type t12  = 1.0 / (X__[2] * t5 - X__[3] * t8);
    real_type t17  = Q__[1] * X__[0] - 1;
    result__[ 0   ] = -t17 * t12 * L__[6];
    result__[ 1   ] = -t17 * t12 * L__[5];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hu_eval",2);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DHuDx_numRows() const
  { return 2; }

  integer
  OCP_eRumby::DHuDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby::DHuDx_nnz() const
  { return 8; }

  void
  OCP_eRumby::DHuDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 1   ; jIndex[ 4  ] = 0   ;
    iIndex[ 5  ] = 1   ; jIndex[ 5  ] = 1   ;
    iIndex[ 6  ] = 1   ; jIndex[ 6  ] = 2   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 3   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DHuDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = L__[6];
    real_type t3   = ModelPars[57];
    real_type t4   = t3 * X__[1];
    real_type t5   = cos(t4);
    real_type t6   = X__[2];
    real_type t8   = sin(t4);
    real_type t9   = X__[3];
    real_type t11  = t6 * t5 - t9 * t8;
    real_type t12  = 1.0 / t11;
    real_type t14  = Q__[1];
    result__[ 0   ] = -t14 * t12 * t1;
    real_type t16  = t11 * t11;
    real_type t17  = 1.0 / t16;
    real_type t18  = t17 * t1;
    real_type t21  = X__[0] * t14 - 1;
    real_type t27  = (-t9 * t5 * t3 - t6 * t8 * t3) * t21;
    result__[ 1   ] = t27 * t18;
    real_type t28  = t5 * t21;
    result__[ 2   ] = t28 * t18;
    real_type t29  = t8 * t21;
    result__[ 3   ] = -t29 * t18;
    real_type t31  = L__[5];
    result__[ 4   ] = -t14 * t12 * t31;
    real_type t34  = t17 * t31;
    result__[ 5   ] = t27 * t34;
    result__[ 6   ] = t28 * t34;
    result__[ 7   ] = -t29 * t34;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DHuDx_sparse",8);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DHuDp_numRows() const
  { return 2; }

  integer
  OCP_eRumby::DHuDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DHuDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DHuDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DHuDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |  _   _
   | | | | |_ __
   | | |_| | '_ \
   | |  _  | |_) |
   | |_| |_| .__/
   |       |_|
  \*/

  integer
  OCP_eRumby::Hp_numEqns() const
  { return 0; }

  void
  OCP_eRumby::Hp_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);

    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hp_eval",0);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DHpDp_numRows() const
  { return 0; }

  integer
  OCP_eRumby::DHpDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DHpDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DHpDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DHpDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |        _
   |    ___| |_ __ _
   |   / _ \ __/ _` |
   |  |  __/ || (_| |
   |   \___|\__\__,_|
  \*/

  integer
  OCP_eRumby::eta_numEqns() const
  { return 7; }

  void
  OCP_eRumby::eta_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = L__[0];
    result__[ 1   ] = -ModelPars[57] * L__[1];
    result__[ 2   ] = L__[3];
    result__[ 3   ] = L__[4];
    result__[ 4   ] = ModelPars[42] * L__[2];
    result__[ 5   ] = ModelPars[55] * ModelPars[53] * L__[5];
    result__[ 6   ] = ModelPars[30] * ModelPars[39] * L__[6];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"eta_eval",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DetaDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DetaDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby::DetaDx_nnz() const
  { return 0; }

  void
  OCP_eRumby::DetaDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DetaDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DetaDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DetaDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DetaDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DetaDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DetaDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |    _ __  _   _
   |   | '_ \| | | |
   |   | | | | |_| |
   |   |_| |_|\__,_|
  \*/

  integer
  OCP_eRumby::nu_numEqns() const
  { return 7; }

  void
  OCP_eRumby::nu_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = V__[0];
    result__[ 1   ] = -ModelPars[57] * V__[1];
    result__[ 2   ] = ModelPars[42] * V__[4];
    result__[ 3   ] = V__[2];
    result__[ 4   ] = V__[3];
    result__[ 5   ] = ModelPars[55] * ModelPars[53] * V__[5];
    result__[ 6   ] = ModelPars[30] * ModelPars[39] * V__[6];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"nu_eval",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DnuDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DnuDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby::DnuDx_nnz() const
  { return 0; }

  void
  OCP_eRumby::DnuDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DnuDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::DnuDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby::DnuDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby::DnuDp_nnz() const
  { return 0; }

  void
  OCP_eRumby::DnuDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby::DnuDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

}

// EOF: OCP_eRumby_Methods.cc
