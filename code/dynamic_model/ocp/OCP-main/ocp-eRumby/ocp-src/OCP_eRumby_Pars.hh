/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_Pars.hh                                             |
 |                                                                       |
 |  version: 1.0   date 13/12/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#ifndef OCP_ERUMBYPARS_HH
#define OCP_ERUMBYPARS_HH

#define numBc                    1
#define numModelPars             58
#define numConstraint1D          3
#define numConstraint2D          0
#define numConstraintU           2
#define numXvars                 7
#define numLvars                 7
#define numUvars                 2
#define numOMEGAvars             1
#define numQvars                 5
#define numPvars                 0
#define numPostProcess           32
#define numIntegratedPostProcess 1
#define numContinuationSteps     1

// Xvars
#define iX_n            0
#define iX_xi__xo       1
#define iX_u            2
#define iX_v            3
#define iX_Omega__xo    4
#define iX_delta__xo    5
#define iX_Fx__r__xo    6

// Lvars
#define iL_lambda1__xo  0
#define iL_lambda2__xo  1
#define iL_lambda3__xo  2
#define iL_lambda4__xo  3
#define iL_lambda5__xo  4
#define iL_lambda6__xo  5
#define iL_lambda7__xo  6

// Uvars
#define iU_Fx__r__O     0
#define iU_delta__O     1

// Qvars
#define iQ_zeta         0
#define iQ_Curv         1
#define iQ_xLane        2
#define iQ_yLane        3
#define iQ_theta        4

// Pvars

// ModelPars Maps
#define iM_Ca           0
#define iM_Cf           1
#define iM_Cr           2
#define iM_Df           3
#define iM_Dr           4
#define iM_Fx_max       5
#define iM_Fx_min       6
#define iM_Fz__f0       7
#define iM_Fz__r0       8
#define iM_Izz          9
#define iM_Kf           10
#define iM_Kr           11
#define iM_L            12
#define iM_Lf           13
#define iM_Lr           14
#define iM_WBCC         15
#define iM_WBCF__n      16
#define iM_WBCF__u      17
#define iM_WBCF__v      18
#define iM_WBCI         19
#define iM_WBCI__n      20
#define iM_WBCI__u      21
#define iM_WBCI__v      22
#define iM_W__time      23
#define iM_fx__i        24
#define iM_h            25
#define iM_m            26
#define iM_n__f         27
#define iM_n__i         28
#define iM_n_delta      29
#define iM_tau__Fx      30
#define iM_u0           31
#define iM_u__f         32
#define iM_u__i         33
#define iM_u_delta      34
#define iM_v__f         35
#define iM_v__i         36
#define iM_xi__f        37
#define iM_xi__i        38
#define iM_Fx__r__s_xo  39
#define iM_Omega__f     40
#define iM_Omega__i     41
#define iM_Omega__s_xo  42
#define iM_WBCF__Omega  43
#define iM_WBCF__delta  44
#define iM_WBCF__xi     45
#define iM_WBCI__Omega  46
#define iM_WBCI__delta  47
#define iM_WBCI__fx     48
#define iM_WBCI__xi     49
#define iM_alpha_width  50
#define iM_delta__f     51
#define iM_delta__i     52
#define iM_delta__s_xo  53
#define iM_delta_max    54
#define iM_tau__delta   55
#define iM_vehHalfWidth 56
#define iM_xi__s_xo     57

#endif

// EOF: OCP_eRumby_Pars.hh
