/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_Methods1.cc                                         |
 |                                                                       |
 |  version: 1.0   date 13/12/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby.hh"
#include "OCP_eRumby_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_u_ref_DD(__t1) pSplineSetReferenceTraj -> eval_DD( __t1,"u")
#define ALIAS_u_ref_D(__t1) pSplineSetReferenceTraj -> eval_D( __t1,"u")
#define ALIAS_u_ref(__t1) pSplineSetReferenceTraj -> eval( __t1,"u")
#define ALIAS_n_ref_DD(__t1) pSplineSetReferenceTraj -> eval_DD( __t1,"n")
#define ALIAS_n_ref_D(__t1) pSplineSetReferenceTraj -> eval_D( __t1,"n")
#define ALIAS_n_ref(__t1) pSplineSetReferenceTraj -> eval( __t1,"n")
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_rightWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_R")
#define ALIAS_rightWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_R")
#define ALIAS_rightWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_R")
#define ALIAS_leftWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_L")
#define ALIAS_leftWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_L")
#define ALIAS_leftWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_L")
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_speed_limit_DD(__t1) speed_limit.DD( __t1)
#define ALIAS_speed_limit_D(__t1) speed_limit.D( __t1)
#define ALIAS_Fx__r__OControl_D_3(__t1, __t2, __t3) Fx__r__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2(__t1, __t2, __t3) Fx__r__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1(__t1, __t2, __t3) Fx__r__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_3_3(__t1, __t2, __t3) Fx__r__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_3(__t1, __t2, __t3) Fx__r__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_2(__t1, __t2, __t3) Fx__r__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_3(__t1, __t2, __t3) Fx__r__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_2(__t1, __t2, __t3) Fx__r__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_1(__t1, __t2, __t3) Fx__r__OControl.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3(__t1, __t2, __t3) delta__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2(__t1, __t2, __t3) delta__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1(__t1, __t2, __t3) delta__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3_3(__t1, __t2, __t3) delta__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_3(__t1, __t2, __t3) delta__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_2(__t1, __t2, __t3) delta__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_3(__t1, __t2, __t3) delta__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_2(__t1, __t2, __t3) delta__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_1(__t1, __t2, __t3) delta__OControl.D_1_1( __t1, __t2, __t3)


namespace OCP_eRumbyDefine {
  /*\
   |   ___         _   _               _   _
   |  / __|___ _ _| |_(_)_ _ _  _ __ _| |_(_)___ _ _
   | | (__/ _ \ ' \  _| | ' \ || / _` |  _| / _ \ ' \
   |  \___\___/_||_\__|_|_||_\_,_\__,_|\__|_\___/_||_|
  \*/

  void
  OCP_eRumby::continuationStep0( real_type s ) {
    if ( infoLevel > 1 && pCout != nullptr )
      (*pCout) << "\n\nContinuation step N. 0 s = " << s << '\n';

  }

  /*\
   |  _   _               ___             _   _
   | | | | |___ ___ _ _  | __|  _ _ _  __| |_(_)___ _ _  ___
   | | |_| (_-</ -_) '_| | _| || | ' \/ _|  _| / _ \ ' \(_-<
   |  \___//__/\___|_|   |_| \_,_|_||_\__|\__|_\___/_||_/__/
  \*/
  // user defined functions which has a body defined in MAPLE
  real_type
  OCP_eRumby::MF_Fy_r( real_type alpha__XO ) const {
    real_type t2   = ModelPars[4];
    real_type t4   = ModelPars[2];
    real_type t11  = atan(alpha__XO / t2 / t4 * ModelPars[11]);
    real_type t13  = sin(t11 * t4);
    return t13 * t2 * ModelPars[8];
  }

  real_type
  OCP_eRumby::MF_Fy_r_D( real_type alpha__XO ) const {
    real_type t2   = ModelPars[11];
    real_type t4   = ModelPars[2];
    real_type t7   = ModelPars[4];
    real_type t11  = atan(alpha__XO / t7 / t4 * t2);
    real_type t13  = cos(t11 * t4);
    real_type t15  = alpha__XO * alpha__XO;
    real_type t16  = t2 * t2;
    real_type t18  = t4 * t4;
    real_type t19  = t7 * t7;
    return t19 * t18 / (t16 * t15 + t19 * t18) * t13 * t2 * ModelPars[8];
  }

  real_type
  OCP_eRumby::MF_Fy_r_DD( real_type alpha__XO ) const {
    real_type t1   = ModelPars[2];
    real_type t2   = t1 * t1;
    real_type t3   = ModelPars[4];
    real_type t4   = t3 * t3;
    real_type t5   = t4 * t2;
    real_type t8   = ModelPars[11];
    real_type t9   = t8 * t8;
    real_type t16  = atan(alpha__XO / t3 / t1 * t8);
    real_type t17  = t16 * t1;
    real_type t18  = sin(t17);
    real_type t20  = cos(t17);
    real_type t26  = alpha__XO * alpha__XO;
    real_type t29  = pow(t9 * t26 + t5, 2);
    return -1.0 / t29 * (t18 * t3 * t2 + 2 * alpha__XO * t20 * t8) * t9 * ModelPars[8] * t5;
  }

  real_type
  OCP_eRumby::MF_Fy_f( real_type alpha__XO ) const {
    real_type t2   = ModelPars[3];
    real_type t4   = ModelPars[1];
    real_type t11  = atan(alpha__XO / t2 / t4 * ModelPars[10]);
    real_type t13  = sin(t11 * t4);
    return t13 * t2 * ModelPars[7];
  }

  real_type
  OCP_eRumby::MF_Fy_f_D( real_type alpha__XO ) const {
    real_type t2   = ModelPars[10];
    real_type t4   = ModelPars[1];
    real_type t7   = ModelPars[3];
    real_type t11  = atan(alpha__XO / t7 / t4 * t2);
    real_type t13  = cos(t11 * t4);
    real_type t15  = alpha__XO * alpha__XO;
    real_type t16  = t2 * t2;
    real_type t18  = t4 * t4;
    real_type t19  = t7 * t7;
    return t19 * t18 / (t16 * t15 + t19 * t18) * t13 * t2 * ModelPars[7];
  }

  real_type
  OCP_eRumby::MF_Fy_f_DD( real_type alpha__XO ) const {
    real_type t1   = ModelPars[1];
    real_type t2   = t1 * t1;
    real_type t3   = ModelPars[3];
    real_type t4   = t3 * t3;
    real_type t5   = t4 * t2;
    real_type t8   = ModelPars[10];
    real_type t9   = t8 * t8;
    real_type t16  = atan(alpha__XO / t3 / t1 * t8);
    real_type t17  = t16 * t1;
    real_type t18  = sin(t17);
    real_type t20  = cos(t17);
    real_type t26  = alpha__XO * alpha__XO;
    real_type t29  = pow(t9 * t26 + t5, 2);
    return -1.0 / t29 * (t18 * t3 * t2 + 2 * alpha__XO * t20 * t8) * t9 * ModelPars[7] * t5;
  }


  /*\
   |  _  _            _ _ _            _
   | | || |__ _ _ __ (_) | |_ ___ _ _ (_)__ _ _ _
   | | __ / _` | '  \| | |  _/ _ \ ' \| / _` | ' \
   | |_||_\__,_|_|_|_|_|_|\__\___/_||_|_\__,_|_||_|
   |
  \*/

  real_type
  OCP_eRumby::H_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t3   = X__[1] * ModelPars[57];
    real_type t4   = cos(t3);
    real_type t5   = X__[2];
    real_type t7   = sin(t3);
    real_type t8   = X__[3];
    real_type t10  = t5 * t4 - t8 * t7;
    real_type t11  = 1.0 / t10;
    real_type t12  = Q__[1];
    real_type t13  = X__[0];
    real_type t15  = t13 * t12 - 1;
    real_type t16  = t15 * t11;
    real_type t17  = speed_limit(t5);
    real_type t19  = Q__[0];
    real_type t20  = ALIAS_rightWidth(t19);
    real_type t21  = ModelPars[50];
    real_type t24  = roadRightLateralBoundaries(t21 * t20 + t13);
    real_type t26  = ALIAS_leftWidth(t19);
    real_type t29  = roadLeftLateralBoundaries(t21 * t26 - t13);
    real_type t31  = ModelPars[23];
    real_type t33  = ALIAS_n_ref(t19);
    real_type t35  = pow(t13 - t33, 2);
    real_type t37  = ModelPars[29] * ModelPars[29];
    real_type t40  = ALIAS_u_ref(t19);
    real_type t42  = pow(t5 - t40, 2);
    real_type t44  = ModelPars[34] * ModelPars[34];
    real_type t63  = X__[4] * ModelPars[42];
    real_type t68  = ModelPars[13];
    real_type t71  = 1.0 / t5;
    real_type t75  = X__[5] * ModelPars[53];
    real_type t77  = MF_Fy_f(-t71 * (t68 * t63 + t8) + t75);
    real_type t80  = 1.0 / ModelPars[9];
    real_type t81  = cos(t75);
    real_type t84  = ModelPars[14];
    real_type t88  = MF_Fy_r(t71 * (t84 * t63 - t8));
    real_type t97  = X__[6] * ModelPars[39];
    real_type t98  = sin(t75);
    real_type t100 = ModelPars[26];
    real_type t104 = t5 * t5;
    real_type t110 = t15 * t11 / t100;
    return -t17 * t16 - t24 * t16 - t29 * t16 - (t31 + (1.0 / t37 * t35 + 1.0 / t44 * t42) * (1 - t31)) * t16 + t16 * (-t8 * t4 - t5 * t7) * L__[0] + t16 * (1.0 / t15 * t10 * t12 + t63) * L__[1] + t16 * (-t81 * t80 * t68 * t77 + t80 * t84 * t88) * L__[2] - t110 * (t100 * t8 * t63 - t104 * ModelPars[0] - t98 * t77 + t97) * L__[3] - t110 * (-t100 * t5 * t63 + t81 * t77 + t88) * L__[4] + t16 * (t75 - U__[1]) * L__[5] + t16 * (t97 - U__[0]) * L__[6];
  }

  /*\
   |   ___               _ _   _
   |  | _ \___ _ _  __ _| | |_(_)___ ___
   |  |  _/ -_) ' \/ _` | |  _| / -_|_-<
   |  |_| \___|_||_\__,_|_|\__|_\___/__/
  \*/

  real_type
  OCP_eRumby::penalties_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t3   = X__[1] * ModelPars[57];
    real_type t4   = cos(t3);
    real_type t5   = X__[2];
    real_type t7   = sin(t3);
    real_type t13  = X__[0];
    real_type t16  = (t13 * Q__[1] - 1) / (t5 * t4 - X__[3] * t7);
    real_type t17  = speed_limit(t5);
    real_type t19  = Q__[0];
    real_type t20  = ALIAS_rightWidth(t19);
    real_type t21  = ModelPars[50];
    real_type t24  = roadRightLateralBoundaries(t21 * t20 + t13);
    real_type t26  = ALIAS_leftWidth(t19);
    real_type t29  = roadLeftLateralBoundaries(t21 * t26 - t13);
    return -t17 * t16 - t24 * t16 - t29 * t16;
  }

  real_type
  OCP_eRumby::control_penalties_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t3   = X__[1] * ModelPars[57];
    real_type t4   = cos(t3);
    real_type t7   = sin(t3);
    real_type t16  = (Q__[1] * X__[0] - 1) / (X__[2] * t4 - X__[3] * t7);
    real_type t18  = ModelPars[54];
    real_type t19  = delta__OControl(U__[1], -t18, t18);
    real_type t24  = Fx__r__OControl(U__[0], ModelPars[6], ModelPars[5]);
    return -t19 * t16 - t24 * t16;
  }

  /*\
   |   _
   |  | |   __ _ __ _ _ _ __ _ _ _  __ _ ___
   |  | |__/ _` / _` | '_/ _` | ' \/ _` / -_)
   |  |____\__,_\__, |_| \__,_|_||_\__, \___|
   |            |___/              |___/
  \*/

  real_type
  OCP_eRumby::lagrange_target(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t3   = X__[1] * ModelPars[57];
    real_type t4   = cos(t3);
    real_type t5   = X__[2];
    real_type t7   = sin(t3);
    real_type t13  = X__[0];
    real_type t17  = ModelPars[23];
    real_type t19  = Q__[0];
    real_type t20  = ALIAS_n_ref(t19);
    real_type t22  = pow(t13 - t20, 2);
    real_type t24  = ModelPars[29] * ModelPars[29];
    real_type t27  = ALIAS_u_ref(t19);
    real_type t29  = pow(t5 - t27, 2);
    real_type t31  = ModelPars[34] * ModelPars[34];
    return -(t17 + (1.0 / t24 * t22 + 1.0 / t31 * t29) * (1 - t17)) * (t13 * Q__[1] - 1) / (t5 * t4 - X__[3] * t7);
  }

  /*\
   |   __  __
   |  |  \/  |__ _ _  _ ___ _ _
   |  | |\/| / _` | || / -_) '_|
   |  |_|  |_\__,_|\_, \___|_|
   |               |__/
  \*/

  real_type
  OCP_eRumby::mayer_target(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    real_type t6   = pow(XL__[2] - ModelPars[33], 2);
    real_type t12  = pow(XL__[3] - ModelPars[36], 2);
    real_type t18  = pow(XL__[0] - ModelPars[28], 2);
    real_type t22  = ModelPars[57];
    real_type t26  = pow(t22 * XL__[1] - ModelPars[38], 2);
    real_type t30  = ModelPars[53];
    real_type t34  = pow(t30 * XL__[5] - ModelPars[52], 2);
    real_type t38  = ModelPars[42];
    real_type t42  = pow(t38 * XL__[4] - ModelPars[41], 2);
    real_type t50  = pow(XL__[6] * ModelPars[39] - ModelPars[24], 2);
    real_type t59  = pow(XR__[2] - ModelPars[32], 2);
    real_type t65  = pow(XR__[3] - ModelPars[35], 2);
    real_type t71  = pow(XR__[0] - ModelPars[27], 2);
    real_type t78  = pow(t22 * XR__[1] - ModelPars[37], 2);
    real_type t85  = pow(t30 * XR__[5] - ModelPars[51], 2);
    real_type t92  = pow(t38 * XR__[4] - ModelPars[40], 2);
    return (t12 * ModelPars[22] + t18 * ModelPars[20] + t26 * ModelPars[49] + t34 * ModelPars[47] + t42 * ModelPars[46] + t50 * ModelPars[48] + t6 * ModelPars[21]) * ModelPars[19] + (t59 * ModelPars[17] + t65 * ModelPars[18] + t71 * ModelPars[16] + t78 * ModelPars[45] + t85 * ModelPars[44] + t92 * ModelPars[43]) * ModelPars[15];
  }

  /*\
   |    ___
   |   / _ \
   |  | (_) |
   |   \__\_\
  \*/

  integer
  OCP_eRumby::q_numEqns() const
  { return 5; }

  void
  OCP_eRumby::q_eval(
    integer        i_node,
    integer        i_segment,
    real_type      s,
    Q_pointer_type result__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = s;
    result__[ 1   ] = ALIAS_Curv(s);
    result__[ 2   ] = ALIAS_xLane(s);
    result__[ 3   ] = ALIAS_yLane(s);
    result__[ 4   ] = ALIAS_theta(s);
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__.pointer(),"q_eval",5);
    #endif
  }

  /*\
   |    ___
   |   / __|_  _ ___ ______
   |  | (_ | || / -_|_-<_-<
   |   \___|\_,_\___/__/__/
  \*/

  integer
  OCP_eRumby::u_guess_numEqns() const
  { return 2; }

  void
  OCP_eRumby::u_guess_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    U_pointer_type       U__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    std::fill_n( U__.pointer(), 2, 0 );
    U__[ iU_Fx__r__O ] = 0;
    U__[ iU_delta__O ] = 0;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(U__.pointer(),"u_guess_eval",2);
    #endif
  }

  /*\
   |    ___ _           _
   |   / __| |_  ___ __| |__
   |  | (__| ' \/ -_) _| / /
   |   \___|_||_\___\__|_\_\
  \*/

  void
  OCP_eRumby::u_check_if_admissible(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    try {
      Fx__r__OControl.check_range(U__[0], ModelPars[6], ModelPars[5]);
      real_type t5   = ModelPars[54];
      delta__OControl.check_range(U__[1], -t5, t5);
    } catch ( exception const & exc ) {
      MECHATRONIX_DO_ERROR("OCP_eRumby::u_check_if_admissible: " << exc.what());
    }
  }

  /*\
   |   ___        _     ___                       _
   |  | _ \___ __| |_  | _ \_ _ ___  __ ___ _____(_)_ _  __ _
   |  |  _/ _ (_-<  _| |  _/ '_/ _ \/ _/ -_|_-<_-< | ' \/ _` |
   |  |_| \___/__/\__| |_| |_| \___/\__\___/__/__/_|_||_\__, |
   |                                                    |___/
  \*/

  integer
  OCP_eRumby::post_numEqns() const
  { return 32; }

  void
  OCP_eRumby::post_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[6];
    real_type t2   = ModelPars[39];
    result__[ 0   ] = t2 * t1;
    real_type t3   = X__[5];
    real_type t4   = ModelPars[53];
    result__[ 1   ] = t4 * t3;
    result__[ 2   ] = X__[1] * ModelPars[57];
    real_type t7   = X__[4];
    real_type t8   = ModelPars[42];
    result__[ 3   ] = t8 * t7;
    real_type t10  = ModelPars[54];
    result__[ 4   ] = delta__OControl(U__[1], -t10, t10);
    result__[ 5   ] = Fx__r__OControl(U__[0], ModelPars[6], ModelPars[5]);
    real_type t14  = X__[2];
    result__[ 6   ] = speed_limit(t14);
    real_type t15  = X__[0];
    real_type t16  = Q__[0];
    real_type t17  = ALIAS_rightWidth(t16);
    real_type t18  = ModelPars[50];
    result__[ 7   ] = roadRightLateralBoundaries(t18 * t17 + t15);
    real_type t21  = ALIAS_leftWidth(t16);
    result__[ 8   ] = roadLeftLateralBoundaries(t18 * t21 - t15);
    real_type t24  = t8 * t7;
    result__[ 9   ] = t14 * t24;
    real_type t25  = cos(result__[2]);
    real_type t27  = sin(result__[2]);
    real_type t28  = X__[3];
    result__[ 10  ] = -1.0 / (t15 * Q__[1] - 1) * (t14 * t25 - t28 * t27);
    real_type t36  = Q__[2];
    real_type t37  = Q__[4];
    real_type t38  = result__[2] + t37;
    real_type t39  = sin(t38);
    result__[ 11  ] = -t15 * t39 + t36;
    real_type t41  = Q__[3];
    real_type t42  = cos(t38);
    result__[ 12  ] = t15 * t42 + t41;
    real_type t44  = ModelPars[56];
    real_type t45  = t15 + t44;
    result__[ 13  ] = -t45 * t39 + t36;
    result__[ 14  ] = t45 * t42 + t41;
    real_type t48  = t15 - t44;
    result__[ 15  ] = -t48 * t39 + t36;
    result__[ 16  ] = t48 * t42 + t41;
    real_type t51  = sin(t37);
    result__[ 17  ] = -t21 * t51 + t36;
    real_type t53  = cos(t37);
    result__[ 18  ] = t21 * t53 + t41;
    result__[ 19  ] = t17 * t51 + t36;
    result__[ 20  ] = -t17 * t53 + t41;
    real_type t57  = 1.0 / t14;
    result__[ 21  ] = atan(t57 * t28);
    real_type t60  = t14 * t14;
    real_type t65  = ModelPars[13] * t24;
    real_type t68  = MF_Fy_f(-t57 * (-t14 * t4 * t3 + t28 + t65));
    real_type t69  = sin(result__[1]);
    real_type t71  = t60 * ModelPars[0] + t69 * t68 - result__[0];
    result__[ 22  ] = -1.0 / ModelPars[26] * t71;
    result__[ 23  ] = -1.0 / ModelPars[12] * ModelPars[25] * t71;
    real_type t82  = -t57 * (t65 + t28) + result__[1];
    result__[ 24  ] = MF_Fy_f(t82);
    real_type t86  = t57 * (ModelPars[14] * t24 - t28);
    result__[ 25  ] = MF_Fy_r(t86);
    result__[ 26  ] = t86;
    result__[ 27  ] = t82;
    real_type t88  = 1.0 / ModelPars[8];
    result__[ 28  ] = t88 * result__[25];
    result__[ 29  ] = 1.0 / ModelPars[7] * result__[24];
    result__[ 30  ] = t88 * t2 * t1;
    real_type t96  = pow(t14 - ModelPars[33], 2);
    real_type t101 = pow(t28 - ModelPars[36], 2);
    real_type t106 = pow(t15 - ModelPars[28], 2);
    real_type t111 = pow(result__[2] - ModelPars[38], 2);
    real_type t116 = pow(result__[1] - ModelPars[52], 2);
    real_type t121 = pow(result__[3] - ModelPars[41], 2);
    real_type t126 = pow(result__[0] - ModelPars[24], 2);
    real_type t134 = pow(t14 - ModelPars[32], 2);
    real_type t139 = pow(t28 - ModelPars[35], 2);
    real_type t144 = pow(t15 - ModelPars[27], 2);
    real_type t149 = pow(result__[2] - ModelPars[37], 2);
    real_type t154 = pow(result__[1] - ModelPars[51], 2);
    real_type t159 = pow(result__[3] - ModelPars[40], 2);
    result__[ 31  ] = (t101 * ModelPars[22] + t106 * ModelPars[20] + t111 * ModelPars[49] + t116 * ModelPars[47] + t121 * ModelPars[46] + t126 * ModelPars[48] + t96 * ModelPars[21]) * ModelPars[19] + (t134 * ModelPars[17] + t139 * ModelPars[18] + t144 * ModelPars[16] + t149 * ModelPars[45] + t154 * ModelPars[44] + t159 * ModelPars[43]) * ModelPars[15];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"post_eval",32);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby::integrated_post_numEqns() const
  { return 1; }

  void
  OCP_eRumby::integrated_post_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t3   = X__[1] * ModelPars[57];
    real_type t4   = cos(t3);
    real_type t7   = sin(t3);
    result__[ 0   ] = -(Q__[1] * X__[0] - 1) / (X__[2] * t4 - X__[3] * t7);
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"integrated_post_eval",1);
    #endif
  }

}

// EOF: OCP_eRumby_Methods1.cc
