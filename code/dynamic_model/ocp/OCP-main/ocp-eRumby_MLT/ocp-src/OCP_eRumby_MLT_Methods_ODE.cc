/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_MLT_Methods.cc                                      |
 |                                                                       |
 |  version: 1.0   date 13/12/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby_MLT.hh"
#include "OCP_eRumby_MLT_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_rightWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_R")
#define ALIAS_rightWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_R")
#define ALIAS_rightWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_R")
#define ALIAS_leftWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_L")
#define ALIAS_leftWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_L")
#define ALIAS_leftWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_L")
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_speed_limit_DD(__t1) speed_limit.DD( __t1)
#define ALIAS_speed_limit_D(__t1) speed_limit.D( __t1)
#define ALIAS_Fx__r__OControl_D_3(__t1, __t2, __t3) Fx__r__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2(__t1, __t2, __t3) Fx__r__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1(__t1, __t2, __t3) Fx__r__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_3_3(__t1, __t2, __t3) Fx__r__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_3(__t1, __t2, __t3) Fx__r__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_2(__t1, __t2, __t3) Fx__r__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_3(__t1, __t2, __t3) Fx__r__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_2(__t1, __t2, __t3) Fx__r__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_1(__t1, __t2, __t3) Fx__r__OControl.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3(__t1, __t2, __t3) delta__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2(__t1, __t2, __t3) delta__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1(__t1, __t2, __t3) delta__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3_3(__t1, __t2, __t3) delta__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_3(__t1, __t2, __t3) delta__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_2(__t1, __t2, __t3) delta__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_3(__t1, __t2, __t3) delta__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_2(__t1, __t2, __t3) delta__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_1(__t1, __t2, __t3) delta__OControl.D_1_1( __t1, __t2, __t3)


namespace OCP_eRumby_MLTDefine {

  /*\
   |    ___  ___  ___
   |   / _ \|   \| __|
   |  | (_) | |) | _|
   |   \___/|___/|___|
  \*/

  integer
  OCP_eRumby_MLT::rhs_ode_numEqns() const
  { return 7; }

  void
  OCP_eRumby_MLT::rhs_ode_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t3   = X__[1] * ModelPars[54];
    real_type t4   = sin(t3);
    real_type t5   = X__[2];
    real_type t7   = cos(t3);
    real_type t8   = X__[3];
    real_type t13  = -t8 * t4 + t5 * t7;
    real_type t14  = 1.0 / t13;
    real_type t16  = Q__[1];
    real_type t19  = X__[0] * t16 - 1;
    result__[ 0   ] = t19 * t14 * (-t5 * t4 - t8 * t7);
    real_type t25  = X__[4] * ModelPars[40];
    result__[ 1   ] = t19 * t14 * (1.0 / t19 * t13 * t16 + t25);
    real_type t28  = ModelPars[13];
    real_type t31  = 1.0 / t5;
    real_type t35  = X__[5] * ModelPars[50];
    real_type t37  = MF_Fy_f(-t31 * (t28 * t25 + t8) + t35);
    real_type t40  = 1.0 / ModelPars[9];
    real_type t41  = cos(t35);
    real_type t44  = ModelPars[14];
    real_type t48  = MF_Fy_r(t31 * (t44 * t25 - t8));
    result__[ 2   ] = t19 * t14 * (-t41 * t40 * t28 * t37 + t40 * t44 * t48);
    real_type t55  = X__[6] * ModelPars[37];
    real_type t56  = sin(t35);
    real_type t58  = ModelPars[25];
    real_type t62  = t5 * t5;
    real_type t65  = 1.0 / t58;
    real_type t67  = t19 * t14;
    result__[ 3   ] = -t67 * t65 * (t58 * t8 * t25 - t56 * t37 - t62 * ModelPars[0] + t55);
    result__[ 4   ] = -t67 * t65 * (-t58 * t5 * t25 + t41 * t37 + t48);
    result__[ 5   ] = t19 * t14 * (t35 - U__[1]);
    result__[ 6   ] = t19 * t14 * (t55 - U__[0]);
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"rhs_ode",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::Drhs_odeDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby_MLT::Drhs_odeDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby_MLT::Drhs_odeDx_nnz() const
  { return 38; }

  void
  OCP_eRumby_MLT::Drhs_odeDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 1   ; jIndex[ 4  ] = 0   ;
    iIndex[ 5  ] = 1   ; jIndex[ 5  ] = 1   ;
    iIndex[ 6  ] = 1   ; jIndex[ 6  ] = 2   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 3   ;
    iIndex[ 8  ] = 1   ; jIndex[ 8  ] = 4   ;
    iIndex[ 9  ] = 2   ; jIndex[ 9  ] = 0   ;
    iIndex[ 10 ] = 2   ; jIndex[ 10 ] = 1   ;
    iIndex[ 11 ] = 2   ; jIndex[ 11 ] = 2   ;
    iIndex[ 12 ] = 2   ; jIndex[ 12 ] = 3   ;
    iIndex[ 13 ] = 2   ; jIndex[ 13 ] = 4   ;
    iIndex[ 14 ] = 2   ; jIndex[ 14 ] = 5   ;
    iIndex[ 15 ] = 3   ; jIndex[ 15 ] = 0   ;
    iIndex[ 16 ] = 3   ; jIndex[ 16 ] = 1   ;
    iIndex[ 17 ] = 3   ; jIndex[ 17 ] = 2   ;
    iIndex[ 18 ] = 3   ; jIndex[ 18 ] = 3   ;
    iIndex[ 19 ] = 3   ; jIndex[ 19 ] = 4   ;
    iIndex[ 20 ] = 3   ; jIndex[ 20 ] = 5   ;
    iIndex[ 21 ] = 3   ; jIndex[ 21 ] = 6   ;
    iIndex[ 22 ] = 4   ; jIndex[ 22 ] = 0   ;
    iIndex[ 23 ] = 4   ; jIndex[ 23 ] = 1   ;
    iIndex[ 24 ] = 4   ; jIndex[ 24 ] = 2   ;
    iIndex[ 25 ] = 4   ; jIndex[ 25 ] = 3   ;
    iIndex[ 26 ] = 4   ; jIndex[ 26 ] = 4   ;
    iIndex[ 27 ] = 4   ; jIndex[ 27 ] = 5   ;
    iIndex[ 28 ] = 5   ; jIndex[ 28 ] = 0   ;
    iIndex[ 29 ] = 5   ; jIndex[ 29 ] = 1   ;
    iIndex[ 30 ] = 5   ; jIndex[ 30 ] = 2   ;
    iIndex[ 31 ] = 5   ; jIndex[ 31 ] = 3   ;
    iIndex[ 32 ] = 5   ; jIndex[ 32 ] = 5   ;
    iIndex[ 33 ] = 6   ; jIndex[ 33 ] = 0   ;
    iIndex[ 34 ] = 6   ; jIndex[ 34 ] = 1   ;
    iIndex[ 35 ] = 6   ; jIndex[ 35 ] = 2   ;
    iIndex[ 36 ] = 6   ; jIndex[ 36 ] = 3   ;
    iIndex[ 37 ] = 6   ; jIndex[ 37 ] = 6   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::Drhs_odeDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t2   = ModelPars[54];
    real_type t3   = t2 * X__[1];
    real_type t4   = sin(t3);
    real_type t5   = X__[2];
    real_type t7   = cos(t3);
    real_type t8   = X__[3];
    real_type t10  = -t5 * t4 - t8 * t7;
    real_type t13  = -t8 * t4 + t5 * t7;
    real_type t14  = 1.0 / t13;
    real_type t16  = Q__[1];
    result__[ 0   ] = t16 * t14 * t10;
    real_type t17  = t7 * t2;
    real_type t19  = t4 * t2;
    real_type t25  = X__[0] * t16 - 1;
    real_type t27  = t13 * t13;
    real_type t28  = 1.0 / t27;
    real_type t29  = t28 * t10;
    real_type t32  = -t8 * t17 - t5 * t19;
    real_type t33  = t32 * t25;
    result__[ 1   ] = t25 * t14 * (-t5 * t17 + t8 * t19) - t33 * t29;
    real_type t37  = t7 * t25;
    result__[ 2   ] = -t25 * t14 * t4 - t37 * t29;
    real_type t41  = t4 * t25;
    result__[ 3   ] = -t25 * t14 * t7 + t41 * t29;
    real_type t43  = t16 * t16;
    real_type t44  = 1.0 / t25;
    real_type t49  = ModelPars[40];
    real_type t50  = t49 * X__[4];
    real_type t51  = t44 * t13 * t16 + t50;
    result__[ 4   ] = t16 * t14 * t51 - t44 * t43;
    real_type t56  = t28 * t51;
    result__[ 5   ] = t14 * t32 * t16 - t33 * t56;
    result__[ 6   ] = t14 * t7 * t16 - t37 * t56;
    result__[ 7   ] = -t14 * t4 * t16 + t41 * t56;
    result__[ 8   ] = t25 * t14 * t49;
    real_type t65  = ModelPars[13];
    real_type t67  = t65 * t50 + t8;
    real_type t68  = 1.0 / t5;
    real_type t71  = ModelPars[50];
    real_type t72  = t71 * X__[5];
    real_type t73  = -t68 * t67 + t72;
    real_type t74  = MF_Fy_f(t73);
    real_type t75  = t65 * t74;
    real_type t77  = 1.0 / ModelPars[9];
    real_type t78  = cos(t72);
    real_type t81  = ModelPars[14];
    real_type t83  = t81 * t50 - t8;
    real_type t84  = t68 * t83;
    real_type t85  = MF_Fy_r(t84);
    real_type t88  = -t78 * t77 * t75 + t77 * t81 * t85;
    result__[ 9   ] = t16 * t14 * t88;
    real_type t90  = t28 * t88;
    result__[ 10  ] = -t33 * t90;
    real_type t92  = MF_Fy_f_D(t73);
    real_type t93  = t67 * t92;
    real_type t94  = t5 * t5;
    real_type t95  = 1.0 / t94;
    real_type t98  = t78 * t77 * t65;
    real_type t100 = MF_Fy_r_D(t84);
    real_type t101 = t83 * t100;
    result__[ 11  ] = t25 * t14 * (-t77 * t81 * t95 * t101 - t98 * t95 * t93) - t37 * t90;
    real_type t109 = t68 * t92;
    real_type t111 = t68 * t100;
    result__[ 12  ] = t25 * t14 * (-t77 * t81 * t111 + t98 * t109) + t41 * t90;
    real_type t118 = t49 * t92;
    real_type t119 = t65 * t65;
    real_type t124 = t49 * t100;
    real_type t125 = t81 * t81;
    result__[ 13  ] = t25 * t14 * (t78 * t77 * t68 * t119 * t118 + t77 * t68 * t125 * t124);
    real_type t131 = t71 * t92;
    real_type t134 = sin(t72);
    result__[ 14  ] = t25 * t14 * (t134 * t71 * t77 * t75 - t98 * t131);
    real_type t140 = ModelPars[37];
    real_type t141 = t140 * X__[6];
    real_type t143 = ModelPars[25];
    real_type t146 = ModelPars[0];
    real_type t149 = 1.0 / t143;
    real_type t150 = t149 * (t143 * t8 * t50 - t134 * t74 - t94 * t146 + t141);
    real_type t151 = t16 * t14;
    result__[ 15  ] = -t151 * t150;
    real_type t153 = t25 * t28;
    real_type t154 = t32 * t153;
    result__[ 16  ] = t154 * t150;
    real_type t161 = t25 * t14;
    real_type t163 = t7 * t153;
    result__[ 17  ] = -t161 * t149 * (-t134 * t95 * t93 - 2 * t5 * t146) + t163 * t150;
    real_type t166 = t143 * t50;
    real_type t170 = t4 * t153;
    result__[ 18  ] = -t161 * t149 * (t134 * t109 + t166) - t170 * t150;
    real_type t172 = t68 * t65;
    result__[ 19  ] = -t161 * t149 * (t134 * t172 * t118 + t143 * t8 * t49);
    real_type t181 = t71 * t74;
    result__[ 20  ] = -t161 * t149 * (-t134 * t131 - t78 * t181);
    result__[ 21  ] = -t161 * t149 * t140;
    real_type t192 = t149 * (-t143 * t5 * t50 + t78 * t74 + t85);
    result__[ 22  ] = -t151 * t192;
    result__[ 23  ] = t154 * t192;
    result__[ 24  ] = -t161 * t149 * (t78 * t95 * t93 - t95 * t101 - t166) + t163 * t192;
    result__[ 25  ] = -t161 * t149 * (-t78 * t109 - t111) - t170 * t192;
    result__[ 26  ] = -t161 * t149 * (-t78 * t172 * t118 + t68 * t81 * t124 - t143 * t5 * t49);
    result__[ 27  ] = -t161 * t149 * (t78 * t131 - t134 * t181);
    real_type t221 = t72 - U__[1];
    result__[ 28  ] = t16 * t14 * t221;
    real_type t223 = t28 * t221;
    result__[ 29  ] = -t33 * t223;
    result__[ 30  ] = -t37 * t223;
    result__[ 31  ] = t41 * t223;
    result__[ 32  ] = t25 * t14 * t71;
    real_type t228 = t141 - U__[0];
    result__[ 33  ] = t16 * t14 * t228;
    real_type t230 = t28 * t228;
    result__[ 34  ] = -t33 * t230;
    result__[ 35  ] = -t37 * t230;
    result__[ 36  ] = t41 * t230;
    result__[ 37  ] = t25 * t14 * t140;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Drhs_odeDxp_sparse",38);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::Drhs_odeDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby_MLT::Drhs_odeDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby_MLT::Drhs_odeDp_nnz() const
  { return 0; }

  void
  OCP_eRumby_MLT::Drhs_odeDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::Drhs_odeDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::Drhs_odeDu_numRows() const
  { return 7; }

  integer
  OCP_eRumby_MLT::Drhs_odeDu_numCols() const
  { return 2; }

  integer
  OCP_eRumby_MLT::Drhs_odeDu_nnz() const
  { return 2; }

  void
  OCP_eRumby_MLT::Drhs_odeDu_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 5   ; jIndex[ 0  ] = 1   ;
    iIndex[ 1  ] = 6   ; jIndex[ 1  ] = 0   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::Drhs_odeDu_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t3   = X__[1] * ModelPars[54];
    real_type t4   = cos(t3);
    real_type t7   = sin(t3);
    result__[ 0   ] = -(Q__[1] * X__[0] - 1) / (X__[2] * t4 - X__[3] * t7);
    result__[ 1   ] = result__[0];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Drhs_odeDu_sparse",2);
    #endif
  }

  /*\
   |   __  __              __  __      _       _
   |  |  \/  |__ _ ______ |  \/  |__ _| |_ _ _(_)_ __
   |  | |\/| / _` (_-<_-< | |\/| / _` |  _| '_| \ \ /
   |  |_|  |_\__,_/__/__/ |_|  |_\__,_|\__|_| |_/_\_\
  \*/

  integer
  OCP_eRumby_MLT::A_numRows() const
  { return 7; }

  integer
  OCP_eRumby_MLT::A_numCols() const
  { return 7; }

  integer
  OCP_eRumby_MLT::A_nnz() const
  { return 7; }

  void
  OCP_eRumby_MLT::A_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 1   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 2   ; jIndex[ 2  ] = 4   ;
    iIndex[ 3  ] = 3   ; jIndex[ 3  ] = 2   ;
    iIndex[ 4  ] = 4   ; jIndex[ 4  ] = 3   ;
    iIndex[ 5  ] = 5   ; jIndex[ 5  ] = 5   ;
    iIndex[ 6  ] = 6   ; jIndex[ 6  ] = 6   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::A_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = 1;
    result__[ 1   ] = -ModelPars[54];
    result__[ 2   ] = ModelPars[40];
    result__[ 3   ] = 1;
    result__[ 4   ] = 1;
    result__[ 5   ] = ModelPars[52] * ModelPars[50];
    result__[ 6   ] = ModelPars[28] * ModelPars[37];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"A_sparse",7);
    #endif
  }

}

// EOF: OCP_eRumby_MLT_Methods.cc
