/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_MLT_Methods.cc                                      |
 |                                                                       |
 |  version: 1.0   date 13/12/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby_MLT.hh"
#include "OCP_eRumby_MLT_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_rightWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_R")
#define ALIAS_rightWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_R")
#define ALIAS_rightWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_R")
#define ALIAS_leftWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_L")
#define ALIAS_leftWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_L")
#define ALIAS_leftWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_L")
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_speed_limit_DD(__t1) speed_limit.DD( __t1)
#define ALIAS_speed_limit_D(__t1) speed_limit.D( __t1)
#define ALIAS_Fx__r__OControl_D_3(__t1, __t2, __t3) Fx__r__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2(__t1, __t2, __t3) Fx__r__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1(__t1, __t2, __t3) Fx__r__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_3_3(__t1, __t2, __t3) Fx__r__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_3(__t1, __t2, __t3) Fx__r__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_2(__t1, __t2, __t3) Fx__r__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_3(__t1, __t2, __t3) Fx__r__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_2(__t1, __t2, __t3) Fx__r__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_1(__t1, __t2, __t3) Fx__r__OControl.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3(__t1, __t2, __t3) delta__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2(__t1, __t2, __t3) delta__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1(__t1, __t2, __t3) delta__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3_3(__t1, __t2, __t3) delta__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_3(__t1, __t2, __t3) delta__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_2(__t1, __t2, __t3) delta__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_3(__t1, __t2, __t3) delta__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_2(__t1, __t2, __t3) delta__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_1(__t1, __t2, __t3) delta__OControl.D_1_1( __t1, __t2, __t3)


namespace OCP_eRumby_MLTDefine {

  /*\
   |  _   _
   | | | | |_  __
   | | |_| \ \/ /
   | |  _  |>  <
   | |_| |_/_/\_\
   |
  \*/

  integer
  OCP_eRumby_MLT::Hx_numEqns() const
  { return 7; }

  void
  OCP_eRumby_MLT::Hx_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t2   = ModelPars[54];
    real_type t3   = t2 * X__[1];
    real_type t4   = cos(t3);
    real_type t5   = X__[2];
    real_type t7   = sin(t3);
    real_type t8   = X__[3];
    real_type t10  = t5 * t4 - t8 * t7;
    real_type t11  = 1.0 / t10;
    real_type t12  = Q__[1];
    real_type t13  = t12 * t11;
    real_type t14  = speed_limit(t5);
    real_type t16  = X__[0];
    real_type t17  = Q__[0];
    real_type t18  = ALIAS_rightWidth(t17);
    real_type t19  = t16 + t18;
    real_type t20  = roadRightLateralBoundaries(t19);
    real_type t23  = t16 * t12 - 1;
    real_type t24  = t23 * t11;
    real_type t25  = ALIAS_roadRightLateralBoundaries_D(t19);
    real_type t27  = ALIAS_leftWidth(t17);
    real_type t28  = t27 - t16;
    real_type t29  = roadLeftLateralBoundaries(t28);
    real_type t31  = ALIAS_roadLeftLateralBoundaries_D(t28);
    real_type t33  = ModelPars[34];
    real_type t36  = L__[0];
    real_type t40  = (-t8 * t4 - t5 * t7) * t36;
    real_type t42  = L__[1];
    real_type t43  = t12 * t12;
    real_type t45  = 1.0 / t23;
    real_type t50  = ModelPars[40];
    real_type t51  = t50 * X__[4];
    real_type t53  = (t45 * t10 * t12 + t51) * t42;
    real_type t55  = L__[2];
    real_type t56  = ModelPars[13];
    real_type t58  = t56 * t51 + t8;
    real_type t59  = 1.0 / t5;
    real_type t62  = ModelPars[50];
    real_type t63  = t62 * X__[5];
    real_type t64  = -t59 * t58 + t63;
    real_type t65  = MF_Fy_f(t64);
    real_type t66  = t56 * t65;
    real_type t68  = 1.0 / ModelPars[9];
    real_type t69  = cos(t63);
    real_type t72  = ModelPars[14];
    real_type t74  = t72 * t51 - t8;
    real_type t75  = t59 * t74;
    real_type t76  = MF_Fy_r(t75);
    real_type t80  = (-t69 * t68 * t66 + t68 * t72 * t76) * t55;
    real_type t82  = L__[3];
    real_type t84  = ModelPars[37];
    real_type t85  = t84 * X__[6];
    real_type t86  = sin(t63);
    real_type t88  = ModelPars[25];
    real_type t91  = ModelPars[0];
    real_type t92  = t5 * t5;
    real_type t95  = (t88 * t8 * t51 - t86 * t65 - t92 * t91 + t85) * t82;
    real_type t96  = 1.0 / t88;
    real_type t97  = t11 * t96;
    real_type t98  = t12 * t97;
    real_type t100 = L__[4];
    real_type t105 = (-t88 * t5 * t51 + t69 * t65 + t76) * t100;
    real_type t107 = L__[5];
    real_type t110 = (t63 - U__[1]) * t107;
    real_type t112 = L__[6];
    real_type t115 = (t85 - U__[0]) * t112;
    result__[ 0   ] = -t12 * t11 * t33 - t45 * t43 * t42 - t98 * t105 + t13 * t110 + t13 * t115 - t14 * t13 - t20 * t13 - t29 * t13 + t13 * t40 + t13 * t53 + t13 * t80 - t25 * t24 + t31 * t24 - t98 * t95;
    real_type t117 = t10 * t10;
    real_type t118 = 1.0 / t117;
    real_type t119 = t23 * t118;
    real_type t120 = t7 * t2;
    real_type t122 = t4 * t2;
    real_type t124 = -t5 * t120 - t8 * t122;
    real_type t131 = t118 * t33;
    real_type t139 = t124 * t119;
    real_type t141 = t12 * t42;
    real_type t146 = t96 * t95;
    real_type t148 = t96 * t105;
    result__[ 1   ] = t124 * t14 * t119 + t124 * t20 * t119 + t124 * t29 * t119 + t124 * t23 * t131 + t24 * (t8 * t120 - t5 * t122) * t36 - t139 * t40 + t11 * t124 * t141 - t139 * t53 - t139 * t80 + t139 * t146 + t139 * t148 - t139 * t110 - t139 * t115;
    real_type t154 = ALIAS_speed_limit_D(t5);
    real_type t164 = t4 * t119;
    real_type t169 = MF_Fy_f_D(t64);
    real_type t170 = t58 * t169;
    real_type t171 = 1.0 / t92;
    real_type t174 = t69 * t68 * t56;
    real_type t176 = MF_Fy_r_D(t75);
    real_type t177 = t74 * t176;
    real_type t191 = t23 * t97;
    real_type t197 = t88 * t51;
    result__[ 2   ] = t4 * t14 * t119 - t154 * t24 + t4 * t20 * t119 + t4 * t29 * t119 + t4 * t23 * t131 - t24 * t7 * t36 - t164 * t40 + t11 * t4 * t141 - t164 * t53 + t24 * (-t68 * t72 * t171 * t177 - t174 * t171 * t170) * t55 - t164 * t80 - t191 * (-t86 * t171 * t170 - 2 * t5 * t91) * t82 + t164 * t146 - t191 * (t69 * t171 * t170 - t171 * t177 - t197) * t100 + t164 * t148 - t164 * t110 - t164 * t115;
    real_type t214 = t7 * t119;
    real_type t219 = t59 * t169;
    real_type t221 = t59 * t176;
    result__[ 3   ] = -t7 * t14 * t119 - t7 * t20 * t119 - t7 * t29 * t119 - t7 * t23 * t131 - t24 * t4 * t36 + t214 * t40 - t11 * t7 * t141 + t214 * t53 + t24 * (-t68 * t72 * t221 + t174 * t219) * t55 + t214 * t80 - t191 * (t86 * t219 + t197) * t82 - t214 * t146 - t191 * (-t69 * t219 - t221) * t100 - t214 * t148 + t214 * t110 + t214 * t115;
    real_type t242 = t50 * t169;
    real_type t243 = t56 * t56;
    real_type t248 = t50 * t176;
    real_type t249 = t72 * t72;
    real_type t256 = t59 * t56;
    result__[ 4   ] = t24 * t50 * t42 + t24 * (t69 * t68 * t59 * t243 * t242 + t68 * t59 * t249 * t248) * t55 - t191 * (t86 * t256 * t242 + t88 * t8 * t50) * t82 - t191 * (-t69 * t256 * t242 + t59 * t72 * t248 - t88 * t5 * t50) * t100;
    real_type t273 = t62 * t169;
    real_type t282 = t62 * t65;
    result__[ 5   ] = t24 * (t86 * t62 * t68 * t66 - t174 * t273) * t55 - t191 * (-t86 * t273 - t69 * t282) * t82 - t191 * (t69 * t273 - t86 * t282) * t100 + t24 * t62 * t107;
    result__[ 6   ] = t24 * t84 * t112 - t191 * t84 * t82;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hx_eval",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::DHxDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby_MLT::DHxDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby_MLT::DHxDx_nnz() const
  { return 44; }

  void
  OCP_eRumby_MLT::DHxDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 0   ; jIndex[ 4  ] = 4   ;
    iIndex[ 5  ] = 0   ; jIndex[ 5  ] = 5   ;
    iIndex[ 6  ] = 0   ; jIndex[ 6  ] = 6   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 0   ;
    iIndex[ 8  ] = 1   ; jIndex[ 8  ] = 1   ;
    iIndex[ 9  ] = 1   ; jIndex[ 9  ] = 2   ;
    iIndex[ 10 ] = 1   ; jIndex[ 10 ] = 3   ;
    iIndex[ 11 ] = 1   ; jIndex[ 11 ] = 4   ;
    iIndex[ 12 ] = 1   ; jIndex[ 12 ] = 5   ;
    iIndex[ 13 ] = 1   ; jIndex[ 13 ] = 6   ;
    iIndex[ 14 ] = 2   ; jIndex[ 14 ] = 0   ;
    iIndex[ 15 ] = 2   ; jIndex[ 15 ] = 1   ;
    iIndex[ 16 ] = 2   ; jIndex[ 16 ] = 2   ;
    iIndex[ 17 ] = 2   ; jIndex[ 17 ] = 3   ;
    iIndex[ 18 ] = 2   ; jIndex[ 18 ] = 4   ;
    iIndex[ 19 ] = 2   ; jIndex[ 19 ] = 5   ;
    iIndex[ 20 ] = 2   ; jIndex[ 20 ] = 6   ;
    iIndex[ 21 ] = 3   ; jIndex[ 21 ] = 0   ;
    iIndex[ 22 ] = 3   ; jIndex[ 22 ] = 1   ;
    iIndex[ 23 ] = 3   ; jIndex[ 23 ] = 2   ;
    iIndex[ 24 ] = 3   ; jIndex[ 24 ] = 3   ;
    iIndex[ 25 ] = 3   ; jIndex[ 25 ] = 4   ;
    iIndex[ 26 ] = 3   ; jIndex[ 26 ] = 5   ;
    iIndex[ 27 ] = 3   ; jIndex[ 27 ] = 6   ;
    iIndex[ 28 ] = 4   ; jIndex[ 28 ] = 0   ;
    iIndex[ 29 ] = 4   ; jIndex[ 29 ] = 1   ;
    iIndex[ 30 ] = 4   ; jIndex[ 30 ] = 2   ;
    iIndex[ 31 ] = 4   ; jIndex[ 31 ] = 3   ;
    iIndex[ 32 ] = 4   ; jIndex[ 32 ] = 4   ;
    iIndex[ 33 ] = 4   ; jIndex[ 33 ] = 5   ;
    iIndex[ 34 ] = 5   ; jIndex[ 34 ] = 0   ;
    iIndex[ 35 ] = 5   ; jIndex[ 35 ] = 1   ;
    iIndex[ 36 ] = 5   ; jIndex[ 36 ] = 2   ;
    iIndex[ 37 ] = 5   ; jIndex[ 37 ] = 3   ;
    iIndex[ 38 ] = 5   ; jIndex[ 38 ] = 4   ;
    iIndex[ 39 ] = 5   ; jIndex[ 39 ] = 5   ;
    iIndex[ 40 ] = 6   ; jIndex[ 40 ] = 0   ;
    iIndex[ 41 ] = 6   ; jIndex[ 41 ] = 1   ;
    iIndex[ 42 ] = 6   ; jIndex[ 42 ] = 2   ;
    iIndex[ 43 ] = 6   ; jIndex[ 43 ] = 3   ;
  }

  void
  OCP_eRumby_MLT::DHxDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t2   = ModelPars[54];
    real_type t3   = t2 * X__[1];
    real_type t4   = cos(t3);
    real_type t5   = X__[2];
    real_type t7   = sin(t3);
    real_type t8   = X__[3];
    real_type t10  = t5 * t4 - t8 * t7;
    real_type t11  = 1.0 / t10;
    real_type t12  = Q__[1];
    real_type t13  = t12 * t11;
    real_type t14  = X__[0];
    real_type t15  = Q__[0];
    real_type t16  = ALIAS_rightWidth(t15);
    real_type t17  = t14 + t16;
    real_type t18  = ALIAS_roadRightLateralBoundaries_D(t17);
    real_type t22  = t14 * t12 - 1;
    real_type t23  = t22 * t11;
    real_type t24  = ALIAS_roadRightLateralBoundaries_DD(t17);
    real_type t26  = ALIAS_leftWidth(t15);
    real_type t27  = t26 - t14;
    real_type t28  = ALIAS_roadLeftLateralBoundaries_D(t27);
    real_type t31  = ALIAS_roadLeftLateralBoundaries_DD(t27);
    result__[ 0   ] = -2 * t18 * t13 + 2 * t28 * t13 - t24 * t23 - t31 * t23;
    real_type t33  = t10 * t10;
    real_type t34  = 1.0 / t33;
    real_type t35  = t12 * t34;
    real_type t36  = speed_limit(t5);
    real_type t37  = t7 * t2;
    real_type t39  = t4 * t2;
    real_type t41  = -t5 * t37 - t8 * t39;
    real_type t42  = t41 * t36;
    real_type t44  = roadRightLateralBoundaries(t17);
    real_type t45  = t41 * t44;
    real_type t47  = t22 * t34;
    real_type t50  = roadLeftLateralBoundaries(t27);
    real_type t51  = t41 * t50;
    real_type t55  = ModelPars[34];
    real_type t56  = t34 * t55;
    real_type t59  = L__[0];
    real_type t63  = (t8 * t37 - t5 * t39) * t59;
    real_type t68  = (-t8 * t4 - t5 * t7) * t59;
    real_type t69  = t41 * t35;
    real_type t71  = L__[1];
    real_type t72  = t12 * t12;
    real_type t73  = t72 * t71;
    real_type t74  = 1.0 / t22;
    real_type t81  = ModelPars[40];
    real_type t82  = t81 * X__[4];
    real_type t84  = (t74 * t10 * t12 + t82) * t71;
    real_type t86  = L__[2];
    real_type t87  = ModelPars[13];
    real_type t89  = t87 * t82 + t8;
    real_type t90  = 1.0 / t5;
    real_type t93  = ModelPars[50];
    real_type t94  = t93 * X__[5];
    real_type t95  = -t90 * t89 + t94;
    real_type t96  = MF_Fy_f(t95);
    real_type t97  = t87 * t96;
    real_type t99  = 1.0 / ModelPars[9];
    real_type t100 = cos(t94);
    real_type t101 = t100 * t99;
    real_type t103 = ModelPars[14];
    real_type t105 = t103 * t82 - t8;
    real_type t106 = t90 * t105;
    real_type t107 = MF_Fy_r(t106);
    real_type t111 = (t99 * t103 * t107 - t101 * t97) * t86;
    real_type t113 = L__[3];
    real_type t115 = ModelPars[37];
    real_type t116 = t115 * X__[6];
    real_type t117 = sin(t94);
    real_type t119 = ModelPars[25];
    real_type t122 = ModelPars[0];
    real_type t123 = t5 * t5;
    real_type t127 = 1.0 / t119;
    real_type t128 = t127 * (t119 * t8 * t82 - t117 * t96 - t123 * t122 + t116) * t113;
    real_type t130 = L__[4];
    real_type t136 = t127 * (-t119 * t5 * t82 + t100 * t96 + t107) * t130;
    real_type t138 = L__[5];
    real_type t141 = (t94 - U__[1]) * t138;
    real_type t143 = L__[6];
    real_type t146 = (t116 - U__[0]) * t143;
    result__[ 1   ] = t11 * t74 * t41 * t73 + t41 * t12 * t56 + t41 * t18 * t47 - t41 * t28 * t47 - t69 * t111 + t69 * t128 + t13 * t63 + t69 * t136 - t69 * t141 - t69 * t146 + t42 * t35 + t45 * t35 + t51 * t35 - t69 * t68 - t69 * t84;
    real_type t148 = t4 * t36;
    real_type t150 = ALIAS_speed_limit_D(t5);
    real_type t152 = t4 * t44;
    real_type t156 = t4 * t50;
    real_type t162 = t7 * t59;
    real_type t164 = t4 * t35;
    real_type t170 = MF_Fy_f_D(t95);
    real_type t171 = t89 * t170;
    real_type t172 = 1.0 / t123;
    real_type t173 = t172 * t171;
    real_type t174 = t99 * t87;
    real_type t175 = t100 * t174;
    real_type t177 = MF_Fy_r_D(t106);
    real_type t178 = t105 * t177;
    real_type t179 = t103 * t172;
    real_type t183 = (-t99 * t179 * t178 - t175 * t173) * t86;
    real_type t186 = t117 * t172;
    real_type t191 = (-2 * t5 * t122 - t186 * t171) * t113;
    real_type t192 = t11 * t127;
    real_type t193 = t12 * t192;
    real_type t199 = t119 * t82;
    real_type t201 = (t100 * t172 * t171 - t172 * t178 - t199) * t130;
    result__[ 2   ] = t11 * t74 * t4 * t73 + t4 * t12 * t56 + t4 * t18 * t47 - t4 * t28 * t47 - t164 * t111 + t164 * t128 - t150 * t13 - t13 * t162 + t13 * t183 + t164 * t136 - t164 * t141 - t164 * t146 + t148 * t35 + t152 * t35 + t156 * t35 - t164 * t68 - t164 * t84 - t193 * t191 - t193 * t201;
    real_type t218 = t4 * t59;
    real_type t220 = t7 * t35;
    real_type t226 = t90 * t170;
    real_type t228 = t90 * t177;
    real_type t229 = t99 * t103;
    real_type t232 = (t175 * t226 - t229 * t228) * t86;
    real_type t237 = (t117 * t226 + t199) * t113;
    real_type t242 = (-t100 * t226 - t228) * t130;
    result__[ 3   ] = -t11 * t74 * t7 * t73 - t7 * t12 * t56 - t7 * t18 * t47 + t7 * t28 * t47 - t7 * t36 * t35 - t7 * t44 * t35 - t7 * t50 * t35 + t220 * t111 - t220 * t128 - t13 * t218 + t13 * t232 - t220 * t136 + t220 * t141 + t220 * t146 - t193 * t237 - t193 * t242 + t220 * t68 + t220 * t84;
    real_type t247 = t81 * t71;
    real_type t249 = t81 * t170;
    real_type t250 = t87 * t87;
    real_type t251 = t250 * t249;
    real_type t252 = t99 * t90;
    real_type t255 = t81 * t177;
    real_type t256 = t103 * t103;
    real_type t261 = (t99 * t90 * t256 * t255 + t100 * t252 * t251) * t86;
    real_type t263 = t90 * t87;
    real_type t264 = t117 * t263;
    real_type t269 = (t119 * t8 * t81 + t264 * t249) * t113;
    real_type t273 = t100 * t263;
    real_type t278 = (t90 * t103 * t255 - t119 * t5 * t81 - t273 * t249) * t130;
    result__[ 4   ] = t13 * t247 + t13 * t261 - t193 * t269 - t193 * t278;
    real_type t280 = t93 * t170;
    real_type t283 = t117 * t93 * t99;
    real_type t286 = (-t175 * t280 + t283 * t97) * t86;
    real_type t289 = t93 * t96;
    real_type t292 = (-t100 * t289 - t117 * t280) * t113;
    real_type t297 = (t100 * t280 - t117 * t289) * t130;
    real_type t299 = t93 * t138;
    result__[ 5   ] = t13 * t286 + t13 * t299 - t193 * t292 - t193 * t297;
    real_type t301 = t115 * t113;
    real_type t303 = t115 * t143;
    result__[ 6   ] = t13 * t303 - t193 * t301;
    result__[ 7   ] = result__[1];
    real_type t305 = t2 * t2;
    real_type t306 = t4 * t305;
    real_type t308 = t7 * t305;
    real_type t310 = -t5 * t306 + t8 * t308;
    real_type t311 = t310 * t47;
    real_type t314 = 1.0 / t33 / t10;
    real_type t315 = t22 * t314;
    real_type t316 = t41 * t41;
    real_type t317 = t316 * t315;
    real_type t323 = t41 * t47;
    real_type t338 = -t311 * t111 + 2 * t317 * t111 + t311 * t128 - 2 * t317 * t128 + t311 * t136 - 2 * t317 * t136 - t311 * t146 + 2 * t317 * t146 - t311 * t68 - t311 * t84 + 2 * t317 * t68 + 2 * t317 * t84 - 2 * t323 * t63;
    real_type t347 = t314 * t55;
    real_type t358 = t12 * t71;
    real_type t374 = -t311 * t141 + 2 * t317 * t141 - 2 * t316 * t36 * t315 + t310 * t36 * t47 - 2 * t316 * t22 * t347 + t310 * t22 * t56 + t23 * (t8 * t306 + t5 * t308) * t59 - 2 * t34 * t316 * t358 + t11 * t310 * t358 - 2 * t316 * t44 * t315 - 2 * t316 * t50 * t315 + t310 * t44 * t47 + t310 * t50 * t47;
    result__[ 8   ] = t338 + t374;
    real_type t375 = t37 * t47;
    real_type t378 = t4 * t41 * t315;
    real_type t384 = t314 * t68;
    real_type t385 = t41 * t22;
    real_type t386 = t4 * t385;
    real_type t389 = t34 * t68;
    real_type t390 = t2 * t22;
    real_type t391 = t7 * t390;
    real_type t393 = t314 * t84;
    real_type t396 = t34 * t84;
    real_type t398 = t314 * t111;
    real_type t401 = t34 * t111;
    real_type t403 = t314 * t146;
    real_type t406 = t34 * t146;
    real_type t408 = t34 * t141;
    real_type t410 = t314 * t141;
    real_type t413 = t127 * t191;
    real_type t415 = -t375 * t128 - 2 * t378 * t128 - t375 * t136 - 2 * t378 * t136 + t323 * t413 + 2 * t386 * t384 + 2 * t386 * t393 + 2 * t386 * t398 + 2 * t386 * t403 + 2 * t386 * t410 + t391 * t389 + t391 * t396 + t391 * t401 + t391 * t406 + t391 * t408;
    real_type t416 = t127 * t201;
    real_type t421 = t2 * t36;
    real_type t428 = t4 * t47;
    real_type t430 = t2 * t59;
    real_type t434 = t34 * t41;
    real_type t443 = t2 * t44;
    real_type t449 = t2 * t50;
    real_type t455 = -t22 * t11 * t4 * t430 - t11 * t37 * t358 + t41 * t150 * t47 - 2 * t4 * t42 * t315 - 2 * t4 * t45 * t315 - 2 * t4 * t51 * t315 - 2 * t4 * t434 * t358 - t7 * t421 * t47 - t7 * t443 * t47 - t7 * t449 * t47 + t323 * t162 - t323 * t183 + t323 * t416 - 2 * t386 * t347 - t391 * t56 - t428 * t63;
    result__[ 9   ] = t415 + t455;
    real_type t456 = t39 * t47;
    real_type t459 = t7 * t41 * t315;
    real_type t465 = t7 * t385;
    real_type t468 = t4 * t390;
    real_type t482 = t127 * t237;
    real_type t484 = -t456 * t128 + 2 * t459 * t128 - t456 * t136 + 2 * t459 * t136 + t323 * t482 - 2 * t465 * t384 + t468 * t389 - 2 * t465 * t393 + t468 * t396 - 2 * t465 * t398 + t468 * t401 - 2 * t465 * t403 + t468 * t406 + t468 * t408 - 2 * t465 * t410;
    real_type t485 = t127 * t242;
    real_type t498 = t7 * t47;
    real_type t517 = t22 * t11 * t7 * t430 - t11 * t39 * t358 + 2 * t7 * t42 * t315 + 2 * t7 * t45 * t315 + 2 * t7 * t51 * t315 + 2 * t7 * t434 * t358 - t4 * t421 * t47 - t4 * t443 * t47 - t4 * t449 * t47 + t323 * t218 - t323 * t232 + t323 * t485 + 2 * t465 * t347 - t468 * t56 + t498 * t63;
    result__[ 10  ] = t484 + t517;
    real_type t520 = t127 * t269;
    real_type t522 = t127 * t278;
    result__[ 11  ] = -t323 * t247 - t323 * t261 + t323 * t520 + t323 * t522;
    real_type t525 = t127 * t292;
    real_type t527 = t127 * t297;
    result__[ 12  ] = -t323 * t286 - t323 * t299 + t323 * t525 + t323 * t527;
    real_type t530 = t127 * t301;
    result__[ 13  ] = -t323 * t303 + t323 * t530;
    result__[ 14  ] = result__[2];
    result__[ 15  ] = result__[9];
    real_type t533 = ALIAS_speed_limit_DD(t5);
    real_type t535 = t4 * t4;
    real_type t541 = MF_Fy_f_DD(t95);
    real_type t542 = t89 * t89;
    real_type t543 = t542 * t541;
    real_type t544 = t123 * t123;
    real_type t545 = 1.0 / t544;
    real_type t549 = 1.0 / t123 / t5;
    real_type t556 = t22 * t192;
    real_type t558 = MF_Fy_r_DD(t106);
    real_type t559 = t105 * t105;
    real_type t560 = t559 * t558;
    real_type t585 = 2 * t428 * t162;
    real_type t595 = t103 * t549;
    real_type t609 = t535 * t315;
    real_type t624 = t23 * (t99 * t103 * t545 * t560 + 2 * t175 * t549 * t171 - t175 * t545 * t543 + 2 * t99 * t595 * t178) * t86 + 2 * t4 * t150 * t47 + 2 * t428 * t413 + 2 * t428 * t416 + 2 * t609 * t141 + 2 * t609 * t146 + 2 * t609 * t68 + 2 * t609 * t84 + 2 * t609 * t111 - 2 * t609 * t128 - 2 * t609 * t136;
    result__[ 16  ] = -t533 * t23 - 2 * t34 * t535 * t358 - 2 * t428 * t183 - t556 * (2 * t117 * t549 * t171 - t117 * t545 * t543 - 2 * t122) * t113 - t556 * (-2 * t100 * t549 * t171 + t100 * t545 * t543 + 2 * t549 * t178 + t545 * t560) * t130 - 2 * t535 * t36 * t315 - 2 * t535 * t44 * t315 - 2 * t535 * t50 * t315 - 2 * t535 * t22 * t347 + t585 + t624;
    real_type t627 = t549 * t541;
    real_type t630 = t172 * t170;
    real_type t635 = t549 * t558;
    real_type t637 = t172 * t177;
    real_type t654 = t7 * t4 * t22;
    real_type t663 = t7 * t7;
    real_type t668 = t498 * t183 - t428 * t232 - t556 * (t117 * t89 * t627 - t117 * t630) * t113 - t556 * (-t100 * t89 * t627 + t100 * t630 + t105 * t635 + t637) * t130 + 2 * t7 * t148 * t315 + 2 * t7 * t152 * t315 + 2 * t7 * t156 * t315 + 2 * t654 * t347 + 2 * t7 * t34 * t4 * t358 - t7 * t150 * t47 - t47 * t663 * t59 + t47 * t535 * t59;
    real_type t694 = t7 * t4 * t315;
    real_type t699 = t23 * (t99 * t103 * t105 * t635 + t175 * t89 * t627 - t175 * t630 + t229 * t637) * t86 - 2 * t654 * t398 - t498 * t413 + t428 * t482 - t498 * t416 + t428 * t485 - 2 * t654 * t410 - 2 * t654 * t403 - 2 * t654 * t384 - 2 * t654 * t393 + 2 * t694 * t128 + 2 * t694 * t136;
    result__[ 17  ] = t668 + t699;
    real_type t701 = t81 * t541;
    real_type t702 = t250 * t701;
    real_type t703 = t89 * t549;
    real_type t707 = t100 * t99 * t172;
    real_type t709 = t81 * t558;
    real_type t714 = t172 * t256;
    real_type t715 = t99 * t714;
    real_type t721 = t87 * t701;
    real_type t724 = t172 * t87;
    real_type t725 = t117 * t724;
    real_type t736 = t100 * t724;
    real_type t738 = t119 * t81;
    result__[ 18  ] = -t428 * t247 + t23 * (-t99 * t105 * t549 * t256 * t709 + t101 * t703 * t702 - t707 * t251 - t715 * t255) * t86 - t428 * t261 - t556 * (t117 * t703 * t721 - t725 * t249) * t113 + t428 * t520 - t556 * (-t100 * t703 * t721 - t105 * t595 * t709 - t179 * t255 + t736 * t249 - t738) * t130 + t428 * t522;
    real_type t743 = t93 * t541;
    real_type t747 = t117 * t93;
    real_type t754 = t172 * t89;
    real_type t757 = t93 * t172;
    result__[ 19  ] = t23 * (-t101 * t724 * t89 * t743 + t747 * t174 * t173) * t86 - t428 * t286 - t556 * (-t100 * t757 * t171 - t117 * t754 * t743) * t113 + t428 * t525 - t556 * (t100 * t754 * t743 - t117 * t757 * t171) * t130 + t428 * t527 - t428 * t299;
    result__[ 20  ] = -t428 * t303 + t428 * t530;
    result__[ 21  ] = result__[3];
    result__[ 22  ] = result__[10];
    result__[ 23  ] = result__[17];
    real_type t787 = t663 * t315;
    real_type t795 = t172 * t541;
    real_type t797 = t172 * t558;
    real_type t806 = t541 * t113;
    result__[ 24  ] = -2 * t663 * t36 * t315 - 2 * t663 * t44 * t315 - 2 * t663 * t50 * t315 - 2 * t663 * t22 * t347 - t585 + 2 * t787 * t68 - 2 * t34 * t663 * t358 + 2 * t787 * t84 + t23 * (-t175 * t795 + t229 * t797) * t86 + 2 * t498 * t232 + 2 * t787 * t111 + t23 * t127 * t117 * t172 * t806 - 2 * t498 * t482 - 2 * t787 * t128 - t556 * (t100 * t795 + t797) * t130 - 2 * t498 * t485 - 2 * t787 * t136 + 2 * t787 * t141 + 2 * t787 * t146;
    result__[ 25  ] = t498 * t247 + t23 * (-t707 * t702 - t715 * t709) * t86 + t498 * t261 - t556 * (-t725 * t701 + t738) * t113 - t498 * t520 - t556 * (-t179 * t709 + t736 * t701) * t130 - t498 * t522;
    result__[ 26  ] = t23 * (t175 * t90 * t743 - t283 * t87 * t226) * t86 + t498 * t286 - t556 * (t100 * t93 * t226 + t117 * t90 * t743) * t113 - t498 * t525 - t556 * (-t100 * t90 * t743 + t747 * t226) * t130 - t498 * t527 + t498 * t299;
    result__[ 27  ] = t498 * t303 - t498 * t530;
    result__[ 28  ] = result__[4];
    result__[ 29  ] = result__[11];
    result__[ 30  ] = result__[18];
    result__[ 31  ] = result__[25];
    real_type t871 = t81 * t81;
    real_type t872 = t871 * t541;
    real_type t876 = t871 * t558;
    result__[ 32  ] = t23 * (t99 * t172 * t256 * t103 * t876 - t707 * t250 * t87 * t872) * t86 + t556 * t186 * t250 * t871 * t806 - t556 * (t100 * t172 * t250 * t872 + t714 * t876) * t130;
    real_type t895 = t81 * t743;
    real_type t905 = t87 * t249;
    real_type t906 = t93 * t90;
    result__[ 33  ] = t23 * (t101 * t90 * t250 * t895 - t747 * t252 * t251) * t86 - t556 * (t100 * t906 * t905 + t264 * t895) * t113 - t556 * (t117 * t906 * t905 - t273 * t895) * t130;
    result__[ 34  ] = result__[5];
    result__[ 35  ] = result__[12];
    result__[ 36  ] = result__[19];
    result__[ 37  ] = result__[26];
    result__[ 38  ] = result__[33];
    real_type t918 = t93 * t93;
    real_type t919 = t918 * t541;
    real_type t921 = t918 * t170;
    real_type t934 = t918 * t96;
    result__[ 39  ] = t23 * (t100 * t918 * t99 * t97 + 2 * t117 * t174 * t921 - t175 * t919) * t86 - t556 * (-2 * t100 * t921 - t117 * t919 + t117 * t934) * t113 - t556 * (t100 * t919 - t100 * t934 - 2 * t117 * t921) * t130;
    result__[ 40  ] = result__[6];
    result__[ 41  ] = result__[13];
    result__[ 42  ] = result__[20];
    result__[ 43  ] = result__[27];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DHxDx_sparse",44);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::DHxDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby_MLT::DHxDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby_MLT::DHxDp_nnz() const
  { return 0; }

  void
  OCP_eRumby_MLT::DHxDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::DHxDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |  _   _
   | | | | |_   _
   | | |_| | | | |
   | |  _  | |_| |
   | |_| |_|\__,_|
   |
  \*/

  integer
  OCP_eRumby_MLT::Hu_numEqns() const
  { return 2; }

  void
  OCP_eRumby_MLT::Hu_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t4   = X__[1] * ModelPars[54];
    real_type t5   = cos(t4);
    real_type t8   = sin(t4);
    real_type t12  = 1.0 / (X__[2] * t5 - X__[3] * t8);
    real_type t17  = Q__[1] * X__[0] - 1;
    result__[ 0   ] = -t17 * t12 * L__[6];
    result__[ 1   ] = -t17 * t12 * L__[5];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hu_eval",2);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::DHuDx_numRows() const
  { return 2; }

  integer
  OCP_eRumby_MLT::DHuDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby_MLT::DHuDx_nnz() const
  { return 8; }

  void
  OCP_eRumby_MLT::DHuDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 1   ; jIndex[ 4  ] = 0   ;
    iIndex[ 5  ] = 1   ; jIndex[ 5  ] = 1   ;
    iIndex[ 6  ] = 1   ; jIndex[ 6  ] = 2   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 3   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::DHuDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = L__[6];
    real_type t3   = ModelPars[54];
    real_type t4   = t3 * X__[1];
    real_type t5   = cos(t4);
    real_type t6   = X__[2];
    real_type t8   = sin(t4);
    real_type t9   = X__[3];
    real_type t11  = t6 * t5 - t9 * t8;
    real_type t12  = 1.0 / t11;
    real_type t14  = Q__[1];
    result__[ 0   ] = -t14 * t12 * t1;
    real_type t16  = t11 * t11;
    real_type t17  = 1.0 / t16;
    real_type t18  = t17 * t1;
    real_type t21  = X__[0] * t14 - 1;
    real_type t27  = (-t9 * t5 * t3 - t6 * t8 * t3) * t21;
    result__[ 1   ] = t27 * t18;
    real_type t28  = t5 * t21;
    result__[ 2   ] = t28 * t18;
    real_type t29  = t8 * t21;
    result__[ 3   ] = -t29 * t18;
    real_type t31  = L__[5];
    result__[ 4   ] = -t14 * t12 * t31;
    real_type t34  = t17 * t31;
    result__[ 5   ] = t27 * t34;
    result__[ 6   ] = t28 * t34;
    result__[ 7   ] = -t29 * t34;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DHuDx_sparse",8);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::DHuDp_numRows() const
  { return 2; }

  integer
  OCP_eRumby_MLT::DHuDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby_MLT::DHuDp_nnz() const
  { return 0; }

  void
  OCP_eRumby_MLT::DHuDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::DHuDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |  _   _
   | | | | |_ __
   | | |_| | '_ \
   | |  _  | |_) |
   | |_| |_| .__/
   |       |_|
  \*/

  integer
  OCP_eRumby_MLT::Hp_numEqns() const
  { return 0; }

  void
  OCP_eRumby_MLT::Hp_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);

    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hp_eval",0);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::DHpDp_numRows() const
  { return 0; }

  integer
  OCP_eRumby_MLT::DHpDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby_MLT::DHpDp_nnz() const
  { return 0; }

  void
  OCP_eRumby_MLT::DHpDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::DHpDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    L_const_pointer_type L__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |        _
   |    ___| |_ __ _
   |   / _ \ __/ _` |
   |  |  __/ || (_| |
   |   \___|\__\__,_|
  \*/

  integer
  OCP_eRumby_MLT::eta_numEqns() const
  { return 7; }

  void
  OCP_eRumby_MLT::eta_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = L__[0];
    result__[ 1   ] = -ModelPars[54] * L__[1];
    result__[ 2   ] = L__[3];
    result__[ 3   ] = L__[4];
    result__[ 4   ] = ModelPars[40] * L__[2];
    result__[ 5   ] = ModelPars[52] * ModelPars[50] * L__[5];
    result__[ 6   ] = ModelPars[28] * ModelPars[37] * L__[6];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"eta_eval",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::DetaDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby_MLT::DetaDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby_MLT::DetaDx_nnz() const
  { return 0; }

  void
  OCP_eRumby_MLT::DetaDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::DetaDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::DetaDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby_MLT::DetaDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby_MLT::DetaDp_nnz() const
  { return 0; }

  void
  OCP_eRumby_MLT::DetaDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::DetaDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    L_const_pointer_type L__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |    _ __  _   _
   |   | '_ \| | | |
   |   | | | | |_| |
   |   |_| |_|\__,_|
  \*/

  integer
  OCP_eRumby_MLT::nu_numEqns() const
  { return 7; }

  void
  OCP_eRumby_MLT::nu_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = V__[0];
    result__[ 1   ] = -ModelPars[54] * V__[1];
    result__[ 2   ] = ModelPars[40] * V__[4];
    result__[ 3   ] = V__[2];
    result__[ 4   ] = V__[3];
    result__[ 5   ] = ModelPars[52] * ModelPars[50] * V__[5];
    result__[ 6   ] = ModelPars[28] * ModelPars[37] * V__[6];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"nu_eval",7);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::DnuDx_numRows() const
  { return 7; }

  integer
  OCP_eRumby_MLT::DnuDx_numCols() const
  { return 7; }

  integer
  OCP_eRumby_MLT::DnuDx_nnz() const
  { return 0; }

  void
  OCP_eRumby_MLT::DnuDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::DnuDx_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MLT::DnuDp_numRows() const
  { return 7; }

  integer
  OCP_eRumby_MLT::DnuDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby_MLT::DnuDp_nnz() const
  { return 0; }

  void
  OCP_eRumby_MLT::DnuDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  OCP_eRumby_MLT::DnuDp_sparse(
    integer              i_segment,
    Q_const_pointer_type Q__,
    X_const_pointer_type X__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

}

// EOF: OCP_eRumby_MLT_Methods.cc
