/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_MLT.cc                                              |
 |                                                                       |
 |  version: 1.0   date 13/12/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


// use pragma to include libraries
#include <MechatronixCore/MechatronixLibs.hh>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
#ifdef __clang__
#pragma clang diagnostic ignored "-Wunused-parameter"
#endif

#include "OCP_eRumby_MLT.hh"
#include "OCP_eRumby_MLT_Pars.hh"

#include <time.h> /* time_t, struct tm, time, localtime, asctime */

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
#ifdef __clang__
#pragma clang diagnostic ignored "-Wunused-parameter"
#endif

namespace OCP_eRumby_MLTDefine {

  /*
  //   _ __   __ _ _ __ ___   ___  ___
  //  | '_ \ / _` | '_ ` _ \ / _ \/ __|
  //  | | | | (_| | | | | | |  __/\__ \
  //  |_| |_|\__,_|_| |_| |_|\___||___/
  //
  */

  char const *namesXvars[numXvars+1] = {
    "n",
    "xi__xo",
    "u",
    "v",
    "Omega__xo",
    "delta__xo",
    "Fx__r__xo",
    nullptr
  };

  char const *namesLvars[numLvars+1] = {
    "lambda1__xo",
    "lambda2__xo",
    "lambda3__xo",
    "lambda4__xo",
    "lambda5__xo",
    "lambda6__xo",
    "lambda7__xo",
    nullptr
  };

  char const *namesUvars[numUvars+1] = {
    "Fx__r__O",
    "delta__O",
    nullptr
  };

  char const *namesQvars[numQvars+1] = {
    "zeta",
    "Curv",
    "xLane",
    "yLane",
    "theta",
    nullptr
  };

  char const *namesPvars[numPvars+1] = {
    nullptr
  };

  char const *namesOMEGAvars[numOMEGAvars+1] = {
    "omega1__xo",
    nullptr
  };

  char const *namesPostProcess[numPostProcess+1] = {
    "Fx__r",
    "delta",
    "xi",
    "Omega",
    "delta__OControl",
    "Fx__r__OControl",
    "speed_limit",
    "roadRightLateralBoundaries",
    "roadLeftLateralBoundaries",
    "ay_SS",
    "s_dot",
    "xCoMCar",
    "yCoMCar",
    "xLeftCar",
    "yLeftCar",
    "xRightCar",
    "yRightCar",
    "xLeftEdge",
    "yLeftEdge",
    "xRightEdge",
    "yRightEdge",
    "beta",
    "a__x",
    "Delta_Fz",
    "Fy__f",
    "Fy__r",
    "alpha__r__SS",
    "alpha__f__SS",
    "mu_yr",
    "mu_yf",
    "mu_xr",
    "mayer_target",
    nullptr
  };

  char const *namesIntegratedPostProcess[numIntegratedPostProcess+1] = {
    "time",
    nullptr
  };

  char const *namesModelPars[numModelPars+1] = {
    "Ca",
    "Cf",
    "Cr",
    "Df",
    "Dr",
    "Fx_max",
    "Fx_min",
    "Fz__f0",
    "Fz__r0",
    "Izz",
    "Kf",
    "Kr",
    "L",
    "Lf",
    "Lr",
    "WBCC",
    "WBCF__n",
    "WBCF__u",
    "WBCF__v",
    "WBCI",
    "WBCI__n",
    "WBCI__u",
    "WBCI__v",
    "fx__i",
    "h",
    "m",
    "n__f",
    "n__i",
    "tau__Fx",
    "u0",
    "u__f",
    "u__i",
    "v__f",
    "v__i",
    "wT",
    "xi__f",
    "xi__i",
    "Fx__r__s_xo",
    "Omega__f",
    "Omega__i",
    "Omega__s_xo",
    "WBCF__Omega",
    "WBCF__delta",
    "WBCF__xi",
    "WBCI__Omega",
    "WBCI__delta",
    "WBCI__fx",
    "WBCI__xi",
    "delta__f",
    "delta__i",
    "delta__s_xo",
    "delta_max",
    "tau__delta",
    "vehHalfWidth",
    "xi__s_xo",
    nullptr
  };

  char const *namesConstraint1D[numConstraint1D+1] = {
    "speed_limit",
    "roadRightLateralBoundaries",
    "roadLeftLateralBoundaries",
    nullptr
  };

  char const *namesConstraint2D[numConstraint2D+1] = {
    nullptr
  };

  char const *namesConstraintU[numConstraintU+1] = {
    "delta__OControl",
    "Fx__r__OControl",
    nullptr
  };

  char const *namesBc[numBc+1] = {
    "initial_Omega",
    nullptr
  };

  /* --------------------------------------------------------------------------
  //    ___             _               _
  //   / __|___ _ _  __| |_ _ _ _  _ __| |_ ___ _ _
  //  | (__/ _ \ ' \(_-<  _| '_| || / _|  _/ _ \ '_|
  //   \___\___/_||_/__/\__|_|  \_,_\__|\__\___/_|
  */
  OCP_eRumby_MLT::OCP_eRumby_MLT(
    string const & name,
    ostream_type * _pCout,
    integer        _infoLevel
  )
  : Discretized_Indirect_OCP(name,_pCout,_infoLevel)
  // Controls
  , delta__OControl("delta__OControl")
  , Fx__r__OControl("Fx__r__OControl")
  // Constraints 1D
  , speed_limit("speed_limit")
  , roadRightLateralBoundaries("roadRightLateralBoundaries")
  , roadLeftLateralBoundaries("roadLeftLateralBoundaries")
  // Constraints 2D
  // User classes
  {
    this->u_solve_iterative = false;

    // continuation
    this->ns_continuation_begin = 0;
    this->ns_continuation_end   = 1;
    // Initialize to NaN all the ModelPars
    std::fill( ModelPars, ModelPars + numModelPars, alglin::NaN<real_type>() );

    // Initialize string of names
    setupNames(
      numPvars,                 namesPvars,
      numXvars,                 namesXvars,
      numLvars,                 namesLvars,
      numUvars,                 namesUvars,
      numQvars,                 namesQvars,
      numPostProcess,           namesPostProcess,
      numIntegratedPostProcess, namesIntegratedPostProcess,
      numBc,                    namesBc
    );
    //pSolver = &this->solverNewtonDumped;
    pSolver = &this->solverHyness;

    #ifdef LAPACK_WRAPPER_USE_OPENBLAS
    openblas_set_num_threads(1);
    goto_set_num_threads(1);
    #endif
  }

  OCP_eRumby_MLT::~OCP_eRumby_MLT() {
  }

  /* --------------------------------------------------------------------------
  //                  _       _       ____  _
  //  _   _ _ __   __| | __ _| |_ ___|  _ \| |__   __ _ ___  ___
  // | | | | '_ \ / _` |/ _` | __/ _ \ |_) | '_ \ / _` / __|/ _ \
  // | |_| | |_) | (_| | (_| | ||  __/  __/| | | | (_| \__ \  __/
  //  \__,_| .__/ \__,_|\__,_|\__\___|_|   |_| |_|\__,_|___/\___|
  //       |_|
  */
  void
  OCP_eRumby_MLT::updateContinuation( integer phase, real_type s ) {
    ASSERT(
      s >= 0 && s <= 1,
      "OCP_eRumby_MLT::updateContinuation( phase number = " << phase <<
      ", s = " << s << ") s must be in the interval [0,1]"
    );
    switch ( phase ) {
      case 0: continuationStep0( s ); break;
      default:
        MECHATRONIX_DO_ERROR(
          "OCP_eRumby_MLT::updateContinuation( phase number = " << phase <<
          ", s = " << s << ") phase N." << phase << " is not defined"
        );
    }
  }

  /* --------------------------------------------------------------------------
  //           _               ____                                _
  //  ___  ___| |_ _   _ _ __ |  _ \ __ _ _ __ __ _ _ __ ___   ___| |_ ___ _ __ ___
  // / __|/ _ \ __| | | | '_ \| |_) / _` | '__/ _` | '_ ` _ \ / _ \ __/ _ \ '__/ __|
  // \__ \  __/ |_| |_| | |_) |  __/ (_| | | | (_| | | | | | |  __/ ||  __/ |  \__ \
  // |___/\___|\__|\__,_| .__/|_|   \__,_|_|  \__,_|_| |_| |_|\___|\__\___|_|  |___/
  //                    |_|
  // initialize parameters using associative array
  */
  void
  OCP_eRumby_MLT::setupParameters( GenericContainer const & gc_data ) {
    ASSERT(
      gc_data.exists("Parameters"),
      "OCP_eRumby_MLT::setupParameters: Missing key `Parameters` in data"
    );
    GenericContainer const & gc = gc_data("Parameters");

    bool allfound = true;
    for ( integer i = 0; i < numModelPars; ++i ) {
      bool ok = gc.exists( namesModelPars[i] );
      if ( !ok ) {
        std::cerr
          << MSG_ERROR( "Missing parameter: '" << namesModelPars[i] << "'" )
          << '\n';
      } else {
        ModelPars[i] = gc(namesModelPars[i]).get_number();
      }
      allfound = allfound && ok;
    }
    ASSERT(
      allfound,
      "in OCP_eRumby_MLT::setup not all parameters are set!"
    );
  }

  void
  OCP_eRumby_MLT::setupParameters( real_type const Pars[] ) {
    std::copy( Pars, Pars + numModelPars, ModelPars );
  }

  /* --------------------------------------------------------------------------
  //            _                ____ _
  //   ___  ___| |_ _   _ _ __  / ___| | __ _ ___ ___  ___  ___
  //  / __|/ _ \ __| | | | '_ \| |   | |/ _` / __/ __|/ _ \/ __|
  //  \__ \  __/ |_| |_| | |_) | |___| | (_| \__ \__ \  __/\__ \
  //  |___/\___|\__|\__,_| .__/ \____|_|\__,_|___/___/\___||___/
  //                     |_|
  */
  void
  OCP_eRumby_MLT::setupClasses( GenericContainer const & gc_data ) {
    ASSERT(
      gc_data.exists("Constraints"),
      "OCP_eRumby_MLT::setupClasses: Missing key `Parameters` in data"
    );
    GenericContainer const & gc = gc_data("Constraints");
    // Initialize Constraints 1D
    ASSERT(
      gc.exists("speed_limit"),
      "in OCP_eRumby_MLT::setupClasses(gc) missing key: ``speed_limit''"
    );
    speed_limit.setup( gc("speed_limit") );

    ASSERT(
      gc.exists("roadRightLateralBoundaries"),
      "in OCP_eRumby_MLT::setupClasses(gc) missing key: ``roadRightLateralBoundaries''"
    );
    roadRightLateralBoundaries.setup( gc("roadRightLateralBoundaries") );

    ASSERT(
      gc.exists("roadLeftLateralBoundaries"),
      "in OCP_eRumby_MLT::setupClasses(gc) missing key: ``roadLeftLateralBoundaries''"
    );
    roadLeftLateralBoundaries.setup( gc("roadLeftLateralBoundaries") );

  }

  /* --------------------------------------------------------------------------
  //           _               _   _                ____ _
  //  ___  ___| |_ _   _ _ __ | | | |___  ___ _ __ / ___| | __ _ ___ ___  ___  ___
  // / __|/ _ \ __| | | | '_ \| | | / __|/ _ \ '__| |   | |/ _` / __/ __|/ _ \/ __|
  // \__ \  __/ |_| |_| | |_) | |_| \__ \  __/ |  | |___| | (_| \__ \__ \  __/\__ \
  // |___/\___|\__|\__,_| .__/ \___/|___/\___|_|   \____|_|\__,_|___/___/\___||___/
  //                    |_|
  */
  void
  OCP_eRumby_MLT::setupUserClasses( GenericContainer const & gc ) {
  }

  /* --------------------------------------------------------------------------
  //           _             _   _
  //   ___ ___| |_ _  _ _ __| | | |___ ___ _ _
  //  (_-</ -_)  _| || | '_ \ |_| (_-</ -_) '_|
  //  /__/\___|\__|\_,_| .__/\___//__/\___|_|
  //                   |_|
  //   __  __                        _ ___             _   _
  //  |  \/  |__ _ _ __ _ __  ___ __| | __|  _ _ _  __| |_(_)___ _ _  ___
  //  | |\/| / _` | '_ \ '_ \/ -_) _` | _| || | ' \/ _|  _| / _ \ ' \(_-<
  //  |_|  |_\__,_| .__/ .__/\___\__,_|_| \_,_|_||_\__|\__|_\___/_||_/__/
  //              |_|  |_|
  */
  void
  OCP_eRumby_MLT::setupUserMappedFunctions( GenericContainer const & gc_data ) {
    ASSERT(
      gc_data.exists("MappedObjects"),
      "OCP_eRumby_MLT::setupClasses: Missing key `MappedObjects` in data"
    );
    GenericContainer const & gc = gc_data("MappedObjects");

    // Initialize user mapped functions

    ASSERT(
      gc.exists("posPart"),
      "in OCP_eRumby_MLT::setupUserMappedFunctions(gc) missing key: ``posPart''"
    );
    posPart.setup( gc("posPart") );

    ASSERT(
      gc.exists("negPart"),
      "in OCP_eRumby_MLT::setupUserMappedFunctions(gc) missing key: ``negPart''"
    );
    negPart.setup( gc("negPart") );

    ASSERT(
      gc.exists("SignReg"),
      "in OCP_eRumby_MLT::setupUserMappedFunctions(gc) missing key: ``SignReg''"
    );
    SignReg.setup( gc("SignReg") );
  }
  /* --------------------------------------------------------------------------
  //            _                ____            _             _
  //   ___  ___| |_ _   _ _ __  / ___|___  _ __ | |_ _ __ ___ | |___
  //  / __|/ _ \ __| | | | '_ \| |   / _ \| '_ \| __| '__/ _ \| / __|
  //  \__ \  __/ |_| |_| | |_) | |__| (_) | | | | |_| | | (_) | \__ \
  //  |___/\___|\__|\__,_| .__/ \____\___/|_| |_|\__|_|  \___/|_|___/
  //                     |_|
  */
  void
  OCP_eRumby_MLT::setupControls( GenericContainer const & gc_data ) {
    // initialize Control penalties
    ASSERT(
      gc_data.exists("Controls"),
      "OCP_eRumby_MLT::setupClasses: Missing key `Controls` in data"
    );
    GenericContainer const & gc = gc_data("Controls");
    delta__OControl.setup( gc("delta__OControl") );
    Fx__r__OControl.setup( gc("Fx__r__OControl") );
    // setup iterative solver
    this->setupControlSolver( gc_data );
  }

  /* --------------------------------------------------------------------------
  //            _               ____       _       _
  //   ___  ___| |_ _   _ _ __ |  _ \ ___ (_)_ __ | |_ ___ _ __ ___
  //  / __|/ _ \ __| | | | '_ \| |_) / _ \| | '_ \| __/ _ \ '__/ __|
  //  \__ \  __/ |_| |_| | |_) |  __/ (_) | | | | | ||  __/ |  \__ \
  //  |___/\___|\__|\__,_| .__/|_|   \___/|_|_| |_|\__\___|_|  |___/
  //                     |_|
  */
  void
  OCP_eRumby_MLT::setupPointers( GenericContainer const & gc_data ) {

    ASSERT(
      gc_data.exists("Pointers"),
      "OCP_eRumby_MLT::setupPointers: Missing key `Pointers` in data"
    );
    GenericContainer const & gc = gc_data("Pointers");

    // Initialize user classes

    ASSERT(
      gc.exists("pSplineSetRoad2D"),
      "in OCP_eRumby_MLT::setupPointers(gc) cant find key `pSplineSetRoad2D' in gc"
    );
    pSplineSetRoad2D = gc("pSplineSetRoad2D").get_pointer<SplineSet*>();

    ASSERT(
      gc.exists("pTrajectory"),
      "in OCP_eRumby_MLT::setupPointers(gc) cant find key `pTrajectory' in gc"
    );
    pTrajectory = gc("pTrajectory").get_pointer<Path2D*>();
  }

  /* --------------------------------------------------------------------------
  //   _        __        ____ _
  //  (_)_ __  / _| ___  / ___| | __ _ ___ ___  ___  ___
  //  | | '_ \| |_ / _ \| |   | |/ _` / __/ __|/ _ \/ __|
  //  | | | | |  _| (_) | |___| | (_| \__ \__ \  __/\__ \
  //  |_|_| |_|_|  \___/ \____|_|\__,_|___/___/\___||___/
  */
  void
  OCP_eRumby_MLT::infoClasses( ostream_type & stream ) const {

    stream << "\nControls\n";
    delta__OControl . info(stream);
    Fx__r__OControl . info(stream);

    stream << "\nConstraints 1D\n";
    speed_limit                . info(stream);
    roadRightLateralBoundaries . info(stream);
    roadLeftLateralBoundaries  . info(stream);

    stream << "\nUser class (pointer)\n";
    stream << "User function `pSplineSetRoad2D`: ";
    pSplineSetRoad2D->info( stream );
    stream << "User function `pTrajectory`: ";
    pTrajectory->info( stream );

    stream << "\nUser mapped functions\n";
    posPart.info( stream );
    negPart.info( stream );
    SignReg.info( stream );

    stream << "\nModel Parameters\n";
    for ( integer i = 0; i < numModelPars; ++i )
      stream
        << setw(40) << setfill('.') << namesModelPars[i]
        << " = " << ModelPars[i] << setfill(' ') << '\n';

  }

  /* --------------------------------------------------------------------------
  //            _
  //   ___  ___| |_ _   _ _ __
  //  / __|/ _ \ __| | | | '_ \
  //  \__ \  __/ |_| |_| | |_) |
  //  |___/\___|\__|\__,_| .__/
  //                     |_|
  */
  void
  OCP_eRumby_MLT::setup( GenericContainer const & gc ) {

    if ( gc.get_map_bool("RedirectStreamToString") ) {
      ss_redirected_stream.str("");
      changeStream(&ss_redirected_stream);
    } else {
      changeStream(&cout);
    }

    this->setupParameters( gc );
    this->setupClasses( gc );
    this->setupUserMappedFunctions( gc );
    this->setupUserClasses( gc );
    this->setupPointers( gc );
    this->setupBC( gc );
    this->setupControls( gc );

    // setup nonlinear system with object handling mesh domain
    this->setup( pTrajectory, gc );
    if ( Indirect_OCP::infoLevel >= 2 ) this->infoBC( *Indirect_OCP::pCout );
    if ( Indirect_OCP::infoLevel >= 3 ) this->infoClasses( *Indirect_OCP::pCout );
    if ( Indirect_OCP::infoLevel >= 2 ) this->info( *Indirect_OCP::pCout );
  }

  /* --------------------------------------------------------------------------
  //              _
  //    __ _  ___| |_     _ __   __ _ _ __ ___   ___  ___
  //   / _` |/ _ \ __|   | '_ \ / _` | '_ ` _ \ / _ \/ __|
  //  | (_| |  __/ |_    | | | | (_| | | | | | |  __/\__ \
  //   \__, |\___|\__|___|_| |_|\__,_|_| |_| |_|\___||___/
  //   |___/        |_____|
  */
  void
  OCP_eRumby_MLT::get_names( GenericContainer & out ) const {
    GenericContainer::vec_string_type & X_names =
      out["state_names"].set_vec_string();
    for ( integer i = 0; i < numXvars; ++i )
      X_names.push_back(namesXvars[i]);

    GenericContainer::vec_string_type & LM_names =
      out["lagrange_multiplier_names"].set_vec_string();
    for ( integer i = 0; i < numLvars; ++i )
      LM_names.push_back(namesLvars[i]);

    GenericContainer::vec_string_type & U_names =
      out["control_names"].set_vec_string();
    for ( integer i = 0; i < numUvars; ++i )
      U_names.push_back(namesUvars[i]);

    GenericContainer::vec_string_type & Q_names =
      out["mesh_variable_names"].set_vec_string();
    for ( integer i = 0; i < numQvars; ++i )
      Q_names.push_back(namesQvars[i]);

    GenericContainer::vec_string_type & P_names =
      out["parameter_names"].set_vec_string();
    for ( integer i = 0; i < numPvars; ++i )
      P_names.push_back(namesPvars[i]);

    GenericContainer::vec_string_type & OMEGA_names =
      out["bc_lagrange_multiplier_names"].set_vec_string();
    for ( integer i = 0; i < numOMEGAvars; ++i )
      OMEGA_names.push_back(namesOMEGAvars[i]);

    GenericContainer::vec_string_type & PP_names =
      out["post_processing_names"].set_vec_string();
    for ( integer i = 0; i < numPostProcess; ++i )
      PP_names.push_back(namesPostProcess[i]);

    for ( integer i = 0; i < numIntegratedPostProcess; ++i )
      PP_names.push_back(namesIntegratedPostProcess[i]);

    GenericContainer::vec_string_type & model_names =
      out["model_names"].set_vec_string();
    for ( integer i = 0; i < numModelPars; ++i )
      model_names.push_back(namesModelPars[i]);
  }

  /* --------------------------------------------------------------------------
  //      _ _                       _   _
  //   __| (_)__ _ __ _ _ _  ___ __| |_(_)__
  //  / _` | / _` / _` | ' \/ _ (_-<  _| / _|
  //  \__,_|_\__,_\__, |_||_\___/__/\__|_\__|
  //              |___/
  */
  void
  OCP_eRumby_MLT::diagnostic( GenericContainer const & gc ) {

    // DA RIFARE--------------

    // If required save function and jacobian
    //if ( gc.exists("DumpFile") )
    //  this->dumpFunctionAndJacobian( pSolver->solution(),
    //                                 gc("DumpFile").get_string() );

    //bool do_diagnosis = gc.get_map_bool("Doctor");
    //if ( do_diagnosis )
    //  this->diagnosis( pSolver->solution(), gc["diagnosis"] );

    real_type epsi = 1e-5;
    gc.get_if_exists("JacobianCheck_epsilon",epsi);
    if ( gc.get_map_bool("JacobianCheck") )
      this->checkJacobian( pSolver->solution(), epsi, *Indirect_OCP::pCout );
    if ( gc.get_map_bool("JacobianCheckFull") )
      this->checkJacobianFull( pSolver->solution(), epsi, *Indirect_OCP::pCout );
  }

  // save model parameters
  void
  OCP_eRumby_MLT::save_OCP_info( GenericContainer & gc ) const {
    for ( integer i = 0; i < numModelPars; ++i )
      gc[namesModelPars[i]] = ModelPars[i]; 

  }

}

// EOF: OCP_eRumby_MLT.cc
