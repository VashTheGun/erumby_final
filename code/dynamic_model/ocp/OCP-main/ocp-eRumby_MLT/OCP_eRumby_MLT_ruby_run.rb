#!/usr/bin/env ruby
#-----------------------------------------------------------------------#
#  file: OCP_eRumby_MLT_ruby_run.rb                                     #
#                                                                       #
#  version: 1.0   date 13/12/2019                                       #
#                                                                       #
#  Copyright (C) 2019                                                   #
#                                                                       #
#      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             #
#      Dipartimento di Ingegneria Industriale                           #
#      Universita` degli Studi di Trento                                #
#      Via Sommarive 9, I-38123, Trento, Italy                          #
#      email: enrico.bertolazzi@unitn.it                                #
#             francesco.biral@unitn.it                                  #
#             paolo.bosetti@unitn.it                                    #
#-----------------------------------------------------------------------#


require './ocp-interfaces/Ruby/OCP_eRumby_MLT_ffi.rb'
require './data/OCP_eRumby_MLT_Data.rb'

ocp = OCP_eRumby_MLT::OCPSolver.new

begin
  res = ocp.solve
  if res[:converged] then
    ocp.write_ocp_solution("data/OCP_eRumby_MLT_OCP_result.txt")
  else
    ocp.write_ocp_solution("data/OCP_eRumby_MLT_OCP_result_NOT_CONVERGED.txt")
  end
rescue RuntimeError => e
  puts e
  ocp.write_ocp_solution("data/OCP_eRumby_MLT_OCP_result_NOT_CONVERGED.txt")
rescue => e
  puts e.message
  puts "Trace"
  e.backtrace.each do |t|
    puts t
  end
rescue
  puts "Unknown error!"
end

if ARGV.include? "-g" then
  dg = Dgraph.new
  dg.set_fields %w(z x v)
  dg.plot "z", "x"
  dg.plot "z", "v"
  res[:data][0].each_with_index do |z,i|
    dg << {z:z, x:res[:data][1][i], v:res[:data][2][i]}
  end
  dg.close
end

puts "All Done Folks!"

# EOF
