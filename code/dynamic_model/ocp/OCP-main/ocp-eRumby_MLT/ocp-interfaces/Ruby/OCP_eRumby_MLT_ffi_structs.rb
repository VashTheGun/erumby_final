#!/usr/bin/env ruby
############################################################################
#                                                                          #
#  file: OCP_eRumby_MLT_ffi_structs.rb                                     #
#                                                                          #
#  version: 1.0   date 13/12/2019                                          #
#                                                                          #
#  Copyright (C) 2019                                                      #
#                                                                          #
#      Enrico Bertolazzi and Francesco Biral and Paolo Bosetti             #
#      Dipartimento di Ingegneria Industriale                              #
#      Universita` degli Studi di Trento                                   #
#      Via Mesiano 77, I-38050 Trento, Italy                               #
#      email: enrico.bertolazzi@ing.unitn.it                               #
#             francesco.biral@ing.unitn.it                                 #
#             paolo.bosetti@ing.unitn.it                                   #
#                                                                          #
############################################################################

module OCP_eRumby_MLT

  typedef :double, :data_t
  typedef :uint32, :index_t
  typedef :int,    :retcode

  class OCP_eRumby_MLT_solver_params < FFI::Struct
    layout(
      :max_iter,             :index_t,
      :max_step_iter,        :index_t,
      :max_accumulated_iter, :index_t,
      :tolerance,            :data_t,
    )
    def initialize
      self[:max_iter]      = 500
      self[:max_step_iter] = 20
      self[:tolerance]     = 1e-10
    end
  end

  class OCP_eRumby_MLT_model_params < FFI::Struct
    layout(

      :Ca,           :data_t,

      :Cf,           :data_t,

      :Cr,           :data_t,

      :Df,           :data_t,

      :Dr,           :data_t,

      :Fx_max,       :data_t,

      :Fx_min,       :data_t,

      :Fz__f0,       :data_t,

      :Fz__r0,       :data_t,

      :Izz,          :data_t,

      :Kf,           :data_t,

      :Kr,           :data_t,

      :L,            :data_t,

      :Lf,           :data_t,

      :Lr,           :data_t,

      :WBCC,         :data_t,

      :WBCF__n,      :data_t,

      :WBCF__u,      :data_t,

      :WBCF__v,      :data_t,

      :WBCI,         :data_t,

      :WBCI__n,      :data_t,

      :WBCI__u,      :data_t,

      :WBCI__v,      :data_t,

      :fx__i,        :data_t,

      :h,            :data_t,

      :m,            :data_t,

      :n__f,         :data_t,

      :n__i,         :data_t,

      :tau__Fx,      :data_t,

      :u0,           :data_t,

      :u__f,         :data_t,

      :u__i,         :data_t,

      :v__f,         :data_t,

      :v__i,         :data_t,

      :wT,           :data_t,

      :xi__f,        :data_t,

      :xi__i,        :data_t,

      :Fx__r__s_xo,  :data_t,

      :Omega__f,     :data_t,

      :Omega__i,     :data_t,

      :Omega__s_xo,  :data_t,

      :WBCF__Omega,  :data_t,

      :WBCF__delta,  :data_t,

      :WBCF__xi,     :data_t,

      :WBCI__Omega,  :data_t,

      :WBCI__delta,  :data_t,

      :WBCI__fx,     :data_t,

      :WBCI__xi,     :data_t,

      :delta__f,     :data_t,

      :delta__i,     :data_t,

      :delta__s_xo,  :data_t,

      :delta_max,    :data_t,

      :tau__delta,   :data_t,

      :vehHalfWidth, :data_t,

      :xi__s_xo,     :data_t,

    )

    def initialize
      members.each { |m| self[m] = Float::NAN }
      # Custom initializations go here:
      # self[:key] = value
    end
  end

  class OCP_eRumby_MLT_BC_params < FFI::Struct
    layout(

      :initial_Omega, :bool,

    )

    def initialize
      members.each { |m| self[m] = true }
      # Custom initializations go here:
      # self[:key] = value
    end
  end

  class OCP_eRumby_MLT_constraints_params < FFI::Struct
    layout(
      # 1D constraints
      :speed_limitSubType,                  :index_t,
      :speed_limitEpsilon,                  :data_t,
      :speed_limitTolerance,                :data_t,
      :speed_limitMinValue,                 :data_t,
      :speed_limitMaxValue,                 :data_t,
      :roadRightLateralBoundariesSubType,   :index_t,
      :roadRightLateralBoundariesEpsilon,   :data_t,
      :roadRightLateralBoundariesTolerance, :data_t,
      :roadLeftLateralBoundariesSubType,    :index_t,
      :roadLeftLateralBoundariesEpsilon,    :data_t,
      :roadLeftLateralBoundariesTolerance,  :data_t,

      # 2D constraints

      # Controls
      :delta__OControlType,      :index_t,
      :delta__OControlEpsilon,   :data_t,
      :delta__OControlTolerance, :data_t,
      :Fx__r__OControlType,      :index_t,
      :Fx__r__OControlEpsilon,   :data_t,
      :Fx__r__OControlTolerance, :data_t,
    )

    def initialize
      members.each do |m|
        case self[m]
        when Float
          self[m] = Float::NAN
        when Fixnum
          self[m] = 0
        when FFI::Pointer
          self[m] = nil
        else
          warn "Unmanaged initialization type in struct (for field #{m} of type #{self[m].class})"
        end
      end
      # Custom initializations go here:
      # self[:key] = value
    end
  end

  attach_function :setup_model,                          # ruby name
                  :OCP_eRumby_MLT_setup_model,        # C name
                  [:pointer ,:pointer ,:pointer ],
                  :void

  attach_function :setup_solver,                         # ruby name
                  :OCP_eRumby_MLT_setup_solver,       # C name
                  [:pointer ],
                  :void

  attach_function :write_solution_to_file,                   # ruby name
                  :OCP_eRumby_MLT_write_solution_to_file, # C name
                  [:string],
                  :void

  attach_function :printout_enabled?,                       # ruby name
                  :OCP_eRumby_MLT_printout_is_enabled,   # C name
                  [],
                  :int

  attach_function :enable_printout,                      # ruby name
                  :OCP_eRumby_MLT_enable_printout,    # C name
                  [],
                  :void

  attach_function :disable_printout,                     # ruby name
                  :OCP_eRumby_MLT_disable_printout,   # C name
                  [],
                  :void

  attach_function :reset_multiplier,                     # ruby name
                  :OCP_eRumby_MLT_reset_multiplier,   # C name
                  [],
                  :void

  attach_function :reset_BC_multiplier,                     # ruby name
                  :OCP_eRumby_MLT_reset_BC_multiplier,   # C name
                  [],
                  :void

  attach_function :set_internal_guess,                     # ruby name
                  :OCP_eRumby_MLT_set_internal_guess,   # C name
                  [],
                  :void

end

# EOF: OCP_eRumby_MLT_ffi_stucts.rb
