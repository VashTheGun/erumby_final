/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_MLT_Main.cc                                         |
 |                                                                       |
 |  version: 1.0   date 13/12/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#define MECHATRONIX_USE_RUBY
//#define MECHATRONIX_USE_LUA

#include "OCP_eRumby_MLT.hh"
#include "OCP_eRumby_MLT_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;

using namespace OCP_eRumby_MLTLoad;
using GenericContainerNamespace::GenericContainer;

int
main() {
  OCP_eRumby_MLT   model("OCP_eRumby_MLT",&cout,1);
  GenericContainer gc_data;
  GenericContainer gc_solution;

  // user defined Object instances (external)
  SplineSet        splineSetRoad2D( "splineSetRoad2D" );
  Path2D           trajectory( "trajectory" );

  try {
    #ifdef MECHATRONIX_USE_RUBY
    string fname = "./data/OCP_eRumby_MLT_Data.rb";
    #endif
    #ifdef MECHATRONIX_USE_LUA
    string fname = "./data/OCP_eRumby_MLT_Data.lua";
    #endif

    // read Ruby/Lua script
    Mechatronix::load_script( fname, gc_data );

    // alias for user object classes passed as pointers
    GenericContainer & ptrs = gc_data["Pointers"];
    // setup user object classes

    ASSERT(gc_data.exists("SplineSetRoad2D"), "missing key: ``SplineSetRoad2D'' in gc_data");
    splineSetRoad2D.setup(gc_data("SplineSetRoad2D"));
    ptrs[ "pSplineSetRoad2D" ] = &splineSetRoad2D;

    ASSERT(gc_data.exists("Trajectory"), "missing key: ``Trajectory'' in gc_data");
    trajectory.setup(gc_data("Trajectory"));
    ptrs[ "pTrajectory" ] = &trajectory;

    // setup model
    model.setup( gc_data );

    // initialize nonlinear system initial point
    model.guess( gc_data("Guess","Missing `Guess` field") );

    // solve nonlinear system
    bool ok = model.solve(); // no spline

    // get solution (even if not converged)
    model.get_solution( gc_solution );
    model.diagnostic( gc_data );

    ofstream file;
    if ( ok ) {
      file.open( "data/OCP_eRumby_MLT_OCP_result.txt" );
    } else {
      cout << gc_solution("solver_message").get_string() << '\n';
      // dump solution to file
      file.open( "data/OCP_eRumby_MLT_OCP_not_converged.txt" );
    }
    file.precision(18);
    Mechatronix::saveOCPsolutionToStream(gc_solution,file);
    file.close();
    cout.precision(18);
    GenericContainer const & target = gc_solution("target");
    cout
      << "Lagrange target    = " << target("lagrange").get_number()  << '\n'
      << "Mayer target       = " << target("mayer").get_number()     << '\n'
      << "Penalties+Barriers = " << target("penalties").get_number() << '\n'
      << "Control penalties  = " << target("control_penalties").get_number() << '\n';
    if ( gc_solution.exists("parameters") ) {
      cout << "Parameters:\n";
      gc_solution("parameters").print(cout);
    }
    if ( gc_solution.exists("diagnosis") ) gc_solution("diagnosis").print(cout);
  }
  catch ( exception const & exc ) {
    std::cerr << MSG_ERROR( exc.what() ) << '\n';
    ALL_DONE_FOLKS;
    exit(0);
  }
  catch ( char const exc[] ) {
    std::cerr << MSG_ERROR( exc ) << '\n';
    ALL_DONE_FOLKS;
    exit(0);
  }
  catch (...) {
    cout << "ERRORE SCONOSCIUTO\n";
    ALL_DONE_FOLKS;
    exit(0);
  }

  ALL_DONE_FOLKS;
}
