/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_MHT_Methods.cc                                      |
 |                                                                       |
 |  version: 1.0   date 13/12/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby_MHT.hh"
#include "OCP_eRumby_MHT_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_rightWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_R")
#define ALIAS_rightWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_R")
#define ALIAS_rightWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_R")
#define ALIAS_leftWidth_DD(__t1) pSplineSetRoad2D -> eval_DD( __t1,"width_L")
#define ALIAS_leftWidth_D(__t1) pSplineSetRoad2D -> eval_D( __t1,"width_L")
#define ALIAS_leftWidth(__t1) pSplineSetRoad2D -> eval( __t1,"width_L")
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_speed_limit_DD(__t1) speed_limit.DD( __t1)
#define ALIAS_speed_limit_D(__t1) speed_limit.D( __t1)
#define ALIAS_Fx__r__OControl_D_3(__t1, __t2, __t3) Fx__r__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2(__t1, __t2, __t3) Fx__r__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1(__t1, __t2, __t3) Fx__r__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_3_3(__t1, __t2, __t3) Fx__r__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_3(__t1, __t2, __t3) Fx__r__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_2_2(__t1, __t2, __t3) Fx__r__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_3(__t1, __t2, __t3) Fx__r__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_2(__t1, __t2, __t3) Fx__r__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_Fx__r__OControl_D_1_1(__t1, __t2, __t3) Fx__r__OControl.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3(__t1, __t2, __t3) delta__OControl.D_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2(__t1, __t2, __t3) delta__OControl.D_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1(__t1, __t2, __t3) delta__OControl.D_1( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_3_3(__t1, __t2, __t3) delta__OControl.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_3(__t1, __t2, __t3) delta__OControl.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_2_2(__t1, __t2, __t3) delta__OControl.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_3(__t1, __t2, __t3) delta__OControl.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_2(__t1, __t2, __t3) delta__OControl.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__OControl_D_1_1(__t1, __t2, __t3) delta__OControl.D_1_1( __t1, __t2, __t3)


namespace OCP_eRumby_MHTDefine {

  /*\
   |   ___                   _
   |  | _ ) ___ _  _ _ _  __| |__ _ _ _ _  _
   |  | _ \/ _ \ || | ' \/ _` / _` | '_| || |
   |  |___/\___/\_,_|_||_\__,_\__,_|_|  \_, |
   |    ___             _ _ _   _       |__/
   |   / __|___ _ _  __| (_) |_(_)___ _ _  ___
   |  | (__/ _ \ ' \/ _` | |  _| / _ \ ' \(_-<
   |   \___\___/_||_\__,_|_|\__|_\___/_||_/__/
  \*/

  integer
  OCP_eRumby_MHT::boundaryConditions_numEqns() const
  { return 1; }

  void
  OCP_eRumby_MHT::boundaryConditions_eval(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    result__[ 0   ] = XL__[4] * ModelPars[40];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"boundaryConditions_eval",1);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  OCP_eRumby_MHT::DboundaryConditionsDx_numRows() const
  { return 1; }

  integer
  OCP_eRumby_MHT::DboundaryConditionsDx_numCols() const
  { return 14; }

  integer
  OCP_eRumby_MHT::DboundaryConditionsDx_nnz() const
  { return 1; }

  void
  OCP_eRumby_MHT::DboundaryConditionsDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 4   ;
  }

  void
  OCP_eRumby_MHT::DboundaryConditionsDx_sparse(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    result__[ 0   ] = ModelPars[40];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DboundaryConditionsDxp_sparse",1);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  OCP_eRumby_MHT::DboundaryConditionsDp_numRows() const
  { return 1; }

  integer
  OCP_eRumby_MHT::DboundaryConditionsDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby_MHT::DboundaryConditionsDp_nnz() const
  { return 0; }

  void
  OCP_eRumby_MHT::DboundaryConditionsDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {

  }

  void
  OCP_eRumby_MHT::DboundaryConditionsDp_sparse(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY

  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  OCP_eRumby_MHT::adjointBC_numEqns() const
  { return 14; }

  void
  OCP_eRumby_MHT::adjointBC_eval(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    L_const_pointer_type LL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    L_const_pointer_type LR__,
    P_const_pointer_type P__,
    OMEGA_full_const_pointer_type OMEGA__,
    real_type                     result__[]
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    real_type t1   = ModelPars[19];
    result__[ 0   ] = 2 * (XL__[0] - ModelPars[27]) * ModelPars[20] * t1 + LL__[0];
    real_type t13  = ModelPars[54];
    result__[ 1   ] = 2 * t13 * (t13 * XL__[1] - ModelPars[36]) * ModelPars[47] * t1 - LL__[1] * t13;
    result__[ 2   ] = 2 * (XL__[2] - ModelPars[31]) * ModelPars[21] * t1 + LL__[3];
    result__[ 3   ] = 2 * (XL__[3] - ModelPars[33]) * ModelPars[22] * t1 + LL__[4];
    real_type t41  = ModelPars[40];
    result__[ 4   ] = 2 * t41 * (t41 * XL__[4] - ModelPars[39]) * ModelPars[44] * t1 + t41 * OMEGA__[0] + LL__[2] * t41;
    real_type t55  = ModelPars[50];
    real_type t63  = t55 * ModelPars[52];
    result__[ 5   ] = 2 * t55 * (t55 * XL__[5] - ModelPars[49]) * ModelPars[45] * t1 + LL__[5] * t63;
    real_type t69  = ModelPars[37];
    real_type t77  = t69 * ModelPars[28];
    result__[ 6   ] = 2 * t69 * (t69 * XL__[6] - ModelPars[23]) * ModelPars[46] * t1 + LL__[6] * t77;
    real_type t80  = ModelPars[15];
    result__[ 7   ] = 2 * (XR__[0] - ModelPars[26]) * ModelPars[16] * t80 - LR__[0];
    result__[ 8   ] = 2 * t13 * (t13 * XR__[1] - ModelPars[35]) * ModelPars[43] * t80 + LR__[1] * t13;
    result__[ 9   ] = 2 * (XR__[2] - ModelPars[30]) * ModelPars[17] * t80 - LR__[3];
    result__[ 10  ] = 2 * (XR__[3] - ModelPars[32]) * ModelPars[18] * t80 - LR__[4];
    result__[ 11  ] = 2 * t41 * (t41 * XR__[4] - ModelPars[38]) * ModelPars[41] * t80 - LR__[2] * t41;
    result__[ 12  ] = 2 * t55 * (t55 * XR__[5] - ModelPars[48]) * ModelPars[42] * t80 - LR__[5] * t63;
    result__[ 13  ] = -LR__[6] * t77;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"adjointBC_eval",14);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  OCP_eRumby_MHT::DadjointBCDx_numRows() const
  { return 14; }

  integer
  OCP_eRumby_MHT::DadjointBCDx_numCols() const
  { return 14; }

  integer
  OCP_eRumby_MHT::DadjointBCDx_nnz() const
  { return 13; }

  void
  OCP_eRumby_MHT::DadjointBCDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 1   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 2   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 3   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 4   ; jIndex[ 4  ] = 4   ;
    iIndex[ 5  ] = 5   ; jIndex[ 5  ] = 5   ;
    iIndex[ 6  ] = 6   ; jIndex[ 6  ] = 6   ;
    iIndex[ 7  ] = 7   ; jIndex[ 7  ] = 7   ;
    iIndex[ 8  ] = 8   ; jIndex[ 8  ] = 8   ;
    iIndex[ 9  ] = 9   ; jIndex[ 9  ] = 9   ;
    iIndex[ 10 ] = 10  ; jIndex[ 10 ] = 10  ;
    iIndex[ 11 ] = 11  ; jIndex[ 11 ] = 11  ;
    iIndex[ 12 ] = 12  ; jIndex[ 12 ] = 12  ;
  }

  void
  OCP_eRumby_MHT::DadjointBCDx_sparse(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    L_const_pointer_type LL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    L_const_pointer_type LR__,
    P_const_pointer_type P__,
    OMEGA_full_const_pointer_type OMEGA__,
    real_type                     result__[]
  ) const {
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    real_type t1   = ModelPars[19];
    result__[ 0   ] = 2 * ModelPars[20] * t1;
    real_type t7   = ModelPars[54] * ModelPars[54];
    result__[ 1   ] = 2 * t7 * ModelPars[47] * t1;
    result__[ 2   ] = 2 * ModelPars[21] * t1;
    result__[ 3   ] = 2 * ModelPars[22] * t1;
    real_type t16  = ModelPars[40] * ModelPars[40];
    result__[ 4   ] = 2 * t16 * ModelPars[44] * t1;
    real_type t21  = ModelPars[50] * ModelPars[50];
    result__[ 5   ] = 2 * t21 * ModelPars[45] * t1;
    real_type t26  = ModelPars[37] * ModelPars[37];
    result__[ 6   ] = 2 * t26 * ModelPars[46] * t1;
    real_type t28  = ModelPars[15];
    result__[ 7   ] = 2 * ModelPars[16] * t28;
    result__[ 8   ] = 2 * t7 * ModelPars[43] * t28;
    result__[ 9   ] = 2 * ModelPars[17] * t28;
    result__[ 10  ] = 2 * ModelPars[18] * t28;
    result__[ 11  ] = 2 * t16 * ModelPars[41] * t28;
    result__[ 12  ] = 2 * t21 * ModelPars[42] * t28;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DadjointBCDxp_sparse",13);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  OCP_eRumby_MHT::DadjointBCDp_numRows() const
  { return 14; }

  integer
  OCP_eRumby_MHT::DadjointBCDp_numCols() const
  { return 0; }

  integer
  OCP_eRumby_MHT::DadjointBCDp_nnz() const
  { return 0; }

  void
  OCP_eRumby_MHT::DadjointBCDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {

  }

  void
  OCP_eRumby_MHT::DadjointBCDp_sparse(
    integer              i_segment_left,
    Q_const_pointer_type QL__,
    X_const_pointer_type XL__,
    L_const_pointer_type LL__,
    integer              i_segment_right,
    Q_const_pointer_type QR__,
    X_const_pointer_type XR__,
    L_const_pointer_type LR__,
    P_const_pointer_type P__,
    OMEGA_full_const_pointer_type OMEGA__,
    real_type                     result__[]
  ) const {
    // EMPTY!
  }

}

// EOF: OCP_eRumby_MHT_Methods.cc
