/*-----------------------------------------------------------------------*\
 |  file: OCP_eRumby_MHT_Main.cc                                         |
 |                                                                       |
 |  version: 1.0   date 13/12/2019                                       |
 |                                                                       |
 |  Copyright (C) 2019                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "OCP_eRumby_MHT.hh"
#include "OCP_eRumby_MHT_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using SplinesLoad::SplineSet;
using Mechatronix::Path2D;

using namespace OCP_eRumby_MHTLoad;
using GenericContainerNamespace::GenericContainer;

int
main() {
  OCP_eRumby_MHT   model("OCP_eRumby_MHT",&cout,1);
  GenericContainer gc_data;
  GenericContainer gc_solution;

  // user defined Object instances (external)
  SplineSet        splineSetRoad2D( "splineSetRoad2D" );
  Path2D           trajectory( "trajectory" );

  try {

    // Auxiliary values
   real_type m = 4.2;
   real_type Lf = 0.172;
   real_type g = 9.81;
   real_type Lr = 0.153;
   real_type pen_epsi_u = 0.1;
   real_type Ca = 0.3;
   real_type u0 = 1;
   real_type xi__s_xo = 0.1;
   real_type fx__i = Ca*u0^2;
   real_type L = Lr+Lf;
   real_type u__i = u0;
   real_type Fx_max = 20;
   real_type u_min = 0.1;
   real_type Izz = (.5*Lr+.5*Lf)^2*m;
   real_type Wf = 0.2;
   real_type Fx_min = -20;
   real_type pen_epsi = 0.01;
   real_type vehHalfWidth = 1/2*Wf;
   real_type delta_max = 0.5;
   real_type Fz__f0 = m*g*Lr/(Lr+Lf);
   real_type Fz__r0 = m*g*Lf/(Lr+Lf);
   real_type u_max = 3;
    integer InfoLevel = 4;

    GenericContainer &  data_ControlSolver = gc_data["ControlSolver"];
    // ==============================================================
    // 'LU', 'LUPQ', 'QR', 'QRP', 'SVD', 'LSS', 'LSY', 'MINIMIZATION'
    // :factorization => 'LU',
    // ==============================================================
    data_ControlSolver["Rcond"]     = 1e-14; // reciprocal condition number thresold for QR, SVD, LSS, LSY
    data_ControlSolver["Tau"]       = 0.1;
    data_ControlSolver["MaxIter"]   = 50;
    data_ControlSolver["Tolerance"] = 1e-9;
    data_ControlSolver["Iterative"] = false;
    data_ControlSolver["InfoLevel"] = 1;

    // Enable doctor
    gc_data["Doctor"] = false;

    // Enable check jacobian
    gc_data["JacobianCheck"]            = false;
    gc_data["JacobianCheckFull"]        = false;
    gc_data["JacobianCheck_epsilon"]    = 1e-4;
    gc_data["FiniteDifferenceJacobian"] = false;

    // Redirect output to GenericContainer["stream_output"]
    gc_data["RedirectStreamToString"] = false;

    // Dump Function and Jacobian if uncommented
    gc_data["DumpFile"] = "OCP_eRumby_MHT_dump";

    // spline output (all values as function of "s")
    gc_data["OutputSplines"] = "s";

    // setup solver
    GenericContainer & data_Solver = gc_data["Solver"];
    // Linear algebra factorization selection:
    // 'LU', 'QR', 'QRP', 'SUPERLU'
    // =================
    data_Solver["factorization"] = "LU";
    // =================

    // Last Block selection:
    // 'LU', 'LUPQ', 'QR', 'QRP', 'SVD', 'LSS', 'LSY'
    // ==============================================
    data_Solver["last_factorization"] = "LU";
    // ==============================================

    // choose solver: Hyness, NewtonDumped
    // ===================================
    data_Solver["solver"] = "Hyness";
    // ===================================

    // solver parameters
    data_Solver["max_iter"]              = 600;
    data_Solver["max_step_iter"]         = 100;
    data_Solver["max_accumulated_iter"]  = 5000;
    data_Solver["tolerance"]             = 9.999999999999999e-10;
    // continuation parameters
    data_Solver["ns_continuation_begin"] = 0;
    data_Solver["ns_continuation_end"]   = 1;
    GenericContainer & data_Continuation = data_Solver["continuation"];
    data_Continuation["initial_step"]   = 0.2;   // initial step for continuation
    data_Continuation["min_step"]       = 0.001; // minimum accepted step for continuation
    data_Continuation["reduce_factor"]  = 0.5;   // p fails, reduce step by this factor
    data_Continuation["augment_factor"] = 1.5;   // if step successful in less than few_iteration augment step by this factor
    data_Continuation["few_iterations"] = 8;

    // Boundary Conditions
     GenericContainer & data_BoundaryConditions = gc_data["BoundaryConditions"];
    data_BoundaryConditions["initial_Omega"] = SET;

    // Guess
    GenericContainer & data_Guess = gc_data["Guess"];
    // possible value: zero, default, none, warm
    Guess["initialize"] = "zero";
    // possible value: default, none, warm, spline, table
    Guess["guess_type"] = "default";

    GenericContainer & data_Parameters = gc_data["Parameters"];
    // Model Parameters
    data_Parameters["Ca"] = Ca;
    data_Parameters["Fx_max"] = Fx_max;
    data_Parameters["Fx_min"] = Fx_min;
    data_Parameters["Izz"] = Izz;
    data_Parameters["Lf"] = Lf;
    data_Parameters["Lr"] = Lr;
    data_Parameters["m"] = m;
    data_Parameters["tau__Fx"] = 0.5;
    data_Parameters["wT"] = 0.1;
    data_Parameters["Fx__r__s_xo"] = 10;
    data_Parameters["Omega__s_xo"] = 100;
    data_Parameters["delta__s_xo"] = 0.1;
    data_Parameters["delta_max"] = delta_max;
    data_Parameters["tau__delta"] = 0.1;
    data_Parameters["xi__s_xo"] = xi__s_xo;

    // Guess Parameters
    data_Parameters[u0] = u0;

    // Boundary Conditions
    data_Parameters["WBCC"] = 0;
    data_Parameters["WBCF__n"] = 1;
    data_Parameters["WBCF__u"] = 1;
    data_Parameters["WBCF__v"] = 1;
    data_Parameters["WBCI"] = 1;
    data_Parameters["WBCI__n"] = 1;
    data_Parameters["WBCI__u"] = 1;
    data_Parameters["WBCI__v"] = 1;
    data_Parameters["fx__i"] = fx__i;
    data_Parameters["n__f"] = 0;
    data_Parameters["n__i"] = 0;
    data_Parameters["u__f"] = 0;
    data_Parameters["u__i"] = u__i;
    data_Parameters["v__f"] = 0;
    data_Parameters["v__i"] = 0;
    data_Parameters["xi__f"] = 0;
    data_Parameters["xi__i"] = 0;
    data_Parameters["Omega__f"] = 0;
    data_Parameters["Omega__i"] = 0;
    data_Parameters["WBCF__Omega"] = 1;
    data_Parameters["WBCF__delta"] = 1;
    data_Parameters["WBCF__xi"] = 1;
    data_Parameters["WBCI__Omega"] = 1;
    data_Parameters["WBCI__delta"] = 1;
    data_Parameters["WBCI__fx"] = 1;
    data_Parameters["WBCI__xi"] = 1;
    data_Parameters["delta__f"] = 0;
    data_Parameters["delta__i"] = 0;

    // Post Processing Parameters
    data_Parameters["Fz__f0"] = Fz__f0;
    data_Parameters["Fz__r0"] = Fz__r0;
    data_Parameters["L"] = L;
    data_Parameters["h"] = 0.09;
    data_Parameters["vehHalfWidth"] = vehHalfWidth;

    // User Function Parameters
    data_Parameters["Cf"] = 1.218;
    data_Parameters["Cr"] = 1.5;
    data_Parameters["Df"] = 0.6403;
    data_Parameters["Dr"] = 0.6784;
    data_Parameters["Kf"] = 15.93;
    data_Parameters["Kr"] = 10.49;

    // Continuation Parameters

    // Constraints Parameters

  # functions mapped on objects
  data.MappedObjects = {}

  # PositivePartRegularizedWithSinAtan
  data.MappedObjects[:posPart] = { :h => 0.1 }

  # NegativePartRegularizedWithSinAtan
  data.MappedObjects[:negPart] = { :h => 0.1 }

  # SignRegularizedWithErf
  data.MappedObjects[:SignReg] = { :epsilon => 0.01, :h => 0.1 }

  # Controls
  # Penalty type controls: "QUADRATIC", "QUADRATIC2", "PARABOLA", "CUBIC"
  # Barrier type controls: "LOGARITHMIC", "COS_LOGARITHMIC", "TAN2", "HYPERBOLIC"

  data.Controls = {}
  data.Controls[:delta__OControl] = {
    :type      => 'QUADRATIC',
    :epsilon   => pen_epsi_u,
    :tolerance => 0.001
  }

  data.Controls[:Fx__r__OControl] = {
    :type      => 'QUADRATIC',
    :epsilon   => pen_epsi_u,
    :tolerance => 0.001
  }


  data.Constraints = {}
  # Constraint1D
  # Penalty subtype: 'PENALTY_REGULAR', 'PENALTY_SMOOTH', 'PENALTY_PIECEWISE'
  # Barrier subtype: 'BARRIER_LOG', 'BARRIER_LOG_EXP', 'BARRIER_LOG0'
  # PenaltyBarrier1DInterval
  data.Constraints[:speed_limit] = {
    :subType   => 'PENALTY_REGULAR',
    :epsilon   => pen_epsi,
    :tolerance => 0.001,
    :min       => u_min,
    :max       => u_max,
    :active    => true
  }
  # PenaltyBarrier1DGreaterThan
  data.Constraints[:roadRightLateralBoundaries] = {
    :subType   => 'PENALTY_REGULAR',
    :epsilon   => pen_epsi,
    :tolerance => 0.001,
    :active    => true
  }
  # PenaltyBarrier1DGreaterThan
  data.Constraints[:roadLeftLateralBoundaries] = {
    :subType   => 'PENALTY_REGULAR',
    :epsilon   => pen_epsi,
    :tolerance => 0.001,
    :active    => true
  }
  # Constraint2D: none defined

  # User defined classes initialization
  # User defined classes: S P L I N E S E T R O A D 2 D
  require_relative('../../Custom_Tracks/spline_set_road2D_data.rb',__FILE__)
  # User defined classes: T R A J E C T O R Y
  require_relative('../../Custom_Tracks/trajectory_data.rb',__FILE__)


end

# EOF


















    // alias for user object classes passed as pointers
    GenericContainer & ptrs = gc_data["Pointers"];
    // setup user object classes

    ASSERT(gc_data.exists("SplineSetRoad2D"), "missing key: ``SplineSetRoad2D'' in gc_data");
    splineSetRoad2D.setup(gc_data("SplineSetRoad2D"));
    ptrs[ "pSplineSetRoad2D" ] = &splineSetRoad2D;

    ASSERT(gc_data.exists("Trajectory"), "missing key: ``Trajectory'' in gc_data");
    trajectory.setup(gc_data("Trajectory"));
    ptrs[ "pTrajectory" ] = &trajectory;

    // setup model
    model.setup( gc_data );

    // initialize nonlinear system initial point
    model.guess( gc_data("Guess","Missing `Guess` field") );

    // solve nonlinear system
    bool ok = model.solve(); // no spline

    // get solution (even if not converged)
    model.get_solution( gc_solution );
    model.diagnostic( gc_data );

    ofstream file;
    if ( ok ) {
      file.open( "data/OCP_eRumby_MHT_OCP_result.txt" );
    } else {
      cout << gc_solution("solver_message").get_string() << '\n';
      // dump solution to file
      file.open( "data/OCP_eRumby_MHT_OCP_not_converged.txt" );
    }
    file.precision(18);
    Mechatronix::saveOCPsolutionToStream(gc_solution,file);
    file.close();
    cout.precision(18);
    GenericContainer const & target = gc_solution("target");
    cout
      << "Lagrange target    = " << target("lagrange").get_number()  << '\n'
      << "Mayer target       = " << target("mayer").get_number()     << '\n'
      << "Penalties+Barriers = " << target("penalties").get_number() << '\n'
      << "Control penalties  = " << target("control_penalties").get_number() << '\n';
    if ( gc_solution.exists("parameters") ) {
      cout << "Parameters:\n";
      gc_solution("parameters").print(cout);
    }
    if ( gc_solution.exists("diagnosis") ) gc_solution("diagnosis").print(cout);
  }
  catch ( exception const & exc ) {
    std::cerr << MSG_ERROR( exc.what() ) << '\n';
    ALL_DONE_FOLKS;
    exit(0);
  }
  catch ( char const exc[] ) {
    std::cerr << MSG_ERROR( exc ) << '\n';
    ALL_DONE_FOLKS;
    exit(0);
  }
  catch (...) {
    cout << "ERRORE SCONOSCIUTO\n";
    ALL_DONE_FOLKS;
    exit(0);
  }

  ALL_DONE_FOLKS;
}
