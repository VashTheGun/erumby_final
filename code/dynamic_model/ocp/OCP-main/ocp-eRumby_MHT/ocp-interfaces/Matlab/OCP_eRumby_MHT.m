%-----------------------------------------------------------------------%
%  file: OCP_eRumby_MHT.m                                               %
%                                                                       %
%  version: 1.0   date 13/12/2019                                       %
%                                                                       %
%  Copyright (C) 2019                                                   %
%                                                                       %
%      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             %
%      Dipartimento di Ingegneria Industriale                           %
%      Universita` degli Studi di Trento                                %
%      Via Sommarive 9, I-38123, Trento, Italy                          %
%      email: enrico.bertolazzi@unitn.it                                %
%             francesco.biral@unitn.it                                  %
%             paolo.bosetti@unitn.it                                    %
%-----------------------------------------------------------------------%


classdef OCP_eRumby_MHT < handle
  properties (SetAccess = private, Hidden = true)
    objectHandle; % Handle to the underlying C++ class instance
  end

  methods

    function self = OCP_eRumby_MHT( name )
      self.objectHandle = OCP_eRumby_MHT_Mex( 'new', name );
    end

    function delete( self )
      %% Destroy the C++ class instance
      OCP_eRumby_MHT_Mex( 'delete', self.objectHandle );
    end

    function help( self )
      OCP_eRumby_MHT_Mex('help');
    end

    % -------------------------------------------------------------------------
    % INITIALIZATION
    % -------------------------------------------------------------------------
    function data = read( self, fname )
      data = OCP_eRumby_MHT_Mex( 'read', self.objectHandle, fname );
    end

    function setup( self, fname_or_struct )
      OCP_eRumby_MHT_Mex( 'setup', self.objectHandle, fname_or_struct );
    end

    function n = names( self )
      n = OCP_eRumby_MHT_Mex( 'names', self.objectHandle );
    end

    function res = dims( self )
      res = OCP_eRumby_MHT_Mex( 'dims', self.objectHandle );
    end

    function res = get_ocp_data( self )
      res = OCP_eRumby_MHT_Mex( 'get_ocp_data', self.objectHandle );
    end

    % -------------------------------------------------------------------------
    % INFO LEVEL
    % -------------------------------------------------------------------------
    function infoLevel( self, infoLvl )
      OCP_eRumby_MHT_Mex( 'infoLevel', self.objectHandle, infoLvl );
    end

    % -------------------------------------------------------------------------
    % GUESS
    % -------------------------------------------------------------------------
    function set_guess( self, varargin )
      OCP_eRumby_MHT_Mex( 'set_guess', self.objectHandle, varargin{:} );
    end
    function guess = get_guess( self )
      guess = OCP_eRumby_MHT_Mex( 'get_guess', self.objectHandle );
    end
    function guess = get_solution_as_guess( self )
      guess = OCP_eRumby_MHT_Mex( 'get_solution_as_guess', self.objectHandle );
    end

    % -------------------------------------------------------------------------
    % SOLVE
    % -------------------------------------------------------------------------
    function ok = solve( self )
      % ok = false if computation failed
      % ok = true if computation is succesfull
      ok = OCP_eRumby_MHT_Mex( 'solve', self.objectHandle );
    end

    function updateContinuation( self, n, s )
      OCP_eRumby_MHT_Mex( 'updateContinuation', self.objectHandle, n, s );
    end

    % -------------------------------------------------------------------------
    % GET SOLUTION
    % -------------------------------------------------------------------------
    function sol = solution( self, varargin )
      sol = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, varargin{:} );
    end

    function sol = pack( self, X, Lambda, Pars, Omega )
      sol = OCP_eRumby_MHT_Mex( 'pack', self.objectHandle, X, Lambda, Pars, Omega );
    end

    function [X, Lambda, Pars, Omega] = unpack( self, sol )
      [X, Lambda, Pars, Omega] = OCP_eRumby_MHT_Mex( 'unpack', self.objectHandle, sol );
    end

    % -------------------------------------------------------------------------
    % ZETA
    % -------------------------------------------------------------------------
    function res = zeta( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'zeta' );
    end

    % -------------------------------------------------------------------------
    % STATES
    % -------------------------------------------------------------------------
    function res = n( self, varargin  )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'n', varargin{:} );
    end
    function res = xi__xo( self, varargin  )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'xi__xo', varargin{:} );
    end
    function res = u( self, varargin  )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'u', varargin{:} );
    end
    function res = v( self, varargin  )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'v', varargin{:} );
    end
    function res = Omega__xo( self, varargin  )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'Omega__xo', varargin{:} );
    end
    function res = delta__xo( self, varargin  )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'delta__xo', varargin{:} );
    end
    function res = Fx__r__xo( self, varargin  )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'Fx__r__xo', varargin{:} );
    end

    % -------------------------------------------------------------------------
    % MULTIPLIER
    % -------------------------------------------------------------------------
    function res = lambda1( self, varargin )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'lambda1__xo', varargin{:} );
    end
    function res = lambda2( self, varargin )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'lambda2__xo', varargin{:} );
    end
    function res = lambda3( self, varargin )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'lambda3__xo', varargin{:} );
    end
    function res = lambda4( self, varargin )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'lambda4__xo', varargin{:} );
    end
    function res = lambda5( self, varargin )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'lambda5__xo', varargin{:} );
    end
    function res = lambda6( self, varargin )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'lambda6__xo', varargin{:} );
    end
    function res = lambda7( self, varargin )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'lambda7__xo', varargin{:} );
    end

    % -------------------------------------------------------------------------
    % CONTROLS
    % -------------------------------------------------------------------------
    function res = Fx__r__O( self, varargin )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'Fx__r__O', varargin{:} );
    end
    function res = delta__O( self, varargin )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'delta__O', varargin{:} );
    end

    % -------------------------------------------------------------------------
    % POSTPROCESSING
    % -------------------------------------------------------------------------
    function res = Fx__r( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'Fx__r' );
    end
    function res = delta( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'delta' );
    end
    function res = xi( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'xi' );
    end
    function res = Omega( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'Omega' );
    end
    function res = delta__OControl( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'delta__OControl' );
    end
    function res = Fx__r__OControl( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'Fx__r__OControl' );
    end
    function res = speed_limit( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'speed_limit' );
    end
    function res = roadRightLateralBoundaries( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'roadRightLateralBoundaries' );
    end
    function res = roadLeftLateralBoundaries( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'roadLeftLateralBoundaries' );
    end
    function res = ay_SS( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'ay_SS' );
    end
    function res = s_dot( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 's_dot' );
    end
    function res = xCoMCar( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'xCoMCar' );
    end
    function res = yCoMCar( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'yCoMCar' );
    end
    function res = xLeftCar( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'xLeftCar' );
    end
    function res = yLeftCar( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'yLeftCar' );
    end
    function res = xRightCar( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'xRightCar' );
    end
    function res = yRightCar( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'yRightCar' );
    end
    function res = xLeftEdge( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'xLeftEdge' );
    end
    function res = yLeftEdge( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'yLeftEdge' );
    end
    function res = xRightEdge( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'xRightEdge' );
    end
    function res = yRightEdge( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'yRightEdge' );
    end
    function res = beta( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'beta' );
    end
    function res = a__x( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'a__x' );
    end
    function res = Delta_Fz( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'Delta_Fz' );
    end
    function res = Fy__f( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'Fy__f' );
    end
    function res = Fy__r( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'Fy__r' );
    end
    function res = alpha__r__SS( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'alpha__r__SS' );
    end
    function res = alpha__f__SS( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'alpha__f__SS' );
    end
    function res = mu_yr( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'mu_yr' );
    end
    function res = mu_yf( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'mu_yf' );
    end
    function res = mu_xr( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'mu_xr' );
    end
    function res = mayer_target( self )
      res = OCP_eRumby_MHT_Mex( 'get_solution', self.objectHandle, 'mayer_target' );
    end

    % -------------------------------------------------------------------------
    % NONLINEAR SYSTEM
    % -------------------------------------------------------------------------
    function F = eval_F( self, x )
      F = OCP_eRumby_MHT_Mex( 'eval_F', self.objectHandle, x );
    end

    function JF = eval_JF( self, x )
      JF = OCP_eRumby_MHT_Mex( 'eval_JF', self.objectHandle, x );
    end

    function JF = eval_JF_pattern( self )
      JF = OCP_eRumby_MHT_Mex( 'eval_JF_pattern', self.objectHandle );
    end

    function x = get_raw_solution( self )
      x = OCP_eRumby_MHT_Mex( 'get_raw_solution', self.objectHandle );
    end

    function set_raw_solution( self, x )
      OCP_eRumby_MHT_Mex( 'set_raw_solution', self.objectHandle, x );
    end

    function ok = check_raw_solution( self, x )
      ok = OCP_eRumby_MHT_Mex( 'check_raw_solution', self.objectHandle, x );
    end

    function check_jacobian( self, x, epsi )
      OCP_eRumby_MHT_Mex( 'check_jacobian', self.objectHandle, x, epsi );
    end

    % -------------------------------------------------------------------------
    % PLOT SOLUTION
    % -------------------------------------------------------------------------
    function plot_states( self )
      plot(...
        self.zeta(), self.n(), ...
        self.zeta(), self.xi__xo(), ...
        self.zeta(), self.u(), ...
        self.zeta(), self.v(), ...
        self.zeta(), self.Omega__xo(), ...
        self.zeta(), self.delta__xo(), ...
        self.zeta(), self.Fx__r__xo(), ...
        'Linewidth', 2 ...
      );
      title('states');
      legend( 'n', '\xi\_xo', 'u', 'v', '\Omega\_xo', '\delta\_xo', 'Fx\_r\_xo' );
    end

    function plot_multipliers( self )
      plot(...
        self.zeta(), self.lambda1(), ...
        self.zeta(), self.lambda2(), ...
        self.zeta(), self.lambda3(), ...
        self.zeta(), self.lambda4(), ...
        self.zeta(), self.lambda5(), ...
        self.zeta(), self.lambda6(), ...
        self.zeta(), self.lambda7(), ...
        'Linewidth', 2 ...
      );
      title('multipliers');
      legend( '\lambda1', '\lambda2', '\lambda3', '\lambda4', '\lambda5', '\lambda6', '\lambda7' );
    end

    function plot_controls( self )
      plot(...
        self.zeta(), self.Fx__r__O(), ...
        self.zeta(), self.delta__O(), ...
        'Linewidth', 2 ...
      );
      title('controls');
      legend( 'Fx\_r\_O', '\delta\_O' );
    end

  end

end

% EOF: OCP_eRumby_MHT.m
