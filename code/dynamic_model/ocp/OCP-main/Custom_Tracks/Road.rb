# User defined classes initialization
# User defined classes: R O A D
#rubbered = 0.1
#initAdherence = 1
#
adh = 1.0

grid1 = 0.5
grid0 = 0.5
width = 4

mechatronix do |data|
  data.Road = {
    :theta0   => 1.570796327,
    :s0       => 0,
    :x0       => 0,
    :y0       => 0,
    :is_SAE   => false,
    :gridSize => grid1, 
    :width    => 1,
    :segments => [

      {
        :rightWidth => width,
        :curvature  => 0*20,
        :leftWidth  => width,
        :length     => 0.2*20,
        :gridSize   => grid0,
      },

            {
        :rightWidth => width,
        :curvature  => 0*20,
        :leftWidth  => width,
        :length     => 0.3*20,
        :gridSize   => grid1,
      },


      {
        :rightWidth => width,
        :curvature  => 0*20,
        :leftWidth  => width,
        :length     => 4.5*20,
        :gridSize   => grid1,
      },

      {
        :rightWidth => width,
        :curvature  => 1/(3.5*20),
        :leftWidth  => width,
        :length     => Math::PI/2*3.5*20,
        :gridSize   => grid1,
      },

      {
        :rightWidth => width,
        :curvature  => 0*20,
        :leftWidth  => width,
        :length     => 5*20,
        :gridSize   => grid1,
      },
    ],
 };

end

# EOF
