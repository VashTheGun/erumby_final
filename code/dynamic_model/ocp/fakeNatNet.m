% Optitrack Matlab / NatNet Polling Sample
%  Requirements:
%   - OptiTrack Motive 2.0 or later
%   - OptiTrack NatNet 3.0 or later
%   - Matlab R2013

clear all; 
close all; 
clc; 

addpath('./OCP-lib');

%% ocp problem
ocp_MLT = OCP_eRumby_MLT( 'OCP_eRumby_MLT' );
ocp_MLT.setup('./OCP-main/ocp-eRumby_MLT/data/OCP_eRumby_MLT_Data.rb');
ocp_MLT.infoLevel(0);
ocp_MLT.set_guess(); % use default guess generated in MAPLE

data_ocp = ocp_MLT.get_ocp_data(); % retrieve ocp-problem data
% -------------------------------------------------------------------------
% COMPUTE SOLUTION
% -------------------------------------------------------------------------
converged = ocp_MLT.solve();

plot(ocp_MLT.solution('xCoMCar'), ocp_MLT.solution('yCoMCar'))


%%
addpath(genpath('./../../../../../lib/matlab/Clothoids'));
% c++ convention
M_PI = pi;
 
 % track parameters
x = [0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0];
y = [0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0];
theta = [0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI, M_PI, 0];

S = ClothoidList() ;

for i = 1:length(x)-1
    S.push_back_G1(x(i), y(i), theta(i), x(i+1), y(i+1), theta(i+1) ) ; % track creation
end

clear x y theta M_PI
%% 
ds      = 0.01;
LH_max = round(S.length);
horizon = [0:ds:LH_max];       

% Compute the horizon
start_zeta = 0;

horizon_shift = horizon + start_zeta;

[x_r,y_r,theta_r,curv_r] = S.evaluate(horizon_shift); % extract data 

data_ocp  = ocp_MLT.get_ocp_data(); % retrieve ocp-problem data

data_ocp.Trajectory.abscissa                = horizon; %: 
data_ocp.Trajectory.abscissa_step           = ds;
data_ocp.Trajectory.curvature               = curv_r; %: 
data_ocp.Trajectory.mesh.segments{1}.length = 1;      
data_ocp.Trajectory.mesh.segments{1}.n      = LH_max/ds;    %length(horizon);
data_ocp.Trajectory.theta0                  = theta_r(1); %
data_ocp.Trajectory.x0                      = x_r(1);   %
data_ocp.Trajectory.y0                      = y_r(1);   %  


% Boundary condition data -----------------------------------------------
% Change boundary conditions
% initial 
bcs.initial.u           = 1;
bcs.initial.v           = 0;
bcs.initial.n           = 0;
bcs.initial.xi          = 0;
bcs.initial.delta       = 0;
bcs.initial.omega__z    = 0;
bcs.initial.fx          = 0;

% 
% % final
bcs.final.u           = 0;%30/3.6;
% bcs.final.v           = 0;
bcs.final.n           = 0;
bcs.final.xi          = 0;
% bcs.final.delta       = 0;
% bcs.final.omega__z    = 0;



data_ocp = create_boundary_conditions(data_ocp,bcs);


ocp_MLT.setup(data_ocp);
ocp_MLT.infoLevel(0);
ocp_MLT.set_guess();

% solve ocp solution

ok = ocp_MLT.solve();

road.xRoadLeft       = ocp_MLT.solution('xLeftEdge');
road.yRoadLeft       = ocp_MLT.solution('yLeftEdge');
road.xRoadRight      = ocp_MLT.solution('xRightEdge');
road.yRoadRight      = ocp_MLT.solution('yRightEdge');

MLT.x_ocp       = ocp_MLT.solution('xCoMCar');
MLT.y_ocp       = ocp_MLT.solution('yCoMCar');

MLT.abscissa        = ocp_MLT.solution('zeta');
MLT.delta           = ocp_MLT.solution('delta__O');
MLT.Omega           = ocp_MLT.solution('Omega');
MLT.u               = ocp_MLT.solution('u');
MLT.xi              = ocp_MLT.solution('xi');
MLT.n               = ocp_MLT.solution('n');

        

clearvars -except ocp_MLT S MLT road


%% ocp problem
load('telem.mat');

ocp_MHT = OCP_eRumby_MHT( 'OCP_eRumby_MHT' );
ocp_MHT.setup('./OCP-main/ocp-eRumby_MHT/data/OCP_eRumby_MHT_Data.rb');
ocp_MHT.infoLevel(0);
ocp_MHT.set_guess(); % use default guess generated in MAPLE

data_ocp = ocp_MHT.get_ocp_data(); % retrieve ocp-problem data
% -------------------------------------------------------------------------
% COMPUTE SOLUTION
% -------------------------------------------------------------------------
converged = ocp_MHT.solve();

LH_max        = 9;  % Lunghezza horizon
guess_ocp_old = [];
ok_old        = 0;

[t, n] = S.find_coord(erumby_tel.x_gps(7000)/1000, erumby_tel.y_gps(7000)/1000);
[~,~,tmp,~] = S.evaluate(t);
% init condizioni iniziali
in_cond.u       = 0;
in_cond.v       = 0;
in_cond.theta   = -erumby_tel.yaw_gps(7000)/1000;
in_cond.n       = -n;
in_cond.delta   = 0;
in_cond.Omega   = 0;
% init condizioni finali
fin_cond.u      = interp1(MLT.abscissa,MLT.u,t+LH_max);
fin_cond.n      = interp1(MLT.abscissa,MLT.n,t+LH_max);
fin_cond.xi     = interp1(MLT.abscissa,MLT.xi,t+LH_max);

[ok, x_rep, y_rep, delta_rep, u_rep, abscissa_rep, guess_ocp, n_iter] = MHT_erumby(S,...
                                ocp_MHT,in_cond , fin_cond, t, LH_max, guess_ocp_old, ok_old);
       

%%




% connect the client to the server (multicast over local loopback) -
% modify for your network
fprintf( 'Connecting to the server\n' )

% create udp sender
udps = dsp.UDPSender('RemoteIPAddress', '10.0.0.1','RemoteIPPort',25000);
udpr = dsp.UDPReceiver('RemoteIPAddress', '10.0.0.1','LocalIPPort',35000, 'MessageDataType','double');

mainLoop = true;

% Init parameters
count_10ms      = 0;
count_100ms    = 0;
time_sim        = erumby_tel.time(7000);
tic;
j = 1;

while mainLoop(time_sim < 40)

    
    count_10ms  = toc + count_10ms;
    count_100ms = toc + count_100ms;
    time_sim    = toc + time_sim;
    
    GPS_x_sim     = interp1(erumby_tel.time, erumby_tel.x_gps, time_sim); 
    GPS_y_sim     = interp1(erumby_tel.time, erumby_tel.y_gps, time_sim); 
    GPS_yaw_sim   = interp1(erumby_tel.time, erumby_tel.yaw_gps, time_sim); 
    
    GPS.x(j) = GPS_x_sim;
    GPS.y(j) = GPS_y_sim;
    
    if(count_10ms >= 0.01)
        [t, ~] = S.find_coord(GPS_x_sim/1000, GPS_y_sim/1000);
        u_ctr       = interp1(abscissa_rep, u_rep, t)*1000;
        delta_ctr   = interp1(abscissa_rep, delta_rep, t);
        delta_ctr   = uint16(6881 - (delta_ctr*4402));
        
        dataSent = [int16((GPS_x_sim ));int16((GPS_y_sim ));int16(GPS_yaw_sim); int16(u_ctr); int16(delta_ctr)];
        udps(dataSent);
%         tmp = udpr();
%         if(isempty(tmp) == 0)
%             dataRec(j) = tmp;
%         end
        count_10ms = 0;
        tic;
        
        if(count_100ms >= 0.1)
            [t, n] = S.find_coord(GPS_x_sim/1000, GPS_y_sim/1000);
            
            [~,~,tmp,~] = S.evaluate(t);
            
            % init condizioni iniziali
            in_cond.u       = interp1(abscissa_rep, u_rep, t);
            in_cond.v       = 0;
            in_cond.theta   = -GPS_yaw_sim/1000;
            in_cond.n       = -n;
            in_cond.delta   = interp1(abscissa_rep, delta_rep, t);
            in_cond.Omega   = 0;
            % init condizioni finali
            fin_cond.u      = interp1(MLT.abscissa,MLT.u,t+LH_max);
            fin_cond.n      = interp1(MLT.abscissa,MLT.n,t+LH_max);
            fin_cond.xi     = interp1(MLT.abscissa,MLT.xi,t+LH_max);
            
            [ok, x_rep, y_rep, delta_rep, u_rep, abscissa_rep, guess_ocp, n_iter] = MHT_erumby(S, ocp_MHT,in_cond , fin_cond, t, LH_max, guess_ocp_old, ok_old);
            count_100ms = 0;
            MHT.x(:,j) = x_rep;
            MHT.y(:,j) = y_rep;
            MHT.delta(:,j) = delta_rep;
            MHT.u(:,j) = u_rep;
            MHT.abscissa(:,j) = abscissa_rep;
            MHT.converged(j) = ok; 
        end
    j = j+1;
    end
end 

disp('NatNet Polling Sample End' )
release(udps);


%%

figure(1)
hold on

plot(road.xRoadLeft, road.yRoadLeft, 'k')
plot(road.xRoadRight, road.yRoadRight, 'k')
plot(MLT.x_ocp, MLT.y_ocp)

for i = 1:size(MHT.x,2)
    plot(MHT.x(:,i), MHT.y(:,i))
%     pause()
end

figure(2)

hold on
for i = 1:size(MHT.x,2)
    plot(MHT.abscissa(:,i), MHT.delta(:,i))
%     pause()
end


 