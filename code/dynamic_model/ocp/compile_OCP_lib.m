clc
clear all

mex_dir = 'OCP-lib/';

[status, message, messageid] = rmdir(mex_dir);
fprintf('%s',message)
[status, message, messageid] = mkdir(mex_dir);
fprintf('%s',message)

fprintf('\n\nCompiling OC problems for VIRTUAL COACH\n');
% Compile Minimum Time on Receding horizon
fprintf('Compiling OC problem for Minimum Time on Receding horizon\n');
base_dir = 'OCP-main/ocp-eRumby_MHT/ocp-interfaces/Matlab/';
oldFolder = cd(base_dir);
[mex_name,lib_name] = CompileMex();
cd(oldFolder)
fprintf('... moving mex file in folder %s\n',mex_dir);
copyfile([base_dir, mex_name], [mex_dir, mex_name]);
copyfile([base_dir, lib_name], [mex_dir, lib_name]);




