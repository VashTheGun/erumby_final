clc; clear all; close all;

velocity_target_vec = [1.0383    1.5533    2.0553    2.5472    2.7836    3.0274    3.2502]*2.5;
steer_input = 0.168;

% Set target
for j = 2:7
    velocity_target = velocity_target_vec(j); % m/s
    sim('dyn_model_simple.slx');
    
    u_SS = mean(u_sim(end - 10:end));
    v_SS = mean(v_sim(end - 10:end));
    Omega_SS = mean(Omega_sim(end - 10:end));
    
%     figure
%     subplot(1,3,1)
%     plot(tout, Omega_sim)
%     xlabel('time [s]'); ylabel('Omega [rad/s]');
%     
%     subplot(1,3,2)
%     hold on
%     plot(tout, u_sim)
%     plot(tout, velocity_target*ones(size(tout,1),1))
%     hold off
%     xlabel('time [s]'); ylabel('u [m/s]');
%     
%     subplot(1,3,3)
%     plot(tout, v_sim)
%     xlabel('time [s]'); ylabel('v [m/s]');

    
    curv_model(j) = Omega_SS/u_SS;
    ay_model(j) = Omega_SS * u_SS;
end



figure
plot(ay_model, curv_model)
xlabel('a_y [m/s^2]'); ylabel('\rho [1/m]');

figure
plot(ay_model, curv_model,'o')
xlabel('a_y [m/s^2]'); ylabel('\rho [1/m]');
