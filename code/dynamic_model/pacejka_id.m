%% Analize state test for erumby project
close all;clear all;clc;
addpath('../dynamic_model');
addpath(genpath('../lib'))
addpath('../../test_session/18_09_05/telemetry_09_05/tyre_force')
% if the tests have different total time take the smallest time

ts = 0.001;
max_time = 28/ts; 


lpf_beta = designfilt('lowpassiir', ...         % Response type
             'FilterOrder',8, ...      % Filter order
             'HalfPowerFrequency',0.005);
% j is the total number of the test

% tyre force and slip angle identification for left cornering test in steady state

for j = 1:7

    filename1 = '../../test_session/18_09_05/telemetry_09_05/tyre_force/';
    i_s = num2str(j+4);
    test = strcat('test',i_s,'.txt');
    
    
    filename1 = strcat(filename1,test);

    filename2 = '../../test_session/18_09_05/optitrack_09_05/tyre_force/';
    test = strcat('test',i_s,'.csv');
    filename2 = strcat(filename2,test);

    [telemetry] = telemetry_import_V2(filename1);
    [optitrack] = optitrack_import(filename2);

    [remap_telemetry,remap_optitrack] = test_data_remap(telemetry,optitrack);
    
    %indexing for steady state
    
    idx = [14000:27000];
    tmp = atan2(remap_optitrack.Vy_rel,remap_optitrack.Vx_rel);
    tmp = filtfilt(lpf_beta,tmp);
    Vx(j) = mean(remap_optitrack.Vx_rel(idx));
    beta(j) = mean(tmp(idx));
    std_beta(j) = std(tmp(idx));
    V(j) = mean(remap_optitrack.V_gps(idx));
    yaw_rate(j) = mean(remap_telemetry.gyro_z(idx));   
    std_curv(j) = std(remap_telemetry.gyro_z(idx)./remap_optitrack.V_gps(idx));
    a_y(j) = mean(remap_telemetry.a_y(idx));
    std_a_y(j) = std(remap_telemetry.a_y(idx));
    X_gps(1,:,j) = remap_optitrack.x_gps(1:max_time);
    yaw_rate_gps(1,:,j) = remap_telemetry.gyro_z(1:max_time); 

end

curv = yaw_rate./V;

Lf = 0.172;              % front wheel base
Lr = 0.153;              % rear wheel base
m = 4.61;
tau = 1.00;

delta_const = mean(remap_telemetry.steering);
delta =   -((Lf+Lr)*(0.0404 + 0.0170*delta_const));  

alpha_f = atan(- beta - Lf*yaw_rate./V) + delta;
alpha_r = atan( - beta + Lr*yaw_rate./V);

Fyr = (a_y*m)./(1+Lr/Lf*cos(delta));
Fyf = Lr/Lf*Fyr;


Fz_f = m*9.81*Lr/(Lf+Lr);
Fz_r = m*9.81*Lf/(Lf+Lr);

D_f = max(abs(Fyf))/Fz_f;

fo = fitoptions('Method','NonlinearLeastSquares',... 
  'Lower',[0,1,-1],...
  'Upper',[1000,1.5,1],...
  'StartPoint',[100, 1.1, 0 ]);

ft = fittype(@(K,C,alpha_off,alpha) Fz_f*D_f*sin(C*atan(K/(C*D_f)*(alpha+alpha_off))),...
             'independent',{'alpha'},...
             'coefficients',{'K','C','alpha_off'});
[curve2,gof2] = fit(alpha_f',Fyf',ft,fo)
%%
x0=10;y0=20;
width=550;
height = 400;
figure()
set(gcf,'units','points','position',[x0,y0,width,height])
plot(curve2)
hold on
plot(alpha_f,Fyf,'o','MarkerSize',12)
title('Pacejka Curve Fitting Front Tire')
grid on
set(gca,'FontSize',20)
ylb = ylabel('F_{yf}')
xlb = xlabel('\alpha_f')
xlb.FontSize = 20;
ylb.FontSize = 20;
legend('Fitted Curve', 'Telemetry Data')

D_r = max(abs(Fyr))/Fz_r;

fo = fitoptions('Method','NonlinearLeastSquares',... 
  'Lower',[0,1,-1],...
  'Upper',[1000,1.5,1],...
  'StartPoint',[100, 1.1, 0 ]);

ft = fittype(@(K,C,alpha_off,alpha) Fz_r*D_r*sin(C*atan(K/(C*D_r)*(alpha+alpha_off))),...
             'independent',{'alpha'},...
             'coefficients',{'K','C','alpha_off'});
[curve2,gof2] = fit(alpha_r',Fyr',ft,fo)

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
plot(curve2)
hold on
plot(alpha_r,Fyr,'o','MarkerSize',12)
title('Pacejka Curve Fitting Rear Tire')
set(gca,'FontSize',20)
grid on
xlb = ylabel('Fyr')
xlb = xlabel('\alpha_r')
xlb.FontSize = 20;
ylb.FontSize = 20;
legend('Fitted Curve', 'Telemetry Data')
% Model handling diagram
steer_input = delta*tau;
%%

for j = 1:7
    % Set target
    velocity_target = Vx(j); % m/s
    sim('dyn_model_simple.slx'); % simulate model
    
    Vx_SS = mean(u_sim(end - 10:end));
    Vy_SS = mean(v_sim(end - 10:end));
    Omega_SS = mean(Omega_sim(end - 10:end));
    
    curv_model(j) = Omega_SS/Vx_SS;
    ay_model(j) = Omega_SS * Vx_SS;
end



%%


figure()
set(gcf,'units','points','position',[x0,y0,width,height])
plot(a_y, curv, '+','DisplayName','Telemetry')
title('Curvature vs a_y')
hold on
grid on

set(gca,'FontSize',20)
xlb = xlabel('a_y [m/s^2]'); 
ylb = ylabel('\rho [1/m]');
lg = legend()
lg.FontSize = 20;
xlb.FontSize = 20;
ylb.FontSize = 20;

for i = 1:7
    a=std_a_y(i); % horizontal radius
    b=std_curv(i); % vertical radius
    x0=a_y(i); % x0,y0 ellipse centre coordinates
    y0=curv(i);
    t=-pi:0.01:pi;
    x=x0+a*cos(t);
    y=y0+b*sin(t);
    plot(x,y,'HandleVisibility','off')  
end
plot(ay_model, curv_model,'-o','DisplayName','Model')

%%
x =a_y(1:3)';
y = curv(1:3)';
X = [ones(size(x)), x];
X = pinv(X);
Y = y;
theta = X*Y;

xn = linspace(0,6,100)';
Xn = [ones(size(xn)), xn];
Yn = Xn * theta;

tmp = [0:0.5:6];
rect = theta(1)+theta(2)*tmp;

figure;
plot(a_y, curv, '+', xn, Yn, tmp, rect,'--',tmp,theta(1),tmp, ones(size(tmp))*theta(1),'--');
title('curvature vs ay')
ylabel('rho')
xlabel('m/s^2')


figure()
plot(a_y,beta,'+')
title('beta vs ay')
ylabel('deg')
xlabel('m/s^2')
hold on
for i = 1:7
    a=std_a_y(i); % horizontal radius
    b=std_beta(i); % vertical radius
    x0=a_y(i); % x0,y0 ellipse centre coordinates
    y0=beta(i);
    t=-pi:0.01:pi;
    x=x0+a*cos(t);
    y=y0+b*sin(t);
    plot(x,y)  
end




%% 

curv_l = curv;
a_y_l = a_y;

%% Analize state test for erumby project
% clear;clc;

% if the tests have different total time take the smallest time

ts = 0.001;
max_time = 28/ts; 


lpf_beta = designfilt('lowpassiir', ...         % Response type
             'FilterOrder',8, ...      % Filter order
             'HalfPowerFrequency',0.005);
         

% j is the total number of the test

% tyre force and slip angle identification for right cornering test in steady state
for j = 1:8

    filename1 = '../test_session/telemetry_09_05/tyre_force/';
    i_s = num2str(j+11);
    test = strcat('test',i_s,'.txt');
    
    
    filename1 = strcat(filename1,test);

    filename2 = '../test_session/optitrack_09_05/tyre_force/';
    test = strcat('test',i_s,'.csv');
    filename2 = strcat(filename2,test);

    [telemetry] = telemetry_import_V2(filename1);
    [optitrack] = optitrack_import(filename2);

    [remap_telemetry,remap_optitrack] = test_data_remap(telemetry,optitrack);
    
    %indexing for steady state
    
    idx = [14000:27000];
    tmp = atan2(remap_optitrack.Vy_rel,remap_optitrack.Vx_rel);
    tmp = filtfilt(lpf_beta,tmp);
    Vx(j) = mean(remap_optitrack.Vx_rel(idx));
    beta(j) = mean(tmp(idx));
    std_beta(j) = std(tmp(idx));
    V(j) = mean(remap_optitrack.V_gps(idx));
    yaw_rate(j) = mean(remap_telemetry.gyro_z(idx));   
    std_curv(j) = std(remap_telemetry.gyro_z(idx)./remap_optitrack.V_gps(idx));
    a_y(j) = mean(remap_telemetry.a_y(idx));
    std_a_y(j) = std(remap_telemetry.a_y(idx));
    X_gps(1,:,j) = remap_optitrack.x_gps(1:max_time);
    yaw_rate_gps(1,:,j) = remap_telemetry.gyro_z(1:max_time); 

end

curv = yaw_rate./V;



delta_const = mean(remap_telemetry.steering);
delta =   -((Lf+Lr)*(0.0404 + 0.0170*delta_const));  

alpha_f = atan(- beta - Lf*yaw_rate./V) + delta;
alpha_r = atan( - beta + Lr*yaw_rate./V);

Fyr = (a_y*m)./(1+Lr/Lf*cos(delta));
Fyf = Lr/Lf*Fyr;


Fz_f = m*9.81*Lr/(Lf+Lr);
Fz_r = m*9.81*Lf/(Lf+Lr);

D_f = max(abs(Fyf))/Fz_f;

fo = fitoptions('Method','NonlinearLeastSquares',... 
  'Lower',[0,1,-1],...
  'Upper',[1000,1.5,1],...
  'StartPoint',[100, 1.1, 0 ]);

ft = fittype(@(K,C,alpha_off,alpha) Fz_f*D_f*sin(C*atan(K/(C*D_f)*(alpha+alpha_off))),...
             'independent',{'alpha'},...
             'coefficients',{'K','C','alpha_off'});
[curveleft_f,gof2] = fit(alpha_f',Fyf',ft,fo)

figure()
plot(curveleft_f,alpha_f,Fyf)


D_r = max(abs(Fyr))/Fz_r;

fo = fitoptions('Method','NonlinearLeastSquares',... 
  'Lower',[0,1,-1],...
  'Upper',[1000,1.5,1],...
  'StartPoint',[100, 1.1, 0 ]);

ft = fittype(@(K,C,alpha_off,alpha) Fz_r*D_r*sin(C*atan(K/(C*D_r)*(alpha+alpha_off))),...
             'independent',{'alpha'},...
             'coefficients',{'K','C','alpha_off'});
[curveleft_r,gof2] = fit(alpha_r',Fyr',ft,fo)

figure()
plot(curveleft_r,alpha_r,Fyr)

% Model handling diagram
steer_input = delta*tau;


for j = 1:7
    % Set target
    velocity_target = Vx(j); % m/s
    sim('dyn_model_simple.slx'); % simulate model
    
    Vx_SS = mean(Vx_sim(end - 10:end));
    Vy_SS = mean(Vy_sim(end - 10:end));
    Omega_SS = mean(Omega_sim(end - 10:end));
    
    curv_model(j) = Omega_SS/Vx_SS;
    ay_model(j) = Omega_SS * Vx_SS;
end




figure()
plot(a_y, curv, '+','DisplayName','Telemetry')
title('curvature vs ay')
hold on


plot(ay_model, curv_model,'-o','DisplayName','Model')
xlabel('a_y [m/s^2]'); ylabel('\rho [1/m]');
legend

for i = 1:7
    a=std_a_y(i); % horizontal radius
    b=std_curv(i); % vertical radius
    x0=a_y(i); % x0,y0 ellipse centre coordinates
    y0=curv(i);
    t=-pi:0.01:pi;
    x=x0+a*cos(t);
    y=y0+b*sin(t);
    plot(x,y,'HandleVisibility','off')  
end