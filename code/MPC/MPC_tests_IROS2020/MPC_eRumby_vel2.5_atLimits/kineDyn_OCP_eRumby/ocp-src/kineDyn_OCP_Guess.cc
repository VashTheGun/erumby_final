/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP_Guess.cc                                           |
 |                                                                       |
 |  version: 1.0   date 27/2/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "kineDyn_OCP.hh"
#include "kineDyn_OCP_Pars.hh"

#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_abs_reg_DD(__t1) abs_reg.DD( __t1)
#define ALIAS_abs_reg_D(__t1) abs_reg.D( __t1)
#define ALIAS_SignReg_smooth_DD(__t1) SignReg_smooth.DD( __t1)
#define ALIAS_SignReg_smooth_D(__t1) SignReg_smooth.D( __t1)
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_GGdiagramEnvelope_DD(__t1) GGdiagramEnvelope.DD( __t1)
#define ALIAS_GGdiagramEnvelope_D(__t1) GGdiagramEnvelope.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_a__x0Control_D_3(__t1, __t2, __t3) a__x0Control.D_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2(__t1, __t2, __t3) a__x0Control.D_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1(__t1, __t2, __t3) a__x0Control.D_1( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_3_3(__t1, __t2, __t3) a__x0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_3(__t1, __t2, __t3) a__x0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_2(__t1, __t2, __t3) a__x0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_3(__t1, __t2, __t3) a__x0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_2(__t1, __t2, __t3) a__x0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_1(__t1, __t2, __t3) a__x0Control.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3(__t1, __t2, __t3) delta__D0Control.D_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2(__t1, __t2, __t3) delta__D0Control.D_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1(__t1, __t2, __t3) delta__D0Control.D_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3_3(__t1, __t2, __t3) delta__D0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_3(__t1, __t2, __t3) delta__D0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_2(__t1, __t2, __t3) delta__D0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_3(__t1, __t2, __t3) delta__D0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_2(__t1, __t2, __t3) delta__D0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_1(__t1, __t2, __t3) delta__D0Control.D_1_1( __t1, __t2, __t3)


namespace kineDyn_OCPDefine {

  using namespace std;

  /*\
   |    ____
   |   / ___|_   _  ___  ___ ___
   |  | |  _| | | |/ _ \/ __/ __|
   |  | |_| | |_| |  __/\__ \__ \
   |   \____|\__,_|\___||___/___/
  \*/

  void
  kineDyn_OCP::p_guess_eval( P_pointer_type P__ ) const {
  }

  void
  kineDyn_OCP::xlambda_guess_eval(
    integer              i_segment,
    Q_const_pointer_type Q__,
    P_const_pointer_type P__,
    X_pointer_type       X__,
    L_pointer_type       L__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = Q__[0];
    real_type t2   = leftWidth(t1);
    real_type t3   = rightWidth(t1);
    X__[ iX_n        ] = t2 / 2 - t3 / 2;
    X__[ iX_xi       ] = 0;
    X__[ iX_v__x     ] = ModelPars[23];
    X__[ iX_Omega    ] = Q__[1] * X__[2];

  }

  /*\
   |    ____ _               _
   |   / ___| |__   ___  ___| | __
   |  | |   | '_ \ / _ \/ __| |/ /
   |  | |___| | | |  __/ (__|   <
   |   \____|_| |_|\___|\___|_|\_\
  \*/

  #define Xoptima__check__lt(A,B) ( (A) <  (B) )
  #define Xoptima__check__le(A,B) ( (A) <= (B) )

  // Node check strings
  #define __message_node_check_0 "0 < v__x(zeta)"

  bool
  kineDyn_OCP::p_check( P_const_pointer_type P__ ) const {
    bool ok = true;

    return ok;
  }

  bool
  kineDyn_OCP::xlambda_check_node(
    integer              ipos,
    NodeType2 const    & NODE__,
    P_const_pointer_type P__
  ) const {
    bool ok = true;
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    ok = ok && Xoptima__check__lt(0, X__[2]);
    return ok;
  }

  bool
  kineDyn_OCP::xlambda_check_cell(
    integer              icell,
    NodeType2 const    & NODE__,
    P_const_pointer_type P__
  ) const {
    return true;
  }

  bool
  kineDyn_OCP::xlambda_check_cell(
    integer              icell,
    NodeType2 const    & LEFT__,
    NodeType2 const    & RIGHT__,
    P_const_pointer_type P__
  ) const {
    return true;
  }
}

// EOF: kineDyn_OCP_Guess.cc
