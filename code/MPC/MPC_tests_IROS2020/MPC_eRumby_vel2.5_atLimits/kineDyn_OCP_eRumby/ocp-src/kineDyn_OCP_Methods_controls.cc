/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP_Methods.cc                                         |
 |                                                                       |
 |  version: 1.0   date 27/2/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "kineDyn_OCP.hh"
#include "kineDyn_OCP_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_abs_reg_DD(__t1) abs_reg.DD( __t1)
#define ALIAS_abs_reg_D(__t1) abs_reg.D( __t1)
#define ALIAS_SignReg_smooth_DD(__t1) SignReg_smooth.DD( __t1)
#define ALIAS_SignReg_smooth_D(__t1) SignReg_smooth.D( __t1)
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_GGdiagramEnvelope_DD(__t1) GGdiagramEnvelope.DD( __t1)
#define ALIAS_GGdiagramEnvelope_D(__t1) GGdiagramEnvelope.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_a__x0Control_D_3(__t1, __t2, __t3) a__x0Control.D_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2(__t1, __t2, __t3) a__x0Control.D_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1(__t1, __t2, __t3) a__x0Control.D_1( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_3_3(__t1, __t2, __t3) a__x0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_3(__t1, __t2, __t3) a__x0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_2(__t1, __t2, __t3) a__x0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_3(__t1, __t2, __t3) a__x0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_2(__t1, __t2, __t3) a__x0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_1(__t1, __t2, __t3) a__x0Control.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3(__t1, __t2, __t3) delta__D0Control.D_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2(__t1, __t2, __t3) delta__D0Control.D_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1(__t1, __t2, __t3) delta__D0Control.D_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3_3(__t1, __t2, __t3) delta__D0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_3(__t1, __t2, __t3) delta__D0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_2(__t1, __t2, __t3) delta__D0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_3(__t1, __t2, __t3) delta__D0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_2(__t1, __t2, __t3) delta__D0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_1(__t1, __t2, __t3) delta__D0Control.D_1_1( __t1, __t2, __t3)


namespace kineDyn_OCPDefine {

  /*\
   |    __ _
   |   / _` |
   |  | (_| |
   |   \__, |
   |   |___/
  \*/

  integer
  kineDyn_OCP::g_numEqns() const
  { return 2; }

  void
  kineDyn_OCP::g_eval(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t11  = (Q__[1] * X__[0] - 1) / X__[2];
    real_type t13  = cos(X__[1]);
    real_type t14  = 1.0 / t13;
    real_type t15  = t14 * t11;
    real_type t20  = ALIAS_a__x0Control_D_1(U__[0], ModelPars[6], ModelPars[5]);
    result__[ 0   ] = -t15 * L__[3] / ModelPars[62] - t20 * t14 * t11;
    real_type t31  = ALIAS_delta__D0Control_D_1(U__[1], ModelPars[41], ModelPars[40]);
    result__[ 1   ] = -t15 * L__[5] / ModelPars[63] - t31 * t14 * t11;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"g_eval",2);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DgDxlp_numRows() const
  { return 2; }

  integer
  kineDyn_OCP::DgDxlp_numCols() const
  { return 12; }

  integer
  kineDyn_OCP::DgDxlp_nnz() const
  { return 8; }

  void
  kineDyn_OCP::DgDxlp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 9   ;
    iIndex[ 4  ] = 1   ; jIndex[ 4  ] = 0   ;
    iIndex[ 5  ] = 1   ; jIndex[ 5  ] = 1   ;
    iIndex[ 6  ] = 1   ; jIndex[ 6  ] = 2   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 11  ;
  }

  void
  kineDyn_OCP::DgDxlp_sparse(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t3   = 1.0 / ModelPars[62];
    real_type t4   = t3 * L__[3];
    real_type t5   = Q__[1];
    real_type t6   = X__[2];
    real_type t7   = 1.0 / t6;
    real_type t8   = t7 * t5;
    real_type t9   = X__[1];
    real_type t10  = cos(t9);
    real_type t11  = 1.0 / t10;
    real_type t12  = t11 * t8;
    real_type t17  = ALIAS_a__x0Control_D_1(U__[0], ModelPars[6], ModelPars[5]);
    real_type t18  = t17 * t11;
    result__[ 0   ] = -t12 * t4 - t18 * t8;
    real_type t22  = X__[0] * t5 - 1;
    real_type t24  = t10 * t10;
    real_type t25  = 1.0 / t24;
    real_type t27  = sin(t9);
    real_type t28  = t27 * t25 * t7;
    real_type t30  = t7 * t22;
    result__[ 1   ] = -t27 * t17 * t25 * t30 - t28 * t22 * t4;
    real_type t34  = t6 * t6;
    real_type t36  = 1.0 / t34 * t22;
    real_type t37  = t11 * t36;
    result__[ 2   ] = t18 * t36 + t37 * t4;
    real_type t41  = t11 * t7;
    result__[ 3   ] = -t41 * t22 * t3;
    real_type t45  = 1.0 / ModelPars[63];
    real_type t46  = t45 * L__[5];
    real_type t51  = ALIAS_delta__D0Control_D_1(U__[1], ModelPars[41], ModelPars[40]);
    real_type t52  = t51 * t11;
    result__[ 4   ] = -t12 * t46 - t52 * t8;
    result__[ 5   ] = -t27 * t51 * t25 * t30 - t28 * t22 * t46;
    result__[ 6   ] = t52 * t36 + t37 * t46;
    result__[ 7   ] = -t41 * t22 * t45;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DgDxlp_sparse",8);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DgDu_numRows() const
  { return 2; }

  integer
  kineDyn_OCP::DgDu_numCols() const
  { return 2; }

  integer
  kineDyn_OCP::DgDu_nnz() const
  { return 2; }

  void
  kineDyn_OCP::DgDu_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 1   ; jIndex[ 1  ] = 1   ;
  }

  void
  kineDyn_OCP::DgDu_sparse(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t7   = (Q__[1] * X__[0] - 1) / X__[2];
    real_type t9   = cos(X__[1]);
    real_type t10  = 1.0 / t9;
    real_type t14  = ALIAS_a__x0Control_D_1_1(U__[0], ModelPars[6], ModelPars[5]);
    result__[ 0   ] = -t14 * t10 * t7;
    real_type t20  = ALIAS_delta__D0Control_D_1_1(U__[1], ModelPars[41], ModelPars[40]);
    result__[ 1   ] = -t20 * t10 * t7;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DgDu_sparse",2);
    #endif
  }

  /*\
   |   ____            _             _
   |  / ___|___  _ __ | |_ _ __ ___ | |___
   | | |   / _ \| '_ \| __| '__/ _ \| / __|
   | | |__| (_) | | | | |_| | | (_) | \__ \
   |  \____\___/|_| |_|\__|_|  \___/|_|___/
  \*/

  /*\
   |                      _
   |  _  _   _____ ____ _| |
   | | || | / -_) V / _` | |
   |  \_,_|_\___|\_/\__,_|_|
   |     |___|
  \*/
  integer
  kineDyn_OCP::u_numEqns() const
  { return 2; }

  void
  kineDyn_OCP::u_eval_analytic(
    NodeType2 const    & NODE__,
    P_const_pointer_type P__,
    U_pointer_type       U__
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    U__[ iU_a__x0     ] = a__x0Control.solve(-1.0 / ModelPars[62] * L__[3], ModelPars[6], ModelPars[5]);
    U__[ iU_delta__D0 ] = delta__D0Control.solve(-L__[5] / ModelPars[63], ModelPars[41], ModelPars[40]);
  }

  void
  kineDyn_OCP::u_eval_analytic(
    NodeType2 const    & LEFT__,
    NodeType2 const    & RIGHT__,
    P_const_pointer_type P__,
    U_pointer_type       U__
  ) const {
    NodeType2 NODE__;
    real_type Q__[5];
    real_type X__[6];
    real_type L__[6];
    NODE__.i_segment = LEFT__.i_segment;
    NODE__.q         = Q__;
    NODE__.x         = X__;
    NODE__.lambda    = L__;
    // Qvars
    Q__[0] = (LEFT__.q[0]+RIGHT__.q[0])/2;
    Q__[1] = (LEFT__.q[1]+RIGHT__.q[1])/2;
    Q__[2] = (LEFT__.q[2]+RIGHT__.q[2])/2;
    Q__[3] = (LEFT__.q[3]+RIGHT__.q[3])/2;
    Q__[4] = (LEFT__.q[4]+RIGHT__.q[4])/2;
    // Xvars
    X__[0] = (LEFT__.x[0]+RIGHT__.x[0])/2;
    X__[1] = (LEFT__.x[1]+RIGHT__.x[1])/2;
    X__[2] = (LEFT__.x[2]+RIGHT__.x[2])/2;
    X__[3] = (LEFT__.x[3]+RIGHT__.x[3])/2;
    X__[4] = (LEFT__.x[4]+RIGHT__.x[4])/2;
    X__[5] = (LEFT__.x[5]+RIGHT__.x[5])/2;
    // Lvars
    L__[0] = (LEFT__.lambda[0]+RIGHT__.lambda[0])/2;
    L__[1] = (LEFT__.lambda[1]+RIGHT__.lambda[1])/2;
    L__[2] = (LEFT__.lambda[2]+RIGHT__.lambda[2])/2;
    L__[3] = (LEFT__.lambda[3]+RIGHT__.lambda[3])/2;
    L__[4] = (LEFT__.lambda[4]+RIGHT__.lambda[4])/2;
    L__[5] = (LEFT__.lambda[5]+RIGHT__.lambda[5])/2;
    this->u_eval_analytic( NODE__, P__, U__ );
  }

  /*\
   |   ___       ___      _                       _      _   _
   |  |   \ _  _|   \__ _| |_ __   __ _ _ _  __ _| |_  _| |_(_)__
   |  | |) | || | |) \ \ / | '_ \ / _` | ' \/ _` | | || |  _| / _|
   |  |___/ \_,_|___//_\_\_| .__/ \__,_|_||_\__,_|_|\_, |\__|_\__|
   |                       |_|                      |__/
  \*/
  void
  kineDyn_OCP::DuDxlp_full_analytic(
    NodeType2 const          & NODE__,
    P_const_pointer_type       P__,
    U_const_pointer_type       U__,
    MatrixWrapper<real_type> & DuDxlp
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    DuDxlp(0, 0) = 0;
    DuDxlp(1, 0) = 0;
    DuDxlp(0, 1) = 0;
    DuDxlp(1, 1) = 0;
    DuDxlp(0, 2) = 0;
    DuDxlp(1, 2) = 0;
    DuDxlp(0, 3) = 0;
    DuDxlp(1, 3) = 0;
    DuDxlp(0, 4) = 0;
    DuDxlp(1, 4) = 0;
    DuDxlp(0, 5) = 0;
    DuDxlp(1, 5) = 0;
    DuDxlp(0, 6) = 0;
    DuDxlp(1, 6) = 0;
    DuDxlp(0, 7) = 0;
    DuDxlp(1, 7) = 0;
    DuDxlp(0, 8) = 0;
    DuDxlp(1, 8) = 0;
    DuDxlp(0, 9) = -a__x0Control.solve_rhs(-1.0 / ModelPars[62] * L__[3], ModelPars[6], ModelPars[5]) / ModelPars[62];
    DuDxlp(1, 9) = 0;
    DuDxlp(0, 10) = 0;
    DuDxlp(1, 10) = 0;
    DuDxlp(0, 11) = 0;
    DuDxlp(1, 11) = -delta__D0Control.solve_rhs(-L__[5] / ModelPars[63], ModelPars[41], ModelPars[40]) / ModelPars[63];
  }

  void
  kineDyn_OCP::DuDxlp_full_analytic(
    NodeType2 const          & LEFT__,
    NodeType2 const          & RIGHT__,
    P_const_pointer_type       P__,
    U_const_pointer_type       U__,
    MatrixWrapper<real_type> & DuDxlp
  ) const {
    NodeType2 NODE__;
    real_type Q__[5];
    real_type X__[6];
    real_type L__[6];
    NODE__.i_segment = LEFT__.i_segment;
    NODE__.q         = Q__;
    NODE__.x         = X__;
    NODE__.lambda    = L__;
    // Qvars
    Q__[0] = (LEFT__.q[0]+RIGHT__.q[0])/2;
    Q__[1] = (LEFT__.q[1]+RIGHT__.q[1])/2;
    Q__[2] = (LEFT__.q[2]+RIGHT__.q[2])/2;
    Q__[3] = (LEFT__.q[3]+RIGHT__.q[3])/2;
    Q__[4] = (LEFT__.q[4]+RIGHT__.q[4])/2;
    // Xvars
    X__[0] = (LEFT__.x[0]+RIGHT__.x[0])/2;
    X__[1] = (LEFT__.x[1]+RIGHT__.x[1])/2;
    X__[2] = (LEFT__.x[2]+RIGHT__.x[2])/2;
    X__[3] = (LEFT__.x[3]+RIGHT__.x[3])/2;
    X__[4] = (LEFT__.x[4]+RIGHT__.x[4])/2;
    X__[5] = (LEFT__.x[5]+RIGHT__.x[5])/2;
    // Lvars
    L__[0] = (LEFT__.lambda[0]+RIGHT__.lambda[0])/2;
    L__[1] = (LEFT__.lambda[1]+RIGHT__.lambda[1])/2;
    L__[2] = (LEFT__.lambda[2]+RIGHT__.lambda[2])/2;
    L__[3] = (LEFT__.lambda[3]+RIGHT__.lambda[3])/2;
    L__[4] = (LEFT__.lambda[4]+RIGHT__.lambda[4])/2;
    L__[5] = (LEFT__.lambda[5]+RIGHT__.lambda[5])/2;
    this->DuDxlp_full_analytic( NODE__, P__, U__, DuDxlp );
  }

  /*\
   |   ___       ___      _                               _
   |  |   \ _  _|   \__ _| |_ __   _ _ _  _ _ __  ___ _ _(_)__
   |  | |) | || | |) \ \ / | '_ \ | ' \ || | '  \/ -_) '_| / _|
   |  |___/ \_,_|___//_\_\_| .__/ |_||_\_,_|_|_|_\___|_| |_\__|
   |                       |_|
  \*/

  bool
  kineDyn_OCP::DuDxlp_full_numeric(
    NodeType2 const          & NODE__,
    P_const_pointer_type       P__,
    U_const_pointer_type       U__,
    MatrixWrapper<real_type> & DuDxlp
  ) const {
    static integer DgDu_I[ 2 ], DgDu_J[ 2 ];
    static integer DgDxlp_I[ 8 ], DgDxlp_J[ 8 ];
    static bool    computed_pattern = false;
    if ( !computed_pattern ) {
      this->DgDu_pattern( DgDu_I, DgDu_J );
      this->DgDxlp_pattern( DgDxlp_I, DgDxlp_J );
      computed_pattern = true;
    }
    real_type DgDu_V[ 2 ], DgDxlp_V[ 8 ];
    this->DgDu_sparse( NODE__, U__, P__, DgDu_V );
    this->DgDxlp_sparse( NODE__, U__, P__, DgDxlp_V );

    integer   ipiv[ 2 ];
    real_type DgDu_storage[ 4 ];
    MatrixWrapper<real_type> DgDu( DgDu_storage, 2, 2, 2 );
    DgDu.zero_fill();
    DuDxlp.zero_fill();

    // fill DgDu
    DgDu( DgDu_I[ 0 ], DgDu_J[ 0 ] ) = DgDu_V[0];
    DgDu( DgDu_I[ 1 ], DgDu_J[ 1 ] ) = DgDu_V[1];
    // fill DgDxlp
    DuDxlp( DgDxlp_I[ 0 ], DgDxlp_J[ 0] ) = -DgDxlp_V[0];
    DuDxlp( DgDxlp_I[ 1 ], DgDxlp_J[ 1] ) = -DgDxlp_V[1];
    DuDxlp( DgDxlp_I[ 2 ], DgDxlp_J[ 2] ) = -DgDxlp_V[2];
    DuDxlp( DgDxlp_I[ 3 ], DgDxlp_J[ 3] ) = -DgDxlp_V[3];
    DuDxlp( DgDxlp_I[ 4 ], DgDxlp_J[ 4] ) = -DgDxlp_V[4];
    DuDxlp( DgDxlp_I[ 5 ], DgDxlp_J[ 5] ) = -DgDxlp_V[5];
    DuDxlp( DgDxlp_I[ 6 ], DgDxlp_J[ 6] ) = -DgDxlp_V[6];
    DuDxlp( DgDxlp_I[ 7 ], DgDxlp_J[ 7] ) = -DgDxlp_V[7];
    integer info = alglin::gesv( 2, 12, DgDu.get_data(), 2, ipiv, DuDxlp.get_data(), 2 );
    return info == 0;
  }

  bool
  kineDyn_OCP::DuDxlp_full_numeric(
    NodeType2 const          & LEFT__,
    NodeType2 const          & RIGHT__,
    P_const_pointer_type       P__,
    U_const_pointer_type       U__,
    MatrixWrapper<real_type> & DuDxlp
  ) const {
    NodeType2 NODE__;
    real_type Q__[5];
    real_type X__[6];
    real_type L__[6];
    NODE__.i_segment = LEFT__.i_segment;
    NODE__.q         = Q__;
    NODE__.x         = X__;
    NODE__.lambda    = L__;
    // Qvars
    Q__[0] = (LEFT__.q[0]+RIGHT__.q[0])/2;
    Q__[1] = (LEFT__.q[1]+RIGHT__.q[1])/2;
    Q__[2] = (LEFT__.q[2]+RIGHT__.q[2])/2;
    Q__[3] = (LEFT__.q[3]+RIGHT__.q[3])/2;
    Q__[4] = (LEFT__.q[4]+RIGHT__.q[4])/2;
    // Xvars
    X__[0] = (LEFT__.x[0]+RIGHT__.x[0])/2;
    X__[1] = (LEFT__.x[1]+RIGHT__.x[1])/2;
    X__[2] = (LEFT__.x[2]+RIGHT__.x[2])/2;
    X__[3] = (LEFT__.x[3]+RIGHT__.x[3])/2;
    X__[4] = (LEFT__.x[4]+RIGHT__.x[4])/2;
    X__[5] = (LEFT__.x[5]+RIGHT__.x[5])/2;
    // Lvars
    L__[0] = (LEFT__.lambda[0]+RIGHT__.lambda[0])/2;
    L__[1] = (LEFT__.lambda[1]+RIGHT__.lambda[1])/2;
    L__[2] = (LEFT__.lambda[2]+RIGHT__.lambda[2])/2;
    L__[3] = (LEFT__.lambda[3]+RIGHT__.lambda[3])/2;
    L__[4] = (LEFT__.lambda[4]+RIGHT__.lambda[4])/2;
    L__[5] = (LEFT__.lambda[5]+RIGHT__.lambda[5])/2;
    return this->DuDxlp_full_numeric( NODE__, P__, U__, DuDxlp );
  }

  /*\
   |   ____                                  _   _     _       _
   |  / ___|  ___  __ _ _ __ ___   ___ _ __ | |_| |   (_)_ __ | | __
   |  \___ \ / _ \/ _` | '_ ` _ \ / _ \ '_ \| __| |   | | '_ \| |/ /
   |   ___) |  __/ (_| | | | | | |  __/ | | | |_| |___| | | | |   <
   |  |____/ \___|\__, |_| |_| |_|\___|_| |_|\__|_____|_|_| |_|_|\_\
   |              |___/
  \*/

  integer
  kineDyn_OCP::segmentLink_numEqns() const
  { return 0; }

  void
  kineDyn_OCP::segmentLink_eval(
    NodeType const     & L,
    NodeType const     & R,
    P_const_pointer_type p,
    real_type            segmentLink[]
  ) const {
    LW_ERROR0("NON IMPLEMENTATA\n");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DsegmentLinkDxp_numRows() const
  { return 0; }

  integer
  kineDyn_OCP::DsegmentLinkDxp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DsegmentLinkDxp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DsegmentLinkDxp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    LW_ERROR0("NON IMPLEMENTATA\n");
  }

  void
  kineDyn_OCP::DsegmentLinkDxp_sparse(
    NodeType const     & L,
    NodeType const     & R,
    P_const_pointer_type p,
    real_type            DsegmentLinkDxp[]
  ) const {
    LW_ERROR0("NON IMPLEMENTATA\n");
  }

  /*\
   |     _
   |  _ | |_  _ _ __  _ __
   | | || | || | '  \| '_ \
   |  \__/ \_,_|_|_|_| .__/
   |                 |_|
  \*/

  integer
  kineDyn_OCP::jump_numEqns() const
  { return 12; }

  void
  kineDyn_OCP::jump_eval(
    NodeType2 const    & LEFT__,
    NodeType2 const    & RIGHT__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment_left  = LEFT__.i_segment;
    real_type const * QL__  = LEFT__.q;
    real_type const * XL__  = LEFT__.x;
    real_type const * LL__  = LEFT__.lambda;
    integer i_segment_right = RIGHT__.i_segment;
    real_type const * QR__  = RIGHT__.q;
    real_type const * XR__  = RIGHT__.x;
    real_type const * LR__  = RIGHT__.lambda;
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    result__[ 0   ] = XR__[0] - XL__[0];
    result__[ 1   ] = XR__[1] - XL__[1];
    result__[ 2   ] = XR__[2] - XL__[2];
    result__[ 3   ] = XR__[3] - XL__[3];
    result__[ 4   ] = XR__[4] - XL__[4];
    result__[ 5   ] = XR__[5] - XL__[5];
    result__[ 6   ] = LR__[0] - LL__[0];
    result__[ 7   ] = LR__[1] - LL__[1];
    result__[ 8   ] = LR__[2] - LL__[2];
    result__[ 9   ] = LR__[3] - LL__[3];
    result__[ 10  ] = LR__[4] - LL__[4];
    result__[ 11  ] = LR__[5] - LL__[5];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"jump_eval",12);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DjumpDxlp_numRows() const
  { return 12; }

  integer
  kineDyn_OCP::DjumpDxlp_numCols() const
  { return 24; }

  integer
  kineDyn_OCP::DjumpDxlp_nnz() const
  { return 24; }

  void
  kineDyn_OCP::DjumpDxlp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 12  ;
    iIndex[ 2  ] = 1   ; jIndex[ 2  ] = 1   ;
    iIndex[ 3  ] = 1   ; jIndex[ 3  ] = 13  ;
    iIndex[ 4  ] = 2   ; jIndex[ 4  ] = 2   ;
    iIndex[ 5  ] = 2   ; jIndex[ 5  ] = 14  ;
    iIndex[ 6  ] = 3   ; jIndex[ 6  ] = 3   ;
    iIndex[ 7  ] = 3   ; jIndex[ 7  ] = 15  ;
    iIndex[ 8  ] = 4   ; jIndex[ 8  ] = 4   ;
    iIndex[ 9  ] = 4   ; jIndex[ 9  ] = 16  ;
    iIndex[ 10 ] = 5   ; jIndex[ 10 ] = 5   ;
    iIndex[ 11 ] = 5   ; jIndex[ 11 ] = 17  ;
    iIndex[ 12 ] = 6   ; jIndex[ 12 ] = 6   ;
    iIndex[ 13 ] = 6   ; jIndex[ 13 ] = 18  ;
    iIndex[ 14 ] = 7   ; jIndex[ 14 ] = 7   ;
    iIndex[ 15 ] = 7   ; jIndex[ 15 ] = 19  ;
    iIndex[ 16 ] = 8   ; jIndex[ 16 ] = 8   ;
    iIndex[ 17 ] = 8   ; jIndex[ 17 ] = 20  ;
    iIndex[ 18 ] = 9   ; jIndex[ 18 ] = 9   ;
    iIndex[ 19 ] = 9   ; jIndex[ 19 ] = 21  ;
    iIndex[ 20 ] = 10  ; jIndex[ 20 ] = 10  ;
    iIndex[ 21 ] = 10  ; jIndex[ 21 ] = 22  ;
    iIndex[ 22 ] = 11  ; jIndex[ 22 ] = 11  ;
    iIndex[ 23 ] = 11  ; jIndex[ 23 ] = 23  ;
  }

  void
  kineDyn_OCP::DjumpDxlp_sparse(
    NodeType2 const    & LEFT__,
    NodeType2 const    & RIGHT__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment_left  = LEFT__.i_segment;
    real_type const * QL__  = LEFT__.q;
    real_type const * XL__  = LEFT__.x;
    real_type const * LL__  = LEFT__.lambda;
    integer i_segment_right = RIGHT__.i_segment;
    real_type const * QR__  = RIGHT__.q;
    real_type const * XR__  = RIGHT__.x;
    real_type const * LR__  = RIGHT__.lambda;
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    result__[ 0   ] = -1;
    result__[ 1   ] = 1;
    result__[ 2   ] = -1;
    result__[ 3   ] = 1;
    result__[ 4   ] = -1;
    result__[ 5   ] = 1;
    result__[ 6   ] = -1;
    result__[ 7   ] = 1;
    result__[ 8   ] = -1;
    result__[ 9   ] = 1;
    result__[ 10  ] = -1;
    result__[ 11  ] = 1;
    result__[ 12  ] = -1;
    result__[ 13  ] = 1;
    result__[ 14  ] = -1;
    result__[ 15  ] = 1;
    result__[ 16  ] = -1;
    result__[ 17  ] = 1;
    result__[ 18  ] = -1;
    result__[ 19  ] = 1;
    result__[ 20  ] = -1;
    result__[ 21  ] = 1;
    result__[ 22  ] = -1;
    result__[ 23  ] = 1;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DjumpDxlp_sparse",24);
    #endif
  }

}

// EOF: kineDyn_OCP_Methods.cc
