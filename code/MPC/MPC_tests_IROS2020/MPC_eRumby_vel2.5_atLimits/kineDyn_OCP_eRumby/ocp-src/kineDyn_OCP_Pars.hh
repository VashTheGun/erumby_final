/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP_Pars.hh                                            |
 |                                                                       |
 |  version: 1.0   date 27/2/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#ifndef KINEDYN_OCPPARS_HH
#define KINEDYN_OCPPARS_HH

#define numBc                    0
#define numModelPars             65
#define numConstraint1D          3
#define numConstraint2D          0
#define numConstraintU           2
#define numXvars                 6
#define numLvars                 6
#define numUvars                 2
#define numOMEGAvars             0
#define numQvars                 5
#define numPvars                 0
#define numPostProcess           20
#define numIntegratedPostProcess 1
#define numContinuationSteps     0

// Xvars
#define iX_n            0
#define iX_xi           1
#define iX_v__x         2
#define iX_a__x         3
#define iX_Omega        4
#define iX_delta__D     5

// Lvars
#define iL_lambda1__xo  0
#define iL_lambda2__xo  1
#define iL_lambda3__xo  2
#define iL_lambda4__xo  3
#define iL_lambda5__xo  4
#define iL_lambda6__xo  5

// Uvars
#define iU_a__x0        0
#define iU_delta__D0    1

// Qvars
#define iQ_zeta         0
#define iQ_Curv         1
#define iQ_xLane        2
#define iQ_yLane        3
#define iQ_theta        4

// Pvars

// ModelPars Maps
#define iM_L            0
#define iM_Omega_i      1
#define iM_WBCF__n      2
#define iM_WBCI__n      3
#define iM_a_xi         4
#define iM_ax0_max      5
#define iM_ax0_min      6
#define iM_axoffs       7
#define iM_kUS_0        8
#define iM_kUS_1        9
#define iM_kUS_2        10
#define iM_kUS_3        11
#define iM_kUS_4        12
#define iM_kUS_5        13
#define iM_kUS_6        14
#define iM_kUS_7        15
#define iM_k__D         16
#define iM_m            17
#define iM_n_f          18
#define iM_n_i          19
#define iM_nlow         20
#define iM_nupp         21
#define iM_tau__D       22
#define iM_v__x0        23
#define iM_v_lim        24
#define iM_v_xf         25
#define iM_v_xi         26
#define iM_wN           27
#define iM_wT           28
#define iM_wU           29
#define iM_xi_f         30
#define iM_xi_i         31
#define iM_WBCF__vx     32
#define iM_WBCF__xi     33
#define iM_WBCI__Omega  34
#define iM_WBCI__ax     35
#define iM_WBCI__delta  36
#define iM_WBCI__vx     37
#define iM_WBCI__xi     38
#define iM_deltaD_i     39
#define iM_delta_max    40
#define iM_delta_min    41
#define iM_fit_axM_0    42
#define iM_fit_axM_1    43
#define iM_fit_axM_2    44
#define iM_fit_axM_3    45
#define iM_fit_axM_4    46
#define iM_fit_axM_5    47
#define iM_fit_axM_6    48
#define iM_fit_axM_7    49
#define iM_fit_axm_0    50
#define iM_fit_axm_1    51
#define iM_fit_axm_2    52
#define iM_fit_axm_3    53
#define iM_fit_axm_4    54
#define iM_fit_ayM_0    55
#define iM_fit_ayM_1    56
#define iM_fit_ayM_2    57
#define iM_fit_ayM_3    58
#define iM_fit_ayM_4    59
#define iM_fit_ayM_5    60
#define iM_tau__Omega   61
#define iM_tau__a__x    62
#define iM_tau__delta   63
#define iM_vehHalfWidth 64

#endif

// EOF: kineDyn_OCP_Pars.hh
