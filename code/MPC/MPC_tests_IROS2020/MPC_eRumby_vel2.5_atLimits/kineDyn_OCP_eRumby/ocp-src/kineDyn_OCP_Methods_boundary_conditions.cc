/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP_Methods.cc                                         |
 |                                                                       |
 |  version: 1.0   date 27/2/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "kineDyn_OCP.hh"
#include "kineDyn_OCP_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_abs_reg_DD(__t1) abs_reg.DD( __t1)
#define ALIAS_abs_reg_D(__t1) abs_reg.D( __t1)
#define ALIAS_SignReg_smooth_DD(__t1) SignReg_smooth.DD( __t1)
#define ALIAS_SignReg_smooth_D(__t1) SignReg_smooth.D( __t1)
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_GGdiagramEnvelope_DD(__t1) GGdiagramEnvelope.DD( __t1)
#define ALIAS_GGdiagramEnvelope_D(__t1) GGdiagramEnvelope.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_a__x0Control_D_3(__t1, __t2, __t3) a__x0Control.D_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2(__t1, __t2, __t3) a__x0Control.D_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1(__t1, __t2, __t3) a__x0Control.D_1( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_3_3(__t1, __t2, __t3) a__x0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_3(__t1, __t2, __t3) a__x0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_2(__t1, __t2, __t3) a__x0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_3(__t1, __t2, __t3) a__x0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_2(__t1, __t2, __t3) a__x0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_1(__t1, __t2, __t3) a__x0Control.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3(__t1, __t2, __t3) delta__D0Control.D_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2(__t1, __t2, __t3) delta__D0Control.D_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1(__t1, __t2, __t3) delta__D0Control.D_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3_3(__t1, __t2, __t3) delta__D0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_3(__t1, __t2, __t3) delta__D0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_2(__t1, __t2, __t3) delta__D0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_3(__t1, __t2, __t3) delta__D0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_2(__t1, __t2, __t3) delta__D0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_1(__t1, __t2, __t3) delta__D0Control.D_1_1( __t1, __t2, __t3)


namespace kineDyn_OCPDefine {

  /*\
   |   ___                   _
   |  | _ ) ___ _  _ _ _  __| |__ _ _ _ _  _
   |  | _ \/ _ \ || | ' \/ _` / _` | '_| || |
   |  |___/\___/\_,_|_||_\__,_\__,_|_|  \_, |
   |    ___             _ _ _   _       |__/
   |   / __|___ _ _  __| (_) |_(_)___ _ _  ___
   |  | (__/ _ \ ' \/ _` | |  _| / _ \ ' \(_-<
   |   \___\___/_||_\__,_|_|\__|_\___/_||_/__/
  \*/

  integer
  kineDyn_OCP::boundaryConditions_numEqns() const
  { return 0; }

  void
  kineDyn_OCP::boundaryConditions_eval(
    NodeType const     & LEFT__,
    NodeType const     & RIGHT__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment_left  = LEFT__.i_segment;
    real_type const * QL__  = LEFT__.q;
    real_type const * XL__  = LEFT__.x;
    integer i_segment_right = RIGHT__.i_segment;
    real_type const * QR__  = RIGHT__.q;
    real_type const * XR__  = RIGHT__.x;
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);

    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"boundaryConditions_eval",0);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  kineDyn_OCP::DboundaryConditionsDx_numRows() const
  { return 0; }

  integer
  kineDyn_OCP::DboundaryConditionsDx_numCols() const
  { return 12; }

  integer
  kineDyn_OCP::DboundaryConditionsDx_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DboundaryConditionsDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {

  }

  void
  kineDyn_OCP::DboundaryConditionsDx_sparse(
    NodeType const     & LEFT__,
    NodeType const     & RIGHT__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  kineDyn_OCP::DboundaryConditionsDp_numRows() const
  { return 0; }

  integer
  kineDyn_OCP::DboundaryConditionsDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DboundaryConditionsDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DboundaryConditionsDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {

  }

  void
  kineDyn_OCP::DboundaryConditionsDp_sparse(
    NodeType const     & LEFT__,
    NodeType const     & RIGHT__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY

  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::adjointBC_numEqns() const
  { return 12; }

  void
  kineDyn_OCP::adjointBC_eval(
    NodeType2 const             & LEFT__,
    NodeType2 const             & RIGHT__,
    P_const_pointer_type          P__,
    OMEGA_full_const_pointer_type OMEGA__,
    real_type                     result__[]
  ) const {
    integer i_segment_left  = LEFT__.i_segment;
    real_type const * QL__  = LEFT__.q;
    real_type const * XL__  = LEFT__.x;
    real_type const * LL__  = LEFT__.lambda;
    integer i_segment_right = RIGHT__.i_segment;
    real_type const * QR__  = RIGHT__.q;
    real_type const * XR__  = RIGHT__.x;
    real_type const * LR__  = RIGHT__.lambda;
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    result__[ 0   ] = 2 * ModelPars[3] * (XL__[0] - ModelPars[19]) + LL__[0];
    result__[ 1   ] = 2 * ModelPars[38] * (XL__[1] - ModelPars[31]) + LL__[1];
    result__[ 2   ] = 2 * ModelPars[37] * (XL__[2] - ModelPars[26]) + LL__[2];
    result__[ 3   ] = 2 * ModelPars[35] * (XL__[3] - ModelPars[4]) + LL__[3];
    result__[ 4   ] = 2 * ModelPars[34] * (XL__[4] - ModelPars[1]) + LL__[4];
    result__[ 5   ] = 2 * ModelPars[36] * (XL__[5] - ModelPars[39]) + LL__[5];
    result__[ 6   ] = 2 * ModelPars[2] * (XR__[0] - ModelPars[18]) - LR__[0];
    result__[ 7   ] = 2 * ModelPars[33] * (XR__[1] - ModelPars[30]) - LR__[1];
    result__[ 8   ] = 2 * ModelPars[32] * (XR__[2] - ModelPars[25]) - LR__[2];
    result__[ 9   ] = -LR__[3];
    result__[ 10  ] = -LR__[4];
    result__[ 11  ] = -LR__[5];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"adjointBC_eval",12);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  kineDyn_OCP::DadjointBCDx_numRows() const
  { return 12; }

  integer
  kineDyn_OCP::DadjointBCDx_numCols() const
  { return 12; }

  integer
  kineDyn_OCP::DadjointBCDx_nnz() const
  { return 9; }

  void
  kineDyn_OCP::DadjointBCDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 1   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 2   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 3   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 4   ; jIndex[ 4  ] = 4   ;
    iIndex[ 5  ] = 5   ; jIndex[ 5  ] = 5   ;
    iIndex[ 6  ] = 6   ; jIndex[ 6  ] = 6   ;
    iIndex[ 7  ] = 7   ; jIndex[ 7  ] = 7   ;
    iIndex[ 8  ] = 8   ; jIndex[ 8  ] = 8   ;
  }

  void
  kineDyn_OCP::DadjointBCDx_sparse(
    NodeType2 const             & LEFT__,
    NodeType2 const             & RIGHT__,
    P_const_pointer_type          P__,
    OMEGA_full_const_pointer_type OMEGA__,
    real_type                     result__[]
  ) const {
    integer i_segment_left  = LEFT__.i_segment;
    real_type const * QL__  = LEFT__.q;
    real_type const * XL__  = LEFT__.x;
    real_type const * LL__  = LEFT__.lambda;
    integer i_segment_right = RIGHT__.i_segment;
    real_type const * QR__  = RIGHT__.q;
    real_type const * XR__  = RIGHT__.x;
    real_type const * LR__  = RIGHT__.lambda;
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    result__[ 0   ] = 2 * ModelPars[3];
    result__[ 1   ] = 2 * ModelPars[38];
    result__[ 2   ] = 2 * ModelPars[37];
    result__[ 3   ] = 2 * ModelPars[35];
    result__[ 4   ] = 2 * ModelPars[34];
    result__[ 5   ] = 2 * ModelPars[36];
    result__[ 6   ] = 2 * ModelPars[2];
    result__[ 7   ] = 2 * ModelPars[33];
    result__[ 8   ] = 2 * ModelPars[32];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DadjointBCDxp_sparse",9);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  integer
  kineDyn_OCP::DadjointBCDp_numRows() const
  { return 12; }

  integer
  kineDyn_OCP::DadjointBCDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DadjointBCDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DadjointBCDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {

  }

  void
  kineDyn_OCP::DadjointBCDp_sparse(
    NodeType2 const             & LEFT__,
    NodeType2 const             & RIGHT__,
    P_const_pointer_type          P__,
    OMEGA_full_const_pointer_type OMEGA__,
    real_type                     result__[]
  ) const {
    // EMPTY!
  }

}

// EOF: kineDyn_OCP_Methods.cc
