/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP_Methods.cc                                         |
 |                                                                       |
 |  version: 1.0   date 27/2/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "kineDyn_OCP.hh"
#include "kineDyn_OCP_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_abs_reg_DD(__t1) abs_reg.DD( __t1)
#define ALIAS_abs_reg_D(__t1) abs_reg.D( __t1)
#define ALIAS_SignReg_smooth_DD(__t1) SignReg_smooth.DD( __t1)
#define ALIAS_SignReg_smooth_D(__t1) SignReg_smooth.D( __t1)
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_GGdiagramEnvelope_DD(__t1) GGdiagramEnvelope.DD( __t1)
#define ALIAS_GGdiagramEnvelope_D(__t1) GGdiagramEnvelope.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_a__x0Control_D_3(__t1, __t2, __t3) a__x0Control.D_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2(__t1, __t2, __t3) a__x0Control.D_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1(__t1, __t2, __t3) a__x0Control.D_1( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_3_3(__t1, __t2, __t3) a__x0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_3(__t1, __t2, __t3) a__x0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_2(__t1, __t2, __t3) a__x0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_3(__t1, __t2, __t3) a__x0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_2(__t1, __t2, __t3) a__x0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_1(__t1, __t2, __t3) a__x0Control.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3(__t1, __t2, __t3) delta__D0Control.D_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2(__t1, __t2, __t3) delta__D0Control.D_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1(__t1, __t2, __t3) delta__D0Control.D_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3_3(__t1, __t2, __t3) delta__D0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_3(__t1, __t2, __t3) delta__D0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_2(__t1, __t2, __t3) delta__D0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_3(__t1, __t2, __t3) delta__D0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_2(__t1, __t2, __t3) delta__D0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_1(__t1, __t2, __t3) delta__D0Control.D_1_1( __t1, __t2, __t3)


namespace kineDyn_OCPDefine {

  /*\
   |  _   _
   | | | | |_  __
   | | |_| \ \/ /
   | |  _  |>  <
   | |_| |_/_/\_\
   |
  \*/

  integer
  kineDyn_OCP::Hx_numEqns() const
  { return 6; }

  void
  kineDyn_OCP::Hx_eval(
    NodeType2 const    & NODE__,
    V_const_pointer_type V__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = Q__[1];
    real_type t2   = X__[2];
    real_type t3   = 1.0 / t2;
    real_type t4   = t3 * t1;
    real_type t5   = X__[1];
    real_type t6   = cos(t5);
    real_type t7   = 1.0 / t6;
    real_type t8   = X__[0];
    real_type t9   = ModelPars[64];
    real_type t10  = Q__[0];
    real_type t11  = rightWidth(t10);
    real_type t12  = t8 - t9 + t11;
    real_type t13  = roadRightLateralBoundaries(t12);
    real_type t14  = t13 * t7;
    real_type t16  = t8 * t1;
    real_type t17  = t16 - 1;
    real_type t18  = t3 * t17;
    real_type t19  = ALIAS_roadRightLateralBoundaries_D(t12);
    real_type t22  = leftWidth(t10);
    real_type t23  = t22 - t8 - t9;
    real_type t24  = roadLeftLateralBoundaries(t23);
    real_type t25  = t24 * t7;
    real_type t27  = ALIAS_roadLeftLateralBoundaries_D(t23);
    real_type t30  = X__[3];
    real_type t32  = t30 - ModelPars[7];
    real_type t33  = SignReg(t32);
    real_type t35  = t33 / 2 + 1.0 / 2.0;
    real_type t36  = X__[4];
    real_type t37  = t36 * t2;
    real_type t38  = abs_reg(t37);
    real_type t39  = aymax(t2);
    real_type t40  = 1.0 / t39;
    real_type t41  = t40 * t38;
    real_type t42  = ModelPars[21];
    real_type t43  = pow(t41, t42);
    real_type t44  = abs_reg(t32);
    real_type t45  = axmax(t2);
    real_type t46  = 1.0 / t45;
    real_type t48  = pow(t46 * t44, t42);
    real_type t49  = t43 + t48;
    real_type t52  = t33 / 2 - 1.0 / 2.0;
    real_type t53  = ModelPars[20];
    real_type t54  = pow(t41, t53);
    real_type t55  = axmin(t2);
    real_type t56  = 1.0 / t55;
    real_type t58  = pow(t56 * t44, t53);
    real_type t59  = t54 + t58;
    real_type t61  = -t49 * t35 + t59 * t52 + 1;
    real_type t62  = GGdiagramEnvelope(t61);
    real_type t63  = t62 * t7;
    real_type t65  = ModelPars[27];
    real_type t67  = t7 * t18;
    real_type t71  = ModelPars[29];
    real_type t73  = t2 - ModelPars[24];
    real_type t74  = t73 * t73;
    real_type t76  = t8 * t8;
    real_type t78  = t76 * t65 + t74 * t71 + ModelPars[28];
    real_type t80  = t7 * t3;
    real_type t82  = L__[0];
    real_type t83  = sin(t5);
    real_type t87  = L__[1];
    real_type t88  = t1 * t87;
    real_type t92  = L__[2];
    real_type t93  = ModelPars[16];
    real_type t94  = t2 * t2;
    real_type t96  = ModelPars[17];
    real_type t100 = 1.0 / t96;
    real_type t101 = t100 * (t96 * t30 - t94 * t93) * t92;
    real_type t102 = t7 * t4;
    real_type t104 = L__[3];
    real_type t109 = 1.0 / ModelPars[62];
    real_type t110 = t109 * (U__[0] - t30) * t104;
    real_type t113 = ModelPars[0];
    real_type t115 = 1.0 / t113 * L__[4];
    real_type t116 = ModelPars[22];
    real_type t117 = 1.0 / t116;
    real_type t119 = 1.0 / ModelPars[61];
    real_type t121 = t119 * t117 * t115;
    real_type t124 = k__US(t36, t2);
    real_type t127 = X__[5];
    real_type t129 = t116 * t113 * t36 + t116 * t2 * t124 - t2 * t127;
    real_type t133 = L__[5];
    real_type t138 = 1.0 / ModelPars[63];
    real_type t139 = t138 * (U__[1] - t127) * t133;
    result__[ 0   ] = t80 * t1 * t129 * t121 - t7 * t1 * t83 * t82 - t7 * t3 * t36 * t88 - t80 * t1 * t78 - t19 * t7 * t18 + t27 * t7 * t18 - 2 * t67 * t8 * t65 - t102 * t101 - t102 * t110 - t102 * t139 - t14 * t4 - t25 * t4 - t63 * t4;
    real_type t141 = t6 * t6;
    real_type t142 = 1.0 / t141;
    real_type t152 = t17 * t78;
    real_type t154 = t83 * t142 * t3;
    real_type t157 = t83 * t83;
    real_type t167 = (t6 * t2 * t1 + t36 * t16 - t36) * t87;
    real_type t170 = t83 * t142 * t18;
    real_type t173 = t17 * t129;
    result__[ 1   ] = -t83 * t13 * t142 * t18 - t142 * t17 * t157 * t82 - t83 * t24 * t142 * t18 - t83 * t62 * t142 * t18 + t154 * t173 * t121 + t7 * t83 * t88 - t170 * t101 - t170 * t110 - t170 * t139 - t154 * t152 - t154 * t167 - t17 * t82;
    real_type t177 = 1.0 / t94;
    real_type t178 = t177 * t17;
    real_type t182 = ALIAS_GGdiagramEnvelope_D(t61);
    real_type t183 = t182 * t7;
    real_type t185 = ALIAS_abs_reg_D(t37);
    real_type t188 = t39 * t39;
    real_type t191 = aymax_D(t2);
    real_type t194 = 1.0 / t38;
    real_type t196 = t39 * t194 * (t40 * t36 * t185 - t191 / t188 * t38);
    real_type t199 = axmax_D(t2);
    real_type t207 = axmin_D(t2);
    real_type t218 = t7 * t177;
    real_type t227 = t7 * t178;
    real_type t230 = k__US_D_2(t36, t2);
    result__[ 2   ] = t14 * t178 + t25 * t178 + t63 * t178 - (-(-t199 * t46 * t42 * t48 + t196 * t42 * t43) * t35 + (-t207 * t56 * t53 * t58 + t196 * t53 * t54) * t52) * t183 * t18 - 2 * t67 * t73 * t71 + t218 * t152 - t3 * t88 + t218 * t167 + 2 * t7 * t17 * t100 * t93 * t92 + t227 * t101 + t227 * t110 + t80 * t17 * (t116 * t2 * t230 + t116 * t124 - t127) * t121 - t218 * t173 * t121 + t227 * t139;
    real_type t241 = ALIAS_SignReg_D(t32);
    real_type t245 = ALIAS_abs_reg_D(t32);
    real_type t247 = 1.0 / t44;
    result__[ 3   ] = -(-t49 * t241 / 2 - t247 * t245 * t42 * t48 * t35 + t59 * t241 / 2 + t247 * t245 * t53 * t58 * t52) * t183 * t18 - t80 * t17 * t92 + t67 * t109 * t104;
    real_type t266 = t194 * t2 * t185;
    real_type t277 = k__US_D_1(t36, t2);
    result__[ 4   ] = -(-t266 * t42 * t43 * t35 + t266 * t53 * t54 * t52) * t183 * t18 - t80 * t17 * t87 + t80 * t17 * (t116 * t2 * t277 + t116 * t113) * t121;
    result__[ 5   ] = -t7 * t17 * t119 * t117 * t115 + t67 * t138 * t133;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hx_eval",6);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DHxDx_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DHxDx_numCols() const
  { return 6; }

  integer
  kineDyn_OCP::DHxDx_nnz() const
  { return 31; }

  void
  kineDyn_OCP::DHxDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 0   ; jIndex[ 3  ] = 3   ;
    iIndex[ 4  ] = 0   ; jIndex[ 4  ] = 4   ;
    iIndex[ 5  ] = 0   ; jIndex[ 5  ] = 5   ;
    iIndex[ 6  ] = 1   ; jIndex[ 6  ] = 0   ;
    iIndex[ 7  ] = 1   ; jIndex[ 7  ] = 1   ;
    iIndex[ 8  ] = 1   ; jIndex[ 8  ] = 2   ;
    iIndex[ 9  ] = 1   ; jIndex[ 9  ] = 3   ;
    iIndex[ 10 ] = 1   ; jIndex[ 10 ] = 4   ;
    iIndex[ 11 ] = 1   ; jIndex[ 11 ] = 5   ;
    iIndex[ 12 ] = 2   ; jIndex[ 12 ] = 0   ;
    iIndex[ 13 ] = 2   ; jIndex[ 13 ] = 1   ;
    iIndex[ 14 ] = 2   ; jIndex[ 14 ] = 2   ;
    iIndex[ 15 ] = 2   ; jIndex[ 15 ] = 3   ;
    iIndex[ 16 ] = 2   ; jIndex[ 16 ] = 4   ;
    iIndex[ 17 ] = 2   ; jIndex[ 17 ] = 5   ;
    iIndex[ 18 ] = 3   ; jIndex[ 18 ] = 0   ;
    iIndex[ 19 ] = 3   ; jIndex[ 19 ] = 1   ;
    iIndex[ 20 ] = 3   ; jIndex[ 20 ] = 2   ;
    iIndex[ 21 ] = 3   ; jIndex[ 21 ] = 3   ;
    iIndex[ 22 ] = 3   ; jIndex[ 22 ] = 4   ;
    iIndex[ 23 ] = 4   ; jIndex[ 23 ] = 0   ;
    iIndex[ 24 ] = 4   ; jIndex[ 24 ] = 1   ;
    iIndex[ 25 ] = 4   ; jIndex[ 25 ] = 2   ;
    iIndex[ 26 ] = 4   ; jIndex[ 26 ] = 3   ;
    iIndex[ 27 ] = 4   ; jIndex[ 27 ] = 4   ;
    iIndex[ 28 ] = 5   ; jIndex[ 28 ] = 0   ;
    iIndex[ 29 ] = 5   ; jIndex[ 29 ] = 1   ;
    iIndex[ 30 ] = 5   ; jIndex[ 30 ] = 2   ;
  }

  void
  kineDyn_OCP::DHxDx_sparse(
    NodeType2 const    & NODE__,
    V_const_pointer_type V__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = Q__[1];
    real_type t2   = X__[2];
    real_type t3   = 1.0 / t2;
    real_type t4   = t3 * t1;
    real_type t5   = X__[1];
    real_type t6   = cos(t5);
    real_type t7   = 1.0 / t6;
    real_type t8   = X__[0];
    real_type t9   = ModelPars[64];
    real_type t10  = Q__[0];
    real_type t11  = rightWidth(t10);
    real_type t12  = t8 - t9 + t11;
    real_type t13  = ALIAS_roadRightLateralBoundaries_D(t12);
    real_type t14  = t13 * t7;
    real_type t17  = t8 * t1;
    real_type t18  = t17 - 1;
    real_type t19  = t3 * t18;
    real_type t20  = ALIAS_roadRightLateralBoundaries_DD(t12);
    real_type t23  = leftWidth(t10);
    real_type t24  = t23 - t8 - t9;
    real_type t25  = ALIAS_roadLeftLateralBoundaries_D(t24);
    real_type t26  = t25 * t7;
    real_type t29  = ALIAS_roadLeftLateralBoundaries_DD(t24);
    real_type t32  = ModelPars[27];
    real_type t34  = t7 * t3;
    real_type t37  = t8 * t32;
    real_type t38  = t7 * t4;
    result__[ 0   ] = -2 * t34 * t18 * t32 - t20 * t7 * t19 - t29 * t7 * t19 - 2 * t14 * t4 + 2 * t26 * t4 - 4 * t38 * t37;
    real_type t41  = t6 * t6;
    real_type t42  = 1.0 / t41;
    real_type t43  = roadRightLateralBoundaries(t12);
    real_type t45  = sin(t5);
    real_type t46  = t45 * t43 * t42;
    real_type t51  = roadLeftLateralBoundaries(t24);
    real_type t53  = t45 * t51 * t42;
    real_type t58  = X__[3];
    real_type t60  = t58 - ModelPars[7];
    real_type t61  = SignReg(t60);
    real_type t63  = t61 / 2 + 1.0 / 2.0;
    real_type t64  = X__[4];
    real_type t65  = t64 * t2;
    real_type t66  = abs_reg(t65);
    real_type t67  = aymax(t2);
    real_type t68  = 1.0 / t67;
    real_type t69  = t68 * t66;
    real_type t70  = ModelPars[21];
    real_type t71  = pow(t69, t70);
    real_type t72  = abs_reg(t60);
    real_type t73  = axmax(t2);
    real_type t74  = 1.0 / t73;
    real_type t76  = pow(t74 * t72, t70);
    real_type t77  = t71 + t76;
    real_type t80  = t61 / 2 - 1.0 / 2.0;
    real_type t81  = ModelPars[20];
    real_type t82  = pow(t69, t81);
    real_type t83  = axmin(t2);
    real_type t84  = 1.0 / t83;
    real_type t86  = pow(t84 * t72, t81);
    real_type t87  = t82 + t86;
    real_type t89  = -t77 * t63 + t87 * t80 + 1;
    real_type t90  = GGdiagramEnvelope(t89);
    real_type t92  = t45 * t90 * t42;
    real_type t96  = t45 * t42 * t3;
    real_type t100 = ModelPars[29];
    real_type t102 = t2 - ModelPars[24];
    real_type t103 = t102 * t102;
    real_type t105 = t8 * t8;
    real_type t107 = t103 * t100 + t105 * t32 + ModelPars[28];
    real_type t108 = t1 * t107;
    real_type t110 = L__[0];
    real_type t112 = t45 * t45;
    real_type t116 = L__[1];
    real_type t117 = t1 * t116;
    real_type t120 = L__[2];
    real_type t121 = ModelPars[16];
    real_type t122 = t2 * t2;
    real_type t124 = ModelPars[17];
    real_type t128 = 1.0 / t124;
    real_type t129 = t128 * (-t122 * t121 + t124 * t58) * t120;
    real_type t130 = t45 * t42;
    real_type t131 = t130 * t4;
    real_type t133 = L__[3];
    real_type t138 = 1.0 / ModelPars[62];
    real_type t139 = t138 * (U__[0] - t58) * t133;
    real_type t142 = ModelPars[0];
    real_type t144 = 1.0 / t142 * L__[4];
    real_type t145 = ModelPars[22];
    real_type t146 = 1.0 / t145;
    real_type t148 = 1.0 / ModelPars[61];
    real_type t150 = t148 * t146 * t144;
    real_type t153 = k__US(t64, t2);
    real_type t156 = X__[5];
    real_type t158 = t145 * t142 * t64 + t145 * t2 * t153 - t2 * t156;
    real_type t159 = t1 * t158;
    real_type t162 = L__[5];
    real_type t167 = 1.0 / ModelPars[63];
    real_type t168 = t167 * (U__[1] - t156) * t162;
    result__[ 1   ] = -t42 * t1 * t112 * t110 - t45 * t13 * t42 * t19 + t45 * t25 * t42 * t19 - t96 * t64 * t117 + t96 * t159 * t150 - 2 * t96 * t18 * t37 - t1 * t110 - t96 * t108 - t131 * t129 - t131 * t139 - t131 * t168 - t46 * t4 - t53 * t4 - t92 * t4;
    real_type t170 = 1.0 / t122;
    real_type t171 = t170 * t1;
    real_type t172 = t43 * t7;
    real_type t174 = t170 * t18;
    real_type t176 = t51 * t7;
    real_type t179 = t90 * t7;
    real_type t181 = ALIAS_GGdiagramEnvelope_D(t89);
    real_type t182 = t181 * t7;
    real_type t183 = t70 * t71;
    real_type t184 = ALIAS_abs_reg_D(t65);
    real_type t185 = t64 * t184;
    real_type t187 = t67 * t67;
    real_type t188 = 1.0 / t187;
    real_type t189 = t188 * t66;
    real_type t190 = aymax_D(t2);
    real_type t192 = t68 * t185 - t190 * t189;
    real_type t193 = 1.0 / t66;
    real_type t194 = t193 * t192;
    real_type t195 = t67 * t194;
    real_type t197 = t70 * t76;
    real_type t198 = axmax_D(t2);
    real_type t199 = t198 * t74;
    real_type t201 = t195 * t183 - t199 * t197;
    real_type t203 = t81 * t82;
    real_type t205 = t81 * t86;
    real_type t206 = axmin_D(t2);
    real_type t207 = t206 * t84;
    real_type t209 = t195 * t203 - t207 * t205;
    real_type t211 = -t201 * t63 + t209 * t80;
    real_type t212 = t211 * t182;
    real_type t214 = t7 * t174;
    real_type t217 = t102 * t100;
    real_type t220 = t7 * t170;
    real_type t225 = t121 * t120;
    real_type t230 = t7 * t171;
    real_type t233 = k__US_D_2(t64, t2);
    real_type t237 = t145 * t2 * t233 + t145 * t153 - t156;
    result__[ 2   ] = 2 * t7 * t1 * t128 * t225 + t34 * t1 * t237 * t150 + t7 * t170 * t64 * t117 - t220 * t159 * t150 + t220 * t108 + t230 * t129 + t230 * t139 + t14 * t174 + t230 * t168 + t172 * t171 + t176 * t171 + t179 * t171 - t26 * t174 - t212 * t4 + 2 * t214 * t37 - 2 * t38 * t217;
    real_type t244 = ALIAS_SignReg_D(t60);
    real_type t247 = t76 * t63;
    real_type t248 = ALIAS_abs_reg_D(t60);
    real_type t250 = 1.0 / t72;
    real_type t251 = t250 * t248 * t70;
    real_type t255 = t86 * t80;
    real_type t257 = t250 * t248 * t81;
    real_type t259 = -t77 * t244 / 2 - t251 * t247 + t87 * t244 / 2 + t257 * t255;
    real_type t260 = t259 * t182;
    real_type t264 = t138 * t133;
    result__[ 3   ] = -t34 * t1 * t120 - t260 * t4 + t38 * t264;
    real_type t266 = t71 * t63;
    real_type t267 = t70 * t266;
    real_type t268 = t2 * t184;
    real_type t269 = t193 * t268;
    real_type t271 = t82 * t80;
    real_type t272 = t81 * t271;
    real_type t274 = -t269 * t267 + t269 * t272;
    real_type t275 = t274 * t182;
    real_type t279 = k__US_D_1(t64, t2);
    real_type t282 = t145 * t2 * t279 + t145 * t142;
    result__[ 4   ] = t34 * t1 * t282 * t150 - t34 * t117 - t275 * t4;
    real_type t286 = t146 * t144;
    real_type t290 = t167 * t162;
    result__[ 5   ] = -t7 * t1 * t148 * t286 + t38 * t290;
    result__[ 6   ] = result__[1];
    real_type t293 = 1.0 / t41 / t6;
    real_type t309 = t18 * t107;
    real_type t311 = t112 * t293 * t3;
    real_type t324 = -2 * t293 * t18 * t112 * t45 * t110 - 2 * t7 * t18 * t45 * t110 - 2 * t112 * t43 * t293 * t19 - 2 * t112 * t51 * t293 * t19 - 2 * t112 * t90 * t293 * t19 - t172 * t19 - t176 * t19 - t179 * t19 - 2 * t311 * t309 - t34 * t309 + t117;
    real_type t332 = (t6 * t2 * t1 + t64 * t17 - t64) * t116;
    real_type t337 = t112 * t293 * t19;
    real_type t340 = t7 * t19;
    real_type t345 = t18 * t158;
    real_type t354 = 2 * t42 * t112 * t117 + 2 * t311 * t345 * t150 + t34 * t345 * t150 - 2 * t337 * t129 - t340 * t129 - 2 * t337 * t139 - t340 * t139 - 2 * t337 * t168 - t340 * t168 - 2 * t311 * t332 - t34 * t332;
    result__[ 7   ] = t324 + t354;
    real_type t358 = t42 * t19;
    real_type t366 = t45 * t42 * t170;
    real_type t376 = t130 * t174;
    real_type t379 = t18 * t237;
    result__[ 8   ] = 2 * t45 * t42 * t18 * t128 * t225 - t45 * t211 * t181 * t358 - t45 * t34 * t117 - t366 * t345 * t150 + t96 * t379 * t150 - 2 * t96 * t18 * t217 + t376 * t129 + t376 * t139 + t376 * t168 + t46 * t174 + t53 * t174 + t92 * t174 + t366 * t309 + t366 * t332;
    real_type t388 = t18 * t120;
    result__[ 9   ] = -t45 * t259 * t181 * t358 + t96 * t18 * t264 - t96 * t388;
    real_type t395 = t18 * t116;
    real_type t397 = t18 * t282;
    result__[ 10  ] = -t45 * t274 * t181 * t358 + t96 * t397 * t150 - t96 * t395;
    result__[ 11  ] = -t130 * t18 * t148 * t286 + t96 * t18 * t290;
    result__[ 12  ] = result__[2];
    result__[ 13  ] = result__[8];
    real_type t406 = 1.0 / t122 / t2;
    real_type t407 = t406 * t18;
    real_type t416 = ALIAS_GGdiagramEnvelope_DD(t89);
    real_type t417 = t416 * t7;
    real_type t418 = t211 * t211;
    real_type t421 = t70 * t70;
    real_type t422 = t421 * t71;
    real_type t423 = t192 * t192;
    real_type t424 = t66 * t66;
    real_type t425 = 1.0 / t424;
    real_type t427 = t187 * t425 * t423;
    real_type t429 = ALIAS_abs_reg_DD(t65);
    real_type t430 = t64 * t64;
    real_type t433 = t190 * t188;
    real_type t439 = t190 * t190;
    real_type t442 = aymax_DD(t2);
    real_type t446 = t67 * t193 * (t68 * t430 * t429 - 2 * t433 * t185 + 2 * t439 / t187 / t67 * t66 - t442 * t189);
    real_type t448 = t192 * t183;
    real_type t449 = t67 * t425;
    real_type t450 = t185 * t449;
    real_type t452 = t190 * t194;
    real_type t455 = t73 * t73;
    real_type t457 = t198 * t198;
    real_type t458 = t457 / t455;
    real_type t461 = axmax_DD(t2);
    real_type t466 = t81 * t81;
    real_type t467 = t466 * t82;
    real_type t470 = t192 * t203;
    real_type t474 = t83 * t83;
    real_type t476 = t206 * t206;
    real_type t477 = t476 / t474;
    real_type t480 = axmin_DD(t2);
    real_type t493 = t7 * t406;
    real_type t505 = t7 * t407;
    real_type t510 = k__US_D_2_2(t64, t2);
    result__[ 14  ] = -2 * t172 * t407 - 2 * t176 * t407 - 2 * t179 * t407 + 2 * t212 * t174 - t418 * t417 * t19 - (-(-t461 * t74 * t197 + t458 * t421 * t76 + t446 * t183 + t452 * t183 + t458 * t197 + t427 * t422 - t450 * t448) * t63 + (-t480 * t84 * t205 + t477 * t466 * t86 + t446 * t203 + t452 * t203 + t477 * t205 + t427 * t467 - t450 * t470) * t80) * t182 * t19 - 2 * t34 * t18 * t100 + 4 * t214 * t217 - 2 * t493 * t309 + 2 * t170 * t117 - 2 * t493 * t332 - 2 * t7 * t18 * t128 * t3 * t225 - 2 * t505 * t129 - 2 * t505 * t139 + t34 * t18 * (t145 * t2 * t510 + 2 * t145 * t233) * t150 - 2 * t220 * t379 * t150 + 2 * t493 * t345 * t150 - 2 * t505 * t168;
    real_type t534 = t250 * t248;
    result__[ 15  ] = t260 * t174 - t211 * t259 * t416 * t340 - (-t201 * t244 / 2 + t199 * t534 * t421 * t247 + t209 * t244 / 2 - t207 * t534 * t466 * t255) * t182 * t19 + t220 * t388 - t214 * t264;
    real_type t547 = t275 * t174;
    real_type t548 = t274 * t416;
    real_type t550 = t211 * t548 * t340;
    real_type t554 = t67 * t192 * t425 * t2;
    real_type t563 = t67 * t193 * (t68 * t64 * t2 * t429 + t68 * t184 - t433 * t268);
    real_type t565 = t268 * t449;
    real_type t578 = t220 * t395;
    real_type t579 = k__US_D_1_2(t64, t2);
    real_type t586 = t34 * t18 * (t145 * t2 * t579 + t145 * t279) * t150;
    real_type t588 = t220 * t397 * t150;
    result__[ 16  ] = t547 - t550 - (-(t554 * t184 * t422 + t563 * t183 - t565 * t448) * t63 + (t554 * t184 * t467 + t563 * t203 - t565 * t470) * t80) * t182 * t19 + t578 + t586 - t588;
    result__[ 17  ] = -t214 * t290;
    result__[ 18  ] = result__[3];
    result__[ 19  ] = result__[9];
    result__[ 20  ] = result__[15];
    real_type t590 = t259 * t259;
    real_type t593 = ALIAS_SignReg_DD(t60);
    real_type t598 = t248 * t248;
    real_type t600 = t72 * t72;
    real_type t601 = 1.0 / t600;
    real_type t604 = ALIAS_abs_reg_DD(t60);
    result__[ 21  ] = -t590 * t417 * t19 - (-t77 * t593 / 2 - t251 * t76 * t244 - t601 * t598 * t421 * t247 - t250 * t604 * t70 * t247 + t601 * t598 * t70 * t247 + t87 * t593 / 2 + t257 * t86 * t244 + t601 * t598 * t466 * t255 + t250 * t604 * t81 * t255 - t601 * t598 * t81 * t255) * t182 * t19;
    result__[ 22  ] = -t259 * t548 * t340 - (-t269 * t70 * t71 * t244 / 2 + t269 * t81 * t82 * t244 / 2) * t182 * t19;
    result__[ 23  ] = result__[4];
    result__[ 24  ] = result__[10];
    real_type t644 = t193 * t2 * t64 * t429;
    real_type t649 = t184 * t184;
    real_type t652 = t64 * t425 * t2 * t649;
    result__[ 25  ] = t547 - t550 - (-t193 * t184 * t70 * t266 + t193 * t184 * t81 * t271 - t565 * t192 * t421 * t266 + t565 * t192 * t466 * t271 - t644 * t267 + t652 * t267 + t644 * t272 - t652 * t272) * t182 * t19 + t578 + t586 - t588;
    result__[ 26  ] = result__[22];
    real_type t665 = t274 * t274;
    real_type t670 = t425 * t122 * t649;
    real_type t673 = t193 * t122 * t429;
    real_type t684 = k__US_D_1_1(t64, t2);
    result__[ 27  ] = -t665 * t417 * t19 - (-t670 * t421 * t266 + t670 * t466 * t271 + t670 * t267 - t673 * t267 - t670 * t272 + t673 * t272) * t182 * t19 + t7 * t18 * t684 * t148 * t144;
    result__[ 28  ] = result__[5];
    result__[ 29  ] = result__[11];
    result__[ 30  ] = result__[17];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DHxDx_sparse",31);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DHxDp_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DHxDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DHxDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DHxDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DHxDp_sparse(
    NodeType2 const    & NODE__,
    V_const_pointer_type V__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |  _   _
   | | | | |_   _
   | | |_| | | | |
   | |  _  | |_| |
   | |_| |_|\__,_|
   |
  \*/

  integer
  kineDyn_OCP::Hu_numEqns() const
  { return 2; }

  void
  kineDyn_OCP::Hu_eval(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t13  = cos(X__[1]);
    real_type t15  = 1.0 / t13 / X__[2] * (Q__[1] * X__[0] - 1);
    result__[ 0   ] = -t15 * L__[3] / ModelPars[62];
    result__[ 1   ] = -t15 * L__[5] / ModelPars[63];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hu_eval",2);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DHuDx_numRows() const
  { return 2; }

  integer
  kineDyn_OCP::DHuDx_numCols() const
  { return 6; }

  integer
  kineDyn_OCP::DHuDx_nnz() const
  { return 6; }

  void
  kineDyn_OCP::DHuDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[ 0  ] = 0   ; jIndex[ 0  ] = 0   ;
    iIndex[ 1  ] = 0   ; jIndex[ 1  ] = 1   ;
    iIndex[ 2  ] = 0   ; jIndex[ 2  ] = 2   ;
    iIndex[ 3  ] = 1   ; jIndex[ 3  ] = 0   ;
    iIndex[ 4  ] = 1   ; jIndex[ 4  ] = 1   ;
    iIndex[ 5  ] = 1   ; jIndex[ 5  ] = 2   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DHuDx_sparse(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t4   = L__[3] / ModelPars[62];
    real_type t5   = Q__[1];
    real_type t6   = X__[2];
    real_type t7   = 1.0 / t6;
    real_type t9   = X__[1];
    real_type t10  = cos(t9);
    real_type t11  = 1.0 / t10;
    real_type t12  = t11 * t7 * t5;
    result__[ 0   ] = -t12 * t4;
    real_type t16  = X__[0] * t5 - 1;
    real_type t18  = t10 * t10;
    real_type t21  = sin(t9);
    real_type t22  = t21 / t18 * t7;
    result__[ 1   ] = -t22 * t16 * t4;
    real_type t24  = t6 * t6;
    real_type t27  = t11 / t24 * t16;
    result__[ 2   ] = t27 * t4;
    real_type t31  = L__[5] / ModelPars[63];
    result__[ 3   ] = -t12 * t31;
    result__[ 4   ] = -t22 * t16 * t31;
    result__[ 5   ] = t27 * t31;
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"DHuDx_sparse",6);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DHuDp_numRows() const
  { return 2; }

  integer
  kineDyn_OCP::DHuDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DHuDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DHuDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DHuDp_sparse(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |  _   _
   | | | | |_ __
   | | |_| | '_ \
   | |  _  | |_) |
   | |_| |_| .__/
   |       |_|
  \*/

  integer
  kineDyn_OCP::Hp_numEqns() const
  { return 0; }

  void
  kineDyn_OCP::Hp_eval(
    NodeType2 const    & NODE__,
    V_const_pointer_type V__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);

    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"Hp_eval",0);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DHpDp_numRows() const
  { return 0; }

  integer
  kineDyn_OCP::DHpDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DHpDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DHpDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DHpDp_sparse(
    NodeType2 const    & NODE__,
    V_const_pointer_type V__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |        _
   |    ___| |_ __ _
   |   / _ \ __/ _` |
   |  |  __/ || (_| |
   |   \___|\__\__,_|
  \*/

  integer
  kineDyn_OCP::eta_numEqns() const
  { return 6; }

  void
  kineDyn_OCP::eta_eval(
    NodeType2 const    & NODE__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = L__[0];
    result__[ 1   ] = L__[1];
    result__[ 2   ] = L__[2];
    result__[ 3   ] = L__[3];
    result__[ 4   ] = L__[4];
    result__[ 5   ] = L__[5];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"eta_eval",6);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DetaDx_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DetaDx_numCols() const
  { return 6; }

  integer
  kineDyn_OCP::DetaDx_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DetaDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DetaDx_sparse(
    NodeType2 const    & NODE__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DetaDp_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DetaDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DetaDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DetaDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DetaDp_sparse(
    NodeType2 const    & NODE__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |    _ __  _   _
   |   | '_ \| | | |
   |   | | | | |_| |
   |   |_| |_|\__,_|
  \*/

  integer
  kineDyn_OCP::nu_numEqns() const
  { return 6; }

  void
  kineDyn_OCP::nu_eval(
    NodeType const     & NODE__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = V__[0];
    result__[ 1   ] = V__[1];
    result__[ 2   ] = V__[2];
    result__[ 3   ] = V__[3];
    result__[ 4   ] = V__[4];
    result__[ 5   ] = V__[5];
    #ifdef MECHATRONIX_DEBUG
    CHECK_NAN(result__,"nu_eval",6);
    #endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DnuDx_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DnuDx_numCols() const
  { return 6; }

  integer
  kineDyn_OCP::DnuDx_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DnuDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DnuDx_sparse(
    NodeType const     & NODE__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DnuDp_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DnuDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DnuDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DnuDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DnuDp_sparse(
    NodeType const     & NODE__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

}

// EOF: kineDyn_OCP_Methods.cc
