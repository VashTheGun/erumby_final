/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP.hh                                                 |
 |                                                                       |
 |  version: 1.0   date 27/2/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#ifndef KINEDYN_OCP_HH
#define KINEDYN_OCP_HH

// Mechatronix Headers
#include <MechatronixCore/MechatronixCore.hh>
#include <MechatronixSolver/MechatronixSolver.hh>
#include <MechatronixInterfaceMruby/MechatronixInterfaceMruby.hh>

// user headers



#ifdef MECHATRONIX_OS_WINDOWS
  #ifndef KINEDYN_OCP_API_DLL
    #ifdef KINEDYN_OCP_EXPORT
      #define KINEDYN_OCP_API_DLL __declspec(dllexport)
    #elif defined(KINEDYN_OCP_IMPORT)
      #define KINEDYN_OCP_API_DLL __declspec(dllimport)
    #else
      #define KINEDYN_OCP_API_DLL
    #endif
  #endif
  #ifndef _SCL_SECURE_NO_WARNINGS
    #define _SCL_SECURE_NO_WARNINGS 1
  #endif
#else
  #define KINEDYN_OCP_API_DLL
#endif

#define OCP_VIRTUAL          MECHATRONIX_OVERRIDE
#define INDIRECT_OCP_VIRTUAL MECHATRONIX_OVERRIDE

namespace kineDyn_OCPDefine {

  using namespace MechatronixLoad;

  using namespace std;
  using Mechatronix::real_type;
  using Mechatronix::integer;
  using Mechatronix::ostream_type;

  // user class in namespaces
  using Mechatronix::Path2D;


  extern char const *namesBc[];
  extern char const *namesXvars[];
  extern char const *namesLvars[];
  extern char const *namesUvars[];
  extern char const *namesQvars[];
  extern char const *namesPvars[];
  extern char const *namesOMEGAvars[];

  extern char const *namesModelPars[];

  extern char const *namesPostProcess[];
  extern char const *namesIntegratedPostProcess[];
  extern char const *namesConstraint1D[];
  extern char const *namesConstraint2D[];
  extern char const *namesConstraintU[];

  using Mechatronix::X_pointer_type;
  using Mechatronix::L_pointer_type;
  using Mechatronix::Z_pointer_type;
  using Mechatronix::U_pointer_type;
  using Mechatronix::V_pointer_type;
  using Mechatronix::Q_pointer_type;
  using Mechatronix::P_pointer_type;
  using Mechatronix::OMEGA_pointer_type;
  using Mechatronix::OMEGA_full_pointer_type;

  using Mechatronix::X_const_pointer_type;
  using Mechatronix::L_const_pointer_type;
  using Mechatronix::Z_const_pointer_type;
  using Mechatronix::U_const_pointer_type;
  using Mechatronix::V_const_pointer_type;
  using Mechatronix::Q_const_pointer_type;
  using Mechatronix::P_const_pointer_type;
  using Mechatronix::OMEGA_const_pointer_type;
  using Mechatronix::OMEGA_full_const_pointer_type;

  using Mechatronix::MatrixWrapper;

  using GenericContainerNamespace::GenericContainer;

  class kineDyn_OCP : public Mechatronix::Discretized_Indirect_OCP {

    // redirect output to a string in GenericContainer - - - - - - - - - - - - -
    stringstream ss_redirected_stream;

    // Model Paramaters  - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    real_type ModelPars[65];

    // Controls  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Mechatronix::PenaltyBarrierU delta__D0Control;
    Mechatronix::PenaltyBarrierU a__x0Control;

    // Constraints 1D  - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Mechatronix::PenaltyBarrier1DGreaterThan roadRightLateralBoundaries;
    Mechatronix::PenaltyBarrier1DGreaterThan roadLeftLateralBoundaries;
    Mechatronix::PenaltyBarrier1DGreaterThan GGdiagramEnvelope;

    // Constraints 2D  - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // User mapped functions - - - - - - - - - - - - - - - - - - - - - - - - - -
    Mechatronix::PositivePartRegularizedWithSinAtan posPart;
    Mechatronix::NegativePartRegularizedWithSinAtan negPart;
    Mechatronix::SignRegularizedWithErf SignReg;
    Mechatronix::SignRegularizedWithErf SignReg_smooth;
    Mechatronix::AbsoluteValueRegularizedWithSinAtan abs_reg;

    // User classes (internal) - - - - - - - - - - - - - - - - - - - - - - - - -

    // User classes (external) - - - - - - - - - - - - - - - - - - - - - - - - -
    Mechatronix::Path2D * pTrajectory;

    // block copy constructor  - - - - - - - - - - - - - - - - - - - - - - - - -
    kineDyn_OCP( kineDyn_OCP const & );
    kineDyn_OCP const & operator = ( kineDyn_OCP const & );

    // subclass for continuation - - - - - - - - - - - - - - - - - - - - - - - -

  public:

    using Mechatronix::Discretized_Indirect_OCP::setup;
    using Mechatronix::Discretized_Indirect_OCP::guess;

    using Mechatronix::Discretized_Indirect_OCP::numOMEGA;

    using Mechatronix::Discretized_Indirect_OCP::bcInvMap;
    using Mechatronix::Discretized_Indirect_OCP::bcMap;
    using Mechatronix::Discretized_Indirect_OCP::numBC;

    using Mechatronix::Discretized_Indirect_OCP::dim_Q;
    using Mechatronix::Discretized_Indirect_OCP::dim_X;
    using Mechatronix::Discretized_Indirect_OCP::dim_U;
    using Mechatronix::Discretized_Indirect_OCP::dim_Pars;
    using Mechatronix::Discretized_Indirect_OCP::dim_Omega;
    using Mechatronix::Discretized_Indirect_OCP::dim_BC;
    using Mechatronix::Discretized_Indirect_OCP::nNodes;

    using Mechatronix::Discretized_Indirect_OCP::numEquations;
    using Mechatronix::Discretized_Indirect_OCP::eval_F;
    using Mechatronix::Discretized_Indirect_OCP::eval_JF_nnz;
    using Mechatronix::Discretized_Indirect_OCP::eval_JF_pattern;
    using Mechatronix::Discretized_Indirect_OCP::eval_JF_values;
    using Mechatronix::Discretized_Indirect_OCP::eval_JF;

    using Mechatronix::Discretized_Indirect_OCP::get_solution;
    using Mechatronix::Discretized_Indirect_OCP::get_solution_as_spline;
    using Mechatronix::Discretized_Indirect_OCP::get_solution_as_guess;

    using Mechatronix::Indirect_OCP::setupBC;

    KINEDYN_OCP_API_DLL
    explicit
    kineDyn_OCP(
      string const & name,
      ThreadPool   * _TP,
      Console      * _pConsole
    );

    KINEDYN_OCP_API_DLL virtual
    ~kineDyn_OCP() MECHATRONIX_OVERRIDE;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    KINEDYN_OCP_API_DLL virtual
    char const * model_name() const MECHATRONIX_OVERRIDE
    { return "kineDyn_OCP"; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    KINEDYN_OCP_API_DLL
    void
    infoClasses() const;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // C++ initializer (raccolti in setup( gc ))
    KINEDYN_OCP_API_DLL
    void
    setupParameters( GenericContainer const & gc );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    KINEDYN_OCP_API_DLL
    void
    setupParameters( real_type const Pars[] );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    KINEDYN_OCP_API_DLL
    void
    updateParameter( real_type val, integer idx )
    { ModelPars[idx] = val; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    KINEDYN_OCP_API_DLL
    void
    setupClasses( GenericContainer const & gc );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    KINEDYN_OCP_API_DLL
    void
    setupUserClasses( GenericContainer const & gc );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    KINEDYN_OCP_API_DLL
    void
    setupUserMappedFunctions( GenericContainer const & gc );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    KINEDYN_OCP_API_DLL
    void
    setupControls( GenericContainer const & gc );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    KINEDYN_OCP_API_DLL
    void
    setupPointers( GenericContainer const & gc );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // user functions prototype (with derivative)
    KINEDYN_OCP_API_DLL real_type leftWidth          ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type leftWidth_D        ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type leftWidth_DD       ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type rightWidth         ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type rightWidth_D       ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type rightWidth_DD      ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type k__US              ( real_type Omega__XO, real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type k__US_D_1          ( real_type Omega__XO, real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type k__US_D_1_1        ( real_type Omega__XO, real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type k__US_D_1_2        ( real_type Omega__XO, real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type k__US_D_2          ( real_type Omega__XO, real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type k__US_D_2_2        ( real_type Omega__XO, real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type axmax              ( real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type axmax_D            ( real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type axmax_DD           ( real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type axmin              ( real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type axmin_D            ( real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type axmin_DD           ( real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type aymax              ( real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type aymax_D            ( real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type aymax_DD           ( real_type v__x__XO ) const;
    KINEDYN_OCP_API_DLL real_type negSign            ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type negSign_D          ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type negSign_DD         ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type posSign            ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type posSign_D          ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type posSign_DD         ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type negSign_sharp      ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type negSign_sharp_D    ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type negSign_sharp_DD   ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type posSign_sharp      ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type posSign_sharp_D    ( real_type zeta__XO ) const;
    KINEDYN_OCP_API_DLL real_type posSign_sharp_DD   ( real_type zeta__XO ) const;

    #include <MechatronixSolver/OCP_methods.hxx>
    #include <MechatronixSolver/Indirect_OCP_methods.hxx>

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // C++ initializer (all in one)
    KINEDYN_OCP_API_DLL
    void
    setup( GenericContainer const & gc );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    void
    get_names( GenericContainer & out ) const;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // do some check on the computed solution
    KINEDYN_OCP_API_DLL
    void
    diagnostic( GenericContainer const & gc_solution );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Continuation phase update
    KINEDYN_OCP_API_DLL
    void
    updateContinuation( integer phase, real_type s ) MECHATRONIX_OVERRIDE;

    // save model parameters
    KINEDYN_OCP_API_DLL virtual
    void
    save_OCP_info( GenericContainer & gc ) const MECHATRONIX_OVERRIDE;

  };
}

namespace kineDyn_OCPLoad {
  using kineDyn_OCPDefine::kineDyn_OCP;

}

#endif

// EOF: kineDyn_OCP.hh
