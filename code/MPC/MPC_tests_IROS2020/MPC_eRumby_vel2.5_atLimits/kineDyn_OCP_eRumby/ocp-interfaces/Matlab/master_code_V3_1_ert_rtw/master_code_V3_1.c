/*
 * master_code_V3_1.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "master_code_V3_1".
 *
 * Model version              : 1.313
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Mon Mar  2 16:28:09 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "master_code_V3_1.h"
#include "master_code_V3_1_private.h"

/* Named constants for Chart: '<Root>/ECU_STATE_MACHINE' */
#define master_cod_IN_Sensing_Receiving ((uint8_T)2U)
#define master_code_V3_1_IN_Running    ((uint8_T)1U)
#define master_code_V3_1_IN_idle_state ((uint8_T)2U)
#define master_code_V3_IN_Interpolation ((uint8_T)1U)
#define master_code_V_IN_initialization ((uint8_T)3U)
#define master_code__IN_NO_ACTIVE_CHILD ((uint8_T)0U)

/* Block signals (default storage) */
B_master_code_V3_1_T master_code_V3_1_B;

/* Block states (default storage) */
DW_master_code_V3_1_T master_code_V3_1_DW;

/* Previous zero-crossings (trigger) states */
PrevZCX_master_code_V3_1_T master_code_V3_1_PrevZCX;

/* Real-time model */
RT_MODEL_master_code_V3_1_T master_code_V3_1_M_;
RT_MODEL_master_code_V3_1_T *const master_code_V3_1_M = &master_code_V3_1_M_;

/* Forward declaration for local functions */
static void master_code__SystemCore_release(const
  codertarget_raspi_internal_I2_T *obj);
static void master_code_V_SystemCore_delete(const
  codertarget_raspi_internal_I2_T *obj);
static void matlabCodegenHandle_matlabCodeg(codertarget_raspi_internal_I2_T *obj);

/* Forward declaration for local functions */
static void m_SystemCore_release_p01msq5wle(const
  codertarget_raspi__nparbhma5g_T *obj);
static void ma_SystemCore_delete_jwpcm5lajz(const
  codertarget_raspi__nparbhma5g_T *obj);
static void matlabCodegenHandle__eekrmxfgnd(codertarget_raspi__nparbhma5g_T *obj);

/* Forward declaration for local functions */
static boolean_T master_code_V3_findpivotIdx_u16(int32_T i, int32_T j, int32_T
  startIdx, int32_T pivot[], uint32_T midxPort[], const uint16_T mdataPort[]);
static int32_T master_code_V3_partitionIdx_u16(int32_T i, int32_T j, int32_T
  startIdx, const int32_T pivot[], uint32_T midxPort[], const uint16_T
  mdataPort[]);
static void master_code_V3_1_MDNQSort_u16(int32_T i, int32_T j, int32_T startIdx,
  uint32_T midxPort[], const uint16_T mdataPort[]);
static void m_SystemCore_release_eh23cloj25(const
  codertarget_raspi__nparbhma5g_T *obj);
static void ma_SystemCore_delete_eh23cloj25(const
  codertarget_raspi__nparbhma5g_T *obj);
static void matlabCodegenHandle__eh23cloj25(codertarget_raspi__nparbhma5g_T *obj);
static void m_SystemCore_rele_kadk1bn3pp(const codertarget_raspi_internal_I2_T
  *obj);
static void m_SystemCore_dele_kadk1bn3pp(const codertarget_raspi_internal_I2_T
  *obj);
static void mast_matlabCodegenHa_kadk1bn3pp(codertarget_raspi_internal_I2_T *obj);

/*
 * Writes out MAT-file header.  Returns success or failure.
 * Returns:
 *      0 - success
 *      1 - failure
 */
int_T rt_WriteMat4FileHeader(FILE *fp, int32_T m, int32_T n, const char *name)
{
  typedef enum { ELITTLE_ENDIAN, EBIG_ENDIAN } ByteOrder;

  int16_T one = 1;
  ByteOrder byteOrder = (*((int8_T *)&one)==1) ? ELITTLE_ENDIAN : EBIG_ENDIAN;
  int32_T type = (byteOrder == ELITTLE_ENDIAN) ? 0: 1000;
  int32_T imagf = 0;
  int32_T name_len = (int32_T)strlen(name) + 1;
  if ((fwrite(&type, sizeof(int32_T), 1, fp) == 0) ||
      (fwrite(&m, sizeof(int32_T), 1, fp) == 0) ||
      (fwrite(&n, sizeof(int32_T), 1, fp) == 0) ||
      (fwrite(&imagf, sizeof(int32_T), 1, fp) == 0) ||
      (fwrite(&name_len, sizeof(int32_T), 1, fp) == 0) ||
      (fwrite(name, sizeof(char), name_len, fp) == 0)) {
    return(1);
  } else {
    return(0);
  }
}                                      /* end rt_WriteMat4FileHeader */

/*
 * Set which subrates need to run this base step (base rate always runs).
 * This function must be called prior to calling the model step function
 * in order to "remember" which rates need to run this base step.  The
 * buffering of events allows for overlapping preemption.
 */
void master_code_V3_1_SetEventsForThisBaseStep(boolean_T *eventFlags)
{
  /* Task runs when its counter is zero, computed via rtmStepTask macro */
  eventFlags[1] = ((boolean_T)rtmStepTask(master_code_V3_1_M, 1));
}

/*
 *   This function updates active task flag for each subrate.
 * The function is called in the model base rate function.
 * It maintains SampleHit information to allow scheduling
 * of the subrates from the base rate function.
 */
void rate_scheduler(void)
{
  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (master_code_V3_1_M->Timing.TaskCounters.TID[1])++;
  if ((master_code_V3_1_M->Timing.TaskCounters.TID[1]) > 9) {/* Sample time: [0.01s, 0.0s] */
    master_code_V3_1_M->Timing.TaskCounters.TID[1] = 0;
  }
}

static void master_code__SystemCore_release(const
  codertarget_raspi_internal_I2_T *obj)
{
  if ((obj->isInitialized == 1) && obj->isSetupComplete) {
    MW_I2C_Close(obj->MW_I2C_HANDLE);
  }
}

static void master_code_V_SystemCore_delete(const
  codertarget_raspi_internal_I2_T *obj)
{
  master_code__SystemCore_release(obj);
}

static void matlabCodegenHandle_matlabCodeg(codertarget_raspi_internal_I2_T *obj)
{
  if (!obj->matlabCodegenIsDeleted) {
    obj->matlabCodegenIsDeleted = true;
    master_code_V_SystemCore_delete(obj);
  }
}

/* Start for function-call system: '<S1>/i2cWr_mpu' */
void master_code_i2cWr_mpu_Start(DW_i2cWr_mpu_master_code_V3_1_T *localDW)
{
  codertarget_raspi_internal_I2_T *obj;
  uint32_T i2cname;

  /* Start for MATLABSystem: '<S6>/I2C Master Write' */
  localDW->obj.matlabCodegenIsDeleted = true;
  localDW->obj.isInitialized = 0;
  localDW->obj.matlabCodegenIsDeleted = false;
  obj = &localDW->obj;
  localDW->obj.isSetupComplete = false;
  localDW->obj.isInitialized = 1;
  i2cname = 1;
  obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
  localDW->obj.BusSpeed = 100000U;
  MW_I2C_SetBusSpeed(localDW->obj.MW_I2C_HANDLE, localDW->obj.BusSpeed);
  localDW->obj.isSetupComplete = true;
}

/* Output and update for function-call system: '<S1>/i2cWr_mpu' */
void master_code_V3_1_i2cWr_mpu(uint16_T rtu_dataw,
  DW_i2cWr_mpu_master_code_V3_1_T *localDW)
{
  uint8_T SwappedDataBytes[2];
  uint16_T b_x;
  uint8_T b_x_0[2];

  /* MATLABSystem: '<S6>/I2C Master Write' */
  memcpy((void *)&SwappedDataBytes[0], (void *)&rtu_dataw, (uint32_T)((size_t)2 *
          sizeof(uint8_T)));
  b_x_0[0] = SwappedDataBytes[1];
  b_x_0[1] = SwappedDataBytes[0];
  memcpy((void *)&b_x, (void *)&b_x_0[0], (uint32_T)((size_t)1 * sizeof(uint16_T)));
  memcpy((void *)&SwappedDataBytes[0], (void *)&b_x, (uint32_T)((size_t)2 *
          sizeof(uint8_T)));
  MW_I2C_MasterWrite(localDW->obj.MW_I2C_HANDLE, 105U, SwappedDataBytes, 2U,
                     false, false);
}

/* Termination for function-call system: '<S1>/i2cWr_mpu' */
void master_code__i2cWr_mpu_Term(DW_i2cWr_mpu_master_code_V3_1_T *localDW)
{
  /* Terminate for MATLABSystem: '<S6>/I2C Master Write' */
  matlabCodegenHandle_matlabCodeg(&localDW->obj);
}

static void m_SystemCore_release_p01msq5wle(const
  codertarget_raspi__nparbhma5g_T *obj)
{
  if ((obj->isInitialized == 1) && obj->isSetupComplete) {
    MW_I2C_Close(obj->MW_I2C_HANDLE);
  }
}

static void ma_SystemCore_delete_jwpcm5lajz(const
  codertarget_raspi__nparbhma5g_T *obj)
{
  m_SystemCore_release_p01msq5wle(obj);
}

static void matlabCodegenHandle__eekrmxfgnd(codertarget_raspi__nparbhma5g_T *obj)
{
  if (!obj->matlabCodegenIsDeleted) {
    obj->matlabCodegenIsDeleted = true;
    ma_SystemCore_delete_jwpcm5lajz(obj);
  }
}

/* System initialize for function-call system: '<S1>/i2cRd' */
void master_code_V3_1_i2cRd_Init(B_i2cRd_master_code_V3_1_T *localB,
  P_i2cRd_master_code_V3_1_T *localP)
{
  int32_T i;

  /* SystemInitialize for Outport: '<S5>/data_imu' */
  for (i = 0; i < 7; i++) {
    localB->DataTypeConversion4[i] = localP->data_imu_Y0;
  }

  /* End of SystemInitialize for Outport: '<S5>/data_imu' */

  /* SystemInitialize for Outport: '<S5>/data_enc_rear' */
  localB->DataTypeConversion1[0] = localP->data_enc_rear_Y0;

  /* SystemInitialize for Outport: '<S5>/data_enc_front' */
  localB->DataTypeConversion6[0] = localP->data_enc_front_Y0;

  /* SystemInitialize for Outport: '<S5>/data_enc_rear' */
  localB->DataTypeConversion1[1] = localP->data_enc_rear_Y0;

  /* SystemInitialize for Outport: '<S5>/data_enc_front' */
  localB->DataTypeConversion6[1] = localP->data_enc_front_Y0;
}

/* Start for function-call system: '<S1>/i2cRd' */
void master_code_V3__i2cRd_Start(DW_i2cRd_master_code_V3_1_T *localDW,
  P_i2cRd_master_code_V3_1_T *localP)
{
  codertarget_raspi__nparbhma5g_T *obj;
  uint32_T i2cname;

  /* Start for MATLABSystem: '<S5>/I2C Master Read4' */
  localDW->obj.matlabCodegenIsDeleted = true;
  localDW->obj.isInitialized = 0;
  localDW->obj.matlabCodegenIsDeleted = false;
  localDW->obj.SampleTime = localP->I2CMasterRead4_SampleTime;
  obj = &localDW->obj;
  localDW->obj.isSetupComplete = false;
  localDW->obj.isInitialized = 1;
  i2cname = 1;
  obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
  localDW->obj.BusSpeed = 100000U;
  MW_I2C_SetBusSpeed(localDW->obj.MW_I2C_HANDLE, localDW->obj.BusSpeed);
  localDW->obj.isSetupComplete = true;

  /* Start for MATLABSystem: '<S5>/I2C Master Read1' */
  localDW->obj_mxsciaoufc.matlabCodegenIsDeleted = true;
  localDW->obj_mxsciaoufc.isInitialized = 0;
  localDW->obj_mxsciaoufc.matlabCodegenIsDeleted = false;
  localDW->obj_mxsciaoufc.SampleTime = localP->I2CMasterRead1_SampleTime;
  obj = &localDW->obj_mxsciaoufc;
  localDW->obj_mxsciaoufc.isSetupComplete = false;
  localDW->obj_mxsciaoufc.isInitialized = 1;
  i2cname = 1;
  obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
  localDW->obj_mxsciaoufc.BusSpeed = 100000U;
  MW_I2C_SetBusSpeed(localDW->obj_mxsciaoufc.MW_I2C_HANDLE,
                     localDW->obj_mxsciaoufc.BusSpeed);
  localDW->obj_mxsciaoufc.isSetupComplete = true;

  /* Start for MATLABSystem: '<S5>/I2C Master Read6' */
  localDW->obj_dz0rzkn42o.matlabCodegenIsDeleted = true;
  localDW->obj_dz0rzkn42o.isInitialized = 0;
  localDW->obj_dz0rzkn42o.matlabCodegenIsDeleted = false;
  localDW->obj_dz0rzkn42o.SampleTime = localP->I2CMasterRead6_SampleTime;
  obj = &localDW->obj_dz0rzkn42o;
  localDW->obj_dz0rzkn42o.isSetupComplete = false;
  localDW->obj_dz0rzkn42o.isInitialized = 1;
  i2cname = 1;
  obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
  localDW->obj_dz0rzkn42o.BusSpeed = 100000U;
  MW_I2C_SetBusSpeed(localDW->obj_dz0rzkn42o.MW_I2C_HANDLE,
                     localDW->obj_dz0rzkn42o.BusSpeed);
  localDW->obj_dz0rzkn42o.isSetupComplete = true;
}

/* Output and update for function-call system: '<S1>/i2cRd' */
void master_code_V3_1_i2cRd(B_i2cRd_master_code_V3_1_T *localB,
  DW_i2cRd_master_code_V3_1_T *localDW, P_i2cRd_master_code_V3_1_T *localP)
{
  uint16_T b_output[7];
  uint8_T status;
  uint8_T output_raw[14];
  uint8_T y[2];
  uint16_T x;
  uint8_T b_x[2];
  uint16_T output[2];
  uint8_T output_raw_0[4];
  int32_T i;

  /* MATLABSystem: '<S5>/I2C Master Read4' */
  if (localDW->obj.SampleTime != localP->I2CMasterRead4_SampleTime) {
    localDW->obj.SampleTime = localP->I2CMasterRead4_SampleTime;
  }

  status = 59U;
  status = MW_I2C_MasterWrite(localDW->obj.MW_I2C_HANDLE, 105U, &status, 1U,
    true, false);
  if (0 == status) {
    MW_I2C_MasterRead(localDW->obj.MW_I2C_HANDLE, 105U, output_raw, 14U, false,
                      true);
    memcpy((void *)&b_output[0], (void *)&output_raw[0], (uint32_T)((size_t)7 *
            sizeof(uint16_T)));
    for (i = 0; i < 7; i++) {
      x = b_output[i];
      memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
      b_x[0] = y[1];
      b_x[1] = y[0];
      memcpy((void *)&b_output[i], (void *)&b_x[0], (uint32_T)((size_t)1 *
              sizeof(uint16_T)));
    }
  } else {
    for (i = 0; i < 7; i++) {
      b_output[i] = 0U;
    }
  }

  /* DataTypeConversion: '<S5>/Data Type Conversion4' incorporates:
   *  MATLABSystem: '<S5>/I2C Master Read4'
   */
  for (i = 0; i < 7; i++) {
    localB->DataTypeConversion4[i] = (int16_T)b_output[i];
  }

  /* End of DataTypeConversion: '<S5>/Data Type Conversion4' */

  /* MATLABSystem: '<S5>/I2C Master Read1' */
  if (localDW->obj_mxsciaoufc.SampleTime != localP->I2CMasterRead1_SampleTime) {
    localDW->obj_mxsciaoufc.SampleTime = localP->I2CMasterRead1_SampleTime;
  }

  MW_I2C_MasterRead(localDW->obj_mxsciaoufc.MW_I2C_HANDLE, 3U, output_raw_0, 4U,
                    false, false);
  memcpy((void *)&output[0], (void *)&output_raw_0[0], (uint32_T)((size_t)2 *
          sizeof(uint16_T)));
  x = output[0];
  memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x[0] = y[1];
  b_x[1] = y[0];
  memcpy((void *)&output[0], (void *)&b_x[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));
  x = output[1];
  memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x[0] = y[1];
  b_x[1] = y[0];
  memcpy((void *)&output[1], (void *)&b_x[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));

  /* DataTypeConversion: '<S5>/Data Type Conversion1' incorporates:
   *  MATLABSystem: '<S5>/I2C Master Read1'
   */
  localB->DataTypeConversion1[0] = (int16_T)output[0];
  localB->DataTypeConversion1[1] = (int16_T)output[1];

  /* MATLABSystem: '<S5>/I2C Master Read6' */
  if (localDW->obj_dz0rzkn42o.SampleTime != localP->I2CMasterRead6_SampleTime) {
    localDW->obj_dz0rzkn42o.SampleTime = localP->I2CMasterRead6_SampleTime;
  }

  MW_I2C_MasterRead(localDW->obj_dz0rzkn42o.MW_I2C_HANDLE, 4U, output_raw_0, 4U,
                    false, false);
  memcpy((void *)&output[0], (void *)&output_raw_0[0], (uint32_T)((size_t)2 *
          sizeof(uint16_T)));
  x = output[0];
  memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x[0] = y[1];
  b_x[1] = y[0];
  memcpy((void *)&output[0], (void *)&b_x[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));
  x = output[1];
  memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x[0] = y[1];
  b_x[1] = y[0];
  memcpy((void *)&output[1], (void *)&b_x[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));

  /* DataTypeConversion: '<S5>/Data Type Conversion6' incorporates:
   *  MATLABSystem: '<S5>/I2C Master Read6'
   */
  localB->DataTypeConversion6[0] = (int16_T)output[0];
  localB->DataTypeConversion6[1] = (int16_T)output[1];
}

/* Termination for function-call system: '<S1>/i2cRd' */
void master_code_V3_1_i2cRd_Term(DW_i2cRd_master_code_V3_1_T *localDW)
{
  /* Terminate for MATLABSystem: '<S5>/I2C Master Read4' */
  matlabCodegenHandle__eekrmxfgnd(&localDW->obj);

  /* Terminate for MATLABSystem: '<S5>/I2C Master Read1' */
  matlabCodegenHandle__eekrmxfgnd(&localDW->obj_mxsciaoufc);

  /* Terminate for MATLABSystem: '<S5>/I2C Master Read6' */
  matlabCodegenHandle__eekrmxfgnd(&localDW->obj_dz0rzkn42o);
}

/* System initialize for function-call system: '<S1>/udpRd' */
void master_code_V3_1_udpRd_Init(B_udpRd_master_code_V3_1_T *localB,
  P_udpRd_master_code_V3_1_T *localP)
{
  int32_T i;

  /* SystemInitialize for Outport: '<S7>/udpr' */
  for (i = 0; i < 20; i++) {
    localB->UDPReceive_o1[i] = localP->udpr_Y0;
  }

  /* End of SystemInitialize for Outport: '<S7>/udpr' */
}

/* Start for function-call system: '<S1>/udpRd' */
void master_code_V3__udpRd_Start(RT_MODEL_master_code_V3_1_T * const
  master_code_V3_1_M, DW_udpRd_master_code_V3_1_T *localDW,
  P_udpRd_master_code_V3_1_T *localP)
{
  char_T *sErr;

  /* Start for S-Function (sdspFromNetwork): '<S7>/UDP Receive' */
  sErr = GetErrorBuffer(&localDW->UDPReceive_NetworkLib[0U]);
  CreateUDPInterface(&localDW->UDPReceive_NetworkLib[0U]);
  if (*sErr == 0) {
    LibCreate_Network(&localDW->UDPReceive_NetworkLib[0U], 0, "0.0.0.0",
                      localP->UDPReceive_Port, "0.0.0.0", -1, 8192, 2, 0);
  }

  if (*sErr == 0) {
    LibStart(&localDW->UDPReceive_NetworkLib[0U]);
  }

  if (*sErr != 0) {
    DestroyUDPInterface(&localDW->UDPReceive_NetworkLib[0U]);
    if (*sErr != 0) {
      rtmSetErrorStatus(master_code_V3_1_M, sErr);
      rtmSetStopRequested(master_code_V3_1_M, 1);
    }
  }

  /* End of Start for S-Function (sdspFromNetwork): '<S7>/UDP Receive' */
}

/* Output and update for function-call system: '<S1>/udpRd' */
void master_code_V3_1_udpRd(RT_MODEL_master_code_V3_1_T * const
  master_code_V3_1_M, B_udpRd_master_code_V3_1_T *localB,
  DW_udpRd_master_code_V3_1_T *localDW)
{
  char_T *sErr;
  int32_T samplesRead;

  /* S-Function (sdspFromNetwork): '<S7>/UDP Receive' */
  sErr = GetErrorBuffer(&localDW->UDPReceive_NetworkLib[0U]);
  samplesRead = 20;
  LibOutputs_Network(&localDW->UDPReceive_NetworkLib[0U], &localB->
                     UDPReceive_o1[0U], &samplesRead);
  if (*sErr != 0) {
    rtmSetErrorStatus(master_code_V3_1_M, sErr);
    rtmSetStopRequested(master_code_V3_1_M, 1);
  }

  /* End of S-Function (sdspFromNetwork): '<S7>/UDP Receive' */
}

/* Termination for function-call system: '<S1>/udpRd' */
void master_code_V3_1_udpRd_Term(RT_MODEL_master_code_V3_1_T * const
  master_code_V3_1_M, DW_udpRd_master_code_V3_1_T *localDW)
{
  char_T *sErr;

  /* Terminate for S-Function (sdspFromNetwork): '<S7>/UDP Receive' */
  sErr = GetErrorBuffer(&localDW->UDPReceive_NetworkLib[0U]);
  LibTerminate(&localDW->UDPReceive_NetworkLib[0U]);
  if (*sErr != 0) {
    rtmSetErrorStatus(master_code_V3_1_M, sErr);
    rtmSetStopRequested(master_code_V3_1_M, 1);
  }

  LibDestroy(&localDW->UDPReceive_NetworkLib[0U], 0);
  DestroyUDPInterface(&localDW->UDPReceive_NetworkLib[0U]);

  /* End of Terminate for S-Function (sdspFromNetwork): '<S7>/UDP Receive' */
}

static boolean_T master_code_V3_findpivotIdx_u16(int32_T i, int32_T j, int32_T
  startIdx, int32_T pivot[], uint32_T midxPort[], const uint16_T mdataPort[])
{
  boolean_T pivotFind;
  uint16_T tmp1_tmp;

  /* S-Function (sdspmdn2): '<S3>/Median' */
  master_code_V3_1_B.mid = (i + j) >> 1;
  master_code_V3_1_B.sIdx = (uint32_T)startIdx;
  master_code_V3_1_B.tmp0 = mdataPort[midxPort[i] + master_code_V3_1_B.sIdx];
  tmp1_tmp = mdataPort[midxPort[master_code_V3_1_B.mid] +
    master_code_V3_1_B.sIdx];
  if (master_code_V3_1_B.tmp0 > tmp1_tmp) {
    master_code_V3_1_B.t = midxPort[i];
    midxPort[i] = midxPort[master_code_V3_1_B.mid];
    midxPort[master_code_V3_1_B.mid] = master_code_V3_1_B.t;
  }

  if (master_code_V3_1_B.tmp0 > mdataPort[midxPort[j] + master_code_V3_1_B.sIdx])
  {
    master_code_V3_1_B.t = midxPort[i];
    midxPort[i] = midxPort[j];
    midxPort[j] = master_code_V3_1_B.t;
  }

  if (tmp1_tmp > mdataPort[midxPort[j] + master_code_V3_1_B.sIdx]) {
    master_code_V3_1_B.t = midxPort[master_code_V3_1_B.mid];
    midxPort[master_code_V3_1_B.mid] = midxPort[j];
    midxPort[j] = master_code_V3_1_B.t;
  }

  master_code_V3_1_B.tmp0 = mdataPort[midxPort[i] + master_code_V3_1_B.sIdx];
  tmp1_tmp = mdataPort[midxPort[master_code_V3_1_B.mid] +
    master_code_V3_1_B.sIdx];
  pivotFind = false;
  if (master_code_V3_1_B.tmp0 < tmp1_tmp) {
    pivot[0U] = master_code_V3_1_B.mid;
    pivotFind = true;
  } else if (tmp1_tmp < mdataPort[midxPort[j] + master_code_V3_1_B.sIdx]) {
    pivot[0U] = j;
    pivotFind = true;
  } else {
    master_code_V3_1_B.mid = i + 1;
    while ((master_code_V3_1_B.mid <= j) && (!pivotFind)) {
      tmp1_tmp = mdataPort[midxPort[master_code_V3_1_B.mid] +
        master_code_V3_1_B.sIdx];
      if (tmp1_tmp != master_code_V3_1_B.tmp0) {
        if (tmp1_tmp < master_code_V3_1_B.tmp0) {
          pivot[0U] = i;
        } else {
          pivot[0U] = master_code_V3_1_B.mid;
        }

        pivotFind = true;
        master_code_V3_1_B.mid = j + 1;
      }

      master_code_V3_1_B.mid++;
    }
  }

  /* End of S-Function (sdspmdn2): '<S3>/Median' */
  return pivotFind;
}

static int32_T master_code_V3_partitionIdx_u16(int32_T i, int32_T j, int32_T
  startIdx, const int32_T pivot[], uint32_T midxPort[], const uint16_T
  mdataPort[])
{
  int32_T idx;
  uint16_T tmp0;

  /* S-Function (sdspmdn2): '<S3>/Median' */
  master_code_V3_1_B.sIdx_kkiq3xxxve = (uint32_T)startIdx;
  tmp0 = mdataPort[midxPort[pivot[0U]] + master_code_V3_1_B.sIdx_kkiq3xxxve];
  master_code_V3_1_B.ctidx = j - i;
  while ((i <= j) && (master_code_V3_1_B.ctidx >= 0)) {
    while (mdataPort[midxPort[i] + master_code_V3_1_B.sIdx_kkiq3xxxve] < tmp0) {
      i++;
    }

    while (mdataPort[midxPort[j] + master_code_V3_1_B.sIdx_kkiq3xxxve] >= tmp0)
    {
      j--;
    }

    if (i < j) {
      master_code_V3_1_B.t_cxarnvbvui = midxPort[i];
      midxPort[i] = midxPort[j];
      midxPort[j] = master_code_V3_1_B.t_cxarnvbvui;
      i++;
      j--;
    }

    master_code_V3_1_B.ctidx--;
  }

  idx = i;

  /* End of S-Function (sdspmdn2): '<S3>/Median' */
  return idx;
}

static void master_code_V3_1_MDNQSort_u16(int32_T i, int32_T j, int32_T startIdx,
  uint32_T midxPort[], const uint16_T mdataPort[])
{
  int32_T pivot;

  /* S-Function (sdspmdn2): '<S3>/Median' */
  if (master_code_V3_findpivotIdx_u16(i, j, startIdx, &pivot, &midxPort[0U],
       &mdataPort[0U])) {
    pivot = master_code_V3_partitionIdx_u16(i, j, startIdx, &pivot, &midxPort[0U],
      &mdataPort[0U]);
    master_code_V3_1_MDNQSort_u16(i, pivot - 1, startIdx, &midxPort[0U],
      &mdataPort[0U]);
    master_code_V3_1_MDNQSort_u16(pivot, j, startIdx, &midxPort[0U], &mdataPort
      [0U]);
  }

  /* End of S-Function (sdspmdn2): '<S3>/Median' */
}

static void m_SystemCore_release_eh23cloj25(const
  codertarget_raspi__nparbhma5g_T *obj)
{
  if ((obj->isInitialized == 1) && obj->isSetupComplete) {
    MW_I2C_Close(obj->MW_I2C_HANDLE);
  }
}

static void ma_SystemCore_delete_eh23cloj25(const
  codertarget_raspi__nparbhma5g_T *obj)
{
  m_SystemCore_release_eh23cloj25(obj);
}

static void matlabCodegenHandle__eh23cloj25(codertarget_raspi__nparbhma5g_T *obj)
{
  if (!obj->matlabCodegenIsDeleted) {
    obj->matlabCodegenIsDeleted = true;
    ma_SystemCore_delete_eh23cloj25(obj);
  }
}

static void m_SystemCore_rele_kadk1bn3pp(const codertarget_raspi_internal_I2_T
  *obj)
{
  if ((obj->isInitialized == 1) && obj->isSetupComplete) {
    MW_I2C_Close(obj->MW_I2C_HANDLE);
  }
}

static void m_SystemCore_dele_kadk1bn3pp(const codertarget_raspi_internal_I2_T
  *obj)
{
  m_SystemCore_rele_kadk1bn3pp(obj);
}

static void mast_matlabCodegenHa_kadk1bn3pp(codertarget_raspi_internal_I2_T *obj)
{
  if (!obj->matlabCodegenIsDeleted) {
    obj->matlabCodegenIsDeleted = true;
    m_SystemCore_dele_kadk1bn3pp(obj);
  }
}

/* Model step function for TID0 */
void master_code_V3_1_step0(void)      /* Sample time: [0.001s, 0.0s] */
{
  int_T tid = 0;

  {                                    /* Sample time: [0.001s, 0.0s] */
    rate_scheduler();
  }

  /* Matfile logging */
  rt_UpdateTXYLogVars(master_code_V3_1_M->rtwLogInfo,
                      (&master_code_V3_1_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.001s, 0.0s] */
    if ((rtmGetTFinal(master_code_V3_1_M)!=-1) &&
        !((rtmGetTFinal(master_code_V3_1_M)-master_code_V3_1_M->Timing.taskTime0)
          > master_code_V3_1_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(master_code_V3_1_M, "Simulation finished");
    }
  }

  /* Update absolute time */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  master_code_V3_1_M->Timing.taskTime0 =
    (++master_code_V3_1_M->Timing.clockTick0) *
    master_code_V3_1_M->Timing.stepSize0;

  /* If subsystem generates rate grouping Output functions,
   * when tid is used in Output function for one rate,
   * all Output functions include tid as a local variable.
   * As result, some Output functions may have unused tid.
   */
  UNUSED_PARAMETER(tid);
}

/* Model step function for TID1 */
void master_code_V3_1_step1(void)      /* Sample time: [0.01s, 0.0s] */
{
  int_T tid = 1;
  boolean_T p;
  boolean_T b_p;
  char_T *sErr;
  uint8_T status;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  boolean_T exitg1;

  /* Chart: '<Root>/ECU_STATE_MACHINE' */
  if (master_code_V3_1_DW.temporalCounter_i1 < MAX_uint32_T) {
    master_code_V3_1_DW.temporalCounter_i1++;
  }

  if (master_code_V3_1_DW.temporalCounter_i2 < 131071U) {
    master_code_V3_1_DW.temporalCounter_i2++;
  }

  /* Gateway: ECU_STATE_MACHINE */
  /* During: ECU_STATE_MACHINE */
  if (master_code_V3_1_DW.is_active_c3_master_code_V3_1 == 0U) {
    /* Entry: ECU_STATE_MACHINE */
    master_code_V3_1_DW.is_active_c3_master_code_V3_1 = 1U;

    /* Entry Internal: ECU_STATE_MACHINE */
    /* Transition: '<S1>:35' */
    master_code_V3_1_DW.is_c3_master_code_V3_1 = master_code_V_IN_initialization;
    master_code_V3_1_DW.temporalCounter_i1 = 0U;

    /* Entry 'initialization': '<S1>:34' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V3_1_B.dataw = 27392U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V3_1_i2cWr_mpu(master_code_V3_1_B.dataw,
      &master_code_V3_1_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V3_1_B.dataw = 6937U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V3_1_i2cWr_mpu(master_code_V3_1_B.dataw,
      &master_code_V3_1_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V3_1_B.dataw = 7176U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V3_1_i2cWr_mpu(master_code_V3_1_B.dataw,
      &master_code_V3_1_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V3_1_B.dataw = 6662U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V3_1_i2cWr_mpu(master_code_V3_1_B.dataw,
      &master_code_V3_1_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V3_1_B.dataw = 14096U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V3_1_i2cWr_mpu(master_code_V3_1_B.dataw,
      &master_code_V3_1_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V3_1_B.dataw = 14336U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V3_1_i2cWr_mpu(master_code_V3_1_B.dataw,
      &master_code_V3_1_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    for (master_code_V3_1_B.i = 0; master_code_V3_1_B.i < 20;
         master_code_V3_1_B.i++) {
      master_code_V3_1_DW.tmp[master_code_V3_1_B.i] = 1;
    }

    master_code_V3_1_DW.i = 1;
  } else {
    guard1 = false;
    guard2 = false;
    switch (master_code_V3_1_DW.is_c3_master_code_V3_1) {
     case master_code_V3_1_IN_Running:
      /* During 'Running': '<S1>:167' */
      if (master_code_V3_1_DW.temporalCounter_i2 >= 100000U) {
        /* Transition: '<S1>:93' */
        /* Exit Internal 'Running': '<S1>:167' */
        master_code_V3_1_DW.is_Running = master_code__IN_NO_ACTIVE_CHILD;
        master_code_V3_1_DW.is_c3_master_code_V3_1 =
          master_code_V3_1_IN_idle_state;

        /* Entry 'idle_state': '<S1>:92' */
      } else {
        switch (master_code_V3_1_DW.is_Running) {
         case master_code_V3_IN_Interpolation:
          /* During 'Interpolation': '<S1>:158' */
          if ((uint32_T)((int32_T)master_code_V3_1_DW.temporalCounter_i1 * 10) >=
              (uint32_T)ceil(10.0 - master_code_V3_1_DW.clock_time_2)) {
            /* Transition: '<S1>:193' */
            if (master_code_V3_1_DW.i < 10) {
              /* Transition: '<S1>:187' */
              master_code_V3_1_B.i = master_code_V3_1_DW.i + 1;
              if (master_code_V3_1_B.i > 32767) {
                master_code_V3_1_B.i = 32767;
              }

              master_code_V3_1_DW.i = (int16_T)master_code_V3_1_B.i;
              master_code_V3_1_DW.interp_flag = 2;
              guard2 = true;
            } else {
              if (master_code_V3_1_DW.i > 9) {
                /* Transition: '<S1>:188' */
                master_code_V3_1_DW.i = 10;
                master_code_V3_1_DW.interp_flag = 2;
                guard2 = true;
              }
            }
          }
          break;

         default:
          /* During 'Sensing_Receiving': '<S1>:154' */
          if (master_code_V3_1_DW.interp_flag == 1) {
            /* Transition: '<S1>:159' */
            guard1 = true;
          } else if (master_code_V3_1_DW.interp_flag == 0) {
            /* Transition: '<S1>:165' */
            master_code_V3_1_DW.i = 1;
            guard1 = true;
          } else {
            /* Outputs for Function Call SubSystem: '<S1>/i2cRd' */
            /* Simulink Function 'i2cRd': '<S1>:56' */
            master_code_V3_1_i2cRd(&master_code_V3_1_B.i2cRd,
              &master_code_V3_1_DW.i2cRd, &master_code_V3_1_P.i2cRd);

            /* End of Outputs for SubSystem: '<S1>/i2cRd' */
            master_code_V3_1_B.enc_rear[0] =
              master_code_V3_1_B.i2cRd.DataTypeConversion1[0];
            master_code_V3_1_B.enc_front[0] =
              master_code_V3_1_B.i2cRd.DataTypeConversion6[0];
            master_code_V3_1_B.enc_rear[1] =
              master_code_V3_1_B.i2cRd.DataTypeConversion1[1];
            master_code_V3_1_B.enc_front[1] =
              master_code_V3_1_B.i2cRd.DataTypeConversion6[1];
            master_code_V3_1_B.i = master_code_V3_1_B.i2cRd.DataTypeConversion4
              [0] - master_code_V3_1_DW.acc_x_calib;
            if (master_code_V3_1_B.i > 32767) {
              master_code_V3_1_B.i = 32767;
            } else {
              if (master_code_V3_1_B.i < -32768) {
                master_code_V3_1_B.i = -32768;
              }
            }

            master_code_V3_1_B.acc_mpu[0] = (int16_T)master_code_V3_1_B.i;
            master_code_V3_1_B.i = master_code_V3_1_B.i2cRd.DataTypeConversion4
              [1] - master_code_V3_1_DW.acc_y_calib;
            if (master_code_V3_1_B.i > 32767) {
              master_code_V3_1_B.i = 32767;
            } else {
              if (master_code_V3_1_B.i < -32768) {
                master_code_V3_1_B.i = -32768;
              }
            }

            master_code_V3_1_B.acc_mpu[1] = (int16_T)master_code_V3_1_B.i;
            master_code_V3_1_B.i = master_code_V3_1_B.i2cRd.DataTypeConversion4
              [2] - master_code_V3_1_DW.acc_z_calib;
            if (master_code_V3_1_B.i > 32767) {
              master_code_V3_1_B.i = 32767;
            } else {
              if (master_code_V3_1_B.i < -32768) {
                master_code_V3_1_B.i = -32768;
              }
            }

            master_code_V3_1_B.acc_mpu[2] = (int16_T)master_code_V3_1_B.i;
            master_code_V3_1_B.i = master_code_V3_1_B.i2cRd.DataTypeConversion4
              [4] - master_code_V3_1_DW.gyro_x_calib;
            if (master_code_V3_1_B.i > 32767) {
              master_code_V3_1_B.i = 32767;
            } else {
              if (master_code_V3_1_B.i < -32768) {
                master_code_V3_1_B.i = -32768;
              }
            }

            master_code_V3_1_B.gyro_mpu[0] = (int16_T)master_code_V3_1_B.i;
            master_code_V3_1_B.i = master_code_V3_1_B.i2cRd.DataTypeConversion4
              [5] - master_code_V3_1_DW.gyro_y_calib;
            if (master_code_V3_1_B.i > 32767) {
              master_code_V3_1_B.i = 32767;
            } else {
              if (master_code_V3_1_B.i < -32768) {
                master_code_V3_1_B.i = -32768;
              }
            }

            master_code_V3_1_B.gyro_mpu[1] = (int16_T)master_code_V3_1_B.i;
            master_code_V3_1_B.i = master_code_V3_1_B.i2cRd.DataTypeConversion4
              [6] - master_code_V3_1_DW.gyro_z_calib;
            if (master_code_V3_1_B.i > 32767) {
              master_code_V3_1_B.i = 32767;
            } else {
              if (master_code_V3_1_B.i < -32768) {
                master_code_V3_1_B.i = -32768;
              }
            }

            master_code_V3_1_B.gyro_mpu[2] = (int16_T)master_code_V3_1_B.i;
            master_code_V3_1_B.data_imu_raw[0] =
              master_code_V3_1_B.i2cRd.DataTypeConversion4[0];
            master_code_V3_1_B.data_imu_raw[1] =
              master_code_V3_1_B.i2cRd.DataTypeConversion4[1];
            master_code_V3_1_B.data_imu_raw[2] =
              master_code_V3_1_B.i2cRd.DataTypeConversion4[2];

            /* Outputs for Function Call SubSystem: '<S1>/udpRd' */
            /* Simulink Function 'udpRd': '<S1>:128' */
            master_code_V3_1_udpRd(master_code_V3_1_M, &master_code_V3_1_B.udpRd,
              &master_code_V3_1_DW.udpRd);

            /* End of Outputs for SubSystem: '<S1>/udpRd' */
            for (master_code_V3_1_B.i = 0; master_code_V3_1_B.i < 20;
                 master_code_V3_1_B.i++) {
              master_code_V3_1_B.udpr[master_code_V3_1_B.i] =
                master_code_V3_1_B.udpRd.UDPReceive_o1[master_code_V3_1_B.i];
            }

            master_code_V3_1_DW.clock_time = (real_T)
              master_code_V3_1_DW.temporalCounter_i1 * 10.0;
            p = false;
            b_p = true;
            master_code_V3_1_B.i = 0;
            exitg1 = false;
            while ((!exitg1) && (master_code_V3_1_B.i < 20)) {
              if (master_code_V3_1_DW.tmp[master_code_V3_1_B.i] !=
                  master_code_V3_1_B.udpr[master_code_V3_1_B.i]) {
                b_p = false;
                exitg1 = true;
              } else {
                master_code_V3_1_B.i++;
              }
            }

            if (b_p) {
              p = true;
            }

            master_code_V3_1_DW.interp_flag = p;
          }
          break;
        }
      }
      break;

     case master_code_V3_1_IN_idle_state:
      /* Outputs for Function Call SubSystem: '<S1>/i2cRd' */
      /* During 'idle_state': '<S1>:92' */
      /* Simulink Function 'i2cRd': '<S1>:56' */
      master_code_V3_1_i2cRd(&master_code_V3_1_B.i2cRd,
        &master_code_V3_1_DW.i2cRd, &master_code_V3_1_P.i2cRd);

      /* End of Outputs for SubSystem: '<S1>/i2cRd' */
      for (master_code_V3_1_B.i = 0; master_code_V3_1_B.i < 7;
           master_code_V3_1_B.i++) {
        master_code_V3_1_B.data_imu_mpu[master_code_V3_1_B.i] =
          master_code_V3_1_B.i2cRd.DataTypeConversion4[master_code_V3_1_B.i];
      }

      master_code_V3_1_B.enc_rear[0] =
        master_code_V3_1_B.i2cRd.DataTypeConversion1[0];
      master_code_V3_1_B.enc_front[0] =
        master_code_V3_1_B.i2cRd.DataTypeConversion6[0];
      master_code_V3_1_B.enc_rear[1] =
        master_code_V3_1_B.i2cRd.DataTypeConversion1[1];
      master_code_V3_1_B.enc_front[1] =
        master_code_V3_1_B.i2cRd.DataTypeConversion6[1];

      /* Outputs for Function Call SubSystem: '<S1>/udpRd' */
      /* Simulink Function 'udpRd': '<S1>:128' */
      master_code_V3_1_udpRd(master_code_V3_1_M, &master_code_V3_1_B.udpRd,
        &master_code_V3_1_DW.udpRd);

      /* End of Outputs for SubSystem: '<S1>/udpRd' */
      for (master_code_V3_1_B.i = 0; master_code_V3_1_B.i < 20;
           master_code_V3_1_B.i++) {
        master_code_V3_1_B.udpr[master_code_V3_1_B.i] =
          master_code_V3_1_B.udpRd.UDPReceive_o1[master_code_V3_1_B.i];
      }

      master_code_V3_1_B.i = master_code_V3_1_B.data_imu_mpu[0] -
        master_code_V3_1_DW.acc_x_calib;
      if (master_code_V3_1_B.i > 32767) {
        master_code_V3_1_B.i = 32767;
      } else {
        if (master_code_V3_1_B.i < -32768) {
          master_code_V3_1_B.i = -32768;
        }
      }

      master_code_V3_1_B.acc_mpu[0] = (int16_T)master_code_V3_1_B.i;
      master_code_V3_1_B.i = master_code_V3_1_B.data_imu_mpu[1] -
        master_code_V3_1_DW.acc_y_calib;
      if (master_code_V3_1_B.i > 32767) {
        master_code_V3_1_B.i = 32767;
      } else {
        if (master_code_V3_1_B.i < -32768) {
          master_code_V3_1_B.i = -32768;
        }
      }

      master_code_V3_1_B.acc_mpu[1] = (int16_T)master_code_V3_1_B.i;
      master_code_V3_1_B.i = master_code_V3_1_B.data_imu_mpu[2] -
        master_code_V3_1_DW.acc_z_calib;
      if (master_code_V3_1_B.i > 32767) {
        master_code_V3_1_B.i = 32767;
      } else {
        if (master_code_V3_1_B.i < -32768) {
          master_code_V3_1_B.i = -32768;
        }
      }

      master_code_V3_1_B.acc_mpu[2] = (int16_T)master_code_V3_1_B.i;
      master_code_V3_1_B.i = master_code_V3_1_B.data_imu_mpu[4] -
        master_code_V3_1_DW.gyro_x_calib;
      if (master_code_V3_1_B.i > 32767) {
        master_code_V3_1_B.i = 32767;
      } else {
        if (master_code_V3_1_B.i < -32768) {
          master_code_V3_1_B.i = -32768;
        }
      }

      master_code_V3_1_B.gyro_mpu[0] = (int16_T)master_code_V3_1_B.i;
      master_code_V3_1_B.i = master_code_V3_1_B.data_imu_mpu[5] -
        master_code_V3_1_DW.gyro_y_calib;
      if (master_code_V3_1_B.i > 32767) {
        master_code_V3_1_B.i = 32767;
      } else {
        if (master_code_V3_1_B.i < -32768) {
          master_code_V3_1_B.i = -32768;
        }
      }

      master_code_V3_1_B.gyro_mpu[1] = (int16_T)master_code_V3_1_B.i;
      master_code_V3_1_B.i = master_code_V3_1_B.data_imu_mpu[6] -
        master_code_V3_1_DW.gyro_z_calib;
      if (master_code_V3_1_B.i > 32767) {
        master_code_V3_1_B.i = 32767;
      } else {
        if (master_code_V3_1_B.i < -32768) {
          master_code_V3_1_B.i = -32768;
        }
      }

      master_code_V3_1_B.gyro_mpu[2] = (int16_T)master_code_V3_1_B.i;
      master_code_V3_1_B.data_imu_raw[0] = master_code_V3_1_B.data_imu_mpu[0];
      master_code_V3_1_B.data_imu_raw[1] = master_code_V3_1_B.data_imu_mpu[1];
      master_code_V3_1_B.data_imu_raw[2] = master_code_V3_1_B.data_imu_mpu[2];
      master_code_V3_1_B.traction = master_code_V3_1_B.udpr[0];
      master_code_V3_1_B.steering = master_code_V3_1_B.udpr[10];
      break;

     default:
      /* During 'initialization': '<S1>:34' */
      if (master_code_V3_1_DW.temporalCounter_i1 >= 500U) {
        /* Transition: '<S1>:60' */
        master_code_V3_1_DW.is_c3_master_code_V3_1 = master_code_V3_1_IN_Running;
        master_code_V3_1_DW.temporalCounter_i2 = 0U;
        master_code_V3_1_DW.is_Running = master_cod_IN_Sensing_Receiving;
        master_code_V3_1_DW.temporalCounter_i1 = 0U;

        /* Entry 'Sensing_Receiving': '<S1>:154' */
        master_code_V3_1_DW.clock_time = 0.0;
      } else {
        /* Outputs for Function Call SubSystem: '<S1>/calib' */
        /* MATLABSystem: '<S3>/I2C Master Read' */
        /* Simulink Function 'calib': '<S1>:102' */
        if (master_code_V3_1_DW.obj.SampleTime !=
            master_code_V3_1_P.I2CMasterRead_SampleTime) {
          master_code_V3_1_DW.obj.SampleTime =
            master_code_V3_1_P.I2CMasterRead_SampleTime;
        }

        status = 59U;
        status = MW_I2C_MasterWrite(master_code_V3_1_DW.obj.MW_I2C_HANDLE, 105U,
          &status, 1U, true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V3_1_DW.obj.MW_I2C_HANDLE, 105U,
                            master_code_V3_1_B.output_raw, 2U, false, true);
          memcpy((void *)&master_code_V3_1_B.b_output, (void *)
                 &master_code_V3_1_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V3_1_B.output_raw[0], (void *)
                 &master_code_V3_1_B.b_output, (uint32_T)((size_t)2 * sizeof
                  (uint8_T)));
          master_code_V3_1_B.b_x_bhxxfovxdy[0] = master_code_V3_1_B.output_raw[1];
          master_code_V3_1_B.b_x_bhxxfovxdy[1] = master_code_V3_1_B.output_raw[0];
          memcpy((void *)&master_code_V3_1_B.I2CMasterRead, (void *)
                 &master_code_V3_1_B.b_x_bhxxfovxdy[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
        } else {
          master_code_V3_1_B.I2CMasterRead = 0U;
        }

        /* End of MATLABSystem: '<S3>/I2C Master Read' */

        /* S-Function (sdspmdn2): '<S3>/Median' */
        master_code_V3_1_MDNQSort_u16(0, 0, 0, &master_code_V3_1_DW.Median_Index,
          &master_code_V3_1_B.I2CMasterRead);

        /* DataTypeConversion: '<S3>/Data Type Conversion' incorporates:
         *  S-Function (sdspmdn2): '<S3>/Median'
         */
        master_code_V3_1_B.DataTypeConversion = (int16_T)
          master_code_V3_1_B.I2CMasterRead;

        /* MATLABSystem: '<S3>/I2C Master Read1' */
        if (master_code_V3_1_DW.obj_iwos115rnw.SampleTime !=
            master_code_V3_1_P.I2CMasterRead1_SampleTime) {
          master_code_V3_1_DW.obj_iwos115rnw.SampleTime =
            master_code_V3_1_P.I2CMasterRead1_SampleTime;
        }

        status = 61U;
        status = MW_I2C_MasterWrite
          (master_code_V3_1_DW.obj_iwos115rnw.MW_I2C_HANDLE, 105U, &status, 1U,
           true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V3_1_DW.obj_iwos115rnw.MW_I2C_HANDLE,
                            105U, master_code_V3_1_B.output_raw, 2U, false, true);
          memcpy((void *)&master_code_V3_1_B.b_output, (void *)
                 &master_code_V3_1_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V3_1_B.output_raw[0], (void *)
                 &master_code_V3_1_B.b_output, (uint32_T)((size_t)2 * sizeof
                  (uint8_T)));
          master_code_V3_1_B.b_x_bhxxfovxdy[0] = master_code_V3_1_B.output_raw[1];
          master_code_V3_1_B.b_x_bhxxfovxdy[1] = master_code_V3_1_B.output_raw[0];
          memcpy((void *)&master_code_V3_1_B.I2CMasterRead1, (void *)
                 &master_code_V3_1_B.b_x_bhxxfovxdy[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
        } else {
          master_code_V3_1_B.I2CMasterRead1 = 0U;
        }

        /* End of MATLABSystem: '<S3>/I2C Master Read1' */

        /* S-Function (sdspmdn2): '<S3>/Median1' */
        master_code_V3_1_MDNQSort_u16(0, 0, 0,
          &master_code_V3_1_DW.Median1_Index, &master_code_V3_1_B.I2CMasterRead1);

        /* DataTypeConversion: '<S3>/Data Type Conversion1' incorporates:
         *  S-Function (sdspmdn2): '<S3>/Median1'
         */
        master_code_V3_1_B.DataTypeConversion1 = (int16_T)
          master_code_V3_1_B.I2CMasterRead1;

        /* MATLABSystem: '<S3>/I2C Master Read2' */
        if (master_code_V3_1_DW.obj_i4zcoyvvmy.SampleTime !=
            master_code_V3_1_P.I2CMasterRead2_SampleTime) {
          master_code_V3_1_DW.obj_i4zcoyvvmy.SampleTime =
            master_code_V3_1_P.I2CMasterRead2_SampleTime;
        }

        status = 63U;
        status = MW_I2C_MasterWrite
          (master_code_V3_1_DW.obj_i4zcoyvvmy.MW_I2C_HANDLE, 105U, &status, 1U,
           true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V3_1_DW.obj_i4zcoyvvmy.MW_I2C_HANDLE,
                            105U, master_code_V3_1_B.output_raw, 2U, false, true);
          memcpy((void *)&master_code_V3_1_B.b_output, (void *)
                 &master_code_V3_1_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V3_1_B.output_raw[0], (void *)
                 &master_code_V3_1_B.b_output, (uint32_T)((size_t)2 * sizeof
                  (uint8_T)));
          master_code_V3_1_B.b_x_bhxxfovxdy[0] = master_code_V3_1_B.output_raw[1];
          master_code_V3_1_B.b_x_bhxxfovxdy[1] = master_code_V3_1_B.output_raw[0];
          memcpy((void *)&master_code_V3_1_B.I2CMasterRead2, (void *)
                 &master_code_V3_1_B.b_x_bhxxfovxdy[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
        } else {
          master_code_V3_1_B.I2CMasterRead2 = 0U;
        }

        /* End of MATLABSystem: '<S3>/I2C Master Read2' */

        /* S-Function (sdspmdn2): '<S3>/Median2' */
        master_code_V3_1_MDNQSort_u16(0, 0, 0,
          &master_code_V3_1_DW.Median2_Index, &master_code_V3_1_B.I2CMasterRead2);

        /* DataTypeConversion: '<S3>/Data Type Conversion2' incorporates:
         *  S-Function (sdspmdn2): '<S3>/Median2'
         */
        master_code_V3_1_B.DataTypeConversion2 = (int16_T)
          master_code_V3_1_B.I2CMasterRead2;

        /* MATLABSystem: '<S3>/I2C Master Read3' */
        if (master_code_V3_1_DW.obj_gwzo2fxivo.SampleTime !=
            master_code_V3_1_P.I2CMasterRead3_SampleTime) {
          master_code_V3_1_DW.obj_gwzo2fxivo.SampleTime =
            master_code_V3_1_P.I2CMasterRead3_SampleTime;
        }

        status = 67U;
        status = MW_I2C_MasterWrite
          (master_code_V3_1_DW.obj_gwzo2fxivo.MW_I2C_HANDLE, 105U, &status, 1U,
           true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V3_1_DW.obj_gwzo2fxivo.MW_I2C_HANDLE,
                            105U, master_code_V3_1_B.output_raw, 2U, false, true);
          memcpy((void *)&master_code_V3_1_B.b_output, (void *)
                 &master_code_V3_1_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V3_1_B.output_raw[0], (void *)
                 &master_code_V3_1_B.b_output, (uint32_T)((size_t)2 * sizeof
                  (uint8_T)));
          master_code_V3_1_B.b_x_bhxxfovxdy[0] = master_code_V3_1_B.output_raw[1];
          master_code_V3_1_B.b_x_bhxxfovxdy[1] = master_code_V3_1_B.output_raw[0];
          memcpy((void *)&master_code_V3_1_B.I2CMasterRead3, (void *)
                 &master_code_V3_1_B.b_x_bhxxfovxdy[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
        } else {
          master_code_V3_1_B.I2CMasterRead3 = 0U;
        }

        /* End of MATLABSystem: '<S3>/I2C Master Read3' */

        /* S-Function (sdspmdn2): '<S3>/Median3' */
        master_code_V3_1_MDNQSort_u16(0, 0, 0,
          &master_code_V3_1_DW.Median3_Index, &master_code_V3_1_B.I2CMasterRead3);

        /* DataTypeConversion: '<S3>/Data Type Conversion3' incorporates:
         *  S-Function (sdspmdn2): '<S3>/Median3'
         */
        master_code_V3_1_B.DataTypeConversion3 = (int16_T)
          master_code_V3_1_B.I2CMasterRead3;

        /* MATLABSystem: '<S3>/I2C Master Read4' */
        if (master_code_V3_1_DW.obj_daui2m5pff.SampleTime !=
            master_code_V3_1_P.I2CMasterRead4_SampleTime) {
          master_code_V3_1_DW.obj_daui2m5pff.SampleTime =
            master_code_V3_1_P.I2CMasterRead4_SampleTime;
        }

        status = 69U;
        status = MW_I2C_MasterWrite
          (master_code_V3_1_DW.obj_daui2m5pff.MW_I2C_HANDLE, 105U, &status, 1U,
           true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V3_1_DW.obj_daui2m5pff.MW_I2C_HANDLE,
                            105U, master_code_V3_1_B.output_raw, 2U, false, true);
          memcpy((void *)&master_code_V3_1_B.b_output, (void *)
                 &master_code_V3_1_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V3_1_B.output_raw[0], (void *)
                 &master_code_V3_1_B.b_output, (uint32_T)((size_t)2 * sizeof
                  (uint8_T)));
          master_code_V3_1_B.b_x_bhxxfovxdy[0] = master_code_V3_1_B.output_raw[1];
          master_code_V3_1_B.b_x_bhxxfovxdy[1] = master_code_V3_1_B.output_raw[0];
          memcpy((void *)&master_code_V3_1_B.I2CMasterRead4, (void *)
                 &master_code_V3_1_B.b_x_bhxxfovxdy[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
        } else {
          master_code_V3_1_B.I2CMasterRead4 = 0U;
        }

        /* End of MATLABSystem: '<S3>/I2C Master Read4' */

        /* S-Function (sdspmdn2): '<S3>/Median4' */
        master_code_V3_1_MDNQSort_u16(0, 0, 0,
          &master_code_V3_1_DW.Median4_Index, &master_code_V3_1_B.I2CMasterRead4);

        /* DataTypeConversion: '<S3>/Data Type Conversion4' incorporates:
         *  S-Function (sdspmdn2): '<S3>/Median4'
         */
        master_code_V3_1_B.DataTypeConversion4 = (int16_T)
          master_code_V3_1_B.I2CMasterRead4;

        /* MATLABSystem: '<S3>/I2C Master Read5' */
        if (master_code_V3_1_DW.obj_dp4cdr5kdf.SampleTime !=
            master_code_V3_1_P.I2CMasterRead5_SampleTime) {
          master_code_V3_1_DW.obj_dp4cdr5kdf.SampleTime =
            master_code_V3_1_P.I2CMasterRead5_SampleTime;
        }

        status = 71U;
        status = MW_I2C_MasterWrite
          (master_code_V3_1_DW.obj_dp4cdr5kdf.MW_I2C_HANDLE, 105U, &status, 1U,
           true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V3_1_DW.obj_dp4cdr5kdf.MW_I2C_HANDLE,
                            105U, master_code_V3_1_B.output_raw, 2U, false, true);
          memcpy((void *)&master_code_V3_1_B.b_output, (void *)
                 &master_code_V3_1_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V3_1_B.output_raw[0], (void *)
                 &master_code_V3_1_B.b_output, (uint32_T)((size_t)2 * sizeof
                  (uint8_T)));
          master_code_V3_1_B.b_x_bhxxfovxdy[0] = master_code_V3_1_B.output_raw[1];
          master_code_V3_1_B.b_x_bhxxfovxdy[1] = master_code_V3_1_B.output_raw[0];
          memcpy((void *)&master_code_V3_1_B.I2CMasterRead5, (void *)
                 &master_code_V3_1_B.b_x_bhxxfovxdy[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
        } else {
          master_code_V3_1_B.I2CMasterRead5 = 0U;
        }

        /* End of MATLABSystem: '<S3>/I2C Master Read5' */

        /* S-Function (sdspmdn2): '<S3>/Median5' */
        master_code_V3_1_MDNQSort_u16(0, 0, 0,
          &master_code_V3_1_DW.Median5_Index, &master_code_V3_1_B.I2CMasterRead5);

        /* DataTypeConversion: '<S3>/Data Type Conversion5' incorporates:
         *  S-Function (sdspmdn2): '<S3>/Median5'
         */
        master_code_V3_1_B.DataTypeConversion5 = (int16_T)
          master_code_V3_1_B.I2CMasterRead5;

        /* End of Outputs for SubSystem: '<S1>/calib' */
        master_code_V3_1_DW.acc_x_calib = master_code_V3_1_B.DataTypeConversion;
        master_code_V3_1_DW.acc_y_calib = master_code_V3_1_B.DataTypeConversion1;
        master_code_V3_1_DW.acc_z_calib = master_code_V3_1_B.DataTypeConversion2;
        master_code_V3_1_DW.gyro_x_calib =
          master_code_V3_1_B.DataTypeConversion3;
        master_code_V3_1_DW.gyro_y_calib =
          master_code_V3_1_B.DataTypeConversion4;
        master_code_V3_1_DW.gyro_z_calib =
          master_code_V3_1_B.DataTypeConversion5;

        /* Outputs for Function Call SubSystem: '<S1>/udpRd' */
        /* Simulink Function 'udpRd': '<S1>:128' */
        master_code_V3_1_udpRd(master_code_V3_1_M, &master_code_V3_1_B.udpRd,
          &master_code_V3_1_DW.udpRd);

        /* End of Outputs for SubSystem: '<S1>/udpRd' */
        for (master_code_V3_1_B.i = 0; master_code_V3_1_B.i < 20;
             master_code_V3_1_B.i++) {
          master_code_V3_1_B.udpr[master_code_V3_1_B.i] =
            master_code_V3_1_B.udpRd.UDPReceive_o1[master_code_V3_1_B.i];
        }

        master_code_V3_1_B.traction = master_code_V3_1_B.udpr[0];
        master_code_V3_1_B.steering = master_code_V3_1_B.udpr[10];

        /* Simulink Function 'cmd_motor': '<S1>:73' */
        master_code_V3_1_B.esc_cmd = master_code_V3_1_B.traction;
        master_code_V3_1_B.servo_cmd = master_code_V3_1_B.steering;

        /* Outputs for Function Call SubSystem: '<S1>/cmd_motor' */
        /* MATLABSystem: '<S4>/I2C Master Write1' incorporates:
         *  DataTypeConversion: '<S4>/Data Type Conversion'
         *  DataTypeConversion: '<S4>/Data Type Conversion1'
         */
        master_code_V3_1_B.b_x[0] = (uint16_T)master_code_V3_1_B.esc_cmd;
        master_code_V3_1_B.b_x[1] = (uint16_T)master_code_V3_1_B.servo_cmd;
        master_code_V3_1_B.I2CMasterRead = master_code_V3_1_B.b_x[0];
        memcpy((void *)&master_code_V3_1_B.output_raw[0], (void *)
               &master_code_V3_1_B.I2CMasterRead, (uint32_T)((size_t)2 * sizeof
                (uint8_T)));
        master_code_V3_1_B.b_x_bhxxfovxdy[0] = master_code_V3_1_B.output_raw[1];
        master_code_V3_1_B.b_x_bhxxfovxdy[1] = master_code_V3_1_B.output_raw[0];
        memcpy((void *)&master_code_V3_1_B.b_x[0], (void *)
               &master_code_V3_1_B.b_x_bhxxfovxdy[0], (uint32_T)((size_t)1 *
                sizeof(uint16_T)));
        master_code_V3_1_B.I2CMasterRead = master_code_V3_1_B.b_x[1];
        memcpy((void *)&master_code_V3_1_B.output_raw[0], (void *)
               &master_code_V3_1_B.I2CMasterRead, (uint32_T)((size_t)2 * sizeof
                (uint8_T)));
        master_code_V3_1_B.b_x_bhxxfovxdy[0] = master_code_V3_1_B.output_raw[1];
        master_code_V3_1_B.b_x_bhxxfovxdy[1] = master_code_V3_1_B.output_raw[0];
        memcpy((void *)&master_code_V3_1_B.b_x[1], (void *)
               &master_code_V3_1_B.b_x_bhxxfovxdy[0], (uint32_T)((size_t)1 *
                sizeof(uint16_T)));
        memcpy((void *)&master_code_V3_1_B.SwappedDataBytes[0], (void *)
               &master_code_V3_1_B.b_x[0], (uint32_T)((size_t)4 * sizeof(uint8_T)));
        MW_I2C_MasterWrite(master_code_V3_1_DW.obj_n15qv4o1gd.MW_I2C_HANDLE, 3U,
                           master_code_V3_1_B.SwappedDataBytes, 4U, false, false);

        /* End of Outputs for SubSystem: '<S1>/cmd_motor' */
      }
      break;
    }

    if (guard2) {
      master_code_V3_1_DW.is_Running = master_cod_IN_Sensing_Receiving;
      master_code_V3_1_DW.temporalCounter_i1 = 0U;

      /* Entry 'Sensing_Receiving': '<S1>:154' */
      master_code_V3_1_DW.clock_time = 0.0;
    }

    if (guard1) {
      master_code_V3_1_DW.is_Running = master_code_V3_IN_Interpolation;
      master_code_V3_1_DW.temporalCounter_i1 = 0U;

      /* Entry 'Interpolation': '<S1>:158' */
      master_code_V3_1_DW.clock_time_2 = 0.0;
      master_code_V3_1_B.traction =
        master_code_V3_1_B.udpr[master_code_V3_1_DW.i - 1];
      master_code_V3_1_B.i = master_code_V3_1_DW.i + 10;
      if (master_code_V3_1_B.i > 32767) {
        master_code_V3_1_B.i = 32767;
      }

      master_code_V3_1_B.steering = master_code_V3_1_B.udpr[master_code_V3_1_B.i
        - 1];

      /* Simulink Function 'cmd_motor': '<S1>:73' */
      master_code_V3_1_B.esc_cmd = master_code_V3_1_B.traction;
      master_code_V3_1_B.servo_cmd = master_code_V3_1_B.steering;

      /* Outputs for Function Call SubSystem: '<S1>/cmd_motor' */
      /* MATLABSystem: '<S4>/I2C Master Write1' incorporates:
       *  DataTypeConversion: '<S4>/Data Type Conversion'
       *  DataTypeConversion: '<S4>/Data Type Conversion1'
       */
      master_code_V3_1_B.b_x[0] = (uint16_T)master_code_V3_1_B.esc_cmd;
      master_code_V3_1_B.b_x[1] = (uint16_T)master_code_V3_1_B.servo_cmd;
      master_code_V3_1_B.I2CMasterRead = master_code_V3_1_B.b_x[0];
      memcpy((void *)&master_code_V3_1_B.output_raw[0], (void *)
             &master_code_V3_1_B.I2CMasterRead, (uint32_T)((size_t)2 * sizeof
              (uint8_T)));
      master_code_V3_1_B.b_x_bhxxfovxdy[0] = master_code_V3_1_B.output_raw[1];
      master_code_V3_1_B.b_x_bhxxfovxdy[1] = master_code_V3_1_B.output_raw[0];
      memcpy((void *)&master_code_V3_1_B.b_x[0], (void *)
             &master_code_V3_1_B.b_x_bhxxfovxdy[0], (uint32_T)((size_t)1 *
              sizeof(uint16_T)));
      master_code_V3_1_B.I2CMasterRead = master_code_V3_1_B.b_x[1];
      memcpy((void *)&master_code_V3_1_B.output_raw[0], (void *)
             &master_code_V3_1_B.I2CMasterRead, (uint32_T)((size_t)2 * sizeof
              (uint8_T)));
      master_code_V3_1_B.b_x_bhxxfovxdy[0] = master_code_V3_1_B.output_raw[1];
      master_code_V3_1_B.b_x_bhxxfovxdy[1] = master_code_V3_1_B.output_raw[0];
      memcpy((void *)&master_code_V3_1_B.b_x[1], (void *)
             &master_code_V3_1_B.b_x_bhxxfovxdy[0], (uint32_T)((size_t)1 *
              sizeof(uint16_T)));
      memcpy((void *)&master_code_V3_1_B.SwappedDataBytes[0], (void *)
             &master_code_V3_1_B.b_x[0], (uint32_T)((size_t)4 * sizeof(uint8_T)));
      MW_I2C_MasterWrite(master_code_V3_1_DW.obj_n15qv4o1gd.MW_I2C_HANDLE, 3U,
                         master_code_V3_1_B.SwappedDataBytes, 4U, false, false);

      /* End of Outputs for SubSystem: '<S1>/cmd_motor' */
      for (master_code_V3_1_B.i = 0; master_code_V3_1_B.i < 20;
           master_code_V3_1_B.i++) {
        master_code_V3_1_DW.tmp[master_code_V3_1_B.i] =
          master_code_V3_1_B.udpr[master_code_V3_1_B.i];
      }

      /* Event: '<S1>:141' */
      master_code_V3_1_DW.send_flagEventCounter++;
      master_code_V3_1_DW.clock_time_2 = (real_T)
        master_code_V3_1_DW.temporalCounter_i1 * 10.0 +
        master_code_V3_1_DW.clock_time;
    }
  }

  if (master_code_V3_1_DW.send_flagEventCounter > 0U) {
    master_code_V3_1_B.send_flag = !master_code_V3_1_B.send_flag;
    master_code_V3_1_DW.send_flagEventCounter--;
  }

  /* End of Chart: '<Root>/ECU_STATE_MACHINE' */

  /* Gain: '<Root>/Gain3' */
  master_code_V3_1_B.rtb_DataTypeConversion2_tmp = (real32_T)
    master_code_V3_1_P.Gain3_Gain * 7.4505806E-9F;

  /* DataTypeConversion: '<Root>/Data Type Conversion2' incorporates:
   *  Gain: '<Root>/Gain3'
   */
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[0] =
    master_code_V3_1_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V3_1_B.acc_mpu[0];

  /* Gain: '<Root>/Gain4' */
  master_code_V3_1_B.rtb_DataTypeConversi_cl54gopm0x = (real32_T)
    master_code_V3_1_P.Gain4_Gain * 5.96046448E-8F;

  /* DataTypeConversion: '<Root>/Data Type Conversion2' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion1'
   *  DataTypeConversion: '<Root>/Data Type Conversion4'
   *  Gain: '<Root>/Gain3'
   *  Gain: '<Root>/Gain4'
   */
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[3] =
    master_code_V3_1_B.rtb_DataTypeConversi_cl54gopm0x * (real32_T)
    master_code_V3_1_B.gyro_mpu[0];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[1] =
    master_code_V3_1_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V3_1_B.acc_mpu[1];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[4] =
    master_code_V3_1_B.rtb_DataTypeConversi_cl54gopm0x * (real32_T)
    master_code_V3_1_B.gyro_mpu[1];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[2] =
    master_code_V3_1_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V3_1_B.acc_mpu[2];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[5] =
    master_code_V3_1_B.rtb_DataTypeConversi_cl54gopm0x * (real32_T)
    master_code_V3_1_B.gyro_mpu[2];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[6] =
    master_code_V3_1_B.enc_rear[0];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[8] =
    master_code_V3_1_B.enc_front[0];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[7] =
    master_code_V3_1_B.enc_rear[1];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[9] =
    master_code_V3_1_B.enc_front[1];

  /* Gain: '<Root>/Gain1' */
  master_code_V3_1_B.rtb_DataTypeConversion2_tmp = (real32_T)
    master_code_V3_1_P.Gain1_Gain * 7.4505806E-9F;

  /* DataTypeConversion: '<Root>/Data Type Conversion2' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion3'
   *  DataTypeConversion: '<Root>/Data Type Conversion5'
   *  DataTypeConversion: '<Root>/Data Type Conversion6'
   *  Gain: '<Root>/Gain1'
   */
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[10] =
    master_code_V3_1_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V3_1_B.data_imu_raw[0];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[11] =
    master_code_V3_1_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V3_1_B.data_imu_raw[1];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[12] =
    master_code_V3_1_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V3_1_B.data_imu_raw[2];
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[13] =
    master_code_V3_1_B.traction;
  master_code_V3_1_B.DataTypeConversion2_mbvzarwird[14] =
    master_code_V3_1_B.steering;
  for (master_code_V3_1_B.i = 0; master_code_V3_1_B.i < 20; master_code_V3_1_B.i
       ++) {
    master_code_V3_1_B.DataTypeConversion2_mbvzarwird[master_code_V3_1_B.i + 15]
      = master_code_V3_1_B.udpr[master_code_V3_1_B.i];
  }

  /* ToFile: '<Root>/To File' */
  if (tid == 1 ) {
    {
      if (!(++master_code_V3_1_DW.ToFile_IWORK.Decimation % 1) &&
          (master_code_V3_1_DW.ToFile_IWORK.Count * (35 + 1)) + 1 < 100000000 )
      {
        FILE *fp = (FILE *) master_code_V3_1_DW.ToFile_PWORK.FilePtr;
        if (fp != (NULL)) {
          real_T u[35 + 1];
          master_code_V3_1_DW.ToFile_IWORK.Decimation = 0;
          u[0] = ((master_code_V3_1_M->Timing.clockTick1) * 0.01);
          u[1] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[0];
          u[2] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[1];
          u[3] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[2];
          u[4] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[3];
          u[5] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[4];
          u[6] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[5];
          u[7] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[6];
          u[8] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[7];
          u[9] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[8];
          u[10] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[9];
          u[11] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[10];
          u[12] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[11];
          u[13] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[12];
          u[14] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[13];
          u[15] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[14];
          u[16] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[15];
          u[17] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[16];
          u[18] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[17];
          u[19] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[18];
          u[20] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[19];
          u[21] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[20];
          u[22] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[21];
          u[23] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[22];
          u[24] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[23];
          u[25] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[24];
          u[26] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[25];
          u[27] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[26];
          u[28] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[27];
          u[29] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[28];
          u[30] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[29];
          u[31] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[30];
          u[32] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[31];
          u[33] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[32];
          u[34] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[33];
          u[35] = master_code_V3_1_B.DataTypeConversion2_mbvzarwird[34];
          if (fwrite(u, sizeof(real_T), 35 + 1, fp) != 35 + 1) {
            rtmSetErrorStatus(master_code_V3_1_M,
                              "Error writing to MAT-file sensors.mat");
            return;
          }

          if (((++master_code_V3_1_DW.ToFile_IWORK.Count) * (35 + 1))+1 >=
              100000000) {
            (void)fprintf(stdout,
                          "*** The ToFile block will stop logging data before\n"
                          "    the simulation has ended, because it has reached\n"
                          "    the maximum number of elements (100000000)\n"
                          "    allowed in MAT-file sensors.mat.\n");
          }
        }
      }
    }
  }

  /* DataTypeConversion: '<Root>/Data Type Conversion7' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion5'
   *  DataTypeConversion: '<Root>/Data Type Conversion6'
   */
  master_code_V3_1_B.DataTypeConversion8[0] = master_code_V3_1_B.traction;
  master_code_V3_1_B.DataTypeConversion8[1] = master_code_V3_1_B.steering;

  /* ToFile: '<Root>/To File1' */
  if (tid == 1 ) {
    {
      if (!(++master_code_V3_1_DW.ToFile1_IWORK.Decimation % 1) &&
          (master_code_V3_1_DW.ToFile1_IWORK.Count * (2 + 1)) + 1 < 100000000 )
      {
        FILE *fp = (FILE *) master_code_V3_1_DW.ToFile1_PWORK.FilePtr;
        if (fp != (NULL)) {
          real_T u[2 + 1];
          master_code_V3_1_DW.ToFile1_IWORK.Decimation = 0;
          u[0] = ((master_code_V3_1_M->Timing.clockTick1) * 0.01);
          u[1] = master_code_V3_1_B.DataTypeConversion8[0];
          u[2] = master_code_V3_1_B.DataTypeConversion8[1];
          if (fwrite(u, sizeof(real_T), 2 + 1, fp) != 2 + 1) {
            rtmSetErrorStatus(master_code_V3_1_M,
                              "Error writing to MAT-file controls_save.mat");
            return;
          }

          if (((++master_code_V3_1_DW.ToFile1_IWORK.Count) * (2 + 1))+1 >=
              100000000) {
            (void)fprintf(stdout,
                          "*** The ToFile block will stop logging data before\n"
                          "    the simulation has ended, because it has reached\n"
                          "    the maximum number of elements (100000000)\n"
                          "    allowed in MAT-file controls_save.mat.\n");
          }
        }
      }
    }
  }

  /* Outputs for Triggered SubSystem: '<Root>/Triggered Subsystem' incorporates:
   *  TriggerPort: '<S2>/Trigger'
   */
  if (master_code_V3_1_B.send_flag &&
      (master_code_V3_1_PrevZCX.TriggeredSubsystem_Trig_ZCE != POS_ZCSIG)) {
    /* Inport: '<S2>/In1' incorporates:
     *  DataTypeConversion: '<Root>/Data Type Conversion5'
     *  DataTypeConversion: '<Root>/Data Type Conversion6'
     */
    master_code_V3_1_B.In1[0] = master_code_V3_1_B.traction;
    master_code_V3_1_B.In1[1] = master_code_V3_1_B.steering;

    /* DataTypeConversion: '<S2>/Cast To Double' */
    master_code_V3_1_B.CastToDouble[0] = master_code_V3_1_B.In1[0];
    master_code_V3_1_B.CastToDouble[1] = master_code_V3_1_B.In1[1];

    /* Update for S-Function (sdspToNetwork): '<S2>/UDP Send1' */
    sErr = GetErrorBuffer(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U]);
    LibUpdate_Network(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U],
                      &master_code_V3_1_B.CastToDouble[0U], 2);
    if (*sErr != 0) {
      rtmSetErrorStatus(master_code_V3_1_M, sErr);
      rtmSetStopRequested(master_code_V3_1_M, 1);
    }

    /* End of Update for S-Function (sdspToNetwork): '<S2>/UDP Send1' */
  }

  master_code_V3_1_PrevZCX.TriggeredSubsystem_Trig_ZCE =
    master_code_V3_1_B.send_flag;

  /* End of Outputs for SubSystem: '<Root>/Triggered Subsystem' */

  /* DataTypeConversion: '<Root>/Data Type Conversion8' */
  master_code_V3_1_B.DataTypeConversion8[0] = master_code_V3_1_B.In1[0];
  master_code_V3_1_B.DataTypeConversion8[1] = master_code_V3_1_B.In1[1];

  /* ToFile: '<Root>/To File2' */
  if (tid == 1 ) {
    {
      if (!(++master_code_V3_1_DW.ToFile2_IWORK.Decimation % 1) &&
          (master_code_V3_1_DW.ToFile2_IWORK.Count * (2 + 1)) + 1 < 100000000 )
      {
        FILE *fp = (FILE *) master_code_V3_1_DW.ToFile2_PWORK.FilePtr;
        if (fp != (NULL)) {
          real_T u[2 + 1];
          master_code_V3_1_DW.ToFile2_IWORK.Decimation = 0;
          u[0] = ((master_code_V3_1_M->Timing.clockTick1) * 0.01);
          u[1] = master_code_V3_1_B.DataTypeConversion8[0];
          u[2] = master_code_V3_1_B.DataTypeConversion8[1];
          if (fwrite(u, sizeof(real_T), 2 + 1, fp) != 2 + 1) {
            rtmSetErrorStatus(master_code_V3_1_M,
                              "Error writing to MAT-file flag.mat");
            return;
          }

          if (((++master_code_V3_1_DW.ToFile2_IWORK.Count) * (2 + 1))+1 >=
              100000000) {
            (void)fprintf(stdout,
                          "*** The ToFile block will stop logging data before\n"
                          "    the simulation has ended, because it has reached\n"
                          "    the maximum number of elements (100000000)\n"
                          "    allowed in MAT-file flag.mat.\n");
          }
        }
      }
    }
  }

  /* Update absolute time */
  /* The "clockTick1" counts the number of times the code of this task has
   * been executed. The resolution of this integer timer is 0.01, which is the step size
   * of the task. Size of "clockTick1" ensures timer will not overflow during the
   * application lifespan selected.
   */
  master_code_V3_1_M->Timing.clockTick1++;

  /* If subsystem generates rate grouping Output functions,
   * when tid is used in Output function for one rate,
   * all Output functions include tid as a local variable.
   * As result, some Output functions may have unused tid.
   */
  UNUSED_PARAMETER(tid);
}

/* Model step wrapper function for compatibility with a static main program */
void master_code_V3_1_step(int_T tid)
{
  switch (tid) {
   case 0 :
    master_code_V3_1_step0();
    break;

   case 1 :
    master_code_V3_1_step1();
    break;

   default :
    break;
  }
}

/* Model initialize function */
void master_code_V3_1_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)master_code_V3_1_M, 0,
                sizeof(RT_MODEL_master_code_V3_1_T));
  rtmSetTFinal(master_code_V3_1_M, 180.0);
  master_code_V3_1_M->Timing.stepSize0 = 0.001;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    master_code_V3_1_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(master_code_V3_1_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(master_code_V3_1_M->rtwLogInfo, (NULL));
    rtliSetLogT(master_code_V3_1_M->rtwLogInfo, "");
    rtliSetLogX(master_code_V3_1_M->rtwLogInfo, "");
    rtliSetLogXFinal(master_code_V3_1_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(master_code_V3_1_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(master_code_V3_1_M->rtwLogInfo, 4);
    rtliSetLogMaxRows(master_code_V3_1_M->rtwLogInfo, 0);
    rtliSetLogDecimation(master_code_V3_1_M->rtwLogInfo, 1);
    rtliSetLogY(master_code_V3_1_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(master_code_V3_1_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(master_code_V3_1_M->rtwLogInfo, (NULL));
  }

  /* block I/O */
  (void) memset(((void *) &master_code_V3_1_B), 0,
                sizeof(B_master_code_V3_1_T));

  /* states (dwork) */
  (void) memset((void *)&master_code_V3_1_DW, 0,
                sizeof(DW_master_code_V3_1_T));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(master_code_V3_1_M->rtwLogInfo, 0.0,
    rtmGetTFinal(master_code_V3_1_M), master_code_V3_1_M->Timing.stepSize0,
    (&rtmGetErrorStatus(master_code_V3_1_M)));

  {
    char_T *sErr;
    codertarget_raspi__nparbhma5g_T *obj;
    uint32_T i2cname;
    codertarget_raspi_internal_I2_T *obj_0;

    /* Start for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/i2cWr_mpu'
     */
    master_code_i2cWr_mpu_Start(&master_code_V3_1_DW.i2cWr_mpu);

    /* Start for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/calib'
     */
    /* Start for MATLABSystem: '<S3>/I2C Master Read' */
    master_code_V3_1_DW.obj.matlabCodegenIsDeleted = true;
    master_code_V3_1_DW.obj.isInitialized = 0;
    master_code_V3_1_DW.obj.matlabCodegenIsDeleted = false;
    master_code_V3_1_DW.obj.SampleTime =
      master_code_V3_1_P.I2CMasterRead_SampleTime;
    obj = &master_code_V3_1_DW.obj;
    master_code_V3_1_DW.obj.isSetupComplete = false;
    master_code_V3_1_DW.obj.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V3_1_DW.obj.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V3_1_DW.obj.MW_I2C_HANDLE,
                       master_code_V3_1_DW.obj.BusSpeed);
    master_code_V3_1_DW.obj.isSetupComplete = true;

    /* Start for MATLABSystem: '<S3>/I2C Master Read1' */
    master_code_V3_1_DW.obj_iwos115rnw.matlabCodegenIsDeleted = true;
    master_code_V3_1_DW.obj_iwos115rnw.isInitialized = 0;
    master_code_V3_1_DW.obj_iwos115rnw.matlabCodegenIsDeleted = false;
    master_code_V3_1_DW.obj_iwos115rnw.SampleTime =
      master_code_V3_1_P.I2CMasterRead1_SampleTime;
    obj = &master_code_V3_1_DW.obj_iwos115rnw;
    master_code_V3_1_DW.obj_iwos115rnw.isSetupComplete = false;
    master_code_V3_1_DW.obj_iwos115rnw.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V3_1_DW.obj_iwos115rnw.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V3_1_DW.obj_iwos115rnw.MW_I2C_HANDLE,
                       master_code_V3_1_DW.obj_iwos115rnw.BusSpeed);
    master_code_V3_1_DW.obj_iwos115rnw.isSetupComplete = true;

    /* Start for MATLABSystem: '<S3>/I2C Master Read2' */
    master_code_V3_1_DW.obj_i4zcoyvvmy.matlabCodegenIsDeleted = true;
    master_code_V3_1_DW.obj_i4zcoyvvmy.isInitialized = 0;
    master_code_V3_1_DW.obj_i4zcoyvvmy.matlabCodegenIsDeleted = false;
    master_code_V3_1_DW.obj_i4zcoyvvmy.SampleTime =
      master_code_V3_1_P.I2CMasterRead2_SampleTime;
    obj = &master_code_V3_1_DW.obj_i4zcoyvvmy;
    master_code_V3_1_DW.obj_i4zcoyvvmy.isSetupComplete = false;
    master_code_V3_1_DW.obj_i4zcoyvvmy.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V3_1_DW.obj_i4zcoyvvmy.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V3_1_DW.obj_i4zcoyvvmy.MW_I2C_HANDLE,
                       master_code_V3_1_DW.obj_i4zcoyvvmy.BusSpeed);
    master_code_V3_1_DW.obj_i4zcoyvvmy.isSetupComplete = true;

    /* Start for MATLABSystem: '<S3>/I2C Master Read3' */
    master_code_V3_1_DW.obj_gwzo2fxivo.matlabCodegenIsDeleted = true;
    master_code_V3_1_DW.obj_gwzo2fxivo.isInitialized = 0;
    master_code_V3_1_DW.obj_gwzo2fxivo.matlabCodegenIsDeleted = false;
    master_code_V3_1_DW.obj_gwzo2fxivo.SampleTime =
      master_code_V3_1_P.I2CMasterRead3_SampleTime;
    obj = &master_code_V3_1_DW.obj_gwzo2fxivo;
    master_code_V3_1_DW.obj_gwzo2fxivo.isSetupComplete = false;
    master_code_V3_1_DW.obj_gwzo2fxivo.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V3_1_DW.obj_gwzo2fxivo.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V3_1_DW.obj_gwzo2fxivo.MW_I2C_HANDLE,
                       master_code_V3_1_DW.obj_gwzo2fxivo.BusSpeed);
    master_code_V3_1_DW.obj_gwzo2fxivo.isSetupComplete = true;

    /* Start for MATLABSystem: '<S3>/I2C Master Read4' */
    master_code_V3_1_DW.obj_daui2m5pff.matlabCodegenIsDeleted = true;
    master_code_V3_1_DW.obj_daui2m5pff.isInitialized = 0;
    master_code_V3_1_DW.obj_daui2m5pff.matlabCodegenIsDeleted = false;
    master_code_V3_1_DW.obj_daui2m5pff.SampleTime =
      master_code_V3_1_P.I2CMasterRead4_SampleTime;
    obj = &master_code_V3_1_DW.obj_daui2m5pff;
    master_code_V3_1_DW.obj_daui2m5pff.isSetupComplete = false;
    master_code_V3_1_DW.obj_daui2m5pff.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V3_1_DW.obj_daui2m5pff.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V3_1_DW.obj_daui2m5pff.MW_I2C_HANDLE,
                       master_code_V3_1_DW.obj_daui2m5pff.BusSpeed);
    master_code_V3_1_DW.obj_daui2m5pff.isSetupComplete = true;

    /* Start for MATLABSystem: '<S3>/I2C Master Read5' */
    master_code_V3_1_DW.obj_dp4cdr5kdf.matlabCodegenIsDeleted = true;
    master_code_V3_1_DW.obj_dp4cdr5kdf.isInitialized = 0;
    master_code_V3_1_DW.obj_dp4cdr5kdf.matlabCodegenIsDeleted = false;
    master_code_V3_1_DW.obj_dp4cdr5kdf.SampleTime =
      master_code_V3_1_P.I2CMasterRead5_SampleTime;
    obj = &master_code_V3_1_DW.obj_dp4cdr5kdf;
    master_code_V3_1_DW.obj_dp4cdr5kdf.isSetupComplete = false;
    master_code_V3_1_DW.obj_dp4cdr5kdf.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V3_1_DW.obj_dp4cdr5kdf.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V3_1_DW.obj_dp4cdr5kdf.MW_I2C_HANDLE,
                       master_code_V3_1_DW.obj_dp4cdr5kdf.BusSpeed);
    master_code_V3_1_DW.obj_dp4cdr5kdf.isSetupComplete = true;

    /* Start for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/i2cRd'
     */
    master_code_V3__i2cRd_Start(&master_code_V3_1_DW.i2cRd,
      &master_code_V3_1_P.i2cRd);

    /* Start for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/udpRd'
     */
    master_code_V3__udpRd_Start(master_code_V3_1_M, &master_code_V3_1_DW.udpRd,
      &master_code_V3_1_P.udpRd);

    /* Start for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/cmd_motor'
     */
    /* Start for MATLABSystem: '<S4>/I2C Master Write1' */
    master_code_V3_1_DW.obj_n15qv4o1gd.matlabCodegenIsDeleted = true;
    master_code_V3_1_DW.obj_n15qv4o1gd.isInitialized = 0;
    master_code_V3_1_DW.obj_n15qv4o1gd.matlabCodegenIsDeleted = false;
    obj_0 = &master_code_V3_1_DW.obj_n15qv4o1gd;
    master_code_V3_1_DW.obj_n15qv4o1gd.isSetupComplete = false;
    master_code_V3_1_DW.obj_n15qv4o1gd.isInitialized = 1;
    i2cname = 1;
    obj_0->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V3_1_DW.obj_n15qv4o1gd.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V3_1_DW.obj_n15qv4o1gd.MW_I2C_HANDLE,
                       master_code_V3_1_DW.obj_n15qv4o1gd.BusSpeed);
    master_code_V3_1_DW.obj_n15qv4o1gd.isSetupComplete = true;

    /* Start for ToFile: '<Root>/To File' */
    {
      FILE *fp = (NULL);
      char fileName[509] = "sensors.mat";
      if ((fp = fopen(fileName, "wb")) == (NULL)) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error creating .mat file sensors.mat");
        return;
      }

      if (rt_WriteMat4FileHeader(fp, 35 + 1, 0, "sensor")) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error writing mat file header to file sensors.mat");
        return;
      }

      master_code_V3_1_DW.ToFile_IWORK.Count = 0;
      master_code_V3_1_DW.ToFile_IWORK.Decimation = -1;
      master_code_V3_1_DW.ToFile_PWORK.FilePtr = fp;
    }

    /* Start for ToFile: '<Root>/To File1' */
    {
      FILE *fp = (NULL);
      char fileName[509] = "controls_save.mat";
      if ((fp = fopen(fileName, "wb")) == (NULL)) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error creating .mat file controls_save.mat");
        return;
      }

      if (rt_WriteMat4FileHeader(fp, 2 + 1, 0, "controls_save")) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error writing mat file header to file controls_save.mat");
        return;
      }

      master_code_V3_1_DW.ToFile1_IWORK.Count = 0;
      master_code_V3_1_DW.ToFile1_IWORK.Decimation = -1;
      master_code_V3_1_DW.ToFile1_PWORK.FilePtr = fp;
    }

    /* Start for Triggered SubSystem: '<Root>/Triggered Subsystem' */
    /* Start for S-Function (sdspToNetwork): '<S2>/UDP Send1' */
    sErr = GetErrorBuffer(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U]);
    CreateUDPInterface(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U]);
    if (*sErr == 0) {
      LibCreate_Network(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U], 1,
                        "0.0.0.0", -1, "10.0.0.13",
                        master_code_V3_1_P.UDPSend1_remotePort, 8192, 8, 0);
    }

    if (*sErr == 0) {
      LibStart(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U]);
    }

    if (*sErr != 0) {
      DestroyUDPInterface(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U]);
      if (*sErr != 0) {
        rtmSetErrorStatus(master_code_V3_1_M, sErr);
        rtmSetStopRequested(master_code_V3_1_M, 1);
      }
    }

    /* End of Start for S-Function (sdspToNetwork): '<S2>/UDP Send1' */
    /* End of Start for SubSystem: '<Root>/Triggered Subsystem' */

    /* Start for ToFile: '<Root>/To File2' */
    {
      FILE *fp = (NULL);
      char fileName[509] = "flag.mat";
      if ((fp = fopen(fileName, "wb")) == (NULL)) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error creating .mat file flag.mat");
        return;
      }

      if (rt_WriteMat4FileHeader(fp, 2 + 1, 0, "flag")) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error writing mat file header to file flag.mat");
        return;
      }

      master_code_V3_1_DW.ToFile2_IWORK.Count = 0;
      master_code_V3_1_DW.ToFile2_IWORK.Decimation = -1;
      master_code_V3_1_DW.ToFile2_PWORK.FilePtr = fp;
    }

    master_code_V3_1_PrevZCX.TriggeredSubsystem_Trig_ZCE = POS_ZCSIG;
    master_code_V3_1_DW.is_Running = master_code__IN_NO_ACTIVE_CHILD;
    master_code_V3_1_DW.temporalCounter_i2 = 0U;
    master_code_V3_1_DW.temporalCounter_i1 = 0U;
    master_code_V3_1_DW.is_active_c3_master_code_V3_1 = 0U;
    master_code_V3_1_DW.is_c3_master_code_V3_1 = master_code__IN_NO_ACTIVE_CHILD;
    master_code_V3_1_DW.send_flagEventCounter = 0U;
    master_code_V3_1_B.send_flag = false;

    /* SystemInitialize for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/calib'
     */
    /* InitializeConditions for S-Function (sdspmdn2): '<S3>/Median' */
    master_code_V3_1_DW.Median_Index = 0U;

    /* InitializeConditions for S-Function (sdspmdn2): '<S3>/Median1' */
    master_code_V3_1_DW.Median1_Index = 0U;

    /* InitializeConditions for S-Function (sdspmdn2): '<S3>/Median2' */
    master_code_V3_1_DW.Median2_Index = 0U;

    /* InitializeConditions for S-Function (sdspmdn2): '<S3>/Median3' */
    master_code_V3_1_DW.Median3_Index = 0U;

    /* InitializeConditions for S-Function (sdspmdn2): '<S3>/Median4' */
    master_code_V3_1_DW.Median4_Index = 0U;

    /* InitializeConditions for S-Function (sdspmdn2): '<S3>/Median5' */
    master_code_V3_1_DW.Median5_Index = 0U;

    /* SystemInitialize for Outport: '<S3>/acc_x_calib' */
    master_code_V3_1_B.DataTypeConversion = master_code_V3_1_P.acc_x_calib_Y0;

    /* SystemInitialize for Outport: '<S3>/acc_y_calib' */
    master_code_V3_1_B.DataTypeConversion1 = master_code_V3_1_P.acc_y_calib_Y0;

    /* SystemInitialize for Outport: '<S3>/acc_z_calib' */
    master_code_V3_1_B.DataTypeConversion2 = master_code_V3_1_P.acc_z_calib_Y0;

    /* SystemInitialize for Outport: '<S3>/gyro_x_calib' */
    master_code_V3_1_B.DataTypeConversion3 = master_code_V3_1_P.gyro_x_calib_Y0;

    /* SystemInitialize for Outport: '<S3>/gyro_y_calib' */
    master_code_V3_1_B.DataTypeConversion4 = master_code_V3_1_P.gyro_y_calib_Y0;

    /* SystemInitialize for Outport: '<S3>/gyro_z_calib' */
    master_code_V3_1_B.DataTypeConversion5 = master_code_V3_1_P.gyro_z_calib_Y0;

    /* SystemInitialize for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/i2cRd'
     */
    master_code_V3_1_i2cRd_Init(&master_code_V3_1_B.i2cRd,
      &master_code_V3_1_P.i2cRd);

    /* SystemInitialize for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/udpRd'
     */
    master_code_V3_1_udpRd_Init(&master_code_V3_1_B.udpRd,
      &master_code_V3_1_P.udpRd);

    /* SystemInitialize for Triggered SubSystem: '<Root>/Triggered Subsystem' */
    /* SystemInitialize for Outport: '<S2>/Out1' */
    master_code_V3_1_B.In1[0] = master_code_V3_1_P.Out1_Y0;
    master_code_V3_1_B.In1[1] = master_code_V3_1_P.Out1_Y0;

    /* End of SystemInitialize for SubSystem: '<Root>/Triggered Subsystem' */
  }
}

/* Model terminate function */
void master_code_V3_1_terminate(void)
{
  char_T *sErr;

  /* Terminate for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  SubSystem: '<S1>/i2cWr_mpu'
   */
  master_code__i2cWr_mpu_Term(&master_code_V3_1_DW.i2cWr_mpu);

  /* Terminate for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  SubSystem: '<S1>/calib'
   */
  /* Terminate for MATLABSystem: '<S3>/I2C Master Read' */
  matlabCodegenHandle__eh23cloj25(&master_code_V3_1_DW.obj);

  /* Terminate for MATLABSystem: '<S3>/I2C Master Read1' */
  matlabCodegenHandle__eh23cloj25(&master_code_V3_1_DW.obj_iwos115rnw);

  /* Terminate for MATLABSystem: '<S3>/I2C Master Read2' */
  matlabCodegenHandle__eh23cloj25(&master_code_V3_1_DW.obj_i4zcoyvvmy);

  /* Terminate for MATLABSystem: '<S3>/I2C Master Read3' */
  matlabCodegenHandle__eh23cloj25(&master_code_V3_1_DW.obj_gwzo2fxivo);

  /* Terminate for MATLABSystem: '<S3>/I2C Master Read4' */
  matlabCodegenHandle__eh23cloj25(&master_code_V3_1_DW.obj_daui2m5pff);

  /* Terminate for MATLABSystem: '<S3>/I2C Master Read5' */
  matlabCodegenHandle__eh23cloj25(&master_code_V3_1_DW.obj_dp4cdr5kdf);

  /* Terminate for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  SubSystem: '<S1>/i2cRd'
   */
  master_code_V3_1_i2cRd_Term(&master_code_V3_1_DW.i2cRd);

  /* Terminate for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  SubSystem: '<S1>/udpRd'
   */
  master_code_V3_1_udpRd_Term(master_code_V3_1_M, &master_code_V3_1_DW.udpRd);

  /* Terminate for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  SubSystem: '<S1>/cmd_motor'
   */
  /* Terminate for MATLABSystem: '<S4>/I2C Master Write1' */
  mast_matlabCodegenHa_kadk1bn3pp(&master_code_V3_1_DW.obj_n15qv4o1gd);

  /* Terminate for ToFile: '<Root>/To File' */
  {
    FILE *fp = (FILE *) master_code_V3_1_DW.ToFile_PWORK.FilePtr;
    if (fp != (NULL)) {
      char fileName[509] = "sensors.mat";
      if (fclose(fp) == EOF) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error closing MAT-file sensors.mat");
        return;
      }

      if ((fp = fopen(fileName, "r+b")) == (NULL)) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error reopening MAT-file sensors.mat");
        return;
      }

      if (rt_WriteMat4FileHeader(fp, 35 + 1,
           master_code_V3_1_DW.ToFile_IWORK.Count, "sensor")) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error writing header for sensor to MAT-file sensors.mat");
      }

      if (fclose(fp) == EOF) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error closing MAT-file sensors.mat");
        return;
      }

      master_code_V3_1_DW.ToFile_PWORK.FilePtr = (NULL);
    }
  }

  /* Terminate for ToFile: '<Root>/To File1' */
  {
    FILE *fp = (FILE *) master_code_V3_1_DW.ToFile1_PWORK.FilePtr;
    if (fp != (NULL)) {
      char fileName[509] = "controls_save.mat";
      if (fclose(fp) == EOF) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error closing MAT-file controls_save.mat");
        return;
      }

      if ((fp = fopen(fileName, "r+b")) == (NULL)) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error reopening MAT-file controls_save.mat");
        return;
      }

      if (rt_WriteMat4FileHeader(fp, 2 + 1,
           master_code_V3_1_DW.ToFile1_IWORK.Count, "controls_save")) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error writing header for controls_save to MAT-file controls_save.mat");
      }

      if (fclose(fp) == EOF) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error closing MAT-file controls_save.mat");
        return;
      }

      master_code_V3_1_DW.ToFile1_PWORK.FilePtr = (NULL);
    }
  }

  /* Terminate for Triggered SubSystem: '<Root>/Triggered Subsystem' */
  /* Terminate for S-Function (sdspToNetwork): '<S2>/UDP Send1' */
  sErr = GetErrorBuffer(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U]);
  LibTerminate(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U]);
  if (*sErr != 0) {
    rtmSetErrorStatus(master_code_V3_1_M, sErr);
    rtmSetStopRequested(master_code_V3_1_M, 1);
  }

  LibDestroy(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U], 1);
  DestroyUDPInterface(&master_code_V3_1_DW.UDPSend1_NetworkLib[0U]);

  /* End of Terminate for S-Function (sdspToNetwork): '<S2>/UDP Send1' */
  /* End of Terminate for SubSystem: '<Root>/Triggered Subsystem' */

  /* Terminate for ToFile: '<Root>/To File2' */
  {
    FILE *fp = (FILE *) master_code_V3_1_DW.ToFile2_PWORK.FilePtr;
    if (fp != (NULL)) {
      char fileName[509] = "flag.mat";
      if (fclose(fp) == EOF) {
        rtmSetErrorStatus(master_code_V3_1_M, "Error closing MAT-file flag.mat");
        return;
      }

      if ((fp = fopen(fileName, "r+b")) == (NULL)) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error reopening MAT-file flag.mat");
        return;
      }

      if (rt_WriteMat4FileHeader(fp, 2 + 1,
           master_code_V3_1_DW.ToFile2_IWORK.Count, "flag")) {
        rtmSetErrorStatus(master_code_V3_1_M,
                          "Error writing header for flag to MAT-file flag.mat");
      }

      if (fclose(fp) == EOF) {
        rtmSetErrorStatus(master_code_V3_1_M, "Error closing MAT-file flag.mat");
        return;
      }

      master_code_V3_1_DW.ToFile2_PWORK.FilePtr = (NULL);
    }
  }
}
