/*
 * master_code_V3_1_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "master_code_V3_1".
 *
 * Model version              : 1.313
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Mon Mar  2 16:28:09 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_master_code_V3_1_types_h_
#define RTW_HEADER_master_code_V3_1_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"

/* Custom Type definition for MATLABSystem: '<S6>/I2C Master Write' */
#include "MW_SVD.h"
#include <stddef.h>
#ifndef typedef_codertarget_raspi_internal_Ha_T
#define typedef_codertarget_raspi_internal_Ha_T

typedef struct {
  int32_T __dummy;
} codertarget_raspi_internal_Ha_T;

#endif                                 /*typedef_codertarget_raspi_internal_Ha_T*/

#ifndef typedef_codertarget_raspi_internal_I2_T
#define typedef_codertarget_raspi_internal_I2_T

typedef struct {
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  codertarget_raspi_internal_Ha_T Hw;
  uint32_T BusSpeed;
  MW_Handle_Type MW_I2C_HANDLE;
} codertarget_raspi_internal_I2_T;

#endif                                 /*typedef_codertarget_raspi_internal_I2_T*/

#ifndef typedef_codertarget_raspi__nparbhma5g_T
#define typedef_codertarget_raspi__nparbhma5g_T

typedef struct {
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  codertarget_raspi_internal_Ha_T Hw;
  uint32_T BusSpeed;
  MW_Handle_Type MW_I2C_HANDLE;
  real_T SampleTime;
} codertarget_raspi__nparbhma5g_T;

#endif                                 /*typedef_codertarget_raspi__nparbhma5g_T*/

/* Parameters for system: '<S1>/i2cRd' */
typedef struct P_i2cRd_master_code_V3_1_T_ P_i2cRd_master_code_V3_1_T;

/* Parameters for system: '<S1>/udpRd' */
typedef struct P_udpRd_master_code_V3_1_T_ P_udpRd_master_code_V3_1_T;

/* Parameters (default storage) */
typedef struct P_master_code_V3_1_T_ P_master_code_V3_1_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_master_code_V3_1_T RT_MODEL_master_code_V3_1_T;

#endif                                 /* RTW_HEADER_master_code_V3_1_types_h_ */
