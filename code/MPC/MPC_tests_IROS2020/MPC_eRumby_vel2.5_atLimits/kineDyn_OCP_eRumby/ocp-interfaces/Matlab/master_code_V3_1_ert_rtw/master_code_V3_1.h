/*
 * master_code_V3_1.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "master_code_V3_1".
 *
 * Model version              : 1.313
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Mon Mar  2 16:28:09 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_master_code_V3_1_h_
#define RTW_HEADER_master_code_V3_1_h_
#include <stddef.h>
#include <math.h>
#include <string.h>
#include <float.h>
#ifndef master_code_V3_1_COMMON_INCLUDES_
# define master_code_V3_1_COMMON_INCLUDES_
#include <stdio.h>
#include <string.h>
#include "rtwtypes.h"
#include "zero_crossing_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#include "MW_I2C.h"
#include "DAHostLib_Network.h"
#endif                                 /* master_code_V3_1_COMMON_INCLUDES_ */

#include "master_code_V3_1_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWLogInfo
# define rtmGetRTWLogInfo(rtm)         ((rtm)->rtwLogInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmStepTask
# define rtmStepTask(rtm, idx)         ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               (&(rtm)->Timing.taskTime0)
#endif

#ifndef rtmTaskCounter
# define rtmTaskCounter(rtm, idx)      ((rtm)->Timing.TaskCounters.TID[(idx)])
#endif

/* Block states (default storage) for system '<S1>/i2cWr_mpu' */
typedef struct {
  codertarget_raspi_internal_I2_T obj; /* '<S6>/I2C Master Write' */
} DW_i2cWr_mpu_master_code_V3_1_T;

/* Block signals for system '<S1>/i2cRd' */
typedef struct {
  int16_T DataTypeConversion4[7];      /* '<S5>/Data Type Conversion4' */
  int16_T DataTypeConversion1[2];      /* '<S5>/Data Type Conversion1' */
  int16_T DataTypeConversion6[2];      /* '<S5>/Data Type Conversion6' */
} B_i2cRd_master_code_V3_1_T;

/* Block states (default storage) for system '<S1>/i2cRd' */
typedef struct {
  codertarget_raspi__nparbhma5g_T obj; /* '<S5>/I2C Master Read4' */
  codertarget_raspi__nparbhma5g_T obj_mxsciaoufc;/* '<S5>/I2C Master Read1' */
  codertarget_raspi__nparbhma5g_T obj_dz0rzkn42o;/* '<S5>/I2C Master Read6' */
} DW_i2cRd_master_code_V3_1_T;

/* Block signals for system '<S1>/udpRd' */
typedef struct {
  int16_T UDPReceive_o1[20];           /* '<S7>/UDP Receive' */
} B_udpRd_master_code_V3_1_T;

/* Block states (default storage) for system '<S1>/udpRd' */
typedef struct {
  real_T UDPReceive_NetworkLib[137];   /* '<S7>/UDP Receive' */
} DW_udpRd_master_code_V3_1_T;

/* Block signals (default storage) */
typedef struct {
  real_T DataTypeConversion2_mbvzarwird[35];/* '<Root>/Data Type Conversion2' */
  real_T CastToDouble[2];              /* '<S2>/Cast To Double' */
  real_T DataTypeConversion8[2];       /* '<Root>/Data Type Conversion8' */
  int16_T data_imu_mpu[7];
  real_T SFunction_o12;                /* '<Root>/ECU_STATE_MACHINE' */
  real32_T In1[2];                     /* '<S2>/In1' */
  int16_T acc_mpu[3];                  /* '<Root>/ECU_STATE_MACHINE' */
  int16_T gyro_mpu[3];                 /* '<Root>/ECU_STATE_MACHINE' */
  uint8_T SwappedDataBytes[4];
  real32_T rtb_DataTypeConversion2_tmp;
  real32_T rtb_DataTypeConversi_cl54gopm0x;
  int32_T i;
  int32_T mid;
  int32_T ctidx;
  uint32_T sIdx;
  uint32_T t;
  uint32_T sIdx_kkiq3xxxve;
  uint32_T t_cxarnvbvui;
  uint16_T b_x[2];
  int16_T enc_rear[2];                 /* '<Root>/ECU_STATE_MACHINE' */
  int16_T enc_front[2];                /* '<Root>/ECU_STATE_MACHINE' */
  int16_T data_imu_raw[3];             /* '<Root>/ECU_STATE_MACHINE' */
  uint8_T output_raw[2];
  uint8_T b_x_bhxxfovxdy[2];
  uint16_T dataw;                      /* '<Root>/ECU_STATE_MACHINE' */
  int16_T traction;                    /* '<Root>/ECU_STATE_MACHINE' */
  int16_T steering;                    /* '<Root>/ECU_STATE_MACHINE' */
  int16_T udpr[20];                    /* '<Root>/ECU_STATE_MACHINE' */
  int16_T esc_cmd;                     /* '<Root>/ECU_STATE_MACHINE' */
  int16_T servo_cmd;                   /* '<Root>/ECU_STATE_MACHINE' */
  int16_T DataTypeConversion;          /* '<S3>/Data Type Conversion' */
  int16_T DataTypeConversion1;         /* '<S3>/Data Type Conversion1' */
  int16_T DataTypeConversion2;         /* '<S3>/Data Type Conversion2' */
  int16_T DataTypeConversion3;         /* '<S3>/Data Type Conversion3' */
  int16_T DataTypeConversion4;         /* '<S3>/Data Type Conversion4' */
  int16_T DataTypeConversion5;         /* '<S3>/Data Type Conversion5' */
  uint16_T b_output;
  uint16_T I2CMasterRead5;             /* '<S3>/I2C Master Read5' */
  uint16_T I2CMasterRead4;             /* '<S3>/I2C Master Read4' */
  uint16_T I2CMasterRead3;             /* '<S3>/I2C Master Read3' */
  uint16_T I2CMasterRead2;             /* '<S3>/I2C Master Read2' */
  uint16_T I2CMasterRead1;             /* '<S3>/I2C Master Read1' */
  uint16_T I2CMasterRead;              /* '<S3>/I2C Master Read' */
  uint16_T tmp0;
  boolean_T send_flag;                 /* '<Root>/ECU_STATE_MACHINE' */
  B_udpRd_master_code_V3_1_T udpRd;    /* '<S1>/udpRd' */
  B_i2cRd_master_code_V3_1_T i2cRd;    /* '<S1>/i2cRd' */
} B_master_code_V3_1_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  codertarget_raspi__nparbhma5g_T obj; /* '<S3>/I2C Master Read' */
  codertarget_raspi__nparbhma5g_T obj_iwos115rnw;/* '<S3>/I2C Master Read1' */
  codertarget_raspi__nparbhma5g_T obj_i4zcoyvvmy;/* '<S3>/I2C Master Read2' */
  codertarget_raspi__nparbhma5g_T obj_gwzo2fxivo;/* '<S3>/I2C Master Read3' */
  codertarget_raspi__nparbhma5g_T obj_daui2m5pff;/* '<S3>/I2C Master Read4' */
  codertarget_raspi__nparbhma5g_T obj_dp4cdr5kdf;/* '<S3>/I2C Master Read5' */
  codertarget_raspi_internal_I2_T obj_n15qv4o1gd;/* '<S4>/I2C Master Write1' */
  real_T UDPSend1_NetworkLib[137];     /* '<S2>/UDP Send1' */
  real_T clock_time;                   /* '<Root>/ECU_STATE_MACHINE' */
  real_T clock_time_2;                 /* '<Root>/ECU_STATE_MACHINE' */
  struct {
    void *FilePtr;
  } ToFile_PWORK;                      /* '<Root>/To File' */

  struct {
    void *FilePtr;
  } ToFile1_PWORK;                     /* '<Root>/To File1' */

  struct {
    void *FilePtr;
  } ToFile2_PWORK;                     /* '<Root>/To File2' */

  uint32_T send_flagEventCounter;      /* '<Root>/ECU_STATE_MACHINE' */
  uint32_T temporalCounter_i1;         /* '<Root>/ECU_STATE_MACHINE' */
  uint32_T temporalCounter_i2;         /* '<Root>/ECU_STATE_MACHINE' */
  uint32_T Median_Index;               /* '<S3>/Median' */
  uint32_T Median1_Index;              /* '<S3>/Median1' */
  uint32_T Median2_Index;              /* '<S3>/Median2' */
  uint32_T Median3_Index;              /* '<S3>/Median3' */
  uint32_T Median4_Index;              /* '<S3>/Median4' */
  uint32_T Median5_Index;              /* '<S3>/Median5' */
  struct {
    int_T Count;
    int_T Decimation;
  } ToFile_IWORK;                      /* '<Root>/To File' */

  struct {
    int_T Count;
    int_T Decimation;
  } ToFile1_IWORK;                     /* '<Root>/To File1' */

  struct {
    int_T Count;
    int_T Decimation;
  } ToFile2_IWORK;                     /* '<Root>/To File2' */

  int16_T acc_x_calib;                 /* '<Root>/ECU_STATE_MACHINE' */
  int16_T acc_z_calib;                 /* '<Root>/ECU_STATE_MACHINE' */
  int16_T acc_y_calib;                 /* '<Root>/ECU_STATE_MACHINE' */
  int16_T gyro_x_calib;                /* '<Root>/ECU_STATE_MACHINE' */
  int16_T gyro_y_calib;                /* '<Root>/ECU_STATE_MACHINE' */
  int16_T gyro_z_calib;                /* '<Root>/ECU_STATE_MACHINE' */
  int16_T tmp[20];                     /* '<Root>/ECU_STATE_MACHINE' */
  int16_T i;                           /* '<Root>/ECU_STATE_MACHINE' */
  int16_T interp_flag;                 /* '<Root>/ECU_STATE_MACHINE' */
  uint8_T is_active_c3_master_code_V3_1;/* '<Root>/ECU_STATE_MACHINE' */
  uint8_T is_c3_master_code_V3_1;      /* '<Root>/ECU_STATE_MACHINE' */
  uint8_T is_Running;                  /* '<Root>/ECU_STATE_MACHINE' */
  DW_udpRd_master_code_V3_1_T udpRd;   /* '<S1>/udpRd' */
  DW_i2cRd_master_code_V3_1_T i2cRd;   /* '<S1>/i2cRd' */
  DW_i2cWr_mpu_master_code_V3_1_T i2cWr_mpu;/* '<S1>/i2cWr_mpu' */
} DW_master_code_V3_1_T;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState TriggeredSubsystem_Trig_ZCE;/* '<Root>/Triggered Subsystem' */
} PrevZCX_master_code_V3_1_T;

/* Parameters for system: '<S1>/i2cRd' */
struct P_i2cRd_master_code_V3_1_T_ {
  real_T I2CMasterRead6_SampleTime;    /* Expression: -1
                                        * Referenced by: '<S5>/I2C Master Read6'
                                        */
  real_T I2CMasterRead1_SampleTime;    /* Expression: -1
                                        * Referenced by: '<S5>/I2C Master Read1'
                                        */
  real_T I2CMasterRead4_SampleTime;    /* Expression: -1
                                        * Referenced by: '<S5>/I2C Master Read4'
                                        */
  int16_T data_imu_Y0;                 /* Computed Parameter: data_imu_Y0
                                        * Referenced by: '<S5>/data_imu'
                                        */
  int16_T data_enc_rear_Y0;            /* Computed Parameter: data_enc_rear_Y0
                                        * Referenced by: '<S5>/data_enc_rear'
                                        */
  int16_T data_enc_front_Y0;           /* Computed Parameter: data_enc_front_Y0
                                        * Referenced by: '<S5>/data_enc_front'
                                        */
};

/* Parameters for system: '<S1>/udpRd' */
struct P_udpRd_master_code_V3_1_T_ {
  int32_T UDPReceive_Port;             /* Computed Parameter: UDPReceive_Port
                                        * Referenced by: '<S7>/UDP Receive'
                                        */
  int16_T udpr_Y0;                     /* Computed Parameter: udpr_Y0
                                        * Referenced by: '<S7>/udpr'
                                        */
};

/* Parameters (default storage) */
struct P_master_code_V3_1_T_ {
  int32_T UDPSend1_remotePort;         /* Mask Parameter: UDPSend1_remotePort
                                        * Referenced by: '<S2>/UDP Send1'
                                        */
  real_T I2CMasterRead5_SampleTime;    /* Expression: -1
                                        * Referenced by: '<S3>/I2C Master Read5'
                                        */
  real_T I2CMasterRead4_SampleTime;    /* Expression: -1
                                        * Referenced by: '<S3>/I2C Master Read4'
                                        */
  real_T I2CMasterRead3_SampleTime;    /* Expression: -1
                                        * Referenced by: '<S3>/I2C Master Read3'
                                        */
  real_T I2CMasterRead2_SampleTime;    /* Expression: -1
                                        * Referenced by: '<S3>/I2C Master Read2'
                                        */
  real_T I2CMasterRead1_SampleTime;    /* Expression: -1
                                        * Referenced by: '<S3>/I2C Master Read1'
                                        */
  real_T I2CMasterRead_SampleTime;     /* Expression: -1
                                        * Referenced by: '<S3>/I2C Master Read'
                                        */
  real32_T Out1_Y0;                    /* Computed Parameter: Out1_Y0
                                        * Referenced by: '<S2>/Out1'
                                        */
  int16_T acc_x_calib_Y0;              /* Computed Parameter: acc_x_calib_Y0
                                        * Referenced by: '<S3>/acc_x_calib'
                                        */
  int16_T acc_y_calib_Y0;              /* Computed Parameter: acc_y_calib_Y0
                                        * Referenced by: '<S3>/acc_y_calib'
                                        */
  int16_T acc_z_calib_Y0;              /* Computed Parameter: acc_z_calib_Y0
                                        * Referenced by: '<S3>/acc_z_calib'
                                        */
  int16_T gyro_x_calib_Y0;             /* Computed Parameter: gyro_x_calib_Y0
                                        * Referenced by: '<S3>/gyro_x_calib'
                                        */
  int16_T gyro_y_calib_Y0;             /* Computed Parameter: gyro_y_calib_Y0
                                        * Referenced by: '<S3>/gyro_y_calib'
                                        */
  int16_T gyro_z_calib_Y0;             /* Computed Parameter: gyro_z_calib_Y0
                                        * Referenced by: '<S3>/gyro_z_calib'
                                        */
  int16_T Gain4_Gain;                  /* Computed Parameter: Gain4_Gain
                                        * Referenced by: '<Root>/Gain4'
                                        */
  int16_T Gain3_Gain;                  /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<Root>/Gain3'
                                        */
  int16_T Gain1_Gain;                  /* Computed Parameter: Gain1_Gain
                                        * Referenced by: '<Root>/Gain1'
                                        */
  P_udpRd_master_code_V3_1_T udpRd;    /* '<S1>/udpRd' */
  P_i2cRd_master_code_V3_1_T i2cRd;    /* '<S1>/i2cRd' */
};

/* Real-time Model Data Structure */
struct tag_RTM_master_code_V3_1_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    struct {
      uint8_T TID[2];
    } TaskCounters;

    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (default storage) */
extern P_master_code_V3_1_T master_code_V3_1_P;

/* Block signals (default storage) */
extern B_master_code_V3_1_T master_code_V3_1_B;

/* Block states (default storage) */
extern DW_master_code_V3_1_T master_code_V3_1_DW;

/* External function called from main */
extern void master_code_V3_1_SetEventsForThisBaseStep(boolean_T *eventFlags);
extern void rate_scheduler(void);

/* Model entry point functions */
extern void master_code_V3_1_SetEventsForThisBaseStep(boolean_T *eventFlags);
extern void master_code_V3_1_initialize(void);
extern void master_code_V3_1_step(int_T tid);
extern void master_code_V3_1_terminate(void);

/* Real-time Model object */
extern RT_MODEL_master_code_V3_1_T *const master_code_V3_1_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'master_code_V3_1'
 * '<S1>'   : 'master_code_V3_1/ECU_STATE_MACHINE'
 * '<S2>'   : 'master_code_V3_1/Triggered Subsystem'
 * '<S3>'   : 'master_code_V3_1/ECU_STATE_MACHINE/calib'
 * '<S4>'   : 'master_code_V3_1/ECU_STATE_MACHINE/cmd_motor'
 * '<S5>'   : 'master_code_V3_1/ECU_STATE_MACHINE/i2cRd'
 * '<S6>'   : 'master_code_V3_1/ECU_STATE_MACHINE/i2cWr_mpu'
 * '<S7>'   : 'master_code_V3_1/ECU_STATE_MACHINE/udpRd'
 * '<S8>'   : 'master_code_V3_1/ECU_STATE_MACHINE/udpSd'
 */
#endif                                 /* RTW_HEADER_master_code_V3_1_h_ */
