% -----------------------
%% Record the video for the MPC replanning -- video of the real vehicle
% -----------------------

clearvars; close all; clc;
enable_docked = 0;
set(0,'DefaultFigureWindowStyle','docked')


%% Load files
load('./Calibration_data/CAM_2/camera_matrix.mat');
load('centerl_line_Pergine_world_points.mat');
MHT_traject = load('../../../../../MPC_test_data/MHT_data_4video');

v = VideoReader('CAM2.mp4');
% img = imread('track_frame.png'); % screenshot of the field


%% Create the new video 

time_frame_video  = 1/30;  % [s] -> the video will run at 1/time_frame fps
time_frame_replan = 1/10;  % [s]
start_frame = 30;
start_MPC_step = 138;

% -----------------------
% Initialize the figure for the video
% -----------------------
if (~enable_docked)
    figure('Name','MPC video','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[50,50,1200,860]) 
else
    figure('Name','MPC video','NumberTitle','off'), clf
end

frame = read(v,start_frame);
hh1 = image(frame);
hold on

xCoMCar = MHT_traject.MHT.xCoMFull(:,start_MPC_step);
yCoMCar = MHT_traject.MHT.yCoMFull(:,start_MPC_step);
% Convert world points to image points
worldPoints = [xCoMCar';yCoMCar'];
imagePoints = homography2d_project(inv(camera_matrix),worldPoints);
hh2 = plot(imagePoints(:,1),imagePoints(:,2),'yo','MarkerFaceColor','y','MarkerSize',6,'HandleVisibility','off');
% return

vidfile = VideoWriter('MPC_traj_realVehicle_CAM2.mp4','MPEG-4');
vidfile.FrameRate = 1/time_frame_video;
open(vidfile);

jj = 1;
kk = 1;
totNumFrames = v.NumFrames;
for i = start_frame:totNumFrames
    frame = read(v,i);
    hh1 = imshow(frame);
    
    xCoMCar = MHT_traject.MHT.xCoMFull(:,start_MPC_step+kk);
    yCoMCar = MHT_traject.MHT.yCoMFull(:,start_MPC_step+kk);
    % Convert world points to image points
    worldPoints = [xCoMCar';yCoMCar'];
    imagePoints = homography2d_project(inv(camera_matrix),worldPoints);
    if (mod(jj,3)==0)
        start_idx = 6;
        % Update the MPC solution to be plotted
        if (kk+start_MPC_step<size(MHT_traject.MHT.xCoMFull,2)-1)
            kk = kk+1;
        else
            break;
        end
    elseif (mod(jj,3)==2)
        start_idx = 5;
    else
        start_idx = 4; 
    end
    hh2 = plot(imagePoints(start_idx:end,1),imagePoints(start_idx:end,2),'yo','MarkerFaceColor','y','MarkerSize',8,'HandleVisibility','off');
        
%     if (mod(jj,3)==0)
%         % Update the MPC solution to be plotted
%         kk = kk+1;
%     end
    
%     drawnow
    
    F(jj) = getframe(gca);
    writeVideo(vidfile,F(jj));
    jj = jj+1;
    
    delete(hh1);
    delete(hh2);
end
hold off

close(vidfile)

% figure
% image(frame);



%%
% figure(2);
% image_plot = imshow(img);
% hold on
% scatter(imagePoints(:,1),imagePoints(:,2),20,'y','filled');
% delete(image_plot);
% set(gca, 'color', 'none')


