void draw_trajectory(FloatList traj_x, FloatList traj_y, int plN) {
  // draw trajectory
  if (trajState) {
    traj_draw = createShape(); // define trajectory vertex
    traj_draw.beginShape();
    traj_draw.stroke(red_s(plN),0,blue_s(plN)); // call sub-functions
    traj_draw.strokeWeight(2);
    traj_draw.noFill();
    for (int t=0; t<traj_x.size(); t++) {
      traj_draw.vertex(traj_x.get(t)*W + offX, traj_y.get(t)*H + offY);
    }
    traj_draw.endShape();
    shape(traj_draw);
  }
  
  // draw circle
  if (circleState) {
    circle_x = traj_x.get(int((traj_x.size()-1)*cursor)); // get normalizated index (0-1) multiplicated by the array length
    circle_y = traj_y.get(int((traj_x.size()-1)*cursor));
    stroke(255);
    strokeWeight(2);
    if (plN < 12) fill(250,100,0);
    else fill(100,130,255);
    ellipse(circle_x*W + offX, circle_y*H + offX, 10, 10);
  }
}

int red_s(int playerN) {
  int red_q;
  if (playerN < 12){
    red_q  = int(255 - ((255/11) * playerN));
  } else {
    red_q  = 0;
  }
  return red_q;
}

int blue_s(int playerN) { 
  int blue_q;
  if (playerN < 12) {
    blue_q = 0;
  } else {
    blue_q = int(255 - ((255/11) * (playerN - 11)));
  }
  return blue_q;
}

void activate_trajectory() {
  if (button1) call_trajectory(1);
  if (button2) call_trajectory(2);
  if (button3) call_trajectory(3);
  if (button4) call_trajectory(4);
  if (button5) call_trajectory(5);
  if (button6) call_trajectory(6);
  if (button7) call_trajectory(7);
  if (button8) call_trajectory(8);
  if (button9) call_trajectory(9);
  if (button10) call_trajectory(10);
  if (button11) call_trajectory(11);
  if (button12) call_trajectory(12);
  if (button13) call_trajectory(13);
  if (button14) call_trajectory(14);
  if (button15) call_trajectory(15);
  if (button16) call_trajectory(16);
  if (button17) call_trajectory(17);
  if (button18) call_trajectory(18);
  if (button19) call_trajectory(19);
  if (button20) call_trajectory(20);
  if (button21) call_trajectory(21);
  if (button22) call_trajectory(22);
}
 
void call_trajectory(int plN) {
  switch(plN) {
    case 1:
      draw_trajectory(player1_x, player1_y, plN);
      break;
    case 2:
      draw_trajectory(player2_x, player2_y, plN);
      break;
    case 3:
      draw_trajectory(player3_x, player3_y, plN);
      break;
    case 4:
      draw_trajectory(player4_x, player4_y, plN);
      break;
    case 5:
      draw_trajectory(player5_x, player5_y, plN);
      break;
    case 6:
      draw_trajectory(player6_x, player6_y, plN);
      break;
    case 7:
      draw_trajectory(player7_x, player7_y, plN);
      break;
    case 8:
      draw_trajectory(player8_x, player8_y, plN);
      break;
    case 9:
      draw_trajectory(player9_x, player9_y, plN);
      break;
    case 10:
      draw_trajectory(player10_x, player10_y, plN);
      break;
    case 11:
      draw_trajectory(player11_x, player11_y, plN);
      break;
    case 12:
      draw_trajectory(player12_x, player12_y, plN);
      break;
    case 13:
      draw_trajectory(player13_x, player13_y, plN);
      break;
    case 14:
      draw_trajectory(player14_x, player14_y, plN);
      break;
    case 15:
      draw_trajectory(player15_x, player15_y, plN);
      break;
    case 16:
      draw_trajectory(player16_x, player16_y, plN);
      break;
    case 17:
      draw_trajectory(player17_x, player17_y, plN);
      break;
    case 18:
      draw_trajectory(player18_x, player18_y, plN);
      break;
    case 19:
      draw_trajectory(player19_x, player19_y, plN);
      break;
    case 20:
      draw_trajectory(player20_x, player20_y, plN);
      break;
    case 21:
      draw_trajectory(player21_x, player21_y, plN);
      break;
    case 22:
      draw_trajectory(player22_x, player22_y, plN);
      break;
  }
}