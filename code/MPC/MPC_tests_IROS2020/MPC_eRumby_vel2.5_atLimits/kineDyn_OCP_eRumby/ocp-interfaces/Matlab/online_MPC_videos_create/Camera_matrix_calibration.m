clc; clear all; close all;

%% Homography matrix of the football pitch

% This script performs the following tasks:
% - loads a frame of the video showing the pitch
% - asks to select a set of pixels
% - allows to save the selection or discard it and use the old set (second
%   is recommended)
% - uses the chosen pixels and a set of normalizated points to compute the
%   camera matrix

% The homography is NORMALIZED at 1 meter for both the directions of the pitch it
% means the coordinates from the homography  are from 0 to one inside the
% field. in this way the dcoordinates of the player can be obtained trough
% the moltiplication for the W and the H of the field

%% Load image

img    = imread('./Calibration_data/CAM_2/VID_20200302_173046.png'); % screenshot of the field
dim    = size(img);                % 1x3 vector with MxN (rowsxcoloumn)
numPix = dim(1)*dim(2);            % numbers of pixels (M*N)

figure(2);
imshow(img); title('Select the points - refer to: order\_of\_points.pdf', 'FontSize', 16); % show image
hold on;

%% Get pixels from imshow

[x, y] = getpts;
pxl    = [x, y];

figure(3);
imshow(img); title('Chosen points', 'FontSize', 16);
hold on;
plot(x, y, 'redx', 'MarkerSize', 10); % highlight chosen pixels
for i = 1:length(pxl(:,1))
    text(pxl(i,1), pxl(i,2),(['  (' num2str(i) ')']),'color',[1 - i/(2*length(pxl(:,1))) 0 i/(2*length(pxl(:,1)))])
end
hold off;

%% Save or discard new points

prompt = 'Save new and overwrite old points (be careful)? Y/N [N]: ';
str = input(prompt,'s');

if isempty(str) % no feedback
    str = 'N';
end
if (str == 'Y') || (str == 'y') % yes
    p_definitives = pxl;
    display('Goodbye old points, welcome new ones.');
else                            % no
    display('Dear old points, you are always the right choice.');
    load('p_definitives.mat');
end

save('p_definitives.mat','p_definitives');

%% Show the saved points

figure(4);
imshow(img); title('Saved points', 'FontSize', 16);
hold on;
plot(p_definitives(:,1), p_definitives(:,2), 'redx', 'MarkerSize', 10); % highlight center points
len = length(p_definitives(:,1));
plot(p_definitives(:,1), p_definitives(:,2),'redo--')
for ii = 1:len
    text(p_definitives(ii,1), p_definitives(ii,2),(['  (' num2str(ii) ')']),'color',[1 - ii/(2*len) 0 ii/(2*len)])
end
hold off;

%% Camera matrix computation

% Target points for the homography (normalized)
load('calibration_set_world_points.mat');

% Select only the calibration points visible in the camera shot
idx = [1:12 14];

p_regular = [x_cal(idx);y_cal(idx)];
    
camera_matrix = homography2d_solve(p_definitives', p_regular)
save('camera_matrix.mat','camera_matrix');

%% Plot transformed points on the image
imagePoints     = homography2d_project(inv(camera_matrix),p_regular);


figure(5);
imshow(img);
hold on
scatter(imagePoints(:,1),imagePoints(:,2),20,'y','filled','DisplayName', 'Transformed Calibration Points');
legend

