void draw_scroll() {
  fill(255);
  stroke(0);
  strokeWeight(1);
  rect(lim_sx, lim_up+20, (lim_dx-lim_sx), 20);
  
  fill(scroll);
  stroke(0);
  strokeWeight(1);
  if (scrollState) {
    rect(mouseX, lim_up, 20, 60);
    lastX = mouseX;
  } else {
    rect(lastX, lim_up, 20, 60);
  }
  
  if (scrollState) cursor = (mouseX-lim_sx)/(lim_dx-lim_sx); // normalizated position
  
  // debug purpose
  /*fill(255);
  stroke(0);
  strokeWeight(1);
  rect(lim_dx+40, lim_up, 120, 60);
  fill(0);
  textFont(font, 20);
  text(cursor, lim_dx+50, lim_up+30);*/
}

void draw_buttons() {
  if (!button1) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button1_x, button1_y, 40, 40);
  if (!button1) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("1", button1_x+15, button1_y+30);
  
  if (!button2) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button2_x, button1_y, 40, 40);
  if (!button2) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("2", button2_x+15, button1_y+30);
  
  if (!button3) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button3_x, button1_y, 40, 40);
  if (!button3) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("3", button3_x+15, button1_y+30);
  
  if (!button4) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button4_x, button1_y, 40, 40);
  if (!button4) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("4", button4_x+15, button1_y+30);
  
  if (!button5) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button5_x, button1_y, 40, 40);
  if (!button5) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("5", button5_x+15, button1_y+30);
  
  if (!button6) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button6_x, button1_y, 40, 40);
  if (!button6) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("6", button6_x+15, button1_y+30);
  
  if (!button7) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button7_x, button1_y, 40, 40);
  if (!button7) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("7", button7_x+15, button1_y+30);
  
  if (!button8) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button8_x, button1_y, 40, 40);
  if (!button8) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("8", button8_x+15, button1_y+30);
  
  if (!button9) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button9_x, button1_y, 40, 40);
  if (!button9) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("9", button9_x+15, button1_y+30);
  
  if (!button10) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button10_x, button1_y, 40, 40);
  if (!button10) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("10", button10_x+7, button1_y+30);
  
  if (!button11) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button11_x, button1_y, 40, 40);
  if (!button11) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("11", button11_x+7, button1_y+30);
  
  if (!button12) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button12_x, button12_y, 40, 40);
  if (!button12) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("12", button12_x+7, button12_y+30);
 
  if (!button13) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button13_x, button12_y, 40, 40);
  if (!button13) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("13", button13_x+7, button12_y+30);
  
  if (!button14) fill(255); // off
  else fill(butOn); // on  
  stroke(0);
  strokeWeight(1);
  rect(button14_x, button12_y, 40, 40);
  if (!button14) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("14", button14_x+7, button12_y+30);
  
  if (!button15) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button15_x, button12_y, 40, 40);
  if (!button15) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("15", button15_x+7, button12_y+30);
  
  if (!button16) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button16_x, button12_y, 40, 40);
  if (!button16) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("16", button16_x+7, button12_y+30);
  
  if (!button17) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button17_x, button12_y, 40, 40);
  if (!button17) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("17", button17_x+7, button12_y+30);
  
  if (!button18) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button18_x, button12_y, 40, 40);
  if (!button18) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("18", button18_x+7, button12_y+30);
  
  if (!button19) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button19_x, button12_y, 40, 40);
  if (!button19) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("19", button19_x+7, button12_y+30);
  
  if (!button20) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button20_x, button12_y, 40, 40);
  if (!button20) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("20", button20_x+7, button12_y+30);
  
  if (!button21) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button21_x, button12_y, 40, 40);
  if (!button21) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("21", button21_x+7, button12_y+30);
  
  if (!button22) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button22_x, button12_y, 40, 40);
  if (!button22) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("22", button22_x+7, button12_y+30);
  
  if (!trajState) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button1_x+700, button1_y+30, 140, 40);
  if (!trajState) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("trajectory", button1_x+720, button1_y+60);
  
  if (!circleState) fill(255); // off
  else fill(butOn); // on
  stroke(0);
  strokeWeight(1);
  rect(button1_x+700, button1_y+90, 140, 40);
  if (!circleState) fill(0); // text off
  else fill(255); // text on
  textFont(font, 24);
  text("position", button1_x+725, button1_y+120);
}