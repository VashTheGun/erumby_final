% ----------------------------------------------------------------------
%% Offline plots of MPC results
% ----------------------------------------------------------------------

set(0,'DefaultFigureWindowStyle','docked');
set(0,'defaultAxesFontSize',20)
set(0,'DefaultLegendFontSize',20)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

addpath('./Utilities');

% --------------------------
%% Load the data file and extract the information
% --------------------------
fileName_load = 'testMPC12_rec';
loadedData    = load(strcat('../../../../MPC_test_data/',fileName_load));
testData      = loadedData.savedData;
MPC_solution  = testData.MPC_solution;
Circuit       = testData.Circuit;
Telem         = testData.Telem;

% shift telemetry data to synchonize it with the MPC results
idx_startTelem = 1000;
idx_speed_start = 10400;
time_telem = Telem.time_telem(idx_startTelem:end) - Telem.time_telem(idx_startTelem);
time_telem_mod = Telem.time_telem + 2.5;
time_telem_speed = Telem.sensorsData.time(idx_speed_start:end) - Telem.sensorsData.time(idx_speed_start);

% set this flag to 1 in order to plot the 3D G-G diagram 
enable_plot_GG_diagr = 1;

% --------------------------
%% Display solution results
% --------------------------
fprintf('\n\n----------------------------- RESULTS -----------------------------\n');
fprintf('Lap time\t\t\t= %.3f s\n',MPC_solution.time_fullMPC(end))
fprintf('--------------------------- Convergence ---------------------------\n');
fprintf('N° of not converged OCPs\t= %d\n',MPC_solution.numNotConvergOCP)
if (MPC_solution.numNotConvergOCP~=0)
    fprintf('Not converged OCPs\t\t= %s\n',MPC_solution.notConverged_MPC_idx)
end
fprintf('--------------------------- CPU timing ----------------------------\n');
fprintf('max CPU time\t\t\t= %.2f ms @OCP n°%d\n',MPC_solution.max_cpuTime,MPC_solution.idx_maxcpuTime+1)
fprintf('min CPU time\t\t\t= %.2f ms @OCP n°%d\n',MPC_solution.min_cpuTime,MPC_solution.idx_mincpuTime+1)
fprintf('mean CPU time\t\t\t= %.2f ms\n',MPC_solution.mean_cpuTime)
fprintf('CPU time for 1st OCP\t\t= %.2f ms\n',MPC_solution.cpu_time_list(1))
fprintf('N° OCPs with CPU time > 100 ms\t= %d \n',length(MPC_solution.slow_MPC_idx));
if (~isempty(MPC_solution.slow_MPC_idx))
    fprintf('OCPs with CPU time > 100 ms\t= %s \n',mat2str((MPC_solution.slow_MPC_idx+1)'));
end
fprintf('-------------------------------------------------------------------\n');

% --------------------------
%% Plot vehicle path
% --------------------------
enable_docked = 1;
if (~enable_docked)
    figure('Name','Vehicle Path','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[50,50,1000,510]) 
else
    figure('Name','Vehicle Path','NumberTitle','off'), clf
end
hold on
plot(Circuit.x_RightMargin,Circuit.y_RightMargin,'Color',color('dark_green'),'LineWidth',2);
plot(Circuit.x_LeftMargin,Circuit.y_LeftMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');    
plot(MPC_solution.xRightCar_fullMPC,MPC_solution.yRightCar_fullMPC,'Color',color('blue'),'LineWidth',1)
plot(MPC_solution.xLeftCar_fullMPC,MPC_solution.yLeftCar_fullMPC,'Color',color('blue'),'LineWidth',1,'HandleVisibility','off')
plot(MPC_solution.xCoM_fullMPC,MPC_solution.yCoM_fullMPC,'Color',color('orange'),'LineWidth',2)
plot(MPC_solution.xCoM_fullMPC,MPC_solution.yCoM_fullMPC,'go','MarkerFaceColor','g','MarkerSize',4)
plot(Telem.sensorsData.x_O,Telem.sensorsData.y_O,'-','color',color('deepsky_blue'),'LineWidth',2)
grid on
axis equal
xlabel('x [m]')
ylabel('y [m]')
legend('Road boundaries','Car edges','CoM position','recorded lap','location','best')
title('Vehicle Path, MPC with receding horizon')

% --------------------------
%% Plot vehicle states
% --------------------------
if (~enable_docked)
    figure('Name','States','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[50,50,1000,510]) 
else
    figure('Name','States','NumberTitle','off'), clf
end
% --- u --- %
ax(1) = subplot(321);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.u_fullMPC,'LineWidth',2,'HandleVisibility','off')
plot(MPC_solution.time_fullMPC,MPC_solution.u_fullMPC,'go','MarkerFaceColor','g','MarkerSize',8)
plot(MPC_solution.time_interp_save,MPC_solution.u_interp_save,'ro','MarkerSize',4)
plot(Telem.time_raspb_use,Telem.u_raspb_use,'mo','MarkerFaceColor','m','MarkerSize',6)
plot(time_telem,Telem.speed_telem(idx_startTelem:end),'ko','MarkerSize',3)
plot(time_telem_speed,Telem.sensorsData.Speed(idx_speed_start:end),'o','Color',color('purple'),'MarkerSize',4)
legend('MPC','interp','rasp','telem','location','best')
grid on
title('$u$ [m/s]')  
% --- delta --- %
ax(2) = subplot(322);
hold on
plot(MPC_solution.time_fullMPC,rad2deg(MPC_solution.delta_fullMPC),'LineWidth',2,'HandleVisibility','off')
plot(MPC_solution.time_fullMPC,rad2deg(MPC_solution.delta_fullMPC),'go','MarkerFaceColor','g','MarkerSize',8)
plot(MPC_solution.time_interp_save,rad2deg(MPC_solution.delta_interp_save),'ro','MarkerSize',4)
plot(Telem.time_raspb_use,rad2deg(Telem.delta_raspb_use),'mo','MarkerFaceColor','m','MarkerSize',6)
legend('MPC','interp','rasp','location','best')
grid on
title('$\delta$ [deg]')
% --- Omega --- %
ax(3) = subplot(323);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.Omega_fullMPC,'LineWidth',2) 
plot(time_telem_mod,Telem.Omega_telem,'ko','MarkerSize',3)
legend('MPC','telem','location','best')
grid on
title('$\Omega$ [rad/s]')
% --- a_x --- %
ax(4) = subplot(324);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.a_x_fullMPC,'LineWidth',2) 
grid on
title('$a_x$ [m/s$^2$]')
% --- a_y --- %
ax(5) = subplot(325);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.a_y_fullMPC,'LineWidth',2) 
plot(Telem.time_telem,Telem.Ay_ss_telem,'ko','MarkerSize',3)
% plot(time_telem,Telem.Ay_ss_telem(idx_startTelem:end),'ko','MarkerSize',3)
legend('MPC','telem','location','best')
grid on
title('$a_y$ [m/s$^2$]')

clear ax

% --------------------------
%% Plot vehicle pose
% --------------------------
if (~enable_docked)
    figure('Name','Pose','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[50,50,1000,510]) 
else
    figure('Name','Pose','NumberTitle','off'), clf
end
% --- xCoM --- %
ax(1) = subplot(231);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.xCoM_fullMPC,'LineWidth',2)
% plot(MPC_solution.time_fullMPC,MPC_solution.xCoM_fullMPC,'go','MarkerFaceColor','g','MarkerSize',6)
grid on
title('x CoM [m]')  
legend('MPC','location','best')
% --- yCoM --- %
ax(2) = subplot(232);
plot(MPC_solution.time_fullMPC,MPC_solution.yCoM_fullMPC,'LineWidth',2)
grid on
title('y CoM [m]')
legend('MPC','location','best')
% --- n --- %
ax(3) = subplot(233);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.n_fullMPC,'LineWidth',2)
% plot(MPC_solution.time_fullMPC,MPC_solution.n_fullMPC,'go','MarkerFaceColor','g','MarkerSize',8)
grid on
title('$n$ [m]')  
legend('MPC','location','best')
% --- xi --- %
ax(4) = subplot(234);
plot(MPC_solution.time_fullMPC,rad2deg(MPC_solution.xi_fullMPC),'LineWidth',2)
grid on
title('$\xi$ [deg]')  
legend('MPC','location','best')
% --- psi --- %
ax(5) = subplot(235);
plot(MPC_solution.time_fullMPC,rad2deg(MPC_solution.vehicleAttitude_fullMPC),'LineWidth',2)
grid on
title('$\psi$ [deg]')  
legend('MPC','location','best')

clear ax

% --------------------------
%% 3D G-G diagram
% --------------------------
if (enable_plot_GG_diagr)
    Axmax_fittingCoeffs = load('../../../GG_fittingData/Axmax_fittingCoeffs');
    Axmin_fittingCoeffs = load('../../../GG_fittingData/Axmin_fittingCoeffs');
    Aymax_fittingCoeffs = load('../../../GG_fittingData/Aymax_fittingCoeffs');
    Axmax_fitCoeffs = Axmax_fittingCoeffs.Axmax_fitCoeffs;
    Axmin_fitCoeffs = Axmin_fittingCoeffs.Axmin_fitCoeffs;
    Aymax_fitCoeffs = Aymax_fittingCoeffs.Aymax_fitCoeffs;

    speed_vect_fit = 0.5:0.1:2.5;  % [m/s]

    vehicle_data = getVehicleDataStruct();
    k_D = vehicle_data.vehicle.k_D;
    m   = vehicle_data.vehicle.m;
    
    % Subtract the aero drag offset to the positive values of a_x_fullMPC
%     a_x_fullMPC_noAeroOffset = MPC_solution.a_x_fullMPC;  % initialize
%     for uu=1:length(MPC_solution.a_x_fullMPC)
%         if (MPC_solution.a_x_fullMPC(uu)>0)
%             a_x_fullMPC_noAeroOffset(uu) = MPC_solution.a_x_fullMPC(uu) - (k_D/m)*(MPC_solution.u_fullMPC(uu)^2);
%         end
%     end

    % Limits for figure axes
    ax_negPlotLimit = -5;
    ax_posPlotLimit = 5;
    ay_negPlotLimit = -7;
    ay_posPlotLimit = 7;

    % Define a custom color order (each color corresponds to a certain speed value)
    ColorOdr = flipud(parula(length(speed_vect_fit)));

    if (~enable_docked)
        figure('Name','3D G-G','NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[50,50,1000,510]) 
    else
        figure('Name','3D G-G','NumberTitle','off'), clf
    end
    hold on
%     axis equal
    for jj = 1:length(speed_vect_fit) 
        ax_max  = Axmax_fitCoeffs(1) + Axmax_fitCoeffs(2)*speed_vect_fit(jj) + Axmax_fitCoeffs(3)*speed_vect_fit(jj).^2 + Axmax_fitCoeffs(4)*speed_vect_fit(jj).^3 + Axmax_fitCoeffs(5)*speed_vect_fit(jj).^4 + Axmax_fitCoeffs(6)*speed_vect_fit(jj).^5 + Axmax_fitCoeffs(7)*speed_vect_fit(jj).^6 + Axmax_fitCoeffs(8)*speed_vect_fit(jj).^7;
%         ax_max  = ax_max - (k_D/m)*speed_vect_fit(jj)^2;
        ax_min  = Axmin_fitCoeffs(1) + Axmin_fitCoeffs(2)*speed_vect_fit(jj) + Axmin_fitCoeffs(3)*speed_vect_fit(jj).^2 + Axmin_fitCoeffs(4)*speed_vect_fit(jj).^3 + Axmin_fitCoeffs(5)*speed_vect_fit(jj).^4;
        ay_max  = Aymax_fitCoeffs(1) + Aymax_fitCoeffs(2)*speed_vect_fit(jj) + Aymax_fitCoeffs(3)*speed_vect_fit(jj).^2 + Aymax_fitCoeffs(4)*speed_vect_fit(jj).^3 + Aymax_fitCoeffs(5)*speed_vect_fit(jj).^4 + Aymax_fitCoeffs(6)*speed_vect_fit(jj).^5;
        ax_offs = 0;
        n_upper = 2;
        n_lower = 2;
        GG_fit = @(ay,ax,u)generalizedEllipse(ax,ay,ax_max,ax_min,ay_max,n_upper,n_lower,ax_offs);
        fimplicit3(GG_fit,[ay_negPlotLimit ay_posPlotLimit ax_negPlotLimit-4 ax_posPlotLimit speed_vect_fit(jj) speed_vect_fit(jj)+0.01],'MeshDensity',40,'EdgeColor',ColorOdr(jj,:),'LineWidth',2.5)
    end
    plot3(MPC_solution.a_y_fullMPC,MPC_solution.a_x_fullMPC,MPC_solution.u_fullMPC,'Color',color('violet_red'),'LineWidth',4)
    grid on
    xlabel('$a_y$ [m/s$^2$]')
    ylabel('$a_x$ [m/s$^2$]')
    zlabel('$u$ [m/s]')
    title('G-G surface points from simulated maneuvers')
    oldcmap = colormap;
    colormap(flipud(oldcmap));
    hcb = colorbar;
    title(hcb,'Speed [m/s]','Interpreter','latex')
    caxis([speed_vect_fit(1), speed_vect_fit(end)]);
end



