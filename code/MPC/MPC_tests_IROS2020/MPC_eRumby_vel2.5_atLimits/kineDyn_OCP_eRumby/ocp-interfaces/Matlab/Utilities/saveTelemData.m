% ------------------
%% Extract sensors data
% ------------------
% constant values related to the vehicle actuators
DUTY_SERVO_SX = 5024;  
DUTY_SERVO_MIDDLE = 6881;
DUTY_SERVO_DX = 8738;

DUTY_ESC_MAX = 8412;
DUTY_ESC_IDLE = 7010;
DUTY_ESC_MIN = 5608;

time = sensor(1,:);
%--------------- axis remap convection ------------------
%
%                a_x ----> -a_y
%                a_y ----> a_x
%                a_z ----> a_z
%
%--------------------------------------------------------
a_x_mpu = sensor(3,:);
a_y_mpu = -sensor(2,:);
a_z_mpu = sensor(4,:);
gyro_x_mpu = sensor(6,:);
gyro_y_mpu = -sensor(5,:);
gyro_z_mpu = sensor(7,:);
enc_rr = sensor(9,:)/100;
enc_rl = sensor(8,:)/100;
enc_fr = medfilt1(sensor(11,:),3)/100;
enc_fl = medfilt1(sensor(12,:),3)/100;
a_x_raw = sensor(14,:);
a_y_raw = -sensor(13,:);
a_z_raw = sensor(15,:);
x_gps = sensor(18,:);
y_gps = sensor(19,:);
yaw_gps = sensor(20,:);

% Steering percent map
% for i=1:length(sensor(17,:))
%     if(sensor(17,i)==0)
%         steering(i) = DUTY_SERVO_MIDDLE;
%     else
%         steering(i) = sensor(17,i);
%     end  
% end
% steering = fixpt_interp1( [DUTY_SERVO_SX, DUTY_SERVO_DX], [-100, 100] ,steering, sfix(16), 1, sfix(16), 0.01);
steering = (6881 - sensor(17,:))/4402;

% Traction percent map
for i=1:length(sensor(10,:))
    if(sensor(10,i)==0)
        traction(i) = DUTY_ESC_IDLE;
    else
        traction(i) = sensor(10,i);
    end  
end
traction = fixpt_interp1( [DUTY_ESC_MIN, DUTY_ESC_MAX], [-100, 100] ,traction, sfix(16), 1, sfix(16), 0.01);
% 
target = sensor(16,:);


% ------------------
%% Create a txt file with telemetry data
% ------------------
% Creation of an incremental file name for different tests
test = "../../../../MPC_test_data/testMPC";             % first file
output_telem_fileName = 'MPC';
i = 1;                          % variable inizialization
check = 0;

while(check == 0)               % loop for creation of brand new file
    file_name = strcat('../../../../MPC_test_data/testMPC', num2str(i), '.txt');
    if ~exist(file_name, 'file')
        check = 1;        
    end
    i = i+1;
end

% creation of the txt file
today = datestr(now);
fileID = fopen(file_name, 'w');
fprintf(fileID, '% s\t % s\n', today, output_telem_fileName);
fprintf(fileID, '% 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t  % 8s\t % 8s\t % 8s\t % 8s\n', '[s]', '[g]', '[g]', '[g]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[g]', '[g]', '[g]', '[%]', '[%}', '[rad/s]', '[mm]', '[mm]', '[grad]');
fprintf(fileID, '% 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\n', 'time', 'a_x_mpu', 'a_y_mpu', 'a_z_mpu', 'gyro_x_mpu', 'gyro_y_mpu', 'gyro_z_mpu', 'enc_fr', 'enc_fl', 'enc_rr', 'enc_rl', 'a_x_raw', 'a_y_raw', 'a_z_raw', 'steering', 'traction', 'target', 'x_gps', 'y_gps', 'yaw_gps');
fprintf(fileID, '% 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\n', [time; a_x_mpu; a_y_mpu; a_z_mpu; gyro_x_mpu; gyro_y_mpu; gyro_z_mpu; enc_fr; enc_fl; enc_rr; enc_rl; a_x_raw; a_y_raw; a_z_raw; steering; traction; target; x_gps; y_gps; yaw_gps]);
fclose('all');

save_telemetryData = 0;