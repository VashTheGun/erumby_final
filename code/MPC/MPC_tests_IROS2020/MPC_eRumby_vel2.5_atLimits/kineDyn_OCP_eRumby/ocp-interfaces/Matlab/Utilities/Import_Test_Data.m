% ---------------------------------------------
%% Import telemetry and OptiTrack sensors data 
% ---------------------------------------------

telemetryData = strcat('./',testName,num2str(i-1),'.txt');
optitrackData = strcat('./',testName,num2str(i-1),'.csv');
[telemetry] = telemetry_import_V2(telemetryData);
[optitrack] = optitrack_import(optitrackData);
[remap_telemetry,remap_optitrack] = test_data_remap(telemetry,optitrack);

