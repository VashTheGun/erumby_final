% --------------------------------------------
%% Extract the results of an MPC iteration
% --------------------------------------------

time_rep = states_out(:,2);
xCoM_rep = pose_out(:,1);
yCoM_rep = pose_out(:,2);
xRightCar_rep = pose_out(:,3);
yRightCar_rep = pose_out(:,4);
xLeftCar_rep = pose_out(:,5);
yLeftCar_rep = pose_out(:,6);
vehicleAttitude_rep = pose_out(:,7);
delta_rep = states_out(:,8);
u_rep = states_out(:,3);
Omega_rep = states_out(:,5);
a_x_rep = states_out(:,4);
n_rep = states_out(:,6);
xi_rep = states_out(:,7);
abscissa_rep = states_out(:,1);

% Compute which controls are actually applied to the vehicle during
% this MPC iteration, before a new replanning takes place
indices_applied = 2;
if (~isempty(find(floor(time_rep/replan_time)>=1,1)))
    indices_applied = find(floor(time_rep/replan_time)>=1,1);
end

MHT.zetaFull(1:length(abscissa_rep),Niter+1) = abscissa_rep;
MHT.time(1:indices_applied-1,Niter+1) = time_rep(1:indices_applied-1);
MHT.timeFull(1:length(time_rep),Niter+1) = time_rep;
MHT.xCoM(1:indices_applied-1,Niter+1) = xCoM_rep(1:indices_applied-1);
MHT.xCoMFull(1:length(xCoM_rep),Niter+1) = xCoM_rep;
MHT.yCoM(1:indices_applied-1,Niter+1) = yCoM_rep(1:indices_applied-1);
MHT.yCoMFull(1:length(yCoM_rep),Niter+1) = yCoM_rep;
MHT.xRightCar(1:indices_applied-1,Niter+1) = xRightCar_rep(1:indices_applied-1);
MHT.yRightCar(1:indices_applied-1,Niter+1) = yRightCar_rep(1:indices_applied-1);
MHT.xLeftCar(1:indices_applied-1,Niter+1) = xLeftCar_rep(1:indices_applied-1);
MHT.yLeftCar(1:indices_applied-1,Niter+1) = yLeftCar_rep(1:indices_applied-1);
MHT.vehicleAttitude(1:indices_applied-1,Niter+1) = vehicleAttitude_rep(1:indices_applied-1);
MHT.vehicleAttitudeFull(1:length(vehicleAttitude_rep),Niter+1) = vehicleAttitude_rep;
MHT.delta(1:indices_applied-1,Niter+1) = delta_rep(1:indices_applied-1);
MHT.u(1:indices_applied-1,Niter+1) = u_rep(1:indices_applied-1);
MHT.uFull(1:length(u_rep),Niter+1) = u_rep;
MHT.Omega(1:indices_applied-1,Niter+1) = Omega_rep(1:indices_applied-1);
MHT.a_x(1:indices_applied-1,Niter+1) = a_x_rep(1:indices_applied-1);
MHT.n(1:indices_applied-1,Niter+1) = n_rep(1:indices_applied-1);
MHT.xi(1:indices_applied-1,Niter+1) = xi_rep(1:indices_applied-1);
MHT.abscissa(1:indices_applied-1,Niter+1) = abscissa_rep(1:indices_applied-1);
MHT.horizon_MHT(1:length(horizon_MHT),Niter+1) = horizon_MHT;
MHT.poseRoad_ini(1:length(poseRoad_ini),Niter+1) = poseRoad_ini;
MHT.converged_MPC(Niter+1) = converged_MPC_flag;
MHT.cpu_time(Niter+1) = cpu_time;

horizon_MHT_prev = horizon_MHT;

MHT.flag_allMPCconverged(Niter+1) = flag_allMPCconverged_out;
