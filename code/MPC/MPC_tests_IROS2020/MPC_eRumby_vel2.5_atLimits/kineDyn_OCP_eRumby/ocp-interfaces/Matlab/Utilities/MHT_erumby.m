function [states_out,controls_out,pose_out,lambda_out,horizon_MHT,cpu_time,converged_MPC_flag,flag_allMPCconverged,poseRoad_ini,ocp_solved] = MHT_erumby(states_in,pose_in,pose_optiTrack,flag_allMPCconverged_in,data_MPC,S,ocp,ocp_prev,Niter,replan_time,track_length,initCondits)    
    
    % ----------------------------------------------------------------
    %% function purpose: implement the MPC high-level planner
    % ----------------------------------------------------------------

    if (Niter==0) 
        
        % --------------------------
        %% MPC for the first horizon
        % --------------------------
        
        fprintf('\n\n================== MPC ON THE HORIZON N�1 ====================\n');

        % Define the MPC horizon 
        start_zeta = initCondits(7);
        ds         = 0.01;
        absc_step  = 0.1;
        LH         = 30;
        horizon    = 0:ds:LH;
        horizon_shift = horizon + start_zeta;
        horizon_MHT = (0:absc_step:LH)' + start_zeta;

        % Road coords, heading and curvature over the horizon
        [x_r,y_r,dir_r,curv_r] = S.evaluate(horizon_shift); 

        % Position and heading of the initial point of the circuit center line 
        x_ini = x_r(1);
        y_ini = y_r(1);
        theta_ini = dir_r(1);

        % Initial conditions for vehicle states  
        bcs.initial.v__x     = initCondits(1);
        bcs.initial.a__x     = initCondits(2);
        bcs.initial.Omega    = initCondits(3);
        bcs.initial.delta__D = 0;
        bcs.initial.n        = initCondits(5);    
        bcs.initial.xi       = initCondits(6);
        fprintf('theta_ini is %.4f deg, xi is %.4f deg\n',rad2deg(theta_ini),rad2deg(bcs.initial.xi));

        % Modify the fields of the data struct
        % Save the new boundary conditions
        data_MPC = add_BoundaryCondits(data_MPC,bcs);

        % Trajectory data
        data_MPC.Trajectory.abscissa                = horizon;  
        data_MPC.Trajectory.abscissa_step           = absc_step;
        data_MPC.Trajectory.curvature               = curv_r; 
        data_MPC.Trajectory.mesh.segments{1}.length = 1;      
        data_MPC.Trajectory.mesh.segments{1}.n      = LH/absc_step;    
        data_MPC.Trajectory.theta0                  = theta_ini; 
        data_MPC.Trajectory.x0                      = x_ini;   
        data_MPC.Trajectory.y0                      = y_ini;   

        ocp.setup(data_MPC);
        ocp.set_guess();    % use the default guess specified in Maple

        % Solve the optimal control problem
        converged_MPC_flag = double(ocp.solve());
        
        % Extract the solution
        solution_MPC = ocp.solution(); % whole solution in a structure
        [states_out,lambda_out,controls_out,pose_out,cpu_time,flag_allMPCconverged,poseRoad_ini] = save_MPCsolution(ocp,solution_MPC,data_MPC,S,start_zeta,x_ini,y_ini,converged_MPC_flag,flag_allMPCconverged_in);
        ocp_solved = ocp;   
               
    else            
        
        % --------------------------
        %% MPC main loop
        % --------------------------

        % Extract the inputs for the Matlab System block
        zeta_in            = states_in(:,1);
        time_in            = states_in(:,2);
        v_x_in             = states_in(:,3);
        a_x_in             = states_in(:,4);
        Omega_in           = states_in(:,5);
        delta_D_in         = states_in(:,8);

        % Initial conditions for vehicle pose (obtained by means of the OptiTrack sensor data)
        zeta_veh_ini = interp1(time_in,zeta_in,replan_time); 
        x_veh_ini   = pose_optiTrack(1);  
        y_veh_ini   = pose_optiTrack(2); 
        psi_veh_ini = pose_optiTrack(3);  
            
        [x_ini,y_ini,start_zeta,n_ini] = S.closestPoint(x_veh_ini,y_veh_ini);
        n_ini = -n_ini;
        [~,~,theta_ini,~] = S.evaluate(start_zeta); 
        theta_ini = (theta_ini);
        xi_ini = (psi_veh_ini - theta_ini);
        if (xi_ini>pi)
            xi_ini = psi_veh_ini - 2*pi - theta_ini;
        elseif (xi_ini<-pi)
            xi_ini = psi_veh_ini - (theta_ini-2*pi);
        end

        % Define the MPC horizon
        ds         = 0.01;
        absc_step  = 0.1;
        LH         = 30;
        horizon    = 0:ds:LH;
        horizon_shift = horizon + start_zeta;
        horizon_MHT = (0:absc_step:LH)' + start_zeta;
        if (start_zeta+LH > track_length)
            horizon_shift_firstPart = start_zeta:ds:track_length-ds+start_zeta-floor(start_zeta/ds)*ds;
            horizon_shift = [horizon_shift_firstPart, start_zeta-floor(start_zeta/ds)*ds:ds:LH-(length(horizon_shift_firstPart)-1)*ds];
        end
        
        fprintf('\n================== MPC ON THE HORIZON N�%d ====================\n',Niter+1);

        % Curvature of the road over the specified horizon
        [~,~,~,curv_r] = S.evaluate(horizon_shift); % extract data  

        % Modify the fields of the data struct
        % Trajectory data
        data_MPC.Trajectory.abscissa                = horizon;  
        data_MPC.Trajectory.abscissa_step           = absc_step;
        data_MPC.Trajectory.curvature               = curv_r; 
        data_MPC.Trajectory.mesh.segments{1}.length = 1;      
        data_MPC.Trajectory.mesh.segments{1}.n      = LH/absc_step;    
        data_MPC.Trajectory.theta0                  = theta_ini; 
        data_MPC.Trajectory.x0                      = x_ini;   
        data_MPC.Trajectory.y0                      = y_ini;

        % Initial conditions for vehicle states
        bcs.initial.v__x     = interp1(zeta_in,v_x_in,zeta_veh_ini);  
        bcs.initial.a__x     = interp1(zeta_in,a_x_in,zeta_veh_ini);     
        bcs.initial.Omega    = interp1(zeta_in,Omega_in,zeta_veh_ini);  
        bcs.initial.delta__D = interp1(zeta_in,delta_D_in,zeta_veh_ini);
        bcs.initial.n        = n_ini;
        bcs.initial.xi       = xi_ini;
        fprintf('theta_ini is %.4f deg, xi is %.4f deg\n',rad2deg(theta_ini),rad2deg(bcs.initial.xi));

        data_MPC = add_BoundaryCondits(data_MPC,bcs);

        % Provide the previous MPC solution as guess for the optimal control solver
        guess_MPC = ocp_prev.get_solution_as_guess();

        ocp.setup(data_MPC);
        ocp.set_guess(guess_MPC);

        % Solve the optimal control problem
        converged_MPC_flag = double(ocp.solve());
        
        % Extract the solution
        solution_MPC = ocp.solution(); % whole solution in a structure
        [states_out,lambda_out,controls_out,pose_out,cpu_time,flag_allMPCconverged,poseRoad_ini] = save_MPCsolution(ocp,solution_MPC,data_MPC,S,start_zeta,x_ini,y_ini,converged_MPC_flag,flag_allMPCconverged_in);
        ocp_solved = ocp;       
        
    end
    
end