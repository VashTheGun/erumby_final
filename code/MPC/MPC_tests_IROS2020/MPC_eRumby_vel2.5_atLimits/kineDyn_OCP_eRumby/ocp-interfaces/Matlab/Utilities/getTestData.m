%     --> subscript _T is used for telemetry data
%     --> subscript _O is used for optitrack data

% Extract the most important sensors data
sensorsData  = extractSensorsData(remap_telemetry,remap_optitrack,vehicle_data);

% Filter and resize sensors data
filter_flag = 1;  % set it to 1 to enable low pass filtering of data
init_sample = 5000;
fin_sample  = 0;
trailing_flag = 0;
filteredData = resize_and_filter(sensorsData,filter_flag,init_sample,fin_sample,trailing_flag);
time_restrict         = filteredData.time;
delta_restrict        = filteredData.delta;
Omega_T_filt_restrict = filteredData.Omega_T;
Speed_O_filt_restrict = filteredData.Speed_O;
Ay_ss_filt_restrict   = filteredData.Ay_ss_T;
Ax_T_filt             = filteredData.Ax_T;

idx_start_telem = find(Speed_O_filt_restrict>=0,1);
time_telem = time_restrict(idx_start_telem:end) - time_restrict(idx_start_telem);
speed_telem = Speed_O_filt_restrict(idx_start_telem:end);
delta_telem = delta_restrict(idx_start_telem:end);
Omega_telem = Omega_T_filt_restrict(idx_start_telem:end);
Ay_ss_telem = Ay_ss_filt_restrict(idx_start_telem:end);
Ax_telem    = Ax_T_filt(idx_start_telem:end);




