% clear all;
clearvars;
close all; 
clc; 

solveOCP_now = 0;
sendControls_now = 0;
sendControls_nows = zeros(10000000,1);
currTime_sendControls = zeros(100000,1);
currTime_solveOCP = zeros(100000,1);

timerObj_ocp = timer('TimerFcn','solveOCP_now = 2;','Period',0.1, ...  
                 'ExecutionMode','fixedRate','BusyMode','queue','StopFcn','disp(''Timer has stopped.'')');    
timerObj_lowLevelCtrl = timer('TimerFcn','sendControls_now = 3;','Period',0.01, ...  % @timerFcn_test
                 'ExecutionMode','fixedRate','BusyMode','queue','StopFcn','disp(''Timer has stopped.'')');  
start(timerObj_ocp);
start(timerObj_lowLevelCtrl);

j = 1;
k = 1;
hh = 1;
tic;
startTime = str2double(datestr(clock,'SS.FFF'));
while(toc <= 2)
    if (sendControls_now==3)
        currTime_sendControls(j,1) = str2double(datestr(clock,'SS.FFF'));
%         fprintf('sendControls_now is %d\n',sendControls_now);
        j = j+1;
        sendControls_now = 0;
    end

    if (solveOCP_now==2)
        currTime_solveOCP(k,1) = str2double(datestr(clock,'SS.FFF'));
%         fprintf('solveOCP_now\n');
        k = k+1;
        solveOCP_now = 0;
    end

%     fprintf('sendControls_now is %d\n',sendControls_now);
    sendControls_nows(hh,1) = sendControls_now;
    hh = hh+1;
end

stop(timerObj_ocp);
stop(timerObj_lowLevelCtrl);
delete(timerObj_ocp);
delete(timerObj_lowLevelCtrl);

timeDiff_sendControls_save = diff(currTime_sendControls(1:j-1,1));
timeDiff_solveOCP_save = diff(currTime_solveOCP(1:k-1,1));
currTime_solveOCP_save = currTime_solveOCP(1:k-1,1);
sendControls_now_save = sendControls_nows(1:hh-1,1);

