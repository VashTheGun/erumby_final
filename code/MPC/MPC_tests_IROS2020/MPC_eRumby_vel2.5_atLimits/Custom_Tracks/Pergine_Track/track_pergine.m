% ----------------------------------------------------
%% Create the road file for the Pergine track
% ----------------------------------------------------

% ---------------
%% Initialize
% ---------------
clc;
clearvars;
close all;

% set this to 1 to enable docked window style in plots
enable_docked = 1;
if (enable_docked)
    set(0,'DefaultFigureWindowStyle','docked');
else    
    set(0,'DefaultFigureWindowStyle','normal');
end
set(0,'defaultAxesFontSize',18)
set(0,'DefaultLegendFontSize',18)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

addpath('../../kineDyn_OCP_eRumby/ocp-interfaces/Matlab/Clothoids/matlab');

% ---------------
%% Circuit points
% ---------------
x = [0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0];
y = [0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0];
theta = [0, 0, 0, 0, 0, pi, pi, 2.1025, 0, 0, 0, pi, pi, pi, 0];
% x = [2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0, 2];
% y = [1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0, 1];
% theta = [0, 0, 0, 0, pi, pi, 2.1025, 0, 0, 0, pi, pi, pi, 0, 0];

% ---------------
%% Fit with clothoids
% ---------------
S = ClothoidList();

for i = 1:length(x)-1
    S.push_back_G1(x(i),y(i),theta(i), x(i+1),y(i+1),theta(i+1)) ; % track creation
end

% Plot
if (~enable_docked)
    figure('Name','Pergine track','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','Pergine track','NumberTitle','off'), clf
end    
hold on
S.plot
grid on
axis equal
xlabel('$x$ [m]')
ylabel('$y$ [m]')
title('Pergine circuit')

% ---------------
%% Save the circuit data
% ---------------
absc_step_1 = 0.01; % fine road mesh
abscissae_1 = 0:absc_step_1:S.length;
[x_path_1,y_path_1,theta_path_1,curv_path_1] = S.evaluate(abscissae_1);
lane_width = 0.3;   % [m]
width_L = lane_width*ones(size(abscissae_1));
width_R = lane_width*ones(size(abscissae_1));

referenceRoad_PINS = fopen('circuit_Pergine_PINS.txt','w');
% Headers
fprintf(referenceRoad_PINS,'%s\t%s\t%s\t%s\n','abscissa','curvature','width_L','width_R');
% Values
for ii=1:length(abscissae_1)
    fprintf(referenceRoad_PINS,'%f\t%f\t%f\t%f\n',abscissae_1(ii),curv_path_1(ii),width_L(ii),width_R(ii));
end
fclose(referenceRoad_PINS);


referenceRoad = fopen('circuit_Pergine_clothoids.txt','w');
% Headers
fprintf(referenceRoad,'%s\t%s\t%s\n','X','Y','theta');
% Values
for ii=1:length(x)
    fprintf(referenceRoad,'%f\t%f\t%f\n',x(ii),y(ii),theta(ii));
end
fclose(referenceRoad);

% ---------------
%% Create the road boundaries
% ---------------
x_RightMargin = zeros(size(abscissae_1));  % initialize
y_RightMargin = zeros(size(abscissae_1));  
x_LeftMargin  = zeros(size(abscissae_1));
y_LeftMargin  = zeros(size(abscissae_1));
for jj=1:length(abscissae_1)
    RotMat = [cos(theta_path_1(jj)) -sin(theta_path_1(jj)); ...
              sin(theta_path_1(jj))  cos(theta_path_1(jj))];
    RightMargin = [x_path_1(jj); y_path_1(jj)] + RotMat*[0; -lane_width];      
    LeftMargin  = [x_path_1(jj); y_path_1(jj)] + RotMat*[0;  lane_width];    
    x_RightMargin(jj) = RightMargin(1);
    y_RightMargin(jj) = RightMargin(2);
    x_LeftMargin(jj) = LeftMargin(1);
    y_LeftMargin(jj) = LeftMargin(2);
end

plot(x_RightMargin,y_RightMargin,'-g','LineWidth',2)
plot(x_LeftMargin,y_LeftMargin,'-g','LineWidth',2)
hold off

% ---------------
%% Save the road boundaries
% ---------------
roadBoarders = fopen('roadMargins_Pergine.txt','w');
% Headers
fprintf(roadBoarders,'%s\t%s\t%s\t%s\n','x_RightMargin','y_RightMargin','x_LeftMargin','y_LeftMargin');
% Values
for ii=1:length(x_RightMargin)
    fprintf(roadBoarders,'%f\t%f\t%f\t%f\n',x_RightMargin(ii),y_RightMargin(ii),x_LeftMargin(ii),y_LeftMargin(ii));
end
fclose(roadBoarders);

