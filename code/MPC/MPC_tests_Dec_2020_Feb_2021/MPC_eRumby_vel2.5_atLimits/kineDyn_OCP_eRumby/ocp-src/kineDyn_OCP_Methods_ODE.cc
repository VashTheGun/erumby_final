/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP_Methods.cc                                         |
 |                                                                       |
 |  version: 1.0   date 8/12/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "kineDyn_OCP.hh"
#include "kineDyn_OCP_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_abs_reg_DD(__t1) abs_reg.DD( __t1)
#define ALIAS_abs_reg_D(__t1) abs_reg.D( __t1)
#define ALIAS_SignReg_smooth_DD(__t1) SignReg_smooth.DD( __t1)
#define ALIAS_SignReg_smooth_D(__t1) SignReg_smooth.D( __t1)
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_GGdiagramEnvelope_DD(__t1) GGdiagramEnvelope.DD( __t1)
#define ALIAS_GGdiagramEnvelope_D(__t1) GGdiagramEnvelope.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_a__x0Control_D_3(__t1, __t2, __t3) a__x0Control.D_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2(__t1, __t2, __t3) a__x0Control.D_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1(__t1, __t2, __t3) a__x0Control.D_1( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_3_3(__t1, __t2, __t3) a__x0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_3(__t1, __t2, __t3) a__x0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_2(__t1, __t2, __t3) a__x0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_3(__t1, __t2, __t3) a__x0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_2(__t1, __t2, __t3) a__x0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_1(__t1, __t2, __t3) a__x0Control.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3(__t1, __t2, __t3) delta__D0Control.D_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2(__t1, __t2, __t3) delta__D0Control.D_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1(__t1, __t2, __t3) delta__D0Control.D_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3_3(__t1, __t2, __t3) delta__D0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_3(__t1, __t2, __t3) delta__D0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_2(__t1, __t2, __t3) delta__D0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_3(__t1, __t2, __t3) delta__D0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_2(__t1, __t2, __t3) delta__D0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_1(__t1, __t2, __t3) delta__D0Control.D_1_1( __t1, __t2, __t3)


namespace kineDyn_OCPDefine {

  /*\
   |    ___  ___  ___
   |   / _ \|   \| __|
   |  | (_) | |) | _|
   |   \___/|___/|___|
  \*/

  integer
  kineDyn_OCP::rhs_ode_numEqns() const
  { return 6; }

  void
  kineDyn_OCP::rhs_ode_eval(
    NodeType const     & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = sin(t1);
    real_type t3   = Q__[1];
    real_type t5   = X__[0] * t3;
    real_type t6   = t5 - 1;
    real_type t8   = cos(t1);
    real_type t9   = 1.0 / t8;
    result__[ 0   ] = -t9 * t6 * t2;
    real_type t11  = X__[2];
    real_type t14  = X__[4];
    real_type t18  = 1.0 / t11;
    result__[ 1   ] = -t18 * t9 * (t3 * t11 * t8 + t14 * t5 - t14);
    real_type t20  = ModelPars[17];
    real_type t22  = t11 * t11;
    real_type t25  = X__[3];
    real_type t30  = t18 * t9 * t6;
    result__[ 2   ] = t30 * (-t20 * t25 + ModelPars[16] * t22) / t20;
    result__[ 3   ] = t30 * (t25 - U__[0]) / ModelPars[62];
    real_type t36  = ModelPars[0];
    real_type t38  = ModelPars[22];
    real_type t46  = k__US(t14, t11);
    real_type t49  = X__[5];
    result__[ 4   ] = t18 * t9 * t6 * (t38 * t46 * t11 + t38 * t36 * t14 - t11 * t49) / ModelPars[61] / t38 / t36;
    result__[ 5   ] = t30 * (t49 - U__[1]) / ModelPars[63];
    if ( m_debug )
      Mechatronix::check_in_segment( result__, "rhs_ode", 6, i_segment );
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::Drhs_odeDx_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::Drhs_odeDx_numCols() const
  { return 6; }

  integer
  kineDyn_OCP::Drhs_odeDx_nnz() const
  { return 23; }

  void
  kineDyn_OCP::Drhs_odeDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[0 ] = 0   ; jIndex[0 ] = 0   ;
    iIndex[1 ] = 0   ; jIndex[1 ] = 1   ;
    iIndex[2 ] = 1   ; jIndex[2 ] = 0   ;
    iIndex[3 ] = 1   ; jIndex[3 ] = 1   ;
    iIndex[4 ] = 1   ; jIndex[4 ] = 2   ;
    iIndex[5 ] = 1   ; jIndex[5 ] = 4   ;
    iIndex[6 ] = 2   ; jIndex[6 ] = 0   ;
    iIndex[7 ] = 2   ; jIndex[7 ] = 1   ;
    iIndex[8 ] = 2   ; jIndex[8 ] = 2   ;
    iIndex[9 ] = 2   ; jIndex[9 ] = 3   ;
    iIndex[10] = 3   ; jIndex[10] = 0   ;
    iIndex[11] = 3   ; jIndex[11] = 1   ;
    iIndex[12] = 3   ; jIndex[12] = 2   ;
    iIndex[13] = 3   ; jIndex[13] = 3   ;
    iIndex[14] = 4   ; jIndex[14] = 0   ;
    iIndex[15] = 4   ; jIndex[15] = 1   ;
    iIndex[16] = 4   ; jIndex[16] = 2   ;
    iIndex[17] = 4   ; jIndex[17] = 4   ;
    iIndex[18] = 4   ; jIndex[18] = 5   ;
    iIndex[19] = 5   ; jIndex[19] = 0   ;
    iIndex[20] = 5   ; jIndex[20] = 1   ;
    iIndex[21] = 5   ; jIndex[21] = 2   ;
    iIndex[22] = 5   ; jIndex[22] = 5   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::Drhs_odeDx_sparse(
    NodeType const     & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[1];
    real_type t2   = sin(t1);
    real_type t3   = Q__[1];
    real_type t5   = cos(t1);
    real_type t6   = 1.0 / t5;
    real_type t7   = t6 * t3 * t2;
    result__[ 0   ] = -t7;
    real_type t9   = t3 * X__[0];
    real_type t10  = t2 * t2;
    real_type t11  = t9 - 1;
    real_type t13  = t5 * t5;
    real_type t14  = 1.0 / t13;
    result__[ 1   ] = -t14 * t11 * t10 - t9 + 1;
    real_type t16  = X__[4];
    real_type t18  = X__[2];
    real_type t19  = 1.0 / t18;
    real_type t20  = t19 * t6;
    result__[ 2   ] = -t20 * t16 * t3;
    real_type t25  = t3 * t18 * t5 + t16 * t9 - t16;
    real_type t27  = t2 * t19;
    result__[ 3   ] = -t27 * t14 * t25 + t7;
    real_type t31  = t18 * t18;
    real_type t32  = 1.0 / t31;
    result__[ 4   ] = t32 * t6 * t25 - t19 * t3;
    real_type t34  = t6 * t11;
    result__[ 5   ] = -t19 * t34;
    real_type t36  = ModelPars[17];
    real_type t37  = 1.0 / t36;
    real_type t38  = ModelPars[16];
    real_type t40  = X__[3];
    real_type t43  = (t38 * t31 - t36 * t40) * t37;
    real_type t45  = t19 * t6 * t3;
    result__[ 6   ] = t45 * t43;
    real_type t48  = t2 * t19 * t14;
    result__[ 7   ] = t48 * t11 * t43;
    real_type t52  = t32 * t34;
    result__[ 8   ] = 2 * t34 * t38 * t37 - t52 * t43;
    result__[ 9   ] = result__[5];
    real_type t55  = 1.0 / ModelPars[62];
    real_type t58  = (t40 - U__[0]) * t55;
    result__[ 10  ] = t45 * t58;
    result__[ 11  ] = t48 * t11 * t58;
    result__[ 12  ] = -t52 * t58;
    result__[ 13  ] = t20 * t11 * t55;
    real_type t62  = ModelPars[0];
    real_type t64  = ModelPars[22];
    real_type t66  = 1.0 / t64 / t62;
    real_type t68  = 1.0 / ModelPars[61];
    real_type t69  = t68 * t66;
    real_type t72  = k__US(t16, t18);
    real_type t75  = X__[5];
    real_type t77  = t64 * t62 * t16 + t64 * t72 * t18 - t18 * t75;
    result__[ 14  ] = t20 * t3 * t77 * t69;
    result__[ 15  ] = t27 * t14 * t11 * t77 * t68 * t66;
    real_type t85  = k__US_D_2(t16, t18);
    result__[ 16  ] = t20 * t11 * (t64 * t85 * t18 + t64 * t72 - t75) * t69 - t32 * t6 * t11 * t77 * t69;
    real_type t97  = k__US_D_1(t16, t18);
    result__[ 17  ] = t20 * t11 * (t64 * t97 * t18 + t64 * t62) * t69;
    result__[ 18  ] = -t6 * t11 * t68 * t66;
    real_type t107 = 1.0 / ModelPars[63];
    real_type t110 = (t75 - U__[1]) * t107;
    result__[ 19  ] = t45 * t110;
    result__[ 20  ] = t48 * t11 * t110;
    result__[ 21  ] = -t52 * t110;
    result__[ 22  ] = t20 * t11 * t107;
    if ( m_debug )
      Mechatronix::check_in_segment( result__, "Drhs_odeDxp_sparse", 23, i_segment );
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::Drhs_odeDp_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::Drhs_odeDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::Drhs_odeDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::Drhs_odeDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::Drhs_odeDp_sparse(
    NodeType const     & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::Drhs_odeDu_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::Drhs_odeDu_numCols() const
  { return 2; }

  integer
  kineDyn_OCP::Drhs_odeDu_nnz() const
  { return 2; }

  void
  kineDyn_OCP::Drhs_odeDu_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[0 ] = 3   ; jIndex[0 ] = 0   ;
    iIndex[1 ] = 5   ; jIndex[1 ] = 1   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::Drhs_odeDu_sparse(
    NodeType const     & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t6   = X__[0] * Q__[1] - 1;
    real_type t9   = cos(X__[1]);
    real_type t13  = 1.0 / X__[2] / t9;
    result__[ 0   ] = -t13 * t6 / ModelPars[62];
    result__[ 1   ] = -t13 * t6 / ModelPars[63];
    if ( m_debug )
      Mechatronix::check_in_segment( result__, "Drhs_odeDu_sparse", 2, i_segment );
  }

  /*\
   |   __  __              __  __      _       _
   |  |  \/  |__ _ ______ |  \/  |__ _| |_ _ _(_)_ __
   |  | |\/| / _` (_-<_-< | |\/| / _` |  _| '_| \ \ /
   |  |_|  |_\__,_/__/__/ |_|  |_\__,_|\__|_| |_/_\_\
  \*/

  integer
  kineDyn_OCP::A_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::A_numCols() const
  { return 6; }

  integer
  kineDyn_OCP::A_nnz() const
  { return 6; }

  void
  kineDyn_OCP::A_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[0 ] = 0   ; jIndex[0 ] = 0   ;
    iIndex[1 ] = 1   ; jIndex[1 ] = 1   ;
    iIndex[2 ] = 2   ; jIndex[2 ] = 2   ;
    iIndex[3 ] = 3   ; jIndex[3 ] = 3   ;
    iIndex[4 ] = 4   ; jIndex[4 ] = 4   ;
    iIndex[5 ] = 5   ; jIndex[5 ] = 5   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::A_sparse(
    NodeType const     & NODE__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = 1;
    result__[ 1   ] = 1;
    result__[ 2   ] = 1;
    result__[ 3   ] = 1;
    result__[ 4   ] = 1;
    result__[ 5   ] = 1;
    if ( m_debug )
      Mechatronix::check_in_segment( result__, "A_sparse", 6, i_segment );
  }

}

// EOF: kineDyn_OCP_Methods.cc
