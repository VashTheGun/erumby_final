/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP_Methods1.cc                                        |
 |                                                                       |
 |  version: 1.0   date 8/12/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "kineDyn_OCP.hh"
#include "kineDyn_OCP_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_abs_reg_DD(__t1) abs_reg.DD( __t1)
#define ALIAS_abs_reg_D(__t1) abs_reg.D( __t1)
#define ALIAS_SignReg_smooth_DD(__t1) SignReg_smooth.DD( __t1)
#define ALIAS_SignReg_smooth_D(__t1) SignReg_smooth.D( __t1)
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_GGdiagramEnvelope_DD(__t1) GGdiagramEnvelope.DD( __t1)
#define ALIAS_GGdiagramEnvelope_D(__t1) GGdiagramEnvelope.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_a__x0Control_D_3(__t1, __t2, __t3) a__x0Control.D_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2(__t1, __t2, __t3) a__x0Control.D_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1(__t1, __t2, __t3) a__x0Control.D_1( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_3_3(__t1, __t2, __t3) a__x0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_3(__t1, __t2, __t3) a__x0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_2(__t1, __t2, __t3) a__x0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_3(__t1, __t2, __t3) a__x0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_2(__t1, __t2, __t3) a__x0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_1(__t1, __t2, __t3) a__x0Control.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3(__t1, __t2, __t3) delta__D0Control.D_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2(__t1, __t2, __t3) delta__D0Control.D_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1(__t1, __t2, __t3) delta__D0Control.D_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3_3(__t1, __t2, __t3) delta__D0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_3(__t1, __t2, __t3) delta__D0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_2(__t1, __t2, __t3) delta__D0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_3(__t1, __t2, __t3) delta__D0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_2(__t1, __t2, __t3) delta__D0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_1(__t1, __t2, __t3) delta__D0Control.D_1_1( __t1, __t2, __t3)


namespace kineDyn_OCPDefine {

  /*\
   |  _   _               ___             _   _
   | | | | |___ ___ _ _  | __|  _ _ _  __| |_(_)___ _ _  ___
   | | |_| (_-</ -_) '_| | _| || | ' \/ _|  _| / _ \ ' \(_-<
   |  \___//__/\___|_|   |_| \_,_|_||_\__|\__|_\___/_||_/__/
  \*/
  // user defined functions which has a body defined in MAPLE
  real_type
  kineDyn_OCP::leftWidth( real_type zeta__XO ) const {
    return 0.3e0;
  }

  real_type
  kineDyn_OCP::leftWidth_D( real_type zeta__XO ) const {
    return 0;
  }

  real_type
  kineDyn_OCP::leftWidth_DD( real_type zeta__XO ) const {
    return 0;
  }

  real_type
  kineDyn_OCP::rightWidth( real_type zeta__XO ) const {
    return 0.3e0;
  }

  real_type
  kineDyn_OCP::rightWidth_D( real_type zeta__XO ) const {
    return 0;
  }

  real_type
  kineDyn_OCP::rightWidth_DD( real_type zeta__XO ) const {
    return 0;
  }

  real_type
  kineDyn_OCP::k__US( real_type Omega__XO, real_type v__x__XO ) const {
    real_type t1   = Omega__XO * Omega__XO;
    real_type t2   = t1 * Omega__XO;
    real_type t3   = t1 * t1;
    real_type t5   = v__x__XO * v__x__XO;
    real_type t6   = t5 * v__x__XO;
    real_type t7   = t5 * t5;
    return ModelPars[14] * t7 * t5 * t3 * t1 + ModelPars[15] * t7 * t6 * t3 * t2 + ModelPars[13] * t7 * v__x__XO * t3 * Omega__XO + ModelPars[10] * t5 * t1 + ModelPars[11] * t6 * t2 + ModelPars[12] * t7 * t3 + Omega__XO * v__x__XO * ModelPars[9] + ModelPars[8];
  }

  real_type
  kineDyn_OCP::k__US_D_1( real_type Omega__XO, real_type v__x__XO ) const {
    real_type t1   = Omega__XO * Omega__XO;
    real_type t2   = t1 * t1;
    real_type t4   = v__x__XO * v__x__XO;
    real_type t5   = t4 * t4;
    return (7 * ModelPars[15] * t5 * t4 * t2 * t1 + 4 * ModelPars[12] * t4 * v__x__XO * t1 * Omega__XO + 6 * ModelPars[14] * t5 * v__x__XO * t2 * Omega__XO + 3 * ModelPars[11] * t4 * t1 + 5 * ModelPars[13] * t5 * t2 + 2 * Omega__XO * v__x__XO * ModelPars[10] + ModelPars[9]) * v__x__XO;
  }

  real_type
  kineDyn_OCP::k__US_D_1_1( real_type Omega__XO, real_type v__x__XO ) const {
    real_type t1   = v__x__XO * v__x__XO;
    real_type t2   = Omega__XO * Omega__XO;
    real_type t3   = t2 * t2;
    real_type t5   = t1 * t1;
    return 2 * (10 * ModelPars[13] * t1 * v__x__XO * t2 * Omega__XO + 21 * ModelPars[15] * t5 * v__x__XO * t3 * Omega__XO + 6 * ModelPars[12] * t1 * t2 + 15 * ModelPars[14] * t5 * t3 + 3 * Omega__XO * v__x__XO * ModelPars[11] + ModelPars[10]) * t1;
  }

  real_type
  kineDyn_OCP::k__US_D_1_2( real_type Omega__XO, real_type v__x__XO ) const {
    real_type t1   = Omega__XO * Omega__XO;
    real_type t2   = t1 * t1;
    real_type t4   = v__x__XO * v__x__XO;
    real_type t5   = t4 * t4;
    return 49 * ModelPars[15] * t5 * t4 * t2 * t1 + 16 * ModelPars[12] * t4 * v__x__XO * t1 * Omega__XO + 36 * ModelPars[14] * t5 * v__x__XO * t2 * Omega__XO + 9 * ModelPars[11] * t4 * t1 + 25 * ModelPars[13] * t5 * t2 + 4 * Omega__XO * v__x__XO * ModelPars[10] + ModelPars[9];
  }

  real_type
  kineDyn_OCP::k__US_D_2( real_type Omega__XO, real_type v__x__XO ) const {
    real_type t1   = Omega__XO * Omega__XO;
    real_type t2   = t1 * t1;
    real_type t4   = v__x__XO * v__x__XO;
    real_type t5   = t4 * t4;
    return (7 * ModelPars[15] * t5 * t4 * t2 * t1 + 4 * ModelPars[12] * t4 * v__x__XO * t1 * Omega__XO + 6 * ModelPars[14] * t5 * v__x__XO * t2 * Omega__XO + 3 * ModelPars[11] * t4 * t1 + 5 * ModelPars[13] * t5 * t2 + 2 * Omega__XO * v__x__XO * ModelPars[10] + ModelPars[9]) * Omega__XO;
  }

  real_type
  kineDyn_OCP::k__US_D_2_2( real_type Omega__XO, real_type v__x__XO ) const {
    real_type t1   = Omega__XO * Omega__XO;
    real_type t2   = t1 * t1;
    real_type t4   = v__x__XO * v__x__XO;
    real_type t5   = t4 * t4;
    return 2 * (10 * ModelPars[13] * t4 * v__x__XO * t1 * Omega__XO + 21 * ModelPars[15] * t5 * v__x__XO * t2 * Omega__XO + 6 * ModelPars[12] * t4 * t1 + 15 * ModelPars[14] * t5 * t2 + 3 * Omega__XO * v__x__XO * ModelPars[11] + ModelPars[10]) * t1;
  }

  real_type
  kineDyn_OCP::axmax( real_type v__x__XO ) const {
    real_type t1   = v__x__XO * v__x__XO;
    real_type t2   = t1 * v__x__XO;
    real_type t3   = t1 * t1;
    return ModelPars[48] * t3 * t1 + ModelPars[49] * t3 * t2 + ModelPars[47] * t3 * v__x__XO + ModelPars[44] * t1 + ModelPars[45] * t2 + ModelPars[46] * t3 + v__x__XO * ModelPars[43] + ModelPars[42];
  }

  real_type
  kineDyn_OCP::axmax_D( real_type v__x__XO ) const {
    real_type t1   = v__x__XO * v__x__XO;
    real_type t2   = t1 * t1;
    return 7 * ModelPars[49] * t2 * t1 + 4 * ModelPars[46] * t1 * v__x__XO + 6 * ModelPars[48] * t2 * v__x__XO + 3 * ModelPars[45] * t1 + 5 * ModelPars[47] * t2 + 2 * v__x__XO * ModelPars[44] + ModelPars[43];
  }

  real_type
  kineDyn_OCP::axmax_DD( real_type v__x__XO ) const {
    real_type t1   = v__x__XO * v__x__XO;
    real_type t2   = t1 * t1;
    return 20 * ModelPars[47] * t1 * v__x__XO + 42 * ModelPars[49] * t2 * v__x__XO + 12 * ModelPars[46] * t1 + 30 * ModelPars[48] * t2 + 6 * v__x__XO * ModelPars[45] + 2 * ModelPars[44];
  }

  real_type
  kineDyn_OCP::axmin( real_type v__x__XO ) const {
    real_type t1   = v__x__XO * v__x__XO;
    real_type t2   = t1 * t1;
    return ModelPars[53] * t1 * v__x__XO + ModelPars[52] * t1 + ModelPars[54] * t2 + v__x__XO * ModelPars[51] + ModelPars[50];
  }

  real_type
  kineDyn_OCP::axmin_D( real_type v__x__XO ) const {
    real_type t1   = v__x__XO * v__x__XO;
    return 4 * ModelPars[54] * t1 * v__x__XO + 3 * ModelPars[53] * t1 + 2 * v__x__XO * ModelPars[52] + ModelPars[51];
  }

  real_type
  kineDyn_OCP::axmin_DD( real_type v__x__XO ) const {
    real_type t1   = v__x__XO * v__x__XO;
    return 12 * ModelPars[54] * t1 + 6 * v__x__XO * ModelPars[53] + 2 * ModelPars[52];
  }

  real_type
  kineDyn_OCP::aymax( real_type v__x__XO ) const {
    real_type t1   = v__x__XO * v__x__XO;
    real_type t2   = t1 * t1;
    return ModelPars[58] * t1 * v__x__XO + ModelPars[60] * t2 * v__x__XO + ModelPars[57] * t1 + ModelPars[59] * t2 + v__x__XO * ModelPars[56] + ModelPars[55];
  }

  real_type
  kineDyn_OCP::aymax_D( real_type v__x__XO ) const {
    real_type t1   = v__x__XO * v__x__XO;
    real_type t2   = t1 * t1;
    return 4 * ModelPars[59] * t1 * v__x__XO + 3 * ModelPars[58] * t1 + 5 * ModelPars[60] * t2 + 2 * v__x__XO * ModelPars[57] + ModelPars[56];
  }

  real_type
  kineDyn_OCP::aymax_DD( real_type v__x__XO ) const {
    real_type t1   = v__x__XO * v__x__XO;
    return 20 * ModelPars[60] * t1 * v__x__XO + 12 * ModelPars[59] * t1 + 6 * v__x__XO * ModelPars[58] + 2 * ModelPars[57];
  }

  real_type
  kineDyn_OCP::negSign( real_type zeta__XO ) const {
    real_type t1   = SignReg_smooth(zeta__XO);
    return t1 / 2 - 1.0 / 2.0;
  }

  real_type
  kineDyn_OCP::negSign_D( real_type zeta__XO ) const {
    real_type t1   = ALIAS_SignReg_smooth_D(zeta__XO);
    return t1 / 2;
  }

  real_type
  kineDyn_OCP::negSign_DD( real_type zeta__XO ) const {
    real_type t1   = ALIAS_SignReg_smooth_DD(zeta__XO);
    return t1 / 2;
  }

  real_type
  kineDyn_OCP::posSign( real_type zeta__XO ) const {
    real_type t1   = SignReg_smooth(zeta__XO);
    return t1 / 2 + 1.0 / 2.0;
  }

  real_type
  kineDyn_OCP::posSign_D( real_type zeta__XO ) const {
    real_type t1   = ALIAS_SignReg_smooth_D(zeta__XO);
    return t1 / 2;
  }

  real_type
  kineDyn_OCP::posSign_DD( real_type zeta__XO ) const {
    real_type t1   = ALIAS_SignReg_smooth_DD(zeta__XO);
    return t1 / 2;
  }

  real_type
  kineDyn_OCP::negSign_sharp( real_type zeta__XO ) const {
    real_type t1   = SignReg(zeta__XO);
    return t1 / 2 - 1.0 / 2.0;
  }

  real_type
  kineDyn_OCP::negSign_sharp_D( real_type zeta__XO ) const {
    real_type t1   = ALIAS_SignReg_D(zeta__XO);
    return t1 / 2;
  }

  real_type
  kineDyn_OCP::negSign_sharp_DD( real_type zeta__XO ) const {
    real_type t1   = ALIAS_SignReg_DD(zeta__XO);
    return t1 / 2;
  }

  real_type
  kineDyn_OCP::posSign_sharp( real_type zeta__XO ) const {
    real_type t1   = SignReg(zeta__XO);
    return t1 / 2 + 1.0 / 2.0;
  }

  real_type
  kineDyn_OCP::posSign_sharp_D( real_type zeta__XO ) const {
    real_type t1   = ALIAS_SignReg_D(zeta__XO);
    return t1 / 2;
  }

  real_type
  kineDyn_OCP::posSign_sharp_DD( real_type zeta__XO ) const {
    real_type t1   = ALIAS_SignReg_DD(zeta__XO);
    return t1 / 2;
  }


  /*\
   |  _  _            _ _ _            _
   | | || |__ _ _ __ (_) | |_ ___ _ _ (_)__ _ _ _
   | | __ / _` | '  \| | |  _/ _ \ ' \| / _` | ' \
   | |_||_\__,_|_|_|_|_|_|\__\___/_||_|_\__,_|_||_|
   |
  \*/

  real_type
  kineDyn_OCP::H_eval(
    integer              i_segment,
    CellType const &     CELL__,
    P_const_pointer_type P__
  ) const {
    integer        i_cell = CELL__.i_cell;
    real_type const * Q__ = CELL__.qM;
    real_type const * X__ = CELL__.xM;
    real_type const * L__ = CELL__.lambdaM;
    real_type const * U__ = CELL__.uM;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[0];
    real_type t2   = Q__[1];
    real_type t3   = t2 * t1;
    real_type t4   = t3 - 1;
    real_type t5   = X__[1];
    real_type t6   = cos(t5);
    real_type t7   = 1.0 / t6;
    real_type t8   = t7 * t4;
    real_type t9   = X__[2];
    real_type t10  = 1.0 / t9;
    real_type t11  = ModelPars[64];
    real_type t12  = Q__[0];
    real_type t13  = rightWidth(t12);
    real_type t15  = roadRightLateralBoundaries(t1 - t11 + t13);
    real_type t18  = leftWidth(t12);
    real_type t20  = roadLeftLateralBoundaries(t18 - t1 - t11);
    real_type t23  = aymax(t9);
    real_type t25  = X__[4];
    real_type t27  = abs_reg(t9 * t25);
    real_type t28  = t27 / t23;
    real_type t29  = ModelPars[21];
    real_type t30  = pow(t28, t29);
    real_type t31  = axmax(t9);
    real_type t33  = X__[3];
    real_type t35  = t33 - ModelPars[7];
    real_type t36  = abs_reg(t35);
    real_type t38  = pow(t36 / t31, t29);
    real_type t40  = SignReg(t35);
    real_type t44  = ModelPars[20];
    real_type t45  = pow(t28, t44);
    real_type t46  = axmin(t9);
    real_type t49  = pow(t36 / t46, t44);
    real_type t55  = GGdiagramEnvelope(1 - (t40 / 2 + 1.0 / 2.0) * (t30 + t38) + (t40 / 2 - 1.0 / 2.0) * (t45 + t49));
    real_type t61  = pow(t9 - ModelPars[24], 2);
    real_type t64  = t1 * t1;
    real_type t71  = sin(t5);
    real_type t80  = t10 * t7;
    real_type t83  = ModelPars[17];
    real_type t86  = t9 * t9;
    real_type t92  = t10 * t8;
    real_type t103 = ModelPars[0];
    real_type t106 = ModelPars[22];
    real_type t114 = k__US(t25, t9);
    real_type t117 = X__[5];
    return -t15 * t10 * t8 - t20 * t10 * t8 - t55 * t10 * t8 - (ModelPars[29] * t61 + ModelPars[27] * t64 + ModelPars[28]) * t10 * t8 - t8 * t71 * L__[0] - t80 * (t2 * t9 * t6 + t25 * t3 - t25) * L__[1] + t92 * (-t83 * t33 + ModelPars[16] * t86) / t83 * L__[2] + t92 * (t33 - U__[0]) / ModelPars[62] * L__[3] + t80 * t4 * (t106 * t103 * t25 + t106 * t114 * t9 - t9 * t117) / ModelPars[61] / t106 / t103 * L__[4] + t92 * (t117 - U__[1]) / ModelPars[63] * L__[5];
  }

  /*\
   |   ___               _ _   _
   |  | _ \___ _ _  __ _| | |_(_)___ ___
   |  |  _/ -_) ' \/ _` | |  _| / -_|_-<
   |  |_| \___|_||_\__,_|_|\__|_\___/__/
  \*/

  real_type
  kineDyn_OCP::penalties_eval(
    NodeType const     & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[0];
    real_type t6   = cos(X__[1]);
    real_type t8   = 1.0 / t6 * (Q__[1] * t1 - 1);
    real_type t9   = X__[2];
    real_type t10  = 1.0 / t9;
    real_type t11  = ModelPars[64];
    real_type t12  = Q__[0];
    real_type t13  = rightWidth(t12);
    real_type t15  = roadRightLateralBoundaries(t1 - t11 + t13);
    real_type t18  = leftWidth(t12);
    real_type t20  = roadLeftLateralBoundaries(t18 - t1 - t11);
    real_type t23  = aymax(t9);
    real_type t27  = abs_reg(t9 * X__[4]);
    real_type t28  = t27 / t23;
    real_type t29  = ModelPars[21];
    real_type t30  = pow(t28, t29);
    real_type t31  = axmax(t9);
    real_type t35  = X__[3] - ModelPars[7];
    real_type t36  = abs_reg(t35);
    real_type t38  = pow(t36 / t31, t29);
    real_type t40  = SignReg(t35);
    real_type t44  = ModelPars[20];
    real_type t45  = pow(t28, t44);
    real_type t46  = axmin(t9);
    real_type t49  = pow(t36 / t46, t44);
    real_type t55  = GGdiagramEnvelope(1 - (t40 / 2 + 1.0 / 2.0) * (t30 + t38) + (t40 / 2 - 1.0 / 2.0) * (t45 + t49));
    return -t15 * t10 * t8 - t20 * t10 * t8 - t55 * t10 * t8;
  }

  real_type
  kineDyn_OCP::control_penalties_eval(
    NodeType const     & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t6   = cos(X__[1]);
    real_type t8   = 1.0 / t6 * (Q__[1] * X__[0] - 1);
    real_type t10  = 1.0 / X__[2];
    real_type t14  = delta__D0Control(U__[1], ModelPars[41], ModelPars[40]);
    real_type t20  = a__x0Control(U__[0], ModelPars[6], ModelPars[5]);
    return -t14 * t10 * t8 - t20 * t10 * t8;
  }

  /*\
   |   _
   |  | |   __ _ __ _ _ _ __ _ _ _  __ _ ___
   |  | |__/ _` / _` | '_/ _` | ' \/ _` / -_)
   |  |____\__,_\__, |_| \__,_|_||_\__, \___|
   |            |___/              |___/
  \*/

  real_type
  kineDyn_OCP::lagrange_target(
    NodeType const     & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = X__[0];
    real_type t6   = cos(X__[1]);
    real_type t9   = X__[2];
    real_type t14  = pow(t9 - ModelPars[24], 2);
    real_type t17  = t1 * t1;
    return -(ModelPars[29] * t14 + ModelPars[27] * t17 + ModelPars[28]) / t9 / t6 * (Q__[1] * t1 - 1);
  }

  /*\
   |   __  __
   |  |  \/  |__ _ _  _ ___ _ _
   |  | |\/| / _` | || / -_) '_|
   |  |_|  |_\__,_|\_, \___|_|
   |               |__/
  \*/

  real_type
  kineDyn_OCP::mayer_target(
    NodeType const     & LEFT__,
    NodeType const     & RIGHT__,
    P_const_pointer_type P__
  ) const {
    integer i_segment_left  = LEFT__.i_segment;
    real_type const * QL__  = LEFT__.q;
    real_type const * XL__  = LEFT__.x;
    integer i_segment_right = RIGHT__.i_segment;
    real_type const * QR__  = RIGHT__.q;
    real_type const * XR__  = RIGHT__.x;
    Path2D::SegmentClass const & segmentLeft  = pTrajectory->getSegmentByIndex(i_segment_left);
    Path2D::SegmentClass const & segmentRight = pTrajectory->getSegmentByIndex(i_segment_right);
    real_type t4   = pow(XL__[2] - ModelPars[26], 2);
    real_type t10  = pow(XL__[3] - ModelPars[4], 2);
    real_type t16  = pow(XL__[4] - ModelPars[1], 2);
    real_type t22  = pow(XL__[5] - ModelPars[39], 2);
    real_type t28  = pow(XL__[0] - ModelPars[19], 2);
    real_type t34  = pow(XL__[1] - ModelPars[31], 2);
    real_type t40  = pow(XR__[2] - ModelPars[25], 2);
    real_type t46  = pow(XR__[0] - ModelPars[18], 2);
    real_type t52  = pow(XR__[1] - ModelPars[30], 2);
    return ModelPars[35] * t10 + ModelPars[34] * t16 + ModelPars[36] * t22 + ModelPars[3] * t28 + ModelPars[38] * t34 + ModelPars[37] * t4 + ModelPars[32] * t40 + ModelPars[2] * t46 + ModelPars[33] * t52;
  }

  /*\
   |    ___
   |   / _ \
   |  | (_) |
   |   \__\_\
  \*/

  integer
  kineDyn_OCP::q_numEqns() const
  { return 5; }

  void
  kineDyn_OCP::q_eval(
    integer        i_node,
    integer        i_segment,
    real_type      s,
    Q_pointer_type result__
  ) const {
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = s;
    result__[ 1   ] = ALIAS_Curv(s);
    result__[ 2   ] = ALIAS_xLane(s);
    result__[ 3   ] = ALIAS_yLane(s);
    result__[ 4   ] = ALIAS_theta(s);
    Mechatronix::check_in_node( result__.pointer(),"q_eval",5, i_node );
  }

  /*\
   |    ___
   |   / __|_  _ ___ ______
   |  | (_ | || / -_|_-<_-<
   |   \___|\_,_\___/__/__/
  \*/

  integer
  kineDyn_OCP::u_guess_numEqns() const
  { return 2; }

  void
  kineDyn_OCP::u_guess_eval(
    NodeType2 const    & NODE__,
    P_const_pointer_type P__,
    U_pointer_type       UGUESS__
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
      Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    std::fill_n( UGUESS__.pointer(), 2, 0 );
    UGUESS__[ iU_a__x0     ] = 0;
    UGUESS__[ iU_delta__D0 ] = 0;
    if ( m_debug )
      Mechatronix::check_in_segment( UGUESS__.pointer(), "u_guess_eval", 2, i_segment );
  }

  void
  kineDyn_OCP::u_guess_eval(
    NodeType2 const    & LEFT__,
    NodeType2 const    & RIGHT__,
    P_const_pointer_type P__,
    U_pointer_type       UGUESS__
  ) const {
    NodeType2 NODE__;
    real_type Q__[5];
    real_type X__[6];
    real_type L__[6];
    NODE__.i_segment = LEFT__.i_segment;
    NODE__.q      = Q__;
    NODE__.x      = X__;
    NODE__.lambda = L__;
    // Qvars
    Q__[0] = (LEFT__.q[0]+RIGHT__.q[0])/2;
    Q__[1] = (LEFT__.q[1]+RIGHT__.q[1])/2;
    Q__[2] = (LEFT__.q[2]+RIGHT__.q[2])/2;
    Q__[3] = (LEFT__.q[3]+RIGHT__.q[3])/2;
    Q__[4] = (LEFT__.q[4]+RIGHT__.q[4])/2;
    // Xvars
    X__[0] = (LEFT__.x[0]+RIGHT__.x[0])/2;
    X__[1] = (LEFT__.x[1]+RIGHT__.x[1])/2;
    X__[2] = (LEFT__.x[2]+RIGHT__.x[2])/2;
    X__[3] = (LEFT__.x[3]+RIGHT__.x[3])/2;
    X__[4] = (LEFT__.x[4]+RIGHT__.x[4])/2;
    X__[5] = (LEFT__.x[5]+RIGHT__.x[5])/2;
    // Lvars
    L__[0] = (LEFT__.lambda[0]+RIGHT__.lambda[0])/2;
    L__[1] = (LEFT__.lambda[1]+RIGHT__.lambda[1])/2;
    L__[2] = (LEFT__.lambda[2]+RIGHT__.lambda[2])/2;
    L__[3] = (LEFT__.lambda[3]+RIGHT__.lambda[3])/2;
    L__[4] = (LEFT__.lambda[4]+RIGHT__.lambda[4])/2;
    L__[5] = (LEFT__.lambda[5]+RIGHT__.lambda[5])/2;
    this->u_guess_eval( NODE__, P__, UGUESS__ );
  }

  /*\
   |    ___ _           _
   |   / __| |_  ___ __| |__
   |  | (__| ' \/ -_) _| / /
   |   \___|_||_\___\__|_\_\
  \*/

  bool
  kineDyn_OCP::u_check_if_admissible(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__
  ) const {
    bool ok = true;
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    ok = ok && a__x0Control.check_range(U__[0], ModelPars[6], ModelPars[5]);
    ok = ok && delta__D0Control.check_range(U__[1], ModelPars[41], ModelPars[40]);
    return ok;
  }

  /*\
   |   ___        _     ___                       _
   |  | _ \___ __| |_  | _ \_ _ ___  __ ___ _____(_)_ _  __ _
   |  |  _/ _ (_-<  _| |  _/ '_/ _ \/ _/ -_|_-<_-< | ' \/ _` |
   |  |_| \___/__/\__| |_| |_| \___/\__\___/__/__/_|_||_\__, |
   |                                                    |___/
  \*/

  integer
  kineDyn_OCP::post_numEqns() const
  { return 20; }

  void
  kineDyn_OCP::post_eval(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = delta__D0Control(U__[1], ModelPars[41], ModelPars[40]);
    result__[ 1   ] = a__x0Control(U__[0], ModelPars[6], ModelPars[5]);
    real_type t7   = X__[0];
    real_type t8   = ModelPars[64];
    real_type t9   = Q__[0];
    real_type t10  = rightWidth(t9);
    result__[ 2   ] = roadRightLateralBoundaries(t7 - t8 + t10);
    real_type t12  = leftWidth(t9);
    result__[ 3   ] = roadLeftLateralBoundaries(t12 - t7 - t8);
    real_type t14  = X__[2];
    real_type t15  = aymax(t14);
    real_type t17  = X__[4];
    real_type t18  = t14 * t17;
    real_type t19  = abs_reg(t18);
    real_type t20  = t19 / t15;
    real_type t21  = ModelPars[21];
    real_type t22  = pow(t20, t21);
    real_type t23  = axmax(t14);
    real_type t25  = X__[3];
    real_type t27  = t25 - ModelPars[7];
    real_type t28  = abs_reg(t27);
    real_type t30  = pow(t28 / t23, t21);
    real_type t32  = SignReg(t27);
    real_type t36  = ModelPars[20];
    real_type t37  = pow(t20, t36);
    real_type t38  = axmin(t14);
    real_type t41  = pow(t28 / t38, t36);
    result__[ 4   ] = GGdiagramEnvelope(1 - (t32 / 2 + 1.0 / 2.0) * (t22 + t30) + (t32 / 2 - 1.0 / 2.0) * (t37 + t41));
    result__[ 5   ] = t18;
    result__[ 6   ] = k__US(t17, t14);
    real_type t51  = X__[1];
    real_type t52  = cos(t51);
    result__[ 7   ] = -t14 * t52 / (t7 * Q__[1] - 1);
    real_type t55  = Q__[2];
    real_type t56  = Q__[4];
    real_type t57  = sin(t56);
    result__[ 8   ] = -t57 * t7 + t55;
    real_type t59  = Q__[3];
    real_type t60  = cos(t56);
    result__[ 9   ] = t60 * t7 + t59;
    real_type t62  = t7 + t8;
    result__[ 10  ] = -t57 * t62 + t55;
    result__[ 11  ] = t60 * t62 + t59;
    real_type t65  = t7 - t8;
    result__[ 12  ] = -t57 * t65 + t55;
    result__[ 13  ] = t60 * t65 + t59;
    result__[ 14  ] = -t57 * t12 + t55;
    result__[ 15  ] = t60 * t12 + t59;
    result__[ 16  ] = t57 * t10 + t55;
    result__[ 17  ] = -t60 * t10 + t59;
    result__[ 18  ] = t56 + t51;
    real_type t74  = pow(t14 - ModelPars[26], 2);
    real_type t79  = pow(t25 - ModelPars[4], 2);
    real_type t84  = pow(t17 - ModelPars[1], 2);
    real_type t90  = pow(X__[5] - ModelPars[39], 2);
    real_type t95  = pow(t7 - ModelPars[19], 2);
    real_type t100 = pow(t51 - ModelPars[31], 2);
    real_type t105 = pow(t14 - ModelPars[25], 2);
    real_type t110 = pow(t7 - ModelPars[18], 2);
    real_type t115 = pow(t51 - ModelPars[30], 2);
    result__[ 19  ] = ModelPars[38] * t100 + ModelPars[32] * t105 + ModelPars[2] * t110 + ModelPars[33] * t115 + ModelPars[37] * t74 + ModelPars[35] * t79 + ModelPars[34] * t84 + ModelPars[36] * t90 + ModelPars[3] * t95;
    Mechatronix::check_in_segment( result__, "post_eval", 20, i_segment );
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::integrated_post_numEqns() const
  { return 1; }

  void
  kineDyn_OCP::integrated_post_eval(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t6   = cos(X__[1]);
    result__[ 0   ] = -1.0 / X__[2] / t6 * (Q__[1] * X__[0] - 1);
    Mechatronix::check_in_segment( result__, "integrated_post_eval", 1, i_segment );
  }

}

// EOF: kineDyn_OCP_Methods1.cc
