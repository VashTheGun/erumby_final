/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP.cc                                                 |
 |                                                                       |
 |  version: 1.0   date 8/12/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


// use pragma to include libraries
#include <MechatronixCore/MechatronixLibs.hh>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
#ifdef __clang__
#pragma clang diagnostic ignored "-Wunused-parameter"
#endif

#include "kineDyn_OCP.hh"
#include "kineDyn_OCP_Pars.hh"

#include <time.h> /* time_t, struct tm, time, localtime, asctime */

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
#ifdef __clang__
#pragma clang diagnostic ignored "-Wunused-parameter"
#endif

namespace kineDyn_OCPDefine {

  using GenericContainerNamespace::vec_string_type;

  /*
  //   _ __   __ _ _ __ ___   ___  ___
  //  | '_ \ / _` | '_ ` _ \ / _ \/ __|
  //  | | | | (_| | | | | | |  __/\__ \
  //  |_| |_|\__,_|_| |_| |_|\___||___/
  //
  */

  char const *namesXvars[numXvars+1] = {
    "n",
    "xi",
    "v__x",
    "a__x",
    "Omega",
    "delta__D",
    nullptr
  };

  char const *namesLvars[numLvars+1] = {
    "lambda1__xo",
    "lambda2__xo",
    "lambda3__xo",
    "lambda4__xo",
    "lambda5__xo",
    "lambda6__xo",
    nullptr
  };

  char const *namesUvars[numUvars+1] = {
    "a__x0",
    "delta__D0",
    nullptr
  };

  char const *namesQvars[numQvars+1] = {
    "zeta",
    "Curv",
    "xLane",
    "yLane",
    "theta",
    nullptr
  };

  char const *namesPvars[numPvars+1] = {
    nullptr
  };

  char const *namesOMEGAvars[numOMEGAvars+1] = {
    nullptr
  };

  char const *namesPostProcess[numPostProcess+1] = {
    "delta__D0Control",
    "a__x0Control",
    "roadRightLateralBoundaries",
    "roadLeftLateralBoundaries",
    "GGdiagramEnvelope",
    "ay_SS",
    "Kus_fcn",
    "s_dot",
    "xCoMCar",
    "yCoMCar",
    "xLeftCar",
    "yLeftCar",
    "xRightCar",
    "yRightCar",
    "xLeftEdge",
    "yLeftEdge",
    "xRightEdge",
    "yRightEdge",
    "attitude_psi",
    "mayer_target",
    nullptr
  };

  char const *namesIntegratedPostProcess[numIntegratedPostProcess+1] = {
    "time",
    nullptr
  };

  char const *namesModelPars[numModelPars+1] = {
    "L",
    "Omega_i",
    "WBCF__n",
    "WBCI__n",
    "a_xi",
    "ax0_max",
    "ax0_min",
    "axoffs",
    "kUS_0",
    "kUS_1",
    "kUS_2",
    "kUS_3",
    "kUS_4",
    "kUS_5",
    "kUS_6",
    "kUS_7",
    "k__D",
    "m",
    "n_f",
    "n_i",
    "nlow",
    "nupp",
    "tau__D",
    "v__x0",
    "v_lim",
    "v_xf",
    "v_xi",
    "wN",
    "wT",
    "wU",
    "xi_f",
    "xi_i",
    "WBCF__vx",
    "WBCF__xi",
    "WBCI__Omega",
    "WBCI__ax",
    "WBCI__delta",
    "WBCI__vx",
    "WBCI__xi",
    "deltaD_i",
    "delta_max",
    "delta_min",
    "fit_axM_0",
    "fit_axM_1",
    "fit_axM_2",
    "fit_axM_3",
    "fit_axM_4",
    "fit_axM_5",
    "fit_axM_6",
    "fit_axM_7",
    "fit_axm_0",
    "fit_axm_1",
    "fit_axm_2",
    "fit_axm_3",
    "fit_axm_4",
    "fit_ayM_0",
    "fit_ayM_1",
    "fit_ayM_2",
    "fit_ayM_3",
    "fit_ayM_4",
    "fit_ayM_5",
    "tau__Omega",
    "tau__a__x",
    "tau__delta",
    "vehHalfWidth",
    nullptr
  };

  char const *namesConstraint1D[numConstraint1D+1] = {
    "roadRightLateralBoundaries",
    "roadLeftLateralBoundaries",
    "GGdiagramEnvelope",
    nullptr
  };

  char const *namesConstraint2D[numConstraint2D+1] = {
    nullptr
  };

  char const *namesConstraintU[numConstraintU+1] = {
    "delta__D0Control",
    "a__x0Control",
    nullptr
  };

  char const *namesBc[numBc+1] = {
    nullptr
  };

  /* --------------------------------------------------------------------------
  //    ___             _               _
  //   / __|___ _ _  __| |_ _ _ _  _ __| |_ ___ _ _
  //  | (__/ _ \ ' \(_-<  _| '_| || / _|  _/ _ \ '_|
  //   \___\___/_||_/__/\__|_|  \_,_\__|\__\___/_|
  */
  kineDyn_OCP::kineDyn_OCP(
    string  const & name,
    ThreadPool    * _TP,
    Console const * _pConsole
  )
  : Discretized_Indirect_OCP( name, _TP, _pConsole )
  // Controls
  , delta__D0Control("delta__D0Control")
  , a__x0Control("a__x0Control")
  // Constraints 1D
  , roadRightLateralBoundaries("roadRightLateralBoundaries")
  , roadLeftLateralBoundaries("roadLeftLateralBoundaries")
  , GGdiagramEnvelope("GGdiagramEnvelope")
  // Constraints 2D
  // User classes
  {
    this->U_solve_iterative = false;

    // Initialize to NaN all the ModelPars
    std::fill( ModelPars, ModelPars + numModelPars, Utils::NaN<real_type>() );

    // Initialize string of names
    setupNames(
      numPvars,                 namesPvars,
      numXvars,                 namesXvars,
      numLvars,                 namesLvars,
      numUvars,                 namesUvars,
      numQvars,                 namesQvars,
      numPostProcess,           namesPostProcess,
      numIntegratedPostProcess, namesIntegratedPostProcess,
      numBc,                    namesBc
    );
    //pSolver = &this->solverNewtonDumped;
    pSolver = &this->solverHyness;

    #ifdef LAPACK_WRAPPER_USE_OPENBLAS
    openblas_set_num_threads(1);
    goto_set_num_threads(1);
    #endif
  }

  kineDyn_OCP::~kineDyn_OCP() {
  }

  /* --------------------------------------------------------------------------
  //                  _       _       ____  _
  //  _   _ _ __   __| | __ _| |_ ___|  _ \| |__   __ _ ___  ___
  // | | | | '_ \ / _` |/ _` | __/ _ \ |_) | '_ \ / _` / __|/ _ \
  // | |_| | |_) | (_| | (_| | ||  __/  __/| | | | (_| \__ \  __/
  //  \__,_| .__/ \__,_|\__,_|\__\___|_|   |_| |_|\__,_|___/\___|
  //       |_|
  */
  void
  kineDyn_OCP::updateContinuation( integer phase, real_type s ) {
  }

  /* --------------------------------------------------------------------------
  //           _               ____                                _
  //  ___  ___| |_ _   _ _ __ |  _ \ __ _ _ __ __ _ _ __ ___   ___| |_ ___ _ __ ___
  // / __|/ _ \ __| | | | '_ \| |_) / _` | '__/ _` | '_ ` _ \ / _ \ __/ _ \ '__/ __|
  // \__ \  __/ |_| |_| | |_) |  __/ (_| | | | (_| | | | | | |  __/ ||  __/ |  \__ \
  // |___/\___|\__|\__,_| .__/|_|   \__,_|_|  \__,_|_| |_| |_|\___|\__\___|_|  |___/
  //                    |_|
  // initialize parameters using associative array
  */
  void
  kineDyn_OCP::setupParameters( GenericContainer const & gc_data ) {
    UTILS_ASSERT0(
      gc_data.exists("Parameters"),
      "kineDyn_OCP::setupParameters: Missing key `Parameters` in data\n"
    );
    GenericContainer const & gc = gc_data("Parameters");

    bool allfound = true;
    for ( integer i = 0; i < numModelPars; ++i ) {
      char const * namei = namesModelPars[i];
      if ( gc.exists( namei ) ) {
        ModelPars[i] = gc(namei).get_number();
      } else {
        m_console->error( fmt::format( "Missing parameter: '{}'\n", namei ) );
        allfound = false;
      }
    }
    UTILS_ASSERT0(
      allfound, "in kineDyn_OCP::setup not all parameters are set!\n"
    );
  }

  void
  kineDyn_OCP::setupParameters( real_type const Pars[] ) {
    std::copy( Pars, Pars + numModelPars, ModelPars );
  }

  /* --------------------------------------------------------------------------
  //            _                ____ _
  //   ___  ___| |_ _   _ _ __  / ___| | __ _ ___ ___  ___  ___
  //  / __|/ _ \ __| | | | '_ \| |   | |/ _` / __/ __|/ _ \/ __|
  //  \__ \  __/ |_| |_| | |_) | |___| | (_| \__ \__ \  __/\__ \
  //  |___/\___|\__|\__,_| .__/ \____|_|\__,_|___/___/\___||___/
  //                     |_|
  */
  void
  kineDyn_OCP::setupClasses( GenericContainer const & gc_data ) {
    UTILS_ASSERT0(
      gc_data.exists("Constraints"),
      "kineDyn_OCP::setupClasses: Missing key `Parameters` in data\n"
    );
    GenericContainer const & gc = gc_data("Constraints");
    // Initialize Constraints 1D
    UTILS_ASSERT0(
      gc.exists("roadRightLateralBoundaries"),
      "in kineDyn_OCP::setupClasses(gc) missing key: ``roadRightLateralBoundaries''\n"
    );
    roadRightLateralBoundaries.setup( gc("roadRightLateralBoundaries") );

    UTILS_ASSERT0(
      gc.exists("roadLeftLateralBoundaries"),
      "in kineDyn_OCP::setupClasses(gc) missing key: ``roadLeftLateralBoundaries''\n"
    );
    roadLeftLateralBoundaries.setup( gc("roadLeftLateralBoundaries") );

    UTILS_ASSERT0(
      gc.exists("GGdiagramEnvelope"),
      "in kineDyn_OCP::setupClasses(gc) missing key: ``GGdiagramEnvelope''\n"
    );
    GGdiagramEnvelope.setup( gc("GGdiagramEnvelope") );

  }

  /* --------------------------------------------------------------------------
  //           _               _   _                ____ _
  //  ___  ___| |_ _   _ _ __ | | | |___  ___ _ __ / ___| | __ _ ___ ___  ___  ___
  // / __|/ _ \ __| | | | '_ \| | | / __|/ _ \ '__| |   | |/ _` / __/ __|/ _ \/ __|
  // \__ \  __/ |_| |_| | |_) | |_| \__ \  __/ |  | |___| | (_| \__ \__ \  __/\__ \
  // |___/\___|\__|\__,_| .__/ \___/|___/\___|_|   \____|_|\__,_|___/___/\___||___/
  //                    |_|
  */
  void
  kineDyn_OCP::setupUserClasses( GenericContainer const & gc ) {
  }

  /* --------------------------------------------------------------------------
  //           _             _   _
  //   ___ ___| |_ _  _ _ __| | | |___ ___ _ _
  //  (_-</ -_)  _| || | '_ \ |_| (_-</ -_) '_|
  //  /__/\___|\__|\_,_| .__/\___//__/\___|_|
  //                   |_|
  //   __  __                        _ ___             _   _
  //  |  \/  |__ _ _ __ _ __  ___ __| | __|  _ _ _  __| |_(_)___ _ _  ___
  //  | |\/| / _` | '_ \ '_ \/ -_) _` | _| || | ' \/ _|  _| / _ \ ' \(_-<
  //  |_|  |_\__,_| .__/ .__/\___\__,_|_| \_,_|_||_\__|\__|_\___/_||_/__/
  //              |_|  |_|
  */
  void
  kineDyn_OCP::setupUserMappedFunctions( GenericContainer const & gc_data ) {
    UTILS_ASSERT0(
      gc_data.exists("MappedObjects"),
      "kineDyn_OCP::setupClasses: Missing key `MappedObjects` in data\n"
    );
    GenericContainer const & gc = gc_data("MappedObjects");

    // Initialize user mapped functions

    UTILS_ASSERT0(
      gc.exists("posPart"),
      "in kineDyn_OCP::setupUserMappedFunctions(gc) missing key: ``posPart''\n"
    );
    posPart.setup( gc("posPart") );

    UTILS_ASSERT0(
      gc.exists("negPart"),
      "in kineDyn_OCP::setupUserMappedFunctions(gc) missing key: ``negPart''\n"
    );
    negPart.setup( gc("negPart") );

    UTILS_ASSERT0(
      gc.exists("SignReg"),
      "in kineDyn_OCP::setupUserMappedFunctions(gc) missing key: ``SignReg''\n"
    );
    SignReg.setup( gc("SignReg") );

    UTILS_ASSERT0(
      gc.exists("SignReg_smooth"),
      "in kineDyn_OCP::setupUserMappedFunctions(gc) missing key: ``SignReg_smooth''\n"
    );
    SignReg_smooth.setup( gc("SignReg_smooth") );

    UTILS_ASSERT0(
      gc.exists("abs_reg"),
      "in kineDyn_OCP::setupUserMappedFunctions(gc) missing key: ``abs_reg''\n"
    );
    abs_reg.setup( gc("abs_reg") );
  }
  /* --------------------------------------------------------------------------
  //            _                ____            _             _
  //   ___  ___| |_ _   _ _ __  / ___|___  _ __ | |_ _ __ ___ | |___
  //  / __|/ _ \ __| | | | '_ \| |   / _ \| '_ \| __| '__/ _ \| / __|
  //  \__ \  __/ |_| |_| | |_) | |__| (_) | | | | |_| | | (_) | \__ \
  //  |___/\___|\__|\__,_| .__/ \____\___/|_| |_|\__|_|  \___/|_|___/
  //                     |_|
  */
  void
  kineDyn_OCP::setupControls( GenericContainer const & gc_data ) {
    // initialize Control penalties
    UTILS_ASSERT0(
      gc_data.exists("Controls"),
      "kineDyn_OCP::setupClasses: Missing key `Controls` in data\n"
    );
    GenericContainer const & gc = gc_data("Controls");
    delta__D0Control.setup( gc("delta__D0Control") );
    a__x0Control.setup( gc("a__x0Control") );
    // setup iterative solver
    this->setupControlSolver( gc_data );
  }

  /* --------------------------------------------------------------------------
  //            _               ____       _       _
  //   ___  ___| |_ _   _ _ __ |  _ \ ___ (_)_ __ | |_ ___ _ __ ___
  //  / __|/ _ \ __| | | | '_ \| |_) / _ \| | '_ \| __/ _ \ '__/ __|
  //  \__ \  __/ |_| |_| | |_) |  __/ (_) | | | | | ||  __/ |  \__ \
  //  |___/\___|\__|\__,_| .__/|_|   \___/|_|_| |_|\__\___|_|  |___/
  //                     |_|
  */
  void
  kineDyn_OCP::setupPointers( GenericContainer const & gc_data ) {

    UTILS_ASSERT0(
      gc_data.exists("Pointers"),
      "kineDyn_OCP::setupPointers: Missing key `Pointers` in data\n"
    );
    GenericContainer const & gc = gc_data("Pointers");

    // Initialize user classes

    UTILS_ASSERT0(
      gc.exists("pTrajectory"),
      "in kineDyn_OCP::setupPointers(gc) cant find key `pTrajectory' in gc\n"
    );
    pTrajectory = gc("pTrajectory").get_pointer<Path2D*>();
  }

  /* --------------------------------------------------------------------------
  //   _        __        ____ _
  //  (_)_ __  / _| ___  / ___| | __ _ ___ ___  ___  ___
  //  | | '_ \| |_ / _ \| |   | |/ _` / __/ __|/ _ \/ __|
  //  | | | | |  _| (_) | |___| | (_| \__ \__ \  __/\__ \
  //  |_|_| |_|_|  \___/ \____|_|\__,_|___/___/\___||___/
  */
  void
  kineDyn_OCP::infoClasses() const {
    int msg_level = 3;
    ostringstream mstr;

    m_console->message("\nControls\n",msg_level);
    mstr.str("");
    delta__D0Control.info(mstr);
    a__x0Control    .info(mstr);
    m_console->message(mstr.str(),msg_level);

    m_console->message("\nConstraints 1D\n",msg_level);
    mstr.str("");
    roadRightLateralBoundaries.info(mstr);
    roadLeftLateralBoundaries .info(mstr);
    GGdiagramEnvelope         .info(mstr);
    m_console->message(mstr.str(),msg_level);

    m_console->message("\nUser class (pointer)\n",msg_level);
    mstr.str("");
    mstr << "User function `pTrajectory`: ";
    pTrajectory->info(mstr);
    m_console->message(mstr.str(),msg_level);

    m_console->message("\nUser mapped functions\n",msg_level);
    mstr.str(""); 
    posPart.info(mstr);
    negPart.info(mstr);
    SignReg.info(mstr);
    SignReg_smooth.info(mstr);
    abs_reg.info(mstr);
    m_console->message(mstr.str(),msg_level);

    m_console->message("\nModel Parameters\n",msg_level);
    for ( integer i = 0; i < numModelPars; ++i ) {
      m_console->message(
        fmt::format("{:.>40} = {}\n",namesModelPars[i], ModelPars[i]),
        msg_level
      );
    }

  }

  /* --------------------------------------------------------------------------
  //            _
  //   ___  ___| |_ _   _ _ __
  //  / __|/ _ \ __| | | | '_ \
  //  \__ \  __/ |_| |_| | |_) |
  //  |___/\___|\__|\__,_| .__/
  //                     |_|
  */
  void
  kineDyn_OCP::setup( GenericContainer const & gc ) {

    if ( gc.exists("Debug") )
      m_debug = gc("Debug").get_bool("kineDyn_OCP::setup, Debug");

    this->setupParameters( gc );
    this->setupClasses( gc );
    this->setupUserMappedFunctions( gc );
    this->setupUserClasses( gc );
    this->setupPointers( gc );
    this->setupBC( gc );
    this->setupControls( gc );

    // setup nonlinear system with object handling mesh domain
    this->setup( pTrajectory, gc );
    this->infoBC();
    this->infoClasses();
    this->info();
  }

  /* --------------------------------------------------------------------------
  //              _
  //    __ _  ___| |_     _ __   __ _ _ __ ___   ___  ___
  //   / _` |/ _ \ __|   | '_ \ / _` | '_ ` _ \ / _ \/ __|
  //  | (_| |  __/ |_    | | | | (_| | | | | | |  __/\__ \
  //   \__, |\___|\__|___|_| |_|\__,_|_| |_| |_|\___||___/
  //   |___/        |_____|
  */
  void
  kineDyn_OCP::get_names( GenericContainer & out ) const {
    vec_string_type & X_names = out["state_names"].set_vec_string();
    for ( integer i = 0; i < numXvars; ++i ) X_names.push_back(namesXvars[i]);

    vec_string_type & LM_names = out["lagrange_multiplier_names"].set_vec_string();
    for ( integer i = 0; i < numLvars; ++i ) LM_names.push_back(namesLvars[i]);

    vec_string_type & U_names = out["control_names"].set_vec_string();
    for ( integer i = 0; i < numUvars; ++i ) U_names.push_back(namesUvars[i]);

    vec_string_type & Q_names = out["mesh_variable_names"].set_vec_string();
    for ( integer i = 0; i < numQvars; ++i ) Q_names.push_back(namesQvars[i]);

    vec_string_type & P_names = out["parameter_names"].set_vec_string();
    for ( integer i = 0; i < numPvars; ++i ) P_names.push_back(namesPvars[i]);

    vec_string_type & OMEGA_names = out["bc_lagrange_multiplier_names"].set_vec_string();
    for ( integer i = 0; i < numOMEGAvars; ++i ) OMEGA_names.push_back(namesOMEGAvars[i]);

    vec_string_type & PP_names = out["post_processing_names"].set_vec_string();
    for ( integer i = 0; i < numPostProcess; ++i ) PP_names.push_back(namesPostProcess[i]);

    for ( integer i = 0; i < numIntegratedPostProcess; ++i )
      PP_names.push_back(namesIntegratedPostProcess[i]);

    vec_string_type & model_names = out["model_names"].set_vec_string();
    for ( integer i = 0; i < numModelPars; ++i )
      model_names.push_back(namesModelPars[i]);
  }

  /* --------------------------------------------------------------------------
  //      _ _                       _   _
  //   __| (_)__ _ __ _ _ _  ___ __| |_(_)__
  //  / _` | / _` / _` | ' \/ _ (_-<  _| / _|
  //  \__,_|_\__,_\__, |_||_\___/__/\__|_\__|
  //              |___/
  */
  void
  kineDyn_OCP::diagnostic( GenericContainer const & gc ) {

    // DA RIFARE--------------

    // If required save function and jacobian
    //if ( gc.exists("DumpFile") )
    //  this->dumpFunctionAndJacobian( pSolver->solution(),
    //                                 gc("DumpFile").get_string() );

    //bool do_diagnosis = gc.get_map_bool("Doctor");
    //if ( do_diagnosis )
    //  this->diagnosis( pSolver->solution(), gc["diagnosis"] );

    real_type epsi = 1e-5;
    gc.get_if_exists("JacobianCheck_epsilon",epsi);
    if ( gc.get_map_bool("JacobianCheck") )
      this->checkJacobian( pSolver->solution(), epsi );
    if ( gc.get_map_bool("JacobianCheckFull") )
      this->checkJacobianFull( pSolver->solution(), epsi );
  }

  // save model parameters
  void
  kineDyn_OCP::save_OCP_info( GenericContainer & gc ) const {
    for ( integer i = 0; i < numModelPars; ++i )
      gc[namesModelPars[i]] = ModelPars[i];

  }

}

// EOF: kineDyn_OCP.cc
