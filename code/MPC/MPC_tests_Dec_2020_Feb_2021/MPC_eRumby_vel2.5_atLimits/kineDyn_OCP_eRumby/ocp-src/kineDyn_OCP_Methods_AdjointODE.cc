/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP_Methods.cc                                         |
 |                                                                       |
 |  version: 1.0   date 8/12/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "kineDyn_OCP.hh"
#include "kineDyn_OCP_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using Mechatronix::Path2D;


#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wunused-macros"
#elif defined(__llvm__) || defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-macros"
#elif defined(_MSC_VER)
#pragma warning( disable : 4100 )
#pragma warning( disable : 4101 )
#endif

// map user defined functions and objects with macros
#define ALIAS_theta_DD(__t1) pTrajectory -> heading_DD( __t1)
#define ALIAS_theta_D(__t1) pTrajectory -> heading_D( __t1)
#define ALIAS_theta(__t1) pTrajectory -> heading( __t1)
#define ALIAS_yLane_DD(__t1) pTrajectory -> yTrajectory_DD( __t1)
#define ALIAS_yLane_D(__t1) pTrajectory -> yTrajectory_D( __t1)
#define ALIAS_yLane(__t1) pTrajectory -> yTrajectory( __t1)
#define ALIAS_xLane_DD(__t1) pTrajectory -> xTrajectory_DD( __t1)
#define ALIAS_xLane_D(__t1) pTrajectory -> xTrajectory_D( __t1)
#define ALIAS_xLane(__t1) pTrajectory -> xTrajectory( __t1)
#define ALIAS_Curv_DD(__t1) pTrajectory -> curvature_DD( __t1)
#define ALIAS_Curv_D(__t1) pTrajectory -> curvature_D( __t1)
#define ALIAS_Curv(__t1) pTrajectory -> curvature( __t1)
#define ALIAS_abs_reg_DD(__t1) abs_reg.DD( __t1)
#define ALIAS_abs_reg_D(__t1) abs_reg.D( __t1)
#define ALIAS_SignReg_smooth_DD(__t1) SignReg_smooth.DD( __t1)
#define ALIAS_SignReg_smooth_D(__t1) SignReg_smooth.D( __t1)
#define ALIAS_SignReg_DD(__t1) SignReg.DD( __t1)
#define ALIAS_SignReg_D(__t1) SignReg.D( __t1)
#define ALIAS_negPart_DD(__t1) negPart.DD( __t1)
#define ALIAS_negPart_D(__t1) negPart.D( __t1)
#define ALIAS_posPart_DD(__t1) posPart.DD( __t1)
#define ALIAS_posPart_D(__t1) posPart.D( __t1)
#define ALIAS_GGdiagramEnvelope_DD(__t1) GGdiagramEnvelope.DD( __t1)
#define ALIAS_GGdiagramEnvelope_D(__t1) GGdiagramEnvelope.D( __t1)
#define ALIAS_roadLeftLateralBoundaries_DD(__t1) roadLeftLateralBoundaries.DD( __t1)
#define ALIAS_roadLeftLateralBoundaries_D(__t1) roadLeftLateralBoundaries.D( __t1)
#define ALIAS_roadRightLateralBoundaries_DD(__t1) roadRightLateralBoundaries.DD( __t1)
#define ALIAS_roadRightLateralBoundaries_D(__t1) roadRightLateralBoundaries.D( __t1)
#define ALIAS_a__x0Control_D_3(__t1, __t2, __t3) a__x0Control.D_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2(__t1, __t2, __t3) a__x0Control.D_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1(__t1, __t2, __t3) a__x0Control.D_1( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_3_3(__t1, __t2, __t3) a__x0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_3(__t1, __t2, __t3) a__x0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_2_2(__t1, __t2, __t3) a__x0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_3(__t1, __t2, __t3) a__x0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_2(__t1, __t2, __t3) a__x0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_a__x0Control_D_1_1(__t1, __t2, __t3) a__x0Control.D_1_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3(__t1, __t2, __t3) delta__D0Control.D_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2(__t1, __t2, __t3) delta__D0Control.D_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1(__t1, __t2, __t3) delta__D0Control.D_1( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_3_3(__t1, __t2, __t3) delta__D0Control.D_3_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_3(__t1, __t2, __t3) delta__D0Control.D_2_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_2_2(__t1, __t2, __t3) delta__D0Control.D_2_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_3(__t1, __t2, __t3) delta__D0Control.D_1_3( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_2(__t1, __t2, __t3) delta__D0Control.D_1_2( __t1, __t2, __t3)
#define ALIAS_delta__D0Control_D_1_1(__t1, __t2, __t3) delta__D0Control.D_1_1( __t1, __t2, __t3)


namespace kineDyn_OCPDefine {

  /*\
   |  _   _
   | | | | |_  __
   | | |_| \ \/ /
   | |  _  |>  <
   | |_| |_/_/\_\
   |
  \*/

  integer
  kineDyn_OCP::Hx_numEqns() const
  { return 6; }

  void
  kineDyn_OCP::Hx_eval(
    NodeType2 const    & NODE__,
    V_const_pointer_type V__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = Q__[1];
    real_type t2   = X__[1];
    real_type t3   = cos(t2);
    real_type t4   = 1.0 / t3;
    real_type t5   = t4 * t1;
    real_type t6   = X__[2];
    real_type t7   = 1.0 / t6;
    real_type t8   = X__[0];
    real_type t9   = ModelPars[64];
    real_type t10  = Q__[0];
    real_type t11  = rightWidth(t10);
    real_type t12  = t8 - t9 + t11;
    real_type t13  = roadRightLateralBoundaries(t12);
    real_type t14  = t13 * t7;
    real_type t16  = t8 * t1;
    real_type t17  = t16 - 1;
    real_type t18  = t4 * t17;
    real_type t19  = ALIAS_roadRightLateralBoundaries_D(t12);
    real_type t22  = leftWidth(t10);
    real_type t23  = t22 - t8 - t9;
    real_type t24  = roadLeftLateralBoundaries(t23);
    real_type t25  = t24 * t7;
    real_type t27  = ALIAS_roadLeftLateralBoundaries_D(t23);
    real_type t30  = aymax(t6);
    real_type t31  = 1.0 / t30;
    real_type t32  = X__[4];
    real_type t33  = t6 * t32;
    real_type t34  = abs_reg(t33);
    real_type t35  = t34 * t31;
    real_type t36  = ModelPars[21];
    real_type t37  = pow(t35, t36);
    real_type t38  = axmax(t6);
    real_type t39  = 1.0 / t38;
    real_type t40  = X__[3];
    real_type t42  = t40 - ModelPars[7];
    real_type t43  = abs_reg(t42);
    real_type t45  = pow(t43 * t39, t36);
    real_type t46  = t37 + t45;
    real_type t47  = SignReg(t42);
    real_type t49  = t47 / 2 + 1.0 / 2.0;
    real_type t51  = ModelPars[20];
    real_type t52  = pow(t35, t51);
    real_type t53  = axmin(t6);
    real_type t54  = 1.0 / t53;
    real_type t56  = pow(t43 * t54, t51);
    real_type t57  = t52 + t56;
    real_type t59  = t47 / 2 - 1.0 / 2.0;
    real_type t61  = -t49 * t46 + t59 * t57 + 1;
    real_type t62  = GGdiagramEnvelope(t61);
    real_type t63  = t62 * t7;
    real_type t67  = t6 - ModelPars[24];
    real_type t68  = t67 * t67;
    real_type t69  = ModelPars[29];
    real_type t71  = t8 * t8;
    real_type t72  = ModelPars[27];
    real_type t74  = t69 * t68 + t72 * t71 + ModelPars[28];
    real_type t75  = t74 * t7;
    real_type t81  = L__[0];
    real_type t82  = sin(t2);
    real_type t85  = L__[1];
    real_type t86  = t1 * t85;
    real_type t90  = L__[2];
    real_type t91  = ModelPars[17];
    real_type t93  = 1.0 / t91 * t90;
    real_type t94  = t6 * t6;
    real_type t95  = ModelPars[16];
    real_type t99  = (-t91 * t40 + t95 * t94) * t93;
    real_type t100 = t7 * t5;
    real_type t105 = 1.0 / ModelPars[62] * L__[3];
    real_type t108 = (t40 - U__[0]) * t105;
    real_type t111 = ModelPars[0];
    real_type t113 = 1.0 / t111 * L__[4];
    real_type t114 = ModelPars[22];
    real_type t115 = 1.0 / t114;
    real_type t117 = 1.0 / ModelPars[61];
    real_type t119 = t117 * t115 * t113;
    real_type t122 = k__US(t32, t6);
    real_type t125 = X__[5];
    real_type t127 = t114 * t111 * t32 + t114 * t122 * t6 - t6 * t125;
    real_type t129 = t7 * t4;
    real_type t135 = L__[5] / ModelPars[63];
    real_type t138 = (t125 - U__[1]) * t135;
    result__[ 0   ] = t129 * t1 * t127 * t119 - 2 * t72 * t8 * t7 * t18 - t7 * t4 * t32 * t86 - t19 * t7 * t18 + t27 * t7 * t18 - t5 * t82 * t81 + t100 * t108 + t100 * t138 + t100 * t99 - t14 * t5 - t25 * t5 - t63 * t5 - t75 * t5;
    real_type t140 = t3 * t3;
    real_type t141 = 1.0 / t140;
    real_type t142 = t141 * t17;
    real_type t152 = t82 * t82;
    real_type t161 = (t1 * t6 * t3 + t32 * t16 - t32) * t85;
    real_type t163 = t82 * t7 * t141;
    real_type t166 = t82 * t7 * t142;
    real_type t169 = t17 * t127;
    result__[ 1   ] = t163 * t169 * t119 - t82 * t14 * t142 - t142 * t152 * t81 - t82 * t25 * t142 - t82 * t63 * t142 - t82 * t75 * t142 + t5 * t82 * t85 + t166 * t108 + t166 * t138 - t163 * t161 + t166 * t99 - t17 * t81;
    real_type t173 = 1.0 / t94;
    real_type t180 = ALIAS_GGdiagramEnvelope_D(t61);
    real_type t181 = t180 * t7;
    real_type t182 = t36 * t37;
    real_type t183 = t30 * t30;
    real_type t186 = aymax_D(t6);
    real_type t188 = ALIAS_abs_reg_D(t33);
    real_type t193 = 1.0 / t34;
    real_type t194 = t193 * t30 * (-t186 * t34 / t183 + t32 * t188 * t31);
    real_type t196 = t36 * t45;
    real_type t197 = axmax_D(t6);
    real_type t202 = t51 * t52;
    real_type t204 = t51 * t56;
    real_type t205 = axmin_D(t6);
    real_type t220 = t173 * t4;
    real_type t226 = t173 * t18;
    real_type t230 = k__US_D_2(t32, t6);
    result__[ 2   ] = t13 * t173 * t18 + t24 * t173 * t18 + t62 * t173 * t18 - (-t49 * (-t197 * t39 * t196 + t194 * t182) + t59 * (-t205 * t54 * t204 + t194 * t202)) * t181 * t18 + t74 * t173 * t18 - 2 * t69 * t67 * t7 * t18 - t7 * t86 + t220 * t161 + 2 * t4 * t17 * t95 * t93 - t226 * t99 - t226 * t108 + t129 * t17 * (t114 * t230 * t6 + t114 * t122 - t125) * t119 - t220 * t169 * t119 - t226 * t138;
    real_type t240 = ALIAS_abs_reg_D(t42);
    real_type t242 = 1.0 / t43 * t240;
    real_type t245 = ALIAS_SignReg_D(t42);
    real_type t257 = t7 * t18;
    result__[ 3   ] = -(-t49 * t242 * t196 - t245 * t46 / 2 + t59 * t242 * t204 + t245 * t57 / 2) * t181 * t18 - t129 * t17 * t90 + t257 * t105;
    real_type t260 = t193 * t6;
    real_type t272 = k__US_D_1(t32, t6);
    result__[ 4   ] = -(-t49 * t260 * t188 * t182 + t59 * t260 * t188 * t202) * t181 * t18 - t129 * t17 * t85 + t129 * t17 * (t114 * t272 * t6 + t114 * t111) * t119;
    result__[ 5   ] = -t4 * t17 * t117 * t115 * t113 + t257 * t135;
    if ( m_debug )
      Mechatronix::check_in_segment( result__, "Hx_eval", 6, i_segment );
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DHxDx_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DHxDx_numCols() const
  { return 6; }

  integer
  kineDyn_OCP::DHxDx_nnz() const
  { return 31; }

  void
  kineDyn_OCP::DHxDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[0 ] = 0   ; jIndex[0 ] = 0   ;
    iIndex[1 ] = 0   ; jIndex[1 ] = 1   ;
    iIndex[2 ] = 0   ; jIndex[2 ] = 2   ;
    iIndex[3 ] = 0   ; jIndex[3 ] = 3   ;
    iIndex[4 ] = 0   ; jIndex[4 ] = 4   ;
    iIndex[5 ] = 0   ; jIndex[5 ] = 5   ;
    iIndex[6 ] = 1   ; jIndex[6 ] = 0   ;
    iIndex[7 ] = 1   ; jIndex[7 ] = 1   ;
    iIndex[8 ] = 1   ; jIndex[8 ] = 2   ;
    iIndex[9 ] = 1   ; jIndex[9 ] = 3   ;
    iIndex[10] = 1   ; jIndex[10] = 4   ;
    iIndex[11] = 1   ; jIndex[11] = 5   ;
    iIndex[12] = 2   ; jIndex[12] = 0   ;
    iIndex[13] = 2   ; jIndex[13] = 1   ;
    iIndex[14] = 2   ; jIndex[14] = 2   ;
    iIndex[15] = 2   ; jIndex[15] = 3   ;
    iIndex[16] = 2   ; jIndex[16] = 4   ;
    iIndex[17] = 2   ; jIndex[17] = 5   ;
    iIndex[18] = 3   ; jIndex[18] = 0   ;
    iIndex[19] = 3   ; jIndex[19] = 1   ;
    iIndex[20] = 3   ; jIndex[20] = 2   ;
    iIndex[21] = 3   ; jIndex[21] = 3   ;
    iIndex[22] = 3   ; jIndex[22] = 4   ;
    iIndex[23] = 4   ; jIndex[23] = 0   ;
    iIndex[24] = 4   ; jIndex[24] = 1   ;
    iIndex[25] = 4   ; jIndex[25] = 2   ;
    iIndex[26] = 4   ; jIndex[26] = 3   ;
    iIndex[27] = 4   ; jIndex[27] = 4   ;
    iIndex[28] = 5   ; jIndex[28] = 0   ;
    iIndex[29] = 5   ; jIndex[29] = 1   ;
    iIndex[30] = 5   ; jIndex[30] = 2   ;
  }

  void
  kineDyn_OCP::DHxDx_sparse(
    NodeType2 const    & NODE__,
    V_const_pointer_type V__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t1   = Q__[1];
    real_type t2   = X__[1];
    real_type t3   = cos(t2);
    real_type t4   = 1.0 / t3;
    real_type t5   = t4 * t1;
    real_type t6   = X__[2];
    real_type t7   = 1.0 / t6;
    real_type t8   = X__[0];
    real_type t9   = ModelPars[64];
    real_type t10  = Q__[0];
    real_type t11  = rightWidth(t10);
    real_type t12  = t8 - t9 + t11;
    real_type t13  = ALIAS_roadRightLateralBoundaries_D(t12);
    real_type t14  = t13 * t7;
    real_type t17  = t1 * t8;
    real_type t18  = t17 - 1;
    real_type t19  = t4 * t18;
    real_type t20  = ALIAS_roadRightLateralBoundaries_DD(t12);
    real_type t23  = leftWidth(t10);
    real_type t24  = t23 - t8 - t9;
    real_type t25  = ALIAS_roadLeftLateralBoundaries_D(t24);
    real_type t26  = t25 * t7;
    real_type t29  = ALIAS_roadLeftLateralBoundaries_DD(t24);
    real_type t33  = ModelPars[27];
    result__[ 0   ] = -4 * t33 * t8 * t7 * t5 - t20 * t7 * t19 - t29 * t7 * t19 - 2 * t33 * t7 * t19 - 2 * t14 * t5 + 2 * t26 * t5;
    real_type t40  = t3 * t3;
    real_type t41  = 1.0 / t40;
    real_type t42  = t41 * t1;
    real_type t43  = roadRightLateralBoundaries(t12);
    real_type t44  = t43 * t7;
    real_type t45  = sin(t2);
    real_type t48  = t41 * t18;
    real_type t51  = roadLeftLateralBoundaries(t24);
    real_type t52  = t51 * t7;
    real_type t57  = aymax(t6);
    real_type t58  = 1.0 / t57;
    real_type t59  = X__[4];
    real_type t60  = t6 * t59;
    real_type t61  = abs_reg(t60);
    real_type t62  = t61 * t58;
    real_type t63  = ModelPars[21];
    real_type t64  = pow(t62, t63);
    real_type t65  = axmax(t6);
    real_type t66  = 1.0 / t65;
    real_type t67  = X__[3];
    real_type t69  = t67 - ModelPars[7];
    real_type t70  = abs_reg(t69);
    real_type t72  = pow(t70 * t66, t63);
    real_type t73  = t64 + t72;
    real_type t74  = SignReg(t69);
    real_type t76  = t74 / 2 + 1.0 / 2.0;
    real_type t78  = ModelPars[20];
    real_type t79  = pow(t62, t78);
    real_type t80  = axmin(t6);
    real_type t81  = 1.0 / t80;
    real_type t83  = pow(t70 * t81, t78);
    real_type t84  = t79 + t83;
    real_type t86  = t74 / 2 - 1.0 / 2.0;
    real_type t88  = -t76 * t73 + t86 * t84 + 1;
    real_type t89  = GGdiagramEnvelope(t88);
    real_type t90  = t89 * t7;
    real_type t95  = t6 - ModelPars[24];
    real_type t96  = t95 * t95;
    real_type t97  = ModelPars[29];
    real_type t99  = t8 * t8;
    real_type t101 = t33 * t99 + t97 * t96 + ModelPars[28];
    real_type t102 = t101 * t7;
    real_type t105 = t7 * t48;
    real_type t110 = L__[0];
    real_type t112 = t45 * t45;
    real_type t115 = L__[1];
    real_type t116 = t1 * t115;
    real_type t119 = t45 * t7 * t41;
    real_type t121 = L__[2];
    real_type t122 = ModelPars[17];
    real_type t124 = 1.0 / t122 * t121;
    real_type t125 = t6 * t6;
    real_type t126 = ModelPars[16];
    real_type t130 = (-t122 * t67 + t126 * t125) * t124;
    real_type t132 = t45 * t7 * t42;
    real_type t137 = L__[3] / ModelPars[62];
    real_type t140 = (t67 - U__[0]) * t137;
    real_type t143 = ModelPars[0];
    real_type t145 = 1.0 / t143 * L__[4];
    real_type t146 = ModelPars[22];
    real_type t147 = 1.0 / t146;
    real_type t149 = 1.0 / ModelPars[61];
    real_type t151 = t149 * t147 * t145;
    real_type t154 = k__US(t59, t6);
    real_type t157 = X__[5];
    real_type t159 = t146 * t143 * t59 + t146 * t154 * t6 - t6 * t157;
    real_type t160 = t1 * t159;
    real_type t166 = L__[5] / ModelPars[63];
    real_type t169 = (t157 - U__[1]) * t166;
    result__[ 1   ] = -2 * t45 * t33 * t8 * t105 - t45 * t102 * t42 - t42 * t112 * t110 - t119 * t59 * t116 + t119 * t160 * t151 - t45 * t14 * t48 + t45 * t26 * t48 - t45 * t44 * t42 - t45 * t52 * t42 - t45 * t90 * t42 - t1 * t110 + t132 * t130 + t132 * t140 + t132 * t169;
    real_type t171 = 1.0 / t125;
    real_type t172 = t43 * t171;
    real_type t176 = t51 * t171;
    real_type t180 = t89 * t171;
    real_type t182 = ALIAS_GGdiagramEnvelope_D(t88);
    real_type t183 = t182 * t7;
    real_type t184 = t63 * t64;
    real_type t185 = t57 * t57;
    real_type t186 = 1.0 / t185;
    real_type t187 = t61 * t186;
    real_type t188 = aymax_D(t6);
    real_type t190 = ALIAS_abs_reg_D(t60);
    real_type t191 = t190 * t58;
    real_type t193 = -t188 * t187 + t59 * t191;
    real_type t194 = t57 * t193;
    real_type t195 = 1.0 / t61;
    real_type t196 = t195 * t194;
    real_type t198 = t63 * t72;
    real_type t199 = axmax_D(t6);
    real_type t202 = -t199 * t66 * t198 + t196 * t184;
    real_type t204 = t78 * t79;
    real_type t206 = t78 * t83;
    real_type t207 = axmin_D(t6);
    real_type t210 = -t207 * t81 * t206 + t196 * t204;
    real_type t212 = -t76 * t202 + t86 * t210;
    real_type t215 = t101 * t171;
    real_type t232 = t171 * t5;
    real_type t236 = k__US_D_2(t59, t6);
    real_type t239 = t146 * t236 * t6 + t146 * t154 - t157;
    real_type t241 = t7 * t4;
    real_type t244 = t171 * t4;
    result__[ 2   ] = t172 * t5 + t13 * t171 * t19 + t176 * t5 - t25 * t171 * t19 + t180 * t5 - t212 * t183 * t5 + t215 * t5 - 2 * t97 * t95 * t7 * t5 + 2 * t33 * t8 * t171 * t19 + t171 * t4 * t59 * t116 + 2 * t4 * t1 * t126 * t124 - t232 * t130 - t232 * t140 + t241 * t1 * t239 * t151 - t244 * t160 * t151 - t232 * t169;
    real_type t248 = ALIAS_abs_reg_D(t69);
    real_type t249 = 1.0 / t70;
    real_type t250 = t249 * t248;
    real_type t253 = ALIAS_SignReg_D(t69);
    real_type t260 = -t76 * t250 * t198 - t253 * t73 / 2 + t86 * t250 * t206 + t253 * t84 / 2;
    real_type t265 = t7 * t5;
    result__[ 3   ] = -t241 * t1 * t121 - t260 * t183 * t5 + t265 * t137;
    real_type t267 = t190 * t184;
    real_type t268 = t195 * t6;
    real_type t271 = t190 * t204;
    real_type t274 = -t76 * t268 * t267 + t86 * t268 * t271;
    real_type t279 = k__US_D_1(t59, t6);
    real_type t282 = t146 * t279 * t6 + t146 * t143;
    result__[ 4   ] = t241 * t1 * t282 * t151 - t274 * t183 * t5 - t241 * t116;
    real_type t286 = t147 * t145;
    result__[ 5   ] = -t4 * t1 * t149 * t286 + t265 * t166;
    result__[ 6   ] = result__[1];
    real_type t292 = 1.0 / t40 / t3;
    real_type t293 = t292 * t18;
    real_type t317 = -2 * t293 * t112 * t45 * t110 - 2 * t112 * t102 * t293 - 2 * t19 * t45 * t110 - 2 * t112 * t44 * t293 - 2 * t112 * t52 * t293 - 2 * t112 * t90 * t293 - t102 * t19 - t44 * t19 - t52 * t19 - t90 * t19 + t116;
    real_type t325 = (t1 * t6 * t3 + t59 * t17 - t59) * t115;
    real_type t327 = t112 * t7 * t292;
    real_type t332 = t112 * t7 * t293;
    real_type t335 = t7 * t19;
    real_type t340 = t18 * t159;
    real_type t349 = 2 * t42 * t112 * t115 + t241 * t340 * t151 + 2 * t327 * t340 * t151 + 2 * t332 * t130 + t335 * t130 + 2 * t332 * t140 + t335 * t140 + 2 * t332 * t169 + t335 * t169 - t241 * t325 - 2 * t327 * t325;
    result__[ 7   ] = t317 + t349;
    real_type t370 = t45 * t171 * t41;
    real_type t377 = t45 * t171 * t48;
    real_type t380 = t18 * t239;
    result__[ 8   ] = -t45 * t7 * t1 * t4 * t115 - t45 * t212 * t182 * t105 - 2 * t45 * t97 * t95 * t105 + 2 * t45 * t48 * t126 * t124 + t119 * t380 * t151 - t370 * t340 * t151 + t45 * t172 * t48 + t45 * t176 * t48 + t45 * t180 * t48 + t45 * t215 * t48 - t377 * t130 - t377 * t140 - t377 * t169 + t370 * t325;
    real_type t389 = t18 * t121;
    result__[ 9   ] = -t45 * t260 * t182 * t105 + t119 * t18 * t137 - t119 * t389;
    real_type t396 = t18 * t115;
    real_type t398 = t18 * t282;
    result__[ 10  ] = -t45 * t274 * t182 * t105 + t119 * t398 * t151 - t119 * t396;
    result__[ 11  ] = -t45 * t41 * t18 * t149 * t286 + t119 * t18 * t166;
    result__[ 12  ] = result__[2];
    result__[ 13  ] = result__[8];
    real_type t408 = 1.0 / t125 / t6;
    real_type t418 = t182 * t171;
    real_type t422 = ALIAS_GGdiagramEnvelope_DD(t88);
    real_type t423 = t422 * t7;
    real_type t424 = t212 * t212;
    real_type t427 = t63 * t63;
    real_type t428 = t427 * t64;
    real_type t429 = t193 * t193;
    real_type t431 = t61 * t61;
    real_type t432 = 1.0 / t431;
    real_type t433 = t432 * t185 * t429;
    real_type t438 = t188 * t188;
    real_type t441 = t190 * t186;
    real_type t445 = aymax_DD(t6);
    real_type t447 = ALIAS_abs_reg_DD(t60);
    real_type t448 = t447 * t58;
    real_type t449 = t59 * t59;
    real_type t453 = t195 * t57 * (2 * t438 * t61 / t185 / t57 - 2 * t188 * t59 * t441 - t445 * t187 + t449 * t448);
    real_type t456 = t195 * t188 * t193;
    real_type t458 = t193 * t184;
    real_type t459 = t432 * t57;
    real_type t461 = t59 * t190 * t459;
    real_type t463 = t427 * t72;
    real_type t464 = t65 * t65;
    real_type t466 = t199 * t199;
    real_type t467 = t466 / t464;
    real_type t470 = axmax_DD(t6);
    real_type t475 = t78 * t78;
    real_type t476 = t475 * t79;
    real_type t480 = t193 * t204;
    real_type t482 = t475 * t83;
    real_type t483 = t80 * t80;
    real_type t485 = t207 * t207;
    real_type t486 = t485 / t483;
    real_type t489 = axmin_DD(t6);
    real_type t509 = t408 * t4;
    real_type t517 = t408 * t19;
    real_type t524 = k__US_D_2_2(t59, t6);
    result__[ 14  ] = -2 * t43 * t408 * t19 - 2 * t51 * t408 * t19 - 2 * t89 * t408 * t19 + 2 * t212 * t418 * t19 - t424 * t423 * t19 - (-t76 * (-t470 * t66 * t198 + t453 * t184 + t456 * t184 + t467 * t198 + t433 * t428 - t461 * t458 + t467 * t463) + t86 * (-t489 * t81 * t206 + t453 * t204 + t456 * t204 + t486 * t206 + t433 * t476 - t461 * t480 + t486 * t482)) * t183 * t19 - 2 * t101 * t408 * t19 + 4 * t97 * t95 * t171 * t19 - 2 * t97 * t7 * t19 + 2 * t171 * t116 - 2 * t509 * t325 - 2 * t4 * t18 * t126 * t7 * t124 + 2 * t517 * t130 + 2 * t517 * t140 + t241 * t18 * (t146 * t524 * t6 + 2 * t146 * t236) * t151 - 2 * t244 * t380 * t151 + 2 * t509 * t340 * t151 + 2 * t517 * t169;
    real_type t562 = t171 * t19;
    result__[ 15  ] = t260 * t418 * t19 - t212 * t260 * t422 * t335 - (t76 * t199 * t66 * t249 * t248 * t463 - t253 * t202 / 2 - t86 * t207 * t81 * t249 * t248 * t482 + t253 * t210 / 2) * t183 * t19 + t244 * t389 - t562 * t137;
    real_type t565 = t274 * t418 * t19;
    real_type t566 = t274 * t422;
    real_type t568 = t212 * t566 * t335;
    real_type t570 = t432 * t6;
    real_type t571 = t194 * t570;
    real_type t578 = t195 * t57 * (-t188 * t6 * t441 + t60 * t448 + t191);
    real_type t581 = t6 * t190 * t459;
    real_type t594 = t244 * t396;
    real_type t596 = k__US_D_1_2(t59, t6);
    real_type t602 = t241 * t18 * (t146 * t596 * t6 + t146 * t279) * t151;
    real_type t604 = t244 * t398 * t151;
    result__[ 16  ] = t565 - t568 - (-t76 * (t571 * t190 * t428 + t578 * t184 - t581 * t458) + t86 * (t571 * t190 * t476 + t578 * t204 - t581 * t480)) * t183 * t19 + t594 + t602 - t604;
    result__[ 17  ] = -t562 * t166;
    result__[ 18  ] = result__[3];
    result__[ 19  ] = result__[9];
    result__[ 20  ] = result__[15];
    real_type t606 = t260 * t260;
    real_type t609 = t248 * t248;
    real_type t610 = t70 * t70;
    real_type t612 = 1.0 / t610 * t609;
    real_type t613 = t76 * t612;
    real_type t615 = ALIAS_abs_reg_DD(t69);
    real_type t616 = t249 * t615;
    real_type t620 = t253 * t250;
    real_type t622 = ALIAS_SignReg_DD(t69);
    real_type t625 = t86 * t612;
    result__[ 21  ] = -t606 * t423 * t19 - (-t613 * t463 - t76 * t616 * t198 + t613 * t198 - t620 * t198 - t622 * t73 / 2 + t625 * t482 + t86 * t616 * t206 - t625 * t206 + t620 * t206 + t622 * t84 / 2) * t183 * t19;
    real_type t638 = t253 * t268;
    result__[ 22  ] = -t260 * t566 * t335 - (-t638 * t267 / 2 + t638 * t271 / 2) * t183 * t19;
    result__[ 23  ] = result__[4];
    result__[ 24  ] = result__[10];
    real_type t646 = t190 * t432;
    real_type t650 = t447 * t184;
    real_type t654 = t195 * t190;
    real_type t657 = t190 * t190;
    real_type t658 = t657 * t184;
    real_type t666 = t447 * t204;
    real_type t672 = t657 * t204;
    result__[ 25  ] = t565 - t568 - (-t76 * t6 * t646 * t194 * t428 + t86 * t6 * t646 * t194 * t476 - t76 * t195 * t60 * t650 + t86 * t195 * t60 * t666 + t59 * t76 * t570 * t658 - t59 * t86 * t570 * t672 - t76 * t654 * t184 + t86 * t654 * t204) * t183 * t19 + t594 + t602 - t604;
    result__[ 26  ] = result__[22];
    real_type t679 = t274 * t274;
    real_type t683 = t432 * t125;
    real_type t684 = t76 * t683;
    real_type t686 = t195 * t125;
    real_type t691 = t86 * t683;
    real_type t700 = k__US_D_1_1(t59, t6);
    result__[ 27  ] = -t679 * t423 * t19 - (-t684 * t657 * t428 + t691 * t657 * t476 - t76 * t686 * t650 + t86 * t686 * t666 + t684 * t658 - t691 * t672) * t183 * t19 + t4 * t18 * t700 * t149 * t145;
    result__[ 28  ] = result__[5];
    result__[ 29  ] = result__[11];
    result__[ 30  ] = result__[17];
    if ( m_debug )
      Mechatronix::check_in_segment( result__, "DHxDx_sparse", 31, i_segment );
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DHxDp_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DHxDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DHxDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DHxDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DHxDp_sparse(
    NodeType2 const    & NODE__,
    V_const_pointer_type V__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |  _   _
   | | | | |_   _
   | | |_| | | | |
   | |  _  | |_| |
   | |_| |_|\__,_|
   |
  \*/

  integer
  kineDyn_OCP::Hu_numEqns() const
  { return 2; }

  void
  kineDyn_OCP::Hu_eval(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t10  = cos(X__[1]);
    real_type t15  = 1.0 / X__[2] / t10 * (Q__[1] * X__[0] - 1);
    result__[ 0   ] = -t15 * L__[3] / ModelPars[62];
    result__[ 1   ] = -t15 * L__[5] / ModelPars[63];
    if ( m_debug )
      Mechatronix::check_in_segment( result__, "Hu_eval", 2, i_segment );
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DHuDx_numRows() const
  { return 2; }

  integer
  kineDyn_OCP::DHuDx_numCols() const
  { return 6; }

  integer
  kineDyn_OCP::DHuDx_nnz() const
  { return 6; }

  void
  kineDyn_OCP::DHuDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
    iIndex[0 ] = 0   ; jIndex[0 ] = 0   ;
    iIndex[1 ] = 0   ; jIndex[1 ] = 1   ;
    iIndex[2 ] = 0   ; jIndex[2 ] = 2   ;
    iIndex[3 ] = 1   ; jIndex[3 ] = 0   ;
    iIndex[4 ] = 1   ; jIndex[4 ] = 1   ;
    iIndex[5 ] = 1   ; jIndex[5 ] = 2   ;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DHuDx_sparse(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    real_type t4   = L__[3] / ModelPars[62];
    real_type t5   = Q__[1];
    real_type t6   = X__[1];
    real_type t7   = cos(t6);
    real_type t8   = 1.0 / t7;
    real_type t10  = X__[2];
    real_type t11  = 1.0 / t10;
    real_type t12  = t11 * t8 * t5;
    result__[ 0   ] = -t12 * t4;
    real_type t16  = t5 * X__[0] - 1;
    real_type t18  = t7 * t7;
    real_type t21  = sin(t6);
    real_type t22  = t21 * t11 / t18;
    result__[ 1   ] = -t22 * t16 * t4;
    real_type t25  = t10 * t10;
    real_type t27  = 1.0 / t25 * t8 * t16;
    result__[ 2   ] = t27 * t4;
    real_type t31  = L__[5] / ModelPars[63];
    result__[ 3   ] = -t12 * t31;
    result__[ 4   ] = -t22 * t16 * t31;
    result__[ 5   ] = t27 * t31;
    if ( m_debug )
      Mechatronix::check_in_segment( result__,"DHuDx_sparse", 6, i_segment );
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DHuDp_numRows() const
  { return 2; }

  integer
  kineDyn_OCP::DHuDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DHuDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DHuDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DHuDp_sparse(
    NodeType2 const    & NODE__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |  _   _
   | | | | |_ __
   | | |_| | '_ \
   | |  _  | |_) |
   | |_| |_| .__/
   |       |_|
  \*/

  integer
  kineDyn_OCP::Hp_numEqns() const
  { return 0; }

  void
  kineDyn_OCP::Hp_eval(
    NodeType2 const    & NODE__,
    V_const_pointer_type V__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);

    if ( m_debug )
      Mechatronix::check_in_segment( result__, "Hp_eval", 0, i_segment );
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DHpDp_numRows() const
  { return 0; }

  integer
  kineDyn_OCP::DHpDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DHpDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DHpDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DHpDp_sparse(
    NodeType2 const    & NODE__,
    V_const_pointer_type V__,
    U_const_pointer_type U__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |        _
   |    ___| |_ __ _
   |   / _ \ __/ _` |
   |  |  __/ || (_| |
   |   \___|\__\__,_|
  \*/

  integer
  kineDyn_OCP::eta_numEqns() const
  { return 6; }

  void
  kineDyn_OCP::eta_eval(
    NodeType2 const    & NODE__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer i_segment     = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    real_type const * L__ = NODE__.lambda;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = L__[0];
    result__[ 1   ] = L__[1];
    result__[ 2   ] = L__[2];
    result__[ 3   ] = L__[3];
    result__[ 4   ] = L__[4];
    result__[ 5   ] = L__[5];
    if ( m_debug )
      Mechatronix::check_in_segment( result__,"eta_eval",6, i_segment );
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DetaDx_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DetaDx_numCols() const
  { return 6; }

  integer
  kineDyn_OCP::DetaDx_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DetaDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DetaDx_sparse(
    NodeType2 const    & NODE__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DetaDp_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DetaDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DetaDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DetaDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DetaDp_sparse(
    NodeType2 const    & NODE__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  /*\
   |    _ __  _   _
   |   | '_ \| | | |
   |   | | | | |_| |
   |   |_| |_|\__,_|
  \*/

  integer
  kineDyn_OCP::nu_numEqns() const
  { return 6; }

  void
  kineDyn_OCP::nu_eval(
    NodeType const     & NODE__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    integer     i_segment = NODE__.i_segment;
    real_type const * Q__ = NODE__.q;
    real_type const * X__ = NODE__.x;
    Path2D::SegmentClass const & segment = pTrajectory->getSegmentByIndex(i_segment);
    result__[ 0   ] = V__[0];
    result__[ 1   ] = V__[1];
    result__[ 2   ] = V__[2];
    result__[ 3   ] = V__[3];
    result__[ 4   ] = V__[4];
    result__[ 5   ] = V__[5];
    if ( m_debug )
      Mechatronix::check_in_segment( result__, "nu_eval", 6, i_segment );
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DnuDx_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DnuDx_numCols() const
  { return 6; }

  integer
  kineDyn_OCP::DnuDx_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DnuDx_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DnuDx_sparse(
    NodeType const     & NODE__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  integer
  kineDyn_OCP::DnuDp_numRows() const
  { return 6; }

  integer
  kineDyn_OCP::DnuDp_numCols() const
  { return 0; }

  integer
  kineDyn_OCP::DnuDp_nnz() const
  { return 0; }

  void
  kineDyn_OCP::DnuDp_pattern(
    integer iIndex[],
    integer jIndex[]
  ) const {
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void
  kineDyn_OCP::DnuDp_sparse(
    NodeType const     & NODE__,
    V_const_pointer_type V__,
    P_const_pointer_type P__,
    real_type            result__[]
  ) const {
    // EMPTY!
  }

}

// EOF: kineDyn_OCP_Methods.cc
