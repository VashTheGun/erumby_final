% ----------------------------------------------------------------
%% Main file for motion planning and control using NMPC
% ----------------------------------------------------------------

% Optitrack Matlab / NatNet Polling Sample
%  Requirements:
%   - OptiTrack Motive 2.0 or later
%   - OptiTrack NatNet 3.0 or later

clearvars;
close all;
clc;

% set this flag to 1 in order to enable UDP communication with Raspberry Pi
enable_UDP_rasp = 0;

% set this flag to 1 in order to receive data from the OptiTrack via UDP
enable_UDP_OptiTrack = 0;

% set this flag to 1 in order to enable docked window style in plots
enable_docked = 1;
if (enable_docked)
    set(0,'DefaultFigureWindowStyle','docked');
else
    set(0,'DefaultFigureWindowStyle','normal');
end
set(0,'defaultAxesFontSize',18)
set(0,'DefaultLegendFontSize',18)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

% addpath('./Clothoids/matlab');
addpath('./Utilities');
addpath('../../../../../../lib');


% ----------------------------------------------------
%% Initialization
% ----------------------------------------------------
% OCP problem
ocp = kineDyn_OCP('kineDyn_OCP');
dataFullLap = ocp.read('../../data/kineDyn_OCP_Data.rb');
dataFullLap.InfoLevel = 0;
ocp.infoLevel(-1);
% dataFullLap.Parameters.v_max = 2;
% if (dataFullLap.Parameters.wU==0)
%     dataFullLap.Parameters.wT = 0.1;
% else
%     dataFullLap.Parameters.wT = 0;
% end
dataFullLap.Solver.max_iter = 100;

% vehicle data
vehicle_data = getVehicleDataStruct();
L  = vehicle_data.vehicle.L;
Lf = vehicle_data.vehicle.Lf;
Lr = vehicle_data.vehicle.Lr;
W  = vehicle_data.vehicle.W;
r_tire     = vehicle_data.vehicle.r_tire;
delta_offs = vehicle_data.vehicle.delta_offs;

% MPC parameters
MPC_params = loadMPCPars();
replan_time = MPC_params.replan_time;

% Load road margins
roadMargins = readtable('../../../Custom_Tracks/Pergine_Track/roadMargins_Pergine.txt');
x_RightMargin = roadMargins.x_RightMargin;
y_RightMargin = roadMargins.y_RightMargin;
x_LeftMargin  = roadMargins.x_LeftMargin;
y_LeftMargin  = roadMargins.y_LeftMargin;


% ------------------
%% Pergine circuit
% ------------------
% track parameters
circuitData = readtable('../../../Custom_Tracks/Pergine_Track/circuit_Pergine_clothoids.txt');
x_road = circuitData.X;
y_road = circuitData.Y;
theta_road = circuitData.theta;

S = ClothoidList();
for i = 1:length(x_road)-1
    S.push_back_G1(x_road(i),y_road(i),theta_road(i), x_road(i+1),y_road(i+1),theta_road(i+1)) ; % track creation
end
track_length = length(S);    % track length [m]


% ----------------------------------------------------
%% Client-Server connection
% ----------------------------------------------------
if (enable_UDP_rasp)
    % connect the client to the server (multicast over local loopback) -
    % modify for your network
    fprintf( 'Connecting to the server\n' )
    
    % create udp sender
    IPaddr_raspberry = '10.0.0.1';
    IPaddr_OptiTrack = '192.168.10.20';
    udps = dsp.UDPSender('RemoteIPAddress',IPaddr_raspberry,'RemoteIPPort',25001);
    udpr = dsp.UDPReceiver('RemoteIPAddress',IPaddr_OptiTrack,'LocalIPPort',25000,'MessageDataType','int16');
    %udpr = dsp.UDPReceiver('RemoteIPAddress',IPaddr_raspberry,'LocalIPPort',25000,'MessageDataType','double');
    
    h = raspberrypi('10.0.0.1','pi','raspberry'); % wifi rasp object
    
%     h.stopModel('master_code_V3_1');
    disp('Press a key to run the code')
    pause();
    h.runModel('master_code_V3_1');
    pause(8);  % wait while the UDP communication is being set up
end
    
dataRec = ones(100000,4)*NaN;
dataSen = ones(100000,21)*NaN;
idx_send = 1;
idx_rec  = 1;


% ----------------------------------------------------
%% Initial conditions for vehicle states and pose
% ----------------------------------------------------
if (enable_UDP_OptiTrack)
    while(1)
        dataReceived = udpr();
        if(~isempty(dataReceived))
            % disp(dataReceived);
            dataRec(idx_rec,1:3) = dataReceived';
            dataRec(idx_rec,4) = str2double(datestr(clock,'SS.FFF'));
            idx_rec = idx_rec+1;
            break;
        end
    end
    x_veh_ini   = dataRec(idx_rec-1,1)/1000;
    y_veh_ini   = dataRec(idx_rec-1,2)/1000;
    psi_veh_ini = (wrapTo2Pi(double(dataRec(idx_rec-1,3))/1000));
    disp([x_veh_ini,y_veh_ini,rad2deg(psi_veh_ini),rad2deg(dataRec(idx_rec-1,3)/1000)]);
else
    x_veh_ini   = -0.6;          
    y_veh_ini   = 0.06;          
    psi_veh_ini = (deg2rad(345));  
end
    
pose_optiTrack = [x_veh_ini,y_veh_ini,psi_veh_ini];
[xCloth_ini,yCloth_ini,curvAbsc_ini,n_ini,~,~] = S.closestPoint(x_veh_ini,y_veh_ini);
[~,~,thetaCloth_ini,~] = S.evaluate(curvAbsc_ini);
thetaCloth_ini = (thetaCloth_ini);
% n_ini  = -n_ini;
xi_ini = psi_veh_ini - thetaCloth_ini;
if (xi_ini>pi)
    xi_ini = psi_veh_ini - 2*pi - thetaCloth_ini;
elseif (xi_ini<-pi)
    xi_ini = psi_veh_ini - (thetaCloth_ini-2*pi);
end
% Initial conditions for the full set of vehicle states
initCondits = loadInitialConditions_eRumby(n_ini,xi_ini,curvAbsc_ini);


% ----------------------------------------------------
%% MPC with receding horizon
% ----------------------------------------------------
% Parameters initialization
n_samples_MPC = 10; % Number of the samples on the MPC horizon that are interpolated and
                    % sent to the Raspberry
lowLevel_sampling = MPC_params.lowLevel_sampling; % [ms] Time distance between samples sent to the Raspberry

MHT.time            = ones(401,2000)*NaN;
MHT.xCoM            = ones(401,2000)*NaN;
MHT.yCoM            = ones(401,2000)*NaN;
MHT.xRightCar       = ones(401,2000)*NaN;
MHT.yRightCar       = ones(401,2000)*NaN;
MHT.xLeftCar        = ones(401,2000)*NaN;
MHT.yLeftCar        = ones(401,2000)*NaN;
MHT.vehicleAttitude = ones(401,2000)*NaN;
MHT.delta           = ones(401,2000)*NaN;
MHT.u               = ones(401,2000)*NaN;
MHT.Omega           = ones(401,2000)*NaN;
MHT.a_x             = ones(401,2000)*NaN;
MHT.n               = ones(401,2000)*NaN;
MHT.xi              = ones(401,2000)*NaN;
MHT.abscissa        = ones(401,2000)*NaN;

interp_idx_save   = 1;
u_interp_save     = ones(10000,1)*NaN;
delta_interp_save = ones(10000,1)*NaN;
time_interp_save  = ones(10000,1)*NaN;

Niter = 0;  % number of the current MPC iteration

assignin('base','solve_OCP_now',0);
assignin('base','send_Controls_now',0);

% -----------
% Send zero controls to the Raspberry while the connection is established
% -----------
u_ref_wait     = zeros(10,1);
delta_ref_wait = zeros(10,1);
u_ref_wait_transf     = (u_ref_wait/r_tire)*100;
delta_ref_wait_transf = 6881 - ((delta_ref_wait+delta_offs)*4402);
dataSent = [int16(u_ref_wait_transf);int16(delta_ref_wait_transf)];
tic;
while(toc<=5)
    if (enable_UDP_rasp)
        udps(dataSent);
    end
end

% -----------
% Timer object to schedule MPC execution
% -----------
solve_OCP_now = 0;
timerObj_ocp = timer('TimerFcn','solve_OCP_now = timerFunct_ocp(solve_OCP_now);','Period',replan_time, ...  %solve_OCP_now = 2; 
    'ExecutionMode','fixedRate','BusyMode','queue','StopFcn','disp(''Timer has stopped.'')'); 
start(timerObj_ocp);
solve_OCP_now = 0;  % reset timer flag


% ---------------------------
%% First iteration of MPC
% ---------------------------
states_in   = zeros(401,8);
pose_in     = zeros(401,11);
lambda_in   = zeros(401,6);
controls_in = zeros(401,2);
flag_allMPCconverged_in = 1;

% -----------
% Solve the optimal control problem
% -----------
[states_out,controls_out,pose_out,lambda_out,horizon_MHT,cpu_time,converged_MPC_flag,flag_allMPCconverged_out,poseRoad_ini,ocp_obj] = MHT_erumby(states_in,pose_in,pose_optiTrack,flag_allMPCconverged_in,dataFullLap,S,ocp,NaN,Niter,replan_time,track_length,initCondits);
extract_MHT_results_eRumby;

Niter = Niter+1;

% -----------
% Interpolate the optimal controls at a higher rate
% -----------
time_vector_interp = (cpu_time/1000:lowLevel_sampling:cpu_time/1000 + lowLevel_sampling*(n_samples_MPC-1))';
u_ctr_interp       = interp1(time_rep,u_rep,time_vector_interp);
delta_ctr_interp   = interp1(time_rep,delta_rep,time_vector_interp);

time_interp_save(interp_idx_save:interp_idx_save+n_samples_MPC-1)  = (Niter-1)*replan_time + time_vector_interp;
u_interp_save(interp_idx_save:interp_idx_save+n_samples_MPC-1)     = u_ctr_interp;
delta_interp_save(interp_idx_save:interp_idx_save+n_samples_MPC-1) = delta_ctr_interp;
interp_idx_save = interp_idx_save+n_samples_MPC;

% -----------
% Scale the optimal controls to be sent to the Raspberry
% -----------
u_ctr_interp_transf     = (u_ctr_interp/r_tire)*100;
delta_ctr_interp_transf = 6881 - ((delta_ctr_interp+delta_offs)*4402);
dataSent = [int16(u_ctr_interp_transf);int16(delta_ctr_interp_transf)];

% -----------
% Send the solution to Raspberry via UDP
% -----------
% (n_samples_MPC points on the horizon will be sent)
if (enable_UDP_rasp)
    udps(dataSent);
end
dataSen(idx_send,1:20) = dataSent';
dataSen(idx_send,21)   = str2double(datestr(clock,'SS.FFF'));
idx_send = idx_send+1;


% ---------------------------
%% MPC loop
% ---------------------------
while (Niter < 300) 
    
    if (enable_UDP_rasp)
        dataReceived = udpr();
        if(~isempty(dataReceived))
            % disp(dataReceived);
            dataRec(idx_rec,1:3) = dataReceived';
            dataRec(idx_rec,4) = str2double(datestr(clock,'SS.FFF'));
            idx_rec = idx_rec+1;
        end
    end
        
    if (solve_OCP_now==2)
        % -----------
        % Solve the optimal control problem
        % -----------
        solve_OCP_now = 0;  % reset the flag to solve the OCP        
        states_in = states_out;
        pose_in = pose_out;
        lambda_in = lambda_out;
        controls_in = controls_out;
        flag_allMPCconverged_in = flag_allMPCconverged_out;
        % Current vehicle pose 
        if (enable_UDP_OptiTrack && ~isnan(dataRec(idx_rec-1,1)))
            x_veh_ini   = dataRec(idx_rec-1,1)/1000;
            y_veh_ini   = dataRec(idx_rec-1,2)/1000;
            psi_veh_ini = (wrapTo2Pi(double(dataRec(idx_rec-1,3))/1000));
            disp([x_veh_ini,y_veh_ini,rad2deg(psi_veh_ini),rad2deg(dataRec(idx_rec-1,3)/1000)]);
        else
            x_veh_ini   = interp1(time_rep,xCoM_rep,replan_time);
            y_veh_ini   = interp1(time_rep,yCoM_rep,replan_time);
            psi_veh_ini = (interp1(time_rep,vehicleAttitude_rep,replan_time));
        end
        pose_optiTrack = [x_veh_ini,y_veh_ini,psi_veh_ini];
        
        [states_out,controls_out,pose_out,lambda_out,horizon_MHT,cpu_time,converged_MPC_flag,flag_allMPCconverged_out,poseRoad_ini,ocp_obj] = MHT_erumby(states_in,pose_in,pose_optiTrack,flag_allMPCconverged_in,dataFullLap,S,ocp,ocp_obj,Niter,replan_time,track_length,initCondits);
        extract_MHT_results_eRumby;
        
        Niter = Niter+1;
        
        % -----------
        % Interpolate the optimal controls at a higher rate
        % -----------
        time_vector_interp = (cpu_time/1000:lowLevel_sampling:cpu_time/1000 + lowLevel_sampling*(n_samples_MPC-1))';
        u_ctr_interp     = interp1(time_rep,u_rep,time_vector_interp);
        delta_ctr_interp = interp1(time_rep,delta_rep,time_vector_interp);
        if (flag_allMPCconverged_out==0)
            % if the OCP does not converge, stop the vehicle
            u_ctr_interp = zeros(n_samples_MPC,1);
            delta_ctr_interp = zeros(n_samples_MPC,1);
        end
        time_interp_save(interp_idx_save:interp_idx_save+n_samples_MPC-1)  = (Niter-1)*replan_time + time_vector_interp;
        u_interp_save(interp_idx_save:interp_idx_save+n_samples_MPC-1)     = u_ctr_interp;
        delta_interp_save(interp_idx_save:interp_idx_save+n_samples_MPC-1) = delta_ctr_interp;
        interp_idx_save = interp_idx_save+n_samples_MPC;
        
        % -----------
        % Scale the optimal controls to be sent to the Raspberry
        % -----------
        u_ctr_interp_transf     = (u_ctr_interp/r_tire)*100;
        delta_ctr_interp_transf = 6881 - ((delta_ctr_interp+delta_offs)*4402);
        dataSent = [int16(u_ctr_interp_transf);int16(delta_ctr_interp_transf)];
        
        % -----------
        % Send the solution to Raspberry via UDP
        % -----------
        % (n_samples_MPC points on the horizon will be sent)   
        if (enable_UDP_rasp)
            udps(dataSent);
        end
        dataSen(idx_send,1:20) = dataSent';
        dataSen(idx_send,21) = str2double(datestr(clock,'SS.FFF'));
        idx_send = idx_send+1;    
    end
    
end

% -----------
% Send zero controls to the Raspberry when the MPC has finished
% -----------
u_ref_wait     = zeros(10,1);
delta_ref_wait = zeros(10,1);
u_ref_wait_transf     = (u_ref_wait/r_tire)*100;
delta_ref_wait_transf = 6881 - ((delta_ref_wait+delta_offs)*4402);
dataSent = [int16(u_ref_wait_transf);int16(delta_ref_wait_transf)];
tic;
while(toc<=5)
    if (enable_UDP_rasp)
        udps(dataSent);
    end
end

save_telemetryData = 1;
fprintf('\nNatNet Polling Sample End\n');

stop(timerObj_ocp);

dataSent_store = dataSen(1:idx_send-1,:);
dataRece_store = dataRec(1:idx_rec-1,:);
if (enable_UDP_rasp)
    h.stopModel('master_code_V3_1');
    release(udps);
    release(udpr);
end

delete(timerObj_ocp);
delete(timerfindall);

% ----------------------------------------------------
%% Post Processing and Data Analysis
% ----------------------------------------------------
postProcessing_eRumby;

