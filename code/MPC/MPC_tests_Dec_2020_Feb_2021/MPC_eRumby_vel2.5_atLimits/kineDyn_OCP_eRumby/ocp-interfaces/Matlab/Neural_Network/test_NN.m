% ---------------------------------------------------
%% Testing the NN for the inverse steering dynamics
% ---------------------------------------------------

clc; clearvars;

% ------------------
% Load the NN parameters
% ------------------
NN_params = load_NN_data();

% ------------------
% Define the NN inputs
% ------------------
NN_in.future_curvat = ones(NN_params.length_futWindow,1)*0.001;
NN_in.future_speed  = ones(NN_params.length_futWindow,1)*20;
NN_in.autoReg_steer = ones(NN_params.length_pastWindow,1)*0.2;

% ------------------
% Compute the NN output
% ------------------
NN_out = NN_fcn(NN_in,NN_params);
