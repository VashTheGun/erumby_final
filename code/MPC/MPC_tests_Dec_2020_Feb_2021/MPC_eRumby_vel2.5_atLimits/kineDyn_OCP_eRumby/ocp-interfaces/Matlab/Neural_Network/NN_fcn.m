function NN_out = NN_fcn(NN_in,NN_params)

    % --------------------------------------------
    %% Neural network function (rebuilt in Matlab)
    % --------------------------------------------

    % Extract the future desired state trajectories
    vx_in   = NN_in.future_speed; 
    curv_in = NN_in.future_curvat; 
    
    % Compute the output of the threading / transformation layers
    NN_out = NN_params.S_coeff*curv_in.*(1 + NN_params.A_coeff*vx_in.^2);
    
    % Compute the output of the fully connected layer
    NN_out = NN_params.FC1_weights*NN_out;

    % Auto-regressive component
    NN_autoReg = NN_params.FC_autoReg_weights*NN_in.autoReg_steer;

    % Compute the output of the NN
    NN_out = NN_out + NN_autoReg;
        
end

