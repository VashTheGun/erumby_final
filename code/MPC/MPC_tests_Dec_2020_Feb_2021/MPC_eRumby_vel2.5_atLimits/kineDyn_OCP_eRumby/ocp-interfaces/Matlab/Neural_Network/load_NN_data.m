function NN_params = load_NN_data()

    % -------------------------
    %% Parameters of the NN
    % -------------------------

    % Properties of the threading layer to model the understeering gradient)
    A_coeff = load('Mathematica/exported_net_params/A_coeff');
    S_coeff = load('Mathematica/exported_net_params/S_coeff');
    NN_params.A_coeff = A_coeff.Expression1;
    NN_params.S_coeff = S_coeff.Expression1;
    
    % Properties of the fully connected layers 
    FC1_weights = load('Mathematica/exported_net_params/FC1_weights');
    NN_params.FC1_weights = FC1_weights.Expression1;

    % Properties of the auto-regressive layers 
    FC_autoReg_weights = load('Mathematica/exported_net_params/FC_autoReg_weights');
    NN_params.FC_autoReg_weights = FC_autoReg_weights.Expression1;
    
    length_pastWindow = length(NN_params.FC_autoReg_weights);
    length_futWindow  = length(NN_params.FC1_weights);
    NN_params.length_pastWindow = length_pastWindow;
    NN_params.length_futWindow  = length_futWindow;
    NN_params.Ts_NN             = 0.01;  % [s]
    NN_params.input_sampling    = 0.01;  % [s]
    NN_params.num_outputs_NN    = 1;

end

