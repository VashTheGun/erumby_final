% ----------------------------------------------------------------
%% Main file for motion planning and control using NMPC (imposed open loop speed and steer profiles)
% ----------------------------------------------------------------

% Optitrack Matlab / NatNet Polling Sample
%  Requirements:
%   - OptiTrack Motive 2.0 or later
%   - OptiTrack NatNet 3.0 or later

clearvars;
close all;
clc;

% set this flag to 1 in order to enable UDP communication with Raspberry Pi
enable_UDP_rasp = 0;

% set this flag to 1 in order to receive data from the OptiTrack via UDP
enable_UDP_OptiTrack = 0;

simulink_modelname = 'master_code_V3_1_imposed';

% set this flag to 1 in order to enable docked window style in plots
enable_docked = 1;
if (enable_docked)
    set(0,'DefaultFigureWindowStyle','docked');
else
    set(0,'DefaultFigureWindowStyle','normal');
end
set(0,'defaultAxesFontSize',18)
set(0,'DefaultLegendFontSize',18)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

% addpath('./Clothoids/matlab');
addpath('./Utilities');
addpath('../../../../../../lib');


% ----------------------------------------------------
%% Initialization
% ----------------------------------------------------
% Load the speed and steer profiles to be fed to the vehicle (in open loop)
fileName_load = 'testMPC9_rec';
loadedData    = load(strcat('../../../../MPC_test_data/',fileName_load));
time_MPC  = loadedData.savedData.MPC_solution.time_fullMPC;
u_MPC     = loadedData.savedData.MPC_solution.u_fullMPC;
delta_MPC = loadedData.savedData.MPC_solution.delta_fullMPC;

% vehicle data
vehicle_data = getVehicleDataStruct();
L  = vehicle_data.vehicle.L;
Lf = vehicle_data.vehicle.Lf;
Lr = vehicle_data.vehicle.Lr;
W  = vehicle_data.vehicle.W;
r_tire     = vehicle_data.vehicle.r_tire;
delta_offs = vehicle_data.vehicle.delta_offs;

% MPC parameters
MPC_params = loadMPCPars();
lowLevel_sampling = MPC_params.lowLevel_sampling;  % [s] Time distance between samples sent to the Raspberry

% Load road margins
roadMargins = readtable('../../../Custom_Tracks/Pergine_Track/roadMargins_Pergine.txt');
x_RightMargin = roadMargins.x_RightMargin;
y_RightMargin = roadMargins.y_RightMargin;
x_LeftMargin  = roadMargins.x_LeftMargin;
y_LeftMargin  = roadMargins.y_LeftMargin;


% ------------------
%% Change the speed and/or steer profiles
% ------------------
time_lim = 22;  % [s]
if (~isempty(find(time_MPC>time_lim,1)))
    idx_apply_end = find(time_MPC>time_lim,1);
else
    idx_apply_end = length(time_MPC);
end

time_MPC_applied  = time_MPC(1:idx_apply_end);
u_MPC_applied     = u_MPC(1:idx_apply_end);
additive_steer = deg2rad(4)*sin(2*pi*0.10*time_MPC_applied);
delta_MPC_applied = delta_MPC(1:idx_apply_end) + additive_steer;

% Plot the speed and steer profiles
figure('Name','Applied controls','NumberTitle','off'), clf
% --- speed --- %
ax(1) = subplot(211);
hold on
plot(time_MPC_applied,u_MPC_applied,'.')
grid on
title('speed $u$ [m/s]')  
% --- steer --- %
ax(2) = subplot(212);
hold on
plot(time_MPC_applied,rad2deg(delta_MPC(1:idx_apply_end)),'.')
plot(time_MPC_applied,rad2deg(delta_MPC_applied),'.')
grid on
title('steer $\delta$ [deg]')  
legend('original','modif and applied','location','best')


% ------------------
%% Pergine circuit
% ------------------
% track parameters
circuitData = readtable('../../../Custom_Tracks/Pergine_Track/circuit_Pergine_clothoids.txt');
x_road = circuitData.X;
y_road = circuitData.Y;
theta_road = circuitData.theta;

S = ClothoidList();
for i = 1:length(x_road)-1
    S.push_back_G1(x_road(i),y_road(i),theta_road(i), x_road(i+1),y_road(i+1),theta_road(i+1)) ; % track creation
end
track_length = length(S);    % track length [m]


% ----------------------------------------------------
%% Client-Server connection
% ----------------------------------------------------
if (enable_UDP_rasp)
    % connect the client to the server (multicast over local loopback) -
    % modify for your network
    fprintf( 'Connecting to the server\n' )
    
    % create udp sender
    IPaddr_raspberry = '10.0.0.1';
    IPaddr_OptiTrack = '192.168.10.20';
    udps = dsp.UDPSender('RemoteIPAddress',IPaddr_raspberry,'RemoteIPPort',25001);
    udpr = dsp.UDPReceiver('RemoteIPAddress',IPaddr_OptiTrack,'LocalIPPort',25000,'MessageDataType','int16');
    %udpr = dsp.UDPReceiver('RemoteIPAddress',IPaddr_raspberry,'LocalIPPort',25000,'MessageDataType','double');
    
    h = raspberrypi('10.0.0.1','pi','raspberry'); % wifi rasp object
    
%     h.stopModel(simulink_modelname);
    disp('Press a key to run the code')
    pause();
    h.runModel(simulink_modelname);
    pause(2);  % wait while the UDP communication is being set up
end


% ----------------------------------------------------
%% Initialization
% ----------------------------------------------------
assignin('base','send_Controls_now',0);

% -----------
% Send zero controls to the Raspberry while the connection is established
% -----------
u_ref_wait     = 0;
delta_ref_wait = 0;
u_ref_wait_transf     = (u_ref_wait/r_tire)*100;
delta_ref_wait_transf = 6881 - ((delta_ref_wait+delta_offs)*4402);
dataSent = [int16(u_ref_wait_transf); int16(delta_ref_wait_transf)];
tic;
while(toc<=5)
    if (enable_UDP_rasp)
        udps(dataSent);
    end
end


% ----------------------------------------------------
%% Send open loop controls
% ----------------------------------------------------
% Timer object to schedule the times at which the controls are sent to the vehicle
send_Controls_now = 0;
timerObj_ctrl = timer('TimerFcn','send_Controls_now = timerFunct_ocp(send_Controls_now);','Period',lowLevel_sampling, ...  %send_Controls_now = 2; 
    'ExecutionMode','fixedRate','BusyMode','queue','StopFcn','disp(''Timer has stopped.'')'); 
start(timerObj_ctrl);
send_Controls_now = 0;  % reset timer flag

num_ctrl_send = time_lim/lowLevel_sampling;

u_interp_save     = ones(num_ctrl_send,1)*NaN;
delta_interp_save = ones(num_ctrl_send,1)*NaN;
time_interp_save  = ones(num_ctrl_send,1)*NaN;

dataSen = ones(num_ctrl_send,2)*NaN;

kk = 1;
while (kk<=num_ctrl_send)
    if (send_Controls_now==2)
        % -----------
        % Send the controls to the vehicle
        % -----------
        send_Controls_now = 0;  % reset the flag to send the controls
        time_send_now = kk*lowLevel_sampling;
        % interpolate the controls profiles to be sent to the rasp
        u_ctr_interp     = interp1(time_MPC_applied,u_MPC_applied,time_send_now);
        delta_ctr_interp = interp1(time_MPC_applied,delta_MPC_applied,time_send_now);
        fprintf('Sending u_ref = %.4e, delta_ref = %.4e\n',u_ctr_interp,delta_ctr_interp)
        
        time_interp_save(kk)  = time_send_now;
        u_interp_save(kk)     = u_ctr_interp;
        delta_interp_save(kk) = delta_ctr_interp;
        
        % -----------
        % Scale the optimal controls to be sent to the Raspberry
        % -----------
        u_ctr_interp_transf     = (u_ctr_interp/r_tire)*100;
        delta_ctr_interp_transf = 6881 - ((delta_ctr_interp+delta_offs)*4402);
        dataSent = [int16(u_ctr_interp_transf); int16(delta_ctr_interp_transf)];
        
        % -----------
        % Send the solution to Raspberry via UDP
        % -----------
        % (n_samples_MPC points on the horizon will be sent)   
        if (enable_UDP_rasp)
            udps(dataSent);
        end
        dataSen(kk,1:2) = dataSent';
        dataSen(kk,3) = str2double(datestr(clock,'SS.FFF'));
        kk = kk+1;
    end
end

% -----------
% Send zero controls to the Raspberry when the MPC has finished
% -----------
u_ref_wait     = 0;
delta_ref_wait = 0;
u_ref_wait_transf     = (u_ref_wait/r_tire)*100;
delta_ref_wait_transf = 6881 - ((delta_ref_wait+delta_offs)*4402);
dataSent = [int16(u_ref_wait_transf); int16(delta_ref_wait_transf)];
tic;
while(toc<=5)
    if (enable_UDP_rasp)
        udps(dataSent);
    end
end

stop(timerObj_ctrl);

dataSent_store = dataSen(1:num_ctrl_send,:);
if (enable_UDP_rasp)
    h.stopModel(simulink_modelname);
    release(udps);
    release(udpr);
end

delete(timerObj_ctrl);
delete(timerfindall);

% ----------------------------------------------------
%% Post Processing and Data Analysis
% ----------------------------------------------------
if (enable_UDP_rasp)
    if (~exist('controls_save','var'))
        h.getFile('controls_save.mat');
        h.getFile('sensors.mat');
        load('controls_save.mat');
        load('sensors.mat');
    else
        load('controls_save.mat');
        load('sensors.mat');
    end

    time_raspb      = controls_save(1,:);
    u_raspb_raw     = controls_save(2,:)*r_tire/100;
    delta_raspb_raw = controls_save(3,:);

    DUTY_SERVO_SX = 5024;  
    DUTY_SERVO_MIDDLE = 6881;
    DUTY_SERVO_DX = 8738;

    delta_raspb_rad = ((6881-delta_raspb_raw)/4402) - delta_offs;

    idx_start_rasp = find(u_raspb_raw,1);
    time_raspb_use = time_raspb(idx_start_rasp:end) - time_raspb(idx_start_rasp);
    u_raspb_use = u_raspb_raw(idx_start_rasp:end);
    delta_raspb_use = delta_raspb_rad(idx_start_rasp:end);
    
    if (save_telemetryData)
        saveTelemData;
    end
    extractTelemData;
end

compare_and_save_data;
