% ----------------------------------------------------------------------
%% Offline plots of MPC results
% ----------------------------------------------------------------------

set(0,'DefaultFigureWindowStyle','docked');
set(0,'defaultAxesFontSize',20)
set(0,'DefaultLegendFontSize',20)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

addpath('./Utilities');

enable_docked = 1;

% --------------------------
%% Load the data file and extract the information
% --------------------------
fileName_load = 'testMPC13_rec';
loadedData    = load(strcat('../../../../MPC_test_data/',fileName_load));
testData      = loadedData.savedData;
MPC_solution  = testData.MPC_solution;
Circuit       = testData.Circuit;
Telem         = testData.Telem;
sensorsData   = Telem.sensorsData;

% Start and final indices for the plots with telemetry data
idx_start_tel = 1;
idx_end_tel   = length(sensorsData.time_T)-1;

if (isfield(sensorsData,'time_O'))
    % Start and final indices for the plots with optitrack data
    idx_start_optiT = 1;
    idx_end_optiT   = length(sensorsData.time_O)-1;
end

% set this flag to 1 in order to plot the 3D G-G diagram 
enable_plot_GG_diagr = 0;

% vehicle data
vehicle_data = getVehicleDataStruct();
L  = vehicle_data.vehicle.L;
Lf = vehicle_data.vehicle.Lf;
Lr = vehicle_data.vehicle.Lr;
W  = vehicle_data.vehicle.W;
r_tire     = vehicle_data.vehicle.r_tire;
delta_offs = vehicle_data.vehicle.delta_offs;

% --------------------------
%% Display solution results
% --------------------------
fprintf('\n\n----------------------------- RESULTS -----------------------------\n');
fprintf('Lap time\t\t\t= %.3f s\n',MPC_solution.time_fullMPC(end))
fprintf('--------------------------- Convergence ---------------------------\n');
fprintf('N° of not converged OCPs\t= %d\n',MPC_solution.numNotConvergOCP)
if (MPC_solution.numNotConvergOCP~=0)
    fprintf('Not converged OCPs\t\t= %s\n',MPC_solution.notConverged_MPC_idx)
end
fprintf('--------------------------- CPU timing ----------------------------\n');
fprintf('max CPU time\t\t\t= %.2f ms @OCP n°%d\n',MPC_solution.max_cpuTime,MPC_solution.idx_maxcpuTime+1)
fprintf('min CPU time\t\t\t= %.2f ms @OCP n°%d\n',MPC_solution.min_cpuTime,MPC_solution.idx_mincpuTime+1)
fprintf('mean CPU time\t\t\t= %.2f ms\n',MPC_solution.mean_cpuTime)
fprintf('CPU time for 1st OCP\t\t= %.2f ms\n',MPC_solution.cpu_time_list(1))
fprintf('N° OCPs with CPU time > 100 ms\t= %d \n',length(MPC_solution.slow_MPC_idx));
if (~isempty(MPC_solution.slow_MPC_idx))
    fprintf('OCPs with CPU time > 100 ms\t= %s \n',mat2str((MPC_solution.slow_MPC_idx+1)'));
end
fprintf('-------------------------------------------------------------------\n');

% --------------------------
%% Plot vehicle path
% --------------------------
figure('Name','Vehicle Path','NumberTitle','off'), clf
hold on
plot(Circuit.x_RightMargin,Circuit.y_RightMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');
plot(Circuit.x_LeftMargin,Circuit.y_LeftMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');    
plot(MPC_solution.xRightCar_fullMPC,MPC_solution.yRightCar_fullMPC,'Color',color('blue'),'LineWidth',1,'HandleVisibility','off')
plot(MPC_solution.xLeftCar_fullMPC,MPC_solution.yLeftCar_fullMPC,'Color',color('blue'),'LineWidth',1,'HandleVisibility','off')
plot(MPC_solution.xCoM_fullMPC,MPC_solution.yCoM_fullMPC,'.')
if (isfield(sensorsData,'time_O'))
    plot(sensorsData.x_O(idx_start_optiT:idx_end_optiT),sensorsData.y_O(idx_start_optiT:idx_end_optiT),'.')
end
plot(sensorsData.x_gps_T(idx_start_tel:idx_end_tel),sensorsData.y_gps_T(idx_start_tel:idx_end_tel),'.')
grid on
axis equal
xlabel('x [m]')
ylabel('y [m]')
if (isfield(sensorsData,'time_O'))
    legend('MPC','OptiT','OptiT telem','location','best')
else
    legend('MPC','OptiT telem','location','best')
end
title('Vehicle Path, MPC with receding horizon')

% --------------------------
%% Longitudinal dynamics
% --------------------------
figure('Name','States','NumberTitle','off'), clf
% --- u --- %
ax(1) = subplot(321);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.u_fullMPC,'.')
plot(MPC_solution.time_interp_save,MPC_solution.u_interp_save,'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.target_speed_T(idx_start_tel:idx_end_tel),'.')
if (isfield(sensorsData,'time_O'))
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.Speed(idx_start_optiT:idx_end_optiT),'.')
end
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Speed_gps_T(idx_start_tel:idx_end_tel),'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rr_T(idx_start_tel:idx_end_tel)*r_tire,'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rl_T(idx_start_tel:idx_end_tel)*r_tire,'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_fr_T(idx_start_tel:idx_end_tel)*r_tire,'.')
if (isfield(sensorsData,'time_O'))
    legend('MPC','MPC interp','rasp ctrl','OptiT','OptiT telem','$\omega_{rr} r_w$','$\omega_{rl} r_w$','$\omega_{fr} r_w$','location','best')
else
    legend('MPC','MPC interp','rasp ctrl','OptiT telem','$\omega_{rr} r_w$','$\omega_{rl} r_w$','$\omega_{fr} r_w$','location','best')
end
grid on
title('$u$ [m/s]')  
% --- traction command --- %
ax(2) = subplot(222);
hold on
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.traction_T(idx_start_tel:idx_end_tel),'.')
grid on
title('traction command [percent]')
xlim([sensorsData.time_T(idx_start_tel) sensorsData.time_T(idx_end_tel)])
% --- Ax --- %
ax(3) = subplot(223);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.a_x_fullMPC,'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Ax_T(idx_start_tel:idx_end_tel),'.')
grid on
title('$A_{x}$ [m/s$^2$]')
legend('MPC','IMU','location','best')
xlim([sensorsData.time_T(idx_start_tel) sensorsData.time_T(idx_end_tel)])
% --- omega_wheels --- %
ax(4) = subplot(224);
hold on
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rr_T(idx_start_tel:idx_end_tel),'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rl_T(idx_start_tel:idx_end_tel),'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_fr_T(idx_start_tel:idx_end_tel),'.')
grid on
title('wheel speed [rad/s]')
legend('$\omega_{rr}$','$\omega_{rl}$','$\omega_{fr}$','location','best')
xlim([sensorsData.time_T(idx_start_tel) sensorsData.time_T(idx_end_tel)])

% --------------------------
%% Lateral dynamics
% --------------------------
figure('Name','lat dyna','NumberTitle','off'), clf   
% --- delta --- %
ax(1) = subplot(221);
hold on
plot(MPC_solution.time_fullMPC,rad2deg(MPC_solution.delta_fullMPC),'.')
plot(MPC_solution.time_interp_save,rad2deg(MPC_solution.delta_interp_save),'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),rad2deg(sensorsData.delta(idx_start_tel:idx_end_tel)),'.')
hold on
grid on
title('$\delta$ [deg]')
legend('MPC','MPC interp','rasp ctrl','location','best')
% --- Omega --- %
ax(2) = subplot(222);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.Omega_fullMPC,'.')
if (isfield(sensorsData,'time_O'))
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.Omega_O(idx_start_optiT:idx_end_optiT),'.')
end
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Omega_gps_T(idx_start_tel:idx_end_tel),'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Omega_T(idx_start_tel:idx_end_tel),'.')
grid on
title('$\Omega$ [rad/s]')
if (isfield(sensorsData,'time_O'))
    legend('MPC','OptiT','OptiT telem','IMU','location','best')
else
    legend('MPC','OptiT telem','IMU','location','best')
end
% --- Ay --- %
ax(3) = subplot(223);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.a_y_fullMPC,'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Ay_T(idx_start_tel:idx_end_tel),'.')
grid on
title('$A_{y}$ [m/s$^2$]')
legend('MPC','IMU','location','best')

% --------------------------
%% Plot vehicle pose
% --------------------------
figure('Name','Pose','NumberTitle','off'), clf
% --- xCoM --- %
ax(1) = subplot(231);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.xCoM_fullMPC,'.')
if (isfield(sensorsData,'time_O'))
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.x_O(idx_start_optiT:idx_end_optiT),'.')
end
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.x_gps_T(idx_start_tel:idx_end_tel),'.')
grid on
title('$x$ [m]')
if (isfield(sensorsData,'time_O'))
    legend('MPC','OptiT','OptiT telem','location','best')
else
    legend('MPC','OptiT telem','location','best')
end
% --- yCoM --- %
ax(2) = subplot(232);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.yCoM_fullMPC,'.')
if (isfield(sensorsData,'time_O'))
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.y_O(idx_start_optiT:idx_end_optiT),'.')
end
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.y_gps_T(idx_start_tel:idx_end_tel),'.')
grid on
title('$y$ [m]')
if (isfield(sensorsData,'time_O'))
    legend('MPC','OptiT','OptiT telem','location','best')
else
    legend('MPC','OptiT telem','location','best')
end
% --- n --- %
ax(3) = subplot(233);
hold on
plot(MPC_solution.time_fullMPC,MPC_solution.n_fullMPC,'LineWidth',2)
% plot(MPC_solution.time_fullMPC,MPC_solution.n_fullMPC,'go','MarkerFaceColor','g','MarkerSize',8)
grid on
title('$n$ [m]')  
legend('MPC','location','best')
% --- xi --- %
ax(4) = subplot(234);
plot(MPC_solution.time_fullMPC,rad2deg(MPC_solution.xi_fullMPC),'LineWidth',2)
grid on
title('$\xi$ [deg]')  
legend('MPC','location','best')
% --- psi --- %
ax(5) = subplot(235);
hold on
plot(MPC_solution.time_fullMPC,rad2deg(MPC_solution.vehicleAttitude_fullMPC),'.')
if (isfield(sensorsData,'time_O'))
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),rad2deg(sensorsData.psi_O(idx_start_optiT:idx_end_optiT)),'.')
end
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),rad2deg(sensorsData.psi_gps_T(idx_start_tel:idx_end_tel)),'.')
grid on
title('$\psi$ [deg]')
if (isfield(sensorsData,'time_O'))
    legend('MPC','OptiT','OptiT telem','location','best')
else
    legend('MPC','OptiT telem','location','best')
end

clear ax

% --------------------------
%% 3D G-G diagram
% --------------------------
if (enable_plot_GG_diagr)
    Axmax_fittingCoeffs = load('../../../GG_fittingData/Axmax_fittingCoeffs');
    Axmin_fittingCoeffs = load('../../../GG_fittingData/Axmin_fittingCoeffs');
    Aymax_fittingCoeffs = load('../../../GG_fittingData/Aymax_fittingCoeffs');
    Axmax_fitCoeffs = Axmax_fittingCoeffs.Axmax_fitCoeffs;
    Axmin_fitCoeffs = Axmin_fittingCoeffs.Axmin_fitCoeffs;
    Aymax_fitCoeffs = Aymax_fittingCoeffs.Aymax_fitCoeffs;

    speed_vect_fit = 0.5:0.1:2.5;  % [m/s]

    vehicle_data = getVehicleDataStruct();
    k_D = vehicle_data.vehicle.k_D;
    m   = vehicle_data.vehicle.m;
    
    % Subtract the aero drag offset to the positive values of a_x_fullMPC
%     a_x_fullMPC_noAeroOffset = MPC_solution.a_x_fullMPC;  % initialize
%     for uu=1:length(MPC_solution.a_x_fullMPC)
%         if (MPC_solution.a_x_fullMPC(uu)>0)
%             a_x_fullMPC_noAeroOffset(uu) = MPC_solution.a_x_fullMPC(uu) - (k_D/m)*(MPC_solution.u_fullMPC(uu)^2);
%         end
%     end

    % Limits for figure axes
    ax_negPlotLimit = -5;
    ax_posPlotLimit = 5;
    ay_negPlotLimit = -7;
    ay_posPlotLimit = 7;

    % Define a custom color order (each color corresponds to a certain speed value)
    ColorOdr = flipud(parula(length(speed_vect_fit)));

    if (~enable_docked)
        figure('Name','3D G-G','NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[50,50,1000,510]) 
    else
        figure('Name','3D G-G','NumberTitle','off'), clf
    end
    hold on
%     axis equal
    for jj = 1:length(speed_vect_fit) 
        ax_max  = Axmax_fitCoeffs(1) + Axmax_fitCoeffs(2)*speed_vect_fit(jj) + Axmax_fitCoeffs(3)*speed_vect_fit(jj).^2 + Axmax_fitCoeffs(4)*speed_vect_fit(jj).^3 + Axmax_fitCoeffs(5)*speed_vect_fit(jj).^4 + Axmax_fitCoeffs(6)*speed_vect_fit(jj).^5 + Axmax_fitCoeffs(7)*speed_vect_fit(jj).^6 + Axmax_fitCoeffs(8)*speed_vect_fit(jj).^7;
%         ax_max  = ax_max - (k_D/m)*speed_vect_fit(jj)^2;
        ax_min  = Axmin_fitCoeffs(1) + Axmin_fitCoeffs(2)*speed_vect_fit(jj) + Axmin_fitCoeffs(3)*speed_vect_fit(jj).^2 + Axmin_fitCoeffs(4)*speed_vect_fit(jj).^3 + Axmin_fitCoeffs(5)*speed_vect_fit(jj).^4;
        ay_max  = Aymax_fitCoeffs(1) + Aymax_fitCoeffs(2)*speed_vect_fit(jj) + Aymax_fitCoeffs(3)*speed_vect_fit(jj).^2 + Aymax_fitCoeffs(4)*speed_vect_fit(jj).^3 + Aymax_fitCoeffs(5)*speed_vect_fit(jj).^4 + Aymax_fitCoeffs(6)*speed_vect_fit(jj).^5;
        ax_offs = 0;
        n_upper = 2;
        n_lower = 2;
        GG_fit = @(ay,ax,u)generalizedEllipse(ax,ay,ax_max,ax_min,ay_max,n_upper,n_lower,ax_offs);
        fimplicit3(GG_fit,[ay_negPlotLimit ay_posPlotLimit ax_negPlotLimit-4 ax_posPlotLimit speed_vect_fit(jj) speed_vect_fit(jj)+0.01],'MeshDensity',40,'EdgeColor',ColorOdr(jj,:),'LineWidth',2.5)
    end
    plot3(MPC_solution.a_y_fullMPC,MPC_solution.a_x_fullMPC,MPC_solution.u_fullMPC,'Color',color('violet_red'),'LineWidth',4)
    grid on
    xlabel('$a_y$ [m/s$^2$]')
    ylabel('$a_x$ [m/s$^2$]')
    zlabel('$u$ [m/s]')
    title('G-G surface points from simulated maneuvers')
    oldcmap = colormap;
    colormap(flipud(oldcmap));
    hcb = colorbar;
    title(hcb,'Speed [m/s]','Interpreter','latex')
    caxis([speed_vect_fit(1), speed_vect_fit(end)]);
end



