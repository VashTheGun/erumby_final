function timings = loadTiming()

    % Timings
    T0 = 0;           % start simulation time
    Ts = 0.001;       % time step
    Tf = 100;          % stop simulation time
    
    timings.T0 = T0;
    timings.Ts = Ts;
    timings.Tf = Tf;

end

