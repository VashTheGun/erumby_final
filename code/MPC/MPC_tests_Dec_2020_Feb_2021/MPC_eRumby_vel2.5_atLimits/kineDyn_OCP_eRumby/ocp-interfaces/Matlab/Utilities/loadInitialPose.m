function initPose = loadInitialPose(x0_road,y0_road,n0,xi0,theta0)
    
    %% ICs for the vehicle pose
    % n0 and xi0 are the initial value of the vehicle lateral displacement w.r.t.
    % the road center line and the initial relative yaw angle (curvilinear coords)
    
    psi0 = theta0+xi0;  % [rad] initial vehicle attitude
    
    % Tmat is the transformation matrix between the vehicle frame and the 
    % absolute frame 
    Tmat = [cos(psi0) -sin(psi0) 0 x0_road-n0*sin(theta0);
            sin(psi0)  cos(psi0) 0 y0_road+n0*cos(theta0);
            0          0         1 0;
            0          0         0 1];
    
    initialPose = Tmat*[0,0,0,1]';
    
    x0 = initialPose(1);  % [m] initial x-coordinate of the vehicle CoM
    y0 = initialPose(2);  % [m] initial y-coordinate of the vehicle CoM
    
    initPose = [x0,y0,psi0]';
       
end

