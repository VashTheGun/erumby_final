% ----------------------------------------------------
%% Post Processing and Data Analysis
% ----------------------------------------------------


%% General info
flag_allMPCConverged = MHT.flag_allMPCconverged;
converged_MPC_flag = MHT.converged_MPC;

% Extract vehicle states and pose
kk = 1;
for ii=1:size(MHT.xCoM,2)
    xCoM_tmp = MHT.xCoM(:,ii);
    yCoM_tmp = MHT.yCoM(:,ii);
    xRightCar_tmp = MHT.xRightCar(:,ii);
    yRightCar_tmp = MHT.yRightCar(:,ii);
    xLeftCar_tmp = MHT.xLeftCar(:,ii);
    yLeftCar_tmp = MHT.yLeftCar(:,ii);    
    delta_tmp = MHT.delta(:,ii);
    u_tmp = MHT.u(:,ii);
    Omega_tmp = MHT.Omega(:,ii);
    a_x_tmp = MHT.a_x(:,ii);
    n_tmp = MHT.n(:,ii);
    xi_tmp = MHT.xi(:,ii);
    vehicleAttitude_tmp = MHT.vehicleAttitude(:,ii);
    
    xCoM_tmp_notNaN = xCoM_tmp((all((~isnan(xCoM_tmp)),2)),:);
    yCoM_tmp_notNaN = yCoM_tmp((all((~isnan(yCoM_tmp)),2)),:);
    xRightCar_tmp_notNaN = xRightCar_tmp((all((~isnan(xRightCar_tmp)),2)),:);
    yRightCar_tmp_notNaN = yRightCar_tmp((all((~isnan(yRightCar_tmp)),2)),:);
    xLeftCar_tmp_notNaN = xLeftCar_tmp((all((~isnan(xLeftCar_tmp)),2)),:);
    yLeftCar_tmp_notNaN = yLeftCar_tmp((all((~isnan(yLeftCar_tmp)),2)),:);
    delta_tmp_notNaN = delta_tmp((all((~isnan(delta_tmp)),2)),:);
    u_tmp_notNaN = u_tmp((all((~isnan(u_tmp)),2)),:);
    Omega_tmp_notNaN = Omega_tmp((all((~isnan(Omega_tmp)),2)),:);
    a_x_tmp_notNaN = a_x_tmp((all((~isnan(a_x_tmp)),2)),:);
    n_tmp_notNaN = n_tmp((all((~isnan(n_tmp)),2)),:);
    xi_tmp_notNaN = xi_tmp((all((~isnan(xi_tmp)),2)),:);
    vehicleAttitude_tmp_notNaN = vehicleAttitude_tmp((all((~isnan(vehicleAttitude_tmp)),2)),:);
     
    if (~isempty(xCoM_tmp_notNaN))
        xCoM_fullMPC(kk:kk+length(xCoM_tmp_notNaN)-1,1) = xCoM_tmp_notNaN;
        yCoM_fullMPC(kk:kk+length(yCoM_tmp_notNaN)-1,1) = yCoM_tmp_notNaN;
        xRightCar_fullMPC(kk:kk+length(xRightCar_tmp_notNaN)-1,1) = xRightCar_tmp_notNaN;
        yRightCar_fullMPC(kk:kk+length(yRightCar_tmp_notNaN)-1,1) = yRightCar_tmp_notNaN;
        xLeftCar_fullMPC(kk:kk+length(xLeftCar_tmp_notNaN)-1,1) = xLeftCar_tmp_notNaN;
        yLeftCar_fullMPC(kk:kk+length(yLeftCar_tmp_notNaN)-1,1) = yLeftCar_tmp_notNaN;
        delta_fullMPC(kk:kk+length(delta_tmp_notNaN)-1,1) = delta_tmp_notNaN;
        u_fullMPC(kk:kk+length(u_tmp_notNaN)-1,1) = u_tmp_notNaN;
        Omega_fullMPC(kk:kk+length(Omega_tmp_notNaN)-1,1) = Omega_tmp_notNaN;
        a_x_fullMPC(kk:kk+length(a_x_tmp_notNaN)-1,1) = a_x_tmp_notNaN;
        n_fullMPC(kk:kk+length(n_tmp_notNaN)-1,1) = n_tmp_notNaN;
        xi_fullMPC(kk:kk+length(xi_tmp_notNaN)-1,1) = xi_tmp_notNaN;
        vehicleAttitude_fullMPC(kk:kk+length(vehicleAttitude_tmp_notNaN)-1,1) = vehicleAttitude_tmp_notNaN;
        
        kk = kk+length(xCoM_tmp_notNaN);
    end
end
a_y_fullMPC = u_fullMPC.*Omega_fullMPC;

% Extract the interpolated controls
time_interp_save  = time_interp_save((all((~isnan(time_interp_save)),2)),:);
u_interp_save     = u_interp_save((all((~isnan(u_interp_save)),2)),:);
delta_interp_save = delta_interp_save((all((~isnan(delta_interp_save)),2)),:);

% Extract the time vector
time_fullMPC = zeros(size(u_fullMPC));
numTimeSamples_firstMPCIter = find(isnan(MHT.time(:,1)),1)-1;  % number of time samples for the 1st iteration
time_fullMPC(1:numTimeSamples_firstMPCIter) = MHT.time(1:numTimeSamples_firstMPCIter,1); % store time samples
timeDifference2Next = replan_time - MHT.time(numTimeSamples_firstMPCIter,1); % time necessary to complete this MPC iteration
c = numTimeSamples_firstMPCIter+1;
for b=2:Niter
    numTimeSamples_bthMPCIter = find(isnan(MHT.time(:,b)),1)-1;
    time_fullMPC(c) = time_fullMPC(c-1) + timeDifference2Next;
    c = c+1;
    if (numTimeSamples_bthMPCIter>1)
        for d=2:numTimeSamples_bthMPCIter
            time_fullMPC(c+d-2) = time_fullMPC(c-1) + MHT.time(d,b);
        end
    end
    c = c+numTimeSamples_bthMPCIter-1;
    timeDifference2Next = replan_time - MHT.time(numTimeSamples_bthMPCIter,b);
end

if all(flag_allMPCConverged)
    numNotConvergOCP = 0;
else
    notConverged_MPC_idx = mat2str((find(converged_MPC_flag==0))');
    numNotConvergOCP = length(find(converged_MPC_flag==0));
end

cpu_time_list = MHT.cpu_time;
[max_cpuTime,idx_maxcpuTime] = max(cpu_time_list(2:end));
[min_cpuTime,idx_mincpuTime] = min(cpu_time_list(2:end));
mean_cpuTime = mean(cpu_time_list(2:end),'omitnan');
slow_MPC_idx = find(cpu_time_list(2:end)>(replan_time/0.001));

fprintf('\n\n----------------------------- RESULTS -----------------------------\n');
fprintf('Lap time\t\t\t= %.3f s\n',time_fullMPC(end))
fprintf('--------------------------- Convergence ---------------------------\n');
fprintf('N° of not converged OCPs\t= %d\n',numNotConvergOCP)
if (numNotConvergOCP~=0)
    fprintf('Not converged OCPs\t\t= %s\n',notConverged_MPC_idx)
end
fprintf('--------------------------- CPU timing ----------------------------\n');
fprintf('max CPU time\t\t\t= %.2f ms @OCP n°%d\n',max_cpuTime,idx_maxcpuTime+1)
fprintf('min CPU time\t\t\t= %.2f ms @OCP n°%d\n',min_cpuTime,idx_mincpuTime+1)
fprintf('mean CPU time\t\t\t= %.2f ms\n',mean_cpuTime)
fprintf('CPU time for 1st OCP\t\t= %.2f ms\n',cpu_time_list(1))
fprintf('N° OCPs with CPU time > 100 ms\t= %d \n',length(slow_MPC_idx));
if (~isempty(slow_MPC_idx))
    fprintf('OCPs with CPU time > 100 ms\t= %s \n',mat2str((slow_MPC_idx+1)'));
end
fprintf('-------------------------------------------------------------------\n');

% ----------------------------------------------
%% Data Analysis
% ----------------------------------------------

% ------------------
%% Extract data from Raspberry
% ------------------
if (enable_UDP_rasp)
    if (~exist('controls_save','var'))
        h.getFile('controls_save.mat');
        h.getFile('sensors.mat');
        load('controls_save.mat');
        load('sensors.mat');
    else
        load('controls_save.mat');
        load('sensors.mat');
    end

    time_raspb      = controls_save(1,:);
    u_raspb_raw     = controls_save(2,:)*r_tire/100;
    delta_raspb_raw = controls_save(3,:);

    DUTY_SERVO_SX = 5024;  
    DUTY_SERVO_MIDDLE = 6881;
    DUTY_SERVO_DX = 8738;

    delta_raspb_rad = ((6881-delta_raspb_raw)/4402) - delta_offs;

    idx_start_rasp = find(u_raspb_raw,1);
    time_raspb_use = time_raspb(idx_start_rasp:end) - time_raspb(idx_start_rasp);
    u_raspb_use = u_raspb_raw(idx_start_rasp:end);
    delta_raspb_use = delta_raspb_rad(idx_start_rasp:end);
    
    if (save_telemetryData)
        saveTelemData;
    end
    extractTelemData;
end

% ------------------
%% Plot vehicle path
% ------------------
if (~enable_docked)
    figure('Name','Vehicle Path','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[50,50,1000,510]) 
else
    figure('Name','Vehicle Path','NumberTitle','off'), clf
end
hold on
plot(x_RightMargin,y_RightMargin,'Color',color('dark_green'),'LineWidth',2);
plot(x_LeftMargin,y_LeftMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');    
plot(xRightCar_fullMPC,yRightCar_fullMPC,'Color',color('blue'),'LineWidth',1)
plot(xLeftCar_fullMPC,yLeftCar_fullMPC,'Color',color('blue'),'LineWidth',1,'HandleVisibility','off')
plot(xCoM_fullMPC,yCoM_fullMPC,'Color',color('orange'),'LineWidth',2)
plot(xCoM_fullMPC,yCoM_fullMPC,'go','MarkerFaceColor','g','MarkerSize',4)
if (enable_UDP_rasp && (isstruct(remap_optitrack)))
    plot(sensorsData.x_O,sensorsData.y_O,'-','color',color('deepsky_blue'),'LineWidth',2)
end
grid on
axis equal
xlabel('x [m]')
ylabel('y [m]')
if (exist('use_test_telemetry_data','var') && use_test_telemetry_data)
    legend('Road boundaries','Car edges','CoM position','recorded lap','location','best')
else
    legend('Road boundaries','Car edges','CoM position MPC','CoM position MPC','location','best')
end
title('Vehicle Path, MPC with receding horizon')

% ------------------
%% Plot vehicle states
% ------------------
if (~enable_docked)
    figure('Name','States','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[50,50,1000,510]) 
else
    figure('Name','States','NumberTitle','off'), clf
end
% --- u --- %
ax(1) = subplot(321);
hold on
plot(time_fullMPC,u_fullMPC,'LineWidth',2,'HandleVisibility','off')
plot(time_fullMPC,u_fullMPC,'go','MarkerFaceColor','g','MarkerSize',8)
plot(time_interp_save,u_interp_save,'ro','MarkerSize',4)
if (enable_UDP_rasp)
    plot(time_raspb_use,u_raspb_use,'mo','MarkerFaceColor','m','MarkerSize',6)
    legend('MPC','interp','rasp','location','best')
else
    legend('MPC','interp','location','best')
end
grid on
title('$u$ [m/s]')  
% --- delta --- %
ax(2) = subplot(322);
hold on
plot(time_fullMPC,rad2deg(delta_fullMPC),'LineWidth',2,'HandleVisibility','off')
plot(time_fullMPC,rad2deg(delta_fullMPC),'go','MarkerFaceColor','g','MarkerSize',8)
plot(time_interp_save,rad2deg(delta_interp_save),'ro','MarkerSize',4)
if (enable_UDP_rasp)
    plot(time_raspb_use,rad2deg(delta_raspb_use),'mo','MarkerFaceColor','m','MarkerSize',6)
    legend('MPC','interp','rasp','location','best')
else
    legend('MPC','interp','location','best')
end
grid on
title('$\delta$ [deg]')
% --- Omega --- %
ax(3) = subplot(323);
hold on
plot(time_fullMPC,Omega_fullMPC,'LineWidth',2) 
if (enable_UDP_rasp)
    plot(sensorsData.time_T,sensorsData.Omega_T,'.')
    if (isstruct(remap_optitrack))
        plot(sensorsData.time_O(1:end-1),sensorsData.Omega_O,'.')
        legend('MPC','IMU','OptiT','location','best')
    else
        legend('MPC','IMU','location','best')
    end
end
grid on
title('$\Omega$ [rad/s]')
% --- a_x --- %
ax(4) = subplot(324);
hold on
plot(time_fullMPC,a_x_fullMPC,'LineWidth',2) 
% if (enable_UDP_rasp)
%     plot(time_telem,Ax_telem,'ko','MarkerSize',6)
%     legend('MPC','telem','location','best')
% end
grid on
title('$a_x$ [m/s$^2$]')
% --- a_y --- %
ax(5) = subplot(325);
hold on
plot(time_fullMPC,a_y_fullMPC,'LineWidth',2) 
if (enable_UDP_rasp)
    plot(sensorsData.time_T,sensorsData.Ay_T,'ko','MarkerSize',3)
    legend('MPC','IMU','location','best')
end
grid on
title('$a_y$ [m/s$^2$]')

% linkaxes(ax,'x')
clear ax

% ------------------
%% Plot vehicle pose
% ------------------
if (~enable_docked)
    figure('Name','Pose','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[50,50,1000,510]) 
else
    figure('Name','Pose','NumberTitle','off'), clf
end
% --- xCoM --- %
ax(1) = subplot(231);
hold on
plot(time_fullMPC,xCoM_fullMPC,'LineWidth',2)
% plot(time_fullMPC,xCoM_fullMPC,'go','MarkerFaceColor','g','MarkerSize',6)
grid on
title('x CoM [m]')  
legend('MPC','location','best')
% --- yCoM --- %
ax(2) = subplot(232);
plot(time_fullMPC,yCoM_fullMPC,'LineWidth',2)
grid on
title('y CoM [m]')
legend('MPC','location','best')
% --- n --- %
ax(3) = subplot(233);
hold on
plot(time_fullMPC,n_fullMPC,'LineWidth',2)
% plot(time_fullMPC,n_fullMPC,'go','MarkerFaceColor','g','MarkerSize',8)
grid on
title('$n$ [m]')  
legend('MPC','location','best')
% --- xi --- %
ax(4) = subplot(234);
plot(time_fullMPC,rad2deg(xi_fullMPC),'LineWidth',2)
grid on
title('$\xi$ [deg]')  
legend('MPC','location','best')
% --- psi --- %
ax(5) = subplot(235);
plot(time_fullMPC,rad2deg(vehicleAttitude_fullMPC),'LineWidth',2)
grid on
title('$\psi$ [deg]')  
legend('MPC','location','best')

% linkaxes(ax,'x')
clear ax

% ------------------
%% Comparison plots
% ------------------
if (enable_UDP_rasp)
    comparison_plots;
end

% ------------------
%% Save MPC results to files
% ------------------
if (enable_UDP_rasp)
    save_MPC_results;
end

% ------------------
%% Save the MHT struct for post-processing videos
% ------------------
% MHT.time_fullMPC = time_fullMPC;
% MHT.u_fullMPC    = u_fullMPC;
% MHT.a_x_fullMPC  = a_x_fullMPC;
% MHT.a_y_fullMPC  = a_y_fullMPC;
% save('../../../../MPC_test_data/MHT_data_4video','MHT');
