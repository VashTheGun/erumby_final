% ---------------------------------------------
%% Import telemetry and OptiTrack sensors data 
% ---------------------------------------------

telemetryData = strcat('./',testName,num2str(i-1),'.txt');
[telemetry] = telemetry_import_V2(telemetryData);
fprintf('\nThe txt telemetry data have been imported.\n')

optitrackData = strcat('./',testName,num2str(i-1),'.csv');
filter_flag = 0;
if (~exist(optitrackData,'file'))
    prompt = '\nDo you want to manually load the Optitrack data now?[y/n]\n';
    answer_load_Optitrack = input(prompt,'s');
    if (strcmpi(answer_load_Optitrack,'y'))
        fprintf('Now please export a csv data file from Motive (Optitrack) and copy-paste it in the MPC_test_data folder.\n')
        fprintf('The csv file must have the same name as the txt file.\n')
        fprintf('Press enter when you have done it.\n')
        pause();
        [optitrack] = optitrack_import(optitrackData);
        fprintf('\nThe csv Optitrack data have been imported.\n')        
        align_variable = 'none';
        clean_gps_telem = 0;  
        [remap_telemetry,remap_optitrack] = test_data_remap_V4(telemetry,optitrack,align_variable,filter_flag);
    else
        remap_telemetry = test_data_remap_V4_no_optitrack(telemetry,filter_flag);
        remap_optitrack = NaN;
    end
end

