% ----------------------------------------------------------------------
%% Save the MPC solution, telemetry and OptiTrack data to external files
% ----------------------------------------------------------------------

% ---------------------
%% Save the MPC results
% ---------------------
% Save the full MHT struct, containing also states and controls trajectories on 
% the entire MPC horizons
MHT_data                             = MHT;

% Vehicle states
MPC_solution.time_fullMPC            = time_fullMPC;
MPC_solution.u_fullMPC               = u_fullMPC;
MPC_solution.delta_fullMPC           = delta_fullMPC;
MPC_solution.Omega_fullMPC           = Omega_fullMPC;
MPC_solution.a_x_fullMPC             = a_x_fullMPC;
MPC_solution.a_y_fullMPC             = a_y_fullMPC;
MPC_solution.n_fullMPC               = n_fullMPC;
MPC_solution.xi_fullMPC              = xi_fullMPC;
% Interpolated data sent to the Raspberry
MPC_solution.time_interp_save        = time_interp_save;
MPC_solution.u_interp_save           = u_interp_save;
MPC_solution.delta_interp_save       = delta_interp_save;
% Vehicle pose
MPC_solution.vehicleAttitude_fullMPC = vehicleAttitude_fullMPC;
MPC_solution.xCoM_fullMPC            = xCoM_fullMPC;
MPC_solution.yCoM_fullMPC            = yCoM_fullMPC;
MPC_solution.xRightCar_fullMPC       = xRightCar_fullMPC;
MPC_solution.yRightCar_fullMPC       = yRightCar_fullMPC;
MPC_solution.xLeftCar_fullMPC        = xLeftCar_fullMPC;
MPC_solution.yLeftCar_fullMPC        = yLeftCar_fullMPC;
MPC_solution.yLeftCar_fullMPC        = yLeftCar_fullMPC;
% Convergence and timing
MPC_solution.numNotConvergOCP        = numNotConvergOCP;
if (numNotConvergOCP~=0)
    MPC_solution.notConverged_MPC_idx    = notConverged_MPC_idx;
else
    MPC_solution.notConverged_MPC_idx    = NaN;
end
MPC_solution.max_cpuTime             = max_cpuTime;
MPC_solution.idx_maxcpuTime          = idx_maxcpuTime;
MPC_solution.min_cpuTime             = min_cpuTime;
MPC_solution.idx_mincpuTime          = idx_mincpuTime;
MPC_solution.mean_cpuTime            = mean_cpuTime;
MPC_solution.cpu_time_list           = cpu_time_list;
MPC_solution.slow_MPC_idx            = slow_MPC_idx;
MPC_solution.cpu_time_list           = cpu_time_list;

% ---------------------
%% Save circuit data
% ---------------------
Circuit.x_RightMargin                = x_RightMargin;
Circuit.y_RightMargin                = y_RightMargin;
Circuit.x_LeftMargin                 = x_LeftMargin;
Circuit.y_LeftMargin                 = y_LeftMargin;

% ---------------------
%% Save telemetry and OptiTrack data
% ---------------------
if (exist('sensorsData','var'))
    Telem.sensorsData                = sensorsData;
    Telem.time_raspb_use             = time_raspb_use;
    Telem.u_raspb_use                = u_raspb_use;
    Telem.delta_raspb_use            = delta_raspb_use;
%     Telem.time_telem                 = time_telem;
%     Telem.speed_telem                = speed_telem;
%     Telem.Omega_telem                = Omega_telem;
%     Telem.Ay_ss_telem                = Ay_ss_telem;
end

% ---------------------
%% Collect all the data and save it
% ---------------------
savedData.MHT_data     = MHT_data;
savedData.MPC_solution = MPC_solution;
savedData.Circuit      = Circuit;
savedData.Telem        = Telem;

filePath = strcat(testName,num2str(i-1),'_rec');
save(filePath,'savedData');

