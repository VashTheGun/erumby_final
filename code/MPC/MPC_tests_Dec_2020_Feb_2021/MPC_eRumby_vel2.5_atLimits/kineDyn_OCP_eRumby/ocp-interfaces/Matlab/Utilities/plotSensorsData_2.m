% -------------------------------
%% Plot sensors data
% -------------------------------

% Start and final indices for the plots with telemetry data
idx_start_tel = 1;
idx_end_tel   = length(sensorsData.time_T)-1;

if (isstruct(remap_optitrack))
    % Start and final indices for the plots with optitrack data
    idx_start_optiT = 1;
    idx_end_optiT   = length(sensorsData.time_O)-1;
end

% ---------------------
%% Longitudinal dynamics
% ---------------------
figure('Name','long dyna','NumberTitle','off'), clf   
% --- u --- %
ax(1) = subplot(221);
hold on
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.target_speed_T(idx_start_tel:idx_end_tel),'.')
if (isstruct(remap_optitrack))
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.Speed(idx_start_optiT:idx_end_optiT),'.')
end
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Speed_gps_T(idx_start_tel:idx_end_tel),'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rr_T(idx_start_tel:idx_end_tel)*r_tire,'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rl_T(idx_start_tel:idx_end_tel)*r_tire,'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_fr_T(idx_start_tel:idx_end_tel)*r_tire,'.')
if (isstruct(remap_optitrack))
    legend('rasp ctrl','OptiT','OptiT telem','$\omega_{rr} r_w$','$\omega_{rl} r_w$','$\omega_{fr} r_w$','location','best')
else
    legend('rasp ctrl','OptiT telem','$\omega_{rr} r_w$','$\omega_{rl} r_w$','$\omega_{fr} r_w$','location','best')
end
grid on
title('$u$ [m/s]')
% --- traction command --- %
ax(2) = subplot(222);
hold on
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.traction_T(idx_start_tel:idx_end_tel),'.')
grid on
title('traction command [percent]')
xlim([sensorsData.time_T(idx_start_tel) sensorsData.time_T(idx_end_tel)])
% --- Ax --- %
ax(3) = subplot(223);
hold on
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Ax_T(idx_start_tel:idx_end_tel),'.')
grid on
title('$A_{x}$ [m/s$^2$]')
legend('IMU','location','best')
xlim([sensorsData.time_T(idx_start_tel) sensorsData.time_T(idx_end_tel)])
% --- omega_wheels --- %
ax(4) = subplot(224);
hold on
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rr_T(idx_start_tel:idx_end_tel),'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rl_T(idx_start_tel:idx_end_tel),'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_fr_T(idx_start_tel:idx_end_tel),'.')
grid on
title('wheel speed [rad/s]')
legend('$\omega_{rr}$','$\omega_{rl}$','$\omega_{fr}$','location','best')
xlim([sensorsData.time_T(idx_start_tel) sensorsData.time_T(idx_end_tel)])

% ---------------------
%% Lateral dynamics
% ---------------------
figure('Name','lat dyna','NumberTitle','off'), clf   
% --- delta --- %
ax(1) = subplot(221);
hold on
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),rad2deg(sensorsData.delta(idx_start_tel:idx_end_tel)),'.')
hold on
grid on
title('$\delta$ [deg]')
legend('rasp ctrl','location','best')
% --- Omega --- %
ax(2) = subplot(222);
hold on
if (isstruct(remap_optitrack))
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.Omega_O(idx_start_optiT:idx_end_optiT),'.')
end
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Omega_gps_T(idx_start_tel:idx_end_tel),'.')
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Omega_T(idx_start_tel:idx_end_tel),'.')
grid on
title('$\Omega$ [rad/s]')
if (isstruct(remap_optitrack))
    legend('OptiT','OptiT telem','IMU','location','best')
else
    legend('OptiT telem','IMU','location','best')
end
% --- Ay --- %
ax(3) = subplot(223);
hold on
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Ay_T(idx_start_tel:idx_end_tel),'.')
grid on
title('$A_{y}$ [m/s$^2$]')
legend('IMU','location','best')

% ---------------------
%% Vehicle path
% ---------------------
figure('Name','Vehicle Path','NumberTitle','off'), clf
hold on   
plot(x_RightMargin,y_RightMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');
plot(x_LeftMargin,y_LeftMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');    
if (isstruct(remap_optitrack))
    plot(sensorsData.x_O(idx_start_optiT:idx_end_optiT),sensorsData.y_O(idx_start_optiT:idx_end_optiT),'.')
end
plot(sensorsData.x_gps_T(idx_start_tel:idx_end_tel),sensorsData.y_gps_T(idx_start_tel:idx_end_tel),'.')
grid on
axis equal
xlabel('x [m]')
ylabel('y [m]')
if (isstruct(remap_optitrack))
    legend('OptiT','OptiT telem','location','best')
else
    legend('OptiT telem','location','best')
end
title('Vehicle CoM Path')

% ---------------------
%% Vehicle pose
% ---------------------
figure('Name','Pose','NumberTitle','off'), clf   
% --- x --- %
ax(1) = subplot(221);
hold on
if (isstruct(remap_optitrack))
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.x_O(idx_start_optiT:idx_end_optiT),'.')
end
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.x_gps_T(idx_start_tel:idx_end_tel),'.')
grid on
title('$x$ [m]')
if (isstruct(remap_optitrack))
    legend('OptiT','OptiT telem','location','best')
else
    legend('OptiT telem','location','best')
end
% --- y --- %
ax(2) = subplot(222);
hold on
if (isstruct(remap_optitrack))
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.y_O(idx_start_optiT:idx_end_optiT),'.')
end
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.y_gps_T(idx_start_tel:idx_end_tel),'.')
grid on
title('$y$ [m]')
if (isstruct(remap_optitrack))
    legend('OptiT','OptiT telem','location','best')
else
    legend('OptiT telem','location','best')
end
% --- psi --- %
ax(3) = subplot(223);
hold on
if (isstruct(remap_optitrack))
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),rad2deg(sensorsData.psi_O(idx_start_optiT:idx_end_optiT)),'.')
end
plot(sensorsData.time_T(idx_start_tel:idx_end_tel),rad2deg(sensorsData.psi_gps_T(idx_start_tel:idx_end_tel)),'.')
grid on
title('$\psi$ [deg]')
if (isstruct(remap_optitrack))
    legend('OptiT','OptiT telem','location','best')
else
    legend('OptiT telem','location','best')
end

