function y = posSign(x)

    % Regularized positive sign
    regFactor = 0.0001;
    y = (sin(atan(x/regFactor))+1)/2;

end