function y = negSign(x)

    % Regularized negative sign
    regFactor = 0.0001;
    y = (sin(atan(x/regFactor))-1)/2;

end