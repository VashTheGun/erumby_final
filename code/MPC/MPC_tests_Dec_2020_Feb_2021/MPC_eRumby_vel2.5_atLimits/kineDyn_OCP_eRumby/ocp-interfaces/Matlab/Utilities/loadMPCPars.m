function MPC_params = loadMPCPars()

    %% Parameters related to the MPC solver
    
    % Time after which the MPC solution is re-computed
    MPC_params.replan_time = 0.1;  % [s]
    
    % Sampling time of the low-level control
    MPC_params.lowLevel_sampling = 0.01;  % [s]
    
end

