function plotSensorsData(sensorsData,filteredData,enable_docked,test_id)

    % --------------------------------------------
    %% function purpose:  plot sensors data
    % --------------------------------------------
    
    % ---------------------
    %% Extract data
    % ---------------------
    time    = sensorsData.time;
    Speed_O = sensorsData.Speed;
    delta   = sensorsData.delta;
    Omega_T = sensorsData.Omega_T;
    Omega_O = sensorsData.Omega_O;
    Ay_T    = sensorsData.Ay_T;
    Ay_ss_O = sensorsData.Ay_ss_O;
    Ay_ss_T = sensorsData.Ay_ss_T;
    Ax_T    = sensorsData.Ax_T;
    x_O     = sensorsData.x_O;
    y_O     = sensorsData.y_O;
    psi_O   = sensorsData.psi_O;
    
    % Extract filtered data
    time_restrict         = filteredData.time;
    delta_restrict        = filteredData.delta;
    Omega_T_filt_restrict = filteredData.Omega_T;
    Speed_O_filt_restrict = filteredData.Speed_O;
    Ay_ss_filt_restrict   = filteredData.Ay_ss_T;

    % ---------------------
    %% States
    % ---------------------
    label_fig_mainQuantities = strcat({'Main quantities'},{' '},{test_id});
    if (~enable_docked)
        figure('Name',label_fig_mainQuantities{1},'NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[150,150,700,400])
    else
        figure('Name',label_fig_mainQuantities{1},'NumberTitle','off'), clf
    end    
    % --- u --- %
    ax(1) = subplot(231);
    hold on
    plot(time,Speed_O,'-r','LineWidth',2)
    plot(time_restrict,Speed_O_filt_restrict,'-','Color',color('midnight_blue'),'LineWidth',3)
    grid on
    title('$u$ [m/s]')
    legend('optitrack','$u_{filt,restrict}$','location','best')
    % --- delta --- %
    ax(2) = subplot(232);
    plot(time,rad2deg(delta),'--','LineWidth',2)
    hold on
    plot(time_restrict,rad2deg(delta_restrict),'-g','LineWidth',2)
    grid on
    title('$\delta$ [deg]')
    legend('$\delta$','$\delta_{restrict}$','location','best')
    % --- Omega --- %
    ax(3) = subplot(233);
    hold on
    plot(time,Omega_T,'LineWidth',2)
    plot(time,Omega_O,'LineWidth',2)
    plot(time_restrict,Omega_T_filt_restrict,'-','Color',color('midnight_blue'),'LineWidth',3)
    grid on
    title('$\Omega$ [rad/s]')
    legend('IMU','optitrack','$\Omega_{filt,restrict}$','location','best')
    % --- Ay_ss --- %
    ax(4) = subplot(234);
    plot(time,Ay_ss_O,'LineWidth',2)
    hold on
    plot(time,Ay_T,'LineWidth',2)
    plot(time,Ay_ss_T,'LineWidth',2)
    plot(time_restrict,Ay_ss_filt_restrict,'-','Color',color('midnight_blue'),'LineWidth',3)
    grid on
    title('$A_{y,ss}$ [m/s$^2$]')
    legend('optitrack','IMU','optitrack+IMU','$A_{y,ss,filt,restrict}$','location','best')
    % --- Ax --- %
    ax(5) = subplot(235);
    plot(time,Ax_T,'LineWidth',2)
    grid on
    title('$A_{x}$ [m/s$^2$]')
    legend('IMU','location','best')

    % linkaxes(ax,'x')
    clear ax

    % ---------------------
    %% Vehicle path
    % ---------------------
    label_fig_vehPath = strcat({'Vehicle path'},{' '},{test_id});
    if (~enable_docked)
        figure('Name',label_fig_vehPath{1},'NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[150,150,700,400])
    else
        figure('Name',label_fig_vehPath{1},'NumberTitle','off'), clf
    end    
    % --- path --- %
    ax(1) = subplot(221);
    plot(x_O,y_O,'-b','LineWidth',2)
    grid on
    axis equal
    xlabel('$x$ [m]')
    ylabel('$y$ [m]')
    title('Vehicle path')
    % --- x coord --- %
    ax(2) = subplot(222);
    hold on
    plot(time,x_O,'-b','LineWidth',2)
    grid on
    title('$x$ [m]')
    % --- y coord --- %
    ax(3) = subplot(223);
    hold on
    plot(time,y_O,'-b','LineWidth',2)
    grid on
    title('$y$ [m]')
    % --- psi --- %
    ax(4) = subplot(224);
    hold on
    psi_plot = rad2deg(psi_O);
    plot(time,psi_plot,'-b','LineWidth',2)
    grid on
    title('$\psi$ [deg]')


end
