function ICs_eRumby = loadInitialConditions_eRumby(n0,xi0,s0)
    
    % ----------------------------------------------
    %% Initial conditions for all the vehicle states
    % ----------------------------------------------
    

    %% ICs for all the vehicle states
    u0     = 0.001;  % [m/s] initial longitudinal speed
    ax0    = 0;    % [m/s^2] initial longitudinal acceleration
    Omega0 = 0;    % [rad/s] initial yaw rate
    delta0 = 0;    % [rad] initial steering angle
    % xi0 is the initial value of relative yaw angle 'xi' (in [rad])
    % n0 is the initial value of the road curvilinear coordinate 'n' (in [m])
    % s0 is the initial value of the road curvilinear abscissa (in [m])
    
    ICs_eRumby = [u0,ax0,Omega0,delta0,n0,xi0,s0];
       
end

