% Enter the test name to read the corresponding files
testName = test;

% ---------------------
%% Load vehicle data
% ---------------------
vehicle_data = getVehicleDataStruct();
m = vehicle_data.vehicle.m;            % [kg] Vehicle mass
L = vehicle_data.vehicle.L;            % [m] Vehicle wheelbase
delta_offs = vehicle_data.vehicle.delta_offs;  % [rad] offset steering angle to let the vehicle go straight

% ---------------------
%% Import telemetry & OptiTrack sensors data
% ---------------------
Import_Test_Data;

% ---------------------
%% Extract, filter, resize and plot sensors data
% ---------------------
%     --> subscript _T is used for telemetry data
%     --> subscript _O is used for optitrack data
getTestData;