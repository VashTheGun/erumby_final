function data_MPC = add_BoundaryCondits(data_MPC_in,bcs)
  
    data_MPC = data_MPC_in;

    % Change boundary conditions
    % initial BCs
    data_MPC.Parameters.v_xi       = bcs.initial.v__x;
    data_MPC.Parameters.a_xi       = bcs.initial.a__x;
    data_MPC.Parameters.Omega_i    = bcs.initial.Omega;
    data_MPC.Parameters.deltaD_i   = bcs.initial.delta__D;
    data_MPC.Parameters.n_i        = bcs.initial.n;
    data_MPC.Parameters.xi_i       = bcs.initial.xi;

    % Mayer term weights
    % initial
    data_MPC.Parameters.WBCI__vx    = 30;
    data_MPC.Parameters.WBCI__ax    = 1;   
    data_MPC.Parameters.WBCI__Omega = 10;
    data_MPC.Parameters.WBCI__delta = 50;
    data_MPC.Parameters.WBCI__n     = 100;  
    data_MPC.Parameters.WBCI__xi    = 40;      
 
end