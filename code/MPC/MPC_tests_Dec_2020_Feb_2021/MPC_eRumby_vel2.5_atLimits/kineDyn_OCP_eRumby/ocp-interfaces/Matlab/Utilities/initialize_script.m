% ------------------------
%% Initialize the script
% ------------------------

clc
clearvars 
% close all   
format long;

% set this to 1 to enable docked window style in plots
enable_docked = 1;
if (enable_docked)
    set(0,'DefaultFigureWindowStyle','docked');
else    
    set(0,'DefaultFigureWindowStyle','normal');
end
set(0,'defaultAxesFontSize',18)
set(0,'DefaultLegendFontSize',18)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

addpath(genpath('../../../../code/lib'))