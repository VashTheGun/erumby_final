function [states_out,lambda_out,controls_out,pose_out,cpu_time,flag_allMPCconverged,poseRoad_ini] = save_MPCsolution(ocp,solution_MPC,data_MPC,S,start_zeta,x_ini,y_ini,converged_MPC_flag,flag_allMPCconverged_in)

    % ---------------------------------------------------------
    %% Function purpose: save the MPC solution and display info
    % ---------------------------------------------------------

    cpu_time = solution_MPC.cpu_time;
    zeta = ocp.solution('zeta');
    [~,~,dir_r_interp,~] = S.evaluate(start_zeta+zeta); 
    vehicleAttitude = dir_r_interp' + ocp.solution('xi');   % vehicle attitude Psi
    
    zeta_out            = zeta;
    time_out            = ocp.solution('time');
    vx_out              = ocp.solution('v__x');
    ax_out              = ocp.solution('a__x');
    Omega_out           = ocp.solution('Omega');
    n_out               = ocp.solution('n');
    xi_out              = ocp.solution('xi');
    delta_D_out         = ocp.solution('delta__D');
    lambda1_out         = ocp.solution('lambda1__xo');
    lambda2_out         = ocp.solution('lambda2__xo');
    lambda3_out         = ocp.solution('lambda3__xo');
    lambda4_out         = ocp.solution('lambda4__xo');
    lambda5_out         = ocp.solution('lambda5__xo');
    lambda6_out         = ocp.solution('lambda6__xo');
    ax0_out             = ocp.solution('a__x0');
    delta_D0_out          = ocp.solution('delta__D0');
    xCoMCar_out         = ocp.solution('xCoMCar');
    yCoMCar_out         = ocp.solution('yCoMCar');
    xRightCar_out       = ocp.solution('xRightCar');
    yRightCar_out       = ocp.solution('yRightCar');
    xLeftCar_out        = ocp.solution('xLeftCar');
    yLeftCar_out        = ocp.solution('yLeftCar');
    vehicleAttitude_out = vehicleAttitude;
    xRightEdge_out      = ocp.solution('xRightEdge');
    yRightEdge_out      = ocp.solution('yRightEdge');
    xLeftEdge_out       = ocp.solution('xLeftEdge');
    yLeftEdge_out       = ocp.solution('yLeftEdge');

    states_out = [zeta_out,time_out,vx_out,ax_out,Omega_out,...
                  n_out,xi_out,delta_D_out];
    lambda_out = [lambda1_out,lambda2_out,lambda3_out,lambda4_out,...
                  lambda5_out,lambda6_out];          
    controls_out = [ax0_out,delta_D0_out];
    pose_out = [xCoMCar_out,yCoMCar_out,xRightCar_out,yRightCar_out,...
                xLeftCar_out,yLeftCar_out,vehicleAttitude_out,...
                xRightEdge_out,yRightEdge_out,xLeftEdge_out,yLeftEdge_out];

    poseRoad_ini = [x_ini,y_ini];
    
    if converged_MPC_flag
        fprintf('zeta_i = %.4f, zeta_f = %.4f, wT = %.2f - CONVERGED: [cpu time = %.2f ms, n�iter = %d]\n',...
            start_zeta+zeta(1),start_zeta+zeta(end),data_MPC.Parameters.wT,cpu_time,solution_MPC.nonlinear_system_solver.iterations);
        if (flag_allMPCconverged_in)
            fprintf('All MPC converged so far\n');
            flag_allMPCconverged = 1;
        else
            fprintf('At least one MPC did not converge so far\n');
            flag_allMPCconverged = 0;
        end    
    else
        fprintf('zeta_i = %.4f, zeta_f = %.4f, wT = %.2f - NOT CONVERGED: [cpu time = %.2f ms, n�iter = %d]\n',...
            start_zeta+zeta(1),start_zeta+zeta(end),data_MPC.Parameters.wT,cpu_time,solution_MPC.nonlinear_system_solver.iterations);
        flag_allMPCconverged = 0;
    end        

end

