% ----------------------------------------------------------------------
%% Offline plots of MPC results
% ----------------------------------------------------------------------

set(0,'DefaultFigureWindowStyle','docked');
set(0,'defaultAxesFontSize',20)
set(0,'DefaultLegendFontSize',20)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

addpath('../../../../../../lib');
addpath('./Utilities');
relPath_input_file = '../../../../MPC_test_data/';

% --------------------------
%% Load the data file and extract the information
% --------------------------
% vehicle data
vehicle_data = getVehicleDataStruct();
L  = vehicle_data.vehicle.L;
Lf = vehicle_data.vehicle.Lf;
Lr = vehicle_data.vehicle.Lr;
W  = vehicle_data.vehicle.W;
r_tire     = vehicle_data.vehicle.r_tire;
delta_offs = vehicle_data.vehicle.delta_offs;

% Load road margins
roadMargins = readtable('../../../Custom_Tracks/Pergine_Track/roadMargins_Pergine.txt');
x_RightMargin = roadMargins.x_RightMargin;
y_RightMargin = roadMargins.y_RightMargin;
x_LeftMargin  = roadMargins.x_LeftMargin;
y_LeftMargin  = roadMargins.y_LeftMargin;

testName = 'testMPC9';
telemetryData = strcat(relPath_input_file,testName,'.txt');
optitrackData = strcat(relPath_input_file,testName,'.csv');
[telemetry] = telemetry_import_V2(telemetryData);
% Low-pass filtering flag -> if filter_flag = 0 then no filtering is applied
filter_flag = 0;
if (exist(optitrackData,'file'))
    [optitrack] = optitrack_import(optitrackData);
    % Variable used to align (in post-proc) the optitrack data with the on-board vehicle sensors
    align_variable = 'none';  % 'speed'  'yaw_rate'  'none'
    % flag to be set to 1 in order to 'clean up' the gps optitrack data recorded on the raspberry 
    % (due to UDP delays and other factors, the sampling rate for these data is usually lower than 100 Hz, 
    % even if the 'pure' optitrack data have a sampling rate of around 120 Hz)
    clean_gps_telem = 0;  
    [remap_telemetry,remap_optitrack] = test_data_remap_V4(telemetry,optitrack,align_variable,filter_flag,clean_gps_telem);
else
    remap_telemetry = test_data_remap_V4_no_optitrack(telemetry,filter_flag);
    remap_optitrack = NaN;
end

sensorsData = extractSensorsData(remap_telemetry,remap_optitrack,vehicle_data);

plotSensorsData_2;
