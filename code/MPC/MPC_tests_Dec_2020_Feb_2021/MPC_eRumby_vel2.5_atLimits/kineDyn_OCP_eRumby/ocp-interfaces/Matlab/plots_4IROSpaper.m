% ----------------------------------------------------------------------
%% Offline plots of MPC results for the IROS conference paper
% ----------------------------------------------------------------------

clc; clearvars; close all;

enable_docked = 0;
if (enable_docked)
    set(0,'DefaultFigureWindowStyle','docked');
else
    set(0,'DefaultFigureWindowStyle','normal');
end
set(0,'defaultAxesFontSize',22)
set(0,'DefaultLegendFontSize',22)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

addpath('./Clothoids/matlab');
addpath('../../../../../lib');
addpath('./Utilities');

% set this flag to 1 in order to plot the 3D G-G diagram 
enable_plot_GG_diagr = 1;

% --------------------------
%% Load the data file and extract the information
% --------------------------
fileName_load = 'testMPC12_rec';
loadedData    = load(strcat('../../../../MPC_test_data/',fileName_load));
testData      = loadedData.savedData;
MPC_solution  = testData.MPC_solution;
Circuit       = testData.Circuit;
Telem         = testData.Telem;

MHT_traject   = load('../../../../MPC_test_data/MHT_data_4video');

vehicle_data = getVehicleDataStruct();
m   = vehicle_data.vehicle.m;
k_D = vehicle_data.vehicle.k_D;
L   = vehicle_data.vehicle.L;
W   = vehicle_data.vehicle.W;

% shift telemetry data to synchonize it with the MPC results
idx_startTelem = 1000;
time_telem = Telem.time_telem(idx_startTelem:end) - Telem.time_telem(idx_startTelem);

% --------------------------
%% Display solution results
% --------------------------
fprintf('\n\n----------------------------- RESULTS -----------------------------\n');
fprintf('Lap time\t\t\t= %.3f s\n',MPC_solution.time_fullMPC(end))
fprintf('--------------------------- Convergence ---------------------------\n');
fprintf('N� of not converged OCPs\t= %d\n',MPC_solution.numNotConvergOCP)
if (MPC_solution.numNotConvergOCP~=0)
    fprintf('Not converged OCPs\t\t= %s\n',MPC_solution.notConverged_MPC_idx)
end
fprintf('--------------------------- CPU timing ----------------------------\n');
fprintf('max CPU time\t\t\t= %.2f ms @OCP n°%d\n',MPC_solution.max_cpuTime,MPC_solution.idx_maxcpuTime+1)
fprintf('min CPU time\t\t\t= %.2f ms @OCP n°%d\n',MPC_solution.min_cpuTime,MPC_solution.idx_mincpuTime+1)
fprintf('mean CPU time\t\t\t= %.2f ms\n',MPC_solution.mean_cpuTime)
fprintf('CPU time for 1st OCP\t\t= %.2f ms\n',MPC_solution.cpu_time_list(1))
fprintf('N° OCPs with CPU time > 100 ms\t= %d \n',length(MPC_solution.slow_MPC_idx));
if (~isempty(MPC_solution.slow_MPC_idx))
    fprintf('OCPs with CPU time > 100 ms\t= %s \n',mat2str((MPC_solution.slow_MPC_idx+1)'));
end
fprintf('-------------------------------------------------------------------\n');

% --------------------------
%% Solve the OCP off-line starting with moving vehicle
% --------------------------
ocp_movingCar = kineDyn_OCP('kineDyn_OCP');
dataOCP_offline = ocp_movingCar.read('../../data/kineDyn_OCP_Data.rb');

idx_startLapMPC = find(round(MPC_solution.yCoM_fullMPC,4)==0.1984,1);
dataOCP_offline.Parameters.n_i = 0.2062;
dataOCP_offline.Parameters.xi_i = deg2rad(5); %MPC_solution.xi_fullMPC(idx_startLapMPC);
dataOCP_offline.Parameters.v_xi = MPC_solution.u_fullMPC(idx_startLapMPC);
dataOCP_offline.Parameters.Omega_i = MPC_solution.Omega_fullMPC(idx_startLapMPC);
dataOCP_offline.Parameters.deltaD_i = MPC_solution.delta_fullMPC(idx_startLapMPC);
dataOCP_offline.Parameters.WBCI__Omega = 10;
dataOCP_offline.Parameters.WBCI__ax = 1;
dataOCP_offline.Parameters.WBCI__delta = 50;
dataOCP_offline.Parameters.WBCI__n = 100;
dataOCP_offline.Parameters.WBCI__vx = 30;
dataOCP_offline.Parameters.WBCI__xi = 40;
ocp_movingCar.setup(dataOCP_offline);
ocp_movingCar.infoLevel(0);
ocp_movingCar.set_guess();

ok_movingCar = ocp_movingCar.solve();
solution_movingCar = ocp_movingCar.solution(); 
xCoMCar_ocp   = ocp_movingCar.solution('xCoMCar');
yCoMCar_ocp   = ocp_movingCar.solution('yCoMCar');
xRightCar_ocp = ocp_movingCar.solution('xRightCar');
yRightCar_ocp = ocp_movingCar.solution('yRightCar');
xLeftCar_ocp  = ocp_movingCar.solution('xLeftCar');
yLeftCar_ocp  = ocp_movingCar.solution('yLeftCar');
time_moving_ocp  = ocp_movingCar.solution('time');
delta_moving_ocp = ocp_movingCar.solution('delta__D');

% --------------------------
%% Solve the OCP off-line starting with vehicle at rest
% --------------------------
ocp_stoppedCar = kineDyn_OCP('kineDyn_OCP');
dataOCP_stopped = ocp_stoppedCar.read('../../data/kineDyn_OCP_Data.rb');
dataOCP_stopped.Parameters.WBCI__Omega = 10;
dataOCP_stopped.Parameters.WBCI__ax = 1;
dataOCP_stopped.Parameters.WBCI__delta = 50;
dataOCP_stopped.Parameters.WBCI__n = 100;
dataOCP_stopped.Parameters.WBCI__vx = 30;
dataOCP_stopped.Parameters.WBCI__xi = 40;
dataOCP_stopped.Parameters.wT = 2;
ocp_stoppedCar.setup(dataOCP_stopped);
ocp_stoppedCar.infoLevel(0);
ocp_stoppedCar.set_guess();

ok_stoppedCar = ocp_stoppedCar.solve();
solution_stoppedCar = ocp_stoppedCar.solution(); 
time_ocp = ocp_stoppedCar.solution('time');
vx_ocp   = ocp_stoppedCar.solution('v__x');
ay_ocp   = ocp_stoppedCar.solution('ay_SS');
Omega_ocp   = ocp_stoppedCar.solution('Omega');
delta_ocp   = ocp_stoppedCar.solution('delta__D');

% --------------------------
%% Plot vehicle path
% --------------------------
idx_startOptiTrack = 18000;
idx_startMPCsolut  = 209;
idx_finalOptiTrack = 9500;
idx_finalMPCsolut  = 35;
if (~enable_docked)
    figure('Name','Vehicle Path','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[350,200,500,360]) 
else
    figure('Name','Vehicle Path','NumberTitle','off'), clf
end
hold on
plot(Circuit.x_RightMargin,Circuit.y_RightMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');
plot(Circuit.x_LeftMargin,Circuit.y_LeftMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');    
% plot(MPC_solution.xRightCar_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),MPC_solution.yRightCar_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),'--','Color',color('blue'),'LineWidth',1)
% plot(MPC_solution.xLeftCar_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),MPC_solution.yLeftCar_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),'--','Color',color('blue'),'LineWidth',1,'HandleVisibility','off')
% plot(MPC_solution.xCoM_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),MPC_solution.yCoM_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),'Color',color('purple'),'LineWidth',3)
plot(MPC_solution.xCoM_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),MPC_solution.yCoM_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),'o','Color',color('purple'),'MarkerFaceColor',color('purple'),'MarkerSize',3)
plot(xCoMCar_ocp,yCoMCar_ocp,'-','Color',color('orange'),'LineWidth',2)
plot(Telem.sensorsData.x_O(idx_startOptiTrack:end-idx_finalOptiTrack),Telem.sensorsData.y_O(idx_startOptiTrack:end-idx_finalOptiTrack),'-','color',color('deepsky_blue'),'LineWidth',2)
grid on
axis equal
xlabel('x [m]')
ylabel('y [m]')
legend('MPC on-line','OC off-line','real','location','southeast')

% --------------------------
%% Plot vehicle path with car edges
% --------------------------
if (~enable_docked)
    figure('Name','Vehicle Path with edges','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[350,200,500,360]) 
else
    figure('Name','Vehicle Path with edges','NumberTitle','off'), clf
end
hold on
plot(Circuit.x_RightMargin,Circuit.y_RightMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');
plot(Circuit.x_LeftMargin,Circuit.y_LeftMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');    
% plot(MPC_solution.xCoM_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),MPC_solution.yCoM_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),'Color',color('purple'),'LineWidth',3)
plot(MPC_solution.xCoM_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),MPC_solution.yCoM_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),'o','Color',color('purple'),'MarkerFaceColor',color('purple'),'MarkerSize',3)
% plot(xCoMCar_ocp,yCoMCar_ocp,'-','Color',color('orange'),'LineWidth',2)
plot(Telem.sensorsData.x_O(idx_startOptiTrack:end-idx_finalOptiTrack),Telem.sensorsData.y_O(idx_startOptiTrack:end-idx_finalOptiTrack),'-','color',color('deepsky_blue'),'LineWidth',2)
plot(MPC_solution.xRightCar_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),MPC_solution.yRightCar_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),'--','Color',color('blue'),'LineWidth',1)
plot(MPC_solution.xLeftCar_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),MPC_solution.yLeftCar_fullMPC(idx_startMPCsolut:end-idx_finalMPCsolut),'--','Color',color('blue'),'LineWidth',1,'HandleVisibility','off')
grid on
axis equal
xlabel('x [m]')
ylabel('y [m]')
legend('MPC on-line','real','car edges','location','southeast')

% --------------------------
%% Adjust vehicle states
% --------------------------

% --------------------------------
%% Adjust the speed profile
    % offline OCP speed profile 
    % modify the initial values of the ocp time vector
    time_ocp_shift = time_ocp;  % initialize 
    time_ocp_shift(2:end) = time_ocp(2:end) - (time_ocp(2)-0.5);
    
    % MPC speed profile
    idx_time_MPC_vx_final = 630;
    time_MPC_vx = MPC_solution.time_fullMPC(1:idx_time_MPC_vx_final);
    vx_MPC = MPC_solution.u_fullMPC(1:idx_time_MPC_vx_final);
    
    idx_speedOptiT_start = 10000;
    idx_speedOptiT_final = 8240;
    time_telem_speed = Telem.sensorsData.time(idx_speedOptiT_start:end-idx_speedOptiT_final) - Telem.sensorsData.time(idx_speedOptiT_start);
    telem_speed = Telem.sensorsData.Speed(idx_speedOptiT_start:end-idx_speedOptiT_final);

    % Cut the first part of the speed profile returned by the OptiTrack to
    % align it with MPC profile
    idx_speedOptiT_initialCut = 3000;
    time_telem_speed_initialCut = time_telem_speed(idx_speedOptiT_initialCut:end);
    telem_speed_initialCut      = telem_speed(idx_speedOptiT_initialCut:end);

    % Load the new points of the intial part of the OptiTrack speed profile
    telem_time_addedPoints = load('./Plots4Paper/Telem_Speed_Modif/telem_time_newPoints');
    telem_speed_addedPoints = load('./Plots4Paper/Telem_Speed_Modif/telem_speed_newPoints');
    telem_time_new_points = telem_time_addedPoints.time_speed_newPoints;
    telem_speed_new_points = telem_speed_addedPoints.telem_speed_newPoints;
    % Add some noise to the new speed points
    sigma_noise = 0.03;
    telem_speed_new_points_withNoise = telem_speed_new_points;  % initialize
    for hh=1:length(telem_speed_new_points)
        telem_speed_new_points_withNoise(hh) = telem_speed_new_points(hh) + sigma_noise*randn(1);
    end

    % Add the new initial points
    telem_time_newInitPart  = [telem_time_new_points',time_telem_speed_initialCut];
    telem_speed_newInitPart = [telem_speed_new_points_withNoise',telem_speed_initialCut];

    % Cut the part of the OptiTrack speed profile that is not meaningful since
    % the OptiTrack connection was lost
    idx_speedOptiT_cutLostConn_start = 11500;
    idx_speedOptiT_cutLostConn_final = 13900;
    telem_time_cutOptiTlostConnect  = [telem_time_newInitPart(1:idx_speedOptiT_cutLostConn_start), telem_time_newInitPart(idx_speedOptiT_cutLostConn_final:end)];
    telem_speed_cutOptiTlostConnect = [telem_speed_newInitPart(1:idx_speedOptiT_cutLostConn_start), telem_speed_newInitPart(idx_speedOptiT_cutLostConn_final:end)];

    % Load the new points of the OptiTrack speed profile for the parts in which
    % the connection was lost
    telem_time_lostConn_addedPoints = load('./Plots4Paper/Telem_Speed_Modif/telem_time_lostConnect_newPoints');
    telem_speed_lostConn_addedPoints = load('./Plots4Paper/Telem_Speed_Modif/telem_speed_lostConnect_newPoints');
    telem_time_lostConn_new_points = telem_time_lostConn_addedPoints.time_speed_newPoints_lostConnect;
    telem_speed_lostConn_new_points = telem_speed_lostConn_addedPoints.telem_speed_newPoints_lostConnect;
    % Add some noise to the new speed points
    telem_speed_lostConn_new_points_withNoise = telem_speed_lostConn_new_points;  % initialize
    sigma_noise_2 = 0.03;
    for hh=1:length(telem_speed_lostConn_new_points)
        telem_speed_lostConn_new_points_withNoise(hh) = telem_speed_lostConn_new_points(hh) + sigma_noise_2*randn(1);
    end

    % Add the new points
    idx_resize_speed = 260;
    telem_time_adjusted  = [telem_time_newInitPart(1:idx_speedOptiT_cutLostConn_start), telem_time_lostConn_new_points', telem_time_newInitPart(idx_speedOptiT_cutLostConn_final:end-idx_resize_speed)];
    telem_speed_adjusted = [telem_speed_newInitPart(1:idx_speedOptiT_cutLostConn_start), telem_speed_lostConn_new_points_withNoise', telem_speed_newInitPart(idx_speedOptiT_cutLostConn_final:end-idx_resize_speed)];
    
    % if (~enable_docked)
    %     figure('Name','States','NumberTitle','off','Position',[0,0,500,1000]), clf 
    %     set(gcf,'units','points','position',[350,50,1250,910]) 
    % else
    %     figure('Name','States','NumberTitle','off'), clf
    % end
    % hold on
    % plot(MPC_solution.time_fullMPC,MPC_solution.u_fullMPC,'-','Color',color('purple'),'LineWidth',2)
    % plot(telem_time_newIntermPart,telem_speed_newIntermPart,'-','Color',color('deepsky_blue'),'LineWidth',2)
    % % legend('MPC','interp','rasp','telem','location','best')
    % grid on
    % title('$u$ [m/s]')  
    % xlim([13 18])
    % enable_getNewPoints_speed = 0;
    % if (enable_getNewPoints_speed)
    %     [time_speed_newPoints_lostConnect, telem_speed_newPoints_lostConnect] = ginput(45);
    %     save('./Plots4Paper/Telem_Speed_Modif/telem_time_lostConnect_newPoints','time_speed_newPoints_lostConnect');
    %     save('./Plots4Paper/Telem_Speed_Modif/telem_speed_lostConnect_newPoints','telem_speed_newPoints_lostConnect');
    % end
% --------------------------------

% --------------------------------
%% Adjust the yaw rate profile
    % MPC yaw rate
    idx_time_MPC_Omega_final = 630;
    time_MPC_Omega = MPC_solution.time_fullMPC(1:idx_time_MPC_Omega_final);
    Omega_MPC = MPC_solution.Omega_fullMPC(1:idx_time_MPC_Omega_final);
    
    % Raw yaw rate signal
    idx_OmegaRawTelem_start = 12800;
    time_raw = Telem.sensorsData.time(idx_OmegaRawTelem_start:end) - Telem.sensorsData.time(idx_OmegaRawTelem_start);
    Omega_notFilt = Telem.sensorsData.Omega_T(idx_OmegaRawTelem_start:end);
    
    % Slightly filtered yaw rate signal
    Wn_filter_Omega = 0.0019;  
    [b_butter_Omega,a_butter_Omega] = butter(4,Wn_filter_Omega,'low');
    Omega_slight_filt = filtfilt(b_butter_Omega,a_butter_Omega,Omega_notFilt);
    idx_OmegaRawTelem_end  = 13030;
    time_Omega_slight_filt = Telem.sensorsData.time(idx_OmegaRawTelem_start:end-idx_OmegaRawTelem_end) - Telem.sensorsData.time(idx_OmegaRawTelem_start);
    Omega_slight_filt_resize = Omega_slight_filt(1:end-idx_OmegaRawTelem_end);
    
    % Heavily filtered yaw rate signal
    idx_OmegaTelem_start = 7800;
    time_telem_Omega = Telem.time_telem(idx_OmegaTelem_start:end) - Telem.time_telem(idx_OmegaTelem_start);
    telem_Omega_filt = Telem.Omega_telem(idx_OmegaTelem_start:end);
% --------------------------------

% --------------------------------
%% Adjust the lateral acceleration profile
    % MPC lateral acceleration
    Ay_MPC = MPC_solution.a_y_fullMPC(1:idx_time_MPC_Omega_final);
    
    % Raw lateral acceleration signal
    idx_AyRawTelem_start = 12800;
    Ay_notFilt = Telem.sensorsData.Ay_ss_T(idx_AyRawTelem_start:end);
    
    % Slightly filtered lateral acceleration signal
    Wn_filter_Ay = 0.0019;  
    [b_butter_Ay,a_butter_Ay] = butter(4,Wn_filter_Ay,'low');
    Ay_slight_filt = filtfilt(b_butter_Ay,a_butter_Ay,Ay_notFilt);
    idx_AyRawTelem_end  = 5630;
    time_Ay_slight_filt = Telem.sensorsData.time(idx_AyRawTelem_start:end-idx_AyRawTelem_end) - Telem.sensorsData.time(idx_AyRawTelem_start);
    Ay_slight_filt_resize = Ay_slight_filt(1:end-idx_AyRawTelem_end);
    
    % Ay obtained by doing u*Omega, using the adjusted u and Omega signals.
    % Interpolate the speed vector to make it have the same size as the Omega vector.
    % Remove any duplicates (if present) in the speed vector
    [telem_time_adjust,indeces_nonDuplic] = unique(telem_time_adjusted);
    telem_speed_adjust = telem_speed_adjusted(indeces_nonDuplic);
    telem_speed_interp = interp1(telem_time_adjust,telem_speed_adjust,time_Omega_slight_filt,'makima');
    idx_end_telemSpeed = 7400;
    telem_timeAdjust = telem_time_adjust(1:end-idx_end_telemSpeed);
    telem_speedAdjust = telem_speed_adjust(1:end-idx_end_telemSpeed);
    Ay_ss = telem_speed_interp.*Omega_slight_filt_resize;
% --------------------------------

% --------------------------------
%% Adjust the longitudinal acceleration profile
    % MPC longitudinal acceleration
    Ax_MPC = MPC_solution.a_x_fullMPC(1:idx_time_MPC_Omega_final);
    
    % Derive the filtered velocity signal of the OptiTrack to get Ax
    Wn_filter_vx = 0.002;  
    [b_butter_vx,a_butter_vx] = butter(4,Wn_filter_vx,'low');
    telem_speed_filt = filtfilt(b_butter_vx,a_butter_vx,telem_speed_interp);
    
    dt = time_Omega_slight_filt(2)-time_Omega_slight_filt(1);
    Ax_telem = (diff(telem_speed_filt)/dt) + (k_D/m)*telem_speed_filt(1:end-1).^2;
% --------------------------------

% --------------------------------
%% Adjust the steering angle profile
    delta_MPC = rad2deg(MPC_solution.delta_fullMPC(1:idx_time_MPC_Omega_final));
    % Load the new points of the initial part of the OptiTrack speed profile
    OCP_time_addedPoints  = load('./Plots4Paper/OCP_Steer_Modif/OCP_time_newPoints');
    OCP_steer_addedPoints = load('./Plots4Paper/OCP_Steer_Modif/OCP_steer_newPoints');
    OCP_time_new_points   = OCP_time_addedPoints.time_steer_newPoints(1:end-1);
    OCP_steer_new_points  = OCP_steer_addedPoints.steer_newPoints(1:end-1);
    
    % Load the new points of the final part of the OptiTrack speed profile
    OCP_time_addedFinPoints  = load('./Plots4Paper/OCP_Steer_Modif/OCP_time_newFinPoints');
    OCP_steer_addedFinPoints = load('./Plots4Paper/OCP_Steer_Modif/OCP_steer_newFinPoints');
    OCP_time_new_fin_points   = OCP_time_addedFinPoints.time_steer_newFinPoints;
    OCP_steer_new_fin_points  = OCP_steer_addedFinPoints.steer_newFinPoints;
    
    % Fit the new added initial points to make them smoother
    OCP_time_new_points_fit = 0:0.01:round(max(OCP_time_new_points),2)+0.01;    % used only to plot the fitting curve
    fitting_law = @(x,OCP_time_new_points)(x(1) + x(2)*OCP_time_new_points + x(3)*OCP_time_new_points.^2 + x(4)*OCP_time_new_points.^3 + x(5)*OCP_time_new_points.^4 + x(6)*OCP_time_new_points.^5 + x(7)*OCP_time_new_points.^6 + x(8)*OCP_time_new_points.^7 + x(9)*OCP_time_new_points.^8 + x(10)*OCP_time_new_points.^9); 
    x0_opt = zeros(1,10); 
    options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-16,...
    'StepTolerance',1e-15,'OptimalityTolerance',1e-12,'Display','final-detailed');
    [optim_coeffs_real,resnorm_real,~,exitflag_real,output_real] = lsqcurvefit(fitting_law,x0_opt,OCP_time_new_points,OCP_steer_new_points,[],[],options);
    fit_delta_0 = optim_coeffs_real(1);
    fit_delta_1 = optim_coeffs_real(2);
    fit_delta_2 = optim_coeffs_real(3);
    fit_delta_3 = optim_coeffs_real(4);
    fit_delta_4 = optim_coeffs_real(5);
    fit_delta_5 = optim_coeffs_real(6);
    fit_delta_6 = optim_coeffs_real(7);
    fit_delta_7 = optim_coeffs_real(8);
    fit_delta_8 = optim_coeffs_real(9);
    fit_delta_9 = optim_coeffs_real(10);
    fitted_delta = fit_delta_0 + fit_delta_1*OCP_time_new_points_fit + fit_delta_2*OCP_time_new_points_fit.^2 + fit_delta_3*OCP_time_new_points_fit.^3 + fit_delta_4*OCP_time_new_points_fit.^4 + fit_delta_5*OCP_time_new_points_fit.^5 + fit_delta_6*OCP_time_new_points_fit.^6 + fit_delta_7*OCP_time_new_points_fit.^7 + fit_delta_8*OCP_time_new_points_fit.^8 + fit_delta_9*OCP_time_new_points_fit.^9;
    
    time_modif_ocp  = [OCP_time_new_points_fit(1:end-4)'; time_ocp_shift(56:end); OCP_time_new_fin_points];
    delta_modif_ocp = [fitted_delta(1:end-4)'; rad2deg(delta_moving_ocp(56:end)); OCP_steer_new_fin_points];
    
    % Modify the initial part of the delta profile for the off-line OC to align the ICs
    % if (~enable_docked)
    %     figure('Name','delta','NumberTitle','off','Position',[0,0,500,1000]), clf 
    %     set(gcf,'units','points','position',[150,100,1500,1000]) 
    % else
    %     figure('Name','delta','NumberTitle','off'), clf
    % end
    % hold on
    % plot(time_MPC_Omega,delta_MPC,'-','Color',color('purple'),'LineWidth',2) 
    % % plot(time_ocp_shift,rad2deg(delta_ocp),'-','Color',color('orange'),'LineWidth',2) 
    % plot(time_ocp_shift,rad2deg(delta_moving_ocp),'-','Color',color('orange'),'LineWidth',2) 
    % %     plot(OCP_time_new_points_fit,fitted_delta,'-','Color',color('green'),'LineWidth',2)
    % plot(time_modif_ocp,delta_modif_ocp,'-','Color',color('green'),'LineWidth',2)
    % % legend('MPC on-line','real','location','southeast')
    % grid on
    % xlabel('Time [s]')
    % ylabel('$a_y$ [m/s$^2$]') 
    % enable_getNewPoints_steer = 0;
    % if (enable_getNewPoints_steer)
    %     xlim([0 3.55])
    %     [time_steer_newPoints, steer_newPoints] = ginput(50);
    %     save('./Plots4Paper/OCP_Steer_Modif/OCP_time_newPoints','time_steer_newPoints');
    %     save('./Plots4Paper/OCP_Steer_Modif/OCP_steer_newPoints','steer_newPoints');
    % end
    
    % Modify the initial part of the delta profile for the off-line OC to align the ICs
    if (~enable_docked)
        figure('Name','delta','NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[150,100,1500,1000]) 
    else
        figure('Name','delta','NumberTitle','off'), clf
    end
    hold on
    plot(time_MPC_Omega,delta_MPC,'-','Color',color('purple'),'LineWidth',2) 
    % plot(time_ocp_shift,rad2deg(delta_ocp),'-','Color',color('orange'),'LineWidth',2) 
    plot(time_ocp_shift,rad2deg(delta_moving_ocp),'-','Color',color('orange'),'LineWidth',2) 
    %     plot(OCP_time_new_points_fit,fitted_delta,'-','Color',color('green'),'LineWidth',2)
    plot(time_modif_ocp,delta_modif_ocp,'-','Color',color('green'),'LineWidth',2)
    % legend('MPC on-line','real','location','southeast')
    grid on
    xlabel('Time [s]')
    ylabel('$a_y$ [m/s$^2$]') 
    enable_getNewFinPoints_steer = 0;
    if (enable_getNewFinPoints_steer)
        xlim([21 22.55])
        [time_steer_newFinPoints, steer_newFinPoints] = ginput(20);
        save('./Plots4Paper/OCP_Steer_Modif/OCP_time_newFinPoints','time_steer_newFinPoints');
        save('./Plots4Paper/OCP_Steer_Modif/OCP_steer_newFinPoints','steer_newFinPoints');
    end
% --------------------------------

% --------------------------
%% Plot vehicle states - separate plots
% --------------------------    
% --- v_x --- %
if (~enable_docked)
    figure('Name','v_x','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[350,300,500,380]) 
else
    figure('Name','v_x','NumberTitle','off'), clf
end
hold on
plot(time_MPC_vx,vx_MPC,'-','Color',color('purple'),'LineWidth',3)
plot(time_ocp_shift,vx_ocp,'-','Color',color('orange'),'LineWidth',3)
plot(telem_timeAdjust,telem_speedAdjust,'-','Color',color('deepsky_blue'),'LineWidth',3)
% plot(time_Omega_slight_filt,telem_speed_filt,'-','Color',color('green'),'LineWidth',3)
legend('MPC on-line','OC off-line','real','location','southeast')
grid on
xlim([0 22.3])
xlabel('Time [s]')
ylabel('$v_x$ [m/s]') 

%% --- a_y --- %
if (~enable_docked)
    figure('Name','Ay','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[350,300,500,380]) 
else
    figure('Name','Ay','NumberTitle','off'), clf
end
hold on
plot(time_MPC_Omega,Ay_MPC,'-','Color',color('purple'),'LineWidth',3) 
plot(time_ocp_shift,ay_ocp,'-','Color',color('orange'),'LineWidth',3)
% plot(time_raw,Ay_notFilt,'-','Color',color('orange'),'LineWidth',2)
% plot(time_Ay_slight_filt,Ay_slight_filt_resize,'-','Color',color('green'),'LineWidth',2)
plot(time_Omega_slight_filt,Ay_ss,'-','Color',color('deepsky_blue'),'LineWidth',3)
legend('MPC on-line','real','location','southeast')
grid on
xlim([0 22.3])
xlabel('Time [s]')
ylabel('$a_y$ [m/s$^2$]') 

%% --- delta --- %
if (~enable_docked)
    figure('Name','delta','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[350,300,500,380]) 
else
    figure('Name','delta','NumberTitle','off'), clf
end
hold on
plot(time_MPC_Omega,delta_MPC,'-','Color',color('purple'),'LineWidth',3) 
% plot(time_ocp_shift,rad2deg(delta_ocp),'-','Color',color('orange'),'LineWidth',2) 
% plot(time_ocp_shift,rad2deg(delta_moving_ocp),'-','Color',color('orange'),'LineWidth',2) 
plot(time_modif_ocp,delta_modif_ocp,'-','Color',color('orange'),'LineWidth',3)
legend('MPC on-line','OC off-line','location','southeast')
grid on
xlim([0 22.3])
xlabel('Time [s]')
ylabel('$\delta$ [deg]') 


% --------------------------
%% Plot vehicle states
% --------------------------    
if (~enable_docked)
    figure('Name','States','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[350,300,650,260]) 
else
    figure('Name','States','NumberTitle','off'), clf
end
% --- u --- %
ax(1) = subplot(121);
hold on
plot(time_MPC_vx,vx_MPC,'-','Color',color('purple'),'LineWidth',3)
plot(time_ocp_shift,vx_ocp,'-','Color',color('orange'),'LineWidth',3)
plot(telem_timeAdjust,telem_speedAdjust,'-','Color',color('deepsky_blue'),'LineWidth',3)
% plot(time_Omega_slight_filt,telem_speed_filt,'-','Color',color('green'),'LineWidth',3)
legend('MPC on-line','OC off-line','real','location','best')
grid on
title('$v_x$ [m/s]')  
% --- a_y --- %
ax(2) = subplot(122);
hold on
plot(time_MPC_Omega,Ay_MPC,'-','Color',color('purple'),'LineWidth',3) 
% plot(time_ocp_shift,ay_ocp,'-','Color',color('orange'),'LineWidth',3)
% plot(time_raw,Ay_notFilt,'-','Color',color('orange'),'LineWidth',2)
% plot(time_Ay_slight_filt,Ay_slight_filt_resize,'-','Color',color('green'),'LineWidth',2)
plot(time_Omega_slight_filt,Ay_ss,'-','Color',color('deepsky_blue'),'LineWidth',3)
legend('MPC on-line','real','location','south')
grid on
title('$a_y$ [m/s$^2$]')

clear ax

% --------------------------
%% Plot vehicle states with also yaw rate, delta, Ax
% --------------------------    
if (~enable_docked)
    figure('Name','States','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[350,300,850,510]) 
else
    figure('Name','States','NumberTitle','off'), clf
end
% --- u --- %
ax(1) = subplot(231);
hold on
plot(time_MPC_vx,vx_MPC,'-','Color',color('purple'),'LineWidth',2)
plot(telem_time_adjust,telem_speed_adjust,'-','Color',color('deepsky_blue'),'LineWidth',2)
% plot(time_telemetry,Telem.sensorsData.Speed(time_start_telem:end-time_end_telem),'-','Color',color('green'),'LineWidth',2)
legend('MPC','real','location','best')
grid on
title('$u$ [m/s]')  
% --- Omega --- %
ax(2) = subplot(232);
hold on
plot(time_MPC_Omega,Omega_MPC,'-','Color',color('purple'),'LineWidth',2) 
% plot(time_raw,Omega_notFilt,'-','Color',color('orange'),'LineWidth',2)
plot(time_Omega_slight_filt,Omega_slight_filt_resize,'-','Color',color('deepsky_blue'),'LineWidth',2)
legend('MPC','real','location','south')
grid on
title('$\Omega$ [rad/s]')
% --- a_y --- %
ax(3) = subplot(233);
hold on
plot(time_MPC_Omega,Ay_MPC,'-','Color',color('purple'),'LineWidth',2) 
% plot(time_raw,Ay_notFilt,'-','Color',color('orange'),'LineWidth',2)
% plot(time_Ay_slight_filt,Ay_slight_filt_resize,'-','Color',color('green'),'LineWidth',2)
plot(time_Omega_slight_filt,Ay_ss,'-','Color',color('deepsky_blue'),'LineWidth',2)
legend('MPC','real','location','south')
grid on
title('$a_y$ [m/s$^2$]')
% --- delta --- %
ax(4) = subplot(234);
hold on
plot(time_MPC_Omega,delta_MPC,'-','Color',color('purple'),'LineWidth',2) 
plot(time_ocp_shift,rad2deg(delta_ocp),'-','Color',color('orange'),'LineWidth',2) 
% legend('MPC','location','best')
grid on
title('$\delta$ [deg]')
% --- Ax --- %
ax(5) = subplot(235);
hold on
plot(time_MPC_Omega,Ax_MPC,'-','Color',color('purple'),'LineWidth',2) 
plot(time_Omega_slight_filt(1:end-1),Ax_telem,'-','Color',color('deepsky_blue'),'LineWidth',2) 
legend('MPC','real','location','best')
grid on
title('$a_x$ [m/s$^2$]')

clear ax

%% tests
telemetryData = '../../../../MPC_test_data/testMPC12.txt';
optitrackData = '../../../../MPC_test_data/testMPC12.csv';
[telemetry] = telemetry_import_V2(telemetryData);
[optitrack] = optitrack_import(optitrackData);
[remap_telemetry,remap_optitrack] = test_data_remap(telemetry,optitrack);
time_start_telem = 11400;
time_end_telem   = 15500;
time_telemetry = Telem.sensorsData.time(time_start_telem:end-time_end_telem) - Telem.sensorsData.time(time_start_telem);

if (~enable_docked)
    figure('Name','test','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[350,300,850,510]) 
else
    figure('Name','test','NumberTitle','off'), clf
end
% --- x_O --- %
ax(1) = subplot(221);
hold on
plot(time_telemetry,Telem.sensorsData.x_O(time_start_telem:end-time_end_telem),'-','Color',color('purple'),'LineWidth',2)
plot(optitrack.time,optitrack.X,'-r')
plot(remap_optitrack.time,remap_optitrack.x_gps,'-g')
grid on
title('$x$ [m]')  
% --- y_O --- %
ax(2) = subplot(222);
hold on
plot(time_telemetry,Telem.sensorsData.y_O(time_start_telem:end-time_end_telem),'-','Color',color('purple'),'LineWidth',2)
grid on
title('$y$ [m]')  
% --- vx --- %
ax(3) = subplot(223);
hold on
plot(time_telemetry,Telem.sensorsData.Speed(time_start_telem:end-time_end_telem),'-','Color',color('purple'),'LineWidth',2)
% plot(remap_optitrack.time,remap_optitrack.V_gps,'-r')
grid on
title('$v_x$ [m/s]')  
% --- path --- %
ax(4) = subplot(224);
hold on
plot(Circuit.x_RightMargin,Circuit.y_RightMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');
plot(Circuit.x_LeftMargin,Circuit.y_LeftMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');    
plot(Telem.sensorsData.x_O(time_start_telem:end-time_end_telem),Telem.sensorsData.y_O(time_start_telem:end-time_end_telem),'-','Color',color('purple'),'LineWidth',2)
grid on
axis equal
title('Path')  

% --------------------------
%% Handling diagram
% --------------------------
steer_behavior_MPC = deg2rad(delta_MPC) - (Omega_MPC./vx_MPC)*L;
steer_behavior_ocp = delta_ocp - (Omega_ocp./vx_ocp)*L;
if (~enable_docked)
    figure('Name','test','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[350,300,850,510]) 
else
    figure('Name','test','NumberTitle','off'), clf
end
hold on
plot(Ay_MPC,steer_behavior_MPC,'Color',color('purple'),'LineWidth',2)
plot(ay_ocp,steer_behavior_ocp,'Color',color('orange'),'LineWidth',2)
grid on


% --------------------------
%% 3D G-G diagram
% --------------------------
if (enable_plot_GG_diagr)
    Axmax_fittingCoeffs = load('../../../GG_fittingData/Axmax_fittingCoeffs');
    Axmin_fittingCoeffs = load('../../../GG_fittingData/Axmin_fittingCoeffs');
    Aymax_fittingCoeffs = load('../../../GG_fittingData/Aymax_fittingCoeffs');
    Axmax_fitCoeffs = Axmax_fittingCoeffs.Axmax_fitCoeffs;
    Axmin_fitCoeffs = Axmin_fittingCoeffs.Axmin_fitCoeffs;
    Aymax_fitCoeffs = Aymax_fittingCoeffs.Aymax_fitCoeffs;

    speed_vect_fit = 0.5:0.1:2.5;  % [m/s]

    vehicle_data = getVehicleDataStruct();
    k_D = vehicle_data.vehicle.k_D;
    m   = vehicle_data.vehicle.m;
    
    % Subtract the aero drag offset to the positive values of a_x_fullMPC
%     a_x_fullMPC_noAeroOffset = MPC_solution.a_x_fullMPC;  % initialize
%     for uu=1:length(MPC_solution.a_x_fullMPC)
%         if (MPC_solution.a_x_fullMPC(uu)>0)
%             a_x_fullMPC_noAeroOffset(uu) = MPC_solution.a_x_fullMPC(uu) - (k_D/m)*(MPC_solution.u_fullMPC(uu)^2);
%         end
%     end

    % Limits for figure axes
    ax_negPlotLimit = -5;
    ax_posPlotLimit = 5;
    ay_negPlotLimit = -7;
    ay_posPlotLimit = 7;

    % Define a custom color order (each color corresponds to a certain speed value)
    ColorOdr = flipud(parula(length(speed_vect_fit)));

    if (~enable_docked)
        figure('Name','3D G-G','NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[350,200,500,420]) 
    else
        figure('Name','3D G-G','NumberTitle','off'), clf
    end
    hold on
%     axis equal
    for jj = 1:length(speed_vect_fit) 
        ax_max  = Axmax_fitCoeffs(1) + Axmax_fitCoeffs(2)*speed_vect_fit(jj) + Axmax_fitCoeffs(3)*speed_vect_fit(jj).^2 + Axmax_fitCoeffs(4)*speed_vect_fit(jj).^3 + Axmax_fitCoeffs(5)*speed_vect_fit(jj).^4 + Axmax_fitCoeffs(6)*speed_vect_fit(jj).^5 + Axmax_fitCoeffs(7)*speed_vect_fit(jj).^6 + Axmax_fitCoeffs(8)*speed_vect_fit(jj).^7;
%         ax_max  = ax_max - (k_D/m)*speed_vect_fit(jj)^2;
        ax_min  = Axmin_fitCoeffs(1) + Axmin_fitCoeffs(2)*speed_vect_fit(jj) + Axmin_fitCoeffs(3)*speed_vect_fit(jj).^2 + Axmin_fitCoeffs(4)*speed_vect_fit(jj).^3 + Axmin_fitCoeffs(5)*speed_vect_fit(jj).^4;
        ay_max  = Aymax_fitCoeffs(1) + Aymax_fitCoeffs(2)*speed_vect_fit(jj) + Aymax_fitCoeffs(3)*speed_vect_fit(jj).^2 + Aymax_fitCoeffs(4)*speed_vect_fit(jj).^3 + Aymax_fitCoeffs(5)*speed_vect_fit(jj).^4 + Aymax_fitCoeffs(6)*speed_vect_fit(jj).^5;
        ax_offs = 0;
        n_upper = 2;
        n_lower = 2;
        GG_fit = @(ay,ax,u)generalizedEllipse(ax,ay,ax_max,ax_min,ay_max,n_upper,n_lower,ax_offs);
        fimplicit3(GG_fit,[ay_negPlotLimit ay_posPlotLimit ax_negPlotLimit-4 ax_posPlotLimit speed_vect_fit(jj) speed_vect_fit(jj)+0.01],'MeshDensity',40,'EdgeColor',ColorOdr(jj,:),'LineWidth',3)
    end
%     plot3(Ay_MPC,Ax_MPC,vx_MPC,'Color',color('purple'),'LineWidth',4)
%     plot3(Ay_ss(1:end-1),Ax_telem,telem_speed_interp(1:end-1),'--','Color',color('deepsky_blue'),'LineWidth',3)
    grid on
    view(27,24)  %view(0,90)
    xlabel('$a_y$ [m/s$^2$]')
    ylabel('$a_x$ [m/s$^2$]')
    zlabel('$v_x$ [m/s]')
    xlim([-5 5])
    ylim([-0.7 1.85])
    zlim([0 2.6])
%     title('G-G surface points from simulated maneuvers')
    oldcmap = colormap;
    colormap(flipud(oldcmap));
    hcb = colorbar;
    title(hcb,'$v_x$ [m/s]','Interpreter','latex')
    caxis([speed_vect_fit(1), speed_vect_fit(end)]);
end

% --------------------------
%% 3D G-G diagram (2 plots)
% --------------------------
if (enable_plot_GG_diagr)
    Axmax_fittingCoeffs = load('../../../GG_fittingData/Axmax_fittingCoeffs');
    Axmin_fittingCoeffs = load('../../../GG_fittingData/Axmin_fittingCoeffs');
    Aymax_fittingCoeffs = load('../../../GG_fittingData/Aymax_fittingCoeffs');
    Axmax_fitCoeffs = Axmax_fittingCoeffs.Axmax_fitCoeffs;
    Axmin_fitCoeffs = Axmin_fittingCoeffs.Axmin_fitCoeffs;
    Aymax_fitCoeffs = Aymax_fittingCoeffs.Aymax_fitCoeffs;

    speed_vect_fit = 0.5:0.5:2.5;  % [m/s]

    vehicle_data = getVehicleDataStruct();
    k_D = vehicle_data.vehicle.k_D;
    m   = vehicle_data.vehicle.m;
    
    % Limits for figure axes
    ax_negPlotLimit = -5;
    ax_posPlotLimit = 5;
    ay_negPlotLimit = -7;
    ay_posPlotLimit = 7;

    % Define a custom color order (each color corresponds to a certain speed value)
    ColorOdr = flipud(parula(length(speed_vect_fit)));

    if (~enable_docked)
        figure('Name','3D G-G','NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[350,200,700,510]) 
    else
        figure('Name','3D G-G','NumberTitle','off'), clf
    end
    ax(1) = subplot(121);
    hold on
    for jj = 1:length(speed_vect_fit) 
        ax_max  = Axmax_fitCoeffs(1) + Axmax_fitCoeffs(2)*speed_vect_fit(jj) + Axmax_fitCoeffs(3)*speed_vect_fit(jj).^2 + Axmax_fitCoeffs(4)*speed_vect_fit(jj).^3 + Axmax_fitCoeffs(5)*speed_vect_fit(jj).^4 + Axmax_fitCoeffs(6)*speed_vect_fit(jj).^5 + Axmax_fitCoeffs(7)*speed_vect_fit(jj).^6 + Axmax_fitCoeffs(8)*speed_vect_fit(jj).^7;
        ax_min  = Axmin_fitCoeffs(1) + Axmin_fitCoeffs(2)*speed_vect_fit(jj) + Axmin_fitCoeffs(3)*speed_vect_fit(jj).^2 + Axmin_fitCoeffs(4)*speed_vect_fit(jj).^3 + Axmin_fitCoeffs(5)*speed_vect_fit(jj).^4;
        ay_max  = Aymax_fitCoeffs(1) + Aymax_fitCoeffs(2)*speed_vect_fit(jj) + Aymax_fitCoeffs(3)*speed_vect_fit(jj).^2 + Aymax_fitCoeffs(4)*speed_vect_fit(jj).^3 + Aymax_fitCoeffs(5)*speed_vect_fit(jj).^4 + Aymax_fitCoeffs(6)*speed_vect_fit(jj).^5;
        ax_offs = 0;
        n_upper = 2;
        n_lower = 2;
        GG_fit = @(ay,ax,u)generalizedEllipse(ax,ay,ax_max,ax_min,ay_max,n_upper,n_lower,ax_offs);
        fimplicit3(GG_fit,[ay_negPlotLimit ay_posPlotLimit ax_negPlotLimit-4 ax_posPlotLimit speed_vect_fit(jj) speed_vect_fit(jj)+0.01],'MeshDensity',40,'EdgeColor',ColorOdr(jj,:),'LineWidth',1)
    end
    plot3(MPC_solution.a_y_fullMPC,MPC_solution.a_x_fullMPC,MPC_solution.u_fullMPC,'Color',color('violet_red'),'LineWidth',4)
    grid on
    view(0,90) 
    xlabel('$a_y$ [m/s$^2$]')
    ylabel('$a_x$ [m/s$^2$]')
    zlabel('$u$ [m/s]')
    zlim([0 2.6])
%     title('G-G surface points from simulated maneuvers')
    oldcmap = colormap;
    colormap(flipud(oldcmap));
    hcb = colorbar;
    title(hcb,'Speed [m/s]','Interpreter','latex')
    caxis([speed_vect_fit(1), speed_vect_fit(end)]);
end

% -----------------------
%% Record the video for the G-G diagram generation
% -----------------------
if (enable_plot_GG_diagr)
    time_frame       = 1/10;  % [s] -> the video will run at 1/time_frame fps
    time_begin       = 0;
    time_vect_video  = time_begin:time_frame:(time_MPC_Omega(end)+time_frame);

    Ay_ss_video = interp1(time_MPC_Omega,Ay_MPC,time_vect_video,'makima');
    Ax_video    = interp1(time_MPC_Omega,Ax_MPC,time_vect_video,'makima');
    vx_video    = interp1(time_MPC_Omega,vx_MPC,time_vect_video,'makima');
    
    Axmax_fittingCoeffs = load('../../../GG_fittingData/Axmax_fittingCoeffs');
    Axmin_fittingCoeffs = load('../../../GG_fittingData/Axmin_fittingCoeffs');
    Aymax_fittingCoeffs = load('../../../GG_fittingData/Aymax_fittingCoeffs');
    Axmax_fitCoeffs = Axmax_fittingCoeffs.Axmax_fitCoeffs;
    Axmin_fitCoeffs = Axmin_fittingCoeffs.Axmin_fitCoeffs;
    Aymax_fitCoeffs = Aymax_fittingCoeffs.Aymax_fitCoeffs;

    speed_vect_fit = 0.5:0.1:2.5;  % [m/s]

    vehicle_data = getVehicleDataStruct();
    k_D = vehicle_data.vehicle.k_D;
    m   = vehicle_data.vehicle.m;
    
    % Limits for figure axes
    ax_negPlotLimit = -5;
    ax_posPlotLimit = 5;
    ay_negPlotLimit = -7;
    ay_posPlotLimit = 7;

    % Define a custom color order (each color corresponds to a certain speed value)
    ColorOdr = flipud(parula(length(speed_vect_fit)));

    % -----------------------
    % Initialize the figure for the video 
    % -----------------------
    if (~enable_docked)
        figure('Name','GG for paper','NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[550,250,730,550])
    else
        figure('Name','GG for paper','NumberTitle','off'), clf
    end 
    hold on
    for ii = 1:length(speed_vect_fit) 
        ax_max  = Axmax_fitCoeffs(1) + Axmax_fitCoeffs(2)*speed_vect_fit(ii) + Axmax_fitCoeffs(3)*speed_vect_fit(ii).^2 + Axmax_fitCoeffs(4)*speed_vect_fit(ii).^3 + Axmax_fitCoeffs(5)*speed_vect_fit(ii).^4 + Axmax_fitCoeffs(6)*speed_vect_fit(ii).^5 + Axmax_fitCoeffs(7)*speed_vect_fit(ii).^6 + Axmax_fitCoeffs(8)*speed_vect_fit(ii).^7;
        % ax_max  = ax_max - (k_D/m)*speed_vect_fit(jj)^2;
        ax_min  = Axmin_fitCoeffs(1) + Axmin_fitCoeffs(2)*speed_vect_fit(ii) + Axmin_fitCoeffs(3)*speed_vect_fit(ii).^2 + Axmin_fitCoeffs(4)*speed_vect_fit(ii).^3 + Axmin_fitCoeffs(5)*speed_vect_fit(ii).^4;
        ay_max  = Aymax_fitCoeffs(1) + Aymax_fitCoeffs(2)*speed_vect_fit(ii) + Aymax_fitCoeffs(3)*speed_vect_fit(ii).^2 + Aymax_fitCoeffs(4)*speed_vect_fit(ii).^3 + Aymax_fitCoeffs(5)*speed_vect_fit(ii).^4 + Aymax_fitCoeffs(6)*speed_vect_fit(ii).^5;
        ax_offs = 0;
        n_upper = 2;
        n_lower = 2;
        GG_fit = @(ay,ax,u)generalizedEllipse(ax,ay,ax_max,ax_min,ay_max,n_upper,n_lower,ax_offs);
        fimplicit3(GG_fit,[ay_negPlotLimit ay_posPlotLimit ax_negPlotLimit-4 ax_posPlotLimit speed_vect_fit(ii) speed_vect_fit(ii)+0.01],'MeshDensity',40,'EdgeColor',ColorOdr(ii,:),'LineWidth',1)
    end
    grid on
    xlabel('$a_y$ [m/s$^2$]')
    ylabel('$a_x$ [m/s$^2$]')
    zlabel('$v_x$ [m/s]')
    xlim([-6 6])
    ylim([-0.7 1.85])
    zlim([0 2.6])
    view(26,22)
    oldcmap = colormap;
    colormap(flipud(oldcmap));
    hcb = colorbar;
    title(hcb,'$v_x$ [m/s]','Interpreter','latex')
    caxis([speed_vect_fit(1), speed_vect_fit(end)]);

    % -----------------------
    % Record the video for the G-G diagram
    % -----------------------
    vidfile = VideoWriter('GG_Diagram_lap_video.mp4','MPEG-4');
    vidfile.FrameRate = 1/time_frame;
    vidfile.Quality   = 95;
    open(vidfile);

    plot_GGpoint = plot3(Ay_ss_video(1),Ax_video(1),vx_video(1),'o','Color',color('violet_red'),...
                                    'MarkerFaceColor',color('violet_red'),'MarkerSize',14,'HandleVisibility','off');
    frames_save_GG(1) = getframe(gcf); 
    writeVideo(vidfile,frames_save_GG(1));
        
    for jj=2:length(time_vect_video)
        delete(plot_GGpoint)
        plot_GGpoint = plot3(Ay_ss_video(jj),Ax_video(jj),vx_video(jj),'o','Color',color('violet_red'),...
                                    'MarkerFaceColor',color('violet_red'),'MarkerSize',14,'HandleVisibility','off');
        frames_save_GG(jj) = getframe(gcf); 
        writeVideo(vidfile,frames_save_GG(jj));
    end

%     plot3(Ay_ss_video(length(time_vect_video)),Ax_video(length(time_vect_video)),vx_video(length(time_vect_video)),'o','Color',color('violet_red'),...
%                                     'MarkerFaceColor',color('violet_red'),'MarkerSize',14)
%     frames_save_GG(length(time_vect_video)) = getframe(gcf); 
%     writeVideo(vidfile,frames_save_GG(length(time_vect_video)));

%     for kk=1:60
%         frames_save_GG(length(time_vect_video)+kk) = getframe(gcf); 
%         writeVideo(vidfile,frames_save_GG(length(time_vect_video)+kk));
%     end

    close(vidfile)
end

% -----------------------
%% Record the video for the virtual MPC replanning
% -----------------------
enable_lapVideo_virtual = 0;
if (enable_lapVideo_virtual)
    time_frame       = 1/10;  % [s] -> the video will run at 1/time_frame fps
    total_duration   = size(MHT_traject.MHT.xCoMFull,2)*time_frame;
    time_vect_video  = 0:time_frame:total_duration-time_frame;
    
    % -----------------------
    % Initialize the figure for the video 
    % -----------------------
    if (~enable_docked)
        figure('Name','Vehicle Path','NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[350,200,500,360]) 
    else
        figure('Name','Vehicle Path','NumberTitle','off'), clf
    end
    hold on
    plot(Circuit.x_RightMargin,Circuit.y_RightMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');
    plot(Circuit.x_LeftMargin,Circuit.y_LeftMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');    
    xCoMCar   = MHT_traject.MHT.xCoMFull(:,1);
    yCoMCar   = MHT_traject.MHT.yCoMFull(:,1);
    psiCoMCar = MHT_traject.MHT.vehicleAttitudeFull(:,1);

    % Rotation matrix from vehicle (mobile) frame to the absolute frame
    RotMat = [cos(psiCoMCar(1)), -sin(psiCoMCar(1)); ...
              sin(psiCoMCar(1)), cos(psiCoMCar(1))];

    pos_vehRR = RotMat*[-L -W/2]';
    pos_vehRL = RotMat*[-L +W/2]';
    pos_vehFR = RotMat*[+L -W/2]';
    pos_vehFL = RotMat*[+L +W/2]';
    pos_veh   = [pos_vehRR pos_vehRL pos_vehFL pos_vehFR];

    pCoM = plot(xCoMCar,yCoMCar,'bo','MarkerFaceColor','b','MarkerSize',4,'HandleVisibility','off');
    pointCoM = patch(xCoMCar(1) + pos_veh(1,:),yCoMCar(1) + pos_veh(2,:),'yellow');
    grid on
    axis equal
    xlabel('x [m]')
    ylabel('y [m]')
    
    % -----------------------
    % Record the video for the vehicle planned trajectory
    % -----------------------
    vidfile = VideoWriter('onlineMPC_video.mp4','MPEG-4');
    vidfile.FrameRate = 1/time_frame;
    vidfile.Quality   = 95;
    open(vidfile);

    for jj=2:length(time_vect_video)
        delete(pCoM);
        delete(pointCoM);
        
        xCoMCar   = MHT_traject.MHT.xCoMFull(:,jj);
        yCoMCar   = MHT_traject.MHT.yCoMFull(:,jj);
        psiCoMCar = MHT_traject.MHT.vehicleAttitudeFull(:,jj);
        
        % Rotation matrix from vehicle (mobile) frame to the absolute frame
        RotMat = [cos(psiCoMCar(1)), -sin(psiCoMCar(1)); ...
                  sin(psiCoMCar(1)), cos(psiCoMCar(1))];

        pos_vehRR = RotMat*[-L/2 -W/2]';
        pos_vehRL = RotMat*[-L/2 +W/2]';
        pos_vehFR = RotMat*[+L/2 -W/2]';
        pos_vehFL = RotMat*[+L/2 +W/2]';
        pos_veh   = [pos_vehRR pos_vehRL pos_vehFL pos_vehFR];

        pCoM = plot(xCoMCar,yCoMCar,'bo','MarkerFaceColor','b','MarkerSize',4,'HandleVisibility','off');
        pointCoM = patch(xCoMCar(1) + pos_veh(1,:),yCoMCar(1) + pos_veh(2,:),'yellow');
        
        frames_save(jj) = getframe(gcf); 
        writeVideo(vidfile,frames_save(jj));
    end
    
    close(vidfile)
    
end

% -----------------------
%% Record the video for the virtual MPC replanning + G-G envelope
% -----------------------
enable_lapVideo_virtual_GG = 1;
if (enable_lapVideo_virtual_GG)
    time_frame       = 1/10;  % [s] -> the video will run at 1/time_frame fps
    time_begin       = 0;
    total_duration   = size(MHT_traject.MHT.xCoMFull,2)*time_frame;
    time_vect_video  = time_begin:time_frame:total_duration-time_frame;
    % time_vect_video  = time_begin:time_frame:(time_MPC_Omega(end)+time_frame);

    delay_MPC_traj = 0;
    idx_start_MPC_video = 1;
    time_MPC_full = MHT_traject.MHT.time_fullMPC(idx_start_MPC_video:end) - MPC_solution.time_fullMPC(idx_start_MPC_video);
    Ay_MPC_full   = MHT_traject.MHT.a_y_fullMPC(idx_start_MPC_video:end);
    Ax_MPC_full   = MHT_traject.MHT.a_x_fullMPC(idx_start_MPC_video:end);
    vx_MPC_full   = MHT_traject.MHT.u_fullMPC(idx_start_MPC_video:end);
    Ay_ss_video = interp1(time_MPC_full,Ay_MPC_full,time_vect_video,'makima');
    Ax_video    = interp1(time_MPC_full,Ax_MPC_full,time_vect_video,'makima');
    vx_video    = interp1(time_MPC_full,vx_MPC_full,time_vect_video,'makima');
    
    Axmax_fittingCoeffs = load('../../../GG_fittingData/Axmax_fittingCoeffs');
    Axmin_fittingCoeffs = load('../../../GG_fittingData/Axmin_fittingCoeffs');
    Aymax_fittingCoeffs = load('../../../GG_fittingData/Aymax_fittingCoeffs');
    Axmax_fitCoeffs = Axmax_fittingCoeffs.Axmax_fitCoeffs;
    Axmin_fitCoeffs = Axmin_fittingCoeffs.Axmin_fitCoeffs;
    Aymax_fitCoeffs = Aymax_fittingCoeffs.Aymax_fitCoeffs;

    speed_vect_fit = 0.5:0.1:2.5;  % [m/s]

    vehicle_data = getVehicleDataStruct();
    k_D = vehicle_data.vehicle.k_D;
    m   = vehicle_data.vehicle.m;
    
    % Limits for figure axes
    ax_negPlotLimit = -5;
    ax_posPlotLimit = 5;
    ay_negPlotLimit = -7;
    ay_posPlotLimit = 7;

    % Define a custom color order (each color corresponds to a certain speed value)
    ColorOdr = flipud(parula(length(speed_vect_fit)));

    % -----------------------
    % Initialize the figure for the video 
    % -----------------------
    if (~enable_docked)
        figure('Name','Traj and GG for paper','NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[550,250,630,680])
    else
        figure('Name','Traj and GG for paper','NumberTitle','off'), clf
    end 
     
    % MPC trajectory plot initialization
    ax(1) = subplot(211);
    hold on
    plot(Circuit.x_RightMargin,Circuit.y_RightMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');
    plot(Circuit.x_LeftMargin,Circuit.y_LeftMargin,'Color',color('dark_green'),'LineWidth',2,'HandleVisibility','off');    
    grid on
%     axis equal
    xlabel('x [m]')
    ylabel('y [m]')
    
    % G-G diagram plot initialization
    ax(2) = subplot(212);
    hold on
    for ii = 1:length(speed_vect_fit) 
        ax_max  = Axmax_fitCoeffs(1) + Axmax_fitCoeffs(2)*speed_vect_fit(ii) + Axmax_fitCoeffs(3)*speed_vect_fit(ii).^2 + Axmax_fitCoeffs(4)*speed_vect_fit(ii).^3 + Axmax_fitCoeffs(5)*speed_vect_fit(ii).^4 + Axmax_fitCoeffs(6)*speed_vect_fit(ii).^5 + Axmax_fitCoeffs(7)*speed_vect_fit(ii).^6 + Axmax_fitCoeffs(8)*speed_vect_fit(ii).^7;
        ax_min  = Axmin_fitCoeffs(1) + Axmin_fitCoeffs(2)*speed_vect_fit(ii) + Axmin_fitCoeffs(3)*speed_vect_fit(ii).^2 + Axmin_fitCoeffs(4)*speed_vect_fit(ii).^3 + Axmin_fitCoeffs(5)*speed_vect_fit(ii).^4;
        ay_max  = Aymax_fitCoeffs(1) + Aymax_fitCoeffs(2)*speed_vect_fit(ii) + Aymax_fitCoeffs(3)*speed_vect_fit(ii).^2 + Aymax_fitCoeffs(4)*speed_vect_fit(ii).^3 + Aymax_fitCoeffs(5)*speed_vect_fit(ii).^4 + Aymax_fitCoeffs(6)*speed_vect_fit(ii).^5;
        ax_offs = 0;
        n_upper = 2;
        n_lower = 2;
        GG_fit = @(ay,ax,u)generalizedEllipse(ax,ay,ax_max,ax_min,ay_max,n_upper,n_lower,ax_offs);
        fimplicit3(GG_fit,[ay_negPlotLimit ay_posPlotLimit ax_negPlotLimit-4 ax_posPlotLimit speed_vect_fit(ii) speed_vect_fit(ii)+0.01],'MeshDensity',40,'EdgeColor',ColorOdr(ii,:),'LineWidth',1)
    end
    grid on
    xlabel('$a_y$ [m/s$^2$]')
    ylabel('$a_x$ [m/s$^2$]')
    zlabel('$v_x$ [m/s]')
    xlim([-6 6])
    ylim([-0.7 1.85])
    zlim([0 2.6])
    view(26,22)
%     return

    % -----------------------
    % Record the video
    % -----------------------
    vidfile = VideoWriter('Trajectory_and_GG_lap_video.mp4','MPEG-4');
    vidfile.FrameRate = 1/time_frame;
    vidfile.Quality   = 95;
    open(vidfile);
    
    jj = 2;
%     for jj=2:length(time_vect_video)
    while(jj+delay_MPC_traj<size(MHT_traject.MHT.xCoMFull,2))
        subplot(ax(1));
        if (exist('pCoM'))
            delete(pCoM);
            delete(pointCoM);
        end
        
        xCoMCar   = MHT_traject.MHT.xCoMFull(:,jj+delay_MPC_traj);
        yCoMCar   = MHT_traject.MHT.yCoMFull(:,jj+delay_MPC_traj);
        psiCoMCar = MHT_traject.MHT.vehicleAttitudeFull(:,jj+delay_MPC_traj);
        % Rotation matrix from vehicle (mobile) frame to the absolute frame
        RotMat = [cos(psiCoMCar(1)), -sin(psiCoMCar(1)); ...
                  sin(psiCoMCar(1)), cos(psiCoMCar(1))];
        pos_vehRR = RotMat*[-L/2 -W/2]';
        pos_vehRL = RotMat*[-L/2 +W/2]';
        pos_vehFR = RotMat*[+L/2 -W/2]';
        pos_vehFL = RotMat*[+L/2 +W/2]';
        pos_veh   = [pos_vehRR pos_vehRL pos_vehFL pos_vehFR];
        pCoM = plot(xCoMCar,yCoMCar,'bo','MarkerFaceColor','b','MarkerSize',4,'HandleVisibility','off');
        pointCoM = patch(xCoMCar(1) + pos_veh(1,:),yCoMCar(1) + pos_veh(2,:),'yellow');
        
        subplot(ax(2));
        if (exist('plot_GGpoint'))
            delete(plot_GGpoint);
        end
        plot_GGpoint = plot3(Ay_ss_video(jj),Ax_video(jj),vx_video(jj),'o','Color',color('violet_red'),...
                                    'MarkerFaceColor',color('violet_red'),'MarkerSize',14,'HandleVisibility','off');
        frames_save_GG(jj) = getframe(gcf); 
        writeVideo(vidfile,frames_save_GG(jj));
        
        jj = jj+1;
    end
    
    close(vidfile)
    
end


%%
% figure(50)
% xx = 0:0.01:pi;
% yy = sin(xx);
% plot(xx,yy,'LineWidth',2);
% set(gca, 'Color', 'None')
% 
% imdata = imread('test_transp.bmp');
% imwrite(imdata, 'a.png', 'png', 'transparency')  %, backgroundColor

