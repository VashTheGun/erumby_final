
% trajectory convertion
set(0,'DefaultFigureWindowStyle','docked')
% x and y are the index 2 e 3 of the cell "data"

%% Load calibration
calibration_path = 'Calibration_data/CAM_2/';

load(strcat(calibration_path, 'camera_matrix.mat'));
load('centerl_line_Pergine_world_points.mat')

% player |number|dimension|vaues|
img    = imread(strcat(calibration_path, 'VID_20200302_173046.png')); % screenshot of the field



%%
x = [0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0];
y = [0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0];

worldPoints = [x_cl;y_cl];

imagePoints     = homography2d_project(inv(camera_matrix),worldPoints);

%%
figure(1);
image_plot = imshow(img);
hold on
scatter(imagePoints(:,1),imagePoints(:,2),20,'y','filled');
% delete(image_plot);
% set(gca, 'color', 'none')



