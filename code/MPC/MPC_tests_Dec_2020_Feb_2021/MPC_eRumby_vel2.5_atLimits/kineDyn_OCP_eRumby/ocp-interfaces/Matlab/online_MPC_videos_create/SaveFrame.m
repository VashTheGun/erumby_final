clear all;close all;clc;

set(0,'DefaultFigureWindowStyle','docked');
%% Load video
v = VideoReader('/Volumes/GoogleDrive/My Drive/Erumby_videos_Session_2020_02_03/VID_20200302_173046.mp4');

%% Select frame and plot it
N_frame = 5;

frame = read(v,N_frame);

figure
image(frame);

%% Save frame
imwrite(frame,'./Calibration_data/CAM_2/VID_20200302_173046.png');