%% track creation
clear all;close all;clc;

addpath(genpath('./../../../../lib/matlab/Clothoids'));
% c++ convention
M_PI = pi;

%  % track parameters
%  x = [4, 0, 0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4];
%  y = [6, 6, 0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6];
%  theta = [M_PI, M_PI, 0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI];

% track parameters
x = [0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0];
y = [0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0];
theta = [0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI, M_PI, 0];

S = ClothoidList() ;

for i = 1:length(x)-1
    S.push_back_G1(x(i), y(i), theta(i), x(i+1), y(i+1), theta(i+1) ) ; % track creation
end




%%
abscissa = 0:0.01:S.length;
for i = 1:length(abscissa)
    [x_cl(i),y_cl(i),~,curvature(i)] = S.evaluate(abscissa(i));
end
width_L = 0.4*ones(size(curvature));
width_R = 0.4*ones(size(curvature));

%% Calibration points
x_cal = [0, 2, 4, 6, 7, 7, 3, 0, -1.99, 0, 3, 7, 7, 0];
y_cal = [0, 1, 1, 0, 0, 2, 2, 1.274,3, 5, 4, 4, 6, 6];




fig = figure;
S.plot
hold on
for i = 1:length(x_cal)
    scatter(x_cal(i), y_cal(i),80,'y','filled','MarkerEdgeColor','k')
    text(x_cal(i),y_cal(i),num2str(i),'FontSize',20)
end

axis equal

save('calibration_set_world_points.mat','x_cal','y_cal')
save('centerl_line_Pergine_world_points.mat','x_cl','y_cl')
saveas(fig,'Pergine_Calibration_World_Points.png');