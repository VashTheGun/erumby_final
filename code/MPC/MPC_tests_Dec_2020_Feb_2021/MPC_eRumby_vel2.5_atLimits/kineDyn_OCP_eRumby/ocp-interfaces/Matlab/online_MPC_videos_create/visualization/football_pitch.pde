void draw_football_pitch() {
  stroke(255); // pitch borders
  strokeWeight(2);
  fill(pitch);
  rect(offX, offY, W, H);
  
  stroke(255); // middle center (cerchio di centrocampo)
  strokeWeight(2);
  fill(pitch);
  ellipse(offX + W/2, offY + H/2, W*0.18, W*0.18);
  
  stroke(255); // middle line (linea di centrocampo)
  strokeWeight(2);
  line(offX + W/2, offY, offX + W/2, offY + H);
  
  stroke(255); // sx area circle (cerchio area sx)
  strokeWeight(2);
  fill(pitch);
  ellipse(offX + 0.105*W, offY + H/2, W*0.18, W*0.18);
  
  stroke(255); // dx area circle (cerchio area dx)
  strokeWeight(2);
  fill(pitch);
  ellipse(offX + W - 0.105*W, offY + H/2, W*0.18, W*0.18);
    
  stroke(255); // sx penalty area (area rigore sx)
  strokeWeight(2);
  fill(pitch);
  rect(offX, offY + (1/6.0)*H, 0.15*W, (2/3.0)*H);
  
  stroke(255); // dx penalty area (area rigore dx)
  strokeWeight(2);
  fill(pitch);
  rect(offX+W, offY + (1/6.0)*H, -0.15*W, (2/3.0)*H);
  
  stroke(255); // sx goal area (area di porta sx)
  strokeWeight(2);
  fill(pitch);
  rect(offX, offY + (3/8.0)*H, 0.052*W, (1/4.0)*H);
  
  stroke(255); // dx goal area (area di porta dx)
  strokeWeight(2);
  fill(pitch);
  rect(offX+W, offY + (3/8.0)*H, -0.052*W, (1/4.0)*H);
}