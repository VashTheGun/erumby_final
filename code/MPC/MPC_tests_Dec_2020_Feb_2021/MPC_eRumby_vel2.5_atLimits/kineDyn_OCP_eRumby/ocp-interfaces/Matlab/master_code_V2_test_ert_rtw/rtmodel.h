/*
 * rtmodel.h
 *
 * Code generation for Simulink model "master_code_V2_test".
 *
 * Simulink Coder version                : 9.4 (R2020b) 29-Jul-2020
 * C source code generated on : Fri Feb 19 17:12:14 2021
 *
 * Note that the generated code is not dependent on this header file.
 * The file is used in cojuction with the automatic build procedure.
 * It is included by the sample main executable harness
 * MATLAB/rtw/c/src/common/rt_main.c.
 *
 */

#ifndef RTW_HEADER_rtmodel_h_
#define RTW_HEADER_rtmodel_h_
#include "master_code_V2_test.h"
#endif                                 /* RTW_HEADER_rtmodel_h_ */
