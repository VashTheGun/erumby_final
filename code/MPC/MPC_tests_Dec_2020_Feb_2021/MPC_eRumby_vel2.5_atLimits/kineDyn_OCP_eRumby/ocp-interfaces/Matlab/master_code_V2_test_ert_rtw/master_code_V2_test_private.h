/*
 * master_code_V2_test_private.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "master_code_V2_test".
 *
 * Model version              : 1.269
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C source code generated on : Fri Feb 19 17:12:14 2021
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_master_code_V2_test_private_h_
#define RTW_HEADER_master_code_V2_test_private_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#include "master_code_V2_test.h"

/* Private macros used by the generated code to access rtModel */
#ifndef rtmSetTFinal
#define rtmSetTFinal(rtm, val)         ((rtm)->Timing.tFinal = (val))
#endif

#ifndef UCHAR_MAX
#include <limits.h>
#endif

#if ( UCHAR_MAX != (0xFFU) ) || ( SCHAR_MAX != (0x7F) )
#error Code was generated for compiler with different sized uchar/char. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( USHRT_MAX != (0xFFFFU) ) || ( SHRT_MAX != (0x7FFF) )
#error Code was generated for compiler with different sized ushort/short. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( UINT_MAX != (0xFFFFFFFFU) ) || ( INT_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized uint/int. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( ULONG_MAX != (0xFFFFFFFFU) ) || ( LONG_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized ulong/long. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

int_T rt_WriteMat4FileHeader(FILE *fp,
  int32_T m,
  int32_T n,
  const char_T *name);
extern void master_code_V2_t_i2cWr_mpu_Init(DW_i2cWr_mpu_master_code_V2_t_T
  *localDW);
extern void master_code_V2_test_i2cWr_mpu(uint16_T rtu_dataw,
  DW_i2cWr_mpu_master_code_V2_t_T *localDW);
extern void master_code_V2_test_i2cRd_Init(B_i2cRd_master_code_V2_test_T *localB,
  DW_i2cRd_master_code_V2_test_T *localDW, P_i2cRd_master_code_V2_test_T *localP);
extern void master_code_V2_test_i2cRd(B_i2cRd_master_code_V2_test_T *localB,
  DW_i2cRd_master_code_V2_test_T *localDW, P_i2cRd_master_code_V2_test_T *localP);
extern void master_code_V2_test_gpsRd_Init(RT_MODEL_master_code_V2_test_T *
  const master_code_V2_test_M, B_gpsRd_master_code_V2_test_T *localB,
  DW_gpsRd_master_code_V2_test_T *localDW, P_gpsRd_master_code_V2_test_T *localP);
extern void master_code_V2_test_gpsRd(RT_MODEL_master_code_V2_test_T * const
  master_code_V2_test_M, B_gpsRd_master_code_V2_test_T *localB,
  DW_gpsRd_master_code_V2_test_T *localDW);
extern void master_code_V2_t_cmd_motor_Init(DW_cmd_motor_master_code_V2_t_T
  *localDW);
extern void master_code_V2_test_cmd_motor(uint16_T rtu_esc, uint16_T rtu_servo,
  DW_cmd_motor_master_code_V2_t_T *localDW);
extern void master_code_V2_t_i2cWr_mpu_Term(DW_i2cWr_mpu_master_code_V2_t_T
  *localDW);
extern void master_code_V2_test_i2cRd_Term(DW_i2cRd_master_code_V2_test_T
  *localDW);
extern void master_code_V2_test_gpsRd_Term(RT_MODEL_master_code_V2_test_T *
  const master_code_V2_test_M, DW_gpsRd_master_code_V2_test_T *localDW);
extern void master_code_V2_t_cmd_motor_Term(DW_cmd_motor_master_code_V2_t_T
  *localDW);

#endif                           /* RTW_HEADER_master_code_V2_test_private_h_ */
