/*
 * master_code_V2_test_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "master_code_V2_test".
 *
 * Model version              : 1.269
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C source code generated on : Fri Feb 19 17:12:14 2021
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_master_code_V2_test_types_h_
#define RTW_HEADER_master_code_V2_test_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"

/* Model Code Variants */

/* Custom Type definition for MATLABSystem: '<S7>/I2C Master Write' */
#include "MW_SVD.h"
#include "MW_I2C.h"
#ifndef struct_tag_R1xdlxXgEuZ2YKjrgC1tPF
#define struct_tag_R1xdlxXgEuZ2YKjrgC1tPF

struct tag_R1xdlxXgEuZ2YKjrgC1tPF
{
  int32_T __dummy;
};

#endif                                 /*struct_tag_R1xdlxXgEuZ2YKjrgC1tPF*/

#ifndef typedef_e_codertarget_raspi_internal__T
#define typedef_e_codertarget_raspi_internal__T

typedef struct tag_R1xdlxXgEuZ2YKjrgC1tPF e_codertarget_raspi_internal__T;

#endif                               /*typedef_e_codertarget_raspi_internal__T*/

#ifndef struct_tag_o4ztkK1PLas0qr9ZkPJ5nH
#define struct_tag_o4ztkK1PLas0qr9ZkPJ5nH

struct tag_o4ztkK1PLas0qr9ZkPJ5nH
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  e_codertarget_raspi_internal__T Hw;
  uint32_T BusSpeed;
  MW_Handle_Type MW_I2C_HANDLE;
};

#endif                                 /*struct_tag_o4ztkK1PLas0qr9ZkPJ5nH*/

#ifndef typedef_codertarget_raspi_internal_I2_T
#define typedef_codertarget_raspi_internal_I2_T

typedef struct tag_o4ztkK1PLas0qr9ZkPJ5nH codertarget_raspi_internal_I2_T;

#endif                               /*typedef_codertarget_raspi_internal_I2_T*/

#ifndef struct_tag_J2gcp8J1tqFs8RI37cGF8F
#define struct_tag_J2gcp8J1tqFs8RI37cGF8F

struct tag_J2gcp8J1tqFs8RI37cGF8F
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  e_codertarget_raspi_internal__T Hw;
  uint32_T BusSpeed;
  MW_Handle_Type MW_I2C_HANDLE;
  real_T SampleTime;
};

#endif                                 /*struct_tag_J2gcp8J1tqFs8RI37cGF8F*/

#ifndef typedef_codertarget_raspi__nparbhma5g_T
#define typedef_codertarget_raspi__nparbhma5g_T

typedef struct tag_J2gcp8J1tqFs8RI37cGF8F codertarget_raspi__nparbhma5g_T;

#endif                               /*typedef_codertarget_raspi__nparbhma5g_T*/

/* Parameters for system: '<S1>/i2cRd' */
typedef struct P_i2cRd_master_code_V2_test_T_ P_i2cRd_master_code_V2_test_T;

/* Parameters for system: '<S1>/gpsRd' */
typedef struct P_gpsRd_master_code_V2_test_T_ P_gpsRd_master_code_V2_test_T;

/* Parameters (default storage) */
typedef struct P_master_code_V2_test_T_ P_master_code_V2_test_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_master_code_V2_test_T RT_MODEL_master_code_V2_test_T;

#endif                             /* RTW_HEADER_master_code_V2_test_types_h_ */
