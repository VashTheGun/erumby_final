/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP_Main.cc                                            |
 |                                                                       |
 |  version: 1.0   date 8/12/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/


#include "kineDyn_OCP.hh"
#include "kineDyn_OCP_Pars.hh"

using namespace std;
using Mechatronix::real_type;
using Mechatronix::integer;
using Mechatronix::ostream_type;

// user class in namespaces
using Mechatronix::Path2D;

using namespace kineDyn_OCPLoad;
using GenericContainerNamespace::GenericContainer;

static bool SET  = true;
static bool FREE = false;

int
main() {

  #ifdef MECHATRONIX_OS_WINDOWS
  __try {
  #endif

  Mechatronix::Console    console(&std::cout,4);
  Mechatronix::ThreadPool TP(std::thread::hardware_concurrency());

  try {

    kineDyn_OCP      model("kineDyn_OCP",&TP,&console);
    GenericContainer gc_data;
    GenericContainer gc_solution;

    // user defined Object instances (external)
    Path2D           trajectory( "trajectory" );

    // Auxiliary values
    real_type delta_max = 2/9.0*Mechatronix::m_pi;
    real_type pen_epsi = 0.01;
    real_type pen_epsi_u = 0.1;
    real_type ax0_min = -5;
    real_type Wf = 0.2;
    real_type vehHalfWidth = 1/2.0*Wf;
    real_type delta_min = -2/9.0*Mechatronix::m_pi;
    real_type ax0_max = 5;
    integer InfoLevel = 4;

    GenericContainer &  data_ControlSolver = gc_data["ControlSolver"];
    // ==============================================================
    // 'LU', 'LUPQ', 'QR', 'QRP', 'SVD', 'LSS', 'LSY', 'MINIMIZATION'
    // :factorization => 'LU',
    // ==============================================================
    data_ControlSolver["Rcond"]     = 1e-14; // reciprocal condition number threshold for QR, SVD, LSS, LSY
    data_ControlSolver["MaxIter"]   = 50;
    data_ControlSolver["Tolerance"] = 1e-9;
    data_ControlSolver["Iterative"] = false;
    data_ControlSolver["InfoLevel"] = 1;

    // Enable doctor
    gc_data["Doctor"] = false;

    // Enable check jacobian
    gc_data["JacobianCheck"]            = false;
    gc_data["JacobianCheckFull"]        = false;
    gc_data["JacobianCheck_epsilon"]    = 1e-4;
    gc_data["FiniteDifferenceJacobian"] = false;

    // Dump Function and Jacobian if uncommented
    gc_data["DumpFile"] = "kineDyn_OCP_dump";

    // spline output (all values as function of "s")
    gc_data["OutputSplines"] = "s";

    // setup solver
    GenericContainer & data_Solver = gc_data["Solver"];
    // Linear algebra factorization selection:
    // 'LU', 'QR', 'QRP', 'SUPERLU'
    // =================
    data_Solver["factorization"] = "LU";
    // =================

    // Last Block selection:
    // 'LU', 'LUPQ', 'QR', 'QRP', 'SVD', 'LSS', 'LSY'
    // ==============================================
    data_Solver["last_factorization"] = "LU";
    // ==============================================

    // choose solver: Hyness, NewtonDumped
    // ===================================
    data_Solver["solver"] = "Hyness";
    // ===================================

    // solver parameters
    data_Solver["max_iter"]              = 600;
    data_Solver["max_step_iter"]         = 100;
    data_Solver["max_accumulated_iter"]  = 5000;
    data_Solver["tolerance"]             = 9.999999999999999e-10;
    // continuation parameters
    data_Solver["ns_continuation_begin"] = 0;
    data_Solver["ns_continuation_end"]   = 0;
    GenericContainer & data_Continuation = data_Solver["continuation"];
    data_Continuation["initial_step"]   = 0.2;   // initial step for continuation
    data_Continuation["min_step"]       = 0.001; // minimum accepted step for continuation
    data_Continuation["reduce_factor"]  = 0.5;   // p fails, reduce step by this factor
    data_Continuation["augment_factor"] = 1.5;   // if step successful in less than few_iteration augment step by this factor
    data_Continuation["few_iterations"] = 8;

    // Boundary Conditions
    GenericContainer & data_BoundaryConditions = gc_data["BoundaryConditions"];

    // Guess
    GenericContainer & data_Guess = gc_data["Guess"];
    // possible value: zero, default, none, warm
    data_Guess["initialize"] = "zero";
    // possible value: default, none, warm, spline, table
    data_Guess["guess_type"] = "default";

    GenericContainer & data_Parameters = gc_data["Parameters"];
    // Model Parameters
    data_Parameters["L"] = 0.325;
    data_Parameters["ax0_max"] = ax0_max;
    data_Parameters["ax0_min"] = ax0_min;
    data_Parameters["axoffs"] = 0;
    data_Parameters["k__D"] = 0.6;
    data_Parameters["m"] = 4.61;
    data_Parameters["nlow"] = 2;
    data_Parameters["nupp"] = 2;
    data_Parameters["tau__D"] = 1;
    data_Parameters["v_lim"] = 1.5;
    data_Parameters["wN"] = 0;
    data_Parameters["wT"] = 1;
    data_Parameters["wU"] = 0;
    data_Parameters["delta_max"] = delta_max;
    data_Parameters["delta_min"] = delta_min;
    data_Parameters["tau__Omega"] = 0.09;
    data_Parameters["tau__a__x"] = 1.2;
    data_Parameters["tau__delta"] = 1;
    data_Parameters["vehHalfWidth"] = vehHalfWidth;

    // Guess Parameters
    data_Parameters["v__x0"] = 0.5;

    // Boundary Conditions
    data_Parameters["Omega_i"] = 0;
    data_Parameters["WBCF__n"] = 0;
    data_Parameters["WBCI__n"] = 100;
    data_Parameters["a_xi"] = 0;
    data_Parameters["n_f"] = 0;
    data_Parameters["n_i"] = 0;
    data_Parameters["v_xf"] = 0.4;
    data_Parameters["v_xi"] = 0.001;
    data_Parameters["xi_f"] = 0;
    data_Parameters["xi_i"] = 0;
    data_Parameters["WBCF__vx"] = 0;
    data_Parameters["WBCF__xi"] = 0;
    data_Parameters["WBCI__Omega"] = 10;
    data_Parameters["WBCI__ax"] = 10;
    data_Parameters["WBCI__delta"] = 5;
    data_Parameters["WBCI__vx"] = 10;
    data_Parameters["WBCI__xi"] = 100;
    data_Parameters["deltaD_i"] = 0;

    // Post Processing Parameters

    // User Function Parameters
    data_Parameters["kUS_0"] = 0.0026972770813694711516;
    data_Parameters["kUS_1"] = -0.0081875537550975263645;
    data_Parameters["kUS_2"] = -0.0011929422362531626536;
    data_Parameters["kUS_3"] = -0.00035294597392617561762;
    data_Parameters["kUS_4"] = 8.5091091260215668234e-05;
    data_Parameters["kUS_5"] = 3.5481515489890636124e-05;
    data_Parameters["kUS_6"] = -2.1830324115438913879e-07;
    data_Parameters["kUS_7"] = 3.3717974046733384754e-08;
    data_Parameters["fit_axM_0"] = 2.0007734867398569989;
    data_Parameters["fit_axM_1"] = -0.40587067207410132408;
    data_Parameters["fit_axM_2"] = 0.315471134325989222;
    data_Parameters["fit_axM_3"] = -0.88497317760962146416;
    data_Parameters["fit_axM_4"] = 1.5535122364178857168;
    data_Parameters["fit_axM_5"] = -1.1831373140545495826;
    data_Parameters["fit_axM_6"] = 0.38319389087590816079;
    data_Parameters["fit_axM_7"] = -0.044248236113664331237;
    data_Parameters["fit_axm_0"] = 0.40018274905557582599;
    data_Parameters["fit_axm_1"] = 0.0010937824305734659448;
    data_Parameters["fit_axm_2"] = 0.044645212480998265614;
    data_Parameters["fit_axm_3"] = -0.018404661172988015516;
    data_Parameters["fit_axm_4"] = 0.0024932247244647430182;
    data_Parameters["fit_ayM_0"] = 0.40130374314511435552;
    data_Parameters["fit_ayM_1"] = 1.0164809600725424055;
    data_Parameters["fit_ayM_2"] = -4.2415333816731148886;
    data_Parameters["fit_ayM_3"] = 6.293036245221803604;
    data_Parameters["fit_ayM_4"] = -2.7206078307679133488;
    data_Parameters["fit_ayM_5"] = 0.37130998060651149162;

    // Continuation Parameters

    // Constraints Parameters

    // functions mapped on objects
    GenericContainer & gc_MappedObjects = gc_data["MappedObjects"];

    // PositivePartRegularizedWithSinAtan
    GenericContainer & data_posPart = gc_MappedObjects["posPart"];
    data_posPart["h"] = 0.01;

    // NegativePartRegularizedWithSinAtan
    GenericContainer & data_negPart = gc_MappedObjects["negPart"];
    data_negPart["h"] = 0.01;

    // SignRegularizedWithErf
    GenericContainer & data_SignReg = gc_MappedObjects["SignReg"];
    data_SignReg["h"] = 0.1;
    data_SignReg["epsilon"] = 0.01;

    // SignRegularizedWithErf
    GenericContainer & data_SignReg_smooth = gc_MappedObjects["SignReg_smooth"];
    data_SignReg_smooth["h"] = 1;
    data_SignReg_smooth["epsilon"] = 0.01;

    // AbsoluteValueRegularizedWithSinAtan
    GenericContainer & data_abs_reg = gc_MappedObjects["abs_reg"];
    data_abs_reg["h"] = 0.01;

    // Controls
    // Control Penalty type: QUADRATIC, QUADRATIC2, PARABOLA, CUBIC
    // Control Barrier type: LOGARITHMIC, COS_LOGARITHMIC, TAN2, HYPERBOLIC
    GenericContainer & data_Controls = gc_data["Controls"];
    GenericContainer & data_delta__D0Control = data_Controls["delta__D0Control"];
    data_delta__D0Control["type"]      = "QUADRATIC";
    data_delta__D0Control["epsilon"]   = pen_epsi_u;
    data_delta__D0Control["tolerance"] = 0.001;


    GenericContainer & data_a__x0Control = data_Controls["a__x0Control"];
    data_a__x0Control["type"]      = "COS_LOGARITHMIC";
    data_a__x0Control["epsilon"]   = pen_epsi_u;
    data_a__x0Control["tolerance"] = 0.001;



    // Constraint1D
    // Penalty subtype: PENALTY_REGULAR, PENALTY_SMOOTH, PENALTY_PIECEWISE
    // Barrier subtype: BARRIER_LOG, BARRIER_LOG_EXP, BARRIER_LOG0
    GenericContainer & data_Constraints = gc_data["Constraints"];
    // PenaltyBarrier1DGreaterThan
    GenericContainer & data_roadRightLateralBoundaries = data_Constraints["roadRightLateralBoundaries"];
    data_roadRightLateralBoundaries["subType"]   = "PENALTY_REGULAR";
    data_roadRightLateralBoundaries["epsilon"]   = pen_epsi;
    data_roadRightLateralBoundaries["tolerance"] = 0.01;
    data_roadRightLateralBoundaries["active"]    = true;
    // PenaltyBarrier1DGreaterThan
    GenericContainer & data_roadLeftLateralBoundaries = data_Constraints["roadLeftLateralBoundaries"];
    data_roadLeftLateralBoundaries["subType"]   = "PENALTY_REGULAR";
    data_roadLeftLateralBoundaries["epsilon"]   = pen_epsi;
    data_roadLeftLateralBoundaries["tolerance"] = 0.01;
    data_roadLeftLateralBoundaries["active"]    = true;
    // PenaltyBarrier1DGreaterThan
    GenericContainer & data_GGdiagramEnvelope = data_Constraints["GGdiagramEnvelope"];
    data_GGdiagramEnvelope["subType"]   = "PENALTY_REGULAR";
    data_GGdiagramEnvelope["epsilon"]   = 0.01;
    data_GGdiagramEnvelope["tolerance"] = 0.01;
    data_GGdiagramEnvelope["active"]    = true;
    // Constraint2D: none defined

    // User defined classes initialization
    // User defined classes: T R A J E C T O R Y


    // alias for user object classes passed as pointers
    GenericContainer & ptrs = gc_data["Pointers"];
    // setup user object classes
    UTILS_ASSERT0(
      gc_data.exists("Trajectory"),
      "missing key: ``Trajectory'' in gc_data\n"
    );
    trajectory.setup(gc_data("Trajectory"));
    ptrs[ "pTrajectory" ] = &trajectory;

    // setup model
    model.setup( gc_data );

    // initialize nonlinear system initial point
    model.guess( gc_data("Guess","Missing `Guess` field") );

    // solve nonlinear system
    // pModel->set_timeout_ms( 100 );
    bool ok = model.solve(); // no spline

    // get solution (even if not converged)
    model.get_solution( gc_solution );
    model.diagnostic( gc_data );

    std::ofstream file;
    if ( ok ) {
      file.open( "data/kineDyn_OCP_OCP_result.txt" );
    } else {
      cout << gc_solution("solver_message").get_string() << '\n';
      // dump solution to file
      file.open( "data/kineDyn_OCP_OCP_not_converged.txt" );
    }
    file.precision(18);
    Mechatronix::saveOCPsolutionToStream(gc_solution,file);
    file.close();
    cout.precision(18);
    GenericContainer const & target = gc_solution("target");
    fmt::print(
      "Lagrange target    = {}\n"
      "Mayer target       = {}\n"
      "Penalties+Barriers = {}\n"
      "Control penalties  = {}\n",
      target("lagrange").get_number(),  target("mayer").get_number(),
      target("penalties").get_number(), target("control_penalties").get_number()
    );
    if ( gc_solution.exists("parameters") ) {
      cout << "Optimization parameters:\n";
      gc_solution("parameters").print(cout);
    }
    if ( gc_solution.exists("diagnosis") ) gc_solution("diagnosis").print(cout);
  }
  catch ( std::exception const & exc ) {
    console.error(exc.what());
    ALL_DONE_FOLKS;
    exit(0);
  }
  catch ( char const exc[] ) {
    console.error(exc);
    ALL_DONE_FOLKS;
    exit(0);
  }
  catch (...) {
    console.error("ERRORE SCONOSCIUTO\n");
    ALL_DONE_FOLKS;
    exit(0);
  }

  ALL_DONE_FOLKS;

  #ifdef MECHATRONIX_OS_WINDOWS
  } __finally {
    cerr << "Unknown windows error found, exiting\n";
  }
  #endif

  return 0;
}
