#!/usr/bin/env ruby
############################################################################
#                                                                          #
#  file: kineDyn_OCP_ffi_structs.rb                                        #
#                                                                          #
#  version: 1.0   date 8/12/2020                                           #
#                                                                          #
#  Copyright (C) 2020                                                      #
#                                                                          #
#      Enrico Bertolazzi and Francesco Biral and Paolo Bosetti             #
#      Dipartimento di Ingegneria Industriale                              #
#      Universita` degli Studi di Trento                                   #
#      Via Mesiano 77, I-38050 Trento, Italy                               #
#      email: enrico.bertolazzi@ing.unitn.it                               #
#             francesco.biral@ing.unitn.it                                 #
#             paolo.bosetti@ing.unitn.it                                   #
#                                                                          #
############################################################################

module kineDyn_OCP

  typedef :double, :data_t
  typedef :uint32, :index_t
  typedef :int,    :retcode

  class kineDyn_OCP_solver_params < FFI::Struct
    layout(
      :max_iter,             :index_t,
      :max_step_iter,        :index_t,
      :max_accumulated_iter, :index_t,
      :tolerance,            :data_t,
    )
    def initialize
      self[:max_iter]      = 500
      self[:max_step_iter] = 20
      self[:tolerance]     = 1e-10
    end
  end

  class kineDyn_OCP_model_params < FFI::Struct
    layout(

      :L,            :data_t,

      :Omega_i,      :data_t,

      :WBCF__n,      :data_t,

      :WBCI__n,      :data_t,

      :a_xi,         :data_t,

      :ax0_max,      :data_t,

      :ax0_min,      :data_t,

      :axoffs,       :data_t,

      :kUS_0,        :data_t,

      :kUS_1,        :data_t,

      :kUS_2,        :data_t,

      :kUS_3,        :data_t,

      :kUS_4,        :data_t,

      :kUS_5,        :data_t,

      :kUS_6,        :data_t,

      :kUS_7,        :data_t,

      :k__D,         :data_t,

      :m,            :data_t,

      :n_f,          :data_t,

      :n_i,          :data_t,

      :nlow,         :data_t,

      :nupp,         :data_t,

      :tau__D,       :data_t,

      :v__x0,        :data_t,

      :v_lim,        :data_t,

      :v_xf,         :data_t,

      :v_xi,         :data_t,

      :wN,           :data_t,

      :wT,           :data_t,

      :wU,           :data_t,

      :xi_f,         :data_t,

      :xi_i,         :data_t,

      :WBCF__vx,     :data_t,

      :WBCF__xi,     :data_t,

      :WBCI__Omega,  :data_t,

      :WBCI__ax,     :data_t,

      :WBCI__delta,  :data_t,

      :WBCI__vx,     :data_t,

      :WBCI__xi,     :data_t,

      :deltaD_i,     :data_t,

      :delta_max,    :data_t,

      :delta_min,    :data_t,

      :fit_axM_0,    :data_t,

      :fit_axM_1,    :data_t,

      :fit_axM_2,    :data_t,

      :fit_axM_3,    :data_t,

      :fit_axM_4,    :data_t,

      :fit_axM_5,    :data_t,

      :fit_axM_6,    :data_t,

      :fit_axM_7,    :data_t,

      :fit_axm_0,    :data_t,

      :fit_axm_1,    :data_t,

      :fit_axm_2,    :data_t,

      :fit_axm_3,    :data_t,

      :fit_axm_4,    :data_t,

      :fit_ayM_0,    :data_t,

      :fit_ayM_1,    :data_t,

      :fit_ayM_2,    :data_t,

      :fit_ayM_3,    :data_t,

      :fit_ayM_4,    :data_t,

      :fit_ayM_5,    :data_t,

      :tau__Omega,   :data_t,

      :tau__a__x,    :data_t,

      :tau__delta,   :data_t,

      :vehHalfWidth, :data_t,

    )

    def initialize
      members.each { |m| self[m] = Float::NAN }
      # Custom initializations go here:
      # self[:key] = value
    end
  end

  class kineDyn_OCP_BC_params < FFI::Struct
    layout(

    )

    def initialize
      members.each { |m| self[m] = true }
      # Custom initializations go here:
      # self[:key] = value
    end
  end

  class kineDyn_OCP_constraints_params < FFI::Struct
    layout(
      # 1D constraints
      :roadRightLateralBoundariesSubType,   :index_t,
      :roadRightLateralBoundariesEpsilon,   :data_t,
      :roadRightLateralBoundariesTolerance, :data_t,
      :roadLeftLateralBoundariesSubType,    :index_t,
      :roadLeftLateralBoundariesEpsilon,    :data_t,
      :roadLeftLateralBoundariesTolerance,  :data_t,
      :GGdiagramEnvelopeSubType,            :index_t,
      :GGdiagramEnvelopeEpsilon,            :data_t,
      :GGdiagramEnvelopeTolerance,          :data_t,

      # 2D constraints

      # Controls
      :delta__D0ControlType,      :index_t,
      :delta__D0ControlEpsilon,   :data_t,
      :delta__D0ControlTolerance, :data_t,
      :a__x0ControlType,          :index_t,
      :a__x0ControlEpsilon,       :data_t,
      :a__x0ControlTolerance,     :data_t,
    )

    def initialize
      members.each do |m|
        case self[m]
        when Float
          self[m] = Float::NAN
        when Fixnum
          self[m] = 0
        when FFI::Pointer
          self[m] = nil
        else
          warn "Unmanaged initialization type in struct (for field #{m} of type #{self[m].class})"
        end
      end
      # Custom initializations go here:
      # self[:key] = value
    end
  end

  attach_function :setup_model, # ruby name
                  :kineDyn_OCP_setup_model, # C name
                  [:pointer ,:pointer ,:pointer ],
                  :void

  attach_function :setup_solver, # ruby name
                  :kineDyn_OCP_setup_solver, # C name
                  [:pointer ],
                  :void

  attach_function :write_solution_to_file, # ruby name
                  :kineDyn_OCP_write_solution_to_file, # C name
                  [:string],
                  :void

  attach_function :printout_enabled?, # ruby name
                  :kineDyn_OCP_printout_is_enabled, # C name
                  [],
                  :int

  attach_function :enable_printout, # ruby name
                  :kineDyn_OCP_enable_printout, # C name
                  [],
                  :void

  attach_function :disable_printout, # ruby name
                  :kineDyn_OCP_disable_printout, # C name
                  [],
                  :void

  attach_function :reset_multiplier, # ruby name
                  :kineDyn_OCP_reset_multiplier, # C name
                  [],
                  :void

  attach_function :reset_BC_multiplier, # ruby name
                  :kineDyn_OCP_reset_BC_multiplier, # C name
                  [],
                  :void

  attach_function :set_internal_guess, # ruby name
                  :kineDyn_OCP_set_internal_guess, # C name
                  [],
                  :void

end

# EOF: kineDyn_OCP_ffi_stucts.rb
