--[[
/*-----------------------------------------------------------------------*\
 |  file: kineDyn_OCP_Data.lua                                           |
 |                                                                       |
 |  version: 1.0   date 8/12/2020                                        |
 |                                                                       |
 |  Copyright (C) 2020                                                   |
 |                                                                       |
 |      Enrico Bertolazzi, Francesco Biral and Paolo Bosetti             |
 |      Dipartimento di Ingegneria Industriale                           |
 |      Universita` degli Studi di Trento                                |
 |      Via Sommarive 9, I-38123, Trento, Italy                          |
 |      email: enrico.bertolazzi@unitn.it                                |
 |             francesco.biral@unitn.it                                  |
 |             paolo.bosetti@unitn.it                                    |
\*-----------------------------------------------------------------------*/

--]]

-- Auxiliary values
delta_max    = 2/9.0*Pi
pen_epsi     = 0.01
pen_epsi_u   = 0.1
ax0_min      = -5
Wf           = 0.2
vehHalfWidth = 1/2.0*Wf
delta_min    = -2/9.0*Pi
ax0_max      = 5

content = {

  -- Level of message
  InfoLevel = 4,

  -- maximum number of threads used for linear algebra and various solvers
  N_threads   = [1,$MAX_THREAD_NUM-1].max,
  U_threaded  = true,
  F_threaded  = true,
  JF_threaded = true,
  LU_threaded = true,

  -- Enable doctor
  Doctor = false,

  -- Activate dynamic debugging
  Debug = false,

  -- Enable check jacobian
  JacobianCheck            = false,
  JacobianCheckFull        = false,
  JacobianCheck_epsilon    = 1e-4,
  FiniteDifferenceJacobian = false,

  -- Dump Function and Jacobian if uncommented
  -- DumpFile = "kineDyn_OCP_dump",

  -- spline output (all values as function of "s")
  -- OutputSplines = [0],

  ControlSolver = {
    -- "LU", "LUPQ", "QR", "QRP", "SVD", "LSS", "LSY", "MINIMIZATION"
    factorization = "LU",
    MaxIter       = 50,
    Tolerance     = 1e-9,
    Iterative     = false,
    InfoLevel     = -1 -- suppress all messages
  },

  -- setup solver
  Solver = {
    -- Linear algebra factorization selection:
    -- "LU", "QR", "QRP", "SUPERLU"
    factorization = "LU",

    -- Last Block selection:
    -- "LU", "LUPQ", "QR", "QRP", "SVD", "LSS", "LSY"
    last_factorization = "LU",

    -- choose solves: Hyness, NewtonDumped
    solver = "Hyness",

    -- solver parameters
    max_iter             = 600,
    max_step_iter        = 100,
    max_accumulated_iter = 5000,
    tolerance            = 9.999999999999999e-10,

    -- continuation parameters
    ns_continuation_begin = 0,
    ns_continuation_end   = 0,
    continuation = {
      initial_step   = 0.2,   -- initial step for continuation
      min_step       = 0.001, -- minimum accepted step for continuation
      reduce_factor  = 0.5,   -- if continuation step fails, reduce step by this factor
      augment_factor = 1.5,   -- if step successful in less than few_iteration augment step by this factor
      few_iterations = 8
    }
  },

  -- Boundary Conditions (SET/FREE)
  BoundaryConditions = {
  },

  -- Guess
  Guess = {
    -- possible value: zero, default, none, warm
    initialize = "zero",
    -- possible value: default, none, warm, spline, table
    guess_type = "default"
  },

  Parameters = {

    -- Model Parameters
    L            = 0.325,
    ax0_max      = ax0_max,
    ax0_min      = ax0_min,
    axoffs       = 0,
    k__D         = 0.6,
    m            = 4.61,
    nlow         = 2,
    nupp         = 2,
    tau__D       = 1,
    v_lim        = 1.5,
    wN           = 0,
    wT           = 1,
    wU           = 0,
    delta_max    = delta_max,
    delta_min    = delta_min,
    tau__Omega   = 0.09,
    tau__a__x    = 1.2,
    tau__delta   = 1,
    vehHalfWidth = vehHalfWidth,

    -- Guess Parameters
    v__x0 = 0.5,

    -- Boundary Conditions
    Omega_i     = 0,
    WBCF__n     = 0,
    WBCI__n     = 100,
    a_xi        = 0,
    n_f         = 0,
    n_i         = 0,
    v_xf        = 0.4,
    v_xi        = 0.001,
    xi_f        = 0,
    xi_i        = 0,
    WBCF__vx    = 0,
    WBCF__xi    = 0,
    WBCI__Omega = 10,
    WBCI__ax    = 10,
    WBCI__delta = 5,
    WBCI__vx    = 10,
    WBCI__xi    = 100,
    deltaD_i    = 0,

    -- Post Processing Parameters

    -- User Function Parameters
    kUS_0     = 0.0026972770813694711516,
    kUS_1     = -0.0081875537550975263645,
    kUS_2     = -0.0011929422362531626536,
    kUS_3     = -0.00035294597392617561762,
    kUS_4     = 8.5091091260215668234e-05,
    kUS_5     = 3.5481515489890636124e-05,
    kUS_6     = -2.1830324115438913879e-07,
    kUS_7     = 3.3717974046733384754e-08,
    fit_axM_0 = 2.0007734867398569989,
    fit_axM_1 = -0.40587067207410132408,
    fit_axM_2 = 0.315471134325989222,
    fit_axM_3 = -0.88497317760962146416,
    fit_axM_4 = 1.5535122364178857168,
    fit_axM_5 = -1.1831373140545495826,
    fit_axM_6 = 0.38319389087590816079,
    fit_axM_7 = -0.044248236113664331237,
    fit_axm_0 = 0.40018274905557582599,
    fit_axm_1 = 0.0010937824305734659448,
    fit_axm_2 = 0.044645212480998265614,
    fit_axm_3 = -0.018404661172988015516,
    fit_axm_4 = 0.0024932247244647430182,
    fit_ayM_0 = 0.40130374314511435552,
    fit_ayM_1 = 1.0164809600725424055,
    fit_ayM_2 = -4.2415333816731148886,
    fit_ayM_3 = 6.293036245221803604,
    fit_ayM_4 = -2.7206078307679133488,
    fit_ayM_5 = 0.37130998060651149162,

    -- Continuation Parameters

    -- Constraints Parameters
  },

  -- functions mapped objects
  MappedObjects = {
  -- PositivePartRegularizedWithSinAtan
    posParth = 0.01,
  -- NegativePartRegularizedWithSinAtan
    negParth = 0.01,
  -- SignRegularizedWithErf
    SignRegh = 0.1,
    SignRegepsilon = 0.01,
  -- SignRegularizedWithErf
    SignReg_smoothh = 1,
    SignReg_smoothepsilon = 0.01,
  -- AbsoluteValueRegularizedWithSinAtan
    abs_regh = 0.01,
  },

  -- Controls
  -- Penalty subtype: PENALTY_REGULAR, PENALTY_SMOOTH, PENALTY_PIECEWISE
  -- Barrier subtype: BARRIER_LOG, BARRIER_LOG_EXP, BARRIER_LOG0
  Controls = {
    delta__D0Control = {
      type      = 'QUADRATIC',
      epsilon   = pen_epsi_u,
      tolerance = 0.001,
    },
    a__x0Control = {
      type      = 'COS_LOGARITHMIC',
      epsilon   = pen_epsi_u,
      tolerance = 0.001,
    },
  },

  Constraints = {
  -- Constraint1D
  -- Penalty subtype: PENALTY_REGULAR, PENALTY_SMOOTH, PENALTY_PIECEWISE
  -- Barrier subtype: BARRIER_LOG, BARRIER_LOG_EXP, BARRIER_LOG0
    -- PenaltyBarrier1DGreaterThan
    roadRightLateralBoundariessubType   = "PENALTY_REGULAR",
    roadRightLateralBoundariesepsilon   = pen_epsi,
    roadRightLateralBoundariestolerance = 0.01,
    roadRightLateralBoundariesactive    = true

    -- PenaltyBarrier1DGreaterThan
    roadLeftLateralBoundariessubType   = "PENALTY_REGULAR",
    roadLeftLateralBoundariesepsilon   = pen_epsi,
    roadLeftLateralBoundariestolerance = 0.01,
    roadLeftLateralBoundariesactive    = true

    -- PenaltyBarrier1DGreaterThan
    GGdiagramEnvelopesubType   = "PENALTY_REGULAR",
    GGdiagramEnvelopeepsilon   = 0.01,
    GGdiagramEnvelopetolerance = 0.01,
    GGdiagramEnvelopeactive    = true

  -- Constraint2D: none defined
  },

  -- User defined classes initialization
  -- User defined classes: T R A J E C T O R Y
  dofile('../../Custom_Tracks/Pergine_Track/trajectory_data.rb')


}

-- EOF
