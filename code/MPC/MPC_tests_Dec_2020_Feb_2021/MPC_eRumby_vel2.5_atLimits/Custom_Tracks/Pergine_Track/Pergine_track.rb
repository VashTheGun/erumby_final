# User defined classes initialization
# User defined classes: R O A D
#rubbered = 0.1
#initAdherence = 1
#
adh = 1.0

grid1 = 0.05
grid0 = 0.01
width = 0.6

mechatronix do |data|
  data.Road = {
    :theta0   => - Math::PI,
    :s0       => 0,
    :x0       => 7,
    :y0       => 2,
    :is_SAE   => false,
    :gridSize => grid1, 
    :width    => width,
    :adherenceLimit => adh,
    :segments => [
    
    {
        :length     =>  4.00000,
        :gridSize   => grid1,
        :initialCurvature  =>  0.00000,
        :finalCurvature  =>  0.00000,
    },
    {
        :length     =>  5.06656,
        :gridSize   => grid1,
        :initialCurvature  =>  0.39991,
        :finalCurvature  => -0.81009,
    },
    {
        :length     =>  4.19845,
        :gridSize   => grid1,
        :initialCurvature  => -0.50025,
        :finalCurvature  => -0.50131,
    },
    {
        :length     =>  3.19517,
        :gridSize   => grid1,
        :initialCurvature  => -0.60360,
        :finalCurvature  =>  0.60360,
    },
    {
        :length     =>  4.00000,
        :gridSize   => grid1,
        :initialCurvature  =>  0.00000,
        :finalCurvature  =>  0.00000,
    },
    {
        :length     =>  3.14159,
        :gridSize   => grid1,
        :initialCurvature  =>  1.00000,
        :finalCurvature  =>  1.00000,
    },
    {
        :length     =>  3.00000,
        :gridSize   => grid1,
        :initialCurvature  =>  0.00000,
        :finalCurvature  =>  0.00000,
    },
    
    {
        :length     =>  2.00000,
        :gridSize   => grid0,
        :initialCurvature  =>  0.00000,
        :finalCurvature  =>  0.00000,
    },
    
    {
        :length     =>  2.00000,
        :gridSize   => grid1,
        :initialCurvature  =>  0.00000,
        :finalCurvature  =>  0.00000,
    },
    {
        :length     =>  9.42478,
        :gridSize   => grid1,
        :initialCurvature  =>  0.33333,
        :finalCurvature  =>  0.33333,
    },
    {
        :length     =>  2.28460,
        :gridSize   => grid1,
        :initialCurvature  =>  1.21516,
        :finalCurvature  => -1.21516,
    },
    {
        :length     =>  2.00000,
        :gridSize   => grid1,
        :initialCurvature  =>  0.00000,
        :finalCurvature  =>  0.00000,
    },
    {
        :length     =>  2.28460,
        :gridSize   => grid1,
        :initialCurvature  => -1.21516,
        :finalCurvature  =>  1.21516,
    },
    {
        :length     =>  1.00000,
        :gridSize   => grid1,
        :initialCurvature  =>  0.00000,
        :finalCurvature  =>  0.00000,
    },
    {
        :length     =>  3.14159,
        :gridSize   => grid1,
        :initialCurvature  =>  1.00000,
        :finalCurvature  =>  1.00000,
    }
=begin
=end

    ],
 };

end

# EOF
