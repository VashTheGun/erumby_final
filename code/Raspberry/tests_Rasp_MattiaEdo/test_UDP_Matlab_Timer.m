%% Initialize
clc; clearvars;

set(0,'defaultAxesFontSize',18)
set(0,'DefaultLegendFontSize',18)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');
set(0,'DefaultFigureWindowStyle','docked');

%% Test communication
udps = dsp.UDPSender('RemoteIPAddress', '10.0.0.1','RemoteIPPort',35000);
udpr = dsp.UDPReceiver('RemoteIPAddress','10.0.0.1', 'LocalIPPort',25000, 'MessageDataType','double');

dataResend = 0;
dataRec = zeros(100000,4);
dataSen = zeros(100000,2);
sendNows = zeros(1,10^9);
i = 1;
j = 1;

startTime = str2double(datestr(clock,'SS.FFF'));
timerObj = timer('TimerFcn','sendNow = 1;','Period',0.1, ...  %'udps(int16(3))'   @timerFcn_UDP
                 'ExecutionMode','fixedRate','BusyMode','queue','StopFcn','disp(''Timer has stopped.'')');    
start(timerObj);

while(i < 20) 
    if (sendNow==1)
        dataSent = int16(dataResend);
        dataSen(i,1) = dataSent;
        dataSen(i,2) = str2double(datestr(clock,'SS.FFF'));
%         udps(dataSent);
        sendNow = 0;
    end
    dataReceived = udpr();
    if(~isempty(dataReceived))
        dataResend = double(dataReceived);
        dataRec(i,1:3) = dataReceived;
        dataRec(i,4) = str2double(datestr(clock,'SS.FFF'));
        disp(dataRec(i,:));
        i = i+1;
    end
    sendNows(j) = sendNow;
    j = j+1;
end
    
stop(timerObj);
delete(timerObj);

release(udps);
release(udpr);

dataSent_store = dataSen(1:i-1,:);
dataRece_store = dataRec(1:i-1,:);

delay_UDP = dataRece_store(:,4) - dataSent_store(:,2);

%% Plots
% ----------------------------------------
% Times at which the messages are received
% ----------------------------------------
figure('Name','Rec time','NumberTitle','off'), clf   
plot(diff(dataRece_store(:,4)),'LineWidth',2)
grid on
xlabel('sample num')
ylabel('Time msg rec [s]')

% ----------------------------------------
% UDP Delay 
% ----------------------------------------
figure('Name','Delay UDP','NumberTitle','off'), clf 
plot(delay_UDP,'LineWidth',2)
grid on
xlabel('sample num')
ylabel('Delay [s]')
