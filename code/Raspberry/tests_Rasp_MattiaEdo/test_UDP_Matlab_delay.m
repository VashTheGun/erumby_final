clc; clearvars;

% create udp sender
udps = dsp.UDPSender('RemoteIPAddress','10.0.0.1', 'RemoteIPPort',35000);
udpr = dsp.UDPReceiver('RemoteIPAddress','10.0.0.1', 'LocalIPPort',25000, 'MessageDataType','double');

timerObj = timer('TimerFcn', @timerFcn_UDP,'Period',1, ...   
                  'ExecutionMode','fixedRate','BusyMode','queue','StopFcn','disp(''Timer has stopped.'')');
start(timerObj);

dataRec = 1;
while(1)
%     dataSent = int16(dataRec);
%     tic;
%     udps(dataSent);
    dataReceived = udpr();
%     elapsedTime = toc;
    if(~isempty(dataReceived))
        dataRec = double(dataReceived);
        fprintf('Received %.3f\n',dataRec)
        % disp(dataRec);
    end
end
    
stop(timerObj);
delete(timerObj);

release(udps);
release(udpr);