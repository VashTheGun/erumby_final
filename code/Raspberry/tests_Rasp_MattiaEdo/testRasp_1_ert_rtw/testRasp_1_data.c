/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: testRasp_1_data.c
 *
 * Code generated for Simulink model 'testRasp_1'.
 *
 * Model version                  : 1.52
 * Simulink Coder version         : 9.2 (R2019b) 18-Jul-2019
 * C/C++ source code generated on : Tue Jan 21 17:04:38 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "testRasp_1.h"
#include "testRasp_1_private.h"

/* Block parameters (default storage) */
P_testRasp_1_T testRasp_1_P = {
  /* Mask Parameter: UDPSend2_remotePort
   * Referenced by: '<S1>/UDP Send2'
   */
  25000,

  /* Expression: 2
   * Referenced by: '<Root>/Constant1'
   */
  2.0,

  /* Expression: 1
   * Referenced by: '<Root>/Pulse Generator'
   */
  1.0,

  /* Computed Parameter: PulseGenerator_Period
   * Referenced by: '<Root>/Pulse Generator'
   */
  1000.0,

  /* Computed Parameter: PulseGenerator_Duty
   * Referenced by: '<Root>/Pulse Generator'
   */
  1.0,

  /* Expression: 0
   * Referenced by: '<Root>/Pulse Generator'
   */
  0.0
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
