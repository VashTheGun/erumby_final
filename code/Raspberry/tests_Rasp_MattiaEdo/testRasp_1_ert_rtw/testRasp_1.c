/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: testRasp_1.c
 *
 * Code generated for Simulink model 'testRasp_1'.
 *
 * Model version                  : 1.52
 * Simulink Coder version         : 9.2 (R2019b) 18-Jul-2019
 * C/C++ source code generated on : Tue Jan 21 17:04:38 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "testRasp_1.h"
#include "testRasp_1_private.h"

/* Block states (default storage) */
DW_testRasp_1_T testRasp_1_DW;

/* Real-time model */
RT_MODEL_testRasp_1_T testRasp_1_M_;
RT_MODEL_testRasp_1_T *const testRasp_1_M = &testRasp_1_M_;

/* Model step function */
void testRasp_1_step(void)
{
  char_T *sErr;
  real_T rtb_PulseGenerator;

  /* DiscretePulseGenerator: '<Root>/Pulse Generator' */
  rtb_PulseGenerator = (testRasp_1_DW.clockTickCounter <
                        testRasp_1_P.PulseGenerator_Duty) &&
    (testRasp_1_DW.clockTickCounter >= 0) ? testRasp_1_P.PulseGenerator_Amp :
    0.0;
  if (testRasp_1_DW.clockTickCounter >= testRasp_1_P.PulseGenerator_Period - 1.0)
  {
    testRasp_1_DW.clockTickCounter = 0;
  } else {
    testRasp_1_DW.clockTickCounter++;
  }

  /* End of DiscretePulseGenerator: '<Root>/Pulse Generator' */

  /* Outputs for Enabled SubSystem: '<Root>/Subsystem' incorporates:
   *  EnablePort: '<S1>/Enable'
   */
  if (rtb_PulseGenerator > 0.0) {
    /* Update for S-Function (sdspToNetwork): '<S1>/UDP Send2' incorporates:
     *  Constant: '<Root>/Constant1'
     */
    sErr = GetErrorBuffer(&testRasp_1_DW.UDPSend2_NetworkLib[0U]);
    LibUpdate_Network(&testRasp_1_DW.UDPSend2_NetworkLib[0U],
                      &testRasp_1_P.Constant1_Value, 1);
    if (*sErr != 0) {
      rtmSetErrorStatus(testRasp_1_M, sErr);
      rtmSetStopRequested(testRasp_1_M, 1);
    }

    /* End of Update for S-Function (sdspToNetwork): '<S1>/UDP Send2' */
  }

  /* End of Outputs for SubSystem: '<Root>/Subsystem' */
}

/* Model initialize function */
void testRasp_1_initialize(void)
{
  {
    char_T *sErr;

    /* Start for Enabled SubSystem: '<Root>/Subsystem' */
    /* Start for S-Function (sdspToNetwork): '<S1>/UDP Send2' */
    sErr = GetErrorBuffer(&testRasp_1_DW.UDPSend2_NetworkLib[0U]);
    CreateUDPInterface(&testRasp_1_DW.UDPSend2_NetworkLib[0U]);
    if (*sErr == 0) {
      LibCreate_Network(&testRasp_1_DW.UDPSend2_NetworkLib[0U], 1, "0.0.0.0", -1,
                        "10.0.0.13", testRasp_1_P.UDPSend2_remotePort, 8192, 8,
                        0);
    }

    if (*sErr == 0) {
      LibStart(&testRasp_1_DW.UDPSend2_NetworkLib[0U]);
    }

    if (*sErr != 0) {
      DestroyUDPInterface(&testRasp_1_DW.UDPSend2_NetworkLib[0U]);
      if (*sErr != 0) {
        rtmSetErrorStatus(testRasp_1_M, sErr);
        rtmSetStopRequested(testRasp_1_M, 1);
      }
    }

    /* End of Start for S-Function (sdspToNetwork): '<S1>/UDP Send2' */
    /* End of Start for SubSystem: '<Root>/Subsystem' */
  }
}

/* Model terminate function */
void testRasp_1_terminate(void)
{
  char_T *sErr;

  /* Terminate for Enabled SubSystem: '<Root>/Subsystem' */
  /* Terminate for S-Function (sdspToNetwork): '<S1>/UDP Send2' */
  sErr = GetErrorBuffer(&testRasp_1_DW.UDPSend2_NetworkLib[0U]);
  LibTerminate(&testRasp_1_DW.UDPSend2_NetworkLib[0U]);
  if (*sErr != 0) {
    rtmSetErrorStatus(testRasp_1_M, sErr);
    rtmSetStopRequested(testRasp_1_M, 1);
  }

  LibDestroy(&testRasp_1_DW.UDPSend2_NetworkLib[0U], 1);
  DestroyUDPInterface(&testRasp_1_DW.UDPSend2_NetworkLib[0U]);

  /* End of Terminate for S-Function (sdspToNetwork): '<S1>/UDP Send2' */
  /* End of Terminate for SubSystem: '<Root>/Subsystem' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
