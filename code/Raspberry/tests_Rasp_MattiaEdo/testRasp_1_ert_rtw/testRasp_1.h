/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: testRasp_1.h
 *
 * Code generated for Simulink model 'testRasp_1'.
 *
 * Model version                  : 1.52
 * Simulink Coder version         : 9.2 (R2019b) 18-Jul-2019
 * C/C++ source code generated on : Tue Jan 21 17:04:38 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_testRasp_1_h_
#define RTW_HEADER_testRasp_1_h_
#include <stddef.h>
#ifndef testRasp_1_COMMON_INCLUDES_
# define testRasp_1_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "DAHostLib_Network.h"
#endif                                 /* testRasp_1_COMMON_INCLUDES_ */

#include "testRasp_1_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T UDPSend2_NetworkLib[137];     /* '<S1>/UDP Send2' */
  int32_T clockTickCounter;            /* '<Root>/Pulse Generator' */
} DW_testRasp_1_T;

/* Parameters (default storage) */
struct P_testRasp_1_T_ {
  int32_T UDPSend2_remotePort;         /* Mask Parameter: UDPSend2_remotePort
                                        * Referenced by: '<S1>/UDP Send2'
                                        */
  real_T Constant1_Value;              /* Expression: 2
                                        * Referenced by: '<Root>/Constant1'
                                        */
  real_T PulseGenerator_Amp;           /* Expression: 1
                                        * Referenced by: '<Root>/Pulse Generator'
                                        */
  real_T PulseGenerator_Period;     /* Computed Parameter: PulseGenerator_Period
                                     * Referenced by: '<Root>/Pulse Generator'
                                     */
  real_T PulseGenerator_Duty;         /* Computed Parameter: PulseGenerator_Duty
                                       * Referenced by: '<Root>/Pulse Generator'
                                       */
  real_T PulseGenerator_PhaseDelay;    /* Expression: 0
                                        * Referenced by: '<Root>/Pulse Generator'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_testRasp_1_T {
  const char_T *errorStatus;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (default storage) */
extern P_testRasp_1_T testRasp_1_P;

/* Block states (default storage) */
extern DW_testRasp_1_T testRasp_1_DW;

/* Model entry point functions */
extern void testRasp_1_initialize(void);
extern void testRasp_1_step(void);
extern void testRasp_1_terminate(void);

/* Real-time Model object */
extern RT_MODEL_testRasp_1_T *const testRasp_1_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'testRasp_1'
 * '<S1>'   : 'testRasp_1/Subsystem'
 */
#endif                                 /* RTW_HEADER_testRasp_1_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
