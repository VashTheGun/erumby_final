clc; clearvars;

% create udp sender
udps = dsp.UDPSender('RemoteIPAddress','10.0.0.1', 'RemoteIPPort',35000);
udpr = dsp.UDPReceiver('RemoteIPAddress','10.0.0.1', 'LocalIPPort',25000, 'MessageDataType','double');


dataRec = 1;
while(1)
    dataSent = int16(dataRec);
    tic;
    udps(dataSent);
    dataReceived = udpr();
    elapsedTime = toc;
    if(~isempty(dataReceived))
        dataRec = double(dataReceived);
        fprintf('Received %.3f, elapsedTime = %.8f\n',dataRec,elapsedTime)
        % disp(dataRec);
    end
end
    
release(udps);
release(udpr);