/*
 * udp_test_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "udp_test".
 *
 * Model version              : 1.175
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C source code generated on : Thu Dec 19 14:44:33 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_udp_test_types_h_
#define RTW_HEADER_udp_test_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"

/* Parameters (default storage) */
typedef struct P_udp_test_T_ P_udp_test_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_udp_test_T RT_MODEL_udp_test_T;

#endif                                 /* RTW_HEADER_udp_test_types_h_ */
