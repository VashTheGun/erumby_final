/*
 * udp_test.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "udp_test".
 *
 * Model version              : 1.175
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C source code generated on : Thu Dec 19 14:44:33 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "udp_test.h"
#include "udp_test_private.h"
#include "udp_test_dt.h"

/* Block signals (default storage) */
B_udp_test_T udp_test_B;

/* Block states (default storage) */
DW_udp_test_T udp_test_DW;

/* Real-time model */
RT_MODEL_udp_test_T udp_test_M_;
RT_MODEL_udp_test_T *const udp_test_M = &udp_test_M_;
static void rate_monotonic_scheduler(void);

/*
 * Set which subrates need to run this base step (base rate always runs).
 * This function must be called prior to calling the model step function
 * in order to "remember" which rates need to run this base step.  The
 * buffering of events allows for overlapping preemption.
 */
void udp_test_SetEventsForThisBaseStep(boolean_T *eventFlags)
{
  /* Task runs when its counter is zero, computed via rtmStepTask macro */
  eventFlags[1] = ((boolean_T)rtmStepTask(udp_test_M, 1));
}

/*
 *   This function updates active task flag for each subrate
 * and rate transition flags for tasks that exchange data.
 * The function assumes rate-monotonic multitasking scheduler.
 * The function must be called at model base rate so that
 * the generated code self-manages all its subrates and rate
 * transition flags.
 */
static void rate_monotonic_scheduler(void)
{
  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (udp_test_M->Timing.TaskCounters.TID[1])++;
  if ((udp_test_M->Timing.TaskCounters.TID[1]) > 99) {/* Sample time: [1.0s, 0.0s] */
    udp_test_M->Timing.TaskCounters.TID[1] = 0;
  }
}

/* Model step function for TID0 */
void udp_test_step0(void)              /* Sample time: [0.01s, 0.0s] */
{
  {                                    /* Sample time: [0.01s, 0.0s] */
    rate_monotonic_scheduler();
  }

  /* Matfile logging */
  rt_UpdateTXYLogVars(udp_test_M->rtwLogInfo, (&udp_test_M->Timing.taskTime0));

  /* External mode */
  rtExtModeUploadCheckTrigger(2);
  rtExtModeUpload(0, (real_T)udp_test_M->Timing.taskTime0);

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.01s, 0.0s] */
    if ((rtmGetTFinal(udp_test_M)!=-1) &&
        !((rtmGetTFinal(udp_test_M)-udp_test_M->Timing.taskTime0) >
          udp_test_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(udp_test_M, "Simulation finished");
    }

    if (rtmGetStopRequested(udp_test_M)) {
      rtmSetErrorStatus(udp_test_M, "Simulation finished");
    }
  }

  /* Update absolute time */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  udp_test_M->Timing.taskTime0 =
    (++udp_test_M->Timing.clockTick0) * udp_test_M->Timing.stepSize0;
}

/* Model step function for TID1 */
void udp_test_step1(void)              /* Sample time: [1.0s, 0.0s] */
{
  char_T *sErr;
  int32_T samplesRead;

  /* S-Function (sdspFromNetwork): '<Root>/UDP Receive' */
  sErr = GetErrorBuffer(&udp_test_DW.UDPReceive_NetworkLib[0U]);
  samplesRead = 255;
  LibOutputs_Network(&udp_test_DW.UDPReceive_NetworkLib[0U],
                     &udp_test_B.UDPReceive[0U], &samplesRead);
  if (*sErr != 0) {
    rtmSetErrorStatus(udp_test_M, sErr);
    rtmSetStopRequested(udp_test_M, 1);
  }

  udp_test_DW.UDPReceive_DIMS1[0] = samplesRead;
  udp_test_DW.UDPReceive_DIMS1[1] = 1;

  /* End of S-Function (sdspFromNetwork): '<Root>/UDP Receive' */

  /* Update for S-Function (sdspToNetwork): '<Root>/UDP Send' incorporates:
   *  Constant: '<Root>/Constant'
   */
  sErr = GetErrorBuffer(&udp_test_DW.UDPSend_NetworkLib[0U]);
  LibUpdate_Network(&udp_test_DW.UDPSend_NetworkLib[0U],
                    &udp_test_P.Constant_Value, 1);
  if (*sErr != 0) {
    rtmSetErrorStatus(udp_test_M, sErr);
    rtmSetStopRequested(udp_test_M, 1);
  }

  /* End of Update for S-Function (sdspToNetwork): '<Root>/UDP Send' */
  rtExtModeUpload(1, (real_T)((udp_test_M->Timing.clockTick1) * 1.0));

  /* Update absolute time */
  /* The "clockTick1" counts the number of times the code of this task has
   * been executed. The resolution of this integer timer is 1.0, which is the step size
   * of the task. Size of "clockTick1" ensures timer will not overflow during the
   * application lifespan selected.
   */
  udp_test_M->Timing.clockTick1++;
}

/* Model step wrapper function for compatibility with a static main program */
void udp_test_step(int_T tid)
{
  switch (tid) {
   case 0 :
    udp_test_step0();
    break;

   case 1 :
    udp_test_step1();
    break;

   default :
    break;
  }
}

/* Model initialize function */
void udp_test_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)udp_test_M, 0,
                sizeof(RT_MODEL_udp_test_T));
  rtmSetTFinal(udp_test_M, -1);
  udp_test_M->Timing.stepSize0 = 0.01;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    udp_test_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(udp_test_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(udp_test_M->rtwLogInfo, (NULL));
    rtliSetLogT(udp_test_M->rtwLogInfo, "");
    rtliSetLogX(udp_test_M->rtwLogInfo, "");
    rtliSetLogXFinal(udp_test_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(udp_test_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(udp_test_M->rtwLogInfo, 4);
    rtliSetLogMaxRows(udp_test_M->rtwLogInfo, 0);
    rtliSetLogDecimation(udp_test_M->rtwLogInfo, 1);
    rtliSetLogY(udp_test_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(udp_test_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(udp_test_M->rtwLogInfo, (NULL));
  }

  /* External mode info */
  udp_test_M->Sizes.checksums[0] = (2761448563U);
  udp_test_M->Sizes.checksums[1] = (1564821486U);
  udp_test_M->Sizes.checksums[2] = (1490021302U);
  udp_test_M->Sizes.checksums[3] = (2835628535U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    udp_test_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(udp_test_M->extModeInfo,
      &udp_test_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(udp_test_M->extModeInfo, udp_test_M->Sizes.checksums);
    rteiSetTPtr(udp_test_M->extModeInfo, rtmGetTPtr(udp_test_M));
  }

  /* block I/O */
  (void) memset(((void *) &udp_test_B), 0,
                sizeof(B_udp_test_T));

  /* states (dwork) */
  (void) memset((void *)&udp_test_DW, 0,
                sizeof(DW_udp_test_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    udp_test_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(udp_test_M->rtwLogInfo, 0.0, rtmGetTFinal
    (udp_test_M), udp_test_M->Timing.stepSize0, (&rtmGetErrorStatus(udp_test_M)));

  {
    char_T *sErr;

    /* Start for S-Function (sdspToNetwork): '<Root>/UDP Send' */
    sErr = GetErrorBuffer(&udp_test_DW.UDPSend_NetworkLib[0U]);
    CreateUDPInterface(&udp_test_DW.UDPSend_NetworkLib[0U]);
    if (*sErr == 0) {
      LibCreate_Network(&udp_test_DW.UDPSend_NetworkLib[0U], 1,
                        "255.255.255.255", -1, "10.0.0.12",
                        udp_test_P.UDPSend_Port, 8192, 8, 0);
    }

    if (*sErr == 0) {
      LibStart(&udp_test_DW.UDPSend_NetworkLib[0U]);
    }

    if (*sErr != 0) {
      DestroyUDPInterface(&udp_test_DW.UDPSend_NetworkLib[0U]);
      if (*sErr != 0) {
        rtmSetErrorStatus(udp_test_M, sErr);
        rtmSetStopRequested(udp_test_M, 1);
      }
    }

    /* End of Start for S-Function (sdspToNetwork): '<Root>/UDP Send' */

    /* Start for S-Function (sdspFromNetwork): '<Root>/UDP Receive' */
    sErr = GetErrorBuffer(&udp_test_DW.UDPReceive_NetworkLib[0U]);
    CreateUDPInterface(&udp_test_DW.UDPReceive_NetworkLib[0U]);
    if (*sErr == 0) {
      LibCreate_Network(&udp_test_DW.UDPReceive_NetworkLib[0U], 0, "0.0.0.0",
                        udp_test_P.UDPReceive_localPort, "0.0.0.0", -1, 8192, 1,
                        0);
    }

    if (*sErr == 0) {
      LibStart(&udp_test_DW.UDPReceive_NetworkLib[0U]);
    }

    if (*sErr != 0) {
      DestroyUDPInterface(&udp_test_DW.UDPReceive_NetworkLib[0U]);
      if (*sErr != 0) {
        rtmSetErrorStatus(udp_test_M, sErr);
        rtmSetStopRequested(udp_test_M, 1);
      }
    }

    /* End of Start for S-Function (sdspFromNetwork): '<Root>/UDP Receive' */
  }
}

/* Model terminate function */
void udp_test_terminate(void)
{
  char_T *sErr;

  /* Terminate for S-Function (sdspToNetwork): '<Root>/UDP Send' */
  sErr = GetErrorBuffer(&udp_test_DW.UDPSend_NetworkLib[0U]);
  LibTerminate(&udp_test_DW.UDPSend_NetworkLib[0U]);
  if (*sErr != 0) {
    rtmSetErrorStatus(udp_test_M, sErr);
    rtmSetStopRequested(udp_test_M, 1);
  }

  LibDestroy(&udp_test_DW.UDPSend_NetworkLib[0U], 1);
  DestroyUDPInterface(&udp_test_DW.UDPSend_NetworkLib[0U]);

  /* End of Terminate for S-Function (sdspToNetwork): '<Root>/UDP Send' */

  /* Terminate for S-Function (sdspFromNetwork): '<Root>/UDP Receive' */
  sErr = GetErrorBuffer(&udp_test_DW.UDPReceive_NetworkLib[0U]);
  LibTerminate(&udp_test_DW.UDPReceive_NetworkLib[0U]);
  if (*sErr != 0) {
    rtmSetErrorStatus(udp_test_M, sErr);
    rtmSetStopRequested(udp_test_M, 1);
  }

  LibDestroy(&udp_test_DW.UDPReceive_NetworkLib[0U], 0);
  DestroyUDPInterface(&udp_test_DW.UDPReceive_NetworkLib[0U]);

  /* End of Terminate for S-Function (sdspFromNetwork): '<Root>/UDP Receive' */
}
