/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: master_code_V0_data.c
 *
 * Code generated for Simulink model 'master_code_V0'.
 *
 * Model version                  : 1.118
 * Simulink Coder version         : 9.1 (R2019a) 23-Nov-2018
 * C/C++ source code generated on : Wed Nov 20 14:30:57 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "master_code_V0.h"
#include "master_code_V0_private.h"

/* Block parameters (default storage) */
P_master_code_V0_T master_code_V0_P = {
  /* Variable: DUTY_ESC_MAX
   * Referenced by: '<Root>/Saturation'
   */
  8412.0,

  /* Variable: DUTY_ESC_MIN
   * Referenced by: '<Root>/Saturation'
   */
  5608.0,

  /* Variable: DUTY_SERVO_MIDDLE
   * Referenced by: '<Root>/steer'
   */
  6881.0,

  /* Expression: -1
   * Referenced by: '<S2>/I2C Master Read5'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S2>/I2C Master Read4'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S2>/I2C Master Read3'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S2>/I2C Master Read2'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S2>/I2C Master Read1'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S2>/I2C Master Read'
   */
  -1.0,

  /* Expression: 5
   * Referenced by: '<Root>/pwm'
   */
  5.0,

  /* Expression: 7010
   * Referenced by: '<Root>/pwm'
   */
  7010.0,

  /* Expression: 7100
   * Referenced by: '<Root>/pwm'
   */
  7100.0,

  /* Expression: -1
   * Referenced by: '<Root>/Conversion for  open loop ctr'
   */
  -1.0,

  /* Computed Parameter: acc_x_calib_Y0
   * Referenced by: '<S2>/acc_x_calib'
   */
  0,

  /* Computed Parameter: acc_y_calib_Y0
   * Referenced by: '<S2>/acc_y_calib'
   */
  0,

  /* Computed Parameter: acc_z_calib_Y0
   * Referenced by: '<S2>/acc_z_calib'
   */
  0,

  /* Computed Parameter: gyro_x_calib_Y0
   * Referenced by: '<S2>/gyro_x_calib'
   */
  0,

  /* Computed Parameter: gyro_y_calib_Y0
   * Referenced by: '<S2>/gyro_y_calib'
   */
  0,

  /* Computed Parameter: gyro_z_calib_Y0
   * Referenced by: '<S2>/gyro_z_calib'
   */
  0,

  /* Computed Parameter: Gain4_Gain
   * Referenced by: '<Root>/Gain4'
   */
  18641,

  /* Computed Parameter: Gain3_Gain
   * Referenced by: '<Root>/Gain3'
   */
  16384,

  /* Computed Parameter: Gain1_Gain
   * Referenced by: '<Root>/Gain1'
   */
  16384,

  /* Start of '<S1>/i2cRd' */
  {
    /* Expression: -1
     * Referenced by: '<S4>/I2C Master Read6'
     */
    -1.0,

    /* Expression: -1
     * Referenced by: '<S4>/I2C Master Read1'
     */
    -1.0,

    /* Expression: -1
     * Referenced by: '<S4>/I2C Master Read4'
     */
    -1.0,

    /* Computed Parameter: data_imu_Y0
     * Referenced by: '<S4>/data_imu'
     */
    0,

    /* Computed Parameter: data_enc_rear_Y0
     * Referenced by: '<S4>/data_enc_rear'
     */
    0,

    /* Computed Parameter: data_enc_front_Y0
     * Referenced by: '<S4>/data_enc_front'
     */
    0
  }
  /* End of '<S1>/i2cRd' */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
