#define _USE_MATH_DEFINES
#define S_FUNCTION_LEVEL 2
#define S_FUNCTION_NAME clothoid_shooting_opti_g2
#include "simstruc.h"

#include <Clothoid.hh>
#include <cstddef>
#include <vector>

// Confgiurations
#define ROWS 5                                  // L dimension
#define COLUMN 3                                // epsilon dimension


// cast per prendere la clotoide nel pwork
inline G2lib::ClothoidList *get_clothoid_list(SimStruct *S, int i) {
    return static_cast< G2lib::ClothoidList * >(ssGetPWorkValue(S, i));
}



/* SELECT_CLOTHOID
 * funzione che seleziona la traiettoria a lunghezza minima
 *
 * la funzione genera diverse traiettorie a grafo tra un punto di partenza ed un punto di arrivo (piu punti sulla
 * normale) scegliendo quella a minima distanza, generando n_eps^nL_prev possibili traiettorie.
 *
 * * --------------------------------------------------------------------------- (roadway)
 *                                           4 o            9 o             o     
 *                                                          | eps         
 *                                           3 o            8 o             o     
 *                                                   L
 *          +(posa attuale)                  2 o  <-------> 7 o             o 
 *
 *                                           1 o            6 o             o
 *
 *                                           0 o            5 o             o
 * ----------------------------------------------------------------------------
 *
 *
 * output: index_n ->indice dei punti che costruiscono la traiettoria migliore secondo la convensione
 *                   scelta
 * input:
 *        x_in, y_in, theta_in ->coordinate e posa del punto di partenza
 *        x_targ, y_targ, theta_targ ->coordinate e posa dei punti per la costruzione del grafo
*/ 
inline int select_path(int &index_1, int &index_2, int &index_3, int &index_4, int &index_5, 
                       const real_T x_in, const real_T y_in, const real_T theta_in, const real_T *x_targ, 
                       const real_T *y_targ, const real_T *theta_targ, const real_T *u) {
    
    int path_dimension = pow(COLUMN,ROWS);      // total number of possible path
    int n_node = COLUMN;                        // number of node having the same normal abscisa
    G2lib::ClothoidList path[path_dimension];   // initialize the vector of clothoids 
    int count = 0;                              // init the counter
    double ax;
    double k_tmp;
    double tmp1, tmp2, tmp3;
    int max_acc = 8;
    
    // loop over the total number of node in s direction (L_prev dimension)
    for (int i = 0; i < n_node; i++) {        
        for (int ii = n_node; ii < n_node*2; ii++) {
            for (int iii = n_node*2; iii < n_node*3; iii++) {
                for (int iiii = n_node*3; iiii < n_node*4; iiii++) {
                    for (int iiiii = n_node*4; iiiii < n_node*5; iiiii++) {
                                // creation of all possible trajecory
                                path[count].push_back_G1(x_in, y_in, theta_in, x_targ[i], y_targ[i], theta_targ[i]);
                                path[count].push_back_G1(x_targ[i], y_targ[i], theta_targ[i], x_targ[ii], y_targ[ii], theta_targ[ii]);
                                path[count].push_back_G1(x_targ[ii], y_targ[ii], theta_targ[ii], x_targ[iii], y_targ[iii], theta_targ[iii]);
                                path[count].push_back_G1(x_targ[iii], y_targ[iii], theta_targ[iii], x_targ[iiii], y_targ[iiii], theta_targ[iiii]);
                                path[count].push_back_G1(x_targ[iiii], y_targ[iiii], theta_targ[iiii], x_targ[iiiii], y_targ[iiiii], theta_targ[iiii]);
                                count ++;   
                    }
                }
            } 
        }
    }
    
    // reset the counter and init another
    count = 0;
    int count_old = 0;
    
    // loop over all trajectory for find the optima
    for (int i = 0; i < n_node; i++) {       
        for (int ii = n_node; ii < n_node*2; ii++) {
            for (int iii = n_node*2; iii < n_node*3; iii++) {
                for (int iiii = n_node*3; iiii < n_node*4; iiii++) {
                    for (int iiiii = n_node*4; iiiii < n_node*5; iiiii++) {     
                        double len = path[count].sEnd(); 
                        path[count].eval(len/1.5, tmp1, k_tmp, tmp2, tmp3);
                        ax = std::abs(k_tmp*pow(*u,2));
                        // optima criteria
                        if( (path[count_old].sEnd() >= path[count].sEnd()) && (ax < max_acc) ) {

                            // save index for the candidate
                            count_old = count;
                            index_1 = i;
                            index_2 = ii;
                            index_3 = iii;
                            index_4 = iiii;
                            index_5 = iiiii;
                        }
                        count ++;      
                    }
                }
            } 
        }
    }
    return 1;
}

#define MDL_INITIAL_SIZES
static void mdlInitializeSizes(SimStruct *S) {
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    // Setting input ports
    if (!ssSetNumInputPorts(S, 10))
        return;
    ssSetInputPortWidth(S, 0, ROWS*COLUMN);  // Porta 1: X_targ : ROWS*COLUMN
    ssSetInputPortWidth(S, 1, ROWS*COLUMN);  // Porta 2: Y_targ : ROWS*COLUMN
    ssSetInputPortWidth(S, 2, ROWS*COLUMN);  // Porta 2: theta_targ : ROWS*COLUMN
    ssSetInputPortWidth(S, 3, 1);   // Porta 2: x_in : 1x1
    ssSetInputPortWidth(S, 4, 1);   // Porta 2: y_in : 1x1
    ssSetInputPortWidth(S, 5, 1);   // Porta 2: theta_in : 1x1
    ssSetInputPortWidth(S, 6, 1);   // flag
    ssSetInputPortWidth(S, 7, 1);   // u
    ssSetInputPortWidth(S, 8, ROWS*COLUMN);   // k_in
    ssSetInputPortWidth(S, 9, 1);   // k_targ

    for (int_T i = 0; i < 10; i++) {
        ssSetInputPortDirectFeedThrough(S, i, 1);
        ssSetInputPortSampleTime(S, i, CONTINUOUS_SAMPLE_TIME);
        ssSetInputPortRequiredContiguous(S, i, 1);
        ssSetInputPortOffsetTime(S, i, 0.0);
    }

  // Setting Output port
    if (!ssSetNumOutputPorts(S, 3))
        return;
    ssSetOutputPortWidth(S, 0, 1);  // Porta 1: k(s) clotoide : 1x1
    ssSetOutputPortWidth(S, 1, 1);  // Porta 2: n(s) clotoide : 1x1
    ssSetOutputPortWidth(S, 2, 1);  // Porta 2: thets(s) clotoide : 1x1
    for (int_T i = 0; i < 3; i++) {
        ssSetOutputPortSampleTime(S, i, CONTINUOUS_SAMPLE_TIME);
        ssSetOutputPortOffsetTime(S, i, 0.0);
    }

    // Setting sample time
    ssSetNumSampleTimes(S, 1);

    ssSetNumPWork(S, 1);  // reserve element in the pointers vector

    ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);
}

static void mdlInitializeSampleTimes(SimStruct *S) {
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    ssSetModelReferenceSampleTimeDefaultInheritance(S);
}

static void mdlOutputs(SimStruct *S, int_T tid) {
    // init clothoid
    G2lib::ClothoidList *current_traj = get_clothoid_list(S, 0);
    // Attaching output port
    real_T *k_out = ssGetOutputPortRealSignal(S, 0);
    real_T *n_out = ssGetOutputPortRealSignal(S, 1);
    real_T *theta_out = ssGetOutputPortRealSignal(S, 2);

    // Attaching input port
    const real_T *x_targ = ssGetInputPortRealSignal(S, 0);
    const real_T *y_targ = ssGetInputPortRealSignal(S, 1);
    const real_T *theta_targ = ssGetInputPortRealSignal(S, 2);
    const real_T *x_in = ssGetInputPortRealSignal(S, 3);
    const real_T *y_in = ssGetInputPortRealSignal(S, 4);
    const real_T *theta_in = ssGetInputPortRealSignal(S, 5);
    const real_T *flag = ssGetInputPortRealSignal(S, 6);
    const real_T *u = ssGetInputPortRealSignal(S, 7);
    const real_T *k_targ = ssGetInputPortRealSignal(S, 8);
    const real_T *k_in = ssGetInputPortRealSignal(S, 9);

    // Output values
    int check;
    int index_1 = 0;
    int index_2 = 0;
    int index_3 = 0;
    int index_4 = 0;
    int index_5 = 0;  

    G2lib::real_type s_, t_, x_, y_, k_, theta_;

    G2lib::real_type s, t;

    if (flag[0] > 0) {  // flag for enabling computation of trajactory

        current_traj->init();
        current_traj->reserve(3*5);
        G2lib::G2solve3arc g2sol_1, g2sol_2, g2sol_3, g2sol_4, g2sol_5 ;
        
        // retrive the index of optimal path
        int check = select_path(index_1, index_2, index_3, index_4, index_5, x_in[0], y_in[0], 
                                      theta_in[0], x_targ, y_targ, theta_targ, u);

        int iter1 = g2sol_1.build(x_in[0], y_in[0], theta_in[0], k_in[0], 
                                 x_targ[index_1], y_targ[index_1], theta_targ[index_1], k_targ[index_1]); 
        ssPrintf("k = %f\n");
        int iter2 = g2sol_2.build( x_targ[index_1], y_targ[index_1], theta_targ[index_1], k_targ[index_1],
                                  x_targ[index_2], y_targ[index_2], theta_targ[index_2], k_targ[index_2]);
        int iter3 = g2sol_3.build( x_targ[index_2], y_targ[index_2], theta_targ[index_2], k_targ[index_2],
                                  x_targ[index_3], y_targ[index_3], theta_targ[index_3], k_targ[index_3]);
        int iter4 = g2sol_4.build( x_targ[index_3], y_targ[index_3], theta_targ[index_3], k_targ[index_3],
                                  x_targ[index_4], y_targ[index_4], theta_targ[index_4], k_targ[index_4]);
        int iter5 = g2sol_5.build( x_targ[index_4], y_targ[index_4], theta_targ[index_4], k_targ[index_4], 
                                    x_targ[index_5], y_targ[index_5], theta_targ[index_5], k_targ[index_5]);
        
        current_traj->push_back(g2sol_1.getS0());
        current_traj->push_back(g2sol_1.getSM());
        current_traj->push_back(g2sol_1.getS1());
        current_traj->push_back(g2sol_2.getS0());
        current_traj->push_back(g2sol_2.getSM());
        current_traj->push_back(g2sol_2.getS1());
        current_traj->push_back(g2sol_3.getS0());
        current_traj->push_back(g2sol_3.getSM());
        current_traj->push_back(g2sol_3.getS1());
        current_traj->push_back(g2sol_4.getS0());
        current_traj->push_back(g2sol_4.getSM());
        current_traj->push_back(g2sol_4.getS1());
        current_traj->push_back(g2sol_5.getS0());
        current_traj->push_back(g2sol_5.getSM());
        current_traj->push_back(g2sol_5.getS1());

    }
    
    // retrive the rho for our position
    current_traj->findST(x_in[0], y_in[0], s_, t_);
    current_traj->eval((s_ <= 0 ? 0 : s_), theta_, k_, x_, y_);

    // output
    k_out[0] = k_;
    n_out[0] = t_;
    theta_out[0] = theta_;

    UNUSED_ARG(tid);
}

#define MDL_START
static void mdlStart(SimStruct *S) {

  G2lib::ClothoidList *current_traj = new G2lib::ClothoidList();
  
  ssSetPWorkValue(S, 0, static_cast< void * >(current_traj));
  
}

#define MDL_TERMINATE
static void mdlTerminate(SimStruct *S) {
  G2lib::ClothoidList *current_traj = get_clothoid_list(S, 0);
  delete current_traj;
}

#ifdef MATLAB_MEX_FILE /* Is this file being compiled as a MEX-file? */
#include "simulink.c"  /* MEX-file interface mechanism */
#else
#include "cg_sfun.h" /* Code generation registration function */
#endif