
%% optimal control comparison
% clear all;clc;

addpath(genpath('../s-function'));
addpath(genpath('../lib'))
addpath('../../../test_session/optimal_control')
 optimal = importdata('Pergine_track_v_max_2m_s.mat');
% optimal = importdata('Pergine_track_v_max_3m_s.mat');

zeta = optimal.zeta(1:5:end);
u = optimal.u(1:5:end);
%% 
M_PI = pi;
% track parameters
x = [4, 0, 0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4];
y = [6, 6, 0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6];
theta = [M_PI, M_PI, 0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI];
S = ClothoidList() ;


for i = 1:length(x)-1
    S.push_back_G1(x(i), y(i), theta(i), x(i+1), y(i+1), theta(i+1) ) ; % track creation
end

s = [0:0.1:S.sEnd];
eps = 0.3;

for i = 1:length(s)
   [x1(i), y1(i),theta1(i), k(i)] = S.evaluate(s(i));  
   xplus(i) = x1(i) - eps*sin(theta1(i));
   yplus(i) = y1(i) + eps*cos(theta1(i));
   
   xmin(i) = x1(i) + eps*sin(theta1(i));
   ymin(i) = y1(i) - eps*cos(theta1(i));
end


%%
figure()
hold on
plot(xplus,yplus,'Color','black')
plot(xmin,ymin,'Color','black')
 plot(optimal.xCoMCar, optimal.yCoMCar)
plot(x_sim,y_sim)