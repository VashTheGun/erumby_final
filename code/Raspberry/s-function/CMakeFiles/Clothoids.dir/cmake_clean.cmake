file(REMOVE_RECURSE
  "CMakeFiles/Clothoids.dir/src/Biarc.cc.obj"
  "CMakeFiles/Clothoids.dir/src/Circle.cc.obj"
  "CMakeFiles/Clothoids.dir/src/Clothoid.cc.obj"
  "CMakeFiles/Clothoids.dir/src/ClothoidAsyPlot.cc.obj"
  "CMakeFiles/Clothoids.dir/src/ClothoidDistance.cc.obj"
  "CMakeFiles/Clothoids.dir/src/ClothoidG2.cc.obj"
  "CMakeFiles/Clothoids.dir/src/ClothoidList.cc.obj"
  "CMakeFiles/Clothoids.dir/src/CubicRootsFlocke.cc.obj"
  "CMakeFiles/Clothoids.dir/src/Fresnel.cc.obj"
  "CMakeFiles/Clothoids.dir/src/G2lib.cc.obj"
  "CMakeFiles/Clothoids.dir/src/Line.cc.obj"
  "CMakeFiles/Clothoids.dir/src/Triangle2D.cc.obj"
  "libClothoids.pdb"
  "libClothoids.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/Clothoids.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
