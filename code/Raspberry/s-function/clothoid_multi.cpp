#define _USE_MATH_DEFINES
#define S_FUNCTION_LEVEL 2
#define S_FUNCTION_NAME  clothoid_multi
#include "simstruc.h"

#include <Clothoid.hh>

#define IDX(r, c, column)    ( c + r *column)

// config
#define ROWS 5                                 // L dimension
#define COLUMN 3                                // epsilon dimension
#define L_prev {1, 1.5, 2.5, 3.5, 4.5}
#define epsilon {-0.2, 0, 0.2}

// cast per prendere la clotoide nel pwork
inline G2lib::ClothoidList* get_clothoid_list(SimStruct *S) {
  return static_cast<G2lib::ClothoidList*>(ssGetPWorkValue(S, 0));
}


/* GET PREVIEW
 * funzione che rispetto alla posa del veicolo attuale ottiene diverse pose (su n) che il veicolo 
 * dovrebbe avere lo stesso rispetto a diverse lunghezze (su s) nul percorso.
 *
 *
 * --------------------------------------------------------------------------- (roadway)
 *                                            -             -     
 *                                                          | eps
 *                                            -             -     
 *                                                   L
 *          +(posa attuale)                   -  <------->  - (pose di target)
 *
 *                                            -             -
 *
 *                                            -             -
 * ----------------------------------------------------------------------------
 *
 *
 * output: x_out, y_out, theta_out, k_out ->pose dei punti di target
 *
 * input: centerline ->il centro strada 
 *        s_coord -> coordinata s della posa attuale rispetto alla quale vengono calcolati i punti di target
 *        end_of_road -> valore di s massimo del circuito
 *
 */ 
inline get_preview(real_T *x_out, real_T *y_out, real_T *k_out, real_T *theta_out,
                   const G2lib::ClothoidList *centerline, const real_T s_coord,
                   const real_T end_of_road) {
    
    static const int n_rows = ROWS;
    static const int n_column = COLUMN; 
    static const real_T eps[] = epsilon;
    static const double L[] = L_prev;
    
    G2lib::real_type x_, y_, k_, theta_;
    
    for (int i = 0; i < n_rows; i++) {
        for (int j = 0; j < n_column; j++){
            centerline->eval(s_coord + L[i]- end_of_road, theta_, k_, x_, y_);
            theta_out[IDX(i, j, n_column)] = theta_;
            k_out[IDX(i, j, n_column)] = k_;
            x_out[IDX(i, j, n_column)] = x_ - eps[j]*sin(theta_);
            y_out[IDX(i, j, n_column)] = y_ + eps[j]*cos(theta_);
        }
    }
}

// ovvia al problema di fine strada per ora
inline get_preview_mod(real_T *x_out, real_T *y_out, real_T *k_out, real_T *theta_out,
                   const G2lib::ClothoidList *centerline, const real_T s_coord,
                   const real_T end_of_road) {
    
    static const int n_rows = ROWS;
    static const int n_column = COLUMN; 
    static const real_T eps[] = epsilon;
    static const double L[] = L_prev;
    
    G2lib::real_type x_, y_, k_, theta_;
    
    for (int i = 0; i < n_rows; i++) {
        for (int j = 0; j < n_column; j++){
            centerline->eval(s_coord + L[i] - end_of_road, theta_, k_, x_, y_);
            theta_out[IDX(i, j, n_column)] = theta_;
            k_out[IDX(i, j, n_column)] = k_;
            x_out[IDX(i, j, n_column)] = x_ - eps[j]*sin(theta_);
            y_out[IDX(i, j, n_column)] = y_ + eps[j]*cos(theta_);
        }
    }
}
       

#define MDL_INITIAL_SIZES
static void mdlInitializeSizes(SimStruct *S) {
  ssSetNumContStates(S, 0);
  ssSetNumDiscStates(S, 0);
  
  // Setting input ports
  if (!ssSetNumInputPorts(S, 2)) return;
  ssSetInputPortWidth(S, 0, 1); // Porta 1: X : 1x1
  ssSetInputPortWidth(S, 1, 1); // Porta 2: Y : 1x1
  
  for(int_T i = 0; i < 2; i++) {
    ssSetInputPortDirectFeedThrough(S, i, 1);
    ssSetInputPortSampleTime(S, i, CONTINUOUS_SAMPLE_TIME);
    ssSetInputPortRequiredContiguous(S, i, 1);
    ssSetInputPortOffsetTime(S, i, 0.0);
  }
  
  // Setting Output port
  if (!ssSetNumOutputPorts(S, 5)) return;
  ssSetOutputPortWidth(S, 0, ROWS*COLUMN); // Porta 1: x clotoide : 1x1 
  ssSetOutputPortWidth(S, 1, ROWS*COLUMN); // Porta 2: y clotoide : 1x1
  ssSetOutputPortWidth(S, 2, ROWS*COLUMN); // Porta 3: theta clotoide : 1x1
  ssSetOutputPortWidth(S, 3, ROWS*COLUMN); // Porta 3: k clotoide : 1x1
  ssSetOutputPortWidth(S, 4, 1); // Porta 4: s clotoide : 1x1
  for(int_T i = 0; i < 5; i++) {
    ssSetOutputPortSampleTime(S, i, CONTINUOUS_SAMPLE_TIME);
    ssSetOutputPortOffsetTime(S, i, 0.0);
  }
  
  // Setting sample time
  ssSetNumSampleTimes(S,  1);
  
  ssSetNumPWork(S, 1); // reserve element in the pointers vector
  
  ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);
  //ssSetOptions(S, SS_OPTION_CALL_TERMINATE_ON_EXIT);
}

static void mdlInitializeSampleTimes(SimStruct *S) {
  ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
  ssSetOffsetTime(S, 0, 0.0);
  ssSetModelReferenceSampleTimeDefaultInheritance(S);
}

static void mdlOutputs(SimStruct *S, int_T tid) {
    
  G2lib::ClothoidList *centerline = get_clothoid_list(S);
  
  // Attaching output port
  real_T *x_out = ssGetOutputPortRealSignal(S, 0);
  real_T *y_out = ssGetOutputPortRealSignal(S, 1);
  real_T *theta_out = ssGetOutputPortRealSignal(S, 2);
  real_T *k_out = ssGetOutputPortRealSignal(S, 3);
  real_T *s_out = ssGetOutputPortRealSignal(S, 4);
  
  // Attaching input port
  const real_T * x = ssGetInputPortRealSignal(S, 0);
  const real_T * y = ssGetInputPortRealSignal(S, 1);

  // Output void
  
  G2lib::real_type s, t;
  
  centerline->findST(x[0], y[0], s, t);
  const real_T end_of_road = centerline->sEnd();
  static const double L[] = L_prev; 
  
  if( ( s+L[0] ) >= (end_of_road)){
      get_preview_mod(x_out, y_out, k_out, theta_out, centerline, s, end_of_road);  
  }
  else{
      get_preview(x_out, y_out, k_out, theta_out, centerline, s, 0);
  }
  s_out[0] = s;

  
  // Required for unused arguments
  UNUSED_ARG(tid);
}   

#define MDL_START
static void mdlStart(SimStruct *S) {
    

  // second track  
  const real_T x[] = {4, 0, 0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4};
  const real_T y[] = {6, 6, 0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6};
  const real_T theta[] = {M_PI, M_PI, 0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI};

  G2lib::ClothoidList * centerline = new G2lib::ClothoidList();
  
  const int n_clothoids =  sizeof(x)/ sizeof(x[0]) - 1;
  
  for (int i = 0; i < n_clothoids; i++) {
     centerline->push_back_G1(x[i], y[i], theta[i],x[i+1], y[i+1], theta[i+1]); 
  }
  ssSetPWorkValue(S, 0, static_cast<void *>(centerline));
}

#define MDL_TERMINATE
static void mdlTerminate(SimStruct *S) {
  G2lib::ClothoidList * centerline = get_clothoid_list(S);
  delete centerline;
} 

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif