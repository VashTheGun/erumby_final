%% utilizzare il comando per generare il mex del cpp che si desidera.

mex(['-I','lib/include'],['-L','lib'],['-L', '.'], ['-l','Clothoids'], 'clothoid.cpp');

mex(['-I','lib/include'],['-L','lib'],['-L', '.'], ['-l','Clothoids'], 'clothoid_traj.cpp');

mex(['-I','lib/include'],['-L','lib'],['-L', '.'], ['-l','Clothoids'], 'clothoid_traj_g1.cpp');

mex(['-I','lib/include'],['-L','lib'],['-L', '.'], ['-l','Clothoids'], 'clothoid_multi.cpp');

mex(['-I','lib/include'],['-L','lib'],['-L', '.'], ['-l','Clothoids'], 'clothoid_traj_g1_multi.cpp');

mex(['-I','lib/include'],['-L','lib'],['-L', '.'], ['-l','Clothoids'], 'clothoid_multi_parallel.cpp');

mex(['-I','lib/include'],['-L','lib'],['-L', '.'], ['-l','Clothoids'], 'clothoid_traj_g1_multi_parallel.cpp');

mex(['-I','lib/include'],['-L','lib'],['-L', '.'], ['-l','Clothoids'], 'clothoid_multi_parallel_preview.cpp');

mex(['-I','lib/include'],['-L','lib'],['-L', '.'], ['-l','Clothoids'], 'clothoid_traj_g1_optimal.cpp');