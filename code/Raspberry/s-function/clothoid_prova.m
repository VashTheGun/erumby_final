classdef (StrictDefaults)clothoid_prova < matlab.System & matlab.system.mixin.Propagates
%slexVarSizeMATLABSystemFindIfSysObj Find input elements that satisfy the given condition.
%     The condition is specified in the block dialog. Both outputs 
%     are variable-sized vectors.
%

%#codegen
% Copyright 2015 The MathWorks, Inc.

properties (Nontunable)
 
end

properties (Access = private)
   % The only property is the associated primitive
%    S = [];
%    for i = 1:10
   S =  ClothoidList()
%    end
   tmpo = ClothoidList();
end

properties (Constant, Hidden)

end

  % methods
  %   function this = slexVarSizeMATLABSystemFindIfSysObj(varargin)
  %       setProperties(this, nargin, varargin{:});
  %   end
  % end
  
  methods(Access=protected)
    
      function validatePropertiesImpl(obj)

      end

      function setupImpl(obj) % init fnct
          
        

      end

      function [ output ] = stepImpl(obj, x, y )
          
          print(obj.S);
          output = [x, y, 1];         
      end
    
    % function num = getNumOutputsImpl(~)
    %     num = 2;
    % end

   function [sz_1] = getOutputSizeImpl(obj) 
      sz_1 = [1 3]; % TEMPORARY !!! 
   end
    
    function [fz1] = isOutputFixedSizeImpl(~)
      %Both outputs are always variable-sized
      fz1 = true; % TEMPORARY !!! 
    end
    
    function [dt1] = getOutputDataTypeImpl(obj)
        dt1 = 'double';
    end
    
    function [cp1] = isOutputComplexImpl(obj)
        cp1 = false; % Linear indices are always real value
    end
    
  end
  
end