/*
 * master_code_V2_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "master_code_V2".
 *
 * Model version              : 1.140
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C source code generated on : Wed Dec 18 15:51:01 2019
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_master_code_V2_types_h_
#define RTW_HEADER_master_code_V2_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"

/* Custom Type definition for MATLABSystem: '<S7>/I2C Master Write' */
#include "MW_SVD.h"
#ifndef typedef_d_codertarget_raspi_internal__T
#define typedef_d_codertarget_raspi_internal__T

typedef struct {
  int32_T __dummy;
} d_codertarget_raspi_internal__T;

#endif                               /*typedef_d_codertarget_raspi_internal__T*/

#ifndef typedef_codertarget_raspi_internal_I2_T
#define typedef_codertarget_raspi_internal_I2_T

typedef struct {
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  d_codertarget_raspi_internal__T Hw;
  uint32_T BusSpeed;
  MW_Handle_Type MW_I2C_HANDLE;
} codertarget_raspi_internal_I2_T;

#endif                               /*typedef_codertarget_raspi_internal_I2_T*/

#ifndef typedef_codertarget_raspi__nparbhma5g_T
#define typedef_codertarget_raspi__nparbhma5g_T

typedef struct {
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  d_codertarget_raspi_internal__T Hw;
  uint32_T BusSpeed;
  MW_Handle_Type MW_I2C_HANDLE;
  real_T SampleTime;
} codertarget_raspi__nparbhma5g_T;

#endif                               /*typedef_codertarget_raspi__nparbhma5g_T*/

/* Parameters for system: '<S1>/i2cRd' */
typedef struct P_i2cRd_master_code_V2_T_ P_i2cRd_master_code_V2_T;

/* Parameters for system: '<S1>/gpsRd' */
typedef struct P_gpsRd_master_code_V2_T_ P_gpsRd_master_code_V2_T;

/* Parameters (default storage) */
typedef struct P_master_code_V2_T_ P_master_code_V2_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_master_code_V2_T RT_MODEL_master_code_V2_T;

#endif                                 /* RTW_HEADER_master_code_V2_types_h_ */
