/*
 * rtmodel.h
 *
 * Code generation for Simulink model "master_code_V3".
 *
 * Simulink Coder version                : 9.1 (R2019a) 23-Nov-2018
 * C source code generated on : Thu Dec 19 14:52:16 2019
 *
 * Note that the generated code is not dependent on this header file.
 * The file is used in cojuction with the automatic build procedure.
 * It is included by the sample main executable harness
 * MATLAB/rtw/c/src/common/rt_main.c.
 *
 */

#ifndef RTW_HEADER_rtmodel_h_
#define RTW_HEADER_rtmodel_h_
#include "master_code_V3.h"
#endif                                 /* RTW_HEADER_rtmodel_h_ */
