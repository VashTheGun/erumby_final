%% init

clear all;clc;

addpath(genpath('Clothoids'));
addpath(genpath('../lib'))
addpath(genpath('../../test_session'));
addpath('../../test_session/optimal_control')
optimal = importdata('ocp_sol_V_const.mat');
optimal.time = importdata('time.mat');
optimal.theta = importdata('theta.mat');

%%
clear S1
S1 = ClothoidList();

for i = 1:3:length(optimal.xCoMCar)-3
    S1.push_back_G1(optimal.xCoMCar(i), optimal.yCoMCar(i), optimal.theta(i), optimal.xCoMCar(i+3), optimal.yCoMCar(i+3), optimal.theta(i+3) ) ; % track creation
end  
%%


%% load your test

erumby_t = telemetry_import_V2('../../test_session/18_10_19/telemetry_10_19/test1.txt');
erumby_o = optitrack_import('../../test_session/18_10_19/optitrack_10_19/test1.csv');
[erumby_tel, erumby_opt] = test_data_remap_V2(erumby_t, erumby_o);

clear erumby_t erumby_o
for i = 1:30500 
    [tmp,clothoid_err(i)] = S1.find_coord( erumby_opt.x_gps(i), erumby_opt.y_gps(i));
end
%%

M_PI = pi;
offset = 0.3;
 
%  % 4th track
%  x = [4, 0, 0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4];
%  y = [6, 6, 0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6];
%  theta = [M_PI, M_PI, 0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI];
 
  % 4th track
 x = [ 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0, 2, 4, 6, 7, 7];
 y = [2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0, 1, 1, 0, 0,2];
 theta = [ M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI, M_PI, 0, 0, 0, 0, 0, M_PI];



S = ClothoidList() ;



for i = 1:length(x)-1
    S.push_back_G1(x(i), y(i), theta(i), x(i+1), y(i+1), theta(i+1) ) ; % track creation
end
s = [0:0.1:S.length];
for i = 1:length(s)
   [x1(i), y1(i),theta1(i), k(i)] = S.evaluate(s(i));  
   xplus(i) = x1(i) - 0.3*sin(theta1(i));
   yplus(i) = y1(i) + 0.3*cos(theta1(i));
   
   xmin(i) = x1(i) + 0.3*sin(theta1(i));
   ymin(i) = y1(i) - 0.3*cos(theta1(i));
end

Lf = 0.172;              % front wheel base
Lr = 0.153;              % rear wheel base

erumby_tel.delta =   -((Lf+Lr)*(0.0404 + 0.0170*erumby_tel.steering));  

erumby_tel.S_coord = S.find_coord(erumby_opt.x_gps(1:30500),erumby_opt.y_gps(1:30500));
optimal.S_coord =  S.find_coord(optimal.xCoMCar, optimal.yCoMCar);
S_coord_sim = S.find_coord(x_sim,y_sim);
a_y_sim = yaw_rate_sim.*u_sim;

%%
x0=10;y0=20;
width=550;
height = 400;

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
hold on
grid on
% plot(x_sim,y_sim,'LineWidth',2)
plot(optimal.xCoMCar, optimal.yCoMCar,'LineWidth',2)
plot(erumby_opt.x_gps(1:30500),erumby_opt.y_gps(1:30500),'LineWidth',2)
plot(xplus,yplus,'k--')
plot(xmin,ymin,'k--')
axis equal
title('Trajectory')
ylabel('Y [m]')
xlabel('X [m]')
legend('simulation','optimal control','experiment')
 
figure()
set(gcf,'units','points','position',[x0,y0,width,height])
subplot(1,2,1)
hold on
grid on
xlim([0 52])
plot(S_coord_sim(26:2383), delta_sim(26:2383))
plot(optimal.S_coord(1:1182), optimal.delta(1:1182))
plot(erumby_tel.S_coord(7824:30416), erumby_tel.delta(7824:30416))
title('Steering')

xlabel('s coordinate [m]')
set(gca,'FontSize',20)
ylb = ylabel('\delta [rad]')
xlb = xlabel('s coordinate [m]')
xlb.FontSize = 20;
ylb.FontSize = 20;
legend('simulation','optimal control','experiment')

subplot(1,2,2)
hold on
grid on
xlim([0 52])
plot(S_coord_sim(26:2383), a_y_sim(26:2383))
plot(optimal.S_coord(1:1182), optimal.ay_SS(1:1182))
plot(erumby_tel.S_coord(7824:30416), erumby_tel.a_y(7824:30416))
title('Lateral acceleration')
set(gca,'FontSize',20)
ylb = ylabel('a_y [m/s^2]')
xlb = xlabel('s coordinate [m]')
xlb.FontSize = 20;
ylb.FontSize = 20;
legend('simulation','optimal control','experiment')

