% steering controller comparison

clear all;clc;

addpath(genpath('../s-function'));
addpath(genpath('../lib'))
addpath(genpath('../../test_session'));

%% load your test
ts = 0.001;
max_time = 24/ts; 

% path used for the manouvre

px = 4 + [-4.90000000000000 -4.80000000000000 -4.70000000000000 -4.60000000000000 -4.50000000000000 -4.40000000000000 -4.30000000000000 -4.20000000000000 -4.10000000000000 -4 -3.90000000000000 -3.80000000000000 -3.70000000000000 -3.60000000000000 -3.50000000000000 -3.40000000000000 -3.30000000000000 -3.20000000000000 -3.10000000000000 -3 -3 -2.88015205209464 -2.76241027186900 -2.64819847509495 -2.53827715390007 -2.43284659582061 -2.33165351897651 -2.23409292502420 -2.13930097223009 -2.04623787082740 -1.95376212917260 -1.86069902776991 -1.76590707497580 -1.66834648102349 -1.56715340417939 -1.46172284609994 -1.35180152490505 -1.23758972813100 -1.11984794790536 -1.00000000000000 -0.900000000000000 -0.800000000000000 -0.700000000000000 -0.600000000000000 -0.500000000000000 -0.400000000000000 -0.300000000000000 -0.200000000000000 -0.100000000000000 0 0.100000000000000 0.200000000000000 0.300000000000000 0.400000000000000 0.500000000000000 0.600000000000000 0.700000000000000 0.800000000000000 0.900000000000000 1 1 1.11984794790536 1.23758972813100 1.35180152490505 1.46172284609994 1.56715340417939 1.66834648102349 1.76590707497580 1.86069902776991 1.95376212917260 2.04623787082740 2.13930097223009 2.23409292502420 2.33165351897651 2.43284659582061 2.53827715390007 2.64819847509495 2.76241027186900 2.88015205209464 3 3.10000000000000 3.20000000000000 3.30000000000000 3.40000000000000 3.50000000000000 3.60000000000000 3.70000000000000 3.80000000000000 3.90000000000000 4 4.10000000000000 4.20000000000000 4.30000000000000 4.40000000000000 4.50000000000000 4.60000000000000 4.70000000000000 4.80000000000000 4.90000000000000 5];   
py = -1 + [2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2.00846256171643 2.03248043991577 2.06989597381590 2.11852993887244 2.17628364429094 2.24119603579078 2.31146397008585 2.38543398222805 2.46157327024107 2.53842672975893 2.61456601777195 2.68853602991415 2.75880396420922 2.82371635570906 2.88147006112756 2.93010402618410 2.96751956008423 2.99153743828357 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 2.99153743828357 2.96751956008423 2.93010402618410 2.88147006112756 2.82371635570906 2.75880396420922 2.68853602991415 2.61456601777195 2.53842672975893 2.46157327024107 2.38543398222805 2.31146397008585 2.24119603579078 2.17628364429094 2.11852993887244 2.06989597381590 2.03248043991577 2.00846256171643 2.00000000000000 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];    

lpf_beta = designfilt('lowpassiir', ...         % Response type
             'FilterOrder',8, ...      % Filter order
             'HalfPowerFrequency',0.005);
% j is the total number of the test

for j = 1:9

    filename1 = '../../test_session/18_07_25/telemetry_07_25/';
    i_s = num2str(j);
    test = strcat('test',i_s,'.txt');
    
    
    filename1 = strcat(filename1,test);

    filename2 = '../../test_session/18_07_25/optitrack_07_25/';
    test = strcat('test',i_s,'.csv');
    filename2 = strcat(filename2,test);

    [telemetry] = telemetry_import_V2(filename1);
    [optitrack] = optitrack_import(filename2);

    [remap_telemetry,remap_optitrack] = test_data_remap(telemetry,optitrack);
    
% trasformation matrix absolute2relative

    for i = 1:length(remap_optitrack.roll)
        R_x(:,:,i) = [ 1 0 0 0;
                       0 cos(remap_optitrack.roll(i)) -sin(remap_optitrack.roll(i)) 0;
                       0 sin(remap_optitrack.roll(i)) cos(remap_optitrack.roll(i)) 0;
                       0 0 0 1];
        R_y(:,:,i) = [ cos(remap_optitrack.pitch(i)) 0 sin(remap_optitrack.pitch(i)) 0;
                       0 1 0 0;
                       -sin(remap_optitrack.pitch(i)) 0 cos(remap_optitrack.pitch(i)) 0;
                       0 0 0 1];
        R_z(:,:,i) = [ cos(remap_optitrack.yaw(i)) -sin(remap_optitrack.yaw(i)) 0 0;
                       sin(remap_optitrack.yaw(i)) cos(remap_optitrack.yaw(i)) 0 0;
                       0 0 1 0;
                       0 0 0 1;];
        T(:,:,i) =   [ 1 0 0 remap_optitrack.x_gps(i);
                       0 1 0 remap_optitrack.y_gps(i);
                       0 0 1 remap_optitrack.z_gps(i);
                       0 0 0 1];

        T_a(:,:,i) = T(:,:,i)*R_z(:,:,i)*R_y(:,:,i)*R_x(:,:,i);
        V_a2r(:,i) = [remap_optitrack.Vx_gps(i), remap_optitrack.Vy_gps(i), 0, 1]*T(:,:,i)*R_z(:,:,i);
    end
    
    tmp = atan2(V_a2r(2,:),V_a2r(1,:));
    idx = find(V_a2r(1,:) < 0.01);
    tmp(idx) = 0;
    tmp = filtfilt(lpf_beta,tmp);
    beta(1,:,j) = atan2(remap_optitrack.Vy_rel(1:max_time),remap_optitrack.Vx_rel(1:max_time));
    steering(1,:,j) = remap_telemetry.steering(1:max_time);
    X_gps(1,:,j) = -remap_optitrack.x_gps(1:max_time);
    Y_gps(1,:,j) = -remap_optitrack.y_gps(1:max_time);
    yaw_gps(1,:,j) = remap_optitrack.yaw(1:max_time);
    V_gps(1,:,j) = remap_optitrack.V_gps(1:max_time);
    
    for l = 1:length(X_gps(1,:,j))
        for k=1:1:length(px)
           dist(k,l)=sqrt((X_gps(1,l,j)-px(k)).^2+(Y_gps(1,l,j)-py(k)).^2); % distance of the location from all the points of the vector px
        end    
       [dmin(1,l),npmin(1,l)]=min(dist(:,l));
    end
    %selecting the minimum distance
    x_track_error(1,:,j) = sqrt((X_gps(1,:,j) - px(npmin)).^2 + (Y_gps(1,:,j) - py(npmin)).^2);
end    

%% plot
x0=10;y0=20;
width=550;
height = 400;

% primo set di plot

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
plot(px,py,'+')
hold on
for i = 1:4
    plot(X_gps(:,:,i),Y_gps(:,:,i))
    title('trajectory')
    xlabel('m')
    ylabel('m')
end
legend('reference', 'pursuit lookhead 2', 'pursuit lookhead 6', 'pursuit lookhead 15', 'preview lookhead 2')

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
hold on
for i = 1:4
    plot(X_gps(:,:,i),x_track_error(:,:,i))
    title('track_error')
    xlabel('x coordinate')
    ylabel('error [m]')
end
legend( 'pursuit lookhead 2', 'pursuit lookhead 6', 'pursuit lookhead 15', 'preview lookhead 2')

% secondo set  di plot

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
plot(px,py,'+')
hold on
for i = 4:5
    plot(X_gps(:,:,i),Y_gps(:,:,i))
    title('trajectory')
    xlabel('m')
    ylabel('m')
end
legend('reference', 'preview old steering map', 'preview new steering map')

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
hold on
for i = 4:5
    plot(X_gps(:,:,i),x_track_error(:,:,i))
    title('track error')
    xlabel('x coordinate')
    ylabel('error [m]')
end
legend('preview old steering map', 'preview new steering map')

% terzo set di plot 

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
plot(px,py,'+')
axis equal
hold on

for i = 5:7
    plot(X_gps(:,600:end,i),Y_gps(:,600:end,i))
    %save data
    x_plot_13(:,:,i-4) = X_gps(:,600:end,i);
    y_plot_13(:,:,i-4) = Y_gps(:,600:end,i);
    title('trajectory')
    xlabel('m')
    ylabel('m')
end
legend('reference', 'preview lookhead 2', 'pursuit lookhead 2', 'pursuit lookhead 6')

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
hold on
ylim([-0.2 0.4])
for i = 5:7
    plot(X_gps(:,600:end,i),x_track_error(:,600:end,i))
    title('track error')
    xlabel('x coordinate')
    ylabel('error [m]')
    
end
legend( 'preview lookhead 2', 'pursuit lookhead 2', 'pursuit lookhead 6')

% quarto set di plot

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
plot(px,py,'+')
hold on
plot(X_gps(:,:,5),Y_gps(:,:,5))
x_plot_14 = zeros(1,24000,3);
y_plot_14 = zeros(1,24000,3);
x_plot_14(:,:,1) = X_gps(:,:,5);
y_plot_14(:,:,1) = Y_gps(:,:,5);
for i = 8:9
    plot(X_gps(:,700:end,i),Y_gps(:,700:end,i))
    x_plot_14(:,700:end,i-6) = X_gps(:,700:end,i);
    y_plot_14(:,700:end,i-6) = Y_gps(:,700:end,i);
    title('trajectory')
    xlabel('m')
    ylabel('m')
end
legend('reference', 'preview 1 m/s', 'preview 2 m/s', 'preview 3 m/s')

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
ylim([-0.2 0.4])
plot(X_gps(:,:,5),x_track_error(:,:,5))
hold on
for i = 8:9
    plot(X_gps(:,700:end,i),x_track_error(:,700:end,i))
    title('track error')
    xlabel('x coordinate')
    ylabel('error [m]')
end
legend('preview 1 m/s', 'preview 2 m/s', 'preview 3 m/s')

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
plot(X_gps(:,:,5),steering(:,:,5))
hold on
for i = 8:9
    plot(X_gps(:,700:end,i),steering(:,700:end,i))
    title('% steer')
    xlabel('x coordinate')
    ylabel('% steer')
end
legend('preview 1 m/s', 'preview 2 m/s', 'preview 3 m/s')

%% clothoid comparison
clear;clc;

ts = 0.001;
max_time = 16/ts; 


% first track
x = [0, 2, 4, 6, 8, 10];
y = [0, 0, 1, 1, 0, 0];
theta = [0, 0, 0, 0, 0, 0];


S = ClothoidList() ;
for i = 1:length(x)-1
    S.push_back_G1(x(i), y(i), theta(i), x(i+1), y(i+1), theta(i+1) ) ; % track creation

end

for j = 1:4

    filename1 = '../../test_session/18_09_20/telemetry_09_20/';
    i_s = num2str(j);
    test = strcat('test',i_s,'.txt');
    
    
    filename1 = strcat(filename1,test);

    filename2 = '../../test_session/18_09_20/optitrack_09_20/';
    test = strcat('test',i_s,'.csv');
    filename2 = strcat(filename2,test);
    [telemetry] = telemetry_import_V2(filename1);
    [optitrack] = optitrack_import(filename2);

    [remap_telemetry,remap_optitrack] = test_data_remap(telemetry,optitrack);
    
    
    for i = 1:max_time
        if ( abs(remap_optitrack.Vy_rel(i))<0.02)
            tmp(i) = 0;
        else
            tmp(i) = remap_optitrack.Vy_rel(i);
        end
    end

    for i = 1:max_time
        if ( abs(remap_optitrack.Vx_rel(i))<0.02)
            tmp1(i) = 0;
        else
            tmp1(i) = remap_optitrack.Vx_rel(i);
        end
    end
    beta(1,:,j) = atan2(tmp,tmp1);
    steering(1,:,j) = remap_telemetry.steering(1:max_time);
    X_gps(1,:,j) = remap_telemetry.x_gps(1:max_time)/1000;
    Y_gps(1,:,j) = remap_telemetry.y_gps(1:max_time)/1000;
    yaw_gps(1,:,j) = remap_telemetry.yaw_gps(1:max_time)/1000;
    V_gps(1,:,j) = remap_optitrack.V_gps(1:max_time);
    a_y(1,:,j) = remap_telemetry.a_y(1:max_time);
    a_x(1,:,j) = remap_telemetry.a_x(1:max_time);
    yaw_rate(1,:,j) = remap_telemetry.gyro_z(1:max_time);
    % error
    [s(1,:,j),err(1,:,j)] = S.find_coord( X_gps(1,:,j), Y_gps(1,:,j));

    [x1(1,:,j),y1(1,:,j),theta1(1,:,j),kappa1(1,:,j)] = S.evaluate( s(1,:,j) );
    att_error(1,:,j) = yaw_gps(1,:,j)-theta1(1,:,j);
end

%% plot
x0=10;y0=20;
width=550;
height = 400;

figure()
set(gcf,'units','points','position',[x0,y0,width,height])
S.plot(100,{'Color','blue','LineWidth',1},{'Color','blue','LineWidth',1});
hold on


p(3) = plot(5+X_gps(:,7000:end,4),-2+Y_gps(:,7000:end,4),'LineWidth',2);
p(4) = plot(X_gps(:,:,3),Y_gps(:,:,3),'LineWidth',2);
title('trajectory')
xlabel('m')
ylabel('m')

legend([p(3),p(4)],{'preview','clothoids'})