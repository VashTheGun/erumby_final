#define _USE_MATH_DEFINES
#define S_FUNCTION_LEVEL 2
#define S_FUNCTION_NAME clothoid_traj_g1_optimal_mod
#include "simstruc.h"

#include <ClothoidList.hh>
#include <cstddef>
#include <vector>

// Confgiurations
#define INBOUND_EPSILON 0.1
#define INBOUND_TOLERANCE 0.2


// cast per prendere la clotoide nel pwork
inline G2lib::ClothoidList *get_clothoid_list(SimStruct *S, int i) {
  return static_cast< G2lib::ClothoidList * >(ssGetPWorkValue(S, i));
}

/* CHECK_INBOUND
 * funzione che controlla se una traiettoria eccede un valore rispetto alla normale del centro strada
 *
 *   discretizza la traiettoria in una serie di punti e per ogni punto controlla se la normale rispetto
 *   al centro strada eccede un valore limite.
 *
 * output: 0 ->la traiettoria � ok  
 *         1 ->la traiettoria si allontana troppo
 * input: guess ->la traiettoria che si vuole percorrere
 *        centerline ->il centro strada
 *        epsilon ->passo di discretizzazione della traiettoria [m]
 *        tolerance -> tolleranza limite rispetto al centro strada [m]
 *        segment_start -> segmento di clotoide dal quale iniziare la ricerca
 *        num_segment -> numero di segmenti da cui � composta la clotoide
 */        

inline int check_inbound(const G2lib::ClothoidCurve &guess, const G2lib::ClothoidList *centerline, 
                         const real_T epsilon, const real_T tolerance,const int segment_start, 
                         const int num_segment) {
  real_T s_vect;
  G2lib::real_type s, t, x, y, k, theta;
  real_T n_points = std::floor(guess.length() / epsilon);

  for (int j = 0; j < n_points; j++) {  // check for bound of roadway
    s_vect = epsilon * j;
    guess.evaluate(s_vect, theta, k, x, y);
   //ssPrintf("x = %d", num_segment);
    if(segment_start == num_segment-2){
        centerline->findST1(segment_start, segment_start+1, x, y, s, t);
        //centerline->findST(x, y, s, t);
    }
    else{
        centerline->findST1(segment_start, segment_start+1, x, y, s, t);
    }
    if (std::abs(t) >= tolerance) {
      return 1;
    }
  }
  return 0;
}

/* SELECT_CLOTHOID
 * funzione che seleziona la traiettoria a lunghezza minima
 *
 * la funzione genera diverse traiettorie dallo stesso punto di partenza a diversi punti di arrivo
 * e le confronta scegliendo quella che, rimanendo all'interno della carreggiata, ha lunghezza minima.
 *
 * output:0 ->almeno una traiettoria � all'interno della carregiata  
 *        1 ->nessuna traiettoria � all'interno della carregiata 
 *        index ->indice dei punti che costruiscono la traiettoria migliore
 * input: centerline ->il centro strada
 *        index_start ->indice di partenza del vettore di punti da analizzare
 *        index_end ->indice di fine del vettore di punti da analizzare
 *        x_in, y_in, theta_in ->coordinate e posa del punto di partenza
 *        x_targ, y_targ, theta_targ ->coordinate e posa dei candidati punti di arrivo
 *        segment_start -> segmento di clotoide dal quale iniziare la ricerca
 *        num_segment -> numero di segmenti da cui � composta la clotoide 
 */ 
inline int select_clothoid(int &index, const G2lib::ClothoidList *centerline, const int &index_start,
                           const int &index_end, const real_T x_in, const real_T y_in, const real_T theta_in,
                           const real_T *x_targ, const real_T *y_targ, const real_T *theta_targ,
                           const int segment_start,  const int num_segment) {
  static const real_T epsilon = INBOUND_EPSILON;
  static const real_T tolerance = INBOUND_TOLERANCE;
  G2lib::ClothoidCurve guess;
  G2lib::ClothoidCurve alternative;
  index = index_start;


  guess.build_G1(x_in, y_in, theta_in, x_targ[index_start], y_targ[index_start], theta_targ[index_start]);
  int bound_check = check_inbound(guess, centerline, epsilon, tolerance, segment_start, num_segment);

  for (int i = index_start + 1; i < index_end; i++) {  // creation of 5 traj
    alternative.build_G1(x_in, y_in, theta_in, x_targ[i], y_targ[i], theta_targ[i]);
    int new_bound = check_inbound(alternative, centerline, epsilon, tolerance, segment_start, num_segment);

    if (new_bound == 1) {  // skip the traj
      continue;
    }
    if ((guess.length() > alternative.length()) || (bound_check != new_bound)) {  // take the best traj
      guess = alternative;
      index = i;
      bound_check = new_bound;
    }
  }
  return bound_check;
}

#define MDL_INITIAL_SIZES
static void mdlInitializeSizes(SimStruct *S) {
  ssSetNumContStates(S, 0);
  ssSetNumDiscStates(S, 0);

  // Setting input ports
  if (!ssSetNumInputPorts(S, 7))
    return;
  ssSetInputPortWidth(S, 0, 10);  // Porta 1: X_targ : 1x1
  ssSetInputPortWidth(S, 1, 10);  // Porta 2: Y_targ : 1x1
  ssSetInputPortWidth(S, 2, 10);  // Porta 2: theta_targ : 1x1
  ssSetInputPortWidth(S, 3, 1);   // Porta 2: x_in : 1x1
  ssSetInputPortWidth(S, 4, 1);   // Porta 2: y_in : 1x1
  ssSetInputPortWidth(S, 5, 1);   // Porta 2: theta_in : 1x1
  ssSetInputPortWidth(S, 6, 1);   // flag

  for (int_T i = 0; i < 7; i++) {
    ssSetInputPortDirectFeedThrough(S, i, 1);
    ssSetInputPortSampleTime(S, i, CONTINUOUS_SAMPLE_TIME);
    ssSetInputPortRequiredContiguous(S, i, 1);
    ssSetInputPortOffsetTime(S, i, 0.0);
  }

  // Setting Output port
  if (!ssSetNumOutputPorts(S, 2))
    return;
  ssSetOutputPortWidth(S, 0, 1);  // Porta 1: k(s) clotoide : 1x1
  ssSetOutputPortWidth(S, 1, 1);  // Porta 2: n(s) clotoide : 1x1
  for (int_T i = 0; i < 2; i++) {
    ssSetOutputPortSampleTime(S, i, CONTINUOUS_SAMPLE_TIME);
    ssSetOutputPortOffsetTime(S, i, 0.0);
  }

  // Setting sample time
  ssSetNumSampleTimes(S, 1);

  ssSetNumPWork(S, 2);  // reserve element in the pointers vector

  ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);
  //ssSetOptions(S, 0);
}

static void mdlInitializeSampleTimes(SimStruct *S) {
  ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
  ssSetOffsetTime(S, 0, 0.0);
  ssSetModelReferenceSampleTimeDefaultInheritance(S);
}

static void mdlOutputs(SimStruct *S, int_T tid) {
  G2lib::ClothoidList *current_traj = get_clothoid_list(S, 0);
  G2lib::ClothoidList *centerline = get_clothoid_list(S, 1);
  // Attaching output port
  real_T *k_out = ssGetOutputPortRealSignal(S, 0);
  real_T *n_out = ssGetOutputPortRealSignal(S, 1);

  // Attaching input port
  const real_T *x_targ = ssGetInputPortRealSignal(S, 0);
  const real_T *y_targ = ssGetInputPortRealSignal(S, 1);
  const real_T *theta_targ = ssGetInputPortRealSignal(S, 2);
  const real_T *x_in = ssGetInputPortRealSignal(S, 3);
  const real_T *y_in = ssGetInputPortRealSignal(S, 4);
  const real_T *theta_in = ssGetInputPortRealSignal(S, 5);
  const real_T *flag = ssGetInputPortRealSignal(S, 6);

  // Output values
  int bound_check;
  int index = 0;

  G2lib::real_type s_, t_, x_, y_, k_, theta_;
  
  G2lib::real_type s, t;
  
  if (flag[0] > 0) {  // flag for enabling computation of traj

    current_traj->init();
    int segment_start = centerline->findST1(x_in[0], y_in[0], s, t);
    const int num_segment = centerline->numSegment();
      
    int bound_check = select_clothoid(index, centerline, 0, 5, x_in[0], y_in[0], theta_in[0], x_targ, y_targ,
                                      theta_targ, segment_start, num_segment);
    if (bound_check == 1) {
      bound_check = select_clothoid(index, centerline, 5, 10, x_in[0], y_in[0], theta_in[0], x_targ, y_targ,
                                    theta_targ, segment_start, num_segment);
    }
    current_traj->push_back_G1(x_in[0], y_in[0], theta_in[0], x_targ[index], y_targ[index], theta_targ[index]);
  }

  current_traj->findST1(x_in[0], y_in[0], s_, t_);
  current_traj->evaluate((s_ <= 0 ? 0 : s_), theta_, k_, x_, y_);

  k_out[0] = k_;
  n_out[0] = index;

  // ssPrintf("x = %f, y = %f, s = %f, t = %f\n", x[0], y[0], s_, t_);
  // Required for unused arguments
  UNUSED_ARG(tid);
}

#define MDL_START
static void mdlStart(SimStruct *S) {

/* //first track
  const real_T x[] = {0, 2, 4, 6, 7, 7, 0, 0};
  const real_T y[] = {0, 1, 1, 0, 0, 2, 2, 0};
  const real_T theta[] = {0, 0, 0, 0, 0, -M_PI, -M_PI, 0};
*/
 
  // second track  
  const real_T x[] = {4, 0, 0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0};
  const real_T y[] = {6, 6, 0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6};
  const real_T theta[] = {M_PI, M_PI, 0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI, M_PI};

    /*
  const real_T x[] = {0, 3, 3, 0, 0};
  const real_T y[] = {0, 3, 0, 3, 0};
  const real_T theta[] = {0, 0, M_PI, M_PI, 0};  
  */
  const int n_clothoids =  sizeof(x)/ sizeof(x[0]) - 1;

  G2lib::ClothoidList *current_traj = new G2lib::ClothoidList();
  G2lib::ClothoidList *centerline = new G2lib::ClothoidList();

  for (int i = 0; i < n_clothoids; i++) {
    centerline->push_back_G1(x[i], y[i], theta[i], x[i + 1], y[i + 1], theta[i + 1]);
  }

  ssSetPWorkValue(S, 0, static_cast< void * >(current_traj));
  ssSetPWorkValue(S, 1, static_cast< void * >(centerline));
}

#define MDL_TERMINATE
static void mdlTerminate(SimStruct *S) {
  G2lib::ClothoidList *current_traj = get_clothoid_list(S, 0);
  G2lib::ClothoidList *centerline = get_clothoid_list(S, 1);
  delete current_traj;
  delete centerline;
}

#ifdef MATLAB_MEX_FILE /* Is this file being compiled as a MEX-file? */
#include "simulink.c"  /* MEX-file interface mechanism */
#else
#include "cg_sfun.h" /* Code generation registration function */
#endif