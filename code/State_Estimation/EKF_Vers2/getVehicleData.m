function vehicle_data = getVehicleData()

    % ----------------------------------------
    %% Build a structure with the vehicle data
    % ----------------------------------------
    
    % Physical parameters
    vehicle_data.m = 4.61;   % [kg] mass
    vehicle_data.L = 0.325;  % [m] wheelbase
    vehicle_data.W = 0.2;    % [m] track width
    
    % Steering behavior / yaw rate dynamics data
    vehicle_data.steerBehavior.tau__Omega = 0.09;  % [s] time constant of the yaw rate dynamics
    % Coefficients of the generalized understeering gradient function
    vehicle_data.steerBehavior.kUS_0 = 0.00269727708136947;
    vehicle_data.steerBehavior.kUS_1 = -0.00818755375509753;
    vehicle_data.steerBehavior.kUS_2 = -0.00119294223625316;
    vehicle_data.steerBehavior.kUS_3 = -0.000352945973926176;
    vehicle_data.steerBehavior.kUS_4 = 0.0000850910912602157;
    vehicle_data.steerBehavior.kUS_5 = 0.0000354815154898906;
    vehicle_data.steerBehavior.kUS_6 = -2.18303241154389*10^(-7);
    vehicle_data.steerBehavior.kUS_7 = 3.37179740467334*10^(-8);

end

