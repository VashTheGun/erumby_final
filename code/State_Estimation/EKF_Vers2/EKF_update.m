function [xk_1_hat,Pk_1] = EKF_update(xk_1_hat_pred,Pk_1_pred,z_meas,h_k_1,R,H)

    % ------------------------------
    %% Update step of the EKF
    % ------------------------------
    
    % Covariance of the residuals
    S = H*Pk_1_pred*H' + R;
    
    % Kalman gain
    W = Pk_1_pred*H'*pinv(S);
    
    % Update for the model states
    xk_1_hat = xk_1_hat_pred + W*(z_meas - h_k_1);
    
    % Update for the covariance of the state estimation error
    Pk_1 = (eye(length(xk_1_hat_pred)) - W*H)*Pk_1_pred;

end

