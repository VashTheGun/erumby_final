function [z_meas,h,R,H,z_meas_last,EKF_flag] = new_measurem(measurem,sensors,states_predict,idx_meas,z_meas_last,idx_time_step,EKF_flag)

    % ------------------------------
    %% Collect the new measurements
    % ------------------------------
    
    z_meas = [];   % initialize the new measurements
    R      = [];   % initialize the covariance of the measurement error
    H      = [];   % initialize the Jacobian of the measurement functions w.r.t. the model states
    h      = [];   % initialize the outputs of the sensors measurement functions
    
    % Extract the predicted model states
    x_pred     = states_predict(1);
    y_pred     = states_predict(2);
    psi_pred   = states_predict(3);
    v_x_pred   = states_predict(4);
    a_x_pred   = states_predict(6);
    a_y_pred   = states_predict(7);
    Omega_pred = states_predict(8);
    
    % Extract the latest measurements (i.e. the ones used in the last update)
    x_meas_last     = z_meas_last(1);
    y_meas_last     = z_meas_last(2);
    psi_meas_last   = z_meas_last(3);
    v_x_meas_last   = z_meas_last(4);
    a_x_meas_last   = z_meas_last(5);
    a_y_meas_last   = z_meas_last(6);
    Omega_meas_last = z_meas_last(7);
    
    tol_newData = 1e-6;    % tolerance to discriminate the new measurements
    tol_outlier_XY = 5;    % value to reject outliers (i.e. bad measurements)
    tol_outlier_vx = 0.3;  % value to reject outliers (i.e. bad measurements)
    
    % Check if new GPS x-y-psi measurements are available, and if they are not outliers
    check_new_GPS_XY = (abs(measurem.x_telem(idx_meas)-x_meas_last) > tol_newData) || (abs(measurem.y_telem(idx_meas)-y_meas_last) > tol_newData);
    check_not_outlier_GPS_XY = (abs(measurem.x_telem(idx_meas)-x_meas_last) < tol_outlier_XY) && (abs(measurem.y_telem(idx_meas)-y_meas_last) < tol_outlier_XY);
    if (check_new_GPS_XY && check_not_outlier_GPS_XY)
        z_meas = [measurem.x_telem(idx_meas); measurem.y_telem(idx_meas); measurem.psi_telem(idx_meas)];
        R = diag(sensors.GPS.sigma(1:3).^2);
        H = [1 0 0 0 0 0 0 0;
             0 1 0 0 0 0 0 0;
             0 0 1 0 0 0 0 0];
        h = [x_pred; y_pred; psi_pred];
        z_meas_last(1:3) = [measurem.x_telem(idx_meas); measurem.y_telem(idx_meas); measurem.psi_telem(idx_meas)];
        EKF_flag.x(idx_time_step) = 1;  EKF_flag.y(idx_time_step) = 1;  EKF_flag.psi(idx_time_step) = 1;
    end
    
    % Check if new GPS v_x measurements are available, and if they are not outliers
    check_new_GPS_vx = (abs(measurem.u_telem(idx_meas)-v_x_meas_last) > tol_newData);
    check_not_outlier_GPS_vx = (abs(measurem.u_telem(idx_meas)) > 1e-6) && (abs(measurem.u_telem(idx_meas)-measurem.u_telem(idx_meas-1)) < tol_outlier_vx) && (abs(measurem.u_telem(idx_meas)-measurem.u_telem(max(idx_meas-2,1))) < tol_outlier_vx) && (abs(measurem.u_telem(idx_meas)-measurem.u_telem(max(idx_meas-3,1))) < tol_outlier_vx);
    if (check_new_GPS_vx && check_not_outlier_GPS_vx)
        z_meas = [z_meas; measurem.u_telem(idx_meas)];
        R = diag([diag(R)',sensors.GPS.sigma(4).^2]);
        H = [H; 
             0 0 0 1 0 0 0 0];
        h = [h; v_x_pred];
        z_meas_last(4) = measurem.u_telem(idx_meas);
        EKF_flag.v_x(idx_time_step) = 1;
    end
    
    % Check if new IMU Ax measurements are available
    check_new_IMU_Ax = abs(measurem.Ax_telem(idx_meas)-a_x_meas_last) > tol_newData;
    if (check_new_IMU_Ax)
        z_meas = [z_meas; measurem.Ax_telem(idx_meas)];
        R = diag([diag(R)',sensors.IMU.sigma(1).^2]);
        H = [H; 
             0 0 0 0 0 1 0 0];
        h = [h; a_x_pred];
        z_meas_last(5) = measurem.Ax_telem(idx_meas);
        EKF_flag.a_x(idx_time_step) = 1;
    end
    
    % Check if new IMU Ay measurements are available
    check_new_IMU_Ay = abs(measurem.Ay_telem(idx_meas)-a_y_meas_last) > tol_newData;
    if (check_new_IMU_Ay)
        z_meas = [z_meas; measurem.Ay_telem(idx_meas)];
        R = diag([diag(R)',sensors.IMU.sigma(2).^2]);
        H = [H; 
             0 0 0 0 0 0 1 0];
        h = [h; a_y_pred];
        z_meas_last(6) = measurem.Ay_telem(idx_meas);
        EKF_flag.a_y(idx_time_step) = 1;
    end
    
    % Check if new IMU Omega measurements are available
    check_new_IMU_Omega = abs(measurem.Omega_telem(idx_meas)-Omega_meas_last) > tol_newData;
    if (check_new_IMU_Omega)
        z_meas = [z_meas; measurem.Omega_telem(idx_meas)];
        R = diag([diag(R)',sensors.IMU.sigma(3).^2]);
        H = [H; 
             0 0 0 0 0 0 0 1];
        h = [h; Omega_pred]; 
        z_meas_last(7) = measurem.Omega_telem(idx_meas);
        EKF_flag.Omega(idx_time_step) = 1;
    end

end

