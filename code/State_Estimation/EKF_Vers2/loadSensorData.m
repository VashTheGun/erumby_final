function sensors = loadSensorData()

    % -------------------------------------------
    %% Define the characteristics of the sensors
    % -------------------------------------------
    % Sensors are defined in terms of:
    %   --> sigma = standard deviation
    
    % ----------------------
    %% IMU sensor 
    %  measures Ax, Ay, Omega
    % ----------------------
    sensors.IMU.sigma = [2/3, 2/3, 0.1/3];    % [m/s^2], [m/s^2], [rad/s]
    
    % ----------------------
    %% GPS sensor 
    %  measures x, y, psi, v_x
    % ----------------------
    sensors.GPS.sigma = [0.1/3, 0.1/3, deg2rad(3)/3, 0.3/3];   % [m], [m], [rad], [m/s]

end

