function [xk_1_hat_pred,Pk_1_pred] = EKF_predict(xk_hat,Pk,delta,Q,T__s,vehicle_data)

    % ------------------------------
    %% Prediction step of the EKF
    % ------------------------------
    
    % Extract the estimated states
    x     = xk_hat(1);
    y     = xk_hat(2);
    psi   = xk_hat(3);
    v__x  = xk_hat(4);
    v__y  = xk_hat(5);
    a__x  = xk_hat(6);
    a__y  = xk_hat(7);
    Omega = xk_hat(8);
    
    % Extract the vehicle data
    L          = vehicle_data.L;
    tau__Omega = vehicle_data.steerBehavior.tau__Omega;
    k__US__0   = vehicle_data.steerBehavior.kUS_0;
    k__US__1   = vehicle_data.steerBehavior.kUS_1;
    k__US__2   = vehicle_data.steerBehavior.kUS_2;
    k__US__3   = vehicle_data.steerBehavior.kUS_3;
    k__US__4   = vehicle_data.steerBehavior.kUS_4;
    k__US__5   = vehicle_data.steerBehavior.kUS_5;
    k__US__6   = vehicle_data.steerBehavior.kUS_6;
    k__US__7   = vehicle_data.steerBehavior.kUS_7;
    
    % Jacobian A (w.r.t. the model states)
    A = [1 0 T__s * (-sin(psi) * v__x - cos(psi) * v__y) T__s * cos(psi) -T__s * sin(psi) 0 0 0;
         0 1 T__s * (cos(psi) * v__x - sin(psi) * v__y) T__s * sin(psi) T__s * cos(psi) 0 0 0;
         0 0 1 0 0 0 0 T__s;
         0 0 0 1 T__s * Omega T__s 0 T__s * v__y;
         0 0 0 -T__s * Omega 1 0 T__s -T__s * v__x;
         0 0 0 0 0 1 0 0;
         0 0 0 0 0 0 1 0;
         0 0 0 T__s / tau__Omega * (0.1e1 / L * (-0.7e1 * Omega ^ 7 * v__x ^ 6 * k__US__7 - 0.6e1 * Omega ^ 6 * v__x ^ 5 * k__US__6 - 0.5e1 * Omega ^ 5 * v__x ^ 4 * k__US__5 - 0.4e1 * Omega ^ 4 * v__x ^ 3 * k__US__4 - 0.3e1 * Omega ^ 3 * v__x ^ 2 * k__US__3 - 0.2e1 * Omega ^ 2 * v__x * k__US__2 - Omega * k__US__1) * v__x + 0.1e1 / L * (-v__x ^ 7 * Omega ^ 7 * k__US__7 - v__x ^ 6 * Omega ^ 6 * k__US__6 - v__x ^ 5 * Omega ^ 5 * k__US__5 - v__x ^ 4 * Omega ^ 4 * k__US__4 - v__x ^ 3 * Omega ^ 3 * k__US__3 - v__x ^ 2 * Omega ^ 2 * k__US__2 - v__x * Omega * k__US__1 + delta - k__US__0)) 0 0 0 0.1e1 + T__s / tau__Omega * (0.1e1 / L * (-0.7e1 * Omega ^ 6 * v__x ^ 7 * k__US__7 - 0.6e1 * Omega ^ 5 * v__x ^ 6 * k__US__6 - 0.5e1 * Omega ^ 4 * v__x ^ 5 * k__US__5 - 0.4e1 * Omega ^ 3 * v__x ^ 4 * k__US__4 - 0.3e1 * Omega ^ 2 * v__x ^ 3 * k__US__3 - 0.2e1 * Omega * v__x ^ 2 * k__US__2 - v__x * k__US__1) * v__x - 0.1e1)];


     % Jacobian G (w.r.t. the process noise)
     G = eye(size(A,1));
     
     % Understeering gradient function
     k__US_fcn = Omega^7*v__x^7*k__US__7 + Omega^6*v__x^6*k__US__6 + Omega^5*v__x^5*k__US__5 + Omega^4*v__x^4*k__US__4 + Omega^3*v__x^3*k__US__3 + Omega^2*v__x^2*k__US__2 + Omega*v__x*k__US__1 + k__US__0;
     
     % Predicted state
     xk_1_hat_pred = [x + T__s * (cos(psi) * v__x - sin(psi) * v__y);
                      y + T__s * (sin(psi) * v__x + cos(psi) * v__y);
                      T__s * Omega + psi;
                      v__x + T__s * (Omega * v__y + a__x);
                      v__y + T__s * (-Omega * v__x + a__y);
                      a__x;
                      a__y;
                      Omega + T__s / tau__Omega * (0.1e1 / L * (-k__US_fcn + delta) * v__x - Omega)];
     
     % Predicted covariance of the estimation error
     Pk_1_pred = A*Pk*A' + G*Q*G';

end

