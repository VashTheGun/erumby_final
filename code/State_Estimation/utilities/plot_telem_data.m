% ------------------------------
%% Plot the main telemetry data 
% ------------------------------

label_fig_mainQuantities = strcat({'Main quantities'},{' '},{testID});
figure('Name',label_fig_mainQuantities{1},'NumberTitle','off'), clf 
% --- u --- %
ax(1) = subplot(231);
hold on
plot(time_telem,u_telem,'.r','LineWidth',2)
grid on
title('$u$ [m/s]')
legend('optitrack','location','best')
xlim([time_telem(1) time_telem(end)])
ylim([0 3.5])
% --- delta --- %
ax(2) = subplot(232);
plot(time_telem,rad2deg(delta_telem),'.')
hold on
grid on
title('$\delta$ [deg]')
legend('$\delta$','location','best')
xlim([time_telem(1) time_telem(end)])
ylim([-25 25])
% --- Omega --- %
ax(3) = subplot(233);
hold on
plot(time_telem,Omega_telem,'.')
grid on
title('$\Omega$ [rad/s]')
legend('IMU','location','best')
xlim([time_telem(1) time_telem(end)])
% --- Ay --- %
ax(4) = subplot(234);
hold on
plot(time_telem,Ay_telem,'.')
plot(time_telem,Ay_ss_telem,'.')
grid on
title('$A_{y}$ [m/s$^2$]')
legend('IMU','$A_{y,ss} = \Omega u$','location','best')
xlim([time_telem(1) time_telem(end)])
ylim([-8 8])
% --- Ax --- %
ax(5) = subplot(235);
plot(time_telem,Ax_telem,'.')
grid on
title('$A_{x}$ [m/s$^2$]')
legend('IMU','location','best')
xlim([time_telem(1) time_telem(end)])
% --- omega_wheels --- %
ax(6) = subplot(236);
hold on
plot(time_telem,omega_rr_telem,'.')
plot(time_telem,omega_rl_telem,'.')
grid on
legend('$\omega_{rr}$','$\omega_{rl}$','location','best')
xlim([time_telem(1) time_telem(end)])

% ------------
%% Plot vehicle path and pose
% ------------
label_fig_vehPath = strcat({'Vehicle path'},{' '},{testID});
figure('Name',label_fig_vehPath{1},'NumberTitle','off'), clf
% --- path --- %
ax(1) = subplot(221);
plot(x_telem,y_telem,'.b','LineWidth',2)
grid on
axis equal
xlabel('$x$ [m]')
ylabel('$y$ [m]')
title('Vehicle path')
% --- x coord --- %
ax(2) = subplot(222);
hold on
plot(time_telem,x_telem,'.b','LineWidth',2)
grid on
title('$x$ [m]')
xlim([time_telem(1) time_telem(end)])
% --- y coord --- %
ax(3) = subplot(223);
hold on
plot(time_telem,y_telem,'.b','LineWidth',2)
grid on
title('$y$ [m]')
xlim([time_telem(1) time_telem(end)])
% --- psi --- %
ax(4) = subplot(224);
hold on
psi_plot = rad2deg(psi_telem);
plot(time_telem,psi_telem,'.b','LineWidth',2)
grid on
title('$\psi$ [deg]')
xlim([time_telem(1) time_telem(end)])

