% ----------------------------
%% Extract the telemetry data
% ----------------------------

% Extract the data
time_telem_orig     = telemData.data(:,1);
u_telem_orig        = telemData.data(:,2);
delta_telem_orig    = telemData.data(:,3);
Omega_telem_orig    = telemData.data(:,4);
Ay_telem_orig       = telemData.data(:,5);
Ay_ss_telem_orig    = telemData.data(:,6);
Ax_telem_orig       = telemData.data(:,7);
x_telem_orig        = telemData.data(:,8);
y_telem_orig        = telemData.data(:,9);
psi_telem_orig      = telemData.data(:,10);
omega_rr_telem_orig = telemData.data(:,11);
omega_rl_telem_orig = telemData.data(:,12);

% USE THE CORRECT WAY FOR EXPRESSING THE YAW ANGLE PSI
psi_telem_orig = psi_telem_orig + pi;

% Re-sample the data at a certain rate
Ts_telem = 0.01;  % [s] new sampling time
time_telem = time_telem_orig(1):Ts_telem:time_telem_orig(end);

u_telem        = interp1(time_telem_orig,u_telem_orig,time_telem);
delta_telem    = interp1(time_telem_orig,delta_telem_orig,time_telem);
Omega_telem    = interp1(time_telem_orig,Omega_telem_orig,time_telem);
Ay_telem       = interp1(time_telem_orig,Ay_telem_orig,time_telem);
Ay_ss_telem    = interp1(time_telem_orig,Ay_ss_telem_orig,time_telem);
Ax_telem       = interp1(time_telem_orig,Ax_telem_orig,time_telem);
x_telem        = interp1(time_telem_orig,x_telem_orig,time_telem);
y_telem        = interp1(time_telem_orig,y_telem_orig,time_telem);
psi_telem      = interp1(time_telem_orig,psi_telem_orig,time_telem);
omega_rr_telem = interp1(time_telem_orig,omega_rr_telem_orig,time_telem);
omega_rl_telem = interp1(time_telem_orig,omega_rl_telem_orig,time_telem);

measurem.x_telem     = x_telem;
measurem.y_telem     = y_telem;
measurem.psi_telem   = psi_telem;
measurem.u_telem     = u_telem;
measurem.Ax_telem    = Ax_telem;
measurem.Ay_telem    = Ay_telem;
measurem.Omega_telem = Omega_telem;
