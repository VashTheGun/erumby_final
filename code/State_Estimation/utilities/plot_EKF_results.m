% ----------------------------------------
%% Plot the main state estimation results
% ----------------------------------------

% -------------------
%% Extract the estimated states
% -------------------
x_estim     = x_hat_save(1,:);
y_estim     = x_hat_save(2,:);
psi_estim   = x_hat_save(3,:);
v_x_estim   = x_hat_save(4,:);
v_y_estim   = x_hat_save(5,:);
a_x_estim   = x_hat_save(6,:);
a_y_estim   = x_hat_save(7,:);
Omega_estim = x_hat_save(8,:);

% Find the indices of the steps in which an update was performed, for each state
idx_x_updat     = find(EKF_flag.x==1);
idx_y_updat     = find(EKF_flag.y==1);
idx_psi_updat   = find(EKF_flag.psi==1);
idx_vx_updat    = find(EKF_flag.v_x==1);
idx_ax_updat    = find(EKF_flag.a_x==1);
idx_ay_updat    = find(EKF_flag.a_y==1);
idx_Omega_updat = find(EKF_flag.Omega==1);

idx_updat = EKF_flag.x | EKF_flag.y | EKF_flag.psi | EKF_flag.v_x | EKF_flag.a_x | EKF_flag.a_y | EKF_flag.Omega;

% -------------------
%% Extract the components of the P matrix
% -------------------
P_11_save = zeros(length(time_vect)-1,1);
P_22_save = zeros(length(time_vect)-1,1);
P_33_save = zeros(length(time_vect)-1,1);
P_44_save = zeros(length(time_vect)-1,1);
P_55_save = zeros(length(time_vect)-1,1);
P_66_save = zeros(length(time_vect)-1,1);
P_77_save = zeros(length(time_vect)-1,1);
P_88_save = zeros(length(time_vect)-1,1);
for ii=1:length(time_vect)-1
    P_mat = P_save{ii};
    P_11_save(ii) = P_mat(1,1);
    P_22_save(ii) = P_mat(2,2);
    P_33_save(ii) = P_mat(3,3);
    P_44_save(ii) = P_mat(4,4);
    P_55_save(ii) = P_mat(5,5);
    P_66_save(ii) = P_mat(6,6);
    P_77_save(ii) = P_mat(7,7);
    P_88_save(ii) = P_mat(8,8);
end

% -------------------
%% Plot the components of the P matrix
% -------------------
label_fig_mainQuantities = strcat({'Covariance P'},{' '},{testID});
figure('Name',label_fig_mainQuantities{1},'NumberTitle','off'), clf 
% --- P_11 --- %
ax(1) = subplot(421);
plot(time_vect(1:end-1),P_11_save,'.')
hold on
grid on
title('$P_{11}$')
xlim([time_vect(1) time_vect(end)])
ylim([min(P_11_save) 1])
% --- P_22 --- %
ax(2) = subplot(422);
plot(time_vect(1:end-1),P_22_save,'.')
hold on
grid on
title('$P_{22}$')
xlim([time_vect(1) time_vect(end)])
ylim([min(P_22_save) 1])
% --- P_33 --- %
ax(3) = subplot(423);
plot(time_vect(1:end-1),P_33_save,'.')
hold on
grid on
title('$P_{33}$')
xlim([time_vect(1) time_vect(end)])
ylim([min(P_33_save) 1])
% --- P_44 --- %
ax(4) = subplot(424);
plot(time_vect(1:end-1),P_44_save,'.')
hold on
grid on
title('$P_{44}$')
xlim([time_vect(1) time_vect(end)])
ylim([min(P_44_save) 1])
% --- P_55 --- %
ax(5) = subplot(425);
plot(time_vect(1:end-1),P_55_save,'.')
hold on
grid on
title('$P_{55}$')
xlim([time_vect(1) time_vect(end)])
ylim([min(P_55_save) 1])
% --- P_66 --- %
ax(6) = subplot(426);
plot(time_vect(1:end-1),P_66_save,'.')
hold on
grid on
title('$P_{66}$')
xlim([time_vect(1) time_vect(end)])
ylim([min(P_66_save) 1])
% --- P_77 --- %
ax(7) = subplot(427);
plot(time_vect(1:end-1),P_77_save,'.')
hold on
grid on
title('$P_{77}$')
xlim([time_vect(1) time_vect(end)])
ylim([min(P_77_save) 1])
% --- P_88 --- %
ax(8) = subplot(428);
plot(time_vect(1:end-1),P_88_save,'.')
hold on
grid on
title('$P_{88}$')
xlim([time_vect(1) time_vect(end)])
ylim([min(P_88_save) 1])

% -------------------
%% Plot some states 
% -------------------
label_fig_mainQuantities = strcat({'States 1'},{' '},{testID});
figure('Name',label_fig_mainQuantities{1},'NumberTitle','off'), clf 
% --- delta --- %
ax(1) = subplot(221);
hold on
plot(time_telem,rad2deg(delta_telem),'.')
plot(time_vect,rad2deg(delta_saved),'o','MarkerSize',4)
hold on
grid on
title('$\delta$ [deg]')
legend('$\delta$','$\delta$ interp','location','best')
xlim([time_telem(1) time_telem(end)])
ylim([-25 25])
% --- Omega --- %
ax(2) = subplot(222);
hold on
plot(time_telem,Omega_telem,'.')
plot(time_vect,Omega_estim,'-','LineWidth',2)
plot(time_vect(idx_updat),Omega_estim(idx_updat),'mo','MarkerFaceColor','m','MarkerSize',4)
plot(time_vect(idx_Omega_updat),Omega_estim(idx_Omega_updat),'go','MarkerFaceColor','g','MarkerSize',4)
grid on
title('$\Omega$ [rad/s]')
legend('optitrack','EKF','EKF update','EKF update $\Omega$','location','best')
xlim([time_telem(1) time_telem(end)])
% --- Ay --- %
ax(3) = subplot(223);
hold on
plot(time_telem,Ay_telem,'.')
plot(time_telem,Ay_ss_telem,'.')
plot(time_vect,a_y_estim,'-','LineWidth',2)
plot(time_vect(idx_updat),a_y_estim(idx_updat),'mo','MarkerFaceColor','m','MarkerSize',4)
plot(time_vect(idx_ay_updat),a_y_estim(idx_ay_updat),'go','MarkerFaceColor','g','MarkerSize',4)
grid on
title('$A_{y}$ [m/s$^2$]')
legend('IMU','$A_{y,ss} = \Omega u$','EKF','EKF update','EKF update $A_y$','location','best')  
xlim([time_telem(1) time_telem(end)])
ylim([-8 8])
% --- Ax --- %
ax(4) = subplot(224);
hold on
plot(time_telem,Ax_telem,'.')
plot(time_vect,a_x_estim,'-','LineWidth',2)
plot(time_vect(idx_updat),a_x_estim(idx_updat),'mo','MarkerFaceColor','m','MarkerSize',4)
plot(time_vect(idx_ax_updat),a_x_estim(idx_ax_updat),'go','MarkerFaceColor','g','MarkerSize',4)
grid on
title('$A_{x}$ [m/s$^2$]')
legend('optitrack','EKF','EKF update','EKF update $A_x$','location','best')
xlim([time_telem(1) time_telem(end)])

% -------------------
%% Plot some states 
% -------------------
label_fig_mainQuantities = strcat({'States 2'},{' '},{testID});
figure('Name',label_fig_mainQuantities{1},'NumberTitle','off'), clf 
% --- u --- %
ax(1) = subplot(221);
hold on
plot(time_telem,u_telem,'.')
plot(time_vect,v_x_estim,'-','LineWidth',2)
plot(time_vect(idx_updat),v_x_estim(idx_updat),'mo','MarkerFaceColor','m','MarkerSize',4)
plot(time_vect(idx_vx_updat),v_x_estim(idx_vx_updat),'go','MarkerFaceColor','g','MarkerSize',4)
grid on
title('$v_x$ [m/s]')
legend('optitrack','EKF','EKF update','EKF update $v_x$','location','best')
xlim([time_telem(1) time_telem(end)])
ylim([0 3.5])
% --- v --- %
ax(2) = subplot(222);
hold on
plot(time_vect,v_y_estim,'-','LineWidth',2)
grid on
title('$v_y$ [m/s]')
legend('EKF','location','best')
xlim([time_telem(1) time_telem(end)])
% --- omega_wheels --- %
ax(3) = subplot(223);
hold on
plot(time_telem,omega_rr_telem,'.')
plot(time_telem,omega_rl_telem,'.')
grid on
title('Wheel speed')
legend('$\omega_{rr}$','$\omega_{rl}$','location','best')
xlim([time_telem(1) time_telem(end)])

% -------------------
%% Plot vehicle path and pose
% -------------------
label_fig_vehPath = strcat({'Vehicle path'},{' '},{testID});
figure('Name',label_fig_vehPath{1},'NumberTitle','off'), clf
% --- path --- %
ax(1) = subplot(221);
hold on
plot(x_telem,y_telem,'.')
plot(x_estim,y_estim,'.')
plot(x_estim(idx_updat),y_estim(idx_updat),'mo','MarkerFaceColor','m','MarkerSize',4)
plot(x_estim(idx_x_updat),y_estim(idx_y_updat),'go','MarkerFaceColor','g','MarkerSize',4)
plot(x_estim(1),y_estim(1),'bo')
grid on
axis equal
xlabel('$x$ [m]')
ylabel('$y$ [m]')
title('Vehicle path')
legend('optitrack','EKF','EKF update','EKF update $x$,$y$','location','best')
% --- x coord --- %
ax(2) = subplot(222);
hold on
plot(time_telem,x_telem,'.')
plot(time_vect,x_estim,'-','LineWidth',2)
plot(time_vect(idx_updat),x_estim(idx_updat),'mo','MarkerFaceColor','m','MarkerSize',4)
plot(time_vect(idx_x_updat),x_estim(idx_x_updat),'go','MarkerFaceColor','g','MarkerSize',4)
grid on
title('$x$ [m]')
legend('optitrack','EKF','EKF update','EKF update $x$','location','best')
xlim([time_telem(1) time_telem(end)])
% --- y coord --- %
ax(3) = subplot(223);
hold on
plot(time_telem,y_telem,'.')
plot(time_vect,y_estim,'-','LineWidth',2)
plot(time_vect(idx_updat),y_estim(idx_updat),'mo','MarkerFaceColor','m','MarkerSize',4)
plot(time_vect(idx_y_updat),y_estim(idx_y_updat),'go','MarkerFaceColor','g','MarkerSize',4)
grid on
title('$y$ [m]')
legend('optitrack','EKF','EKF update','EKF update $y$','location','best')
xlim([time_telem(1) time_telem(end)])
% --- psi --- %
ax(4) = subplot(224);
hold on
plot(time_telem,rad2deg(psi_telem),'.')
plot(time_vect,rad2deg(psi_estim),'-','LineWidth',2)
plot(time_vect(idx_updat),rad2deg(psi_estim(idx_updat)),'mo','MarkerFaceColor','m','MarkerSize',4)
plot(time_vect(idx_psi_updat),rad2deg(psi_estim(idx_psi_updat)),'go','MarkerFaceColor','g','MarkerSize',4)
grid on
title('$\psi$ [deg]')
legend('optitrack','EKF','EKF update','EKF update $\psi$','location','best')
xlim([time_telem(1) time_telem(end)])

