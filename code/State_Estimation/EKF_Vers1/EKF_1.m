% -----------------------------------
%% Load and plot telemetry data
% -----------------------------------
addpath('../MPC_lap_telem')
addpath('../utilities')
initialize_script;

% ------------------------
%% Load telemetry data
% ------------------------
% testID can be:
%   --> 'testMPCY', with Y = {10,12,14,15,16}, to load the telemetry of a full lap (with MPC)
testID = 'testMPC12';
telemData = importdata(strcat('../MPC_lap_telem/Dataset_saved/',testID,'.txt'));

% ------------------------
%% Extract and plot telemetry data
% ------------------------
extract_telem_data;
% plot_telem_data;

% ------------------------
%% Initialize the EKF
% ------------------------
% Timing
Ts = 1e-3;  % sampling time
time_vect = time_telem(1):Ts:time_telem(end);
delta_saved = interp1(time_telem_orig,delta_telem_orig,time_vect);  % interpolated steering angle control

% Characteristics of the sensors
sensors = loadSensorData();

% Covariance of the process noise
Q = diag([0.04/3, 0.04/3, deg2rad(2)/3, 0.04/3, 0.04/3, 0.1/3, 0.25/3, 0.1/3].^2);

% Initial covariance of the state estimation error
P0 = diag([sensors.GPS.sigma, 0.001, 0.001, 0.001, 0.001].^2);

P_save = cell(1,length(time_vect));
P_save{1} = P0;

% Initial conditions
x0 = [x_telem(1); y_telem(1); psi_telem(1); 0; 0; Ax_telem(1); Ay_telem(1); Omega_telem(1)];
nx = length(x0);  % n° of states

% Vector that will contain the estimated state
x_hat_save = zeros(nx,length(time_vect));   % initialize
x_hat_save(:,1) = x0 + randn(nx,1).*sqrt(diag(P0));

% EKF_flag is a filter flag, useful for post-proc, which can be:
%   --> EKF_flag = 0 then only a prediction step was performed in a certain time step
%   --> EKF_flag = 1 then only a prediction + update step was performed in a certain time step
EKF_flag.x     = zeros(length(time_vect),1);   % initialize
EKF_flag.y     = zeros(length(time_vect),1);   % initialize
EKF_flag.psi   = zeros(length(time_vect),1);   % initialize
EKF_flag.v_x   = zeros(length(time_vect),1);   % initialize
EKF_flag.a_x   = zeros(length(time_vect),1);   % initialize
EKF_flag.a_y   = zeros(length(time_vect),1);   % initialize
EKF_flag.Omega = zeros(length(time_vect),1);   % initialize

% ------------------------
%% Use of the EKF
% ------------------------
xk_hat = x_hat_save(:,1);
Pk = P0;
% Latest measurements
z_meas_last = [x_telem(1); y_telem(1); psi_telem(1); u_telem(1); Ax_telem(1); Ay_telem(1); Omega_telem(1)];  
jj = 2;

% State estimation loop
for kk = 2:length(time_vect)-1
    % Prediction step
    [xk_1_hat,Pk_1] = EKF_predict(xk_hat,Pk,Q,Ts);
    if (mod(time_vect(kk),Ts_telem)==0)
        % Measurements
        [z_meas,h_k_1,R,H,z_meas_last,EKF_flag] = new_measurem(measurem,sensors,xk_1_hat,jj,z_meas_last,kk,EKF_flag);
        if (~isempty(z_meas))
            % Update step
            [xk_1_hat,Pk_1] = EKF_update(xk_1_hat,Pk_1,z_meas,h_k_1,R,H);
        end
        jj = jj+1;
    end
    
    x_hat_save(:,kk) = xk_1_hat;
    P_save{kk} = Pk_1;
    xk_hat = xk_1_hat;
    Pk     = Pk_1;
end

% ------------------------
%% Post-processing
% ------------------------
plot_EKF_results;
