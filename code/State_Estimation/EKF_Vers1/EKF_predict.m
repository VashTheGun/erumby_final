function [xk_1_hat_pred,Pk_1_pred] = EKF_predict(xk_hat,Pk,Q,T__s)

    % ------------------------------
    %% Prediction step of the EKF
    % ------------------------------
    
    % Extract the estimated states
    x     = xk_hat(1);
    y     = xk_hat(2);
    psi   = xk_hat(3);
    v__x  = xk_hat(4);
    v__y  = xk_hat(5);
    a__x  = xk_hat(6);
    a__y  = xk_hat(7);
    Omega = xk_hat(8);
    
    % Jacobian A (w.r.t. the model states)
    A = [1 0 T__s * (-sin(psi) * v__x - cos(psi) * v__y) T__s * cos(psi) -T__s * sin(psi) 0 0 0;
         0 1 T__s * (cos(psi) * v__x - sin(psi) * v__y) T__s * sin(psi) T__s * cos(psi) 0 0 0;
         0 0 1 0 0 0 0 T__s; 
         0 0 0 1 T__s * Omega T__s 0 T__s * v__y; 
         0 0 0 -T__s * Omega 1 0 T__s -T__s * v__x; 
         0 0 0 0 0 1 0 0; 
         0 0 0 0 0 0 1 0; 
         0 0 0 0 0 0 0 1];

     % Jacobian G (w.r.t. the process noise)
     G = eye(size(A,1));
     
     % Predicted state
     xk_1_hat_pred = [x + T__s * (cos(psi) * v__x - sin(psi) * v__y);
                      y + T__s * (sin(psi) * v__x + cos(psi) * v__y);
                      T__s * Omega + psi;
                      v__x + T__s * (Omega * v__y + a__x);
                      v__y + T__s * (-Omega * v__x + a__y);
                      a__x;
                      a__y;
                      Omega];
     
     % Predicted covariance of the estimation error
     Pk_1_pred = A*Pk*A' + G*Q*G';

end

