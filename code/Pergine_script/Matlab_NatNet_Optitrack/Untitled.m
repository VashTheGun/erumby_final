
% fixed 1

idx_fixed_x_1 = find(X1 > 1.525 & X1 < 1.565 );
idx_fixed_z_1 = find(Z1 > -4.5 & Z1 < -4.1 );
idx_fixed_1   = intersect(idx_fixed_x_1,idx_fixed_z_1);
x_fixed_1 = mean(X1(idx_fixed_1));
z_fixed_1 = mean(Z1(idx_fixed_1));

% fixed 2

idx_fixed_x_2 = find(X1 > 1.575 & X1 < 1.59 );
idx_fixed_z_2 = find(Z1 > -2.62 & Z1 < -2.57 );
idx_fixed_2   = intersect(idx_fixed_x_2,idx_fixed_z_2);
x_fixed_2 = mean(X1(idx_fixed_2));
z_fixed_2 = mean(Z1(idx_fixed_2));
% distance

DISTANCE_FIXED = sqrt( ( x_fixed_2 - x_fixed_1 )^2 +  ( z_fixed_2 - z_fixed_1 )^2  );

% mobile 1

idx_fixed_x_1 = find(X1 > 1.525 & X1 < 1.565 );
idx_fixed_z_1 = find(Z1 > -4.5 & Z1 < -4.1 );
idx_fixed_1   = intersect(idx_fixed_x_1,idx_fixed_z_1);
x_fixed_1 = mean(X1(idx_fixed_1));
z_fixed_1 = mean(Z1(idx_fixed_1));