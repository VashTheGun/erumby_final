% Optitrack Matlab / NatNet Polling Sample
%  Requirements:
%   - OptiTrack Motive 2.0 or later
%   - OptiTrack NatNet 3.0 or later
%   - Matlab R2013
% This sample connects to the server and displays rigid body data.
% natnet.p, needs to be located on the Matlab Path.

function NatNetPolling_MacBook_and_rasp
    fprintf( 'NatNet Polling Sample Start\n' )

    % create an instance of the natnet client class
    fprintf( 'Creating natnet class object\n' )
    natnetclient = natnet;

    % connect the client to the server (multicast over local loopback) -
    % modify for your network
    fprintf( 'Connecting to the server\n' )
    natnetclient.HostIP = '127.0.0.1';
    natnetclient.ClientIP = '127.0.0.1';
    natnetclient.ConnectionType = 'Multicast';
    natnetclient.connect;
    if ( natnetclient.IsConnected == 0 )
        fprintf( 'Client failed to connect\n' )
        fprintf( '\tMake sure the host is connected to the network\n' )
        fprintf( '\tand that the host and client IP addresses are correct\n\n' )
        return
    end

    % get the asset descriptions for the asset names
    model = natnetclient.getModelDescription;
    if ( model.RigidBodyCount < 1 )
        return
    end

    % Poll for the rigid body data a regular intervals (~1 sec) for 10 sec.
    fprintf( '\nPrinting rigid body frame data approximately every second for 10 seconds...\n\n' )
    udps_MacBook = dsp.UDPSender('RemoteIPAddress', '192.168.10.99','RemoteIPPort',25000);
    udps_rasp = dsp.UDPSender('RemoteIPAddress', '10.0.0.1','RemoteIPPort',25002);

    figure('color', 'white', 'name', 'NatNet');
    load matlab.mat
    load matlab_min.mat
    plot(xmin, ymin, 'r-');
    hold('on')
    plot(xplus, yplus, 'r-');
    xlim([-3, 9]);
    ylim([-2, 7]);
    axis('equal');
    grid('on');
    hold('on');
    label = text(-2.9, 5.9, 'Position');

    storedDataSent = zeros(1,3);
    jj = 1;
    
    mainLoop = true;

    while mainLoop
        %timerLoop = tic;
        % 		java.lang.Thread.sleep( 996 );
        pause(0.01)
        data = natnetclient.getFrame; % method to get current frame
%         if (isempty(data.RigidBody(1)))
%             fprintf( '\tPacket is empty/stale\n' );
%             fprintf( '\tMake sure the server is in Live mode or playing in playback\n\n');
%             break;
%         end
        % fprintf( 'Frame:%6d  ' , data.Frame )
        % fprintf( 'Time:%0.2f\n' , data.Timestamp )
        for i = 1:model.RigidBodyCount
            % fprintf( 'Name:"%s"  ', model.RigidBody( i ).Name )
            % fprintf( 'X:%0.1fmm  ', data.RigidBody( i ).x * 1000 )
            % fprintf( 'Y:%0.1fmm  ', data.RigidBody( i ).y * 1000 )
            q = quaternion( data.RigidBody( i ).qx, data.RigidBody( i ).qy, data.RigidBody( i ).qz, data.RigidBody( i ).qw ); % extrnal file quaternion.m
            % qRot = quaternion( 0, 0, 0, 1);    % rotate pitch 180 to avoid 180/-180 flip for nicer graphing
            % q = mtimes(q, qRot);
            angles = EulerAngles(q,'yzx');
            yaw = angles(1)*1000;
            % pitch = -angles(1) * 180.0 / pi;  % must invert due to 180 flip above
            % roll = -angles(3) * 180.0 / pi;  % must invert due to 180 flip above
            dataSent = [int16((data.RigidBody( i ).x * 1000)); int16((-data.RigidBody( i ).z * 1000)); int16(-yaw)];
            udps_MacBook(dataSent);
            udps_rasp(dataSent);
            % yaw1(idx) = -yaw*180/(pi*1000);
            % fprintf( 'yaw:%0.3fdeg\n',angles(1)*180/pi)

            % -----------------
            % UNCOMMENT THE FOLLOWING LINES TO STORE THE DATA SENT TO THE RASP AND MACBOOK (for debugging)
            % -----------------
            storedDataSent(jj,:) = dataSent';
            jj = jj+1;
            
            % -----------------
            % UNCOMMENT THE FOLLOWING LINES TO PLOT AND DISPLAY THE VEHICLE POSE IN REAL-TIME
            % -----------------
%             disp(dataSent');
%             fig = gcf;
%             if strcmp(fig.Name, 'NatNet')
%                 %fprintf(1,'P: (% 2.3f, % 2.3f) Y: % 3.3f  rad (% 3.3f  deg)\n', data.RigidBody( i ).x * 1000, data.RigidBody( i ).z * 1000, yaw/1000, rad2deg(yaw/1000));
%                 label.String = sprintf('P: (% 1.3f, % 1.3f) m Y: % 3.3f  rad (% 3.3f  deg)\n', data.RigidBody( i ).x, -data.RigidBody( i ).z, -yaw/1000, rad2deg(-yaw/1000));
%                 label.Position = [data.RigidBody( i ).x + 0.1, -data.RigidBody( i ).z + 0.1, 0];
%                 plot(data.RigidBody( i ).x, -data.RigidBody( i ).z, 'ko');
%                 xlim([-3, 9]);
%                 ylim([-2, 7]);
%             else
%                 mainLoop = false;
%             end
        end
    end
    disp('NatNet Polling Sample End' )
    release(udps_MacBook);
    release(udps_rasp);
    if mainLoop
        label.String(0, 0, 'Process ended');
    else
        close(gcf);
    end
end







