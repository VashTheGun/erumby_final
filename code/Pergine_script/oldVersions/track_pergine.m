%% track creation
clc;clear

addpath(genpath('./../../../../lib/matlab/Clothoids'));
% c++ convention
M_PI = pi;
 
 % track parameters
 x = [4, 0, 0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4];
 y = [6, 6, 0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6];
 theta = [M_PI, M_PI, 0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI];
 
  % track parameters
 x = [0, 2, 4, 6, 7, 7, 3, -1.72, 0, 3, 7, 7, 4, 0, 0];
 y = [0, 1, 1, 0, 0, 2, 2, 1.99, 5, 4, 4, 6, 6, 6, 0];
 theta = [0, 0, 0, 0, 0, M_PI, M_PI, 2.1025, 0, 0, 0, M_PI, M_PI, M_PI, 0];

S = ClothoidList() ;

for i = 1:length(x)-1
    S.push_back_G1(x(i), y(i), theta(i), x(i+1), y(i+1), theta(i+1) ) ; % track creation
end

S.plot
hold on
% plot(x_sim,y_sim,'+')

%%
abscissa = 0:0.01:S.length;
for i = 1:length(abscissa)
    [~,~,~,curvature(i)] = S.evaluate(abscissa(i));
end
width_L = 0.4*ones(size(curvature));
width_R = 0.4*ones(size(curvature));


test = "test1.txt";             % first file
fileID  = fopen(test, 'w');

fprintf(fileID, '% 8s\t % 8s\t % 8s\t % 8s\n', 'abscissa', 'curvature', 'width_L', 'width_R');
fprintf(fileID, '% 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\n', [abscissa; curvature; width_L; width_R ]);
fclose('all');