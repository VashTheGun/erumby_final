function [remap_telemetry, remap_optitrack] = test_data_remap_V4(telemetry, optitrack, align_variable, filter_flag, clean_gps_telem)

    % ------------------------------------------------------
    %% Function purposes: 
    %   --> align the Optitrack and telemetry data (if needed)
    %   --> filter the data and create the output structures
    % ------------------------------------------------------
    
    Ts_telem = telemetry.time(2) - telemetry.time(1);    % [s] sampling time for the telemetry data
    Ts_optiT = optitrack.time(501)-optitrack.time(500);  % [s] sampling time of optitrack system
    Ts_telem_interp = Ts_telem;   
    
    r = 0.05;   % [m] wheel radius
    
    % ----------------
    % Filter definitions
    % ----------------
    % Filter for the telemetry data
    lpf_tel = designfilt('lowpassiir', ...         % Response type
                 'FilterOrder',8, ...              % Filter order
                 'HalfPowerFrequency',0.15);       % cut off frequency 7.5 Hz (norm_cut_frq*Fs/2), with Fs=100Hz
    % Filter for the Optitrack data         
    lpf_optiT = designfilt('lowpassiir','FilterOrder',12, ...   % cut off freq 5 Hz (Fs=120Hz)
        'HalfPowerFrequency',0.09,'DesignMethod','butter');         
    
    if (filter_flag)
        telemetry.a_x_mpu = filtfilt(lpf_tel,telemetry.a_x_mpu);
        telemetry.a_y_mpu = filtfilt(lpf_tel,telemetry.a_y_mpu);
        telemetry.a_z_mpu = filtfilt(lpf_tel,telemetry.a_z_mpu);
        telemetry.gyro_z_mpu = filtfilt(lpf_tel, telemetry.gyro_z_mpu);
    end
    
    % Rescaling the Optitrack data sent to the Raspberry
    telemetry.x_gps = telemetry.x_gps/1000;
    telemetry.y_gps = telemetry.y_gps/1000;
    telemetry.yaw_gps = wrapTo2Pi(telemetry.yaw_gps/1000);
    
    % filling (i.e. replacing) NaN values
    optitrack.roll  = fillmissing(optitrack.roll,'nearest');
    optitrack.pitch = fillmissing(optitrack.pitch,'nearest');
    optitrack.yaw   = fillmissing(optitrack.yaw,'nearest');
    optitrack.X     = fillmissing(optitrack.X,'nearest');
    optitrack.Y     = fillmissing(optitrack.Y,'nearest');
    optitrack.Z     = fillmissing(optitrack.Z,'nearest');
    
    telemetry.x_gps = fillmissing(telemetry.x_gps,'nearest');
    telemetry.y_gps = fillmissing(telemetry.y_gps,'nearest');
    telemetry.yaw_gps = fillmissing(telemetry.yaw_gps,'nearest');
    
    % Fix the optitrack data sent to the rasp (UDP) and recorded on the rasp
    if (clean_gps_telem)
        num_samples_telem = length(telemetry.time);
        max_delay = 3;  % max n° of consecutive samples for which a wrong quantity 
                        % may have been saved (mainly due to delays in the UDP communication etc)
        
        % x_gps signal recorded by the rasp                
        new_indices_x_gps_tel = (1:num_samples_telem)';
        ii = 1;
        while (ii<=num_samples_telem-max_delay-1)
            if (telemetry.x_gps(min(ii+max_delay+1,num_samples_telem))==telemetry.x_gps(ii))
                % This can happen at the beginning of the test, when the car is still at rest
                for jj=max_delay+1:num_samples_telem
                    if (telemetry.x_gps(min(ii+jj,num_samples_telem))~=telemetry.x_gps(ii))
                        ii = ii+jj;
                        break;
                    end
                end
            else
                for jj=1:max_delay
                    if (telemetry.x_gps(min(ii+jj,num_samples_telem))==telemetry.x_gps(ii))
                        new_indices_x_gps_tel(min(ii+jj,num_samples_telem)) = NaN;
                        offs = 1;
                    else
                        offs = 0;
                        break;  % break the inner for loop
                    end
                end
                ii = ii+jj+offs;
            end
        end
        
        % y_gps signal recorded by the rasp                
        new_indices_y_gps_tel = (1:num_samples_telem)';
        ii = 1;
        while (ii<=num_samples_telem-max_delay-1)
            if (telemetry.y_gps(min(ii+max_delay+1,num_samples_telem))==telemetry.y_gps(ii))
                % This can happen at the beginning of the test, when the car is still at rest
                for jj=max_delay+1:num_samples_telem
                    if (telemetry.y_gps(min(ii+jj,num_samples_telem))~=telemetry.y_gps(ii))
                        ii = ii+jj;
                        break;
                    end
                end
            else
                for jj=1:max_delay
                    if (telemetry.y_gps(min(ii+jj,num_samples_telem))==telemetry.y_gps(ii))
                        new_indices_y_gps_tel(min(ii+jj,num_samples_telem)) = NaN;
                        offs = 1;
                    else
                        offs = 0;
                        break;  % break the inner for loop
                    end
                end
                ii = ii+jj+offs;
            end
        end
        
        % yaw_gps signal recorded by the rasp                
        new_indices_yaw_gps_tel = (1:num_samples_telem)';
        ii = 1;
        while (ii<=num_samples_telem-max_delay-1)
            if (telemetry.yaw_gps(min(ii+max_delay+1,num_samples_telem))==telemetry.yaw_gps(ii))
                % This can happen at the beginning of the test, when the car is still at rest
                for jj=max_delay+1:num_samples_telem
                    if (telemetry.yaw_gps(min(ii+jj,num_samples_telem))~=telemetry.yaw_gps(ii))
                        ii = ii+jj;
                        break;
                    end
                end
            else
                for jj=1:max_delay
                    if (telemetry.yaw_gps(min(ii+jj,num_samples_telem))==telemetry.yaw_gps(ii))
                        new_indices_yaw_gps_tel(min(ii+jj,num_samples_telem)) = NaN;
                        offs = 1;
                    else
                        offs = 0;
                        break;  % break the inner for loop
                    end
                end
                ii = ii+jj+offs;
            end
        end
        
        new_indices_x_gps_tel = new_indices_x_gps_tel((all((~isnan(new_indices_x_gps_tel)),2)),:);
        new_indices_x_gps_tel = new_indices_x_gps_tel(new_indices_x_gps_tel<=num_samples_telem-max_delay);
        new_indices_y_gps_tel = new_indices_y_gps_tel((all((~isnan(new_indices_y_gps_tel)),2)),:);
        new_indices_y_gps_tel = new_indices_y_gps_tel(new_indices_y_gps_tel<=num_samples_telem-max_delay);
        new_indices_yaw_gps_tel = new_indices_yaw_gps_tel((all((~isnan(new_indices_yaw_gps_tel)),2)),:);
        new_indices_yaw_gps_tel = new_indices_yaw_gps_tel(new_indices_yaw_gps_tel<=num_samples_telem-max_delay);

        telemetry.time_x_gps_clean = telemetry.time(new_indices_x_gps_tel);
        telemetry.x_gps_clean = telemetry.x_gps(new_indices_x_gps_tel);
        telemetry.time_y_gps_clean = telemetry.time(new_indices_y_gps_tel);
        telemetry.y_gps_clean = telemetry.y_gps(new_indices_y_gps_tel);
        telemetry.time_yaw_gps_clean = telemetry.time(new_indices_yaw_gps_tel);
        telemetry.yaw_gps_clean = telemetry.yaw_gps(new_indices_yaw_gps_tel);
        
        %Ts_telem_interp = 1e-3;
        telemetry.time_interp = 0:Ts_telem_interp:telemetry.time(end);
        telemetry.x_gps = interp1(telemetry.time_x_gps_clean,telemetry.x_gps_clean,telemetry.time_interp);
        telemetry.y_gps = interp1(telemetry.time_y_gps_clean,telemetry.y_gps_clean,telemetry.time_interp);
        telemetry.yaw_gps = interp1(telemetry.time_yaw_gps_clean,telemetry.yaw_gps_clean,telemetry.time_interp);
    end
    
    % unwrapping the angle measure
    yaw_tmp_optiT = unwrap(optitrack.yaw); 
    yaw_tmp_telem = unwrap(telemetry.yaw_gps);

    % derivative of position for obtain velocity
    Vx_optiT = ones(length(optitrack.X)-1,1)*NaN;  % initialize
    Vy_optiT = ones(length(optitrack.Y)-1,1)*NaN;  % initialize
    Vz_optiT = ones(length(optitrack.Z)-1,1)*NaN;  % initialize
    V_optiT  = ones(length(optitrack.X)-1,1)*NaN;  % initialize
    yaw_rate_optiT = ones(length(optitrack.yaw)-1,1)*NaN;  % initialize
    for i = 1:(length(optitrack.X)-1)
       Vx_optiT(i) = (((optitrack.X(i+1)-optitrack.X(i))/Ts_optiT)); 
       Vy_optiT(i) = ((optitrack.Y(i+1)-optitrack.Y(i))/Ts_optiT);
       Vz_optiT(i) = ((optitrack.Z(i+1)-optitrack.Z(i))/Ts_optiT);
       V_optiT(i)  = sqrt(Vx_optiT(i)^2+ Vy_optiT(i)^2 + Vz_optiT(i)^2);  % absolute velocity    
       yaw_rate_optiT(i) = ((yaw_tmp_optiT(i+1)-yaw_tmp_optiT(i))/Ts_optiT)*pi/180;
    end
    
    Vx_telem = ones(length(telemetry.x_gps)-1,1)*NaN;  % initialize
    Vy_telem = ones(length(telemetry.y_gps)-1,1)*NaN;  % initialize
    V_telem  = ones(length(telemetry.x_gps)-1,1)*NaN;  % initialize
    yaw_rate_telem = ones(length(telemetry.yaw_gps)-1,1)*NaN;  % initialize
    for i = 1:(length(telemetry.x_gps)-1)
       Vx_telem(i) = (((telemetry.x_gps(i+1)-telemetry.x_gps(i))/Ts_telem_interp)); 
       Vy_telem(i) = ((telemetry.y_gps(i+1)-telemetry.y_gps(i))/Ts_telem_interp);
       V_telem(i)  = sqrt(Vx_telem(i)^2+ Vy_telem(i)^2);  % absolute velocity    
       yaw_rate_telem(i) = ((yaw_tmp_telem(i+1)-yaw_tmp_telem(i))/Ts_telem_interp)*pi/180;
    end
    
    % encoder linear velocity (only freewheel)
    V_enc = r.*telemetry.enc_fr;  %r.*(telemetry.enc_fl+telemetry.enc_fr)./2;
    
    if (filter_flag)
        % filtering the optitrack data
        V_enc    = filtfilt(lpf_optiT,V_enc);
        V_optiT        = filtfilt(lpf_optiT,V_optiT);
        Vx_optiT       = filtfilt(lpf_optiT,Vx_optiT);
        Vy_optiT       = filtfilt(lpf_optiT,Vy_optiT);
        yaw_rate_optiT = filtfilt(lpf_optiT,yaw_rate_optiT);
    end
    
    % unwrapping
    optitrack.roll  = unwrap(optitrack.roll*pi/180);
    optitrack.yaw   = unwrap(optitrack.yaw*pi/180);
    optitrack.pitch = unwrap(optitrack.pitch*pi/180);
    
    % relative velocity
    for i = 1:length(optitrack.roll)-1
        R_x(:,:,i) = [ 1 0 0 0;
                       0 cos(optitrack.roll(i)) -sin(optitrack.roll(i)) 0;
                       0 sin(optitrack.roll(i)) cos(optitrack.roll(i)) 0;
                       0 0 0 1];
        R_y(:,:,i) = [ cos(optitrack.pitch(i)) 0 sin(optitrack.pitch(i)) 0;
                       0 1 0 0;
                       -sin(optitrack.pitch(i)) 0 cos(optitrack.pitch(i)) 0;
                       0 0 0 1];
        R_z(:,:,i) = [ cos(optitrack.yaw(i)) -sin(optitrack.yaw(i)) 0 0;
                       sin(optitrack.yaw(i)) cos(optitrack.yaw(i)) 0 0;
                       0 0 1 0;
                       0 0 0 1;];
        T(:,:,i) =   [ 1 0 0 optitrack.X(i);
                       0 1 0 optitrack.Y(i);
                       0 0 1 optitrack.Z(i);
                       0 0 0 1];

        V_relative(:,i) = R_z(:,:,i)'*[Vx_optiT(i); Vy_optiT(i); 0; 0];
    end
    
    if (~(strcmp(align_variable,'none')))
        % ----------------
        % Re-alignment of the data
        % ----------------
        % Re-sampling at a higher rate, before the alignment
        Ts_new  = 0.001;  % [s]          
        newtime = 0:Ts_new:telemetry.time(end);   % time vector resempled at a higher rate   
        if (strcmp(align_variable,'speed'))
            v_thresh = 0.2;  % [m/s] threshold value
            idx_thresh_V_enc   = find(V_enc>=v_thresh,1);
            idx_thresh_V_optiT = find(V_optiT>=v_thresh,1);
            time_window_xcorr  = 0.5;  % [s]
            fin_idx_xcorr_V_enc   = idx_thresh_V_enc + time_window_xcorr/Ts_telem;
            fin_idx_xcorr_V_optiT = idx_thresh_V_optiT + round(time_window_xcorr/Ts_optiT);
%             fin_idx_xcorr_V_enc   = length(V_enc);
%             fin_idx_xcorr_V_optiT = length(V_optiT);
            new_fin_time = min(telemetry.time(fin_idx_xcorr_V_enc),optitrack.time(fin_idx_xcorr_V_optiT));
            newtime_align = 0:Ts_new:new_fin_time;   % time vector resempled at a higher rate   
            newV_enc = interp1(telemetry.time(1:fin_idx_xcorr_V_enc), V_enc(1:fin_idx_xcorr_V_enc), newtime_align);   % upsampling of the two signals
            newV = interp1(optitrack.time(1:fin_idx_xcorr_V_optiT), V_optiT(1:fin_idx_xcorr_V_optiT), newtime_align);
            newV(isnan(newV)) = 0;
            var_optiT = newV;
            var_telem = newV_enc;
        elseif (strcmp(align_variable,'yaw_rate'))
            newtime_align = 0:Ts_new:telemetry.time(end);   % time vector resempled at a higher rate   
            newOmega_telem = interp1(telemetry.time, telemetry.gyro_z_mpu, newtime_align);   % upsampling of the two signals
            newOmega_optiT = interp1(optitrack.time(1:length(yaw_rate_optiT)), yaw_rate_optiT, newtime_align);
            newOmega_optiT(isnan(newOmega_optiT)) = 0;
            var_optiT = newOmega_optiT;
            var_telem = newOmega_telem;
        end

        % alignment of the data basing on the cross correlation
        [tmp,lag] = xcorr(var_optiT,var_telem);
        [~,I] = max(abs(tmp));
        lagDiff = lag(I);
        timeDiff = lagDiff*Ts_new;

        offset = abs(round(timeDiff/Ts_telem));  % find the time offset with cross correlation

        % ----------------
        % Telemetry realignment
        % ----------------
        a_x         = interp1(telemetry.time(1:end-offset), telemetry.a_x_mpu(1+offset:end), newtime)*9.8;
        a_y         = interp1(telemetry.time(1:end-offset), telemetry.a_y_mpu(1+offset:end), newtime)*9.8;
        a_z         = interp1(telemetry.time(1:end-offset), telemetry.a_z_mpu(1+offset:end), newtime)*9.8;
        gyro_x      = interp1(telemetry.time(1:end-offset), telemetry.gyro_x_mpu(1+offset:end), newtime);
        gyro_y      = interp1(telemetry.time(1:end-offset), telemetry.gyro_y_mpu(1+offset:end), newtime);
        gyro_z      = interp1(telemetry.time(1:end-offset), telemetry.gyro_z_mpu(1+offset:end), newtime);
        enc_fl      = interp1(telemetry.time(1:end-offset), telemetry.enc_fl(1+offset:end), newtime);
        enc_fr      = interp1(telemetry.time(1:end-offset), telemetry.enc_fr(1+offset:end), newtime);
        enc_rl      = interp1(telemetry.time(1:end-offset), telemetry.enc_rl(1+offset:end), newtime);
        enc_rr      = interp1(telemetry.time(1:end-offset), telemetry.enc_rr(1+offset:end), newtime);
        traction    = interp1(telemetry.time(1:end-offset), telemetry.traction(1+offset:end), newtime);
        steering    = interp1(telemetry.time(1:end-offset), telemetry.steering(1+offset:end), newtime);
        target      = interp1(telemetry.time(1:end-offset), telemetry.target(1+offset:end), newtime);
        V_enc       = interp1(telemetry.time(1:end-offset), V_enc(1+offset:end), newtime);
        x_gps_tel   = interp1(telemetry.time(1:end-offset), telemetry.x_gps(1+offset:end), newtime);
        y_gps_tel   = interp1(telemetry.time(1:end-offset), telemetry.y_gps(1+offset:end), newtime);  
        yaw_gps_tel = interp1(telemetry.time(1:end-offset), telemetry.yaw_gps(1+offset:end), newtime);

        a_x(isnan(a_x))                 = 0;
        a_y(isnan(a_y))                 = 0;
        a_z(isnan(a_z))                 = 0;
        gyro_x(isnan(gyro_x))           = 0;
        gyro_y(isnan(gyro_y))           = 0;
        gyro_z(isnan(gyro_z))           = 0;
        enc_fl(isnan(enc_fl))           = 0;
        enc_fr(isnan(enc_fr))           = 0;
        enc_rl(isnan(enc_rl))           = 0;
        enc_rr(isnan(enc_rr))           = 0;
        traction(isnan(traction))       = 0;
        steering(isnan(steering))       = 0;    
        target(isnan(target))           = 0;  
        V_enc(isnan(V_enc))             = 0;
        x_gps_tel(isnan(x_gps_tel))     = 0; 
        y_gps_tel(isnan(y_gps_tel))     = 0; 
        yaw_gps_tel(isnan(yaw_gps_tel)) = 0;

        % ----------------
        % Optitrack realignemt
        % ----------------
        x_gps = interp1(optitrack.time, optitrack.X, newtime);
        x_gps(isnan(x_gps)) = 0;
        y_gps = interp1(optitrack.time, optitrack.Y, newtime);
        y_gps(isnan(y_gps)) = 0;
        z_gps = interp1(optitrack.time, optitrack.Z, newtime);
        z_gps(isnan(z_gps)) = 0;
        yaw = interp1(optitrack.time, optitrack.yaw, newtime);
        yaw(isnan(yaw)) = 0;
        roll = interp1(optitrack.time, optitrack.roll, newtime);
        roll(isnan(roll)) = 0;
        pitch = interp1(optitrack.time, optitrack.pitch, newtime);
        pitch(isnan(pitch)) = 0;
        V_gps = interp1(optitrack.time(1:end-1), V_optiT, newtime);
        V_gps(isnan(V_gps)) = 0; 
        Vx_gps = interp1(optitrack.time(1:end-1), Vx_optiT, newtime);
        Vx_gps(isnan(Vx_gps)) = 0; 
        Vy_gps = interp1(optitrack.time(1:end-1), Vy_optiT, newtime);
        Vy_gps(isnan(Vy_gps)) = 0; 
        yaw_rate_gps = interp1(optitrack.time(1:end-1), yaw_rate_optiT, newtime);
        yaw_rate_gps(isnan(yaw_rate_gps)) = 0;   
        Vx_rel = interp1(optitrack.time(1:end-1), V_relative(1,:), newtime);
        Vx_rel(isnan(Vx_rel)) = 0; 
        Vy_rel = interp1(optitrack.time(1:end-1), V_relative(2,:), newtime);
        Vy_rel(isnan(Vy_rel)) = 0; 
        
        % ----------------
        % Creation of the output structures
        % ----------------
        remap_telemetry = struct('time',newtime,'a_x',a_x,'a_y',a_y,'a_z',a_z,...
                    'gyro_x',gyro_x,'gyro_y',gyro_y,'gyro_z',gyro_z,...
                    'enc_fr',enc_fr,'enc_fl',enc_fl,'enc_rr',enc_rr,'enc_rl',enc_rl,...
                    'traction',traction,'steering',steering,'target',target,'V_enc',V_enc,...
                    'x_gps', x_gps_tel,'y_gps', y_gps_tel,'yaw_gps', yaw_gps_tel);

        remap_optitrack = struct('time',newtime,'roll',roll,'pitch',pitch,'yaw',yaw,'x_gps',x_gps,'y_gps',y_gps,...
                                'z_gps',z_gps,'V_gps',V_gps,'Vx_gps',Vx_gps,'Vy_gps',Vy_gps,'yaw_rate_gps',yaw_rate_gps,...
                                'Vx_rel',Vx_rel,'Vy_rel',Vy_rel);
    else
        % ----------------
        % No data alignment 
        % ----------------
        a_x         = telemetry.a_x_mpu*9.8;
        a_y         = telemetry.a_y_mpu*9.8;
        a_z         = telemetry.a_z_mpu*9.8;
        gyro_x      = telemetry.gyro_x_mpu;
        gyro_y      = telemetry.gyro_y_mpu;
        gyro_z      = telemetry.gyro_z_mpu;
        enc_fl      = telemetry.enc_fl;
        enc_fr      = telemetry.enc_fr;
        enc_rl      = telemetry.enc_rl;
        enc_rr      = telemetry.enc_rr;
        traction    = telemetry.traction;
        steering    = telemetry.steering;
        target      = telemetry.target;
        V_enc       = V_enc;
        x_gps_tel   = telemetry.x_gps;
        y_gps_tel   = telemetry.y_gps;  
        yaw_gps_tel = telemetry.yaw_gps;
        
        V_gps_tel   = V_telem;
        Vx_gps_tel  = Vx_telem;
        Vy_gps_tel  = Vy_telem;
        yaw_rate_gps_tel = yaw_rate_telem;

        a_x(isnan(a_x))                 = 0;
        a_y(isnan(a_y))                 = 0;
        a_z(isnan(a_z))                 = 0;
        gyro_x(isnan(gyro_x))           = 0;
        gyro_y(isnan(gyro_y))           = 0;
        gyro_z(isnan(gyro_z))           = 0;
        enc_fl(isnan(enc_fl))           = 0;
        enc_fr(isnan(enc_fr))           = 0;
        enc_rl(isnan(enc_rl))           = 0;
        enc_rr(isnan(enc_rr))           = 0;
        traction(isnan(traction))       = 0;
        steering(isnan(steering))       = 0;    
        target(isnan(target))           = 0;  
        V_enc(isnan(V_enc))             = 0;
        x_gps_tel(isnan(x_gps_tel))     = 0; 
        y_gps_tel(isnan(y_gps_tel))     = 0; 
        yaw_gps_tel(isnan(yaw_gps_tel)) = 0;
        V_gps_tel(isnan(V_gps_tel))     = 0;
        Vx_gps_tel(isnan(Vx_gps_tel))   = 0;
        Vy_gps_tel(isnan(Vy_gps_tel))   = 0;
        yaw_rate_gps_tel(isnan(yaw_rate_gps_tel)) = 0;

        % ----------------
        % Optitrack realignemt
        % ----------------
        x_gps = optitrack.X;
        x_gps(isnan(x_gps)) = 0;
        y_gps = optitrack.Y;
        y_gps(isnan(y_gps)) = 0;
        z_gps = optitrack.Z;
        z_gps(isnan(z_gps)) = 0;
        yaw = optitrack.yaw;
        yaw(isnan(yaw)) = 0;
        roll = optitrack.roll;
        roll(isnan(roll)) = 0;
        pitch = optitrack.pitch;
        pitch(isnan(pitch)) = 0;
        V_gps = V_optiT;
        V_gps(isnan(V_gps)) = 0; 
        Vx_gps = Vx_optiT;
        Vx_gps(isnan(Vx_gps)) = 0; 
        Vy_gps = Vy_optiT;
        Vy_gps(isnan(Vy_gps)) = 0; 
        yaw_rate_gps = yaw_rate_optiT;
        yaw_rate_gps(isnan(yaw_rate_gps)) = 0;   
        Vx_rel = V_relative(1,:);
        Vx_rel(isnan(Vx_rel)) = 0; 
        Vy_rel = V_relative(2,:);
        Vy_rel(isnan(Vy_rel)) = 0; 
        
        % ----------------
        % Creation of the output structures
        % ----------------
        remap_telemetry = struct('time',telemetry.time,'a_x',a_x,'a_y',a_y,'a_z',a_z,...
                    'gyro_x',gyro_x,'gyro_y',gyro_y,'gyro_z',gyro_z,...
                    'enc_fr',enc_fr,'enc_fl',enc_fl,'enc_rr',enc_rr,'enc_rl',enc_rl,...
                    'traction',traction,'steering',steering,'target',target,'V_enc',V_enc,...
                    'x_gps_tel',x_gps_tel, 'y_gps_tel',y_gps_tel, 'yaw_gps_tel',yaw_gps_tel,...
                    'V_gps_tel',V_gps_tel, 'Vx_gps_tel',Vx_gps_tel, 'Vy_gps_tel',Vy_gps_tel,...
                    'yaw_rate_gps_tel',yaw_rate_gps_tel);

        remap_optitrack = struct('time',optitrack.time,'roll',roll,'pitch',pitch,'yaw',yaw,'x_gps',x_gps,'y_gps',y_gps,...
                                'z_gps',z_gps,'V_gps',V_gps,'Vx_gps',Vx_gps,'Vy_gps',Vy_gps,'yaw_rate_gps',yaw_rate_gps,...
                                'Vx_rel',Vx_rel,'Vy_rel',Vy_rel);
    end

end