function [telemetry] = telemetry_import(filename)
    delimiter = '\t';
    startRow = 4;

    %% Format for each line of text:
    %   column1: double (%f)
    %	column2: double (%f)
    %   column3: double (%f)
    %	column4: double (%f)
    %   column5: double (%f)
    %	column6: double (%f)
    %   column7: double (%f)
    %	column8: double (%f)
    %   column9: double (%f)
    %	column10: double (%f)
    %   column11: double (%f)
    %	column12: double (%f)
    %   column13: double (%f)
    %	column14: double (%f)
    %   column15: double (%f)
    %	column16: double (%f)
    %   column17: double (%f)
    % For more information, see the TEXTSCAN documentation.
    formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

    %% Open the text file.
    fileID = fopen(filename,'r');

    %% Read columns of data according to the format.
    % This call is based on the structure of the file used to generate this
    % code. If an error occurs for a different file, try regenerating the code
    % from the Import Tool.
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'EmptyValue', NaN, 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');

    %% Close the text file.
    fclose(fileID);

    %% Post processing for unimportable data.
    % No unimportable data rules were applied during the import, so no post
    % processing code is included. To generate code which works for
    % unimportable data, select unimportable cells in a file and regenerate the
    % script.

    %% Allocate imported array to column variable names
    time = dataArray{:, 1};
    a_x_mpu = dataArray{:, 2};
    a_y_mpu = dataArray{:, 3};
    a_z_mpu = dataArray{:, 4};
    gyro_x_mpu = dataArray{:, 5};
    gyro_y_mpu = dataArray{:, 6};
    gyro_z_mpu = dataArray{:, 7};
    enc_fr = dataArray{:, 8};
    enc_fl = dataArray{:, 9};
    enc_rr = dataArray{:, 10};
    enc_rl = dataArray{:, 11};
    a_x_raw = dataArray{:, 12};
    a_y_raw = dataArray{:, 13};
    a_z_raw = dataArray{:, 14};
    steering = dataArray{:, 15};
    traction = dataArray{:, 16};
    target = dataArray{:, 17};

    telemetry = struct('time',time,'a_x_mpu',a_x_mpu,'a_y_mpu',a_y_mpu,'a_z_mpu',a_z_mpu,...
                        'gyro_x_mpu',gyro_x_mpu,'gyro_y_mpu',gyro_y_mpu,'gyro_z_mpu',gyro_z_mpu,...
                        'enc_fr',enc_fr,'enc_fl',enc_fl,'enc_rr',enc_rr,'enc_rl',enc_rl,...
                        'steering',steering,'traction',traction,'target',target);

end