function [remap_telemetry, remap_optitrack] = test_data_remap_V2(telemetry, optitrack)

    % function for allign the data of two different system with correlation
    % the script filter the data and create the output structure
    
    % constant initialization
    %
    dt = optitrack.time(501)-optitrack.time(500);           % sampling time of optitrack system
    r = 0.05;                           % wheel radius
    
    % filtering
    
    lpf_tel = designfilt('lowpassiir', ...         % Response type
                 'FilterOrder',8, ...              % Filter order
                 'HalfPowerFrequency',0.15);       % cut of frequency 7.5 Hz (norm_cut_frq*fs/2)
    
    telemetry.a_x_mpu = filtfilt(lpf_tel,telemetry.a_x_mpu);
    telemetry.a_y_mpu = filtfilt(lpf_tel,telemetry.a_y_mpu);
    telemetry.a_z_mpu = filtfilt(lpf_tel,telemetry.a_z_mpu);
    telemetry.gyro_z_mpu = filtfilt(lpf_tel, telemetry.gyro_z_mpu);
    
    % filling NaN value
    optitrack.roll = fillmissing(optitrack.roll,'nearest');
    optitrack.pitch = fillmissing(optitrack.pitch,'nearest');
    optitrack.yaw = fillmissing(optitrack.yaw,'nearest');

    optitrack.X = fillmissing(optitrack.X,'nearest');
    optitrack.Y = fillmissing(optitrack.Y,'nearest');
    optitrack.Z = fillmissing(optitrack.Z,'nearest');
    
    % unwrapping the angle measure
    yaw_tmp = unwrap(optitrack.yaw); 

    % derivative of position for obtain velocity
    for i = 1:(length(optitrack.X)-1)
       Vx(i) = (((optitrack.X(i+1)-optitrack.X(i))/dt)); 
       Vy(i) = ((optitrack.Y(i+1)-optitrack.Y(i))/dt);
       Vz(i) = ((optitrack.Z(i+1)-optitrack.Z(i))/dt);
       V(i) = sqrt(Vx(i)^2+ Vy(i)^2 + Vz(i)^2);                 % absolute velocity    
       yaw_rate(i) = ((yaw_tmp(i+1)-yaw_tmp(i))/dt)*pi/180;
    end

    % encoder linear velocity (only freewheel)
    V_enc = r.*(telemetry.enc_fl+telemetry.enc_fr)./2;
    
    % filtering the optitrack data

    lpf = designfilt('lowpassiir','FilterOrder',12, ...         % cut off freq 5 Hz
        'HalfPowerFrequency',0.09,'DesignMethod','butter');
    V_enc = filtfilt(lpf,V_enc);
    V = filtfilt(lpf,V);
    Vx = filtfilt(lpf,Vx);
    Vy = filtfilt(lpf,Vy);
    yaw_rate = filtfilt(lpf,yaw_rate);
    
    
    % start the allignment of the data
    
    newtime = 0:0.001:telemetry.time(end);                      % time vector resempled at 1000 Hz             

    newV_enc = interp1(telemetry.time, V_enc, newtime);         % upsampling of the two signal
    newV = interp1(optitrack.time(1:length(V)), V, newtime);
    newV(isnan(newV))=0;

    dt1 = newV(2)-newV(1);

    [tmp,lag] = xcorr(newV,newV_enc);
    [~,I] = max(abs(tmp));
    lagDiff = lag(I);
    timeDiff = lagDiff*0.001;
    
    offset = abs(round(timeDiff/0.01));                         % find the time offset wit crosscorrelation
    dt = 0.001;

    % telemetry realignment
    a_x = interp1(telemetry.time(1:end-offset), telemetry.a_x_mpu(1+offset:end), newtime)*9.8;
    a_y = interp1(telemetry.time(1:end-offset), telemetry.a_y_mpu(1+offset:end), newtime)*9.8;
    a_z = interp1(telemetry.time(1:end-offset), telemetry.a_z_mpu(1+offset:end), newtime)*9.8;
    gyro_x = interp1(telemetry.time(1:end-offset), telemetry.gyro_x_mpu(1+offset:end), newtime);
    gyro_y = interp1(telemetry.time(1:end-offset), telemetry.gyro_y_mpu(1+offset:end), newtime);
    gyro_z = interp1(telemetry.time(1:end-offset), telemetry.gyro_z_mpu(1+offset:end), newtime);
    enc_fl = interp1(telemetry.time(1:end-offset), telemetry.enc_fl(1+offset:end), newtime);
    enc_fr = interp1(telemetry.time(1:end-offset), telemetry.enc_fr(1+offset:end), newtime);
    enc_rl = interp1(telemetry.time(1:end-offset), telemetry.enc_rl(1+offset:end), newtime);
    enc_rr = interp1(telemetry.time(1:end-offset), telemetry.enc_rr(1+offset:end), newtime);
    traction = interp1(telemetry.time(1:end-offset), telemetry.traction(1+offset:end), newtime);
    steering = interp1(telemetry.time(1:end-offset), telemetry.steering(1+offset:end), newtime);
    target = interp1(telemetry.time(1:end-offset), telemetry.target(1+offset:end), newtime);
    V_enc = interp1(telemetry.time(1:end-offset), V_enc(1+offset:end), newtime);
    x_gps_tel = interp1(telemetry.time(1:end-offset), telemetry.x_gps(1+offset:end), newtime);
    y_gps_tel = interp1(telemetry.time(1:end-offset), telemetry.y_gps(1+offset:end), newtime);  
    yaw_gps_tel = interp1(telemetry.time(1:end-offset), telemetry.yaw_gps(1+offset:end), newtime);    

    a_x(isnan(a_x))=0;
    a_y(isnan(a_y))=0;
    a_z(isnan(a_z))=0;
    gyro_x(isnan(gyro_x))=0;
    gyro_y(isnan(gyro_y))=0;
    gyro_z(isnan(gyro_z))=0;
    enc_fl(isnan(enc_fl))=0;
    enc_fr(isnan(enc_fr))=0;
    enc_rl(isnan(enc_rl))=0;
    enc_rr(isnan(enc_rr))=0;
    traction(isnan(traction))=0;
    steering(isnan(steering))=0;    
    target(isnan(target))=0;  
    V_enc(isnan(V_enc))=0;
    
    



    % optitrack realignemt
    x_gps = interp1(optitrack.time, optitrack.X, newtime);
    x_gps(isnan(x_gps))=0;
    y_gps = interp1(optitrack.time, optitrack.Y, newtime);
    y_gps(isnan(y_gps))=0;
    z_gps = interp1(optitrack.time, optitrack.Z, newtime);
    z_gps(isnan(z_gps))=0;
    yaw = interp1(optitrack.time, optitrack.yaw, newtime)*pi/180;
    yaw(isnan(yaw))=0;
    roll = interp1(optitrack.time, optitrack.roll, newtime)*pi/180;
    roll(isnan(roll))=0;
    pitch = interp1(optitrack.time, optitrack.pitch, newtime)*pi/180;
    pitch(isnan(pitch))=0;
    V_gps = interp1(optitrack.time(1:end-1), V, newtime);
    V_gps(isnan(V_gps))=0; 
    Vx_gps = interp1(optitrack.time(1:end-1), Vx, newtime);
    Vx_gps(isnan(Vx_gps))=0; 
    Vy_gps = interp1(optitrack.time(1:end-1), Vy, newtime);
    Vy_gps(isnan(Vy_gps))=0; 
    yaw_rate_gps = interp1(optitrack.time(1:end-1), yaw_rate, newtime);
    yaw_rate_gps(isnan(yaw_rate_gps))=0; 
    
    % creation of the structure
    
    remap_telemetry = struct('time',newtime,'a_x',a_x,'a_y',a_y,'a_z',a_z,...
                'gyro_x',gyro_x,'gyro_y',gyro_y,'gyro_z',gyro_z,...
                'enc_fr',enc_fr,'enc_fl',enc_fl,'enc_rr',enc_rr,'enc_rl',enc_rl,...
                'traction',traction,'steering',steering,'target',target,'V_enc',V_enc,...
                'x_gps', x_gps_tel,'y_gps', y_gps_tel,'yaw_gps', yaw_gps_tel);
                
    remap_optitrack = struct('time',newtime,'roll',roll,'pitch',pitch,'yaw',yaw,'x_gps',x_gps,'y_gps',y_gps,...
                            'z_gps',z_gps,'V_gps',V_gps,'Vx_gps',Vx_gps,'Vy_gps',Vy_gps,'yaw_rate_gps',yaw_rate_gps);

end