function sensorsData = extractSensorsData(remap_telemetry,remap_optitrack,vehicle_data)

    % -----------------------------------------------------
    %% function purpose:  extract and save sensors data
    % -----------------------------------------------------
    %     --> subscript _T is used for telemetry data
    %     --> subscript _O is used for optitrack data
    
    % ------------------
    %% Load vehicle data
    % ------------------
    Lf = vehicle_data.vehicle.Lf;
    Lr = vehicle_data.vehicle.Lr;
    r_tire = vehicle_data.vehicle.r_tire;
    delta_offs = vehicle_data.vehicle.delta_offs;
    
    % ------------------
    %% Extract and save telemetry and OptiTrack data
    % ------------------
    
    % --------
    % Telemetry data
    % --------
    time_T  = remap_telemetry.time;
    delta_T = remap_telemetry.steering - delta_offs;  %-((Lf+Lr)*(0.0404 + 0.0170*remap_telemetry.steering));  % [rad]
    Omega_T = remap_telemetry.gyro_z;
    Speed_T = remap_telemetry.V_enc;
    Ay_T    = remap_telemetry.a_y;
    Ax_T    = remap_telemetry.a_x;
    omega_rr_T = remap_telemetry.enc_rr;
    omega_rl_T = remap_telemetry.enc_rl;
    omega_fr_T = remap_telemetry.enc_fr;
    omega_fl_T = remap_telemetry.enc_fl;
    traction_T = remap_telemetry.traction;
    target_speed_T = remap_telemetry.target*r_tire/100;
    x_gps_T     = remap_telemetry.x_gps_tel;
    y_gps_T     = remap_telemetry.y_gps_tel;
    psi_gps_T   = remap_telemetry.yaw_gps_tel;
    Speed_gps_T = remap_telemetry.V_gps_tel;
    u_gps_T     = remap_telemetry.Vx_gps_tel;
    v_gps_T     = remap_telemetry.Vy_gps_tel;
    Omega_gps_T = remap_telemetry.yaw_rate_gps_tel;
    
    sensorsData.time_T         = time_T;
    sensorsData.delta          = delta_T;
    sensorsData.Omega_T        = Omega_T;
    sensorsData.Omega_gps_T    = Omega_gps_T;
    sensorsData.Speed_T        = Speed_T;
    sensorsData.Speed_gps_T    = Speed_gps_T;
    sensorsData.Ay_T           = Ay_T;
    sensorsData.Ax_T           = Ax_T;
    sensorsData.x_gps_T        = x_gps_T;
    sensorsData.y_gps_T        = y_gps_T;
    sensorsData.psi_gps_T      = psi_gps_T;
    sensorsData.omega_rr_T     = omega_rr_T;
    sensorsData.omega_rl_T     = omega_rl_T;
    sensorsData.omega_fr_T     = omega_fr_T;
    sensorsData.omega_fl_T     = omega_fl_T;
    sensorsData.traction_T     = traction_T;
    sensorsData.target_speed_T = target_speed_T;
    
    if (isstruct(remap_optitrack))
        % --------
        % OptiTrack data
        % --------
        time_O  = remap_optitrack.time;
        x_O     = remap_optitrack.x_gps;
        y_O     = remap_optitrack.y_gps;
        psi_O   = remap_optitrack.yaw;
        Omega_O = remap_optitrack.yaw_rate_gps;
        u_O     = remap_optitrack.Vx_gps;        % longitudinal speed [m/s]
        v_O     = remap_optitrack.Vy_gps;        % lateral speed [m/s]
        Speed_O = sqrt(u_O.^2 + v_O.^2);         % total speed [m/s]
        Ay_ss_O = Speed_O.*Omega_O;

        sensorsData.time_O  = time_O;
        sensorsData.Omega_O = Omega_O;
        sensorsData.Speed   = Speed_O;
        sensorsData.Ay_ss_O = Ay_ss_O;
        sensorsData.x_O     = x_O;
        sensorsData.y_O     = y_O;
        sensorsData.psi_O   = psi_O;
    end

end

