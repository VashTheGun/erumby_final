function remap_telemetry = test_data_remap_V4_no_optitrack(telemetry,filter_flag)

    % ------------------------------------------------------
    %% Function purposes: 
    %   --> filter the data (if needed) and create the output structures
    % ------------------------------------------------------
    
    Ts_telem = telemetry.time(2) - telemetry.time(1);    % [s] sampling time for the telemetry data
    Ts_telem_interp = Ts_telem;   
    
    r = 0.05;   % [m] wheel radius
    
    % ----------------
    % Filter definitions
    % ----------------
    % Filter for the telemetry data
    lpf_tel = designfilt('lowpassiir', ...         % Response type
                 'FilterOrder',8, ...              % Filter order
                 'HalfPowerFrequency',0.15);       % cut off frequency 7.5 Hz (norm_cut_frq*Fs/2), with Fs=100Hz       
    
    if (filter_flag)
        telemetry.a_x_mpu = filtfilt(lpf_tel,telemetry.a_x_mpu);
        telemetry.a_y_mpu = filtfilt(lpf_tel,telemetry.a_y_mpu);
        telemetry.a_z_mpu = filtfilt(lpf_tel,telemetry.a_z_mpu);
        telemetry.gyro_z_mpu = filtfilt(lpf_tel, telemetry.gyro_z_mpu);
    end
    
    % Rescaling the Optitrack data sent to the Raspberry
    telemetry.x_gps = telemetry.x_gps/1000;
    telemetry.y_gps = telemetry.y_gps/1000;
    telemetry.yaw_gps = wrapTo2Pi(telemetry.yaw_gps/1000);
    
    telemetry.x_gps = fillmissing(telemetry.x_gps,'nearest');
    telemetry.y_gps = fillmissing(telemetry.y_gps,'nearest');
    telemetry.yaw_gps = fillmissing(telemetry.yaw_gps,'nearest');
    
    % unwrapping the angle measure
    yaw_tmp_telem = unwrap(telemetry.yaw_gps);
    
    Vx_telem = ones(length(telemetry.x_gps)-1,1)*NaN;  % initialize
    Vy_telem = ones(length(telemetry.y_gps)-1,1)*NaN;  % initialize
    V_telem  = ones(length(telemetry.x_gps)-1,1)*NaN;  % initialize
    yaw_rate_telem = ones(length(telemetry.yaw_gps)-1,1)*NaN;  % initialize
    for i = 1:(length(telemetry.x_gps)-1)
       Vx_telem(i) = (((telemetry.x_gps(i+1)-telemetry.x_gps(i))/Ts_telem_interp)); 
       Vy_telem(i) = ((telemetry.y_gps(i+1)-telemetry.y_gps(i))/Ts_telem_interp);
       V_telem(i)  = sqrt(Vx_telem(i)^2+ Vy_telem(i)^2);  % absolute velocity    
       yaw_rate_telem(i) = ((yaw_tmp_telem(i+1)-yaw_tmp_telem(i))/Ts_telem_interp)*pi/180;
    end
    
    % encoder linear velocity (only freewheel)
    V_enc = r.*telemetry.enc_fr;  %r.*(telemetry.enc_fl+telemetry.enc_fr)./2;
    
    % ----------------
    % No data alignment 
    % ----------------
    a_x         = telemetry.a_x_mpu*9.8;
    a_y         = telemetry.a_y_mpu*9.8;
    a_z         = telemetry.a_z_mpu*9.8;
    gyro_x      = telemetry.gyro_x_mpu;
    gyro_y      = telemetry.gyro_y_mpu;
    gyro_z      = telemetry.gyro_z_mpu;
    enc_fl      = telemetry.enc_fl;
    enc_fr      = telemetry.enc_fr;
    enc_rl      = telemetry.enc_rl;
    enc_rr      = telemetry.enc_rr;
    traction    = telemetry.traction;
    steering    = telemetry.steering;
    target      = telemetry.target;
    V_enc       = V_enc;
    x_gps_tel   = telemetry.x_gps;
    y_gps_tel   = telemetry.y_gps;  
    yaw_gps_tel = telemetry.yaw_gps;

    V_gps_tel   = V_telem;
    Vx_gps_tel  = Vx_telem;
    Vy_gps_tel  = Vy_telem;
    yaw_rate_gps_tel = yaw_rate_telem;

    a_x(isnan(a_x))                 = 0;
    a_y(isnan(a_y))                 = 0;
    a_z(isnan(a_z))                 = 0;
    gyro_x(isnan(gyro_x))           = 0;
    gyro_y(isnan(gyro_y))           = 0;
    gyro_z(isnan(gyro_z))           = 0;
    enc_fl(isnan(enc_fl))           = 0;
    enc_fr(isnan(enc_fr))           = 0;
    enc_rl(isnan(enc_rl))           = 0;
    enc_rr(isnan(enc_rr))           = 0;
    traction(isnan(traction))       = 0;
    steering(isnan(steering))       = 0;    
    target(isnan(target))           = 0;  
    V_enc(isnan(V_enc))             = 0;
    x_gps_tel(isnan(x_gps_tel))     = 0; 
    y_gps_tel(isnan(y_gps_tel))     = 0; 
    yaw_gps_tel(isnan(yaw_gps_tel)) = 0;
    V_gps_tel(isnan(V_gps_tel))     = 0;
    Vx_gps_tel(isnan(Vx_gps_tel))   = 0;
    Vy_gps_tel(isnan(Vy_gps_tel))   = 0;
    yaw_rate_gps_tel(isnan(yaw_rate_gps_tel)) = 0;

    % ----------------
    % Creation of the output structures
    % ----------------
    remap_telemetry = struct('time',telemetry.time,'a_x',a_x,'a_y',a_y,'a_z',a_z,...
                'gyro_x',gyro_x,'gyro_y',gyro_y,'gyro_z',gyro_z,...
                'enc_fr',enc_fr,'enc_fl',enc_fl,'enc_rr',enc_rr,'enc_rl',enc_rl,...
                'traction',traction,'steering',steering,'target',target,'V_enc',V_enc,...
                'x_gps_tel',x_gps_tel, 'y_gps_tel',y_gps_tel, 'yaw_gps_tel',yaw_gps_tel,...
                'V_gps_tel',V_gps_tel, 'Vx_gps_tel',Vx_gps_tel, 'Vy_gps_tel',Vy_gps_tel,...
                'yaw_rate_gps_tel',yaw_rate_gps_tel);

end