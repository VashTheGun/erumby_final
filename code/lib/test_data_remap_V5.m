function [remap_telemetry, remap_optitrack] = test_data_remap_V5(telemetry, optitrack, align_variable, filter_flag, MPC_flag)

    % ------------------------------------------------------
    %% Function purposes: 
    %   --> align the Optitrack and telemetry data (if needed)
    %   --> filter the data and create the output structures
    % ------------------------------------------------------
    
    Ts_telem = mean(diff(telemetry.time));    % [s] sampling time for the telemetry data
    Ts_optiT = mean(diff(optitrack.time));    % [s] sampling time of optitrack system
    Ts_telem_interp = Ts_telem;   
    
    r = 0.05;   % [m] wheel radius
    
    % ----------------
    % Filter definitions
    % ----------------
    % Filter for the telemetry data
    lpf_tel = designfilt('lowpassiir', ...         % Response type
                 'FilterOrder',8, ...              % Filter order
                 'HalfPowerFrequency',0.15);       % cut off frequency 7.5 Hz (norm_cut_frq*Fs/2), with Fs=100Hz
    % Filter for the Optitrack data         
    lpf_optiT = designfilt('lowpassiir','FilterOrder',12, ...   % cut off freq 5 Hz (Fs=120Hz)
        'HalfPowerFrequency',0.09,'DesignMethod','butter');         
    
    if (filter_flag)
        telemetry.a_x_mpu = filtfilt(lpf_tel,telemetry.a_x_mpu);
        telemetry.a_y_mpu = filtfilt(lpf_tel,telemetry.a_y_mpu);
        telemetry.a_z_mpu = filtfilt(lpf_tel,telemetry.a_z_mpu);
        telemetry.gyro_z_mpu = filtfilt(lpf_tel, telemetry.gyro_z_mpu);
    end
    
    % Rescaling the Optitrack data sent to the Raspberry
    telemetry.x_gps = telemetry.x_gps/1000;
    telemetry.y_gps = telemetry.y_gps/1000;
    telemetry.yaw_gps = wrapTo2Pi(telemetry.yaw_gps/1000);
    
    % filling (i.e. replacing) NaN values
    optitrack.roll  = fillmissing(optitrack.roll,'nearest');
    optitrack.pitch = fillmissing(optitrack.pitch,'nearest');
    optitrack.yaw   = fillmissing(optitrack.yaw,'nearest');
    optitrack.X     = fillmissing(optitrack.X,'nearest');
    optitrack.Y     = fillmissing(optitrack.Y,'nearest');
    optitrack.Z     = fillmissing(optitrack.Z,'nearest');
    
    telemetry.x_gps = fillmissing(telemetry.x_gps,'nearest');
    telemetry.y_gps = fillmissing(telemetry.y_gps,'nearest');
    telemetry.yaw_gps = fillmissing(telemetry.yaw_gps,'nearest');
    
    % unwrapping the angle measure
    yaw_tmp_optiT = unwrap(optitrack.yaw); 
    yaw_tmp_telem = unwrap(telemetry.yaw_gps);

    % derivative of position for obtain velocity
    Vx_optiT = ones(length(optitrack.X)-1,1)*NaN;  % initialize
    Vy_optiT = ones(length(optitrack.Y)-1,1)*NaN;  % initialize
    Vz_optiT = ones(length(optitrack.Z)-1,1)*NaN;  % initialize
    V_optiT  = ones(length(optitrack.X)-1,1)*NaN;  % initialize
    yaw_rate_optiT = ones(length(optitrack.yaw)-1,1)*NaN;  % initialize
    for i = 1:(length(optitrack.X)-1)
       Vx_optiT(i) = (((optitrack.X(i+1)-optitrack.X(i))/Ts_optiT)); 
       Vy_optiT(i) = ((optitrack.Y(i+1)-optitrack.Y(i))/Ts_optiT);
       Vz_optiT(i) = ((optitrack.Z(i+1)-optitrack.Z(i))/Ts_optiT);
       V_optiT(i)  = sqrt(Vx_optiT(i)^2+ Vy_optiT(i)^2 + Vz_optiT(i)^2);  % absolute velocity    
       yaw_rate_optiT(i) = ((yaw_tmp_optiT(i+1)-yaw_tmp_optiT(i))/Ts_optiT)*pi/180;
    end
    
    Vx_telem = ones(length(telemetry.x_gps)-1,1)*NaN;  % initialize
    Vy_telem = ones(length(telemetry.y_gps)-1,1)*NaN;  % initialize
    V_telem  = ones(length(telemetry.x_gps)-1,1)*NaN;  % initialize
    yaw_rate_telem = ones(length(telemetry.yaw_gps)-1,1)*NaN;  % initialize
    for i = 1:(length(telemetry.x_gps)-1)
       Vx_telem(i) = (((telemetry.x_gps(i+1)-telemetry.x_gps(i))/Ts_telem_interp)); 
       Vy_telem(i) = ((telemetry.y_gps(i+1)-telemetry.y_gps(i))/Ts_telem_interp);
       V_telem(i)  = sqrt(Vx_telem(i)^2+ Vy_telem(i)^2);  % absolute velocity    
       yaw_rate_telem(i) = ((yaw_tmp_telem(i+1)-yaw_tmp_telem(i))/Ts_telem_interp)*pi/180;
    end
    
    % encoder linear velocity (only freewheel)
    V_enc = r.*telemetry.enc_fr;  %r.*(telemetry.enc_fl+telemetry.enc_fr)./2;
    
    if (filter_flag)
        % filtering the optitrack data
        V_enc          = filtfilt(lpf_optiT,V_enc);
        V_optiT        = filtfilt(lpf_optiT,V_optiT);
        Vx_optiT       = filtfilt(lpf_optiT,Vx_optiT);
        Vy_optiT       = filtfilt(lpf_optiT,Vy_optiT);
        yaw_rate_optiT = filtfilt(lpf_optiT,yaw_rate_optiT);
    end
    
    % unwrapping
    optitrack.roll  = unwrap(optitrack.roll*pi/180);
    optitrack.yaw   = unwrap(optitrack.yaw*pi/180);
    optitrack.pitch = unwrap(optitrack.pitch*pi/180);
    
    % relative velocity
    for i = 1:length(optitrack.roll)-1
        R_x(:,:,i) = [ 1 0 0 0;
                       0 cos(optitrack.roll(i)) -sin(optitrack.roll(i)) 0;
                       0 sin(optitrack.roll(i)) cos(optitrack.roll(i)) 0;
                       0 0 0 1];
        R_y(:,:,i) = [ cos(optitrack.pitch(i)) 0 sin(optitrack.pitch(i)) 0;
                       0 1 0 0;
                       -sin(optitrack.pitch(i)) 0 cos(optitrack.pitch(i)) 0;
                       0 0 0 1];
        R_z(:,:,i) = [ cos(optitrack.yaw(i)) -sin(optitrack.yaw(i)) 0 0;
                       sin(optitrack.yaw(i)) cos(optitrack.yaw(i)) 0 0;
                       0 0 1 0;
                       0 0 0 1;];
        T(:,:,i) =   [ 1 0 0 optitrack.X(i);
                       0 1 0 optitrack.Y(i);
                       0 0 1 optitrack.Z(i);
                       0 0 0 1];

        V_relative(:,i) = R_z(:,:,i)'*[Vx_optiT(i); Vy_optiT(i); 0; 0];
    end
    
    if (~(strcmp(align_variable,'none')))
        % ----------------
        % Re-alignment of the data
        % ----------------
        % Re-sampling at a higher rate, before the alignment
        Ts_new = 0.001;   % [s]          
        
        tol_optiT = 2e-3;  % [m] tolerance to detect when the car is moving 
        idx_start_optiT_x = 1;
        if (~isempty(find(abs(optitrack.X-optitrack.X(1))>tol_optiT,1)))
            idx_start_optiT_x = find(abs(optitrack.X-optitrack.X(1))>tol_optiT,1);
        end
        idx_start_optiT_y = 1;
        if (~isempty(find(abs(optitrack.Y-optitrack.Y(1))>tol_optiT,1)))
            idx_start_optiT_y = find(abs(optitrack.Y-optitrack.Y(1))>tol_optiT,1);
        end
        conserv_optiT = 200;  % conservative n° of samples to avoid selecting a starting index that is too far from the real one
        idx_start_optiT = max(min(idx_start_optiT_x-conserv_optiT,idx_start_optiT_y-conserv_optiT),1);
        optitrack.time = optitrack.time(idx_start_optiT:end) - optitrack.time(idx_start_optiT);
        optitrack.X = optitrack.X(idx_start_optiT:end);
        optitrack.Y = optitrack.Y(idx_start_optiT:end);

        idx_start_tel_gps_x = 1;
        if (~isempty(find(abs(telemetry.x_gps(2:end)-telemetry.x_gps(2))>tol_optiT,1)))
            idx_start_tel_gps_x = find(abs(telemetry.x_gps(2:end)-telemetry.x_gps(2))>tol_optiT,1);
        end
        idx_start_tel_gps_y = 1;
        if (~isempty(find(abs(telemetry.y_gps(2:end)-telemetry.y_gps(2))>tol_optiT,1)))
            idx_start_tel_gps_y = find(abs(telemetry.y_gps(2:end)-telemetry.y_gps(2))>tol_optiT,1);
        end
        
        conserv_telem = 0;  % conservative n° of samples to avoid selecting a starting index that is too far from the real one
        idx_start_tel_gps = max(min([idx_start_tel_gps_x+1-conserv_telem,idx_start_tel_gps_y+1-conserv_telem]),1);
        
        tol_1 = 2e-2;
        if (MPC_flag)
            % The (front) encoder data are used to evaluate when the car starts moving
            idx_start_tel_enc_f = 1;
            if (~isempty(find(abs(telemetry.enc_fr(2:end)-telemetry.enc_fr(2))>tol_1,1)))
                idx_start_tel_enc_f = find(abs(telemetry.enc_fr(2:end)-telemetry.enc_fr(2))>tol_1,1);
            end
            % The traction, rear encoders and other signals may be non zero (for a small amount of time) even when 
            % the car still has a zero velocity
            idx_start_tel_tract = 1;
            if (~isempty(find(abs(telemetry.traction(2:end)-telemetry.traction(2))>0,1)))
                idx_start_tel_tract = find(abs(telemetry.traction(2:end)-telemetry.traction(2))>0,1);
            end
            idx_start_tel_enc_r = 1;
            if (~isempty(find(abs(telemetry.enc_rr(2:end)-telemetry.enc_rr(2))>tol_1,1)))
                idx_start_tel_enc_r = find(abs(telemetry.enc_rr(2:end)-telemetry.enc_rr(2))>tol_1,1);
            end
            idx_start_tel_ax = 1;
            if (~isempty(find(abs(telemetry.a_x_mpu(2:end)-telemetry.a_x_mpu(2))>tol_1,1)))
                idx_start_tel_ax = find(abs(telemetry.a_x_mpu(2:end)-telemetry.a_x_mpu(2))>tol_1,1);
            end
            idx_start_tel_target = 1;
            if (~isempty(find(abs(telemetry.target(2:end)-telemetry.target(2))>tol_1,1)))
                idx_start_tel_target = find(abs(telemetry.target(2:end)-telemetry.target(2))>tol_1,1);
            end
            idx_start_tel_steer = idx_start_tel_target;
            idx_start_tel = max(idx_start_tel_enc_f-conserv_telem,1);
            
            % Delay between the velocity (front encoder) and other signals  
            delay_vel_tract  = max(idx_start_tel_enc_f-idx_start_tel_tract,0);
            delay_vel_enc_r  = max(idx_start_tel_enc_f-idx_start_tel_enc_r,0);    
            delay_vel_ax     = max(idx_start_tel_enc_f-idx_start_tel_ax,0);  
            delay_vel_steer  = max(idx_start_tel_enc_f-idx_start_tel_steer,0);  
            delay_vel_target = max(idx_start_tel_enc_f-idx_start_tel_target,0);  
        else
            idx_start_tel_tract = 1;
            if (~isempty(find(abs(telemetry.traction(2:end)-telemetry.traction(2))>0,1)))
                idx_start_tel_tract = find(abs(telemetry.traction(2:end)-telemetry.traction(2))>0,1);
            end
            
            idx_start_tel = max(idx_start_tel_tract-conserv_telem,1);
            idx_start_tel_tract  = idx_start_tel;
            idx_start_tel_enc_r  = idx_start_tel;
            idx_start_tel_ax     = idx_start_tel;
            idx_start_tel_steer  = idx_start_tel;
            idx_start_tel_target = idx_start_tel;
            
            % Delay between the velocity (front encoder) and other signals  
            delay_vel_tract  = 0;
            delay_vel_enc_r  = 0;    
            delay_vel_ax     = 0;  
            delay_vel_steer  = 0;  
            delay_vel_target = 0;  
        end
        
        extra_delay_gps_tel = max(idx_start_tel_gps-idx_start_tel,0);  % extra delay that may be present in the gps data recorded on the rasp, w.r.t. the other
                                                                       % data recorded on the rasp
        
        telemetry.time  = telemetry.time(idx_start_tel_gps:end) - telemetry.time(idx_start_tel_gps);
        telemetry.x_gps = telemetry.x_gps(idx_start_tel_gps:end);
        telemetry.y_gps = telemetry.y_gps(idx_start_tel_gps:end);
        
        final_time = min(telemetry.time(end),optitrack.time(end));
        newtime_align = 0:Ts_new:final_time;    % time vector resempled at a higher rate   
        % Select the variable (x or y) to be used for the alignment (use the one that varies the most)
        variabity_x = abs(max(optitrack.X)-min(optitrack.X));
        variabity_y = abs(max(optitrack.Y)-min(optitrack.Y));
        if (variabity_x>variabity_y)
            telem_var = telemetry.x_gps;
            optiT_var = optitrack.X;
        else
            telem_var = telemetry.y_gps;
            optiT_var = optitrack.Y;
        end
        new_telem_var = interp1(telemetry.time, telem_var, newtime_align);   % upsampling of the two signals
        new_optiT_var = interp1(optitrack.time, optiT_var, newtime_align);

        new_optiT_var(isnan(new_optiT_var)) = 0;
        var_optiT = new_optiT_var;
        var_telem = new_telem_var;
        
        % Align the 2 signals by shifting one of the two until the MSE between the 2 signals reaches the minimum
        num_test_delays = round(0.8*length(var_telem));  % max n° of sample lags to be considered
        MSE_vals = ones(num_test_delays,1)*NaN;   % initialize
        for jj=1:num_test_delays
            var_telem_lag = [ones(1,jj)*var_telem(1), var_telem(1:end-jj)];
            MSE_vals(jj) = immse(var_telem_lag,var_optiT);  % store the MSE
        end
        [~,lagDiff] = min(MSE_vals); 

        timeDiff = lagDiff*Ts_new;  % time lag between the 2 signals
        offset = abs(round(timeDiff/Ts_telem));  % offset n° of samples

        telemetry.time = (telemetry.time(1):Ts_telem:Ts_telem*(length(telemetry.time)+offset-1))'; 
        
        % Final index for the Optitrack data
        idx_end_optiT = length(optitrack.time)-1;
        if (~isempty(find(optitrack.time>telemetry.time(end),1)))
            idx_end_optiT = find(optitrack.time>telemetry.time(end),1);
        end
        optitrack.time = optitrack.time(1:idx_end_optiT);
        
        % ----------------
        % Telemetry realignment
        % ----------------     
        a_x         = [ones(offset-delay_vel_ax,1)*telemetry.a_x_mpu(idx_start_tel_ax); telemetry.a_x_mpu(idx_start_tel_ax:idx_start_tel_ax+length(telemetry.time)-offset+delay_vel_ax-1)*9.8];
        a_y         = [ones(offset-delay_vel_ax,1)*telemetry.a_y_mpu(idx_start_tel_ax); telemetry.a_y_mpu(idx_start_tel_ax:idx_start_tel_ax+length(telemetry.time)-offset+delay_vel_ax-1)*9.8];
        a_z         = [ones(offset-delay_vel_ax,1)*telemetry.a_z_mpu(idx_start_tel_ax); telemetry.a_z_mpu(idx_start_tel_ax:idx_start_tel_ax+length(telemetry.time)-offset+delay_vel_ax-1)*9.8];
        gyro_x      = [ones(offset-delay_vel_ax,1)*telemetry.gyro_x_mpu(idx_start_tel_ax); telemetry.gyro_x_mpu(idx_start_tel_ax:idx_start_tel_ax+length(telemetry.time)-offset+delay_vel_ax-1)];
        gyro_y      = [ones(offset-delay_vel_ax,1)*telemetry.gyro_y_mpu(idx_start_tel_ax); telemetry.gyro_y_mpu(idx_start_tel_ax:idx_start_tel_ax+length(telemetry.time)-offset+delay_vel_ax-1)];
        gyro_z      = [ones(offset-delay_vel_ax,1)*telemetry.gyro_z_mpu(idx_start_tel_ax); telemetry.gyro_z_mpu(idx_start_tel_ax:idx_start_tel_ax+length(telemetry.time)-offset+delay_vel_ax-1)];
        enc_fl      = [ones(offset,1)*telemetry.enc_fl(idx_start_tel); telemetry.enc_fl(idx_start_tel:idx_start_tel+length(telemetry.time)-offset-1)];
        enc_fr      = [ones(offset,1)*telemetry.enc_fr(idx_start_tel); telemetry.enc_fr(idx_start_tel:idx_start_tel+length(telemetry.time)-offset-1)];
        enc_rl      = [ones(offset-delay_vel_enc_r,1)*telemetry.enc_rl(idx_start_tel_enc_r); telemetry.enc_rl(idx_start_tel_enc_r:idx_start_tel_enc_r+length(telemetry.time)-offset+delay_vel_enc_r-1)];
        enc_rr      = [ones(offset-delay_vel_enc_r,1)*telemetry.enc_rr(idx_start_tel_enc_r); telemetry.enc_rr(idx_start_tel_enc_r:idx_start_tel_enc_r+length(telemetry.time)-offset+delay_vel_enc_r-1)];
        traction    = [ones(offset-delay_vel_tract,1)*telemetry.traction(idx_start_tel_tract); telemetry.traction(idx_start_tel_tract:idx_start_tel_tract+length(telemetry.time)-offset+delay_vel_tract-1)];
        steering    = [ones(offset-delay_vel_steer,1)*telemetry.steering(idx_start_tel_steer); telemetry.steering(idx_start_tel_steer:idx_start_tel_steer+length(telemetry.time)-offset+delay_vel_steer-1)];
        target      = [ones(offset-delay_vel_target,1)*telemetry.target(idx_start_tel_target); telemetry.target(idx_start_tel_target:idx_start_tel_target+length(telemetry.time)-offset+delay_vel_target-1)];
        V_enc       = [ones(offset,1)*V_enc(idx_start_tel); V_enc(idx_start_tel:idx_start_tel+length(telemetry.time)-offset-1)];
        x_gps_tel   = [ones(offset,1)*telemetry.x_gps(1); telemetry.x_gps(1:end)];
        y_gps_tel   = [ones(offset,1)*telemetry.y_gps(1); telemetry.y_gps(1:end)];  
        yaw_gps_tel = [ones(offset,1)*telemetry.yaw_gps(idx_start_tel_gps); telemetry.yaw_gps(idx_start_tel_gps:end)];
        
        V_gps_tel   = [ones(offset,1)*V_telem(idx_start_tel_gps); V_telem(idx_start_tel_gps:end)];
        Vx_gps_tel  = [ones(offset,1)*Vx_telem(idx_start_tel_gps); Vx_telem(idx_start_tel_gps:end)];
        Vy_gps_tel  = [ones(offset,1)*Vy_telem(idx_start_tel_gps); Vy_telem(idx_start_tel_gps:end)];
        yaw_rate_gps_tel = [ones(offset,1)*yaw_rate_telem(idx_start_tel_gps); yaw_rate_telem(idx_start_tel_gps:end)];

        % ----------------
        % Optitrack realignemt
        % ----------------
        x_gps = optitrack.X(1:idx_end_optiT);
        x_gps(isnan(x_gps)) = 0;
        y_gps = optitrack.Y(1:idx_end_optiT);
        y_gps(isnan(y_gps)) = 0;
        z_gps = optitrack.Z(idx_start_optiT:idx_start_optiT+idx_end_optiT-1);
        z_gps(isnan(z_gps)) = 0;
        yaw = optitrack.yaw(idx_start_optiT:idx_start_optiT+idx_end_optiT-1);
        yaw(isnan(yaw)) = 0;
        roll = optitrack.roll(idx_start_optiT:idx_start_optiT+idx_end_optiT-1);
        roll(isnan(roll)) = 0;
        pitch = optitrack.pitch(idx_start_optiT:idx_start_optiT+idx_end_optiT-1);
        pitch(isnan(pitch)) = 0;
        V_gps = V_optiT(idx_start_optiT:idx_start_optiT+idx_end_optiT-1);
        V_gps(isnan(V_gps)) = 0; 
        Vx_gps = Vx_optiT(idx_start_optiT:idx_start_optiT+idx_end_optiT-1);
        Vx_gps(isnan(Vx_gps)) = 0; 
        Vy_gps = Vy_optiT(idx_start_optiT:idx_start_optiT+idx_end_optiT-1);
        Vy_gps(isnan(Vy_gps)) = 0; 
        yaw_rate_gps = yaw_rate_optiT(idx_start_optiT:idx_start_optiT+idx_end_optiT-1);
        yaw_rate_gps(isnan(yaw_rate_gps)) = 0;   
        Vx_rel = V_relative(1,(idx_start_optiT:idx_start_optiT+idx_end_optiT-1));
        Vx_rel(isnan(Vx_rel)) = 0; 
        Vy_rel = V_relative(2,(idx_start_optiT:idx_start_optiT+idx_end_optiT-1));
        Vy_rel(isnan(Vy_rel)) = 0; 
    else
        % ----------------
        % No data alignment 
        % ----------------
        a_x         = telemetry.a_x_mpu*9.8;
        a_y         = telemetry.a_y_mpu*9.8;
        a_z         = telemetry.a_z_mpu*9.8;
        gyro_x      = telemetry.gyro_x_mpu;
        gyro_y      = telemetry.gyro_y_mpu;
        gyro_z      = telemetry.gyro_z_mpu;
        enc_fl      = telemetry.enc_fl;
        enc_fr      = telemetry.enc_fr;
        enc_rl      = telemetry.enc_rl;
        enc_rr      = telemetry.enc_rr;
        traction    = telemetry.traction;
        steering    = telemetry.steering;
        target      = telemetry.target;
        V_enc       = V_enc;
        x_gps_tel   = telemetry.x_gps;
        y_gps_tel   = telemetry.y_gps;  
        yaw_gps_tel = telemetry.yaw_gps;
        
        V_gps_tel   = V_telem;
        Vx_gps_tel  = Vx_telem;
        Vy_gps_tel  = Vy_telem;
        yaw_rate_gps_tel = yaw_rate_telem;

        % ----------------
        % Optitrack realignemt
        % ----------------
        x_gps = optitrack.X;
        x_gps(isnan(x_gps)) = 0;
        y_gps = optitrack.Y;
        y_gps(isnan(y_gps)) = 0;
        z_gps = optitrack.Z;
        z_gps(isnan(z_gps)) = 0;
        yaw = optitrack.yaw;
        yaw(isnan(yaw)) = 0;
        roll = optitrack.roll;
        roll(isnan(roll)) = 0;
        pitch = optitrack.pitch;
        pitch(isnan(pitch)) = 0;
        V_gps = V_optiT;
        V_gps(isnan(V_gps)) = 0; 
        Vx_gps = Vx_optiT;
        Vx_gps(isnan(Vx_gps)) = 0; 
        Vy_gps = Vy_optiT;
        Vy_gps(isnan(Vy_gps)) = 0; 
        yaw_rate_gps = yaw_rate_optiT;
        yaw_rate_gps(isnan(yaw_rate_gps)) = 0;   
        Vx_rel = V_relative(1,:);
        Vx_rel(isnan(Vx_rel)) = 0; 
        Vy_rel = V_relative(2,:);
        Vy_rel(isnan(Vy_rel)) = 0; 
    end

    % Apply a median filter to the front encoders data
    telemetry.enc_fr = medfilt1(telemetry.enc_fr,3);
    telemetry.enc_fl = medfilt1(telemetry.enc_fl,3);
    
    % ----------------
    % Creation of the output structures
    % ----------------
    remap_telemetry = struct('time',telemetry.time,'a_x',a_x,'a_y',a_y,'a_z',a_z,...
                'gyro_x',gyro_x,'gyro_y',gyro_y,'gyro_z',gyro_z,...
                'enc_fr',enc_fr,'enc_fl',enc_fl,'enc_rr',enc_rr,'enc_rl',enc_rl,...
                'traction',traction,'steering',steering,'target',target,'V_enc',V_enc,...
                'x_gps_tel',x_gps_tel, 'y_gps_tel',y_gps_tel, 'yaw_gps_tel',yaw_gps_tel,...
                'V_gps_tel',V_gps_tel, 'Vx_gps_tel',Vx_gps_tel, 'Vy_gps_tel',Vy_gps_tel,...
                'yaw_rate_gps_tel',yaw_rate_gps_tel);

    remap_optitrack = struct('time',optitrack.time,'roll',roll,'pitch',pitch,'yaw',yaw,'x_gps',x_gps,'y_gps',y_gps,...
                            'z_gps',z_gps,'V_gps',V_gps,'Vx_gps',Vx_gps,'Vy_gps',Vy_gps,'yaw_rate_gps',yaw_rate_gps,...
                            'Vx_rel',Vx_rel,'Vy_rel',Vy_rel);                 
                        
end