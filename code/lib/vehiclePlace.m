function vehiclePlace( transform_object , x,y,h )
 % this function places the vehicle on an axes, given the transform handle and the vehicle position
 % vehiclePlace( transform object , Vehicle2PosAtt ) Vehicle2PosAtt = [ x y h ]

 % Form z-axis rotation matrix
    TRz = makehgtform('zrotate',h);
    % Set transforms for both transform objects

    TRz(1,4) = x;
    TRz(2,4) = y;

    set( transform_object , 'Matrix' , TRz );

end