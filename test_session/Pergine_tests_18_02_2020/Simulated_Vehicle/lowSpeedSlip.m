function alpha_lowSpeed = lowSpeedSlip(Vx,Vsy,alpha,Vlow,By,Cy,Dy,Fz)

    % ---------------------------------------------------
    %% Low-speed corrections of the tire side slip angle
    % ---------------------------------------------------

    CFalpha = By*Cy*Dy*Fz; 

    kVlow0 = 500;
    if abs(Vx) <= Vlow
      kVlow = 1/2*kVlow0*(1+cos(pi*abs(Vx)/Vlow));
    else
      kVlow = 0;
    end

    alpha_lowSpeed = alpha + kVlow*Vsy/CFalpha;

end