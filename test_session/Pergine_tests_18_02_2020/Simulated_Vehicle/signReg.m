function y = signReg(x)

    % Regularized sign function
    regFactor = 0.01;
    y = sin(atan(x/regFactor));

end