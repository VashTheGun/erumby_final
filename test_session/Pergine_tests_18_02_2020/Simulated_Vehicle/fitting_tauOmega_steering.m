% ---------------------------------------------------------------------------------------
%% Fitting the time constant tau_Omega for the yaw rate dynamics of the Kineto-Dyn model
% ---------------------------------------------------------------------------------------

% Extract the quantities of the eRumby vehicle model 
u_realVeh     = MPC_sim.states.u.data(2000:end);
v_realVeh     = MPC_sim.states.v.data(2000:end);
Omega_realVeh = MPC_sim.states.Omega.data(2000:end);
delta_realVeh = MPC_sim.states.delta.data(2000:end);
time_simul    = MPC_sim.states.u.time(2000:end);

% Extract the quantities of the kineto-dynamical vehicle model 
u_kineDyn     = MPC_sim.u_kineDyn.data(2000:end);
Omega_kineDyn = MPC_sim.Omega_kineDyn.data(2000:end);
delta_kineDyn = MPC_sim.delta_kineDyn.data(2000:end);

Ay_realVeh = Omega_realVeh.*u_realVeh;
Ay_kineDyn = Omega_kineDyn.*u_kineDyn;

steer_tendency_realVeh = delta_realVeh - (Omega_realVeh./u_realVeh)*L;
steer_tendency_kineDyn = delta_kineDyn - (Omega_kineDyn./u_kineDyn)*L;

% ----------------------------------
%% Comparison of yaw rate profiles for the 2 vehicle models
% ----------------------------------
if (~enable_docked)
    figure('Name','Omega compare','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','Omega compare','NumberTitle','off'), clf
end    
hold on
plot(time_simul,Omega_realVeh,'-b','LineWidth',2)
plot(time_simul,Omega_kineDyn,'Color',color('orange'),'LineWidth',2)
grid on
xlabel('time [s]')
ylabel('$\Omega$ [rad/s]')
legend('eRumby model','kine-dyn model')

% ----------------------------------
%% Handling diagram of the eRumby vehicle model
% ----------------------------------
if (~enable_docked)
    figure('Name','HandlDiagr eRumbyModel','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','HandlDiagr eRumbyModel','NumberTitle','off'), clf
end    
plot(Ay_realVeh,steer_tendency_realVeh,'-b','LineWidth',2)
grid on
xlabel('$A_y$ [m/s$^2$]')
ylabel('$\delta - \frac{\Omega}{u}L$ [rad]')
title('Handling diagram, eRumby vehicle model')

% ----------------------------------
%% Handling diagram of the kineto-dyn vehicle model
% ----------------------------------
if (~enable_docked)
    figure('Name','HandlDiagr kineDyn','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','HandlDiagr kineDyn','NumberTitle','off'), clf
end    
plot(Ay_kineDyn,steer_tendency_kineDyn,'Color',color('orange'),'LineWidth',2)
grid on
xlabel('$A_y$ [m/s$^2$]')
ylabel('$\delta - \frac{\Omega}{u}L$ [rad]')
title('Handling diagram, kineto-dyn vehicle model')






