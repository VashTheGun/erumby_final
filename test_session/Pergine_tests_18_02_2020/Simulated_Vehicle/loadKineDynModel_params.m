function KineDynModel_params = loadKineDynModel_params()

    % -------------------------------------------------------------------
    %% Parameters of the kineto-dynamical model used by the MPC high-level planner
    % -------------------------------------------------------------------
    
    L = 0.325;        % [m] vehicle wheelbase
    tau_Omega = 0.09;  % [s] time constant for yaw rate dynamics
    
    KineDynModel_params.L = L;
    KineDynModel_params.tau_Omega = tau_Omega;

end

