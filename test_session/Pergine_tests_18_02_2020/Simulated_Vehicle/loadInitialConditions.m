function ICs_vehicle = loadInitialConditions()
    
    % ----------------------------------------------
    %% Initial conditions for all the vehicle states
    % ----------------------------------------------

    % Load a struct containing vehicle parameters
    vehParams = getVehicleDataStruct; 
    Lf  = vehParams.vehicle.Lf;        
    Lr  = vehParams.vehicle.Lr; 
    L   = Lf + Lr;
    m   = vehParams.vehicle.m;        
    g   = vehParams.vehicle.g;

    %% ICs for all the vehicle states
    u0       = 0.1;        % [m/s] initial longitudinal speed
    v0       = 0;          % [m/s] initial lateral speed
    Omega0   = 0;          % [rad/s] initial yaw rate
    delta0   = 0;          % [rad] initial steering angle
    alpha_r0 = 0;          % [rad] initial side slip angle for the rear tire
    alpha_f0 = 0;          % [rad] initial side slip angle for the front tire
    F_zr0    = m*g*Lf/L;   % [N] initial vertical load for the rear tire
    F_zf0    = m*g*Lr/L;   % [N] initial vertical load for the front tire
    
    ICs_vehicle = [u0, v0, Omega0, delta0, alpha_r0, alpha_f0, F_zr0, F_zf0];
       
end

