% -------------------------------------------------------
%% PostProcessing and Data Analysis
% -------------------------------------------------------

% --------------------------------
%% Extract the data from the simulink model
% --------------------------------
time_sim = MPC_sim.states.u.time;
dt = time_sim(2)-time_sim(1);

% Inputs
delta_D = MPC_sim.inputs.delta_D.data;
F_xr    = MPC_sim.inputs.F_xr.data;

% States Measurements
u       = MPC_sim.states.u.data;
v       = MPC_sim.states.v.data;
Omega   = MPC_sim.states.Omega.data;
delta   = MPC_sim.states.delta.data;
alpha_r = MPC_sim.states.alpha_r.data;
alpha_f = MPC_sim.states.alpha_f.data;
Fz_r    = MPC_sim.states.F_zr.data;
Fz_f    = MPC_sim.states.F_zf.data;

% Extra Parameters Measurements
Fy_r    = MPC_sim.extra_params.F_yr.data;
Fy_f    = MPC_sim.extra_params.F_yf.data;

% Chassis side slip angle beta [rad]
beta = atan(v./u);

% Derivatives of u, v [m/s^2]
dot_u = diff(u)/dt;
dot_v = diff(v)/dt;

% Total longitudinal and lateral accelerations
Ax = dot_u - Omega(2:end).*v(2:end);
Ay = dot_v + Omega(2:end).*u(2:end);
% Ax low-pass filtered signal
[b_butt,a_butt] = butter(4,0.005,'low');
Ax_filt = filter(b_butt,a_butt,Ax);

% Longitudinal jerk [m/s^3]
jerk_x = diff(dot_u)/dt;

% Steady state lateral acceleration
Ay_ss = Omega.*u;

% Total CoM speed [m/s]
vG = sqrt(u.^2 + v.^2);

% Steady state and transient curvature [m]
rho_ss   = Omega./vG;
rho_tran = ((dot_v.*u(1:end-1) - dot_u.*v(1:end-1)) ./ ((vG(1:end-1)).^3)) + rho_ss(1:end-1);

% Vehicle Pose
x_veh = MPC_sim.vehicle_pose(:,1);
y_veh = MPC_sim.vehicle_pose(:,2);
psi_veh = MPC_sim.vehicle_pose(:,3);

% Desired sinusoidal steering angle for the equivalent single track front wheel
desired_steer_atWheel = delta_D/tau__D;


% ----------------------------------
%% Plot inputs
% ----------------------------------
if (~enable_docked)
    figure('Name','Inputs','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','Inputs','NumberTitle','off'), clf
end    
% --- F_xr --- %
ax(1) = subplot(121);
plot(time_sim,F_xr,'LineWidth',2)
grid on
title('$F_{xr}$ [N]')
xlim([0 time_sim(end)+1])
% --- delta_D --- %
ax(2) = subplot(122);
plot(time_sim,rad2deg(delta_D),'LineWidth',1)
hold on
plot(time_sim,rad2deg(delta)*tau__D,'Color',color('orange'),'LineWidth',2)
grid on
title('$\delta_D$ [deg]')
legend('target','real')
xlim([0 time_sim(end)+1])

% linkaxes(ax,'x')
clear ax


% ----------------------------------
%% Plot states 1-4
% ----------------------------------
if (~enable_docked)
    figure('Name','States 1-4','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','States 1-4','NumberTitle','off'), clf
end  
% --- u --- %
ax(1) = subplot(221);
hold on
plot(time_sim,u,'LineWidth',2)
grid on
title('$u$ [m/s]')  
xlim([0 time_sim(end)+1])
% --- v --- %
ax(2) = subplot(222);
plot(time_sim,v,'LineWidth',2)
grid on
title('$v$ [m/s]')
xlim([0 time_sim(end)+1])
% --- Omega --- %
ax(3) = subplot(223);
plot(time_sim,Omega,'LineWidth',2)
grid on
title('$\Omega$ [rad/s]')
xlim([0 time_sim(end)+1])
% --- delta --- %
ax(4) = subplot(224);
plot(time_sim,rad2deg(delta),'LineWidth',2)
grid on
title('$\delta$ [deg]')
xlim([0 time_sim(end)+1])

% linkaxes(ax,'x')
clear ax


% ----------------------------------
%% Plot states 5-8
% ----------------------------------
if (~enable_docked)
    figure('Name','States 5-8','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','States 5-8','NumberTitle','off'), clf
end  
% --- alpha_r --- %
ax(1) = subplot(221);
hold on
plot(time_sim,rad2deg(alpha_r),'LineWidth',2)
grid on
title('$\alpha_r$ [deg]')  
xlim([0 time_sim(end)+1])
% --- alpha_f --- %
ax(2) = subplot(222);
hold on
plot(time_sim,rad2deg(alpha_f),'LineWidth',2)
grid on
title('$\alpha_f$ [deg]')
xlim([0 time_sim(end)+1])
% --- Fz_r --- %
ax(3) = subplot(223);
plot(time_sim,Fz_r,'LineWidth',2)
grid on
title('$F_{zr}$ [N]')
xlim([0 time_sim(end)+1])
% --- Fz_f --- %
ax(4) = subplot(224);
plot(time_sim,Fz_f,'LineWidth',2)
grid on
title('$F_{zf}$ [N]')
xlim([0 time_sim(end)+1])

% linkaxes(ax,'x')
clear ax


% ----------------------------------
%% Plot extra parameters
% ----------------------------------
if (~enable_docked)
    figure('Name','Extra pars - 1','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','Extra pars - 1','NumberTitle','off'), clf
end  
% --- Fy_r --- %
ax(1) = subplot(121);
plot(time_sim,Fy_r,'LineWidth',2)
grid on
title('$F_{yr}$ [N]')
xlim([0 time_sim(end)+1])
% --- Fy_f --- %
ax(2) = subplot(122);
plot(time_sim,Fy_f,'LineWidth',2)
grid on
title('$F_{yf}$ [N]')
xlim([0 time_sim(end)+1])

% linkaxes(ax,'x')
clear ax


% ----------------------------------
%% Plot accelerations, chassis side slip angle and curvature
% ----------------------------------
if (~enable_docked)
    figure('Name','Extra pars - 2','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','Extra pars - 2','NumberTitle','off'), clf
end 
% --- ax --- %
ax(1) = subplot(321);
hold on
plot(time_sim(2:end),dot_u - Omega(2:end).*v(2:end),'LineWidth',2)
plot(time_sim(2:end),diff(u)/dt,'--g','LineWidth',1)
plot(time_sim(2:end),Ax_filt,'-.b','LineWidth',2)
grid on
title('$a_{x}$ $[m/s^2]$')
legend('$\dot{u}-\Omega v$','$\dot{u}$','filt $\dot{u}$','Location','northeast')
xlim([0 time_sim(end)+1])
% --- ay --- %
ax(2) = subplot(322);
hold on
plot(time_sim(2:end),dot_v + Omega(2:end).*u(2:end),'LineWidth',2)
plot(time_sim(2:end),Omega(2:end).*u(2:end),'--g','LineWidth',1)
grid on
title('$a_{y}$ $[m/s^2]$')
legend('$\dot{v}+\Omega u$','$\Omega u$','Location','best')
xlim([0 time_sim(end)+1])
% --- beta --- %
ax(3) = subplot(323);
plot(time_sim,rad2deg(beta),'LineWidth',2)
grid on
title('$\beta$ [deg]')
xlim([0 time_sim(end)+1])
% --- rho --- %
ax(4) = subplot(324);
hold on
plot(time_sim,rho_ss,'LineWidth',2)
plot(time_sim(1:end-1),rho_tran,'--g','LineWidth',1)
grid on
title('$\rho$ [$m^{-1}$]')
legend('$\rho_{ss}$','$\rho_{transient}$','Location','best')
xlim([0 time_sim(end)+1])
% --- jerk jx --- %
ax(5) = subplot(325);
plot(time_sim(3:end),jerk_x,'LineWidth',2)
grid on
title('jerk $j_x$ [$m/s^3$]')
xlim([0 time_sim(end)+1])

% linkaxes(ax,'x')
clear ax


% ----------------------------------
%% Plot vehicle pose x,y,psi
% ----------------------------------
if (~enable_docked)
    figure('Name','Pose','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','Pose','NumberTitle','off'), clf
end 
% --- x --- %
ax(1) = subplot(321);
plot(time_sim,x_veh,'LineWidth',2)
grid on
title('$x$ [m]')
xlim([0 time_sim(end)+1])
% --- y --- %
ax(2) = subplot(322);
plot(time_sim,y_veh,'LineWidth',2)
grid on
title('$y$ [m]')
xlim([0 time_sim(end)+1])
% --- psi --- %
ax(3) = subplot(323);
plot(time_sim,rad2deg(psi_veh),'LineWidth',2)
grid on
title('$\psi$ [deg]')
xlim([0 time_sim(end)+1])

% linkaxes(ax,'x')
clear ax


% ----------------------------------
%% Plot G-G diagram from simulation data
% ----------------------------------
if (~enable_docked)
    figure('Name','G-G plot','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','G-G plot','NumberTitle','off'), clf
end 
axis equal
%plot(Ay,Ax,'Color',color('gold'),'LineWidth',2)
hold on
% for j=1:length(u_vector)
%     plot3(adherenceRad_vector(1+(j-1)*length(theta_0_vector):j*length(theta_0_vector)).*cos(theta_0_vector),adherenceRad_vector(1+(j-1)*length(theta_0_vector):j*length(theta_0_vector)).*sin(theta_0_vector),ones(1,length((1+(j-1)*length(theta_0_vector):j*length(theta_0_vector))))*u_vector(j),'o','Color',ColorOdr(j,:),'MarkerFaceColor',ColorOdr(j,:),'MarkerSize',4)
% end
plot3(Ay,Ax_filt,u(1:end-1),'Color',color('purple'),'LineWidth',3)
xlabel('$a_y$ [m/s$^2$]')
ylabel('$a_x$ [m/s$^2$]')
zlabel('$u$ [m/s]')
title('G-G diagram from simulation data, Pergine circuit')
grid on
% oldcmap = colormap;
% colormap(flipud(oldcmap));
% hcb = colorbar;
% title(hcb,'Speed [m/s]','Interpreter','latex')
% caxis([6, 27]);


% ----------------------------------
%% Plot vehicle path
% ----------------------------------
N = length(time_sim);
if (~enable_docked)
    figure('Name','Real Vehicle Path','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','Real Vehicle Path','NumberTitle','off'), clf
end 
set(gca,'fontsize',16)
hold on
axis equal
xlabel('x-coord [m]')
ylabel('y-coord [m]')
title('Real Vehicle Path')
plot(x_veh,y_veh,'Color',color('gold'),'LineWidth',2)
for i = 1:floor(N/20):N
    rot_mat = [cos(psi_veh(i)) -sin(psi_veh(i)) ; sin(psi_veh(i)) cos(psi_veh(i))];
    pos_rr = rot_mat*[-Lr -W/2]';
    pos_rl = rot_mat*[-Lr +W/2]';
    pos_fr = rot_mat*[+Lf -W/2]';
    pos_fl = rot_mat*[+Lf +W/2]';
    pos = [pos_rr pos_rl pos_fl pos_fr];
    p = patch(x_veh(i) + pos(1,:),y_veh(i) + pos(2,:),'blue');
    quiver(x_veh(i), y_veh(i), u(i)*cos(psi_veh(i)), u(i)*sin(psi_veh(i)), 'color', [1,0,0]);
    quiver(x_veh(i), y_veh(i), -v(i)*sin(psi_veh(i)), v(i)*cos(psi_veh(i)), 'color', [0.23,0.37,0.17]);
end
grid on
hold off



%% Save inputs for system identif. and PID tuning for cruise control - Step input torque test

% Tm_rr_save = fopen('Tm_rr_SysIdent.txt','w');
% fprintf(Tm_rr_save,'%f\n',Tm_rr);
% fclose(Tm_rr_save);
% 
% Tm_rl_save = fopen('Tm_rl_SysIdent.txt','w');
% fprintf(Tm_rl_save,'%f\n',Tm_rl);
% fclose(Tm_rl_save);
% 
% u_save = fopen('u_SysIdent.txt','w');
% fprintf(u_save,'%f\n',u);
% fclose(u_save);
% 
% time_save = fopen('time_SysIdent.txt','w');
% fprintf(time_save,'%f\n',time_sim);
% fclose(time_save);

