function maneuver_speed = getManeuverSpeed(steerAngle,L,maneuver_Ay)    
    
    % --------------------------------------------------------------
    %% Select the list of speed values with which to perform steady-state cornering maneuvers
    % --------------------------------------------------------------

    maneuver_speed = sqrt(maneuver_Ay*L./deg2rad(steerAngle));

end