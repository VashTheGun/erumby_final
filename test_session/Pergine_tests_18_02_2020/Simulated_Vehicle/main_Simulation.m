% --------------------------------------------------------------------
%% Main file for the matching of the vehicle model with experimental data of the eRumby vehicle
%    - author:      Mattia Piccinini
%    - date:        30/12/2019
% --------------------------------------------------------------------


% ----------------------------------------------------
%% Initialization
% ----------------------------------------------------
environment_initializer;


% ----------------------------------------------------
%% Vehicle data
% ----------------------------------------------------
vehicle_data = getVehicleDataStruct();
m  = vehicle_data.vehicle.m; 
g  = vehicle_data.vehicle.g; 
Lf = vehicle_data.vehicle.Lf;        
Lr = vehicle_data.vehicle.Lr;                 
L  = vehicle_data.vehicle.L;  
W  = vehicle_data.vehicle.W;  
tau__D = vehicle_data.vehicle.tau_D;
K_D = vehicle_data.vehicle.K_D;


% ----------------------------------------------------
%% Longitudinal controller parameters
% ----------------------------------------------------
longController_params = loadLongitController_params();
c1  = longController_params.c1; 
c2  = longController_params.c2; 
a   = longController_params.a;
tau = longController_params.tau;
Kp  = longController_params.Kp;
Ki  = longController_params.Ki;


% ----------------------------------------------------
%% Initial conditions for vehicle states and pose
% ----------------------------------------------------
% Initial conditions for the full set of vehicle states
X0 = loadInitialConditions();
x0_road = 0; y0_road = 0;
n0 = 0; xi0 = 0; theta0 = 0;
initPose = loadInitialPose(x0_road,y0_road,n0,xi0,theta0);


% ----------------------------------------------------
%% Load the desired maneuver
% ----------------------------------------------------
maneuver = loadManeuver();
maneuver_type  = maneuver.type;
maneuver_steer = maneuver.steer;
maneuver_speed = maneuver.speed;
if (maneuver_type==1)
    maneuver_Ay = maneuver.Ay;    
end
set_param('vehicle_model/Kineto Dynamical Model','Commented','on')
if (maneuver_type==2)
    maneuver_steer_ampl = maneuver_steer.ampl;
    maneuver_steer_freq = maneuver_steer.freq;
    maneuver_steer      = maneuver_steer_ampl;
    load_Kus_fitting;
    set_param('vehicle_model/Kineto Dynamical Model','Commented','off')
else
    maneuver_steer_ampl = NaN;
    maneuver_steer_freq = NaN;
end
sineSteer_inputs = [maneuver_steer_ampl,maneuver_steer_freq];
maneuver_speed_idx = 1;  maneuver_steer_idx = 1;
maneuver_indices = [maneuver_speed_idx,maneuver_steer_idx];
maneuver_idx = 1;


% ----------------------------------------------------
%% Load simulation timings
% ----------------------------------------------------
timings = loadTiming(maneuver_type);
T0 = timings.T0;
Ts = timings.Ts;
Tf = timings.Tf;


% ----------------------------------------------------
%% Run the simulation
% ----------------------------------------------------
tic
if (maneuver_type==1 && enable_fit_Krho_vs_Ay)
    % steady-state cornering maneuvers
    u_ssCorner     = zeros(length(maneuver_speed)*length(maneuver_steer),1);   % initialize
    v_ssCorner     = zeros(length(maneuver_speed)*length(maneuver_steer),1);   % initialize
    Omega_ssCorner = zeros(length(maneuver_speed)*length(maneuver_steer),1);   % initialize
    for kk=1:length(maneuver_steer)
        maneuver_speed = getManeuverSpeed(maneuver_steer(maneuver_steer_idx),L,maneuver_Ay);
        for jj=1:length(maneuver_speed)
            fprintf('Running maneuver n�%d\n',maneuver_idx);
            maneuver_indices = [maneuver_speed_idx,maneuver_steer_idx];
            MPC_sim = sim('vehicle_model'); 
            extract_sim_results;
            maneuver_speed_idx = maneuver_speed_idx+1;
            maneuver_idx = maneuver_idx+1;
        end
        maneuver_steer_idx = maneuver_steer_idx+1;
        maneuver_speed_idx = 1;
    end
else  % maneuver_type==1 || maneuver_type==2
    MPC_sim = sim('vehicle_model'); 
end
elapsed_time_simulation = toc;


% ----------------------------------------------------
%% Post Processing and data analysis
% ----------------------------------------------------
fprintf('\n================== POST PROCESSING ====================\n');

fprintf('The total simulation time was %.2f seconds\n',elapsed_time_simulation);

if (enable_dataAnalysis)
    dataAnalysis;
end

if (maneuver_type==1)
    if (enable_fitting_Krho)
        fitting_Krho_steering;
    end
end

if (maneuver_type==2)
    fitting_tauOmega_steering;
end


