% ------------------------------------
%% G-G envelope of max performance
% ------------------------------------

clc;

% Set this to 1 to save the max performance results
enable_save_GGenvelope = 1;

vehicle_data = getVehicleDataStruct();
k_D = vehicle_data.vehicle.k_D;
m   = vehicle_data.vehicle.m;
Ca_approx = k_D/m;  % approximate aero drag coefficient

% ------------------
%% Create the dataset
% ------------------
% List of speed values [m/s] - add also some negative speed values to
% ensure a feasible behavior even outside the operating range 

% v_max=2 m/s 
% speed_list = [0.0, 0.5, 0.75, 1.0, 1.25, 1.50, 1.65, 1.80, 1.90, 2.10];

% v_max=2.5 m/s 
speed_list = [0.0, 0.5, 0.75, 1.00, 1.25, 1.50, 1.65, 1.80, 2.20, 2.50, 2.70];

% v_max=3 m/s 
% speed_list = [0.0, 0.5, 0.75, 1.00, 1.25, 1.50, 1.65, 1.80, 2.20, 2.50, 2.75, 3.00, 3.20];

% List of max lateral accelerations [m/s^2]
% v_max=2 m/s
% Aymax_list = [0.4, 0.5, 0.60, 1.2, 1.80, 2.65, 3.40, 3.40, 3.50, 3.55];

% v_max=2.5 m/s 
Aymax_list = [0.4, 0.5, 0.60, 1.18, 1.80, 2.65, 3.30, 3.50, 4.60, 4.70, 4.80];

% v_max=3 m/s 
% Aymax_list = [0.4, 0.5, 0.60, 1.18, 1.80, 2.65, 3.30, 3.50, 4.60, 4.70, 4.80, 4.90, 5.00];

safety_factor_Ay = 1;     
Aymax_list = Aymax_list/safety_factor_Ay;

% List of max longitudinal accelerations [m/s^2]
% v_max=2 m/s
% Axmax_list = [3.5, 3.0, 2.85, 2.6, 2.40, 2.10, 1.80, 1.20, 0.02, -0.2] + Ca_approx*speed_list.^2;

% v_max=2.5 m/s
% Axmax_list = [3.5, 3.0, 2.85, 2.70, 2.50, 2.30, 2.15, 2.00, 1.40, 0.01, -0.20] + Ca_approx*speed_list.^2;
Axmax_list = [2.0, 1.8, 1.7, 1.6, 1.50, 1.30, 1.15, 1.00, 0.40, 0.01, -0.20] + Ca_approx*speed_list.^2;

% v_max=3 m/s 
% Axmax_list = [3.4, 3.0, 2.90, 2.80, 2.60, 2.40, 2.30, 2.15, 1.70, 1.20, 0.60, 0.01, -0.20] + Ca_approx*speed_list.^2;
% Axmax_list = [2.0, 1.9, 1.80, 1.70, 1.60, 1.40, 1.25, 1.15, 0.90, 0.70, 0.40, 0.01, -0.20] + Ca_approx*speed_list.^2;

% List of min longitudinal braking decelerations [m/s^2]
Axmin_list = linspace(0.4,0.5,length(speed_list));   
% Axmin_list = linspace(1,2.5,length(speed_list)); 

% ------------------
%% Approximate the G-G envelope with generalized ellipses
% ------------------
% Coefficients of the generalized ellipses
ax_offs = zeros(size(speed_list)); %k_D/m*speed_list.^2;
ax_max = Axmax_list;  %-ax_offs + Axmax_list
ax_min = abs(Axmin_list);  %abs(-ax_offs + Axmin_list);
ay_max = Aymax_list;
n_upper = 2.0;
n_lower = 2.0;

speed_list_interp = linspace(speed_list(1),speed_list(end),100);
ax_max_interp = interp1(speed_list,ax_max,speed_list_interp,'makima');

% Fit and plot the generalized ellipses, for each speed value
idx_test_select = 5;
u_select = speed_list(idx_test_select);
ax_max_select = ax_max(idx_test_select)- Ca_approx*speed_list(idx_test_select).^2;
ax_min_select = ax_min(idx_test_select);
ay_max_select = ay_max(idx_test_select);
ax_offs_select = ax_offs(idx_test_select);
GG_fit = @(ay,ax)generalizedEllipse(ax,ay,ax_max_select,ax_min_select,ay_max_select,n_upper,n_lower,ax_offs_select);

% Limits for figure axes
ax_negPlotLimit = -5;
ax_posPlotLimit = 5;
ay_negPlotLimit = -7;
ay_posPlotLimit = 7;

enable_docked = 1;
if (~enable_docked)
    figure('Name','QSS Fitted G-G Diagram','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,50,700,400])
else
    figure('Name','QSS Fitted G-G Diagram','NumberTitle','off'), clf
end   
hold on 
plot(0,ax_offs_select+ax_max_select,'bo','MarkerFaceColor','b','MarkerSize',12)
plot(0,ax_offs_select-ax_min_select,'bo','MarkerFaceColor','b','MarkerSize',12)
plot(Aymax_list(idx_test_select),ax_offs_select,'bo','MarkerFaceColor','b','MarkerSize',12)
plot(-Aymax_list(idx_test_select),ax_offs_select,'bo','MarkerFaceColor','b','MarkerSize',12)
fimplicit(GG_fit,[ay_negPlotLimit ay_posPlotLimit ax_negPlotLimit ax_posPlotLimit],'LineWidth',2.5,'Color',color('orange'))
grid on
axis equal
xlim([ay_negPlotLimit ay_posPlotLimit])
ylim([ax_negPlotLimit ax_posPlotLimit])
xlabel('$a_y$ [m/s$^2$]')
ylabel('$a_x$ [m/s$^2$]')
title(sprintf('QSS Fitted G-G Diagram @ %.1f m/s',u_select))

% ------------------
%% Fit the generalized ellipses coefficients as a function of vehicle speed
% ------------------
options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-9,...
'StepTolerance',1e-9,'Display','final-detailed');
speed_fit = min(speed_list):0.01:max(speed_list);  % used only for the fitting

% fit Aymax vs speed
fit_Aymax = @(x,speed_list)(x(1) + x(2)*speed_list + x(3)*speed_list.^2 + x(4)*speed_list.^3 + x(5)*speed_list.^4 + x(6)*speed_list.^5); 
x0_Aymax = [0,0,0,0,0,0];
[optim_coeffs_Aymax,resnorm_Aymax,~,exitflag_Aymax,output_Aymax] = lsqcurvefit(fit_Aymax,x0_Aymax,speed_list,ay_max,[],[],options);
Aymax_fitCoeff_0 = optim_coeffs_Aymax(1);
Aymax_fitCoeff_1 = optim_coeffs_Aymax(2);
Aymax_fitCoeff_2 = optim_coeffs_Aymax(3);
Aymax_fitCoeff_3 = optim_coeffs_Aymax(4);
Aymax_fitCoeff_4 = optim_coeffs_Aymax(5);
Aymax_fitCoeff_5 = optim_coeffs_Aymax(6);
Aymax_fitCoeffs  = [Aymax_fitCoeff_0,Aymax_fitCoeff_1,Aymax_fitCoeff_2,Aymax_fitCoeff_3,Aymax_fitCoeff_4,Aymax_fitCoeff_5];
fitted_Aymax = Aymax_fitCoeff_0 + Aymax_fitCoeff_1*speed_fit + Aymax_fitCoeff_2*speed_fit.^2 + Aymax_fitCoeff_3*speed_fit.^3 + Aymax_fitCoeff_4*speed_fit.^4 + Aymax_fitCoeff_5*speed_fit.^5;

% fit Axmax vs speed
fit_Axmax = @(x,speed_list)(x(1) + x(2)*speed_list + x(3)*speed_list.^2 + x(4)*speed_list.^3 + x(5)*speed_list.^4 + x(6)*speed_list.^5 + x(7)*speed_list.^6 + x(8)*speed_list.^7); 
x0_Axmax = [0,0,0,0,0,0,0,0];
[optim_coeffs_Axmax,resnorm_Axmax,~,exitflag_Axmax,output_Axmax] = lsqcurvefit(fit_Axmax,x0_Axmax,speed_list_interp,ax_max_interp,[],[],options);
Axmax_fitCoeff_0 = optim_coeffs_Axmax(1);
Axmax_fitCoeff_1 = optim_coeffs_Axmax(2);
Axmax_fitCoeff_2 = optim_coeffs_Axmax(3);
Axmax_fitCoeff_3 = optim_coeffs_Axmax(4);
Axmax_fitCoeff_4 = optim_coeffs_Axmax(5);
Axmax_fitCoeff_5 = optim_coeffs_Axmax(6);
Axmax_fitCoeff_6 = optim_coeffs_Axmax(7);
Axmax_fitCoeff_7 = optim_coeffs_Axmax(8);
Axmax_fitCoeffs  = [Axmax_fitCoeff_0,Axmax_fitCoeff_1,Axmax_fitCoeff_2,Axmax_fitCoeff_3,Axmax_fitCoeff_4,Axmax_fitCoeff_5,Axmax_fitCoeff_6,Axmax_fitCoeff_7];
fitted_Axmax = Axmax_fitCoeff_0 + Axmax_fitCoeff_1*speed_fit + Axmax_fitCoeff_2*speed_fit.^2 + Axmax_fitCoeff_3*speed_fit.^3 + Axmax_fitCoeff_4*speed_fit.^4 + Axmax_fitCoeff_5*speed_fit.^5 + Axmax_fitCoeff_6*speed_fit.^6 + Axmax_fitCoeff_7*speed_fit.^7;

% fit Axmin vs speed
fit_Axmin = @(x,speed_list)(x(1) + x(2)*speed_list + x(3)*speed_list.^2 + x(4)*speed_list.^3 + x(5)*speed_list.^4); 
x0_Axmin = [0,0,0,0,0];
[optim_coeffs_Axmin,resnorm_Axmin,~,exitflag_Axmin,output_Axmin] = lsqcurvefit(fit_Axmin,x0_Axmin,speed_list,ax_min,[],[],options);
Axmin_fitCoeff_0 = optim_coeffs_Axmin(1);
Axmin_fitCoeff_1 = optim_coeffs_Axmin(2);
Axmin_fitCoeff_2 = optim_coeffs_Axmin(3);
Axmin_fitCoeff_3 = optim_coeffs_Axmin(4);
Axmin_fitCoeff_4 = optim_coeffs_Axmin(5);
Axmin_fitCoeffs  = [Axmin_fitCoeff_0,Axmin_fitCoeff_1,Axmin_fitCoeff_2,Axmin_fitCoeff_3,Axmin_fitCoeff_4];
fitted_Axmin = Axmin_fitCoeff_0 + Axmin_fitCoeff_1*speed_fit + Axmin_fitCoeff_2*speed_fit.^2 + Axmin_fitCoeff_3*speed_fit.^3 + Axmin_fitCoeff_4*speed_fit.^4;

% ------------------
%% Plot fitting results
% ------------------
% Plot the experimental data together with the fitting results
if (~enable_docked)
    figure('Name','Max accelerations','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,700,400])
else
    figure('Name','Max accelerations','NumberTitle','off'), clf
end
% --- Axmax --- %
ax(1) = subplot(221);
hold on
plot(speed_list,ax_max,'go','MarkerFaceColor','g','MarkerSize',12)
plot(speed_list_interp,ax_max_interp,'ro','MarkerFaceColor','r','MarkerSize',4)
plot(speed_fit,fitted_Axmax,'-b','MarkerFaceColor','b','LineWidth',3)
grid on
xlabel('$u$ [m/s]')
ylabel('$A_{xmax}$ [m/s$^2$]')
legend('real','makima interp','fitted','location','best')
xlim([min(speed_fit) max(speed_fit)])
% --- Axmin --- %
ax(2) = subplot(222);
hold on
plot(speed_list,ax_min,'go','MarkerFaceColor','g','MarkerSize',12)
plot(speed_fit,fitted_Axmin,'-b','MarkerFaceColor','b','LineWidth',3)
grid on
xlabel('$u$ [m/s]')
ylabel('$A_{xmin}$ [m/s$^2$]')
legend('real','fitted','location','best')
xlim([min(speed_fit) max(speed_fit)])
% --- Aymax --- %
ax(3) = subplot(223);
hold on
plot(speed_list,ay_max,'go','MarkerFaceColor','g','MarkerSize',12)
plot(speed_fit,fitted_Aymax,'-b','MarkerFaceColor','b','LineWidth',3)
grid on
xlabel('$u$ [m/s]')
ylabel('$A_{ymax}$ [m/s$^2$]')
legend('real','fitted','location','best')
xlim([min(speed_fit) max(speed_fit)])

% linkaxes(ax,'x')
clear ax

% ------------------
%% Save fitting results
% ------------------
GG_fittingCoeffs.Aymax = Aymax_fitCoeffs;
GG_fittingCoeffs.Axmax = Axmax_fitCoeffs;
GG_fittingCoeffs.Axmin = Axmin_fitCoeffs;
GG_fittingCoeffs.ax_offs = ax_offs;
GG_fittingCoeffs.n_upp = n_upper;
GG_fittingCoeffs.n_low = n_lower;

if (enable_save_GGenvelope)
    save('./Fitted_GG_Envelope/GG_fitCoeffs','GG_fittingCoeffs');
    save('./Fitted_GG_Envelope/Aymax_fittingCoeffs','Aymax_fitCoeffs');
    save('./Fitted_GG_Envelope/Axmax_fittingCoeffs','Axmax_fitCoeffs');
    save('./Fitted_GG_Envelope/Axmin_fittingCoeffs','Axmin_fitCoeffs');
end



