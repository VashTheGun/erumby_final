function sensorsData = extractSensorsData(remap_telemetry,remap_optitrack,vehicle_data)

    % -----------------------------------------------------
    %% function purpose:  extract and save sensors data
    % -----------------------------------------------------
    %     --> subscript _T is used for telemetry data
    %     --> subscript _O is used for optitrack data
    
    %% Load vehicle data
    Lf = vehicle_data.vehicle.Lf;
    Lr = vehicle_data.vehicle.Lr;
    delta_offs = vehicle_data.vehicle.delta_offs;
    
    %% Extract telemetry and OptiTrack data
    % Telemetry data
    time_T  = remap_telemetry.time;
    delta_T = remap_telemetry.steering - delta_offs; %-((Lf+Lr)*(0.0404 + 0.0170*remap_telemetry.steering));  % [rad]
    Omega_T = remap_telemetry.gyro_z;
    Speed_T = remap_telemetry.V_enc;
    Ay_T    = remap_telemetry.a_y;
    Ax_T    = remap_telemetry.a_x;
    % OptiTrack data
    time_O  = remap_optitrack.time;
    x_O     = remap_optitrack.x_gps;
    y_O     = remap_optitrack.y_gps;
    psi_O   = remap_optitrack.yaw;
    Omega_O = remap_optitrack.yaw_rate_gps;
    u_O     = remap_optitrack.Vx_gps;        % longitudinal speed [m/s]
    v_O     = remap_optitrack.Vy_gps;        % lateral speed [m/s]
    Speed_O = sqrt(u_O.^2 + v_O.^2);         % total speed [m/s]
    Ay_ss_O = Speed_O.*Omega_O;
    Ay_ss_T = Speed_O.*Omega_T;
    
    %% Save data
    sensorsData.time = time_T;
    sensorsData.delta = delta_T;
    sensorsData.Omega_T = Omega_T;
    sensorsData.Omega_O = Omega_O;
    sensorsData.Speed = Speed_O;
    sensorsData.Speed_T = Speed_T;
    sensorsData.Ay_T = Ay_T;
    sensorsData.Ay_ss_O = Ay_ss_O;
    sensorsData.Ay_ss_T = Ay_ss_T;
    sensorsData.Ax_T = Ax_T;
    sensorsData.x_O = x_O;
    sensorsData.y_O = y_O;
    sensorsData.psi_O = psi_O;

end

