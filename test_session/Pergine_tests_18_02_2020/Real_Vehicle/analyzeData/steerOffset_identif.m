% --------------------------------------------
%% Identification of the steering angle offset
% --------------------------------------------

% The steering angle offset delta_offs is identified by feeding the vehicle
% with zero steering, and computing the corresponding trajectory curvature,
% under the assumption of perfect kinematic steering (the test is performed at low speed)
start_sample_offsetIdentif = 12000;
rho_curv = mean(filteredData_test1.Omega_T(start_sample_offsetIdentif:end)./filteredData_test1.Speed_O(start_sample_offsetIdentif:end));

delta_offs = rho_curv*L;  % -> delta_offs = 0.04049*L = 0.013159 [rad]