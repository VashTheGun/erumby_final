% ----------------------------------------------------------------
%% Main script for eRumby handling & max performance identification
%   - authors: Mattia Piccinini & Edoardo Pagot
%   - date:    19/02/2020
% ----------------------------------------------------------------

% ---------------------
%% Initialize
% ---------------------
initialize_script;
% Enter the test name to read the corresponding files
testName = 'test14';

% Set this flag to 1 in order to plot the handling diagram 
enable_plot_handlingDiagr = 1;
enable_saveHandlDiagr_points = 0;
enable_fit_Kus_vs_u  = 0;

% ---------------------
%% Load vehicle data
% ---------------------
vehicle_data = getVehicleDataStruct();
m = vehicle_data.vehicle.m;            % [kg] Vehicle mass
L = vehicle_data.vehicle.L;            % [m] Vehicle wheelbase
delta_offs = vehicle_data.vehicle.delta_offs;  % [rad] offset steering angle to let the vehicle go straight
k_D = vehicle_data.vehicle.k_D;

% ---------------------
%% Import telemetry & OptiTrack sensors data
% ---------------------
Import_Test_Data;

% ---------------------
%% Extract, filter, resize and plot sensors data
% ---------------------
%     --> subscript _T is used for telemetry data
%     --> subscript _O is used for optitrack data
getTestData;
 
% ---------------------
%% Steering behavior
% ---------------------
% Plot handling diagram
if (enable_plot_handlingDiagr)
    fitHandlingDiagram;
end

% fitting_Kus_vs_speed


