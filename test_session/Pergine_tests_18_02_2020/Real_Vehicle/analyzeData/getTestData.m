%     --> subscript _T is used for telemetry data
%     --> subscript _O is used for optitrack data

% Extract the most important sensors data
sensorsData  = extractSensorsData(remap_telemetry,remap_optitrack,vehicle_data);

% Filter and resize sensors data
filter_flag = 1;  % set it to 1 to enable low pass filtering of data
init_sample = 7000;
fin_sample  = 500;  % 500 for test14, 3000 for test12
trailing_flag = 1;
filteredData = resize_and_filter(sensorsData,filter_flag,init_sample,fin_sample,trailing_flag);
time_restrict         = filteredData.time;
delta_restrict        = filteredData.delta;
Omega_T_filt_restrict = filteredData.Omega_T;
Speed_O_filt_restrict = filteredData.Speed_O;
Ay_ss_filt_restrict   = filteredData.Ay_ss_T;
Ax_T_filt             = filteredData.Ax_T;

% Plot sensors data
enable_plotRampSteerSensorsData = 1;
if (enable_plotRampSteerSensorsData)
    plotSensorsData(sensorsData,filteredData,enable_docked,testName);
end




