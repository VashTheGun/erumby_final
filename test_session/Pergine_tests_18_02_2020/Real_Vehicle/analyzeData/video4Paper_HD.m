% ----------------------------------------------------------------------------
%% Video for IROS paper -- identification and fitting of the Handling Diagram
% ----------------------------------------------------------------------------

set(0,'DefaultFigureWindowStyle','normal');
set(0,'defaultAxesFontSize',22)
set(0,'DefaultLegendFontSize',22)

time_paper_shift = time_paper_cut - time_paper_cut(1);
time_frame       = 1/60;  % [s] -> the video will run at 1/time_frame fps
time_vect_video  = 0:time_frame:(round(time_paper_shift(end),2)+time_frame);

Ay_ss_video          = interp1(time_paper_shift,Ay_ss_paper_cut,time_vect_video);
steer_tendency_video = interp1(time_paper_shift,steer_tendency_paper_cut,time_vect_video);

% -----------------------
% Initialize the figure for the video 
% -----------------------
if (~enable_docked)
    figure('Name','HD for paper','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[550,250,430,350])
else
    figure('Name','HD for paper','NumberTitle','off'), clf
end 
hold on
grid on
xlabel('$a_y$ [m/s$^2$]')
ylabel('$\delta - \frac{\Omega}{v_x}L$ [rad]')
xlim([min(Ay_ss_resize)-0.5 max(Ay_ss_resize)+0.5])
ylim([min(steer_tendency_resize)-0.02 max(steer_tendency_resize)+0.02])

% -----------------------
% Record the video for the handling diagram generation
% -----------------------
vidfile = VideoWriter('HandlDiagram_video.mp4','MPEG-4');
vidfile.FrameRate = 60;
vidfile.Quality   = 95;
open(vidfile);

for jj=1:length(time_vect_video)-1
    plot(Ay_ss_video(jj),steer_tendency_video(jj),'bo','MarkerFaceColor','b','MarkerSize',4,'HandleVisibility','off')
    frames_save(jj) = getframe(gcf); 
    writeVideo(vidfile,frames_save(jj));
end

plot(Ay_ss_video(length(time_vect_video)),steer_tendency_video(length(time_vect_video)),'bo','MarkerFaceColor','b','MarkerSize',4)
frames_save(length(time_vect_video)) = getframe(gcf); 
writeVideo(vidfile,frames_save(length(time_vect_video)));

line([-5 -5],[-0.3 0.3],'Color',color('red'),'LineStyle','--','LineWidth',2);
line([5 5],[-0.3 0.3],'Color',color('red'),'LineStyle','--','LineWidth',2,'HandleVisibility','off');
frames_save(length(time_vect_video)+1) = getframe(gcf); 
writeVideo(vidfile,frames_save(length(time_vect_video)+1));

plot(Ay_sineSteer_fit,fitted_handlingDiagram,'-','Color',color('green'),'LineWidth',4)
legend('raw','$a_y$ bounds','fitted','location','south')
frames_save(length(time_vect_video)+2) = getframe(gcf); 
writeVideo(vidfile,frames_save(length(time_vect_video)+2));

for kk=1:60
    frames_save(length(time_vect_video)+2+kk) = getframe(gcf); 
    writeVideo(vidfile,frames_save(length(time_vect_video)+2+kk));
end

close(vidfile)
