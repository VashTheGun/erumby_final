% ----------------------------------------------------------------------------
%% Video for IROS paper -- identification and fitting of the G-G diagram with a sine-steer maneuver
% ----------------------------------------------------------------------------

set(0,'DefaultFigureWindowStyle','normal');
set(0,'defaultAxesFontSize',22)
set(0,'DefaultLegendFontSize',22)

time_paper_shift = time_paper_cut - time_paper_cut(1);
time_frame       = 1/60;  % [s] -> the video will run at 1/time_frame fps
time_vect_video  = 0:time_frame:(round(time_paper_shift(end),2)+time_frame);

Ay_ss_video = interp1(time_paper_shift,Ay_ss_paper_cut,time_vect_video,'makima');
Ax_video    = interp1(time_paper_shift,Ax_paper_cut,time_vect_video,'makima');
vx_video    = interp1(time_paper_shift,vx_paper_cut,time_vect_video,'makima');

% Load the fitted G-G envelope
Axmax_fittingCoeffs = load('./Fitted_GG_Envelope/Axmax_fittingCoeffs');
Axmin_fittingCoeffs = load('./Fitted_GG_Envelope/Axmin_fittingCoeffs');
Aymax_fittingCoeffs = load('./Fitted_GG_Envelope/Aymax_fittingCoeffs');
Axmax_fitCoeffs = Axmax_fittingCoeffs.Axmax_fitCoeffs;
Axmin_fitCoeffs = Axmin_fittingCoeffs.Axmin_fitCoeffs;
Aymax_fitCoeffs = Aymax_fittingCoeffs.Aymax_fitCoeffs;

speed_vect_fit = 0.5:0.1:2.5;  % [m/s]
vehicle_data = getVehicleDataStruct();
k_D = vehicle_data.vehicle.k_D;
m   = vehicle_data.vehicle.m;

% Limits for figure axes
ax_negPlotLimit = -5;
ax_posPlotLimit = 5;
ay_negPlotLimit = -7;
ay_posPlotLimit = 7;

% Define a custom color order (each color corresponds to a certain speed value)
ColorOdr = flipud(parula(length(speed_vect_fit)));

% -----------------------
% Initialize the figure for the video 
% -----------------------
if (~enable_docked)
    figure('Name','GG for paper','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[550,250,430,350])
else
    figure('Name','GG for paper','NumberTitle','off'), clf
end 
hold on
grid on
xlabel('$a_y$ [m/s$^2$]')
ylabel('$a_x$ [m/s$^2$]')
zlabel('$v_x$ [m/s]')
xlim([-6 6])
ylim([-0.7 1.85])
zlim([0 2.6])

% -----------------------
%% Record the video for the G-G diagram generation
% -----------------------
vidfile = VideoWriter('GG_Diagram_video.mp4','MPEG-4');
vidfile.FrameRate = 60;
vidfile.Quality   = 95;
open(vidfile);

for jj=1:length(time_vect_video)-1
    plot3(Ay_ss_video(jj),Ax_video(jj),vx_video(jj),'o','Color',color('violet_red'),...
                                'MarkerFaceColor',color('violet_red'),'LineWidth',4,'HandleVisibility','off')
    frames_save_GG(jj) = getframe(gcf); 
    writeVideo(vidfile,frames_save_GG(jj));
end

plot3(Ay_ss_video(length(time_vect_video)),Ax_video(length(time_vect_video)),vx_video(length(time_vect_video)),'o','Color',color('violet_red'),...
                                'MarkerFaceColor',color('violet_red'),'LineWidth',4)
frames_save_GG(length(time_vect_video)) = getframe(gcf); 
writeVideo(vidfile,frames_save_GG(length(time_vect_video)));

for ii = 1:length(speed_vect_fit) 
    ax_max  = Axmax_fitCoeffs(1) + Axmax_fitCoeffs(2)*speed_vect_fit(ii) + Axmax_fitCoeffs(3)*speed_vect_fit(ii).^2 + Axmax_fitCoeffs(4)*speed_vect_fit(ii).^3 + Axmax_fitCoeffs(5)*speed_vect_fit(ii).^4 + Axmax_fitCoeffs(6)*speed_vect_fit(ii).^5 + Axmax_fitCoeffs(7)*speed_vect_fit(ii).^6 + Axmax_fitCoeffs(8)*speed_vect_fit(ii).^7;
    % ax_max  = ax_max - (k_D/m)*speed_vect_fit(jj)^2;
    ax_min  = Axmin_fitCoeffs(1) + Axmin_fitCoeffs(2)*speed_vect_fit(ii) + Axmin_fitCoeffs(3)*speed_vect_fit(ii).^2 + Axmin_fitCoeffs(4)*speed_vect_fit(ii).^3 + Axmin_fitCoeffs(5)*speed_vect_fit(ii).^4;
    ay_max  = Aymax_fitCoeffs(1) + Aymax_fitCoeffs(2)*speed_vect_fit(ii) + Aymax_fitCoeffs(3)*speed_vect_fit(ii).^2 + Aymax_fitCoeffs(4)*speed_vect_fit(ii).^3 + Aymax_fitCoeffs(5)*speed_vect_fit(ii).^4 + Aymax_fitCoeffs(6)*speed_vect_fit(ii).^5;
    ax_offs = 0;
    n_upper = 2;
    n_lower = 2;
    GG_fit = @(ay,ax,u)generalizedEllipse(ax,ay,ax_max,ax_min,ay_max,n_upper,n_lower,ax_offs);
    fimplicit3(GG_fit,[ay_negPlotLimit ay_posPlotLimit ax_negPlotLimit-4 ax_posPlotLimit speed_vect_fit(ii) speed_vect_fit(ii)+0.01],'MeshDensity',40,'EdgeColor',ColorOdr(ii,:),'LineWidth',2.5)
end
oldcmap = colormap;
colormap(flipud(oldcmap));
hcb = colorbar;
title(hcb,'$v_x$ [m/s]','Interpreter','latex')
caxis([speed_vect_fit(1), speed_vect_fit(end)]);
frames_save_GG(length(time_vect_video)+1) = getframe(gcf); 
writeVideo(vidfile,frames_save_GG(length(time_vect_video)+1));

for kk=1:60
    frames_save_GG(length(time_vect_video)+1+kk) = getframe(gcf); 
    writeVideo(vidfile,frames_save_GG(length(time_vect_video)+1+kk));
end

close(vidfile)
