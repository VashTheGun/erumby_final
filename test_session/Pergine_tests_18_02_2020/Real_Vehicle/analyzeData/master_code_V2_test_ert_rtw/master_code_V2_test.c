/*
 * master_code_V2_test.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "master_code_V2_test".
 *
 * Model version              : 1.215
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Mon Mar  2 13:21:09 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "master_code_V2_test.h"
#include "master_code_V2_test_private.h"

/* Named constants for Chart: '<Root>/ECU_STATE_MACHINE' */
#define master_code_V2_te_IN_idle_state ((uint8_T)1U)
#define master_code_V_DUTY_SERVO_MIDDLE ((uint16_T)0U)
#define master_code_V_IN_initialization ((uint8_T)2U)
#define master_code__IN_NO_ACTIVE_CHILD ((uint8_T)0U)
#define master_code__IN_reading_sensors ((uint8_T)3U)

/* Block signals (default storage) */
B_master_code_V2_test_T master_code_V2_test_B;

/* Block states (default storage) */
DW_master_code_V2_test_T master_code_V2_test_DW;

/* Real-time model */
RT_MODEL_master_code_V2_test_T master_code_V2_test_M_;
RT_MODEL_master_code_V2_test_T *const master_code_V2_test_M =
  &master_code_V2_test_M_;

/* Forward declaration for local functions */
static void master_code__SystemCore_release(const
  codertarget_raspi_internal_I2_T *obj);
static void master_code_V_SystemCore_delete(const
  codertarget_raspi_internal_I2_T *obj);
static void matlabCodegenHandle_matlabCodeg(codertarget_raspi_internal_I2_T *obj);

/* Forward declaration for local functions */
static void m_SystemCore_release_p01msq5wle(const
  codertarget_raspi__nparbhma5g_T *obj);
static void ma_SystemCore_delete_jwpcm5lajz(const
  codertarget_raspi__nparbhma5g_T *obj);
static void matlabCodegenHandle__eekrmxfgnd(codertarget_raspi__nparbhma5g_T *obj);

/* Forward declaration for local functions */
static void m_SystemCore_release_aabbulabox(const
  codertarget_raspi_internal_I2_T *obj);
static void ma_SystemCore_delete_oans5guacd(const
  codertarget_raspi_internal_I2_T *obj);
static void matlabCodegenHandle__mvxzfw4jho(codertarget_raspi_internal_I2_T *obj);

/* Forward declaration for local functions */
static boolean_T master_code_V2_findpivotIdx_u16(int32_T i, int32_T j, int32_T
  startIdx, int32_T pivot[], uint32_T midxPort[], const uint16_T mdataPort[]);
static int32_T master_code_V2_partitionIdx_u16(int32_T i, int32_T j, int32_T
  startIdx, const int32_T pivot[], uint32_T midxPort[], const uint16_T
  mdataPort[]);
static void master_code_V2_tes_MDNQSort_u16(int32_T i, int32_T j, int32_T
  startIdx, uint32_T midxPort[], const uint16_T mdataPort[]);
static void m_SystemCore_release_byt53hqfpc(const
  codertarget_raspi__nparbhma5g_T *obj);
static void ma_SystemCore_delete_byt53hqfpc(const
  codertarget_raspi__nparbhma5g_T *obj);
static void matlabCodegenHandle__byt53hqfpc(codertarget_raspi__nparbhma5g_T *obj);

/*
 * Writes out MAT-file header.  Returns success or failure.
 * Returns:
 *      0 - success
 *      1 - failure
 */
int_T rt_WriteMat4FileHeader(FILE *fp, int32_T m, int32_T n, const char *name)
{
  typedef enum { ELITTLE_ENDIAN, EBIG_ENDIAN } ByteOrder;

  int16_T one = 1;
  ByteOrder byteOrder = (*((int8_T *)&one)==1) ? ELITTLE_ENDIAN : EBIG_ENDIAN;
  int32_T type = (byteOrder == ELITTLE_ENDIAN) ? 0: 1000;
  int32_T imagf = 0;
  int32_T name_len = (int32_T)strlen(name) + 1;
  if ((fwrite(&type, sizeof(int32_T), 1, fp) == 0) ||
      (fwrite(&m, sizeof(int32_T), 1, fp) == 0) ||
      (fwrite(&n, sizeof(int32_T), 1, fp) == 0) ||
      (fwrite(&imagf, sizeof(int32_T), 1, fp) == 0) ||
      (fwrite(&name_len, sizeof(int32_T), 1, fp) == 0) ||
      (fwrite(name, sizeof(char), name_len, fp) == 0)) {
    return(1);
  } else {
    return(0);
  }
}                                      /* end rt_WriteMat4FileHeader */

static void master_code__SystemCore_release(const
  codertarget_raspi_internal_I2_T *obj)
{
  if ((obj->isInitialized == 1) && obj->isSetupComplete) {
    MW_I2C_Close(obj->MW_I2C_HANDLE);
  }
}

static void master_code_V_SystemCore_delete(const
  codertarget_raspi_internal_I2_T *obj)
{
  master_code__SystemCore_release(obj);
}

static void matlabCodegenHandle_matlabCodeg(codertarget_raspi_internal_I2_T *obj)
{
  if (!obj->matlabCodegenIsDeleted) {
    obj->matlabCodegenIsDeleted = true;
    master_code_V_SystemCore_delete(obj);
  }
}

/* Start for function-call system: '<S1>/i2cWr_mpu' */
void master_code_V2__i2cWr_mpu_Start(DW_i2cWr_mpu_master_code_V2_t_T *localDW)
{
  codertarget_raspi_internal_I2_T *obj;
  uint32_T i2cname;

  /* Start for MATLABSystem: '<S8>/I2C Master Write' */
  localDW->obj.matlabCodegenIsDeleted = true;
  localDW->obj.isInitialized = 0;
  localDW->obj.matlabCodegenIsDeleted = false;
  obj = &localDW->obj;
  localDW->obj.isSetupComplete = false;
  localDW->obj.isInitialized = 1;
  i2cname = 1;
  obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
  localDW->obj.BusSpeed = 100000U;
  MW_I2C_SetBusSpeed(localDW->obj.MW_I2C_HANDLE, localDW->obj.BusSpeed);
  localDW->obj.isSetupComplete = true;
}

/* Output and update for function-call system: '<S1>/i2cWr_mpu' */
void master_code_V2_test_i2cWr_mpu(uint16_T rtu_dataw,
  DW_i2cWr_mpu_master_code_V2_t_T *localDW)
{
  uint8_T SwappedDataBytes[2];
  uint16_T b_x;
  uint8_T b_x_0[2];

  /* MATLABSystem: '<S8>/I2C Master Write' */
  memcpy((void *)&SwappedDataBytes[0], (void *)&rtu_dataw, (uint32_T)((size_t)2 *
          sizeof(uint8_T)));
  b_x_0[0] = SwappedDataBytes[1];
  b_x_0[1] = SwappedDataBytes[0];
  memcpy((void *)&b_x, (void *)&b_x_0[0], (uint32_T)((size_t)1 * sizeof(uint16_T)));
  memcpy((void *)&SwappedDataBytes[0], (void *)&b_x, (uint32_T)((size_t)2 *
          sizeof(uint8_T)));
  MW_I2C_MasterWrite(localDW->obj.MW_I2C_HANDLE, 105U, SwappedDataBytes, 2U,
                     false, false);
}

/* Termination for function-call system: '<S1>/i2cWr_mpu' */
void master_code_V2_t_i2cWr_mpu_Term(DW_i2cWr_mpu_master_code_V2_t_T *localDW)
{
  /* Terminate for MATLABSystem: '<S8>/I2C Master Write' */
  matlabCodegenHandle_matlabCodeg(&localDW->obj);
}

static void m_SystemCore_release_p01msq5wle(const
  codertarget_raspi__nparbhma5g_T *obj)
{
  if ((obj->isInitialized == 1) && obj->isSetupComplete) {
    MW_I2C_Close(obj->MW_I2C_HANDLE);
  }
}

static void ma_SystemCore_delete_jwpcm5lajz(const
  codertarget_raspi__nparbhma5g_T *obj)
{
  m_SystemCore_release_p01msq5wle(obj);
}

static void matlabCodegenHandle__eekrmxfgnd(codertarget_raspi__nparbhma5g_T *obj)
{
  if (!obj->matlabCodegenIsDeleted) {
    obj->matlabCodegenIsDeleted = true;
    ma_SystemCore_delete_jwpcm5lajz(obj);
  }
}

/* System initialize for function-call system: '<S1>/i2cRd' */
void master_code_V2_test_i2cRd_Init(B_i2cRd_master_code_V2_test_T *localB,
  P_i2cRd_master_code_V2_test_T *localP)
{
  int32_T i;

  /* SystemInitialize for Outport: '<S7>/data_imu' */
  for (i = 0; i < 7; i++) {
    localB->DataTypeConversion4[i] = localP->data_imu_Y0;
  }

  /* End of SystemInitialize for Outport: '<S7>/data_imu' */

  /* SystemInitialize for Outport: '<S7>/data_enc_rear' */
  localB->DataTypeConversion1[0] = localP->data_enc_rear_Y0;
  localB->DataTypeConversion1[1] = localP->data_enc_rear_Y0;
  localB->DataTypeConversion1[2] = localP->data_enc_rear_Y0;

  /* SystemInitialize for Outport: '<S7>/data_enc_front' */
  localB->DataTypeConversion6[0] = localP->data_enc_front_Y0;
  localB->DataTypeConversion6[1] = localP->data_enc_front_Y0;
}

/* Start for function-call system: '<S1>/i2cRd' */
void master_code_V2_test_i2cRd_Start(DW_i2cRd_master_code_V2_test_T *localDW,
  P_i2cRd_master_code_V2_test_T *localP)
{
  codertarget_raspi__nparbhma5g_T *obj;
  uint32_T i2cname;

  /* Start for MATLABSystem: '<S7>/I2C Master Read4' */
  localDW->obj.matlabCodegenIsDeleted = true;
  localDW->obj.isInitialized = 0;
  localDW->obj.matlabCodegenIsDeleted = false;
  localDW->obj.SampleTime = localP->I2CMasterRead4_SampleTime;
  obj = &localDW->obj;
  localDW->obj.isSetupComplete = false;
  localDW->obj.isInitialized = 1;
  i2cname = 1;
  obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
  localDW->obj.BusSpeed = 100000U;
  MW_I2C_SetBusSpeed(localDW->obj.MW_I2C_HANDLE, localDW->obj.BusSpeed);
  localDW->obj.isSetupComplete = true;

  /* Start for MATLABSystem: '<S7>/I2C Master Read1' */
  localDW->obj_mxsciaoufc.matlabCodegenIsDeleted = true;
  localDW->obj_mxsciaoufc.isInitialized = 0;
  localDW->obj_mxsciaoufc.matlabCodegenIsDeleted = false;
  localDW->obj_mxsciaoufc.SampleTime = localP->I2CMasterRead1_SampleTime;
  obj = &localDW->obj_mxsciaoufc;
  localDW->obj_mxsciaoufc.isSetupComplete = false;
  localDW->obj_mxsciaoufc.isInitialized = 1;
  i2cname = 1;
  obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
  localDW->obj_mxsciaoufc.BusSpeed = 100000U;
  MW_I2C_SetBusSpeed(localDW->obj_mxsciaoufc.MW_I2C_HANDLE,
                     localDW->obj_mxsciaoufc.BusSpeed);
  localDW->obj_mxsciaoufc.isSetupComplete = true;

  /* Start for MATLABSystem: '<S7>/I2C Master Read6' */
  localDW->obj_dz0rzkn42o.matlabCodegenIsDeleted = true;
  localDW->obj_dz0rzkn42o.isInitialized = 0;
  localDW->obj_dz0rzkn42o.matlabCodegenIsDeleted = false;
  localDW->obj_dz0rzkn42o.SampleTime = localP->I2CMasterRead6_SampleTime;
  obj = &localDW->obj_dz0rzkn42o;
  localDW->obj_dz0rzkn42o.isSetupComplete = false;
  localDW->obj_dz0rzkn42o.isInitialized = 1;
  i2cname = 1;
  obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
  localDW->obj_dz0rzkn42o.BusSpeed = 100000U;
  MW_I2C_SetBusSpeed(localDW->obj_dz0rzkn42o.MW_I2C_HANDLE,
                     localDW->obj_dz0rzkn42o.BusSpeed);
  localDW->obj_dz0rzkn42o.isSetupComplete = true;
}

/* Output and update for function-call system: '<S1>/i2cRd' */
void master_code_V2_test_i2cRd(B_i2cRd_master_code_V2_test_T *localB,
  DW_i2cRd_master_code_V2_test_T *localDW, P_i2cRd_master_code_V2_test_T *localP)
{
  uint16_T b_output[7];
  uint8_T status;
  uint8_T output_raw[14];
  uint8_T y[2];
  uint16_T x;
  uint8_T b_x[2];
  uint16_T output[3];
  uint8_T output_raw_0[6];
  uint16_T output_0[2];
  uint8_T output_raw_1[4];
  int32_T i;

  /* MATLABSystem: '<S7>/I2C Master Read4' */
  if (localDW->obj.SampleTime != localP->I2CMasterRead4_SampleTime) {
    localDW->obj.SampleTime = localP->I2CMasterRead4_SampleTime;
  }

  status = 59U;
  status = MW_I2C_MasterWrite(localDW->obj.MW_I2C_HANDLE, 105U, &status, 1U,
    true, false);
  if (0 == status) {
    MW_I2C_MasterRead(localDW->obj.MW_I2C_HANDLE, 105U, output_raw, 14U, false,
                      true);
    memcpy((void *)&b_output[0], (void *)&output_raw[0], (uint32_T)((size_t)7 *
            sizeof(uint16_T)));
    for (i = 0; i < 7; i++) {
      x = b_output[i];
      memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
      b_x[0] = y[1];
      b_x[1] = y[0];
      memcpy((void *)&b_output[i], (void *)&b_x[0], (uint32_T)((size_t)1 *
              sizeof(uint16_T)));
    }
  } else {
    for (i = 0; i < 7; i++) {
      b_output[i] = 0U;
    }
  }

  /* DataTypeConversion: '<S7>/Data Type Conversion4' incorporates:
   *  MATLABSystem: '<S7>/I2C Master Read4'
   */
  for (i = 0; i < 7; i++) {
    localB->DataTypeConversion4[i] = (int16_T)b_output[i];
  }

  /* End of DataTypeConversion: '<S7>/Data Type Conversion4' */

  /* MATLABSystem: '<S7>/I2C Master Read1' */
  if (localDW->obj_mxsciaoufc.SampleTime != localP->I2CMasterRead1_SampleTime) {
    localDW->obj_mxsciaoufc.SampleTime = localP->I2CMasterRead1_SampleTime;
  }

  MW_I2C_MasterRead(localDW->obj_mxsciaoufc.MW_I2C_HANDLE, 3U, output_raw_0, 6U,
                    false, false);
  memcpy((void *)&output[0], (void *)&output_raw_0[0], (uint32_T)((size_t)3 *
          sizeof(uint16_T)));
  x = output[0];
  memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x[0] = y[1];
  b_x[1] = y[0];
  memcpy((void *)&output[0], (void *)&b_x[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));
  x = output[1];
  memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x[0] = y[1];
  b_x[1] = y[0];
  memcpy((void *)&output[1], (void *)&b_x[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));
  x = output[2];
  memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x[0] = y[1];
  b_x[1] = y[0];
  memcpy((void *)&output[2], (void *)&b_x[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));

  /* DataTypeConversion: '<S7>/Data Type Conversion1' incorporates:
   *  MATLABSystem: '<S7>/I2C Master Read1'
   */
  localB->DataTypeConversion1[0] = (int16_T)output[0];
  localB->DataTypeConversion1[1] = (int16_T)output[1];
  localB->DataTypeConversion1[2] = (int16_T)output[2];

  /* MATLABSystem: '<S7>/I2C Master Read6' */
  if (localDW->obj_dz0rzkn42o.SampleTime != localP->I2CMasterRead6_SampleTime) {
    localDW->obj_dz0rzkn42o.SampleTime = localP->I2CMasterRead6_SampleTime;
  }

  MW_I2C_MasterRead(localDW->obj_dz0rzkn42o.MW_I2C_HANDLE, 4U, output_raw_1, 4U,
                    false, false);
  memcpy((void *)&output_0[0], (void *)&output_raw_1[0], (uint32_T)((size_t)2 *
          sizeof(uint16_T)));
  x = output_0[0];
  memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x[0] = y[1];
  b_x[1] = y[0];
  memcpy((void *)&output_0[0], (void *)&b_x[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));
  x = output_0[1];
  memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x[0] = y[1];
  b_x[1] = y[0];
  memcpy((void *)&output_0[1], (void *)&b_x[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));

  /* DataTypeConversion: '<S7>/Data Type Conversion6' incorporates:
   *  MATLABSystem: '<S7>/I2C Master Read6'
   */
  localB->DataTypeConversion6[0] = (int16_T)output_0[0];
  localB->DataTypeConversion6[1] = (int16_T)output_0[1];
}

/* Termination for function-call system: '<S1>/i2cRd' */
void master_code_V2_test_i2cRd_Term(DW_i2cRd_master_code_V2_test_T *localDW)
{
  /* Terminate for MATLABSystem: '<S7>/I2C Master Read4' */
  matlabCodegenHandle__eekrmxfgnd(&localDW->obj);

  /* Terminate for MATLABSystem: '<S7>/I2C Master Read1' */
  matlabCodegenHandle__eekrmxfgnd(&localDW->obj_mxsciaoufc);

  /* Terminate for MATLABSystem: '<S7>/I2C Master Read6' */
  matlabCodegenHandle__eekrmxfgnd(&localDW->obj_dz0rzkn42o);
}

/* System initialize for function-call system: '<S1>/gpsRd' */
void master_code_V2_test_gpsRd_Init(B_gpsRd_master_code_V2_test_T *localB,
  P_gpsRd_master_code_V2_test_T *localP)
{
  /* SystemInitialize for Outport: '<S6>/gps' */
  localB->UDPReceive_o1[0] = localP->gps_Y0;
  localB->UDPReceive_o1[1] = localP->gps_Y0;
  localB->UDPReceive_o1[2] = localP->gps_Y0;
}

/* Start for function-call system: '<S1>/gpsRd' */
void master_code_V2_test_gpsRd_Start(RT_MODEL_master_code_V2_test_T * const
  master_code_V2_test_M, DW_gpsRd_master_code_V2_test_T *localDW,
  P_gpsRd_master_code_V2_test_T *localP)
{
  char_T *sErr;

  /* Start for S-Function (sdspFromNetwork): '<S6>/UDP Receive' */
  sErr = GetErrorBuffer(&localDW->UDPReceive_NetworkLib[0U]);
  CreateUDPInterface(&localDW->UDPReceive_NetworkLib[0U]);
  if (*sErr == 0) {
    LibCreate_Network(&localDW->UDPReceive_NetworkLib[0U], 0, "0.0.0.0",
                      localP->UDPReceive_Port, "0.0.0.0", -1, 8192, 2, 0);
  }

  if (*sErr == 0) {
    LibStart(&localDW->UDPReceive_NetworkLib[0U]);
  }

  if (*sErr != 0) {
    DestroyUDPInterface(&localDW->UDPReceive_NetworkLib[0U]);
    if (*sErr != 0) {
      rtmSetErrorStatus(master_code_V2_test_M, sErr);
      rtmSetStopRequested(master_code_V2_test_M, 1);
    }
  }

  /* End of Start for S-Function (sdspFromNetwork): '<S6>/UDP Receive' */
}

/* Output and update for function-call system: '<S1>/gpsRd' */
void master_code_V2_test_gpsRd(RT_MODEL_master_code_V2_test_T * const
  master_code_V2_test_M, B_gpsRd_master_code_V2_test_T *localB,
  DW_gpsRd_master_code_V2_test_T *localDW)
{
  char_T *sErr;
  int32_T samplesRead;

  /* S-Function (sdspFromNetwork): '<S6>/UDP Receive' */
  sErr = GetErrorBuffer(&localDW->UDPReceive_NetworkLib[0U]);
  samplesRead = 3;
  LibOutputs_Network(&localDW->UDPReceive_NetworkLib[0U], &localB->
                     UDPReceive_o1[0U], &samplesRead);
  if (*sErr != 0) {
    rtmSetErrorStatus(master_code_V2_test_M, sErr);
    rtmSetStopRequested(master_code_V2_test_M, 1);
  }

  /* End of S-Function (sdspFromNetwork): '<S6>/UDP Receive' */
}

/* Termination for function-call system: '<S1>/gpsRd' */
void master_code_V2_test_gpsRd_Term(RT_MODEL_master_code_V2_test_T * const
  master_code_V2_test_M, DW_gpsRd_master_code_V2_test_T *localDW)
{
  char_T *sErr;

  /* Terminate for S-Function (sdspFromNetwork): '<S6>/UDP Receive' */
  sErr = GetErrorBuffer(&localDW->UDPReceive_NetworkLib[0U]);
  LibTerminate(&localDW->UDPReceive_NetworkLib[0U]);
  if (*sErr != 0) {
    rtmSetErrorStatus(master_code_V2_test_M, sErr);
    rtmSetStopRequested(master_code_V2_test_M, 1);
  }

  LibDestroy(&localDW->UDPReceive_NetworkLib[0U], 0);
  DestroyUDPInterface(&localDW->UDPReceive_NetworkLib[0U]);

  /* End of Terminate for S-Function (sdspFromNetwork): '<S6>/UDP Receive' */
}

static void m_SystemCore_release_aabbulabox(const
  codertarget_raspi_internal_I2_T *obj)
{
  if ((obj->isInitialized == 1) && obj->isSetupComplete) {
    MW_I2C_Close(obj->MW_I2C_HANDLE);
  }
}

static void ma_SystemCore_delete_oans5guacd(const
  codertarget_raspi_internal_I2_T *obj)
{
  m_SystemCore_release_aabbulabox(obj);
}

static void matlabCodegenHandle__mvxzfw4jho(codertarget_raspi_internal_I2_T *obj)
{
  if (!obj->matlabCodegenIsDeleted) {
    obj->matlabCodegenIsDeleted = true;
    ma_SystemCore_delete_oans5guacd(obj);
  }
}

/* Start for function-call system: '<S1>/cmd_motor' */
void master_code_V2__cmd_motor_Start(DW_cmd_motor_master_code_V2_t_T *localDW)
{
  codertarget_raspi_internal_I2_T *obj;
  uint32_T i2cname;

  /* Start for MATLABSystem: '<S5>/I2C Master Write1' */
  localDW->obj.matlabCodegenIsDeleted = true;
  localDW->obj.isInitialized = 0;
  localDW->obj.matlabCodegenIsDeleted = false;
  obj = &localDW->obj;
  localDW->obj.isSetupComplete = false;
  localDW->obj.isInitialized = 1;
  i2cname = 1;
  obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
  localDW->obj.BusSpeed = 100000U;
  MW_I2C_SetBusSpeed(localDW->obj.MW_I2C_HANDLE, localDW->obj.BusSpeed);
  localDW->obj.isSetupComplete = true;
}

/* Output and update for function-call system: '<S1>/cmd_motor' */
void master_code_V2_test_cmd_motor(uint16_T rtu_esc, uint16_T rtu_servo,
  DW_cmd_motor_master_code_V2_t_T *localDW)
{
  uint8_T SwappedDataBytes[4];
  uint16_T b_x[2];
  uint8_T y[2];
  uint16_T x;
  uint8_T b_x_0[2];

  /* MATLABSystem: '<S5>/I2C Master Write1' incorporates:
   *  SignalConversion: '<S5>/TmpSignal ConversionAtI2C Master Write1Inport1'
   */
  b_x[0] = rtu_esc;
  b_x[1] = rtu_servo;
  memcpy((void *)&y[0], (void *)&rtu_esc, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x_0[0] = y[1];
  b_x_0[1] = y[0];
  memcpy((void *)&b_x[0], (void *)&b_x_0[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));
  x = b_x[1];
  memcpy((void *)&y[0], (void *)&x, (uint32_T)((size_t)2 * sizeof(uint8_T)));
  b_x_0[0] = y[1];
  b_x_0[1] = y[0];
  memcpy((void *)&b_x[1], (void *)&b_x_0[0], (uint32_T)((size_t)1 * sizeof
          (uint16_T)));
  memcpy((void *)&SwappedDataBytes[0], (void *)&b_x[0], (uint32_T)((size_t)4 *
          sizeof(uint8_T)));
  MW_I2C_MasterWrite(localDW->obj.MW_I2C_HANDLE, 3U, SwappedDataBytes, 4U, false,
                     false);
}

/* Termination for function-call system: '<S1>/cmd_motor' */
void master_code_V2_t_cmd_motor_Term(DW_cmd_motor_master_code_V2_t_T *localDW)
{
  /* Terminate for MATLABSystem: '<S5>/I2C Master Write1' */
  matlabCodegenHandle__mvxzfw4jho(&localDW->obj);
}

static boolean_T master_code_V2_findpivotIdx_u16(int32_T i, int32_T j, int32_T
  startIdx, int32_T pivot[], uint32_T midxPort[], const uint16_T mdataPort[])
{
  boolean_T pivotFind;

  /* S-Function (sdspmdn2): '<S4>/Median' */
  master_code_V2_test_B.mid = (i + j) >> 1;
  master_code_V2_test_B.sIdx = (uint32_T)startIdx;
  master_code_V2_test_B.tmp0 = mdataPort[midxPort[i] +
    master_code_V2_test_B.sIdx];
  master_code_V2_test_B.tmp1_tmp = mdataPort[midxPort[master_code_V2_test_B.mid]
    + master_code_V2_test_B.sIdx];
  if (master_code_V2_test_B.tmp0 > master_code_V2_test_B.tmp1_tmp) {
    master_code_V2_test_B.t = midxPort[i];
    midxPort[i] = midxPort[master_code_V2_test_B.mid];
    midxPort[master_code_V2_test_B.mid] = master_code_V2_test_B.t;
  }

  if (master_code_V2_test_B.tmp0 > mdataPort[midxPort[j] +
      master_code_V2_test_B.sIdx]) {
    master_code_V2_test_B.t = midxPort[i];
    midxPort[i] = midxPort[j];
    midxPort[j] = master_code_V2_test_B.t;
  }

  if (master_code_V2_test_B.tmp1_tmp > mdataPort[midxPort[j] +
      master_code_V2_test_B.sIdx]) {
    master_code_V2_test_B.t = midxPort[master_code_V2_test_B.mid];
    midxPort[master_code_V2_test_B.mid] = midxPort[j];
    midxPort[j] = master_code_V2_test_B.t;
  }

  master_code_V2_test_B.tmp0 = mdataPort[midxPort[i] +
    master_code_V2_test_B.sIdx];
  master_code_V2_test_B.tmp1_tmp = mdataPort[midxPort[master_code_V2_test_B.mid]
    + master_code_V2_test_B.sIdx];
  pivotFind = false;
  if (master_code_V2_test_B.tmp0 < master_code_V2_test_B.tmp1_tmp) {
    pivot[0U] = master_code_V2_test_B.mid;
    pivotFind = true;
  } else if (master_code_V2_test_B.tmp1_tmp < mdataPort[midxPort[j] +
             master_code_V2_test_B.sIdx]) {
    pivot[0U] = j;
    pivotFind = true;
  } else {
    master_code_V2_test_B.mid = i + 1;
    while ((master_code_V2_test_B.mid <= j) && (!pivotFind)) {
      master_code_V2_test_B.tmp1_tmp =
        mdataPort[midxPort[master_code_V2_test_B.mid] +
        master_code_V2_test_B.sIdx];
      if (master_code_V2_test_B.tmp1_tmp != master_code_V2_test_B.tmp0) {
        if (master_code_V2_test_B.tmp1_tmp < master_code_V2_test_B.tmp0) {
          pivot[0U] = i;
        } else {
          pivot[0U] = master_code_V2_test_B.mid;
        }

        pivotFind = true;
        master_code_V2_test_B.mid = j + 1;
      }

      master_code_V2_test_B.mid++;
    }
  }

  /* End of S-Function (sdspmdn2): '<S4>/Median' */
  return pivotFind;
}

static int32_T master_code_V2_partitionIdx_u16(int32_T i, int32_T j, int32_T
  startIdx, const int32_T pivot[], uint32_T midxPort[], const uint16_T
  mdataPort[])
{
  int32_T idx;

  /* S-Function (sdspmdn2): '<S4>/Median' */
  master_code_V2_test_B.sIdx_cl54gopm0x = (uint32_T)startIdx;
  master_code_V2_test_B.tmp0_cxarnvbvui = mdataPort[midxPort[pivot[0U]] +
    master_code_V2_test_B.sIdx_cl54gopm0x];
  master_code_V2_test_B.ctidx = j - i;
  while ((i <= j) && (master_code_V2_test_B.ctidx >= 0)) {
    while (mdataPort[midxPort[i] + master_code_V2_test_B.sIdx_cl54gopm0x] <
           master_code_V2_test_B.tmp0_cxarnvbvui) {
      i++;
    }

    while (mdataPort[midxPort[j] + master_code_V2_test_B.sIdx_cl54gopm0x] >=
           master_code_V2_test_B.tmp0_cxarnvbvui) {
      j--;
    }

    if (i < j) {
      master_code_V2_test_B.t_kkiq3xxxve = midxPort[i];
      midxPort[i] = midxPort[j];
      midxPort[j] = master_code_V2_test_B.t_kkiq3xxxve;
      i++;
      j--;
    }

    master_code_V2_test_B.ctidx--;
  }

  idx = i;

  /* End of S-Function (sdspmdn2): '<S4>/Median' */
  return idx;
}

static void master_code_V2_tes_MDNQSort_u16(int32_T i, int32_T j, int32_T
  startIdx, uint32_T midxPort[], const uint16_T mdataPort[])
{
  int32_T pivot;

  /* S-Function (sdspmdn2): '<S4>/Median' */
  if (master_code_V2_findpivotIdx_u16(i, j, startIdx, &pivot, &midxPort[0U],
       &mdataPort[0U])) {
    pivot = master_code_V2_partitionIdx_u16(i, j, startIdx, &pivot, &midxPort[0U],
      &mdataPort[0U]);
    master_code_V2_tes_MDNQSort_u16(i, pivot - 1, startIdx, &midxPort[0U],
      &mdataPort[0U]);
    master_code_V2_tes_MDNQSort_u16(pivot, j, startIdx, &midxPort[0U],
      &mdataPort[0U]);
  }

  /* End of S-Function (sdspmdn2): '<S4>/Median' */
}

static void m_SystemCore_release_byt53hqfpc(const
  codertarget_raspi__nparbhma5g_T *obj)
{
  if ((obj->isInitialized == 1) && obj->isSetupComplete) {
    MW_I2C_Close(obj->MW_I2C_HANDLE);
  }
}

static void ma_SystemCore_delete_byt53hqfpc(const
  codertarget_raspi__nparbhma5g_T *obj)
{
  m_SystemCore_release_byt53hqfpc(obj);
}

static void matlabCodegenHandle__byt53hqfpc(codertarget_raspi__nparbhma5g_T *obj)
{
  if (!obj->matlabCodegenIsDeleted) {
    obj->matlabCodegenIsDeleted = true;
    ma_SystemCore_delete_byt53hqfpc(obj);
  }
}

/* Model step function */
void master_code_V2_test_step(void)
{
  uint8_T status;

  /* DigitalClock: '<Root>/Digital Clock' */
  master_code_V2_test_B.DigitalClock = master_code_V2_test_M->Timing.taskTime0;

  /* MATLAB Function: '<Root>/controls_req' */
  /* MATLAB Function 'controls_req': '<S3>:1' */
  /* '<S3>:1:9' */
  if (master_code_V2_test_B.DigitalClock <= 7.0) {
    /* '<S3>:1:10' */
    /* '<S3>:1:11' */
    master_code_V2_test_B.DigitalClock = 0.0;
  } else {
    /* '<S3>:1:13' */
    master_code_V2_test_B.DigitalClock = sin((master_code_V2_test_B.DigitalClock
      - 7.0) * 0.56548667764616278) * 35.0 * 0.017453292519943295;
  }

  /* DataTypeConversion: '<Root>/Data Type Conversion14' incorporates:
   *  MATLAB Function: '<Root>/controls_req'
   */
  /* '<S3>:1:15' */
  /* '<S3>:1:17' */
  /* '<S3>:1:19' */
  /* '<S3>:1:20' */
  master_code_V2_test_B.DigitalClock = floor(6881.0 -
    (master_code_V2_test_B.DigitalClock + 0.013159) * 4402.0);
  if (rtIsNaN(master_code_V2_test_B.DigitalClock) || rtIsInf
      (master_code_V2_test_B.DigitalClock)) {
    master_code_V2_test_B.DigitalClock = 0.0;
  } else {
    master_code_V2_test_B.DigitalClock = fmod(master_code_V2_test_B.DigitalClock,
      65536.0);
  }

  master_code_V2_test_B.DataTypeConversion14 = (uint16_T)
    (master_code_V2_test_B.DigitalClock < 0.0 ? (int32_T)(uint16_T)-(int16_T)
     (uint16_T)-master_code_V2_test_B.DigitalClock : (int32_T)(uint16_T)
     master_code_V2_test_B.DigitalClock);

  /* End of DataTypeConversion: '<Root>/Data Type Conversion14' */

  /* Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  DataTypeConversion: '<S4>/Data Type Conversion5'
   *  S-Function (sdspmdn2): '<S4>/Median5'
   */
  if (master_code_V2_test_DW.temporalCounter_i1 < 32767U) {
    master_code_V2_test_DW.temporalCounter_i1++;
  }

  /* Gateway: ECU_STATE_MACHINE */
  /* During: ECU_STATE_MACHINE */
  if (master_code_V2_test_DW.is_active_c3_master_code_V2_tes == 0U) {
    /* Entry: ECU_STATE_MACHINE */
    master_code_V2_test_DW.is_active_c3_master_code_V2_tes = 1U;

    /* Entry Internal: ECU_STATE_MACHINE */
    /* Transition: '<S1>:35' */
    master_code_V2_test_DW.is_c3_master_code_V2_test =
      master_code_V_IN_initialization;
    master_code_V2_test_DW.temporalCounter_i1 = 0U;

    /* Entry 'initialization': '<S1>:34' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V2_test_B.dataw = 27392U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V2_test_i2cWr_mpu(master_code_V2_test_B.dataw,
      &master_code_V2_test_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V2_test_B.dataw = 6937U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V2_test_i2cWr_mpu(master_code_V2_test_B.dataw,
      &master_code_V2_test_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V2_test_B.dataw = 7176U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V2_test_i2cWr_mpu(master_code_V2_test_B.dataw,
      &master_code_V2_test_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V2_test_B.dataw = 6662U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V2_test_i2cWr_mpu(master_code_V2_test_B.dataw,
      &master_code_V2_test_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V2_test_B.dataw = 14096U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V2_test_i2cWr_mpu(master_code_V2_test_B.dataw,
      &master_code_V2_test_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
    /* Simulink Function 'i2cWr_mpu': '<S1>:28' */
    master_code_V2_test_B.dataw = 14336U;

    /* Outputs for Function Call SubSystem: '<S1>/i2cWr_mpu' */
    master_code_V2_test_i2cWr_mpu(master_code_V2_test_B.dataw,
      &master_code_V2_test_DW.i2cWr_mpu);

    /* End of Outputs for SubSystem: '<S1>/i2cWr_mpu' */
  } else {
    switch (master_code_V2_test_DW.is_c3_master_code_V2_test) {
     case master_code_V2_te_IN_idle_state:
      /* Outputs for Function Call SubSystem: '<S1>/i2cRd' */
      /* During 'idle_state': '<S1>:92' */
      /* Simulink Function 'i2cRd': '<S1>:56' */
      master_code_V2_test_i2cRd(&master_code_V2_test_B.i2cRd,
        &master_code_V2_test_DW.i2cRd, &master_code_V2_test_P.i2cRd);

      /* End of Outputs for SubSystem: '<S1>/i2cRd' */
      for (master_code_V2_test_B.i = 0; master_code_V2_test_B.i < 7;
           master_code_V2_test_B.i++) {
        master_code_V2_test_B.data_imu_mpu[master_code_V2_test_B.i] =
          master_code_V2_test_B.i2cRd.DataTypeConversion4[master_code_V2_test_B.i];
      }

      master_code_V2_test_B.enc_rear[0] =
        master_code_V2_test_B.i2cRd.DataTypeConversion1[0];
      master_code_V2_test_B.enc_rear[1] =
        master_code_V2_test_B.i2cRd.DataTypeConversion1[1];
      master_code_V2_test_B.enc_rear[2] =
        master_code_V2_test_B.i2cRd.DataTypeConversion1[2];
      master_code_V2_test_B.enc_front[0] =
        master_code_V2_test_B.i2cRd.DataTypeConversion6[0];
      master_code_V2_test_B.enc_front[1] =
        master_code_V2_test_B.i2cRd.DataTypeConversion6[1];

      /* Outputs for Function Call SubSystem: '<S1>/gpsRd' */
      /* Simulink Function 'gpsRd': '<S1>:128' */
      master_code_V2_test_gpsRd(master_code_V2_test_M,
        &master_code_V2_test_B.gpsRd, &master_code_V2_test_DW.gpsRd);

      /* End of Outputs for SubSystem: '<S1>/gpsRd' */
      master_code_V2_test_B.gps[0] = master_code_V2_test_B.gpsRd.UDPReceive_o1[0];
      master_code_V2_test_B.gps[1] = master_code_V2_test_B.gpsRd.UDPReceive_o1[1];
      master_code_V2_test_B.gps[2] = master_code_V2_test_B.gpsRd.UDPReceive_o1[2];

      /* Outputs for Function Call SubSystem: '<S1>/cmd_motor' */
      /* Simulink Function 'cmd_motor': '<S1>:73' */
      master_code_V2_test_cmd_motor(0,
        master_code_V2_test_B.DataTypeConversion14,
        &master_code_V2_test_DW.cmd_motor);

      /* End of Outputs for SubSystem: '<S1>/cmd_motor' */
      master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[0] -
        master_code_V2_test_DW.acc_x_calib;
      if (master_code_V2_test_B.i > 32767) {
        master_code_V2_test_B.i = 32767;
      } else {
        if (master_code_V2_test_B.i < -32768) {
          master_code_V2_test_B.i = -32768;
        }
      }

      master_code_V2_test_B.acc_mpu[0] = (int16_T)master_code_V2_test_B.i;
      master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[1] -
        master_code_V2_test_DW.acc_y_calib;
      if (master_code_V2_test_B.i > 32767) {
        master_code_V2_test_B.i = 32767;
      } else {
        if (master_code_V2_test_B.i < -32768) {
          master_code_V2_test_B.i = -32768;
        }
      }

      master_code_V2_test_B.acc_mpu[1] = (int16_T)master_code_V2_test_B.i;
      master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[2] -
        master_code_V2_test_DW.acc_z_calib;
      if (master_code_V2_test_B.i > 32767) {
        master_code_V2_test_B.i = 32767;
      } else {
        if (master_code_V2_test_B.i < -32768) {
          master_code_V2_test_B.i = -32768;
        }
      }

      master_code_V2_test_B.acc_mpu[2] = (int16_T)master_code_V2_test_B.i;
      master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[4] -
        master_code_V2_test_DW.gyro_x_calib;
      if (master_code_V2_test_B.i > 32767) {
        master_code_V2_test_B.i = 32767;
      } else {
        if (master_code_V2_test_B.i < -32768) {
          master_code_V2_test_B.i = -32768;
        }
      }

      master_code_V2_test_B.gyro_mpu[0] = (int16_T)master_code_V2_test_B.i;
      master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[5] -
        master_code_V2_test_DW.gyro_y_calib;
      if (master_code_V2_test_B.i > 32767) {
        master_code_V2_test_B.i = 32767;
      } else {
        if (master_code_V2_test_B.i < -32768) {
          master_code_V2_test_B.i = -32768;
        }
      }

      master_code_V2_test_B.gyro_mpu[1] = (int16_T)master_code_V2_test_B.i;
      master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[6] -
        master_code_V2_test_DW.gyro_z_calib;
      if (master_code_V2_test_B.i > 32767) {
        master_code_V2_test_B.i = 32767;
      } else {
        if (master_code_V2_test_B.i < -32768) {
          master_code_V2_test_B.i = -32768;
        }
      }

      master_code_V2_test_B.gyro_mpu[2] = (int16_T)master_code_V2_test_B.i;
      master_code_V2_test_B.data_imu_raw[0] =
        master_code_V2_test_B.data_imu_mpu[0];
      master_code_V2_test_B.data_imu_raw[1] =
        master_code_V2_test_B.data_imu_mpu[1];
      master_code_V2_test_B.data_imu_raw[2] =
        master_code_V2_test_B.data_imu_mpu[2];
      master_code_V2_test_B.traction = 5000U;
      master_code_V2_test_B.steering =
        master_code_V2_test_B.DataTypeConversion14;
      break;

     case master_code_V_IN_initialization:
      /* During 'initialization': '<S1>:34' */
      if (master_code_V2_test_DW.temporalCounter_i1 >= 500U) {
        /* Transition: '<S1>:60' */
        master_code_V2_test_DW.is_c3_master_code_V2_test =
          master_code__IN_reading_sensors;
        master_code_V2_test_DW.temporalCounter_i1 = 0U;

        /* Entry 'reading_sensors': '<S1>:52' */
      } else {
        /* Outputs for Function Call SubSystem: '<S1>/calib' */
        /* MATLABSystem: '<S4>/I2C Master Read' */
        /* Simulink Function 'calib': '<S1>:102' */
        if (master_code_V2_test_DW.obj.SampleTime !=
            master_code_V2_test_P.I2CMasterRead_SampleTime) {
          master_code_V2_test_DW.obj.SampleTime =
            master_code_V2_test_P.I2CMasterRead_SampleTime;
        }

        status = 59U;
        status = MW_I2C_MasterWrite(master_code_V2_test_DW.obj.MW_I2C_HANDLE,
          105U, &status, 1U, true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V2_test_DW.obj.MW_I2C_HANDLE, 105U,
                            master_code_V2_test_B.output_raw, 2U, false, true);
          memcpy((void *)&master_code_V2_test_B.DataTypeConversion14, (void *)
                 &master_code_V2_test_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V2_test_B.output_raw[0], (void *)
                 &master_code_V2_test_B.DataTypeConversion14, (uint32_T)((size_t)
                  2 * sizeof(uint8_T)));
          master_code_V2_test_B.b_x[0] = master_code_V2_test_B.output_raw[1];
          master_code_V2_test_B.b_x[1] = master_code_V2_test_B.output_raw[0];
          memcpy((void *)&master_code_V2_test_B.I2CMasterRead, (void *)
                 &master_code_V2_test_B.b_x[0], (uint32_T)((size_t)1 * sizeof
                  (uint16_T)));
        } else {
          master_code_V2_test_B.I2CMasterRead = 0U;
        }

        /* End of MATLABSystem: '<S4>/I2C Master Read' */

        /* S-Function (sdspmdn2): '<S4>/Median' */
        master_code_V2_tes_MDNQSort_u16(0, 0, 0,
          &master_code_V2_test_DW.Median_Index,
          &master_code_V2_test_B.I2CMasterRead);

        /* DataTypeConversion: '<S4>/Data Type Conversion' incorporates:
         *  S-Function (sdspmdn2): '<S4>/Median'
         */
        master_code_V2_test_DW.acc_x_calib = (int16_T)
          master_code_V2_test_B.I2CMasterRead;

        /* MATLABSystem: '<S4>/I2C Master Read1' */
        if (master_code_V2_test_DW.obj_iwos115rnw.SampleTime !=
            master_code_V2_test_P.I2CMasterRead1_SampleTime) {
          master_code_V2_test_DW.obj_iwos115rnw.SampleTime =
            master_code_V2_test_P.I2CMasterRead1_SampleTime;
        }

        status = 61U;
        status = MW_I2C_MasterWrite
          (master_code_V2_test_DW.obj_iwos115rnw.MW_I2C_HANDLE, 105U, &status,
           1U, true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V2_test_DW.obj_iwos115rnw.MW_I2C_HANDLE,
                            105U, master_code_V2_test_B.output_raw, 2U, false,
                            true);
          memcpy((void *)&master_code_V2_test_B.DataTypeConversion14, (void *)
                 &master_code_V2_test_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V2_test_B.output_raw[0], (void *)
                 &master_code_V2_test_B.DataTypeConversion14, (uint32_T)((size_t)
                  2 * sizeof(uint8_T)));
          master_code_V2_test_B.b_x[0] = master_code_V2_test_B.output_raw[1];
          master_code_V2_test_B.b_x[1] = master_code_V2_test_B.output_raw[0];
          memcpy((void *)&master_code_V2_test_B.I2CMasterRead1, (void *)
                 &master_code_V2_test_B.b_x[0], (uint32_T)((size_t)1 * sizeof
                  (uint16_T)));
        } else {
          master_code_V2_test_B.I2CMasterRead1 = 0U;
        }

        /* End of MATLABSystem: '<S4>/I2C Master Read1' */

        /* S-Function (sdspmdn2): '<S4>/Median1' */
        master_code_V2_tes_MDNQSort_u16(0, 0, 0,
          &master_code_V2_test_DW.Median1_Index,
          &master_code_V2_test_B.I2CMasterRead1);

        /* DataTypeConversion: '<S4>/Data Type Conversion1' incorporates:
         *  S-Function (sdspmdn2): '<S4>/Median1'
         */
        master_code_V2_test_DW.acc_y_calib = (int16_T)
          master_code_V2_test_B.I2CMasterRead1;

        /* MATLABSystem: '<S4>/I2C Master Read2' */
        if (master_code_V2_test_DW.obj_i4zcoyvvmy.SampleTime !=
            master_code_V2_test_P.I2CMasterRead2_SampleTime) {
          master_code_V2_test_DW.obj_i4zcoyvvmy.SampleTime =
            master_code_V2_test_P.I2CMasterRead2_SampleTime;
        }

        status = 63U;
        status = MW_I2C_MasterWrite
          (master_code_V2_test_DW.obj_i4zcoyvvmy.MW_I2C_HANDLE, 105U, &status,
           1U, true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V2_test_DW.obj_i4zcoyvvmy.MW_I2C_HANDLE,
                            105U, master_code_V2_test_B.output_raw, 2U, false,
                            true);
          memcpy((void *)&master_code_V2_test_B.DataTypeConversion14, (void *)
                 &master_code_V2_test_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V2_test_B.output_raw[0], (void *)
                 &master_code_V2_test_B.DataTypeConversion14, (uint32_T)((size_t)
                  2 * sizeof(uint8_T)));
          master_code_V2_test_B.b_x[0] = master_code_V2_test_B.output_raw[1];
          master_code_V2_test_B.b_x[1] = master_code_V2_test_B.output_raw[0];
          memcpy((void *)&master_code_V2_test_B.I2CMasterRead2, (void *)
                 &master_code_V2_test_B.b_x[0], (uint32_T)((size_t)1 * sizeof
                  (uint16_T)));
        } else {
          master_code_V2_test_B.I2CMasterRead2 = 0U;
        }

        /* End of MATLABSystem: '<S4>/I2C Master Read2' */

        /* S-Function (sdspmdn2): '<S4>/Median2' */
        master_code_V2_tes_MDNQSort_u16(0, 0, 0,
          &master_code_V2_test_DW.Median2_Index,
          &master_code_V2_test_B.I2CMasterRead2);

        /* DataTypeConversion: '<S4>/Data Type Conversion2' incorporates:
         *  S-Function (sdspmdn2): '<S4>/Median2'
         */
        master_code_V2_test_DW.acc_z_calib = (int16_T)
          master_code_V2_test_B.I2CMasterRead2;

        /* MATLABSystem: '<S4>/I2C Master Read3' */
        if (master_code_V2_test_DW.obj_gwzo2fxivo.SampleTime !=
            master_code_V2_test_P.I2CMasterRead3_SampleTime) {
          master_code_V2_test_DW.obj_gwzo2fxivo.SampleTime =
            master_code_V2_test_P.I2CMasterRead3_SampleTime;
        }

        status = 67U;
        status = MW_I2C_MasterWrite
          (master_code_V2_test_DW.obj_gwzo2fxivo.MW_I2C_HANDLE, 105U, &status,
           1U, true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V2_test_DW.obj_gwzo2fxivo.MW_I2C_HANDLE,
                            105U, master_code_V2_test_B.output_raw, 2U, false,
                            true);
          memcpy((void *)&master_code_V2_test_B.DataTypeConversion14, (void *)
                 &master_code_V2_test_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V2_test_B.output_raw[0], (void *)
                 &master_code_V2_test_B.DataTypeConversion14, (uint32_T)((size_t)
                  2 * sizeof(uint8_T)));
          master_code_V2_test_B.b_x[0] = master_code_V2_test_B.output_raw[1];
          master_code_V2_test_B.b_x[1] = master_code_V2_test_B.output_raw[0];
          memcpy((void *)&master_code_V2_test_B.I2CMasterRead3, (void *)
                 &master_code_V2_test_B.b_x[0], (uint32_T)((size_t)1 * sizeof
                  (uint16_T)));
        } else {
          master_code_V2_test_B.I2CMasterRead3 = 0U;
        }

        /* End of MATLABSystem: '<S4>/I2C Master Read3' */

        /* S-Function (sdspmdn2): '<S4>/Median3' */
        master_code_V2_tes_MDNQSort_u16(0, 0, 0,
          &master_code_V2_test_DW.Median3_Index,
          &master_code_V2_test_B.I2CMasterRead3);

        /* DataTypeConversion: '<S4>/Data Type Conversion3' incorporates:
         *  S-Function (sdspmdn2): '<S4>/Median3'
         */
        master_code_V2_test_DW.gyro_x_calib = (int16_T)
          master_code_V2_test_B.I2CMasterRead3;

        /* MATLABSystem: '<S4>/I2C Master Read4' */
        if (master_code_V2_test_DW.obj_daui2m5pff.SampleTime !=
            master_code_V2_test_P.I2CMasterRead4_SampleTime) {
          master_code_V2_test_DW.obj_daui2m5pff.SampleTime =
            master_code_V2_test_P.I2CMasterRead4_SampleTime;
        }

        status = 69U;
        status = MW_I2C_MasterWrite
          (master_code_V2_test_DW.obj_daui2m5pff.MW_I2C_HANDLE, 105U, &status,
           1U, true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V2_test_DW.obj_daui2m5pff.MW_I2C_HANDLE,
                            105U, master_code_V2_test_B.output_raw, 2U, false,
                            true);
          memcpy((void *)&master_code_V2_test_B.DataTypeConversion14, (void *)
                 &master_code_V2_test_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V2_test_B.output_raw[0], (void *)
                 &master_code_V2_test_B.DataTypeConversion14, (uint32_T)((size_t)
                  2 * sizeof(uint8_T)));
          master_code_V2_test_B.b_x[0] = master_code_V2_test_B.output_raw[1];
          master_code_V2_test_B.b_x[1] = master_code_V2_test_B.output_raw[0];
          memcpy((void *)&master_code_V2_test_B.I2CMasterRead4, (void *)
                 &master_code_V2_test_B.b_x[0], (uint32_T)((size_t)1 * sizeof
                  (uint16_T)));
        } else {
          master_code_V2_test_B.I2CMasterRead4 = 0U;
        }

        /* End of MATLABSystem: '<S4>/I2C Master Read4' */

        /* S-Function (sdspmdn2): '<S4>/Median4' */
        master_code_V2_tes_MDNQSort_u16(0, 0, 0,
          &master_code_V2_test_DW.Median4_Index,
          &master_code_V2_test_B.I2CMasterRead4);

        /* DataTypeConversion: '<S4>/Data Type Conversion4' incorporates:
         *  S-Function (sdspmdn2): '<S4>/Median4'
         */
        master_code_V2_test_DW.gyro_y_calib = (int16_T)
          master_code_V2_test_B.I2CMasterRead4;

        /* MATLABSystem: '<S4>/I2C Master Read5' */
        if (master_code_V2_test_DW.obj_dp4cdr5kdf.SampleTime !=
            master_code_V2_test_P.I2CMasterRead5_SampleTime) {
          master_code_V2_test_DW.obj_dp4cdr5kdf.SampleTime =
            master_code_V2_test_P.I2CMasterRead5_SampleTime;
        }

        status = 71U;
        status = MW_I2C_MasterWrite
          (master_code_V2_test_DW.obj_dp4cdr5kdf.MW_I2C_HANDLE, 105U, &status,
           1U, true, false);
        if (0 == status) {
          MW_I2C_MasterRead(master_code_V2_test_DW.obj_dp4cdr5kdf.MW_I2C_HANDLE,
                            105U, master_code_V2_test_B.output_raw, 2U, false,
                            true);
          memcpy((void *)&master_code_V2_test_B.DataTypeConversion14, (void *)
                 &master_code_V2_test_B.output_raw[0], (uint32_T)((size_t)1 *
                  sizeof(uint16_T)));
          memcpy((void *)&master_code_V2_test_B.output_raw[0], (void *)
                 &master_code_V2_test_B.DataTypeConversion14, (uint32_T)((size_t)
                  2 * sizeof(uint8_T)));
          master_code_V2_test_B.b_x[0] = master_code_V2_test_B.output_raw[1];
          master_code_V2_test_B.b_x[1] = master_code_V2_test_B.output_raw[0];
          memcpy((void *)&master_code_V2_test_B.I2CMasterRead5, (void *)
                 &master_code_V2_test_B.b_x[0], (uint32_T)((size_t)1 * sizeof
                  (uint16_T)));
        } else {
          master_code_V2_test_B.I2CMasterRead5 = 0U;
        }

        /* End of MATLABSystem: '<S4>/I2C Master Read5' */

        /* S-Function (sdspmdn2): '<S4>/Median5' */
        master_code_V2_tes_MDNQSort_u16(0, 0, 0,
          &master_code_V2_test_DW.Median5_Index,
          &master_code_V2_test_B.I2CMasterRead5);
        master_code_V2_test_DW.gyro_z_calib = (int16_T)
          master_code_V2_test_B.I2CMasterRead5;

        /* End of Outputs for SubSystem: '<S1>/calib' */

        /* Outputs for Function Call SubSystem: '<S1>/cmd_motor' */
        /* Simulink Function 'cmd_motor': '<S1>:73' */
        master_code_V2_test_cmd_motor(0, master_code_V_DUTY_SERVO_MIDDLE,
          &master_code_V2_test_DW.cmd_motor);

        /* End of Outputs for SubSystem: '<S1>/cmd_motor' */
        master_code_V2_test_B.steering = master_code_V_DUTY_SERVO_MIDDLE;
        master_code_V2_test_B.traction = 5000U;
      }
      break;

     default:
      /* During 'reading_sensors': '<S1>:52' */
      if (master_code_V2_test_DW.temporalCounter_i1 >= 17000U) {
        /* Transition: '<S1>:93' */
        master_code_V2_test_DW.is_c3_master_code_V2_test =
          master_code_V2_te_IN_idle_state;

        /* Entry 'idle_state': '<S1>:92' */
      } else {
        /* Outputs for Function Call SubSystem: '<S1>/i2cRd' */
        /* Simulink Function 'i2cRd': '<S1>:56' */
        master_code_V2_test_i2cRd(&master_code_V2_test_B.i2cRd,
          &master_code_V2_test_DW.i2cRd, &master_code_V2_test_P.i2cRd);

        /* End of Outputs for SubSystem: '<S1>/i2cRd' */
        for (master_code_V2_test_B.i = 0; master_code_V2_test_B.i < 7;
             master_code_V2_test_B.i++) {
          master_code_V2_test_B.data_imu_mpu[master_code_V2_test_B.i] =
            master_code_V2_test_B.i2cRd.DataTypeConversion4[master_code_V2_test_B.i];
        }

        master_code_V2_test_B.enc_rear[0] =
          master_code_V2_test_B.i2cRd.DataTypeConversion1[0];
        master_code_V2_test_B.enc_rear[1] =
          master_code_V2_test_B.i2cRd.DataTypeConversion1[1];
        master_code_V2_test_B.enc_rear[2] =
          master_code_V2_test_B.i2cRd.DataTypeConversion1[2];
        master_code_V2_test_B.enc_front[0] =
          master_code_V2_test_B.i2cRd.DataTypeConversion6[0];
        master_code_V2_test_B.enc_front[1] =
          master_code_V2_test_B.i2cRd.DataTypeConversion6[1];

        /* Outputs for Function Call SubSystem: '<S1>/gpsRd' */
        /* Simulink Function 'gpsRd': '<S1>:128' */
        master_code_V2_test_gpsRd(master_code_V2_test_M,
          &master_code_V2_test_B.gpsRd, &master_code_V2_test_DW.gpsRd);

        /* End of Outputs for SubSystem: '<S1>/gpsRd' */
        master_code_V2_test_B.gps[0] =
          master_code_V2_test_B.gpsRd.UDPReceive_o1[0];
        master_code_V2_test_B.gps[1] =
          master_code_V2_test_B.gpsRd.UDPReceive_o1[1];
        master_code_V2_test_B.gps[2] =
          master_code_V2_test_B.gpsRd.UDPReceive_o1[2];

        /* Outputs for Function Call SubSystem: '<S1>/cmd_motor' */
        /* Simulink Function 'cmd_motor': '<S1>:73' */
        master_code_V2_test_cmd_motor(5000,
          master_code_V2_test_B.DataTypeConversion14,
          &master_code_V2_test_DW.cmd_motor);

        /* End of Outputs for SubSystem: '<S1>/cmd_motor' */
        master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[0] -
          master_code_V2_test_DW.acc_x_calib;
        if (master_code_V2_test_B.i > 32767) {
          master_code_V2_test_B.i = 32767;
        } else {
          if (master_code_V2_test_B.i < -32768) {
            master_code_V2_test_B.i = -32768;
          }
        }

        master_code_V2_test_B.acc_mpu[0] = (int16_T)master_code_V2_test_B.i;
        master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[1] -
          master_code_V2_test_DW.acc_y_calib;
        if (master_code_V2_test_B.i > 32767) {
          master_code_V2_test_B.i = 32767;
        } else {
          if (master_code_V2_test_B.i < -32768) {
            master_code_V2_test_B.i = -32768;
          }
        }

        master_code_V2_test_B.acc_mpu[1] = (int16_T)master_code_V2_test_B.i;
        master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[2] -
          master_code_V2_test_DW.acc_z_calib;
        if (master_code_V2_test_B.i > 32767) {
          master_code_V2_test_B.i = 32767;
        } else {
          if (master_code_V2_test_B.i < -32768) {
            master_code_V2_test_B.i = -32768;
          }
        }

        master_code_V2_test_B.acc_mpu[2] = (int16_T)master_code_V2_test_B.i;
        master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[4] -
          master_code_V2_test_DW.gyro_x_calib;
        if (master_code_V2_test_B.i > 32767) {
          master_code_V2_test_B.i = 32767;
        } else {
          if (master_code_V2_test_B.i < -32768) {
            master_code_V2_test_B.i = -32768;
          }
        }

        master_code_V2_test_B.gyro_mpu[0] = (int16_T)master_code_V2_test_B.i;
        master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[5] -
          master_code_V2_test_DW.gyro_y_calib;
        if (master_code_V2_test_B.i > 32767) {
          master_code_V2_test_B.i = 32767;
        } else {
          if (master_code_V2_test_B.i < -32768) {
            master_code_V2_test_B.i = -32768;
          }
        }

        master_code_V2_test_B.gyro_mpu[1] = (int16_T)master_code_V2_test_B.i;
        master_code_V2_test_B.i = master_code_V2_test_B.data_imu_mpu[6] -
          master_code_V2_test_DW.gyro_z_calib;
        if (master_code_V2_test_B.i > 32767) {
          master_code_V2_test_B.i = 32767;
        } else {
          if (master_code_V2_test_B.i < -32768) {
            master_code_V2_test_B.i = -32768;
          }
        }

        master_code_V2_test_B.gyro_mpu[2] = (int16_T)master_code_V2_test_B.i;
        master_code_V2_test_B.data_imu_raw[0] =
          master_code_V2_test_B.data_imu_mpu[0];
        master_code_V2_test_B.data_imu_raw[1] =
          master_code_V2_test_B.data_imu_mpu[1];
        master_code_V2_test_B.data_imu_raw[2] =
          master_code_V2_test_B.data_imu_mpu[2];
        master_code_V2_test_B.traction = 5000U;
        master_code_V2_test_B.steering =
          master_code_V2_test_B.DataTypeConversion14;
      }
      break;
    }
  }

  /* End of Chart: '<Root>/ECU_STATE_MACHINE' */

  /* Gain: '<Root>/Gain3' */
  master_code_V2_test_B.rtb_DataTypeConversion2_tmp = (real32_T)
    master_code_V2_test_P.Gain3_Gain * 7.4505806E-9F;

  /* DataTypeConversion: '<Root>/Data Type Conversion2' incorporates:
   *  Gain: '<Root>/Gain3'
   */
  master_code_V2_test_B.DataTypeConversion2[0] =
    master_code_V2_test_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V2_test_B.acc_mpu[0];

  /* Gain: '<Root>/Gain4' */
  master_code_V2_test_B.rtb_DataTypeConversi_mbvzarwird = (real32_T)
    master_code_V2_test_P.Gain4_Gain * 5.96046448E-8F;

  /* DataTypeConversion: '<Root>/Data Type Conversion2' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion1'
   *  DataTypeConversion: '<Root>/Data Type Conversion4'
   *  DataTypeConversion: '<Root>/Data Type Conversion5'
   *  DataTypeConversion: '<Root>/Data Type Conversion6'
   *  Gain: '<Root>/Gain3'
   *  Gain: '<Root>/Gain4'
   */
  master_code_V2_test_B.DataTypeConversion2[3] =
    master_code_V2_test_B.rtb_DataTypeConversi_mbvzarwird * (real32_T)
    master_code_V2_test_B.gyro_mpu[0];
  master_code_V2_test_B.DataTypeConversion2[6] = master_code_V2_test_B.enc_rear
    [0];
  master_code_V2_test_B.DataTypeConversion2[1] =
    master_code_V2_test_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V2_test_B.acc_mpu[1];
  master_code_V2_test_B.DataTypeConversion2[4] =
    master_code_V2_test_B.rtb_DataTypeConversi_mbvzarwird * (real32_T)
    master_code_V2_test_B.gyro_mpu[1];
  master_code_V2_test_B.DataTypeConversion2[7] = master_code_V2_test_B.enc_rear
    [1];
  master_code_V2_test_B.DataTypeConversion2[2] =
    master_code_V2_test_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V2_test_B.acc_mpu[2];
  master_code_V2_test_B.DataTypeConversion2[5] =
    master_code_V2_test_B.rtb_DataTypeConversi_mbvzarwird * (real32_T)
    master_code_V2_test_B.gyro_mpu[2];
  master_code_V2_test_B.DataTypeConversion2[8] = master_code_V2_test_B.enc_rear
    [2];
  master_code_V2_test_B.DataTypeConversion2[9] =
    master_code_V2_test_B.enc_front[0];
  master_code_V2_test_B.DataTypeConversion2[10] =
    master_code_V2_test_B.enc_front[1];
  master_code_V2_test_B.DataTypeConversion2[14] = master_code_V2_test_B.traction;
  master_code_V2_test_B.DataTypeConversion2[15] = master_code_V2_test_B.steering;

  /* Gain: '<Root>/Gain1' */
  master_code_V2_test_B.rtb_DataTypeConversion2_tmp = (real32_T)
    master_code_V2_test_P.Gain1_Gain * 7.4505806E-9F;

  /* DataTypeConversion: '<Root>/Data Type Conversion2' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion3'
   *  Gain: '<Root>/Gain1'
   */
  master_code_V2_test_B.DataTypeConversion2[11] =
    master_code_V2_test_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V2_test_B.data_imu_raw[0];
  master_code_V2_test_B.DataTypeConversion2[16] = master_code_V2_test_B.gps[0];
  master_code_V2_test_B.DataTypeConversion2[12] =
    master_code_V2_test_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V2_test_B.data_imu_raw[1];
  master_code_V2_test_B.DataTypeConversion2[17] = master_code_V2_test_B.gps[1];
  master_code_V2_test_B.DataTypeConversion2[13] =
    master_code_V2_test_B.rtb_DataTypeConversion2_tmp * (real32_T)
    master_code_V2_test_B.data_imu_raw[2];
  master_code_V2_test_B.DataTypeConversion2[18] = master_code_V2_test_B.gps[2];

  /* ToFile: '<Root>/To File' */
  {
    if (!(++master_code_V2_test_DW.ToFile_IWORK.Decimation % 1) &&
        (master_code_V2_test_DW.ToFile_IWORK.Count * (19 + 1)) + 1 < 100000000 )
    {
      FILE *fp = (FILE *) master_code_V2_test_DW.ToFile_PWORK.FilePtr;
      if (fp != (NULL)) {
        real_T u[19 + 1];
        master_code_V2_test_DW.ToFile_IWORK.Decimation = 0;
        u[0] = master_code_V2_test_M->Timing.taskTime0;
        u[1] = master_code_V2_test_B.DataTypeConversion2[0];
        u[2] = master_code_V2_test_B.DataTypeConversion2[1];
        u[3] = master_code_V2_test_B.DataTypeConversion2[2];
        u[4] = master_code_V2_test_B.DataTypeConversion2[3];
        u[5] = master_code_V2_test_B.DataTypeConversion2[4];
        u[6] = master_code_V2_test_B.DataTypeConversion2[5];
        u[7] = master_code_V2_test_B.DataTypeConversion2[6];
        u[8] = master_code_V2_test_B.DataTypeConversion2[7];
        u[9] = master_code_V2_test_B.DataTypeConversion2[8];
        u[10] = master_code_V2_test_B.DataTypeConversion2[9];
        u[11] = master_code_V2_test_B.DataTypeConversion2[10];
        u[12] = master_code_V2_test_B.DataTypeConversion2[11];
        u[13] = master_code_V2_test_B.DataTypeConversion2[12];
        u[14] = master_code_V2_test_B.DataTypeConversion2[13];
        u[15] = master_code_V2_test_B.DataTypeConversion2[14];
        u[16] = master_code_V2_test_B.DataTypeConversion2[15];
        u[17] = master_code_V2_test_B.DataTypeConversion2[16];
        u[18] = master_code_V2_test_B.DataTypeConversion2[17];
        u[19] = master_code_V2_test_B.DataTypeConversion2[18];
        if (fwrite(u, sizeof(real_T), 19 + 1, fp) != 19 + 1) {
          rtmSetErrorStatus(master_code_V2_test_M,
                            "Error writing to MAT-file sensors.mat");
          return;
        }

        if (((++master_code_V2_test_DW.ToFile_IWORK.Count) * (19 + 1))+1 >=
            100000000) {
          (void)fprintf(stdout,
                        "*** The ToFile block will stop logging data before\n"
                        "    the simulation has ended, because it has reached\n"
                        "    the maximum number of elements (100000000)\n"
                        "    allowed in MAT-file sensors.mat.\n");
        }
      }
    }
  }

  /* Matfile logging */
  rt_UpdateTXYLogVars(master_code_V2_test_M->rtwLogInfo,
                      (&master_code_V2_test_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.01s, 0.0s] */
    if ((rtmGetTFinal(master_code_V2_test_M)!=-1) &&
        !((rtmGetTFinal(master_code_V2_test_M)-
           master_code_V2_test_M->Timing.taskTime0) >
          master_code_V2_test_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(master_code_V2_test_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  master_code_V2_test_M->Timing.taskTime0 =
    (++master_code_V2_test_M->Timing.clockTick0) *
    master_code_V2_test_M->Timing.stepSize0;
}

/* Model initialize function */
void master_code_V2_test_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)master_code_V2_test_M, 0,
                sizeof(RT_MODEL_master_code_V2_test_T));
  rtmSetTFinal(master_code_V2_test_M, 180.0);
  master_code_V2_test_M->Timing.stepSize0 = 0.01;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    master_code_V2_test_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(master_code_V2_test_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(master_code_V2_test_M->rtwLogInfo, (NULL));
    rtliSetLogT(master_code_V2_test_M->rtwLogInfo, "");
    rtliSetLogX(master_code_V2_test_M->rtwLogInfo, "");
    rtliSetLogXFinal(master_code_V2_test_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(master_code_V2_test_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(master_code_V2_test_M->rtwLogInfo, 4);
    rtliSetLogMaxRows(master_code_V2_test_M->rtwLogInfo, 0);
    rtliSetLogDecimation(master_code_V2_test_M->rtwLogInfo, 1);
    rtliSetLogY(master_code_V2_test_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(master_code_V2_test_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(master_code_V2_test_M->rtwLogInfo, (NULL));
  }

  /* block I/O */
  (void) memset(((void *) &master_code_V2_test_B), 0,
                sizeof(B_master_code_V2_test_T));

  /* states (dwork) */
  (void) memset((void *)&master_code_V2_test_DW, 0,
                sizeof(DW_master_code_V2_test_T));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(master_code_V2_test_M->rtwLogInfo, 0.0,
    rtmGetTFinal(master_code_V2_test_M), master_code_V2_test_M->Timing.stepSize0,
    (&rtmGetErrorStatus(master_code_V2_test_M)));

  {
    codertarget_raspi__nparbhma5g_T *obj;
    uint32_T i2cname;

    /* Start for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/i2cWr_mpu'
     */
    master_code_V2__i2cWr_mpu_Start(&master_code_V2_test_DW.i2cWr_mpu);

    /* Start for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/calib'
     */
    /* Start for MATLABSystem: '<S4>/I2C Master Read' */
    master_code_V2_test_DW.obj.matlabCodegenIsDeleted = true;
    master_code_V2_test_DW.obj.isInitialized = 0;
    master_code_V2_test_DW.obj.matlabCodegenIsDeleted = false;
    master_code_V2_test_DW.obj.SampleTime =
      master_code_V2_test_P.I2CMasterRead_SampleTime;
    obj = &master_code_V2_test_DW.obj;
    master_code_V2_test_DW.obj.isSetupComplete = false;
    master_code_V2_test_DW.obj.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V2_test_DW.obj.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V2_test_DW.obj.MW_I2C_HANDLE,
                       master_code_V2_test_DW.obj.BusSpeed);
    master_code_V2_test_DW.obj.isSetupComplete = true;

    /* Start for MATLABSystem: '<S4>/I2C Master Read1' */
    master_code_V2_test_DW.obj_iwos115rnw.matlabCodegenIsDeleted = true;
    master_code_V2_test_DW.obj_iwos115rnw.isInitialized = 0;
    master_code_V2_test_DW.obj_iwos115rnw.matlabCodegenIsDeleted = false;
    master_code_V2_test_DW.obj_iwos115rnw.SampleTime =
      master_code_V2_test_P.I2CMasterRead1_SampleTime;
    obj = &master_code_V2_test_DW.obj_iwos115rnw;
    master_code_V2_test_DW.obj_iwos115rnw.isSetupComplete = false;
    master_code_V2_test_DW.obj_iwos115rnw.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V2_test_DW.obj_iwos115rnw.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V2_test_DW.obj_iwos115rnw.MW_I2C_HANDLE,
                       master_code_V2_test_DW.obj_iwos115rnw.BusSpeed);
    master_code_V2_test_DW.obj_iwos115rnw.isSetupComplete = true;

    /* Start for MATLABSystem: '<S4>/I2C Master Read2' */
    master_code_V2_test_DW.obj_i4zcoyvvmy.matlabCodegenIsDeleted = true;
    master_code_V2_test_DW.obj_i4zcoyvvmy.isInitialized = 0;
    master_code_V2_test_DW.obj_i4zcoyvvmy.matlabCodegenIsDeleted = false;
    master_code_V2_test_DW.obj_i4zcoyvvmy.SampleTime =
      master_code_V2_test_P.I2CMasterRead2_SampleTime;
    obj = &master_code_V2_test_DW.obj_i4zcoyvvmy;
    master_code_V2_test_DW.obj_i4zcoyvvmy.isSetupComplete = false;
    master_code_V2_test_DW.obj_i4zcoyvvmy.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V2_test_DW.obj_i4zcoyvvmy.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V2_test_DW.obj_i4zcoyvvmy.MW_I2C_HANDLE,
                       master_code_V2_test_DW.obj_i4zcoyvvmy.BusSpeed);
    master_code_V2_test_DW.obj_i4zcoyvvmy.isSetupComplete = true;

    /* Start for MATLABSystem: '<S4>/I2C Master Read3' */
    master_code_V2_test_DW.obj_gwzo2fxivo.matlabCodegenIsDeleted = true;
    master_code_V2_test_DW.obj_gwzo2fxivo.isInitialized = 0;
    master_code_V2_test_DW.obj_gwzo2fxivo.matlabCodegenIsDeleted = false;
    master_code_V2_test_DW.obj_gwzo2fxivo.SampleTime =
      master_code_V2_test_P.I2CMasterRead3_SampleTime;
    obj = &master_code_V2_test_DW.obj_gwzo2fxivo;
    master_code_V2_test_DW.obj_gwzo2fxivo.isSetupComplete = false;
    master_code_V2_test_DW.obj_gwzo2fxivo.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V2_test_DW.obj_gwzo2fxivo.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V2_test_DW.obj_gwzo2fxivo.MW_I2C_HANDLE,
                       master_code_V2_test_DW.obj_gwzo2fxivo.BusSpeed);
    master_code_V2_test_DW.obj_gwzo2fxivo.isSetupComplete = true;

    /* Start for MATLABSystem: '<S4>/I2C Master Read4' */
    master_code_V2_test_DW.obj_daui2m5pff.matlabCodegenIsDeleted = true;
    master_code_V2_test_DW.obj_daui2m5pff.isInitialized = 0;
    master_code_V2_test_DW.obj_daui2m5pff.matlabCodegenIsDeleted = false;
    master_code_V2_test_DW.obj_daui2m5pff.SampleTime =
      master_code_V2_test_P.I2CMasterRead4_SampleTime;
    obj = &master_code_V2_test_DW.obj_daui2m5pff;
    master_code_V2_test_DW.obj_daui2m5pff.isSetupComplete = false;
    master_code_V2_test_DW.obj_daui2m5pff.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V2_test_DW.obj_daui2m5pff.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V2_test_DW.obj_daui2m5pff.MW_I2C_HANDLE,
                       master_code_V2_test_DW.obj_daui2m5pff.BusSpeed);
    master_code_V2_test_DW.obj_daui2m5pff.isSetupComplete = true;

    /* Start for MATLABSystem: '<S4>/I2C Master Read5' */
    master_code_V2_test_DW.obj_dp4cdr5kdf.matlabCodegenIsDeleted = true;
    master_code_V2_test_DW.obj_dp4cdr5kdf.isInitialized = 0;
    master_code_V2_test_DW.obj_dp4cdr5kdf.matlabCodegenIsDeleted = false;
    master_code_V2_test_DW.obj_dp4cdr5kdf.SampleTime =
      master_code_V2_test_P.I2CMasterRead5_SampleTime;
    obj = &master_code_V2_test_DW.obj_dp4cdr5kdf;
    master_code_V2_test_DW.obj_dp4cdr5kdf.isSetupComplete = false;
    master_code_V2_test_DW.obj_dp4cdr5kdf.isInitialized = 1;
    i2cname = 1;
    obj->MW_I2C_HANDLE = MW_I2C_Open(i2cname, 0);
    master_code_V2_test_DW.obj_dp4cdr5kdf.BusSpeed = 100000U;
    MW_I2C_SetBusSpeed(master_code_V2_test_DW.obj_dp4cdr5kdf.MW_I2C_HANDLE,
                       master_code_V2_test_DW.obj_dp4cdr5kdf.BusSpeed);
    master_code_V2_test_DW.obj_dp4cdr5kdf.isSetupComplete = true;

    /* Start for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/i2cRd'
     */
    master_code_V2_test_i2cRd_Start(&master_code_V2_test_DW.i2cRd,
      &master_code_V2_test_P.i2cRd);

    /* Start for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/gpsRd'
     */
    master_code_V2_test_gpsRd_Start(master_code_V2_test_M,
      &master_code_V2_test_DW.gpsRd, &master_code_V2_test_P.gpsRd);

    /* Start for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/cmd_motor'
     */
    master_code_V2__cmd_motor_Start(&master_code_V2_test_DW.cmd_motor);

    /* Start for ToFile: '<Root>/To File' */
    {
      FILE *fp = (NULL);
      char fileName[509] = "sensors.mat";
      if ((fp = fopen(fileName, "wb")) == (NULL)) {
        rtmSetErrorStatus(master_code_V2_test_M,
                          "Error creating .mat file sensors.mat");
        return;
      }

      if (rt_WriteMat4FileHeader(fp, 19 + 1, 0, "sensor")) {
        rtmSetErrorStatus(master_code_V2_test_M,
                          "Error writing mat file header to file sensors.mat");
        return;
      }

      master_code_V2_test_DW.ToFile_IWORK.Count = 0;
      master_code_V2_test_DW.ToFile_IWORK.Decimation = -1;
      master_code_V2_test_DW.ToFile_PWORK.FilePtr = fp;
    }

    master_code_V2_test_DW.temporalCounter_i1 = 0U;
    master_code_V2_test_DW.is_active_c3_master_code_V2_tes = 0U;
    master_code_V2_test_DW.is_c3_master_code_V2_test =
      master_code__IN_NO_ACTIVE_CHILD;

    /* SystemInitialize for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/calib'
     */
    /* InitializeConditions for S-Function (sdspmdn2): '<S4>/Median' */
    master_code_V2_test_DW.Median_Index = 0U;

    /* InitializeConditions for S-Function (sdspmdn2): '<S4>/Median1' */
    master_code_V2_test_DW.Median1_Index = 0U;

    /* InitializeConditions for S-Function (sdspmdn2): '<S4>/Median2' */
    master_code_V2_test_DW.Median2_Index = 0U;

    /* InitializeConditions for S-Function (sdspmdn2): '<S4>/Median3' */
    master_code_V2_test_DW.Median3_Index = 0U;

    /* InitializeConditions for S-Function (sdspmdn2): '<S4>/Median4' */
    master_code_V2_test_DW.Median4_Index = 0U;

    /* InitializeConditions for S-Function (sdspmdn2): '<S4>/Median5' */
    master_code_V2_test_DW.Median5_Index = 0U;

    /* SystemInitialize for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/i2cRd'
     */
    master_code_V2_test_i2cRd_Init(&master_code_V2_test_B.i2cRd,
      &master_code_V2_test_P.i2cRd);

    /* SystemInitialize for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
     *  SubSystem: '<S1>/gpsRd'
     */
    master_code_V2_test_gpsRd_Init(&master_code_V2_test_B.gpsRd,
      &master_code_V2_test_P.gpsRd);
  }
}

/* Model terminate function */
void master_code_V2_test_terminate(void)
{
  /* Terminate for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  SubSystem: '<S1>/i2cWr_mpu'
   */
  master_code_V2_t_i2cWr_mpu_Term(&master_code_V2_test_DW.i2cWr_mpu);

  /* Terminate for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  SubSystem: '<S1>/calib'
   */
  /* Terminate for MATLABSystem: '<S4>/I2C Master Read' */
  matlabCodegenHandle__byt53hqfpc(&master_code_V2_test_DW.obj);

  /* Terminate for MATLABSystem: '<S4>/I2C Master Read1' */
  matlabCodegenHandle__byt53hqfpc(&master_code_V2_test_DW.obj_iwos115rnw);

  /* Terminate for MATLABSystem: '<S4>/I2C Master Read2' */
  matlabCodegenHandle__byt53hqfpc(&master_code_V2_test_DW.obj_i4zcoyvvmy);

  /* Terminate for MATLABSystem: '<S4>/I2C Master Read3' */
  matlabCodegenHandle__byt53hqfpc(&master_code_V2_test_DW.obj_gwzo2fxivo);

  /* Terminate for MATLABSystem: '<S4>/I2C Master Read4' */
  matlabCodegenHandle__byt53hqfpc(&master_code_V2_test_DW.obj_daui2m5pff);

  /* Terminate for MATLABSystem: '<S4>/I2C Master Read5' */
  matlabCodegenHandle__byt53hqfpc(&master_code_V2_test_DW.obj_dp4cdr5kdf);

  /* Terminate for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  SubSystem: '<S1>/i2cRd'
   */
  master_code_V2_test_i2cRd_Term(&master_code_V2_test_DW.i2cRd);

  /* Terminate for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  SubSystem: '<S1>/gpsRd'
   */
  master_code_V2_test_gpsRd_Term(master_code_V2_test_M,
    &master_code_V2_test_DW.gpsRd);

  /* Terminate for Chart: '<Root>/ECU_STATE_MACHINE' incorporates:
   *  SubSystem: '<S1>/cmd_motor'
   */
  master_code_V2_t_cmd_motor_Term(&master_code_V2_test_DW.cmd_motor);

  /* Terminate for ToFile: '<Root>/To File' */
  {
    FILE *fp = (FILE *) master_code_V2_test_DW.ToFile_PWORK.FilePtr;
    if (fp != (NULL)) {
      char fileName[509] = "sensors.mat";
      if (fclose(fp) == EOF) {
        rtmSetErrorStatus(master_code_V2_test_M,
                          "Error closing MAT-file sensors.mat");
        return;
      }

      if ((fp = fopen(fileName, "r+b")) == (NULL)) {
        rtmSetErrorStatus(master_code_V2_test_M,
                          "Error reopening MAT-file sensors.mat");
        return;
      }

      if (rt_WriteMat4FileHeader(fp, 19 + 1,
           master_code_V2_test_DW.ToFile_IWORK.Count, "sensor")) {
        rtmSetErrorStatus(master_code_V2_test_M,
                          "Error writing header for sensor to MAT-file sensors.mat");
      }

      if (fclose(fp) == EOF) {
        rtmSetErrorStatus(master_code_V2_test_M,
                          "Error closing MAT-file sensors.mat");
        return;
      }

      master_code_V2_test_DW.ToFile_PWORK.FilePtr = (NULL);
    }
  }
}
