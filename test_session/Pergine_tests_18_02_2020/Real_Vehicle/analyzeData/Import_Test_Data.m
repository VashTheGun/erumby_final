% ---------------------------------------------
%% Import telemetry and OptiTrack sensors data 
% ---------------------------------------------

telemetryData = strcat('../deployCode_and_readSensorsData/',testName,'.txt');
optitrackData = strcat('../deployCode_and_readSensorsData/',testName,'.csv');
[telemetry] = telemetry_import_V2(telemetryData);
[optitrack] = optitrack_import(optitrackData);
[remap_telemetry,remap_optitrack] = test_data_remap(telemetry,optitrack);

