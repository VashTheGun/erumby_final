%% raspberry ethernet connection
% clear;clc;close all;
clc;clear
 h = raspberrypi('10.0.0.1'); %% wif


h.getFile('sensors.mat');
%%

% constant declaration
DUTY_SERVO_SX = 5024;  
DUTY_SERVO_MIDDLE = 6881;
DUTY_SERVO_DX = 8738;

DUTY_ESC_MAX = 8412;
DUTY_ESC_IDLE = 7010;
DUTY_ESC_MIN = 5608;


load('sensors.mat');
%
%--------------- axis remap convection ------------------
%
%                a_x ----> -a_y
%                a_y ----> a_x
%                a_z ----> a_z
%
%--------------------------------------------------------
time = sensor(1,:);
a_x_mpu = sensor(3,:);
a_y_mpu = -sensor(2,:);
a_z_mpu = sensor(4,:);
gyro_x_mpu = sensor(6,:);
gyro_y_mpu = -sensor(5,:);
gyro_z_mpu = sensor(7,:);
% enc_rr = medfilt1(sensor(9,:),3)/100;
% enc_rl = medfilt1(sensor(8,:),3)/100;
enc_rr = sensor(9,:)/100;
enc_rl = sensor(8,:)/100;
enc_fr = medfilt1(sensor(11,:),3)/100;
enc_fl = medfilt1(sensor(12,:),3)/100;
a_x_raw = sensor(14,:);
a_y_raw = -sensor(13,:);
a_z_raw = sensor(15,:);
x_gps = sensor(18,:);
y_gps = sensor(19,:);
yaw_gps = sensor(20,:);


% Steering and traction percent map
for i=1:length(sensor(17,:))
    if(sensor(17,i)==0)
        steering(i) = DUTY_SERVO_MIDDLE;
    else
        steering(i) = sensor(17,i);
    end
    
end

steering = fixpt_interp1( [DUTY_SERVO_SX, DUTY_SERVO_DX], [-100, 100] ,steering, sfix(16), 1, sfix(16), 0.01);
for i=1:length(sensor(10,:))
    if(sensor(10,i)==0)
        traction(i) = DUTY_ESC_IDLE;
    else
        traction(i) = sensor(10,i);
    end
    
end
traction = fixpt_interp1( [DUTY_ESC_MIN, DUTY_ESC_MAX], [-100, 100] ,traction, sfix(16), 1, sfix(16), 0.01);

target = sensor(16,:);


% plot for check the goodness of test
figure()
plot( (enc_rr+enc_rl)/2)
hold on
plot(target/100)






% figure()
% set(gcf,'units','points','position',[x0,y0,width,height])
% plot(a_y_filt(idx), curv,'+');
% title('steering behaviour')


%%
% File creation

%Script for creation of incremental file name for different test 

test = "test1.txt";             % first file
i = 1;                          % variable inizialization
check = 0;

while(check == 0)               % loop for creation of brand new file
    if exist(test, 'file')
        test = strcat('test', num2str(i), '.txt');
    else
        check = 1;
    end
    i= i+1;
end

% creation of the txt file

today = datestr(now);
fileID  = fopen(test, 'w');
fprintf(fileID, '% s\t % s\n', today,'test curvilineo SX spirale 70%');
fprintf(fileID, '% 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t  % 8s\t % 8s\t % 8s\t % 8s\n', '[s]', '[g]', '[g]', '[g]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[g]', '[g]', '[g]', '[%]', '[%}', '[rad/s]', '[mm]', '[mm]', '[grad]');
fprintf(fileID, '% 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\n', 'time', 'a_x_mpu', 'a_y_mpu', 'a_z_mpu', 'gyro_x_mpu', 'gyro_y_mpu', 'gyro_z_mpu', 'enc_fr', 'enc_fl', 'enc_rr', 'enc_rl', 'a_x_raw', 'a_y_raw', 'a_z_raw', 'steering', 'traction', 'target', 'x_gps', 'y_gps', 'yaw_gps');
fprintf(fileID, '% 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\n', [time; a_x_mpu; a_y_mpu; a_z_mpu; gyro_x_mpu; gyro_y_mpu; gyro_z_mpu; enc_fr; enc_fl; enc_rr; enc_rl; a_x_raw; a_y_raw; a_z_raw; steering; traction; target; x_gps; y_gps; yaw_gps]);
fclose('all');

