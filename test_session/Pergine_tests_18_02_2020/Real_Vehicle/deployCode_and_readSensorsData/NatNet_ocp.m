% Optitrack Matlab / NatNet Polling Sample
%  Requirements:
%   - OptiTrack Motive 2.0 or later
%   - OptiTrack NatNet 3.0 or later
%   - Matlab R2013
clc;clear;

fprintf( 'NatNet Start\n' )

% create an instance of the natnet client class
natnetclient = natnet;

% connect the client to the server (multicast over local loopback) -
% modify for your network
fprintf( 'Connecting to the server\n' )
natnetclient.HostIP = '127.0.0.1';
natnetclient.ClientIP = '127.0.0.1';
natnetclient.ConnectionType = 'Multicast';
natnetclient.connect;
if ( natnetclient.IsConnected == 0 )
    fprintf( 'Client failed to connect\n' )
    fprintf( '\tMake sure the host is connected to the network\n' )
    fprintf( '\tand that the host and client IP addresses are correct\n\n' ) 
    return
end

% get the asset descriptions for the asset names
model = natnetclient.getModelDescription;
if ( model.RigidBodyCount < 1 )
    return
end

% create udp sender
udps = dsp.UDPSender('RemoteIPAddress', '10.0.0.1','RemoteIPPort',25000);

mainLoop = true;

% Init parameters
count_10ms      = 0;
count_100_ms    = 0;
tic;

while mainLoop

    count_10ms  = toc(tic) + count_10ms;
    count_100ms = toc(tic) + count_100ms;
    
    if(count_10ms >= 0.01)
        
        data = natnetclient.getFrame; % method to get current frame

        if (isempty(data.RigidBody(1)))
            fprintf( '\tPacket is empty/stale\n' );
            fprintf( '\tMake sure the server is in Live mode or playing in playback\n\n');
            break;
        end

        for i = 1:model.RigidBodyCount

            q = quaternion( data.RigidBody( i ).qx, data.RigidBody( i ).qy, data.RigidBody( i ).qz, data.RigidBody( i ).qw ); % extrnal file quaternion.m
            angles = EulerAngles(q,'yzx');
            yaw = angles(1)*1000;
            dataSent = [int16((data.RigidBody( i ).x * 1000));int16((-data.RigidBody( i ).z * 1000));int16(-yaw)];
            udps(dataSent);

        end
        
        count_10ms = 0;
    end
end 

disp('NatNet Polling Sample End' )
release(udps);





 