% ---------------------------------------------------------
%% Script to receive data from a Windows PC via UDP
% ---------------------------------------------------------

%% Run this script on the MacBook

clc; clearvars;
format short;

% Create udp receiver
% PC_IPaddr = '10.0.0.3';
PC_IPaddr = '192.168.10.20';

MacBook_receivePort = 25000;
% udpr = dsp.UDPReceiver('RemoteIPAddress',PC_IPaddr, 'LocalIPPort',MacBook_receivePort, 'MessageDataType','double');
udpr = dsp.UDPReceiver('RemoteIPAddress',PC_IPaddr, 'LocalIPPort',MacBook_receivePort, 'MessageDataType','int16');

idx_rec = 1;
dataRec = ones(10000,1)*NaN;  % initialize

% Receive data
tic;
while(toc<=200)
    dataReceived = udpr();
    if(~isempty(dataReceived))
        dataRec(idx_rec) = double(dataReceived(3));
%         if (idx_rec>=4)
%             unwrapped_psi = rad2deg(unwrap(double(dataRec(idx_rec-3:idx_rec))/1000));
%             psi_rec = unwrapped_psi(end);
%         else
%             psi_rec = rad2deg(unwrap(double(dataRec(idx_rec))/1000));
%         end
        psi_rec = rad2deg(wrapTo2Pi(double(dataRec(idx_rec))/1000));
        disp([rad2deg(double(dataRec(idx_rec))/1000), psi_rec]);
%         dataRec(idx_rec,3) = str2double(datestr(clock,'SS.FFF'));
        %disp(dataReceived);
        idx_rec = idx_rec+1;
    end
end

release(udpr);

% dataRec_store = dataRec((all((~isnan(dataRec)),2)),:);

