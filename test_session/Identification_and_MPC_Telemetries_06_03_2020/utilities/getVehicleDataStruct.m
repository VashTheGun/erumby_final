function vehicle_data = getVehicleDataStruct()

% ----------------------------------------------------------------
%% Function purpose: define a struct containing vehicle data. 
%                    All parameters refer to the eRumby vehicle
% ----------------------------------------------------------------


% ----------------------------------------------------------------
%    ___                   _ _  __   __   _    _    _       ___       _        
%   / _ \__ _____ _ _ __ _| | | \ \ / /__| |_ (_)__| |___  |   \ __ _| |_ __ _ 
%  | (_) \ V / -_) '_/ _` | | |  \ V / -_) ' \| / _| / -_) | |) / _` |  _/ _` |
%   \___/ \_/\___|_| \__,_|_|_|   \_/\___|_||_|_\__|_\___| |___/\__,_|\__\__,_|
%                                                                              
% ----------------------------------------------------------------

% VEHICLE
Lr         = 0.153;      % [m] Distance between vehicle CoM and front wheels axle
Lf         = 0.172;      % [m] Distance between vehicle CoM and front wheels axle
L          = Lr+Lf;      % [m] Vehicle wheelbase
m          = 4.61;       % [kg] Total mass of the vehicle + driver = 245 + 70 = 315 kg
delta_offs = 0.04049*L;  % [rad] offset steering angle to let the vehicle go straight
g          = 9.81;       % [m/s^2] acceleration due to gravity
k_D        = 0.6;        % [kg/m] aero drag coefficient
r_w        = 0.05;       % [m] wheel radius
  
vehicle.Lf         = Lf;        
vehicle.Lr         = Lr;        
vehicle.L          = L;     
vehicle.m          = m;     
vehicle.delta_offs = delta_offs;
vehicle.g          = g;
vehicle.k_D        = k_D;
vehicle.r_w        = r_w;


% ----------------------------------------------------------------
% __   __   _    _    _       ___ _               _     ___       _        
% \ \ / /__| |_ (_)__| |___  / __| |_ _ _ _  _ __| |_  |   \ __ _| |_ __ _ 
%  \ V / -_) ' \| / _| / -_) \__ \  _| '_| || / _|  _| | |) / _` |  _/ _` |
%   \_/\___|_||_|_\__|_\___| |___/\__|_|  \_,_\__|\__| |___/\__,_|\__\__,_|
% 
% ----------------------------------------------------------------

% Store all sub-structures of sub-systems data in vehicle structure

vehicle_data.vehicle          = vehicle;

end


