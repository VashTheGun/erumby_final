% ------------------------------------------------------------------------
%% Store all the available telemetry data for the MPC laps
% ------------------------------------------------------------------------

relPath_input_file  = './telemetry_data/MPC_lap_telemetries/Dataset_orig/';
relPath_output_file = './telemetry_data/MPC_lap_telemetries/Dataset_saved/';

test_IDs = [10,12,14,15,16];

for ii=1:length(test_IDs)
    test_ID = test_IDs(ii);
    testName = strcat('testMPC',mat2str(test_ID));
    Import_Test_Data_MPC;
    get_Test_Data;
    save_Test_Data;
end

