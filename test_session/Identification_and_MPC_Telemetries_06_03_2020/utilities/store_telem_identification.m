% ------------------------------------------------------------------------
%% Store all the available telemetry data for the identification maneuvers
% ------------------------------------------------------------------------

relPath_input_file  = './telemetry_data/Identification_Telemetries/Dataset_orig/';
relPath_output_file = './telemetry_data/Identification_Telemetries/Dataset_saved/';

for test_ID=2:21 
    testName = strcat('test',mat2str(test_ID));
    Import_Test_Data;
    get_Test_Data;
    save_Test_Data;
    if (test_ID==12)
        test_bis = '12_bis';
        testName = strcat('test',test_bis);
        Import_Test_Data;
        get_Test_Data;
        save_Test_Data;
    end
end
