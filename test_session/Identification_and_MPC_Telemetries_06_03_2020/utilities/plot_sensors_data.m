function plot_sensors_data(sensorsData,filteredData,test_id,traction_percent_logged)

    % --------------------------------------------
    %% function purpose:  plot sensors data
    % --------------------------------------------
    
    % ---------------------
    %% Extract data
    % ---------------------
    time_T  = sensorsData.time_T;
    time_O  = sensorsData.time_O;
    Speed_O = sensorsData.Speed;
    Speed_T = sensorsData.Speed_T;
    delta   = sensorsData.delta;
    Omega_T = sensorsData.Omega_T;
    Omega_O = sensorsData.Omega_O;
    Ay_T    = sensorsData.Ay_T;
    Ay_ss_O = sensorsData.Ay_ss_O;
%     Ay_ss_T = sensorsData.Ay_ss_T;
    Ax_T    = sensorsData.Ax_T;
    x_O     = sensorsData.x_O;
    y_O     = sensorsData.y_O;
    psi_O   = sensorsData.psi_O;
    omega_rr_T = sensorsData.omega_rr_T;
    omega_rl_T = sensorsData.omega_rl_T;
    omega_fr_T = sensorsData.omega_fr_T;
    omega_fl_T = sensorsData.omega_fl_T;
    if (traction_percent_logged)
        traction_T = sensorsData.traction_T;
    end
    
    % Extract filtered data
    time_restrict         = filteredData.time;
    delta_restrict        = filteredData.delta;
    Omega_T_filt_restrict = filteredData.Omega_T;
    Speed_O_filt_restrict = filteredData.Speed_O;
%     Ay_ss_filt_restrict   = filteredData.Ay_ss_T;

    vehicle_data = getVehicleDataStruct();
    r_w = vehicle_data.vehicle.r_w;  % [m] wheel radius
    
    % ---------------------
    %% States
    % ---------------------
    label_fig_mainQuantities = strcat({'Main quantities'},{' '},{test_id});
    figure('Name',label_fig_mainQuantities{1},'NumberTitle','off'), clf   
    % --- u --- %
    ax(1) = subplot(331);
    hold on
    plot(time_O(1:length(Speed_O)),Speed_O,'.')
    plot(time_T,omega_rr_T*r_w,'.')
    plot(time_T,omega_rl_T*r_w,'.')
    if (~contains(test_id,'MPC'))
        plot(time_T,omega_fr_T*r_w,'.')
        legend('optitrack','$\omega_{rr} r_w$','$\omega_{rl} r_w$','$\omega_{fr} r_w$','location','best')
    else
        legend('optitrack','$\omega_{rr} r_w$','$\omega_{rl} r_w$','location','best')
    end
    grid on
    title('$u$ [m/s]')
    xlim([time_T(1) max(time_T(end),time_O(end))])
    % --- delta --- %
    ax(2) = subplot(332);
    plot(time_T,rad2deg(delta),'.')
    hold on
    grid on
    title('$\delta$ [deg]')
    legend('$\delta$','location','best')
    xlim([time_T(1) max(time_T(end),time_O(end))])
    % --- Omega --- %
    ax(3) = subplot(333);
    hold on
    plot(time_O(1:length(Omega_O)),Omega_O,'.')
    plot(time_T,Omega_T,'.')
    grid on
    title('$\Omega$ [rad/s]')
    legend('optitrack','IMU','location','best')
    xlim([time_T(1) max(time_T(end),time_O(end))])
    % --- Ay --- %
    ax(4) = subplot(334);
    hold on
    plot(time_T,Ay_T,'.')
%     plot(time_T,Ay_ss_T,'.')
    grid on
    title('$A_{y}$ [m/s$^2$]')
    legend('IMU','location','best')
    xlim([time_T(1) max(time_T(end),time_O(end))])
    % --- Ax --- %
    ax(5) = subplot(335);
    plot(time_T,Ax_T,'.')
    grid on
    title('$A_{x}$ [m/s$^2$]')
    legend('IMU','location','best')
    xlim([time_T(1) max(time_T(end),time_O(end))])
    % --- omega_wheels --- %
    ax(6) = subplot(336);
    hold on
    plot(time_T,omega_rr_T,'.')
    plot(time_T,omega_rl_T,'.')
    if (~contains(test_id,'MPC'))
        plot(time_T,omega_fr_T,'.')
        legend('$\omega_{rr}$','$\omega_{rl}$','$\omega_{fr}$','location','best')
    else
        legend('$\omega_{rr}$','$\omega_{rl}$','location','best')
    end
    grid on
    title('wheel speed [rad/s]')
    xlim([time_T(1) max(time_T(end),time_O(end))])
    if (traction_percent_logged)
        % --- traction command --- %
        ax(7) = subplot(337);
        hold on
        plot(time_T,traction_T,'.')
        grid on
        title('traction command [-]')
        xlim([time_T(1) max(time_T(end),time_O(end))])
    end

    % linkaxes(ax,'x')
    clear ax

    % ---------------------
    %% Vehicle path
    % ---------------------
    label_fig_vehPath = strcat({'Vehicle path'},{' '},{test_id});
    figure('Name',label_fig_vehPath{1},'NumberTitle','off'), clf
    % --- path --- %
    ax(1) = subplot(221);
    plot(x_O,y_O,'.')
    grid on
    axis equal
    xlabel('$x$ [m]')
    ylabel('$y$ [m]')
    title('Vehicle path')
    % --- x coord --- %
    ax(2) = subplot(222);
    hold on
    plot(time_O,x_O,'.')
    grid on
    title('$x$ [m]')
    xlim([time_T(1) max(time_T(end),time_O(end))])
    % --- y coord --- %
    ax(3) = subplot(223);
    hold on
    plot(time_O,y_O,'.')
    grid on
    title('$y$ [m]')
    xlim([time_T(1) max(time_T(end),time_O(end))])
    % --- psi --- %
    ax(4) = subplot(224);
    hold on
    psi_plot = rad2deg(psi_O);
    plot(time_O,psi_plot,'.')
    grid on
    title('$\psi$ [deg]')
    xlim([time_T(1) max(time_T(end),time_O(end))])

end
