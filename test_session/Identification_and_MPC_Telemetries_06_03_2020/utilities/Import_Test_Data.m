% ---------------------------------------------
%% Import telemetry and OptiTrack sensors data 
% ---------------------------------------------

telemetryData = strcat(relPath_input_file,testName,'.txt');
optitrackData = strcat(relPath_input_file,testName,'.csv');
[telemetry] = telemetry_import_V2(telemetryData);
[optitrack] = optitrack_import(optitrackData);

% Variable used to align (in post-proc) the optitrack data with the on-board vehicle sensors
align_variable = 'none';  % 'speed'  'yaw_rate'  'none'
% Low-pass filtering flag -> if filter_flag = 0 then no filtering is applied
filter_flag = 0;
[remap_telemetry,remap_optitrack] = test_data_remap_V3(telemetry,optitrack,align_variable,filter_flag); 
traction_percent_logged = 1;  % The traction percent (PWM %) was logged