# TEST 12/06/18

Serie di test di turning in steady state per velocità crescenti, nel veicolo è stato introdotto un controllore di velocità

 * test1: spirale con sterzo a sinistra (velocità 20 rad/s) 
 * test2: spirale con sterzo a sinistra (velocità 30 rad/s) 
 * test3: spirale con sterzo a sinistra (velocità 40 rad/s) 
 * test4: spirale con sterzo a sinistra (velocità 50 rad/s) 
 * test5: spirale con sterzo a sinistra (velocità 60 rad/s) 
 * test6: spirale con sterzo a sinistra (velocità 65 rad/s) 
 * test7: spirale con sterzo a sinistra (velocità 70 rad/s) 