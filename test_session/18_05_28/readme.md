# TEST 28/05/18

serie di test effettuati con un aumento della velocità più graduale rispetto ai precedenti (23/05), al fine di avere uno s.s. più consistente
i test sono tutti fatti in configurazione open loop.

 * test1: spirale con sterzo a sinistra (percetuale di sterzo 92%) 
 * test2: spirale con sterzo a sinistra (percetuale di sterzo 71%) 
 * test3: spirale con sterzo a sinistra (percetuale di sterzo 71%) (velocità di partenza aumentata) 
 * test4: spirale con sterzo a sinistra (percetuale di sterzo 71%) (test ripetuto per un confronto con il precedente) 
 * test5: spirale con sterzo a sinistra (percetuale di sterzo 71%) (aumentato il tempo totale del test per sautare l'esc) 
 * test6: spirale con sterzo a destra (percetuale di sterzo 50%) 