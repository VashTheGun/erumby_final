# TEST 30/05/18

serie di test effettuati invertendo il motore per raggiungere velocità più elevate. Inoltre è stato cambiato il posizionamento degli assi
sul robot ( x avanti y verticale e z a destra). Nei primi due test il comportamento è molto non lineare alle alte velocità.
Nell'ultimo test questo effetto è minore.

 * test6: spirale con sterzo a sinistra (percetuale di sterzo 92%) 
 * test7: spirale con sterzo a destra (percetuale di sterzo 92%) 
 * test8: spirale con sterzo a sinistra (percetuale di sterzo 71%)