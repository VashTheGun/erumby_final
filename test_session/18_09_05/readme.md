# TEST 05/09/18

doppia serie di test divisi in una per l'identificazione dell'angolo di offset dello sterzo ed un'altra serie per identificare le forze laterali a s.state.

test sterzo
 * test1: dritto closed loop
 * test2: dritto open loop
 * test3: dritto open loop (stesso test di sopra ripetuto)
 * test4: dritto open loop (stesso test di sopra ripetuto)
 * test5: dritto closed loop su traiettoria diagonale
 * test6: dritto closed loop su traiettoria diagonale diversi guadagni controllore

test tyre force
 * test5: spirale con sterzo a sinistra (vel 20 rad/s) 
 * test6: spirale con sterzo a sinistra (vel 30 rad/s) 
 * test7: spirale con sterzo a sinistra (vel 40 rad/s) 
 * test8: spirale con sterzo a sinistra (vel 50 rad/s) 
 * test9: spirale con sterzo a sinistra (vel 55 rad/s) 
 * test10: spirale con sterzo a sinistra (vel 60 rad/s) 
 * test11: spirale con sterzo a sinistra (vel 65 rad/s) 
 * test12: spirale con sterzo a dx (vel 20 rad/s) 
 * test13: spirale con sterzo a dx  (vel 30 rad/s) 
 * test14: spirale con sterzo a dx  (vel 40 rad/s) 
 * test15: spirale con sterzo a dx  (vel 50 rad/s) 
 * test16: spirale con sterzo a dx  (vel 55 rad/s) 
 * test17: spirale con sterzo a dx  (vel 60 rad/s) 
 * test18: spirale con sterzo a dx  (vel 65 rad/s) 
 * test18: spirale con sterzo a dx  (vel 70 rad/s) 