% ----------------------------------------------------------------------------------------------
%% Main script to collect all the available telemetries for the eRumby vehicle (Pergine circuit)
%   - author:  Mattia Piccinini 
%   - date:    20/02/2021
% ----------------------------------------------------------------------------------------------

% ---------------------
%% Initialize
% ---------------------
addpath('./utilities');
addpath('./tools');
initialize_script_telem;

% ---------------------
%% Load vehicle data
% ---------------------
vehicle_data = getVehicleDataStruct();

% ---------------------
%% Store telemetry data 
%  for identification maneuvers
% ---------------------
store_telem_identification;

% ---------------------
%% Store telemetry data 
%  for the MPC full lap tests
% ---------------------
store_telem_MPC_laps;
