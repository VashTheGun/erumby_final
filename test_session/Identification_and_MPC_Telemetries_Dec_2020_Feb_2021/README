--> The folder Identification_and_MPC_Telemetries_Dec_2020_Feb_2021 contains all the telemetry data acquired with the eRumby vehicle in the period December 2020 - February 2021.

--> The folder telemetry_data contains the telemetries: the folder telemetry_data/Identification_Telemetries contains the telemetries related to the identification maneuvers, while the folder telemetry_data/MPC_lap_telemetries contains the data collected with the vehicle autonomously driven along the Pergine circuit (using MPC).

--> The script collect_telemetries.m creates txt files with the useful telemetries, stored in the folders telemetry_data/Identification_Telemetries/Dataset_saved and telemetry_data/MPC_lap_telemetries/Dataset_saved. 

--> The localization and state estimation of the eRumby vehicle is performed with an Optitrack system (that is an indoor positioning system based on cameras and markers) + on-board sensors (IMU and encoders).

--> Each txt telemetry file contains the following data:
		o time_T -> time vector [s], with sampling rate = 100 Hz, referring to the on-board telemetry data (signals: delta, Omega, Ay, Ax, omega_rr, omega_rl, omega_fr);
		o time_O -> time vector [s], with sampling time = 120 Hz, referring to the Optitrack telemetry data (signals: u, x, y, psi);
		o u      -> forward speed [m/s], from the Optitrack system
		o delta  -> steering angle [rad] provided as a reference to the steering servo 
		o Omega  -> yaw rate [rad/s] measured with the IMU
		o Ay     -> lateral acceleration [m/s^2] measured with the IMU
		o Ax     -> longitudinal acceleration [m/s^2] measured with the IMU
		o x      -> x-coord of the vehicle CoM [m], measured with the Optitrack system
		o y      -> y-coord of the vehicle CoM [m], measured with the Optitrack system
		o psi    -> yaw angle [rad], measured with the Optitrack system

--> Notes: 
		o The forward speed u signal measured with the Optitrack system sometimes features some large spikes, due to markers reflections and other phenomena. Such spikes have to be manually removed before using the data. 	
		o The traction % (i.e. the PWM percentage provided by the ESC) was NOT recorded during the tests with the MPC controller, but it was recorded in the identification maneuvers
		o The Optitrack data were NOT sent to the raspberry, so the Optitrack data are NOT aligned by default with the other telemetry data
		o In the identification telemetries, the data measured by the front right encoder are available, but in the MPC telemetries they are NOT (due to a programming error)