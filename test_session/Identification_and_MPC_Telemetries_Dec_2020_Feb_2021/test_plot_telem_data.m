% -------------------------------
%% Plot telemetry data
% -------------------------------

addpath('./telemetry_data/Identification_Telemetries/Dataset_saved')
addpath('./telemetry_data/MPC_lap_telemetries/Dataset_saved')
addpath('./utilities')

set(0,'DefaultFigureWindowStyle','docked');
set(0,'defaultAxesFontSize',20)
set(0,'DefaultLegendFontSize',20)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

% ------------
%% Select the telemetry to import
% ------------
% MPC test data must be one of: 'testMPC8','testMPC9','testMPC12',...,'testMPC16'
% Identification maneuvers data must be one of: 'test2',...,'test11'
testID = 'test11';

test_data = readtable(strcat(testID,'.txt'));

vehicle_data = getVehicleDataStruct();
r_tire = vehicle_data.vehicle.r_tire;

% ------------
%% Plots
% ------------

% ------------
%% States
% ------------
figure('Name','Main quantities','NumberTitle','off'), clf   
% --- u --- %
ax(1) = subplot(331);
hold on
plot(test_data.time_O,test_data.u,'.')
plot(test_data.time_T,test_data.omega_rr*r_tire,'.')
plot(test_data.time_T,test_data.omega_rl*r_tire,'.')
plot(test_data.time_T,test_data.omega_fr*r_tire,'.')
legend('OptiT','$\omega_{rr} r_w$','$\omega_{rl} r_w$','$\omega_{fr} r_w$','location','best')
grid on
title('$u$ [m/s]')
% --- delta --- %
ax(2) = subplot(332);
hold on
plot(test_data.time_T,rad2deg(test_data.delta),'.')
grid on
title('$\delta$ [deg]')
legend('rasp ctrl','location','best')
% --- Omega --- %
ax(3) = subplot(333);
hold on
plot(test_data.time_T,test_data.Omega,'.')
grid on
title('$\Omega$ [rad/s]')
legend('IMU','location','best')
% --- Ay --- %
ax(4) = subplot(334);
hold on
plot(test_data.time_T,test_data.Ay,'.')
grid on
title('$A_{y}$ [m/s$^2$]')
legend('IMU','location','best')
% --- Ax --- %
ax(5) = subplot(335);
hold on
plot(test_data.time_T,test_data.Ax,'.')
grid on
title('$A_{x}$ [m/s$^2$]')
legend('IMU','location','best')
% --- omega_wheels --- %
ax(6) = subplot(336);
hold on
plot(test_data.time_T,test_data.omega_rr,'.')
plot(test_data.time_T,test_data.omega_rl,'.')
plot(test_data.time_T,test_data.omega_fr,'.')
grid on
title('wheel speed [rad/s]')
legend('$\omega_{rr}$','$\omega_{rl}$','$\omega_{fr}$','location','best')
% --- traction command --- %
ax(7) = subplot(337);
hold on
plot(test_data.time_T,test_data.pedal,'.')
grid on
title('traction command [percent]')
% --- lateral cmp --- %
ax(8) = subplot(338);
hold on
plot(test_data.time_T,rad2deg(test_data.delta),'.')
plot(test_data.time_T,test_data.Omega*10,'.')
grid on
title('lateral dyna')
legend('$\delta$','$\Omega$ scaled','location','best')
% --- longit cmp --- %
ax(9) = subplot(339);
hold on
plot(test_data.time_O,test_data.u,'.')
plot(test_data.time_T,test_data.omega_rr*r_tire,'.')
plot(test_data.time_T,test_data.omega_rl*r_tire,'.')
plot(test_data.time_T,test_data.omega_fr*r_tire,'.')
plot(test_data.time_T,test_data.Ax,'.')
plot(test_data.time_T,test_data.pedal/10,'.')
grid on
title('lateral dyna')
legend('$u$ optiT','$\omega_{rr} r_w$','$\omega_{rl} r_w$','$\omega_{fr} r_w$','$a_x$','traction scaled','location','best')

% linkaxes(ax,'x')
clear ax

% ------------
%% Vehicle path
% ------------
figure('Name','Vehicle path','NumberTitle','off'), clf
% --- path --- %
ax(1) = subplot(221);
hold on   
plot(test_data.x,test_data.y,'.')
grid on
axis equal
xlabel('x [m]')
ylabel('y [m]')
legend('OptiT','location','best')
title('Vehicle CoM Path')
% --- x --- %
ax(2) = subplot(222);
hold on
plot(test_data.time_O,test_data.x,'.')
grid on
title('$x$ [m]')
legend('OptiT','location','best')
% --- y --- %
ax(3) = subplot(223);
hold on
plot(test_data.time_O,test_data.y,'.')
grid on
title('$y$ [m]')
legend('OptiT','location','best')
% --- psi --- %
ax(4) = subplot(224);
hold on
plot(test_data.time_O,rad2deg(test_data.psi),'.')
grid on
title('$\psi$ [deg]')
legend('OptiT','location','best')
