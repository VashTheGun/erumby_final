%     --> subscript _T is used for telemetry data
%     --> subscript _O is used for optitrack data

% Extract the most important sensors data
sensorsData = extractSensorsData(remap_telemetry,remap_optitrack,vehicle_data);

% % Filter and resize sensors data
% filter_flag = 0;  % set it to 1 to enable low pass filtering of data
% init_sample = 1;
% fin_sample  = 0;  % 500 for test14, 3000 for test12
% trailing_flag = 0;
% filteredData = resize_and_filter(sensorsData,filter_flag,init_sample,fin_sample,trailing_flag);

% Plot sensors data
enable_plotSensorsData = 1;
if (enable_plotSensorsData)
    plot_sensors_data(sensorsData,testName);
end
