% -----------------------------------------------------------
%% Extract the useful telemetry data and save them in a file
% -----------------------------------------------------------

time_T_save = sensorsData.time_T;
time_O_save = sensorsData.time_O;
max_length = max(length(time_T_save),length(time_O_save));
if (max_length==length(time_T_save))
    time_T_save_new = time_T_save;
    time_O_save_new = [time_O_save; ones(length(time_T_save)-length(time_O_save),1)*NaN];
    u_save     = [sensorsData.Speed; ones(length(time_T_save)-length(time_O_save),1)*NaN];
    x_save     = [sensorsData.x_O; ones(length(time_T_save)-length(time_O_save),1)*NaN];
    y_save     = [sensorsData.y_O; ones(length(time_T_save)-length(time_O_save),1)*NaN];
    psi_save   = [sensorsData.psi_O; ones(length(time_T_save)-length(time_O_save),1)*NaN];
    delta_save = sensorsData.delta;
    Omega_save = sensorsData.Omega_T;
    Ay_save    = sensorsData.Ay_T;
    Ax_save    = sensorsData.Ax_T;
    omega_rr_save = sensorsData.omega_rr_T;
    omega_rl_save = sensorsData.omega_rl_T;
    omega_fr_save = sensorsData.omega_fr_T;
    pedal_save = sensorsData.traction_T;
else  %(max_length==length(time_T_save))
    time_T_save_new = [time_T_save; ones(length(time_O_save)-length(time_T_save),1)*NaN];
    time_O_save_new = time_O_save;
    u_save     = sensorsData.Speed;
    x_save     = sensorsData.x_O;
    y_save     = sensorsData.y_O;
    psi_save   = sensorsData.psi_O;
    delta_save = [sensorsData.delta; ones(length(time_O_save)-length(time_T_save),1)*NaN];
    Omega_save = [sensorsData.Omega_T; ones(length(time_O_save)-length(time_T_save),1)*NaN];
    Ay_save    = [sensorsData.Ay_T; ones(length(time_O_save)-length(time_T_save),1)*NaN];
    Ax_save    = [sensorsData.Ax_T; ones(length(time_O_save)-length(time_T_save),1)*NaN];
    omega_rr_save = [sensorsData.omega_rr_T; ones(length(time_O_save)-length(time_T_save),1)*NaN];
    omega_rl_save = [sensorsData.omega_rl_T; ones(length(time_O_save)-length(time_T_save),1)*NaN];
    omega_fr_save = [sensorsData.omega_fr_T; ones(length(time_O_save)-length(time_T_save),1)*NaN];
    pedal_save = [sensorsData.traction_T; ones(length(time_O_save)-length(time_T_save),1)*NaN];
end

% Save data
file_path = strcat(relPath_output_file,testName,'.txt');
fileID = fopen(file_path, 'w');
fprintf(fileID,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','time_T','time_O','u','delta','Omega','Ay','Ax','x','y','psi','omega_rr','omega_rl','omega_fr','pedal');
for ii = 1:max_length
    fprintf(fileID,'%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\n',time_T_save_new(ii),time_O_save_new(ii),u_save(ii),delta_save(ii),Omega_save(ii),Ay_save(ii),Ax_save(ii),x_save(ii),y_save(ii),psi_save(ii),omega_rr_save(ii),omega_rl_save(ii),omega_fr_save(ii),pedal_save(ii));
end
fclose(fileID);
