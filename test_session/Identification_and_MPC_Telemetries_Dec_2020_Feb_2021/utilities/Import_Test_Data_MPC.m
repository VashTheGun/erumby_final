% ---------------------------------------------
%% Import telemetry and OptiTrack sensors data 
% ---------------------------------------------

telemetryData = strcat(relPath_input_file,testName,'.txt');
optitrackData = strcat(relPath_input_file,testName,'.csv');
[telemetry] = telemetry_import_V2(telemetryData);
[optitrack] = optitrack_import(optitrackData);

% Variable used to align (in post-proc) the optitrack data with the on-board vehicle sensors
align_variable = 'pose';  % 'none'
% Low-pass filtering flag -> if filter_flag = 0 then no filtering is applied
filter_flag = 0;
% Flag to specify whether the MPC controller was used or if the maneuver was run in open loop (i.e. for identification)
MPC_flag = 1;
[remap_telemetry,remap_optitrack] = test_data_remap_V5(telemetry,optitrack,align_variable,filter_flag,MPC_flag); 
