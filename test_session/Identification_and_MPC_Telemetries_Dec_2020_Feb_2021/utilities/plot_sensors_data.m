function plot_sensors_data(sensorsData,test_id)

    % --------------------------------------------
    %% function purpose: plot sensors data
    % --------------------------------------------
    
    % Start and final indices for the plots with telemetry data
    idx_start_tel = 1;
    idx_end_tel   = length(sensorsData.time_T)-1;

    % Start and final indices for the plots with optitrack data
    idx_start_optiT = 1;
    idx_end_optiT   = length(sensorsData.time_O)-1;

    vehicle_data = getVehicleDataStruct();
    r_tire = vehicle_data.vehicle.r_tire;  % [m] wheel radius
    
    % ---------------------
    %% States
    % ---------------------
    label_fig_mainQuantities = strcat({'Main quantities'},{' '},{test_id});
    figure('Name',label_fig_mainQuantities{1},'NumberTitle','off'), clf   
    % --- u --- %
    ax(1) = subplot(331);
    hold on
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.target_speed_T(idx_start_tel:idx_end_tel),'.')
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.Speed(idx_start_optiT:idx_end_optiT),'.')
    %plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Speed_gps_T(idx_start_tel:idx_end_tel),'.')
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rr_T(idx_start_tel:idx_end_tel)*r_tire,'.')
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rl_T(idx_start_tel:idx_end_tel)*r_tire,'.')
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_fr_T(idx_start_tel:idx_end_tel)*r_tire,'.')
    legend('rasp ctrl','OptiT','$\omega_{rr} r_w$','$\omega_{rl} r_w$','$\omega_{fr} r_w$','location','best')
    grid on
    title('$u$ [m/s]')
    % --- delta --- %
    ax(2) = subplot(332);
    hold on
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),rad2deg(sensorsData.delta(idx_start_tel:idx_end_tel)),'.')
    hold on
    grid on
    title('$\delta$ [deg]')
    legend('rasp ctrl','location','best')
    % --- Omega --- %
    ax(3) = subplot(333);
    hold on
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.Omega_O(idx_start_optiT:idx_end_optiT),'.')
    %plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Omega_gps_T(idx_start_tel:idx_end_tel),'.')
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Omega_T(idx_start_tel:idx_end_tel),'.')
    grid on
    title('$\Omega$ [rad/s]')
    legend('OptiT','IMU','location','best')
    % --- Ay --- %
    ax(4) = subplot(334);
    hold on
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Ay_T(idx_start_tel:idx_end_tel),'.')
    grid on
    title('$A_{y}$ [m/s$^2$]')
    legend('IMU','location','best')
    % --- Ax --- %
    ax(5) = subplot(335);
    hold on
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.Ax_T(idx_start_tel:idx_end_tel),'.')
    grid on
    title('$A_{x}$ [m/s$^2$]')
    legend('IMU','location','best')
    xlim([sensorsData.time_T(idx_start_tel) sensorsData.time_T(idx_end_tel)])
    % --- omega_wheels --- %
    ax(6) = subplot(336);
    hold on
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rr_T(idx_start_tel:idx_end_tel),'.')
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_rl_T(idx_start_tel:idx_end_tel),'.')
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.omega_fr_T(idx_start_tel:idx_end_tel),'.')
    grid on
    title('wheel speed [rad/s]')
    legend('$\omega_{rr}$','$\omega_{rl}$','$\omega_{fr}$','location','best')
    xlim([sensorsData.time_T(idx_start_tel) sensorsData.time_T(idx_end_tel)])
    % --- traction command --- %
    ax(7) = subplot(337);
    hold on
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.traction_T(idx_start_tel:idx_end_tel),'.')
    grid on
    title('traction command [percent]')
    xlim([sensorsData.time_T(idx_start_tel) sensorsData.time_T(idx_end_tel)])
    
    % linkaxes(ax,'x')
    clear ax

    % ---------------------
    %% Vehicle path
    % ---------------------
    label_fig_vehPath = strcat({'Vehicle path'},{' '},{test_id});
    figure('Name',label_fig_vehPath{1},'NumberTitle','off'), clf
    % --- path --- %
    ax(1) = subplot(221);
    hold on   
    plot(sensorsData.x_O(idx_start_optiT:idx_end_optiT),sensorsData.y_O(idx_start_optiT:idx_end_optiT),'.')
    plot(sensorsData.x_gps_T(idx_start_tel:idx_end_tel),sensorsData.y_gps_T(idx_start_tel:idx_end_tel),'.')
    grid on
    axis equal
    xlabel('x [m]')
    ylabel('y [m]')
    legend('OptiT','OptiT telem','location','best')
    title('Vehicle CoM Path')
    % --- x --- %
    ax(2) = subplot(222);
    hold on
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.x_O(idx_start_optiT:idx_end_optiT),'.')
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.x_gps_T(idx_start_tel:idx_end_tel),'.')
    grid on
    title('$x$ [m]')
    legend('OptiT','OptiT telem','location','best')
    % --- y --- %
    ax(3) = subplot(223);
    hold on
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),sensorsData.y_O(idx_start_optiT:idx_end_optiT),'.')
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),sensorsData.y_gps_T(idx_start_tel:idx_end_tel),'.')
    grid on
    title('$y$ [m]')
    legend('OptiT','OptiT telem','location','best')
    % --- psi --- %
    ax(4) = subplot(224);
    hold on
    plot(sensorsData.time_O(idx_start_optiT:idx_end_optiT),rad2deg(sensorsData.psi_O(idx_start_optiT:idx_end_optiT)),'.')
    plot(sensorsData.time_T(idx_start_tel:idx_end_tel),rad2deg(sensorsData.psi_gps_T(idx_start_tel:idx_end_tel)),'.')
    grid on
    title('$\psi$ [deg]')
    legend('OptiT','OptiT telem','location','best')

end
