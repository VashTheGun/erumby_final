% ------------------------------------------------------------------------
%% Store all the available telemetry data for the identification maneuvers
% ------------------------------------------------------------------------

relPath_input_file  = './telemetry_data/Identification_Telemetries/Dataset_orig/';
relPath_output_file = './telemetry_data/Identification_Telemetries/Dataset_saved/';

for test_ID=2:11 
    testName = strcat('test',mat2str(test_ID));
    Import_Test_Data;
    get_Test_Data;
    save_Test_Data;
end
