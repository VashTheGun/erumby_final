# TEST 23/05/18

serie di test effettuati per ottenere una prima stima dei parametri del veicolo, i test sono tutti fatti in configurazione open loop.

 * test1: spirale con sterzo a sinistra (percetuale di sterzo 92%) 
 * test2: spirale con sterzo a sinistra (percetuale di sterzo 71%) 
 * test3: spirale con sterzo a sinistra (percetuale di sterzo 49%) 
 * test4: spirale con sterzo a destra (percetuale di sterzo 92%) 
 * test5: spirale con sterzo a destra (percetuale di sterzo 71%) 
 * test6: spirale con sterzo a destra (percetuale di sterzo 49%) 
 * test7: test dritto (percentuale esc 35%) 
 * test8: test dritto (percentuale esc 49%) 
 * test9: test dritto con sinewave applicata allo sterzo (ampienza 17% frequenza 1 rad/s) 
 * test10: test dritto con sinewave applicata allo sterzo (ampienza 25% frequenza 5 rad/s) 
 * test11: test dritto con sinewave applicata allo sterzo (ampienza 25% frequenza 7 rad/s) 