function vehicle_data = getVehicleDataStruct()

% ----------------------------------------------------------------
%% Function purpose: define a struct containing vehicle data. 
%                    All parameters refer to the eRumby vehicle
% ----------------------------------------------------------------


% ----------------------------------------------------------------
%    ___                   _ _  __   __   _    _    _       ___       _        
%   / _ \__ _____ _ _ __ _| | | \ \ / /__| |_ (_)__| |___  |   \ __ _| |_ __ _ 
%  | (_) \ V / -_) '_/ _` | | |  \ V / -_) ' \| / _| / -_) | |) / _` |  _/ _` |
%   \___/ \_/\___|_| \__,_|_|_|   \_/\___|_||_|_\__|_\___| |___/\__,_|\__\__,_|
%                                                                              
% ----------------------------------------------------------------

% VEHICLE
Lr  = 0.153;   % [m] Distance between vehicle CoM and front wheels axle
Lf  = 0.172;   % [m] Distance between vehicle CoM and front wheels axle
L   = Lr+Lf;   % [m] Vehicle wheelbase
W   = 0.2;     % [m] Vehicle track width
m   = 4.61;    % [kg] Total mass of the vehicle + driver = 245 + 70 = 315 kg
h_G = 0.05;    % [m] Vehicle CoM height from ground
Iz  = ((0.5*(Lf+Lr))^2*m);   % [kg*m^2] Moment of inertia z-axis
g   = 9.81;    % [m/s^2] acceleration due to gravity
K_D = 0.3*m;   % [kg/m] Aerodynamic drag coefficient
tau_D = 1;     % [-] Transmission ratio steering system
tau_H = 0.04;  % [s] Steering actuation time constant  
tau_N = 0.2;   % [s] Vertical loads dynamics time constant 

vehicle.Lf  = Lf;        
vehicle.Lr  = Lr;        
vehicle.L   = L; 
vehicle.W   = W;
vehicle.m   = m;  
vehicle.h_G = h_G;  
vehicle.g   = g;
vehicle.K_D = K_D;
vehicle.Iz  = Iz;
vehicle.tau_D = tau_D;
vehicle.tau_H = tau_H;
vehicle.tau_N = tau_N; 

% TIRES
Kf = 15.93;  
Kr = 10.49;  
Cf = 1.218;    
Cr = 1.5;
Df = 0.6403;   
Dr = 0.6784;
Bf = Kf/(Cf*Df);
Br = Kr/(Cr*Dr);
alpha_off_f = 0;  % 0.04143;
alpha_off_r = 0;  % -0.0097;
sigma_yf = 0.2;  % [m] relaxation length for the front tire
sigma_yr = 0.2;  % [m] relaxation length for the rear tire

tires.Kf = Kf;
tires.Kr = Kr;
tires.Cf = Cf;
tires.Cr = Cr;
tires.Df = Df;
tires.Dr = Dr;
tires.Bf = Bf;
tires.Br = Br;
tires.alpha_off_f = alpha_off_f;
tires.alpha_off_r = alpha_off_r;
tires.sigma_yf = sigma_yf;
tires.sigma_yr = sigma_yr;

% ----------------------------------------------------------------
% __   __   _    _    _       ___ _               _     ___       _        
% \ \ / /__| |_ (_)__| |___  / __| |_ _ _ _  _ __| |_  |   \ __ _| |_ __ _ 
%  \ V / -_) ' \| / _| / -_) \__ \  _| '_| || / _|  _| | |) / _` |  _/ _` |
%   \_/\___|_||_|_\__|_\___| |___/\__|_|  \_,_\__|\__| |___/\__,_|\__\__,_|
% 
% ----------------------------------------------------------------

% Store all sub-structures of sub-systems data in vehicle structure

vehicle_data.vehicle = vehicle;
vehicle_data.tires   = tires;

end


