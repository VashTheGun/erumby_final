% -------------------------------------------------------
%% Extract and save the main simulation results
% -------------------------------------------------------

if (maneuver_type==1)
    % steady-state cornering maneuver
    u = MPC_sim.states.u.data;
    v = MPC_sim.states.v.data;
    Omega = MPC_sim.states.Omega.data;
    delta_D = MPC_sim.inputs.delta_D.data;
    N = length(u);
    u_ssCorner(maneuver_idx) = u((N-1)/2);
    v_ssCorner(maneuver_idx) = v((N-1)/2);
    Omega_ssCorner(maneuver_idx) = Omega((N-1)/2);
    delta_D_ssCorner(maneuver_idx) = delta_D((N-1)/2);
end