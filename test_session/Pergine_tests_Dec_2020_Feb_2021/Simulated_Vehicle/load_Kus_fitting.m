% ---------------------------------
%% Load Krho fitting coefficients
% ---------------------------------

fitted_Kus_vs_Ay = load('./fitting_Kus_coeffs_vs_Ay/Kus_vs_Ay_speed214');
fitted_Kus = fitted_Kus_vs_Ay.K_us_fitCoeffs_vsAy;

