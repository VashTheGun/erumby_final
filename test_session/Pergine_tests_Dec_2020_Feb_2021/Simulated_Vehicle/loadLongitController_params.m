function longController_params = loadLongitController_params()
    
    longController_params.c1  = 0.00118; 
    longController_params.c2  = 1.532e-05; 
    longController_params.a   = 3.17;
    longController_params.tau = 0.08;
    longController_params.Kp  = 0.02;
    longController_params.Ki  = 0.03;

end