function timings = loadTiming(maneuver_type)

    % Timings
    T0 = 0;           % start simulation time
    Ts = 0.001;       % time step
    if (maneuver_type==2)
        Tf = 22;     % stop simulation time
    else
        Tf = 10;
    end
    
    timings.T0 = T0;
    timings.Ts = Ts;
    timings.Tf = Tf;

end

