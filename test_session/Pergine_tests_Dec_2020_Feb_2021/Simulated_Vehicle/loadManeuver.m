function maneuver = loadManeuver()

    % -------------------------------
    %% Select the type of maneuver
    % -------------------------------
    % Description of the maneuvers and their indeces
    %    - maneuver n�1 = 'ssCorner'        --> costant steering and constant vehicle speed
    %    - maneuver n�2 = 'sineSteer_comp'  --> sine steer maneuver to be carried out to compare 
    %                                           the 'real' vehicle model with the kineto-dynamical one
    %    - maneuver n�3 = 'custom'          --> custom maneuver, user-defined
      
    % Select the desired maneuver
    maneuver.type = 2;  
    
    if (maneuver.type==1)
        % Parameters for the steady-state cornering maneuvers
        maneuver.steer = 1:1:45;           % [deg]
        maneuver.speed = ones(1,10)*NaN;   % only initialized, it will be filled later, with another script
        maneuver.Ay    = [0.1,0.5:0.5:5];  % [m/s^2] list of lateral accelerations
    elseif (maneuver.type==2)
        maneuver.steer.ampl = 35;
        maneuver.steer.freq = 0.09;
        maneuver.speed = 2.2;
    else %(maneuver.type==3)
        maneuver.steer = 0;
        maneuver.speed = 1;
    end

end

