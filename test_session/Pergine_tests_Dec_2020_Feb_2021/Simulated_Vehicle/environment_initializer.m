% ----------------------------------------------------
%% Initialize the simulation environment
% ----------------------------------------------------
clc;
clearvars;
% close all
format long;

% set this to 1 to enable docked window style in plots
enable_docked = 1;
if (enable_docked)
    set(0,'DefaultFigureWindowStyle','docked');
else    
    set(0,'DefaultFigureWindowStyle','normal');
end
set(0,'defaultAxesFontSize',18)
set(0,'DefaultLegendFontSize',18)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

addpath('./Clothoids-develop/matlab');


% ----------------------------------------------------
%% Open Simulink model
% ----------------------------------------------------
% Check if the Simulink model is already opened. Otherwise open it
openModels = find_system('SearchDepth', 0);
if (isempty(find(strcmp(openModels,'vehicle_model'),1)))
    load_system('vehicle_model');
    open_system('vehicle_model');
end 


% ----------------------------------------------------
%% Enable/Disable post-processing
% ----------------------------------------------------
% set this to 1 to enable data analysis plots
enable_dataAnalysis = 0;

% set this to 1 to enable the fitting of Krho, for the handling identification with ss cornering maneuvers
enable_fitting_Krho = 1;

% set this to 1 to enable the fitting of Krho as a function of Ay, for different values of steering angle
enable_fit_Krho_vs_Ay = 1;



