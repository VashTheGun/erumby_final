function y = absReg(x)

    % Regularized sign function
    regFactor = 0.01;
    y = sin(atan(x/regFactor)).*x;

end