/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: master_code_V2_test_data.c
 *
 * Code generated for Simulink model 'master_code_V2_test'.
 *
 * Model version                  : 1.283
 * Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
 * C/C++ source code generated on : Fri Feb 19 17:51:23 2021
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "master_code_V2_test.h"
#include "master_code_V2_test_private.h"

/* Block parameters (default storage) */
P_master_code_V2_test_T master_code_V2_test_P = {
  /* Expression: -1
   * Referenced by: '<S3>/I2C Master Read'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S3>/I2C Master Read1'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S3>/I2C Master Read2'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S3>/I2C Master Read3'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S3>/I2C Master Read4'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S3>/I2C Master Read5'
   */
  -1.0,

  /* Computed Parameter: acc_x_calib_Y0
   * Referenced by: '<S3>/acc_x_calib'
   */
  0,

  /* Computed Parameter: acc_y_calib_Y0
   * Referenced by: '<S3>/acc_y_calib'
   */
  0,

  /* Computed Parameter: acc_z_calib_Y0
   * Referenced by: '<S3>/acc_z_calib'
   */
  0,

  /* Computed Parameter: gyro_x_calib_Y0
   * Referenced by: '<S3>/gyro_x_calib'
   */
  0,

  /* Computed Parameter: gyro_y_calib_Y0
   * Referenced by: '<S3>/gyro_y_calib'
   */
  0,

  /* Computed Parameter: gyro_z_calib_Y0
   * Referenced by: '<S3>/gyro_z_calib'
   */
  0,

  /* Computed Parameter: Gain4_Gain
   * Referenced by: '<Root>/Gain4'
   */
  18641,

  /* Computed Parameter: Gain3_Gain
   * Referenced by: '<Root>/Gain3'
   */
  16384,

  /* Computed Parameter: Gain1_Gain
   * Referenced by: '<Root>/Gain1'
   */
  16384,

  /* Start of '<S1>/gpsRd' */
  {
    /* Computed Parameter: UDPReceive_Port
     * Referenced by: '<S5>/UDP Receive'
     */
    25002,

    /* Computed Parameter: gps_Y0
     * Referenced by: '<S5>/gps'
     */
    0
  }
  ,

  /* End of '<S1>/gpsRd' */

  /* Start of '<S1>/i2cRd' */
  {
    /* Expression: -1
     * Referenced by: '<S6>/I2C Master Read1'
     */
    -1.0,

    /* Expression: -1
     * Referenced by: '<S6>/I2C Master Read4'
     */
    -1.0,

    /* Expression: -1
     * Referenced by: '<S6>/I2C Master Read6'
     */
    -1.0,

    /* Computed Parameter: data_imu_Y0
     * Referenced by: '<S6>/data_imu'
     */
    0,

    /* Computed Parameter: data_enc_rear_Y0
     * Referenced by: '<S6>/data_enc_rear'
     */
    0,

    /* Computed Parameter: data_enc_front_Y0
     * Referenced by: '<S6>/data_enc_front'
     */
    0
  }
  /* End of '<S1>/i2cRd' */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
