% ----------------------------------------
%% Script to read sensors data
% ----------------------------------------

% ------------------
%% Initialize
% ------------------
clc;
clear;

% set this to 1 to enable docked window style in plots
enable_docked = 1;
if (enable_docked)
    set(0,'DefaultFigureWindowStyle','docked');
else    
    set(0,'DefaultFigureWindowStyle','normal');
end
set(0,'defaultAxesFontSize',18)
set(0,'DefaultLegendFontSize',18)

% Set LaTeX as default interpreter for axis labels, ticks and legends
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');

% ------------------
%% Run the code
% ------------------

% Define the raspberry object with its IP address
h = raspberrypi('10.0.0.1'); 

% Set this flag to 1 in order to enable the reception of data (Optitrack) from the Pergine PC (mainly for debugging)
enable_UDP_rec_OptiTrack = 1;

if (enable_UDP_rec_OptiTrack)
    IPaddr_OptiTrack = '192.168.10.20';
    udpr = dsp.UDPReceiver('RemoteIPAddress',IPaddr_OptiTrack,'LocalIPPort',25000,'MessageDataType','int16');
    % Initialize an array to store the data sent from the Pergine PC (if needed)
    storedDataRec = zeros(1,3);
    jj = 1;
end

h.runModel('master_code_V2_test');
final_time_maneuver = 15;  % must be > time_stop_controls of the controls_req Simulink block
tic;
while (toc<=final_time_maneuver)
    % -----------------
    % UNCOMMENT THE FOLLOWING LINES TO STORE THE DATA SENT FROM THE PERGINE PC TO THE MACBOOK
    % -----------------
%     if (enable_UDP_rec_OptiTrack)
%         dataRec_PC = udpr();
%         if (~isempty(dataRec_PC))
%             storedDataRec(jj,:) = dataRec_PC';
%         end
%         jj = jj+1;
%     end
end
h.stopModel('master_code_V2_test');

% Load the file containing sensors data
h.getFile('sensors.mat');
load('sensors.mat');

% Enter the name of the output txt file that will be created, with all telemetry data
output_telem_fileName = 'HalfSineSteerNeg1';

% ------------------
%% Extract sensors data
% ------------------
% constant values related to the vehicle actuators
DUTY_SERVO_SX = 5024;  
DUTY_SERVO_MIDDLE = 6881;
DUTY_SERVO_DX = 8738;

DUTY_ESC_MAX = 8412;
DUTY_ESC_IDLE = 7010;
DUTY_ESC_MIN = 5608;

time = sensor(1,:);
%--------------- axis remap convection ------------------
%
%                a_x ----> -a_y
%                a_y ----> a_x
%                a_z ----> a_z
%
%--------------------------------------------------------
a_x_mpu = sensor(3,:);
a_y_mpu = -sensor(2,:);
a_z_mpu = sensor(4,:);
gyro_x_mpu = sensor(6,:);
gyro_y_mpu = -sensor(5,:);
gyro_z_mpu = sensor(7,:);
enc_rr = sensor(8,:)/100;
enc_rl = sensor(9,:)/100;
enc_fr = sensor(11,:)/100;  %medfilt1(sensor(11,:),3)/100;
enc_fl = sensor(12,:)/100;  %medfilt1(sensor(12,:),3)/100;
a_x_raw = sensor(14,:);
a_y_raw = -sensor(13,:);
a_z_raw = sensor(15,:);
x_gps = sensor(18,:);
y_gps = sensor(19,:);
yaw_gps = sensor(20,:);

% Steering percent map
% for i=1:length(sensor(17,:))
%     if(sensor(17,i)==0)
%         steering(i) = DUTY_SERVO_MIDDLE;
%     else
%         steering(i) = sensor(17,i);
%     end  
% end
% steering = fixpt_interp1( [DUTY_SERVO_SX, DUTY_SERVO_DX], [-100, 100] ,steering, sfix(16), 1, sfix(16), 0.01);
steering = (6881 - sensor(17,:))/4402;

% Traction percent map
for i=1:length(sensor(10,:))
    if(sensor(10,i)==0)
        traction(i) = DUTY_ESC_IDLE;
    else
        traction(i) = sensor(10,i);
    end  
end
traction = fixpt_interp1([DUTY_ESC_MIN, DUTY_ESC_MAX], [-100, 100], traction, sfix(16), 1, sfix(16), 0.01);

target = sensor(16,:);

% ------------------
%% Plots
% ------------------
tire_radius = 0.05;  % [m]
speed_request = sensor(16,:)*tire_radius/100; 
steer_request = (6881 - sensor(17,:))/4402; 
if (~enable_docked)
    figure('Name','Controls','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,700,400])
else
    figure('Name','Controls','NumberTitle','off'), clf
end    
% --- u --- %
ax(1) = subplot(121);
hold on
plot(time,speed_request,'-b','LineWidth',2)
grid on
title('$u$ [m/s]')
% --- delta --- %
ax(2) = subplot(122);
hold on
plot(time,rad2deg(steer_request),'-b','LineWidth',2)
grid on
title('$\delta$ [deg]')

% plot to check the goodness of the test
% figure(1)
% plot( (enc_rr+enc_rl)/2)
% hold on
% plot(target/100)

% ------------------
%% Create a txt file with telemetry data
% ------------------
% Creation of an incremental file name for different tests
test = "test1.txt";             % first file
i = 1;                          % variable inizialization
check = 0;

while(check == 0)               % loop for creation of brand new file
    if exist(test, 'file')
        test = strcat('test', num2str(i), '.txt');
    else
        check = 1;
    end
    i = i+1;
end

% creation of the txt file
today = datestr(now);
fileID = fopen(test, 'w');
fprintf(fileID, '% s\t % s\n', today, output_telem_fileName);
fprintf(fileID, '% 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t  % 8s\t % 8s\t % 8s\t % 8s\n', '[s]', '[g]', '[g]', '[g]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[rad/s]', '[g]', '[g]', '[g]', '[%]', '[%}', '[rad/s]', '[mm]', '[mm]', '[grad]');
fprintf(fileID, '% 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\t % 8s\n', 'time', 'a_x_mpu', 'a_y_mpu', 'a_z_mpu', 'gyro_x_mpu', 'gyro_y_mpu', 'gyro_z_mpu', 'enc_fr', 'enc_fl', 'enc_rr', 'enc_rl', 'a_x_raw', 'a_y_raw', 'a_z_raw', 'steering', 'traction', 'target', 'x_gps', 'y_gps', 'yaw_gps');
fprintf(fileID, '% 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\t % 3.5f\n', [time; a_x_mpu; a_y_mpu; a_z_mpu; gyro_x_mpu; gyro_y_mpu; gyro_z_mpu; enc_fr; enc_fl; enc_rr; enc_rl; a_x_raw; a_y_raw; a_z_raw; steering; traction; target; x_gps; y_gps; yaw_gps]);
fclose('all');

