% ---------------------------------------------------------
%% Script to send data from Windows to MacBook Pro via UDP
% ---------------------------------------------------------

%% Run this script on the Windows PC that has to send data to the MacBook

clc; clearvars;
format long;

% Create udp sender
udps = dsp.UDPSender('RemoteIPAddress','192.168.1.204','RemoteIPPort',25000);

freq_sendUDP = 10;  % [Hz] frequency at which the data are sent to the MacBook
sampleTime_sendUDP = 1/freq_sendUDP;

% Timer to schedule the message exchange with a certain timestep
timerObj_testUDP = timer('TimerFcn','sendToMac_now = timerFunct_sendUDP(sendToMac_now);','Period',sampleTime_sendUDP, ...   
    'ExecutionMode','fixedRate','BusyMode','queue','StopFcn','disp(''Timer has stopped.'')'); 
start(timerObj_testUDP);
sendToMac_now = 0;  % reset timer flag

dataSen  = ones(10000,3)*NaN;  % initialize
idx_send = 1;

pause(5);
fprintf('Starting to send data\n');

% Send data
tic;
while(toc<=10)
    if (sendToMac_now==2)
        sendToMac_now = 0;
        dataSent = [3,2];
        udps(dataSent);
        dataSen(idx_send,1:2) = dataSent;
        dataSen(idx_send,3)   = str2double(datestr(clock,'SS.FFF'));
        idx_send = idx_send+1;
    end
end
    
stop(timerObj_testUDP);
delete(timerObj_testUDP);

release(udps);

dataSent_store = dataSen((all((~isnan(dataSen)),2)),:);

