% ----------------------------------------------------------------
%% Main script for eRumby handling & max performance identification
%   - authors: Mattia Piccinini & Davide Piscini
%   - date:    15/12/2019
% ----------------------------------------------------------------

% ---------------------
%% Initialize
% ---------------------
initialize_script;
% Enter the test name to read the corresponding files
testName = 'test5';

% Set this flag to 1 in order to plot the handling diagram 
enable_plot_handlingDiagr = 0;

enable_steerOffset_ident = 0;

% ---------------------
%% Load vehicle data
% ---------------------
vehicle_data = getVehicleDataStruct();
m = vehicle_data.vehicle.m;            % [kg] Vehicle mass
L = vehicle_data.vehicle.L;            % [m] Vehicle wheelbase
delta_offs = vehicle_data.vehicle.delta_offs;  % [rad] offset steering angle to let the vehicle go straight
r_tire = vehicle_data.vehicle.r_tire;

% ---------------------
%% Import telemetry & OptiTrack sensors data
% ---------------------
Import_Maneuver_Data;

% ---------------------
%% Extract, filter and resize sensors data
% ---------------------
%     --> subscript _T is used for telemetry data
%     --> subscript _O is used for optitrack data
getTestData;

% ---------------------
%% Plot sensors data
% ---------------------
plotSensorsData;
 
% ---------------------
%% Steering behavior
% ---------------------
% Plot handling diagram
if (enable_plot_handlingDiagr)
    plotHandlingDiagram;
end

if (enable_steerOffset_ident)
    steerOffset_identif;  
end
