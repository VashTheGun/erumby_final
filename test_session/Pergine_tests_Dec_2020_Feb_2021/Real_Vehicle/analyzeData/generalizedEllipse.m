function genEllipse = generalizedEllipse(ax,ay,ax_max,ax_min,ay_max,nupp,nlow,ax_offs)

   genEllipse = posSign(ax-ax_offs).*((absReg(ay)/ay_max).^nupp + (absReg(ax-ax_offs)/ax_max).^nupp) - ... 
                negSign(ax-ax_offs).*((absReg(ay)/ay_max).^nlow + (absReg(ax-ax_offs)/ax_min).^nlow) - 1;

end

