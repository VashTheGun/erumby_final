% -------------------------------------------------------
%% Fitting of the Kus coefficients as a function of the vehicle speed 
% -------------------------------------------------------

% Extract the already calculated Kus fitting coefficients as a function of Ay, for each speed level 
K_us_vsAy_speed100_load = load('./fitting_Kus_coeffs_vs_Ay/Kus_vs_Ay_speed100');
K_us_vsAy_speed124_load = load('./fitting_Kus_coeffs_vs_Ay/Kus_vs_Ay_speed124');
K_us_vsAy_speed145_load = load('./fitting_Kus_coeffs_vs_Ay/Kus_vs_Ay_speed145');
K_us_vsAy_speed163_load = load('./fitting_Kus_coeffs_vs_Ay/Kus_vs_Ay_speed163');
K_us_vsAy_speed100 = K_us_vsAy_speed100_load.K_us_fitCoeffs_vsAy;
K_us_vsAy_speed124 = K_us_vsAy_speed124_load.K_us_fitCoeffs_vsAy;
K_us_vsAy_speed145 = K_us_vsAy_speed145_load.K_us_fitCoeffs_vsAy;
K_us_vsAy_speed163 = K_us_vsAy_speed163_load.K_us_fitCoeffs_vsAy;

% List of speed values at which the maneuvers are carried out
speed_vect = [1.24,1.45,1.63];  % [m/s]
speed_vect_interp = speed_vect(1):0.1:ceil(speed_vect(end)*10)/10;
speed_vect_fit = 0:0.01:max(speed_vect);  % used just to plot the fitting results

K_us_fitCoeffs_vsAy_global = [%K_us_vsAy_speed100; ...
                              K_us_vsAy_speed124; ...
                              K_us_vsAy_speed145; ...
                              K_us_vsAy_speed163];

K_us_0 = K_us_fitCoeffs_vsAy_global(:,1);
K_us_1 = K_us_fitCoeffs_vsAy_global(:,2);
K_us_2 = K_us_fitCoeffs_vsAy_global(:,3);
K_us_3 = K_us_fitCoeffs_vsAy_global(:,4);
K_us_4 = K_us_fitCoeffs_vsAy_global(:,5);
K_us_5 = K_us_fitCoeffs_vsAy_global(:,6);
K_us_6 = K_us_fitCoeffs_vsAy_global(:,7);
K_us_7 = K_us_fitCoeffs_vsAy_global(:,8);

K_us_0_interp = interp1(speed_vect,K_us_0,speed_vect_interp,'makima')';
K_us_1_interp = interp1(speed_vect,K_us_1,speed_vect_interp,'makima')';
K_us_2_interp = interp1(speed_vect,K_us_2,speed_vect_interp,'makima')';
K_us_3_interp = interp1(speed_vect,K_us_3,speed_vect_interp,'makima')';
K_us_4_interp = interp1(speed_vect,K_us_4,speed_vect_interp,'makima')';
K_us_5_interp = interp1(speed_vect,K_us_5,speed_vect_interp,'makima')';
K_us_6_interp = interp1(speed_vect,K_us_6,speed_vect_interp,'makima')';
K_us_7_interp = interp1(speed_vect,K_us_7,speed_vect_interp,'makima')';  %'linear','extrap'

% Remove any duplicates (if present) in the previous vectors
% [speed_vect,indeces_nonDuplic] = unique(speed_vect);
% K_us_1 = K_us_1(indeces_nonDuplic);
% K_us_2 = K_us_2(indeces_nonDuplic);
% K_us_3 = K_us_3(indeces_nonDuplic);
% K_us_4 = K_us_4(indeces_nonDuplic);
% K_us_5 = K_us_5(indeces_nonDuplic);

% ----------------------------------
%% Fitting with polynomials
% ----------------------------------
% ---- kus_0 ---- %
K_us_0_vs_speed = @(x,speed_vect)(x(1) + x(2)*speed_vect + x(3)*speed_vect.^2 + x(4)*speed_vect.^3); 
x0 = zeros(1,4);
options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-12,...
            'StepTolerance',1e-15,'OptimalityTolerance',1e-9,'Display','final-detailed');
[optim_coeffs_K_us_0,resnorm_K_us_0,~,exitflag_K_us_0,output_K_us_0] = lsqcurvefit(K_us_0_vs_speed,x0,speed_vect,K_us_0',[],[],options);
K_us_0_vsSpeed_0 = optim_coeffs_K_us_0(1);
K_us_0_vsSpeed_1 = optim_coeffs_K_us_0(2);
K_us_0_vsSpeed_2 = optim_coeffs_K_us_0(3);
K_us_0_vsSpeed_3 = optim_coeffs_K_us_0(4);
K_us_0_vsSpeed_coeffs = [K_us_0_vsSpeed_0,K_us_0_vsSpeed_1,K_us_0_vsSpeed_2,K_us_0_vsSpeed_3];
K_us_0_lowSpeed = K_us_0_vsSpeed_0 + K_us_0_vsSpeed_1*speed_vect(1) + K_us_0_vsSpeed_2*speed_vect(1)^2 + K_us_0_vsSpeed_3*speed_vect(1)^3;
K_us_0_vsSpeed_fitted = -K_us_0_lowSpeed*negSign(speed_vect_fit-speed_vect(1)) + (K_us_0_vsSpeed_0 + K_us_0_vsSpeed_1*speed_vect_fit + K_us_0_vsSpeed_2*speed_vect_fit.^2 + K_us_0_vsSpeed_3*speed_vect_fit.^3).*posSign(speed_vect_fit-speed_vect(1));
save('./fitting_Kus_coeffs_vs_speed/fitted_K_us_0_vs_speed_poly','K_us_0_vsSpeed_coeffs');
% ---- kus_1 ---- %
K_us_1_vs_speed = @(x,speed_vect)(x(1) + x(2)*speed_vect + x(3)*speed_vect.^2 + x(4)*speed_vect.^3); 
x0 = zeros(1,4);
options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-12,...
            'StepTolerance',1e-15,'OptimalityTolerance',1e-9,'Display','final-detailed');
[optim_coeffs_K_us_1,resnorm_K_us_1,~,exitflag_K_us_1,output_K_us_1] = lsqcurvefit(K_us_1_vs_speed,x0,speed_vect,K_us_1',[],[],options);
K_us_1_vsSpeed_0 = optim_coeffs_K_us_1(1);
K_us_1_vsSpeed_1 = optim_coeffs_K_us_1(2);
K_us_1_vsSpeed_2 = optim_coeffs_K_us_1(3);
K_us_1_vsSpeed_3 = optim_coeffs_K_us_1(4);
K_us_1_vsSpeed_coeffs = [K_us_1_vsSpeed_0,K_us_1_vsSpeed_1,K_us_1_vsSpeed_2,K_us_1_vsSpeed_3];
K_us_1_lowSpeed = K_us_1_vsSpeed_0 + K_us_1_vsSpeed_1*speed_vect(1) + K_us_1_vsSpeed_2*speed_vect(1)^2 + K_us_1_vsSpeed_3*speed_vect(1)^3;
K_us_1_vsSpeed_fitted = -K_us_1_lowSpeed*negSign(speed_vect_fit-speed_vect(1)) + (K_us_1_vsSpeed_0 + K_us_1_vsSpeed_1*speed_vect_fit + K_us_1_vsSpeed_2*speed_vect_fit.^2 + K_us_1_vsSpeed_3*speed_vect_fit.^3).*posSign(speed_vect_fit-speed_vect(1));
save('./fitting_Kus_coeffs_vs_speed/fitted_K_us_1_vs_speed_poly','K_us_1_vsSpeed_coeffs');
% ---- kus_2 ---- %
K_us_2_vs_speed = @(x,speed_vect)(x(1) + x(2)*speed_vect + x(3)*speed_vect.^2 + x(4)*speed_vect.^3); 
x0 = zeros(1,4);
options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-12,...
            'StepTolerance',1e-15,'OptimalityTolerance',1e-9,'Display','final-detailed');
[optim_coeffs_K_us_2,resnorm_K_us_2,~,exitflag_K_us_2,output_K_us_2] = lsqcurvefit(K_us_2_vs_speed,x0,speed_vect,K_us_2',[],[],options);
K_us_2_vsSpeed_0 = optim_coeffs_K_us_2(1);
K_us_2_vsSpeed_1 = optim_coeffs_K_us_2(2);
K_us_2_vsSpeed_2 = optim_coeffs_K_us_2(3);
K_us_2_vsSpeed_3 = optim_coeffs_K_us_2(4);
K_us_2_vsSpeed_coeffs = [K_us_2_vsSpeed_0,K_us_2_vsSpeed_1,K_us_2_vsSpeed_2,K_us_2_vsSpeed_3];
K_us_2_lowSpeed = K_us_2_vsSpeed_0 + K_us_2_vsSpeed_1*speed_vect(1) + K_us_2_vsSpeed_2*speed_vect(1)^2 + K_us_2_vsSpeed_3*speed_vect(1)^3;
K_us_2_vsSpeed_fitted = -K_us_2_lowSpeed*negSign(speed_vect_fit-speed_vect(1)) + (K_us_2_vsSpeed_0 + K_us_2_vsSpeed_1*speed_vect_fit + K_us_2_vsSpeed_2*speed_vect_fit.^2 + K_us_2_vsSpeed_3*speed_vect_fit.^3).*posSign(speed_vect_fit-speed_vect(1));
save('./fitting_Kus_coeffs_vs_speed/fitted_K_us_2_vs_speed_poly','K_us_2_vsSpeed_coeffs');
% ---- kus_3 ---- %
K_us_3_vs_speed = @(x,speed_vect)(x(1) + x(2)*speed_vect + x(3)*speed_vect.^2 + x(4)*speed_vect.^3); 
x0 = zeros(1,4);
options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-12,...
            'StepTolerance',1e-15,'OptimalityTolerance',1e-9,'Display','final-detailed');
[optim_coeffs_K_us_3,resnorm_K_us_3,~,exitflag_K_us_3,output_K_us_3] = lsqcurvefit(K_us_3_vs_speed,x0,speed_vect,K_us_3',[],[],options);
K_us_3_vsSpeed_0 = optim_coeffs_K_us_3(1);
K_us_3_vsSpeed_1 = optim_coeffs_K_us_3(2);
K_us_3_vsSpeed_2 = optim_coeffs_K_us_3(3);
K_us_3_vsSpeed_3 = optim_coeffs_K_us_3(4);
K_us_3_vsSpeed_coeffs = [K_us_3_vsSpeed_0,K_us_3_vsSpeed_1,K_us_3_vsSpeed_2,K_us_3_vsSpeed_3];
K_us_3_lowSpeed = K_us_3_vsSpeed_0 + K_us_3_vsSpeed_1*speed_vect(1) + K_us_3_vsSpeed_2*speed_vect(1)^2 + K_us_3_vsSpeed_3*speed_vect(1)^3;
K_us_3_vsSpeed_fitted = -K_us_3_lowSpeed*negSign(speed_vect_fit-speed_vect(1)) + (K_us_3_vsSpeed_0 + K_us_3_vsSpeed_1*speed_vect_fit + K_us_3_vsSpeed_2*speed_vect_fit.^2 + K_us_3_vsSpeed_3*speed_vect_fit.^3).*posSign(speed_vect_fit-speed_vect(1));
save('./fitting_Kus_coeffs_vs_speed/fitted_K_us_3_vs_speed_poly','K_us_3_vsSpeed_coeffs');
% ---- kus_4 ---- %
K_us_4_vs_speed = @(x,speed_vect)(x(1) + x(2)*speed_vect + x(3)*speed_vect.^2 + x(4)*speed_vect.^3); 
x0 = zeros(1,4);
options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-12,...
            'StepTolerance',1e-15,'OptimalityTolerance',1e-9,'Display','final-detailed');
[optim_coeffs_K_us_4,resnorm_K_us_4,~,exitflag_K_us_4,output_K_us_4] = lsqcurvefit(K_us_4_vs_speed,x0,speed_vect_interp,K_us_4_interp',[],[],options);
K_us_4_vsSpeed_0 = optim_coeffs_K_us_4(1);
K_us_4_vsSpeed_1 = optim_coeffs_K_us_4(2);
K_us_4_vsSpeed_2 = optim_coeffs_K_us_4(3);
K_us_4_vsSpeed_3 = optim_coeffs_K_us_4(4);
K_us_4_vsSpeed_coeffs = [K_us_4_vsSpeed_0,K_us_4_vsSpeed_1,K_us_4_vsSpeed_2,K_us_4_vsSpeed_3];
K_us_4_lowSpeed = K_us_4_vsSpeed_0 + K_us_4_vsSpeed_1*speed_vect(1) + K_us_4_vsSpeed_2*speed_vect(1)^2 + K_us_4_vsSpeed_3*speed_vect(1)^3;
K_us_4_vsSpeed_fitted = -K_us_4_lowSpeed*negSign(speed_vect_fit-speed_vect(1)) + (K_us_4_vsSpeed_0 + K_us_4_vsSpeed_1*speed_vect_fit + K_us_4_vsSpeed_2*speed_vect_fit.^2 + K_us_4_vsSpeed_3*speed_vect_fit.^3).*posSign(speed_vect_fit-speed_vect(1));
save('./fitting_Kus_coeffs_vs_speed/fitted_K_us_4_vs_speed_poly','K_us_4_vsSpeed_coeffs');
% ---- kus_5 ---- %
K_us_5_vs_speed = @(x,speed_vect)(x(1) + x(2)*speed_vect + x(3)*speed_vect.^2 + x(4)*speed_vect.^3); 
x0 = zeros(1,4);
options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-12,...
            'StepTolerance',1e-15,'OptimalityTolerance',1e-9,'Display','final-detailed');
[optim_coeffs_K_us_5,resnorm_K_us_5,~,exitflag_K_us_5,output_K_us_5] = lsqcurvefit(K_us_5_vs_speed,x0,speed_vect,K_us_5',[],[],options);
K_us_5_vsSpeed_0 = optim_coeffs_K_us_5(1);
K_us_5_vsSpeed_1 = optim_coeffs_K_us_5(2);
K_us_5_vsSpeed_2 = optim_coeffs_K_us_5(3);
K_us_5_vsSpeed_3 = optim_coeffs_K_us_5(4);
K_us_5_vsSpeed_coeffs = [K_us_5_vsSpeed_0,K_us_5_vsSpeed_1,K_us_5_vsSpeed_2,K_us_5_vsSpeed_3];
K_us_5_lowSpeed = K_us_5_vsSpeed_0 + K_us_5_vsSpeed_1*speed_vect(1) + K_us_5_vsSpeed_2*speed_vect(1)^2 + K_us_5_vsSpeed_3*speed_vect(1)^3;
K_us_5_vsSpeed_fitted = -K_us_5_lowSpeed*negSign(speed_vect_fit-speed_vect(1)) + (K_us_5_vsSpeed_0 + K_us_5_vsSpeed_1*speed_vect_fit + K_us_5_vsSpeed_2*speed_vect_fit.^2 + K_us_5_vsSpeed_3*speed_vect_fit.^3).*posSign(speed_vect_fit-speed_vect(1));
save('./fitting_Kus_coeffs_vs_speed/fitted_K_us_5_vs_speed_poly','K_us_5_vsSpeed_coeffs');
% ---- kus_6 ---- %
K_us_6_vs_speed = @(x,speed_vect)(x(1) + x(2)*speed_vect + x(3)*speed_vect.^2 + x(4)*speed_vect.^3); 
x0 = zeros(1,4);
options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-12,...
            'StepTolerance',1e-15,'OptimalityTolerance',1e-9,'Display','final-detailed');
[optim_coeffs_K_us_6,resnorm_K_us_6,~,exitflag_K_us_6,output_K_us_6] = lsqcurvefit(K_us_6_vs_speed,x0,speed_vect_interp,K_us_6_interp',[],[],options);
K_us_6_vsSpeed_0 = optim_coeffs_K_us_6(1);
K_us_6_vsSpeed_1 = optim_coeffs_K_us_6(2);
K_us_6_vsSpeed_2 = optim_coeffs_K_us_6(3);
K_us_6_vsSpeed_3 = optim_coeffs_K_us_6(4);
K_us_6_vsSpeed_coeffs = [K_us_6_vsSpeed_0,K_us_6_vsSpeed_1,K_us_6_vsSpeed_2,K_us_6_vsSpeed_3];
K_us_6_lowSpeed = K_us_6_vsSpeed_0 + K_us_6_vsSpeed_1*speed_vect(1) + K_us_6_vsSpeed_2*speed_vect(1)^2 + K_us_6_vsSpeed_3*speed_vect(1)^3;
K_us_6_vsSpeed_fitted = -K_us_6_lowSpeed*negSign(speed_vect_fit-speed_vect(1)) + (K_us_6_vsSpeed_0 + K_us_6_vsSpeed_1*speed_vect_fit + K_us_6_vsSpeed_2*speed_vect_fit.^2 + K_us_6_vsSpeed_3*speed_vect_fit.^3).*posSign(speed_vect_fit-speed_vect(1));
save('./fitting_Kus_coeffs_vs_speed/fitted_K_us_6_vs_speed_poly','K_us_6_vsSpeed_coeffs');
% ---- kus_7 ---- %
K_us_7_vs_speed = @(x,speed_vect)(x(1) + x(2)*speed_vect + x(3)*speed_vect.^2 + x(4)*speed_vect.^3); 
x0 = zeros(1,4);
options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-12,...
            'StepTolerance',1e-15,'OptimalityTolerance',1e-9,'Display','final-detailed');
[optim_coeffs_K_us_7,resnorm_K_us_7,~,exitflag_K_us_7,output_K_us_7] = lsqcurvefit(K_us_7_vs_speed,x0,speed_vect_interp,K_us_7_interp',[],[],options);
K_us_7_vsSpeed_0 = optim_coeffs_K_us_7(1);
K_us_7_vsSpeed_1 = optim_coeffs_K_us_7(2);
K_us_7_vsSpeed_2 = optim_coeffs_K_us_7(3);
K_us_7_vsSpeed_3 = optim_coeffs_K_us_7(4);
K_us_7_vsSpeed_coeffs = [K_us_7_vsSpeed_0,K_us_7_vsSpeed_1,K_us_7_vsSpeed_2,K_us_7_vsSpeed_3];
K_us_7_lowSpeed = K_us_7_vsSpeed_0 + K_us_7_vsSpeed_1*speed_vect(1) + K_us_7_vsSpeed_2*speed_vect(1)^2 + K_us_7_vsSpeed_3*speed_vect(1)^3;
K_us_7_vsSpeed_fitted = -K_us_7_lowSpeed*negSign(speed_vect_fit-speed_vect(1)) + (K_us_7_vsSpeed_0 + K_us_7_vsSpeed_1*speed_vect_fit + K_us_7_vsSpeed_2*speed_vect_fit.^2 + K_us_7_vsSpeed_3*speed_vect_fit.^3).*posSign(speed_vect_fit-speed_vect(1));
save('./fitting_Kus_coeffs_vs_speed/fitted_K_us_7_vs_speed_poly','K_us_7_vsSpeed_coeffs');


% ----------------------------------
%% Plot the fitting Kus coefficients, as a function of the vehicle speed
% ----------------------------------
enable_docked = 1;
if (~enable_docked)
    figure('Name','Kus_coeffs_vs_speed','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name','Kus_coeffs_vs_speed','NumberTitle','off'), clf
end    
% --- Kus_0 --- %
ax(1) = subplot(331);
hold on
plot(speed_vect,K_us_0,'mo','MarkerFaceColor','m','MarkerSize',8)
plot(speed_vect_interp,K_us_0_interp,'go','MarkerSize',5)
plot(speed_vect_fit,K_us_0_vsSpeed_fitted,'-b','LineWidth',2)
grid on
xlabel('speed [m/s]')
title('$K_{us0}$')
legend('real','interp','fitted')
xlim([speed_vect_fit(1) speed_vect_fit(end)])
% --- Kus_1 --- %
ax(2) = subplot(332);
hold on
plot(speed_vect,K_us_1,'mo','MarkerFaceColor','m','MarkerSize',8)
plot(speed_vect_fit,K_us_1_vsSpeed_fitted,'-b','LineWidth',2)
grid on
xlabel('speed [m/s]')
title('$K_{us1}$')
xlim([speed_vect_fit(1) speed_vect_fit(end)])
% --- Kus_2 --- %
ax(3) = subplot(333);
hold on
plot(speed_vect,K_us_2,'mo','MarkerFaceColor','m','MarkerSize',8)
plot(speed_vect_fit,K_us_2_vsSpeed_fitted,'-b','LineWidth',2)
grid on
xlabel('speed [m/s]')
title('$K_{us2}$')
xlim([speed_vect_fit(1) speed_vect_fit(end)])
% --- Kus_3 --- %
ax(4) = subplot(334);
hold on
plot(speed_vect,K_us_3,'mo','MarkerFaceColor','m','MarkerSize',8)
plot(speed_vect_fit,K_us_3_vsSpeed_fitted,'-b','LineWidth',2)
grid on
xlabel('speed [m/s]')
title('$K_{us3}$')
xlim([speed_vect_fit(1) speed_vect_fit(end)])
% --- Kus_4 --- %
ax(5) = subplot(335);
hold on
plot(speed_vect,K_us_4,'mo','MarkerFaceColor','m','MarkerSize',8)
plot(speed_vect_fit,K_us_4_vsSpeed_fitted,'-b','LineWidth',2)
grid on
xlabel('speed [m/s]')
title('$K_{us4}$')
xlim([speed_vect_fit(1) speed_vect_fit(end)])
% --- Kus_5 --- %
ax(6) = subplot(336);
hold on
plot(speed_vect,K_us_5,'mo','MarkerFaceColor','m','MarkerSize',8)
plot(speed_vect_fit,K_us_5_vsSpeed_fitted,'-b','LineWidth',2)
grid on
xlabel('speed [m/s]')
title('$K_{us5}$')
xlim([speed_vect_fit(1) speed_vect_fit(end)])
% --- Kus_6 --- %
ax(7) = subplot(337);
hold on
plot(speed_vect,K_us_6,'mo','MarkerFaceColor','m','MarkerSize',8)
plot(speed_vect_fit,K_us_6_vsSpeed_fitted,'-b','LineWidth',2)
grid on
xlabel('speed [m/s]')
title('$K_{us6}$')
xlim([speed_vect_fit(1) speed_vect_fit(end)])
% --- Kus_7 --- %
ax(8) = subplot(338);
hold on
plot(speed_vect,K_us_7,'mo','MarkerFaceColor','m','MarkerSize',8)
plot(speed_vect_fit,K_us_7_vsSpeed_fitted,'-b','LineWidth',2)
grid on
xlabel('speed [m/s]')
title('$K_{us7}$')
xlim([speed_vect_fit(1) speed_vect_fit(end)])

% linkaxes(ax,'x')
clear ax


%% Save points for spline fitting in PINS
Kus_splineFit = fopen('./fitting_Kus_coeffs_vs_speed/fitted_Kus_vs_speed.txt','w');
speed_vect_splines_save = [0.75,1,1.24,1.45,1.63,2];  % [m/s]
K_us_6_splines_save = [K_us_6(1),K_us_6(1),K_us_6',K_us_6(end)];
K_us_7_splines_save = [K_us_7(1),K_us_7(1),K_us_7',K_us_7(end)];
% Headers
fprintf(Kus_splineFit,'%s\t%s\t%s\n','speed','Kus_6','Kus_7');
% Values
for ii=1:length(speed_vect_splines_save)
    fprintf(Kus_splineFit,'%.2f\t%.16f\t%.16f\n',speed_vect_splines_save(ii),K_us_6_splines_save(ii),K_us_7_splines_save(ii));
end
fclose(Kus_splineFit);

