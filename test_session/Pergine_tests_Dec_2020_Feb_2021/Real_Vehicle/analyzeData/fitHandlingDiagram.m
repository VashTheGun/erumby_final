% --------------------------------------
%% Fitting of the handling diagram
% --------------------------------------

% Compute the steering tendency for the handling diagram
mean_speed = mean(Speed_O_filt_restrict);
steer_tendency = delta_restrict - (Omega_T_filt_restrict./mean_speed)*L;  
fin_sample_handling = length(steer_tendency);
steer_tendency_resize = steer_tendency(1:fin_sample_handling);
Ay_ss_resize = Ay_ss_filt_restrict(1:fin_sample_handling);

check_path = strcat('./HandlingDiagr_Points/speed',num2str(round(mean_speed,2)*100));
if (isfile(strcat(check_path,'/Ay_points.mat')))
    Ay_ss_points_load = load(strcat(check_path,'/Ay_points')); 
    steer_tendency_points_load = load(strcat(check_path,'/SteerTenden_points')); 
    Ay_ss_points_use = Ay_ss_points_load.Ay_points_use;
    steer_tendency_points_use = steer_tendency_points_load.steer_tenden_points_use;
    Ay_ss_points_use_interp = floor(min(Ay_ss_points_use)*100)/100:0.01:ceil(max(Ay_ss_points_use)*100)/100;
    steer_tendency_points_use_interp = interp1(Ay_ss_points_use,steer_tendency_points_use,Ay_ss_points_use_interp,'makima');
    enable_fit_Kus_vs_Ay = 1;
else
    enable_fit_Kus_vs_Ay = 0;
end

fitting_Kus_vs_Ay;


