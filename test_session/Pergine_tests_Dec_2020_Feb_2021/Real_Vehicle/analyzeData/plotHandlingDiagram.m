% --------------------------------------
%% Plot the handling diagram
% --------------------------------------

% Compute the steering tendency for the handling diagram
steer_tendency = delta_restrict - (Omega_T_filt_restrict./Speed_O_filt_restrict)*L;  
fin_sample_handling = length(steer_tendency);
steer_tendency_resize = steer_tendency(1:fin_sample_handling);
Ay_ss_resize = Ay_ss_filt_restrict(1:fin_sample_handling);


if (~enable_docked)
    figure('Name','HandlDiagr','NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,700,400])
else
    figure('Name','HandlDiagr','NumberTitle','off'), clf
end
hold on
plot(Ay_ss_resize,steer_tendency_resize,'-b','LineWidth',3)
grid on
xlabel('$a_y$ [m/s$^2$]')
ylabel('$\delta-\frac{\Omega}{u} L$ [rad]')
title('Handling Diagram')
legend('test 1','location','best')
xlim([min(Ay_ss_resize) max(Ay_ss_resize)])




