% ---------------------------------------------
%% Import telemetry and OptiTrack sensors data 
% ---------------------------------------------

telemetryData = strcat('../deployCode_and_readSensorsData/',testName,'.txt');
[telemetry] = telemetry_import_V2(telemetryData);
fprintf('\nThe txt telemetry data have been imported.\n')

optitrackData = strcat('../deployCode_and_readSensorsData/',testName,'.csv');
% Low-pass filtering flag -> if filter_flag = 0 then no filtering is applied
filter_flag = 0;
% Variable used to align (in post-proc) the optitrack data with the on-board vehicle sensors
align_variable = 'none';  % 'speed'  'yaw_rate'
% flag to be set to 1 in order to 'clean up' the gps optitrack data recorded on the raspberry 
% (due to UDP delays and other factors, the sampling rate for these data is usually lower than 100 Hz, 
% even if the 'pure' optitrack data have a sampling rate of around 120 Hz)
clean_gps_telem = 0;  
if (~exist(optitrackData,'file'))
    prompt = '\nDo you want to manually load the Optitrack data now?[y/n]\n';
    answer_load_Optitrack = input(prompt,'s');
    if (strcmpi(answer_load_Optitrack,'y'))
        fprintf('Now please export a csv data file from Motive (Optitrack) and copy-paste it in the deployCode_and_readSensorsData folder.\n')
        fprintf('The csv file must have the same name as the txt file.\n')
        fprintf('Press enter when you have done it.\n')
        pause();
        [optitrack] = optitrack_import(optitrackData);
        fprintf('\nThe csv Optitrack data have been imported.\n')
        [remap_telemetry,remap_optitrack] = test_data_remap_V4(telemetry,optitrack,align_variable,filter_flag,clean_gps_telem);
    else
        remap_telemetry = test_data_remap_V4_no_optitrack(telemetry,filter_flag);
        remap_optitrack = NaN;
    end
else
    [optitrack] = optitrack_import(optitrackData);
    fprintf('\nThe csv Optitrack data have been imported.\n')
    [remap_telemetry,remap_optitrack] = test_data_remap_V4(telemetry,optitrack,align_variable,filter_flag,clean_gps_telem);
end

