function filteredData = resize_and_filter(sensorsData,filter_flag,init_sample,fin_sample,trailing_flag)

    % --------------------------------------------
    %% function purpose:  resize and filter sensors data
    % --------------------------------------------

    % ---------------------
    %% Extract data
    % ---------------------
    time    = sensorsData.time;
    Speed_O = sensorsData.Speed;
    delta   = sensorsData.delta;
    Omega_T = sensorsData.Omega_T;
    Ax_T    = sensorsData.Ax_T;
    
    % ---------------------
    %% Low-pass filter data
    % ---------------------
    if (filter_flag)
        % zero-phase digital low-pass filtering
        Wn_filter_speed = 0.0003;  % cutoff frequency of the low pass filter
        Wn_filter_Omega = 0.001;  
        [b_butt_speed,a_butt_speed] = butter(4,Wn_filter_speed,'low');
        [b_butt_Omega,a_butt_Omega] = butter(4,Wn_filter_Omega,'low');
        if (trailing_flag)
            Omega_T_dot_end = (Omega_T(end)-Omega_T(end-9))/0.01;
            Omega_T_trailed = [Omega_T, Omega_T(end) + Omega_T_dot_end*(1e-3:1e-3:1)];  
            Omega_T_trailed_filt    = filtfilt(b_butt_Omega,a_butt_Omega,Omega_T_trailed);
            Omega_T_filt    = Omega_T_trailed_filt(1:length(Omega_T));
        else
            Omega_T_filt            = filtfilt(b_butt_Omega,a_butt_Omega,Omega_T);  
        end
        Speed_O_filt    = filtfilt(b_butt_speed,a_butt_speed,Speed_O);
        Ax_T_filt       = filtfilt(b_butt_speed,a_butt_speed,Ax_T);
    else
        Omega_T_filt = Omega_T;
        Speed_O_filt = Speed_O;
        Ax_T_filt    = Ax_T;
    end
    
    % ---------------------
    %% Resize data
    % ---------------------
    % Resize some variables to consider only steady state operation
    time_restrict         = time(init_sample:end-fin_sample);
    delta_restrict        = delta(init_sample:end-fin_sample);
    Omega_T_filt_restrict = Omega_T_filt(init_sample:end-fin_sample);
    Speed_O_filt_restrict = Speed_O_filt(init_sample:end-fin_sample);
    Ax_T_filt_restrict    = Ax_T_filt(init_sample:end-fin_sample);
    Ay_ss_filt_restrict   = Speed_O_filt_restrict.*Omega_T_filt_restrict;
    
    % ---------------------
    %% Save filtered and resized data
    % ---------------------
    filteredData.time    = time_restrict;
    filteredData.delta   = delta_restrict;
    filteredData.Omega_T = Omega_T_filt_restrict;
    filteredData.Speed_O = Speed_O_filt_restrict;
    filteredData.Ay_ss_T = Ay_ss_filt_restrict;
    filteredData.Ax_T    = Ax_T_filt_restrict;

end

