% -------------------------------------------------------------------
%% Fit the understeering gradient Kus as a function of Ay --> Kus(Ay)
% -------------------------------------------------------------------

if (enable_fit_Kus_vs_Ay)
    Ay_sineSteer_fit = round(min(Ay_ss_points_use),2)-0.1:0.01:round(max(Ay_ss_points_use),2)+0.1;    % used only to plot the fitting curves
    steer_behavior_real = @(x,Ay_ss_points_use)(x(1) + x(2)*Ay_ss_points_use + x(3)*Ay_ss_points_use.^2 + x(4)*Ay_ss_points_use.^3 + x(5)*Ay_ss_points_use.^4 + x(6)*Ay_ss_points_use.^5 + x(7)*Ay_ss_points_use.^6 + x(8)*Ay_ss_points_use.^7); 
    x0_opt = zeros(1,8); %[0.01132,-0.04711,0.01377,0.08553,-0.04004,-0.1137,0.02415,0.05188];
    options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',50000,'MaxIterations',50000,'FunctionTolerance',1e-16,...
    'StepTolerance',1e-15,'OptimalityTolerance',1e-12,'Display','final-detailed');
    [optim_coeffs_real,resnorm_real,~,exitflag_real,output_real] = lsqcurvefit(steer_behavior_real,x0_opt,Ay_ss_points_use_interp,steer_tendency_points_use_interp,[],[],options);
    K_us_real_0 = optim_coeffs_real(1);
    K_us_real_1 = optim_coeffs_real(2);
    K_us_real_2 = optim_coeffs_real(3);
    K_us_real_3 = optim_coeffs_real(4);
    K_us_real_4 = optim_coeffs_real(5);
    K_us_real_5 = optim_coeffs_real(6);
    K_us_real_6 = optim_coeffs_real(7);
    K_us_real_7 = optim_coeffs_real(8);
    K_us_fitCoeffs_vsAy = [K_us_real_0,K_us_real_1,K_us_real_2,K_us_real_3,K_us_real_4,K_us_real_5,K_us_real_6,K_us_real_7];
    fitted_handlingDiagram = K_us_real_0 + K_us_real_1*Ay_sineSteer_fit + K_us_real_2*Ay_sineSteer_fit.^2 + K_us_real_3*Ay_sineSteer_fit.^3 + K_us_real_4*Ay_sineSteer_fit.^4 + K_us_real_5*Ay_sineSteer_fit.^5 + K_us_real_6*Ay_sineSteer_fit.^6 + K_us_real_7*Ay_sineSteer_fit.^7;
end

% Plot the original and the fitted handling diagrams
figName = strcat('handling diagram, speed = ',num2str(round(mean(Speed_O_filt_restrict),2)));
if (~enable_docked)
    figure('Name',figName,'NumberTitle','off','Position',[0,0,500,1000]), clf 
    set(gcf,'units','points','position',[150,150,600,350])
else
    figure('Name',figName,'NumberTitle','off'), clf
end 
hold on
plot(Ay_ss_resize,steer_tendency_resize,'-b','LineWidth',3)
if (enable_fit_Kus_vs_Ay)
    plot(Ay_ss_points_use,steer_tendency_points_use,'go','MarkerFaceColor','g','MarkerSize',8)
    plot(Ay_ss_points_use_interp,steer_tendency_points_use_interp,'bo','MarkerSize',5)
    plot(Ay_sineSteer_fit,fitted_handlingDiagram,'-','Color',color('orange'),'LineWidth',4)
    legend('raw','select','select-interp','fitted','location','best')
end
grid on
xlabel('$a_y$ [m/s$^2$]')
ylabel('$\delta - \frac{\Omega}{u}L$ [rad]')
% xlim([min(Ay_ss_resize) max(Ay_ss_resize)])
ylim_abs = max(abs(steer_tendency_resize));
% ylim([-ylim_abs ylim_abs])
title('Handling diagram')
test_label = strcat('speed',num2str(round(mean_speed,2)*100));

enable_saveHandlDiagr_points = 0;
if (enable_saveHandlDiagr_points)
%     tol_Ay = 0.001;
%     Ay_points_use = -3.6:0.1:3.6;
%     for ii=1:length(Ay_points_use)
%         idx_Ay_inRange = find(Ay_ss_resize<=3+tol_Ay & Ay_ss_resize>=3-tol_Ay)
%     end
    [Ay_points_use, steer_tenden_points_use] = ginput(30);
    save_dir = strcat('./HandlingDiagr_Points/',test_label);
    if (~exist(save_dir, 'dir'))
       mkdir(save_dir);
    end
    save(strcat(save_dir,'/Ay_points'),'Ay_points_use');
    save(strcat(save_dir,'/SteerTenden_points'),'steer_tenden_points_use');
end

if (enable_fit_Kus_vs_Ay)
    % Save the fitting results
    path_save_Kus_vs_Ay = strcat('./fitting_Kus_coeffs_vs_Ay/','Kus_vs_Ay_',test_label);
    save(path_save_Kus_vs_Ay,'K_us_fitCoeffs_vsAy');
end


% ----------------------
%% Plot for the paper
% ----------------------

enable_plot4Paper  = 1;
enable_video4Paper = 0;

if (enable_plot4Paper)
    set(0,'DefaultFigureWindowStyle','normal');
    set(0,'defaultAxesFontSize',22)
    set(0,'DefaultLegendFontSize',22)
    clear Ay_ss_paper steer_tendency_paper
    
    final_idx_paper = 350;
    Ay_ss_paper_cut          = Ay_ss_resize(1:end-final_idx_paper);
    steer_tendency_paper_cut = steer_tendency_resize(1:end-final_idx_paper);
    vx_paper_cut             = Speed_O_filt_restrict(1:end-final_idx_paper);
    Ax_paper_cut             = Ax_T_filt(1:end-final_idx_paper) + (k_D/m)*vx_paper_cut.^2;
    time_paper_cut           = time_restrict(1:end-350);
    
    if (~exist('Ay_points_add','var') || ~exist('steer_tenden_points_add','var'))
        Ay_points_add_load = load('./HandlingDiagr_Paper/Ay_points_add_HD');
        steer_tenden_points_add_load = load('./HandlingDiagr_Paper/steer_tenden_points_add_HD');
        Ay_points_add = Ay_points_add_load.Ay_points_add;
        steer_tenden_points_add = steer_tenden_points_add_load.steer_tenden_points_add;
    end
    Ay_ss_paper = [Ay_ss_paper_cut,Ay_points_add'];
    steer_tendency_paper = [steer_tendency_paper_cut,steer_tenden_points_add'];
    
    if (~enable_docked)
        figure('Name','HD for paper','NumberTitle','off','Position',[0,0,500,1000]), clf 
        set(gcf,'units','points','position',[550,250,430,350])
    else
        figure('Name','HD for paper','NumberTitle','off'), clf
    end 
    hold on
    plot(Ay_ss_paper,steer_tendency_paper,'-b','LineWidth',3)
    line([-5 -5],[-0.3 0.3],'Color',color('darkslate_grey'),'LineStyle','--','LineWidth',2);
    line([5 5],[-0.3 0.3],'Color',color('darkslate_grey'),'LineStyle','--','LineWidth',2,'HandleVisibility','off');
    if (enable_fit_Kus_vs_Ay)
        plot(Ay_sineSteer_fit,fitted_handlingDiagram,'-','Color',color('green'),'LineWidth',4)
        legend('raw','$a_y$ bounds','fitted','location','south')
    end
    grid on
    enable_getOtherPoints = 0;
    if (enable_getOtherPoints)
        clear Ay_points_add steer_tenden_points_add
        [Ay_points_add, steer_tenden_points_add] = ginput(20);
        save('./HandlingDiagr_Paper/Ay_points_add_HD','Ay_points_add');
        save('./HandlingDiagr_Paper/steer_tenden_points_add_HD','steer_tenden_points_add');
    end
    xlabel('$a_y$ [m/s$^2$]')
    ylabel('$\delta - \frac{\Omega}{v_x}L$ [rad]')
    xlim([min(Ay_ss_resize)-0.5 max(Ay_ss_resize)+0.5])
    ylim([min(steer_tendency_resize)-0.02 max(steer_tendency_resize)+0.02])
end


if (enable_video4Paper)
    video4Paper_HD;
    video4Paper_GG;
end


