# TEST 25/07/18

Serie di test per testare gli algoritmi di controllo di traiettoria, due controlli sono stati testati pursuit e preview in diverse
circostanze. La manovra testata è un double lane change.
NOTA (i dati del gps hanno un offset in maniera da far entrare il percorso nell'arena, quelli della telemetria lo hanno già incluso,
quelli del sist4ema optitrack no)

 * test1: pursuit (lookhead 2, velocità 20 rad/sec, mappa di sterzo vecchia)
 * test2: pursuit (lookhead 6, velocità 20 rad/sec, mappa di sterzo vecchia) 
 * test3: pursuit (lookhead 15, velocità 20 rad/sec, mappa di sterzo vecchia) 
 * test4: preview (lookhead 2, k1 0.5, k2 0.5, velocità 20 rad/sec, mappa di sterzo vecchia) 
 * test5: preview (lookhead 2, k1 0.5, k2 0.5, velocità 20 rad/sec, mappa di sterzo nuova)
 * test6: (lookhead 2, velocità 20 rad/sec, mappa di sterzo nuova) 
 * test7: (lookhead 6, velocità 20 rad/sec, mappa di sterzo nuova)
 * test8: preview (lookhead 2, k1 0.5, k2 0.5, velocità 40 rad/sec, mappa di sterzo nuova)
 * test9: preview (lookhead 2, k1 0.5, k2 0.5, velocità 50 rad/sec, mappa di sterzo nuova)