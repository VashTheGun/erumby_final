# TEST 17/05/18

serie di test effettuati per prendere confidenza con il sistema optitrack, i test sono sia in open loop che con il controllo di velocità longitudinale

 * test1: dritto a velocità di target imposta (20 rad/s) 
 * test2: curvilineo sx a velocità di target imposta (percetuale di sterzo 92%, v_targ= 20 rad/s) 
 * test3: curvilineo sx a velocità di target imposta (percetuale di sterzo 92%, v_targ= 40 rad/s) 
 * test4: dritto in open loop (esc 57%)
 * test5: curvilineo sx in open loop (sterzo = 92%, esc = 57%)