# TEST 20/09/18

Serie di test per testare gli algoritmi di controllo di traiettoria, il test è stato effettuato con la libreria per la generazione di clotoidi, il controllo genera una serie di clotoidi seguendone la curvatura per effettuare un double lane change.

 * test1: preview L = 1  v = 30 rad/s senza correzzione K_us
 * test2: preview L = 1  v = 30 rad/s con correzzione K_us
 * test3 vecchio test con controllo pid utile per il confronto.
 * test4 vecchio test con controllo pid utile per il confronto.
 * test5 preview L = 1 v = 30 rad/s partenza con veicolo disalineato rispetto al tracciato.

