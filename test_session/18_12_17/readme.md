# TEST 19/10/18

Serie di test con il nuovo controllore di velocità

 * Straight Test: test 1-3, sterzo in open loop e differenti gradini di velocità in ingresso.

   ​			test 4-6 sterzo controllato e differenti gradini di velocità in ingresso.

- ramp velocity: serie di test con angolo di sterzo costante e rampa di velocità, test eseguiti sia a destra che a sinistra
- rampsteering: serie di test a velocita costante e con rampa di sterzo sia a destra che a sinistra in ingresso, i test sono eseguiti per due diverse velocità

