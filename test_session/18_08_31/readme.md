# TEST 31/08/18

Serie di test per testare gli algoritmi di controllo di traiettoria, il test è stato effettuato con la libreria per la generazione di clotoidi, il controllo è di tipo preview e la clotoide genera un double lane change.

 * test1: preview a v = 20 rad/s
 * test2: preview a v = 50 rad/s