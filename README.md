# eRumby-Car

The eRumby is an autonomous RC electric vehicle based on the Kyosho INFERNO VE Race car.

The repo contains the following folders:

--> Datasheet sensor: datasheet for the main sensors
--> Documentation: main doc for the vehicle and control systems
--> test_session: different telemetry datasets. The telemetries for the identification maneuvers acquired on the Pergine circuit and used for the paper presented at the IROS 2020 conference are in the folder Pergine_tests_18_02_2020. The telemetries for the identification maneuvers and MPC control tests (used for IROS 2020 paper) are collected in the folder Identification_and_MPC_Telemetries_06_03_2020
--> code: the MPC folder contains the code and telemetries with the MPC algorithm applied to control the vehicle (results collected in the IROS 2020 paper). The lib folder contains some scripts to read telemetry and Optitrack data. The State_Estimation folder contains some state estimation algorithms. 
